({
	doInit : function(component, event, helper) {
        var action = component.get("c.getCookieInfo");
        
        action.setCallback(this, function(resp) {
            console.log(resp.getReturnValue());
            component.set("v.cookie", resp.getReturnValue());
        });
	    
	    setTimeout(
	        function(){
	            $A.enqueueAction(action);
	        },3000);
	},
	getCookie: function(component, event, helper) {
	    console.log(event.getParam('caseId'));
	    component.set("v.cookie", event.getParams().get('caseId'));
	}
})