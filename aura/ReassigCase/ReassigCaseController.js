({
	init : function(component, event, helper) {
		console.log('comp '+component.get('v.recordId'));
     component.set('v.selectedValue','Central Triage');
        helper.getInit(component, event, helper);
      
        
        
      
	},
     handleClick : function (component, event, helper) {
     
         var utilityBarAPI = component.find("utilitybar");
	  var eventHandler = function(response){
          	this.init(component, event, helper);
            console.log('one ',response);
        };
        
        utilityBarAPI.onUtilityClick({ 
               eventHandler: eventHandler 
        }).then(function(result){
            console.log('two',result);
            helper.saveHandler(component, event, helper);
        }).catch(function(error){
        	console.log('three',error);
        });
    }

})