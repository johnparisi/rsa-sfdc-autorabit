({
	saveHandler : function(component, event, helper) {
		console.log('in handler');
         helper.getCaseHandler(component, event, helper,true);
       
       
	},
    
	saveMethod : function(component, event, helper) {
		console.log('in save method handler');
         var action = component.get('c.saveCase');
        action.setParams({
            caseToUpdate:component.get('v.caseDetail'),
            SelectedValue:component.get('v.selectedValue')
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                   
               window.open('/lightning/o/Case/list?filterName=Recent','_self');
      
       
                
            }
        });
         $A.enqueueAction(action);
	},
  
     getInit : function(component, event, helper ){
        var action = component.get('c.getQueueNames');
        console.log('init handler');
       
      
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(component.get('v.recordId') != null){
                   helper.getCaseHandler(component, event, helper,false); 
                }
                   
                console.log('success', response.getReturnValue());
                var custs = [];
                var conts = response.getReturnValue();
                 console.log('case '+component.get('v.caseDetail'));
                for(var key in conts){
                   
                    custs.push({value:conts[key], key:key});
                }
               // console.log('custs '+custs);
                component.set("v.sectionLabels", custs);
                component.set('v.groupNames',response.getReturnValue());
                
            }
        });
         $A.enqueueAction(action);
    },
    getCaseHandler : function(component, event, helper, fromSaveMethod) {
        
      var action = component.get('c.getCase');
        action.setParams({
            caseId:component.get('v.recordId')
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('success', response.getReturnValue());
               component.set('v.caseDetail',response.getReturnValue());
                var custs = [];
                 var conts = component.get('v.sectionLabels');
                 for(var key in conts){
                       console.log('value '+ conts[key].value);
                       console.log('key '+key);
                    if(conts[key].value == component.get('v.caseDetail').Owner.Name){
                        custs.push({value:conts[key].value, key:conts[key].key, selected :true});
                    }else{
                        custs.push({value:conts[key].value, key:conts[key].key});
                    }
                    
                }
            
                 component.set('v.sectionLabels',custs);
                if(fromSaveMethod){
                    helper.saveMethod(component, event, helper);
                }
                
            }
        });
         $A.enqueueAction(action);
                       
    }           
})