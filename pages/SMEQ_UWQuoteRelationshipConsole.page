<apex:page standardController="Quote__c" standardStylesheets="false" showHeader="false" extensions="RelationshipControllerExtension">

    <apex:stylesheet value="{!URLFOR($Resource.SMEQ_UWRelationshipConsole, 'assets/styles/rmc-styles.css')}" />
    <apex:includeScript value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"/>
    <apex:includeScript value="/soap/ajax/26.0/connection.js"/>
    <apex:includeScript value="/support/console/28.0/integration.js"/>
    
    <script type="text/javascript">
    function openLink(Id) {
        var url = '/' + Id;
        if (sforce.console.isInConsole()) {
            sforce.console.getEnclosingPrimaryTabId(function(result){
                sforce.console.openSubtab(result.id, url, true, '', null);
            });
        } else {
            window.open(url, '_blank');
        }
    }
    </script>
    
    <!-- HTML for SF Lightning Design System -->
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en"/>
    <!-- Import the Design System style sheet -->
    <apex:slds />
    <!-- REQUIRED SLDS WRAPPER -->
    <div class="slds-scope">
   
        <div class="header-blue">
            <apex:form >
                <div class="slds-form-element">
                    <div class="slds-form-element__control float-left">
                        <div class="slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right search-bar">
                            <apex:inputText styleClass="slds-input slds-combobox__input search-text" html-placeholder="{!$Label.Search_Accounts_and_Contacts}" value="{!searchText}"/>
                            <span class="slds-icon_container slds-icon-utility-search slds-input__icon slds-input__icon_right" title="Search Accounts and Contacts">
                                <span class="slds-assistive-text">{!$Label.Search_Accounts_and_Contacts}</span>
                                <img src="{!URLFOR($Resource.SMEQ_UWRelationshipConsole, 'assets/img/search.svg')}" class="search-icon"/>
                            </span>
                        </div>
                    </div>
                    <div class="slds-p-horizontal--small slds-m-top--medium slds-size--1-of-1">
                        <apex:commandButton action="{!searchForParties}" value="{!UPPER($Label.SearchBtn)}" styleClass="slds-button slds-button--inverse float-left"/>
                    </div>
                    <div class="slds-p-horizontal--small slds-m-top--medium slds-size--1-of-1">
                        <apex:commandButton onclick="window.open('{!otherPartyAccountURL}');" value="{!$Label.New_Other_Party_Account}" styleClass="slds-button slds-button--neutral float-right margin-left-5"/>
                        <apex:commandButton onclick="window.open('{!otherPartyContactURL}');" value="{!$Label.New_Other_Party_Contact}" styleClass="slds-button slds-button--neutral float-right"/>
                    </div>
                </div>
            </apex:form>
        </div>
        <div class="slds-scrollable_y relationship-table">
            <apex:form id="theForm"> 
            <div id="rowsDiv">
                <table class="slds-table slds-table_bordered slds-table_cell-buffer fixed-table" id="relationshipsTable">
                    <thead>
                        <tr class="slds-text-title_caps">
                            <th scope="col" class="select-col">
                                <div class="sldc-truncate">{!$Label.Selected}</div>
                            </th>  
                            <th scope="col" class="account-contact-col">
                                <div class="slds-truncate">{!$Label.AccountOrContact}</div>
                            </th>
                            <th scope="col" class="account-contact-col">
                                <div class="slds-truncate">{!$Label.Street}</div>
                            </th>
                            <th scope="col" class="account-contact-col">
                                <div class="slds-truncate">{!$Label.Province}</div>
                            </th>
                            <th scope="col" class="party-relationship-col">
                                <div class="slds-truncate half-div">{!$Label.Party_Type}</div>
                                <div class="slds-truncate half-div padding-left-10">{!$Label.Relationship_Type}</div>
                            </th>
                            <th scope="col" class="action-col">{!$Label.Action}</th>
                        </tr>
                    </thead>
                    <tbody>                  
                        <apex:repeat value="{!partiesList}" var="r" id="repeater">
                            <tr id="row">
                                <td class="width-10-percent">
                                <apex:outputPanel id="checkbox">
                                <div class=" slds-form-element slds-align--absolute-center">
                                    <label class="slds-checkbox">
                                        <apex:inputCheckbox styleClass="slds-input" value="{!r.selected}" disabled="{!(NOT(r.addedBySearch))&&(NOT(r.toBeDeleted))}"/>
                                        <span class="{!IF((r.toBeDeleted && (NOT(r.addedBySearch))),'deleting-checkbox slds-checkbox--faux','slds-checkbox--faux')}" ></span>             
                                        <span class="slds-form-element__label"></span>    
                                    </label>
                                </div>
                                </apex:outputPanel>
                            </td>
                            <td class="slds-truncate account-contact-col">
                                <a href="#" style="text-decoration:underline" onclick="openLink('{!r.account}');">
                                    <apex:outputPanel rendered="{!r.account != ''}">{!r.accountName}</apex:outputPanel>
                                </a>
                                <a href="#" style="text-decoration:underline" onclick="openLink('{!r.contact}');">
                                    <apex:outputPanel rendered="{!r.contact != ''}">{!r.contactName}</apex:outputPanel>
                                </a>
                            </td>
                            <td class="slds-truncate account-contact-col">
                                <apex:outputPanel rendered="{!r.account != ''}">{!r.accountStreet}</apex:outputPanel>
                                <apex:outputPanel rendered="{!r.contact != ''}">{!r.contactStreet}</apex:outputPanel>
                            </td>
                            <td class="slds-truncate account-contact-col">
                                <apex:outputPanel rendered="{!r.account != ''}">{!r.accountProvince}</apex:outputPanel>
                                <apex:outputPanel rendered="{!r.contact != ''}">{!r.contactProvince}</apex:outputPanel>
                            </td>
                            <td class="slds-truncate" style="party-relationship-col" id="partyCol">  
                                <apex:outputPanel id="partyAndRelationship">
                                    <!--Party Type-->     
                                    <div class="slds-form-element half-div">
                                        <div class="slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right">
                                            <apex:selectList styleClass="slds-input slds-combobox__input" rendered="{!r.isEditable}" value="{!r.partyType}" multiselect="false" size="1" > 
                                                <apex:selectOptions value="{!partyTypesList}" />
                                                <apex:actionSupport event="onchange" action="{!RequeryRelationshipTypeOptions}" reRender="reltypePanel"/>
                                            </apex:selectList>
                                            <apex:outputPanel rendered="{!NOT(r.isEditable)}">{!r.partyType}</apex:outputPanel> 
                                        </div>
                                    </div>
                                    <!--Relationship Type-->
                                    <apex:outputPanel id="reltypePanel">                                  
                                    <div class="slds-form-element half-div padding-left-10">
                                        <div class="slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right">
                                            <apex:selectList styleClass="slds-input slds-combobox__input" rendered="{!r.isEditable}" value="{!r.relationshipType}" multiselect="false" size="1"> 
                                                <apex:selectOptions value="{!r.relationshipTypeOptions}" />
                                            </apex:selectList>
                                            <apex:outputPanel rendered="{!NOT(r.isEditable)}">{!r.relationshipType}</apex:outputPanel> 
                                        </div>
                                    </div>
                                    </apex:outputPanel>                    
                                </apex:outputPanel>
                            </td>
                                <td class="action-col">  
                                    <div class="half-div">
                                        <apex:commandButton title="Edit" action="{!editToggle}" value="Edit" reRender="partyAndRelationship" image="{!URLFOR($Resource.SMEQ_UWRelationshipConsole, 'assets/img/edit.svg')}" styleClass="edit-btn">
                                            <apex:param assignTo="{!selectedBindParty.id}" name="editID"  value="{!r.id}"/> 
                                        </apex:commandButton>   
                                    </div>
                                    <div class="half-div">
                                        <apex:commandButton title="Delete" action="{!deleteToggle}" id="deleteButton" rendered="{!r.id!=null}" reRender="checkbox, partyAndRelationship" image="{!URLFOR($Resource.SMEQ_UWRelationshipConsole, 'assets/img/delete.svg')}" styleClass="delete-btn">
                                            <apex:param assignTo="{!deletedBindParty.id}" name="deleteID"  value="{!r.id}"/> 
                                        </apex:commandButton>   
                                    </div>
                                </td>
                            </tr>
                        </apex:repeat>
                  </tbody>
                </table>
            </div>             
            <div class="slds-form-element">
                <div class="slds-form-element__control footer-blue">
                    <div class="slds-p-horizontal--small slds-m-top--medium slds-size--1-of-1 save-btn">
                        <apex:commandButton action="{!saveQuoteRiskRelationships}" value="{!UPPER($Label.SaveBtn)}" id="saveButton" styleClass="slds-button slds-button--brand">
                        </apex:commandButton>
                    </div>                                                                                                                                 
                </div>
               </div>
               </apex:form>
               </div>
    </div>
</apex:page>