<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Fishing,Hunting, Trapping</label>
    <protected>false</protected>
    <values>
        <field>CSIO_Segment__c</field>
        <value xsi:type="xsd:string">FIS</value>
    </values>
    <values>
        <field>SF_Segment_c__c</field>
        <value xsi:type="xsd:string">Fishing,Hunting, Trapping</value>
    </values>
</CustomMetadata>
