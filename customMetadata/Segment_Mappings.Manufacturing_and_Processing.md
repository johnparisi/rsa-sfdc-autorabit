<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Manufacturing and Processing</label>
    <protected>false</protected>
    <values>
        <field>CSIO_Segment__c</field>
        <value xsi:type="xsd:string">MANF</value>
    </values>
    <values>
        <field>SF_Segment_c__c</field>
        <value xsi:type="xsd:string">Manufacturing and Processing</value>
    </values>
</CustomMetadata>
