<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>HubPacific</label>
    <protected>false</protected>
    <values>
        <field>Email__c</field>
        <value xsi:type="xsd:string">rsasme_hub@rsagroup.ca</value>
    </values>
    <values>
        <field>Offering__c</field>
        <value xsi:type="xsd:string">HUB</value>
    </values>
    <values>
        <field>Phone__c</field>
        <value xsi:type="xsd:string">(844) 249-3538</value>
    </values>
    <values>
        <field>Region__c</field>
        <value xsi:type="xsd:string">Pacific</value>
    </values>
</CustomMetadata>
