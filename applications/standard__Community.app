<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Idea</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>SMEQ_RESOURCE_BUNDLES__c</tabs>
    <tabs>QlikSenseSalesDashboard</tabs>
    <tabs>QlikSenseSalesDashboardQuebec</tabs>
    <tabs>QlikSensePortfolioDashboard</tabs>
    <tabs>Risk__c</tabs>
    <tabs>Coverages__c</tabs>
    <tabs>vlocity_ins__OmniScript__c</tabs>
    <tabs>ReferralTriggerRules__c</tabs>
</CustomApplication>
