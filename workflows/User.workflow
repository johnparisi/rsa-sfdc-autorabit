<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Ext_User_Id</fullName>
        <field>Ext_User__c</field>
        <formula>Alias</formula>
        <name>Ext User Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrUser_CopyEmailtoEmailExternalId</fullName>
        <description>Copies the user&apos;s email address to the email (external id) field to assist in simplifying data management workflows relating to the user.</description>
        <field>bkrUser_Email_ExternalId__c</field>
        <formula>Email</formula>
        <name>Copy Email to Email External ID Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>On Active User is Created or Edited</fullName>
        <actions>
            <name>bkrUser_CopyEmailtoEmailExternalId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Runs a workflow when any active user&apos;s record is created or edited.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update External User wth Alias</fullName>
        <actions>
            <name>Ext_User_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update External user with Alias</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
