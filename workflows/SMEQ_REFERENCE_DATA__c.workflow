<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SMEQ_REFERENCE_DATA_ID_UPDATE</fullName>
        <field>SMEQ_REFERENCE_DATA_ID__c</field>
        <formula>variant__c + type__c + language__c + code__c</formula>
        <name>SMEQ_REFERENCE_DATA_ID_UPDATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SMEQ REFERENCE DATA ID Populate</fullName>
        <actions>
            <name>SMEQ_REFERENCE_DATA_ID_UPDATE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate SMEQ REFERENCE DATA id when it&apos;s created/updated</description>
        <formula>NOT(ISBLANK(code__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
