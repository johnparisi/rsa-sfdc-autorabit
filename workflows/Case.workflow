<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Broker_Connect_Case_Closed</fullName>
        <description>Broker Connect - Case Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>brokerconnect@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrBrokerConnect/Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Broker_Connect_Follow_up_Survey</fullName>
        <description>Broker Connect - Follow-up Survey (NPS)</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>brokerconnect@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrBrokerConnect/Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Case_Assigned_to_an_Underwriter</fullName>
        <description>Case Assigned to an Underwriter</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrCommercialInsurance/bkrEN_CaseAssigned</template>
    </alerts>
    <alerts>
        <fullName>Case_Assigned_to_an_Underwriter_FR</fullName>
        <description>Case Assigned to an Underwriter FR</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrCommercialInsurance/bkrFR_CaseAssigned</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Email_Received_Notification</fullName>
        <description>New Case Email Received Notification (Used by CI-SME Merge Case flow)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrCommercialInsurance/bkrEN_NewCaseEmailReceivedNotification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Broker_Policy_Issued</fullName>
        <description>Notify Broker - Policy Issued</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrCommercialInsurance/bkrEN_PolicyIssued</template>
    </alerts>
    <alerts>
        <fullName>This_email_alert_will_be_sent_to_French_broker_when_UW_starts_working_on_their_c</fullName>
        <description>This email alert will be sent to French broker when UW starts working on their cases</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrCommercialInsurance/bkrFR_CaseAssigned</template>
    </alerts>
    <alerts>
        <fullName>This_email_alert_will_be_sent_to_broker_when_UW_starts_working_on_their_cases</fullName>
        <description>This email alert will be sent to broker when UW starts working on their cases</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNS_CI/bkrEN_CNS_CaseAssigned</template>
    </alerts>
    <alerts>
        <fullName>bkrCI_SendNetPromoterScoreSurveyEmail</fullName>
        <description>Send Net Promoter Score Survey Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrWorkflowDriven/bkrCI_NetPromoterScoreSurvey</template>
    </alerts>
    <alerts>
        <fullName>bkrMMGSL_ATL_SendConfirmationEmail</fullName>
        <description>Mid-Market &amp; GSL: Atlantic: Send Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>rsacommercial@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrMMGSL_EN/bkrMMGSL_EN_AutoReply</template>
    </alerts>
    <alerts>
        <fullName>bkrMMGSL_PAC_SendCaseReceivedNotification</fullName>
        <description>Send Case Received Notification to Broker (Pacific Region)</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>rsavancouver.underwriting@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrMMGSL_EN/bkrMMGSL_EN_AutoReply</template>
    </alerts>
    <alerts>
        <fullName>bkrMMGSL_PRA_SendCaseReceivedNotification</fullName>
        <description>Send Mid-Market &amp; GSL Case Received Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>prairiesci@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrMMGSL_EN/bkrMMGSL_EN_AutoReply</template>
    </alerts>
    <alerts>
        <fullName>bkrMMGSL_SendCaseCreatedNotificationOntGSL</fullName>
        <description>OntGSL@rsagroup.ca Case Received Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>ontgsl@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrMMGSL_EN/bkrMMGSL_EN_AutoReply</template>
    </alerts>
    <alerts>
        <fullName>bkrMMGSL_SendNotificationToBrokerOnReceipt</fullName>
        <description>Send Receipt Confirmation and Case Number to Broker</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrMMGSL_EN/bkrMMGSL_EN_AutoReply</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Assign_case_to_SME_Broker_UW_Queu</fullName>
        <field>OwnerId</field>
        <lookupValue>SME_Broker_UW</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case - Assign case to SME Broker UW Queu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Close_Reason_No_Action</fullName>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>No Action from Broker</literalValue>
        <name>Case - Close Reason: No Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Lock_Case_Status_After_Quoted</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case - Lock Case Status After Quoted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_COM_with_Policy</fullName>
        <field>COM__c</field>
        <formula>bkrCase_Policy__c</formula>
        <name>Case: Populate COM# with Policy #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Quote_Handover</fullName>
        <field>Quote_application_handed_over_to_UW__c</field>
        <literalValue>1</literalValue>
        <name>Case - Set Quote Handover</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_SPRNT_Offering</fullName>
        <field>Offering_Project__c</field>
        <literalValue>SPRNT v1</literalValue>
        <name>Case - Set SPRNT Offering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Case_RT_to_Open_SPRNT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CI_Open_SPRNT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case - Update Case RT to Open SPRNT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Case - Update Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_the_mid_market_referral_case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close the mid-market referral case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_the_mid_market_referral_case_1</fullName>
        <description>Close_the_mid_market_referral_case_1</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close the mid-market referral case 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assigned_to_UW_Date</fullName>
        <field>bkrCase_Assigned_to_UW_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Assigned to UW Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Call_Broker_Task_Reminder</fullName>
        <field>bkrCase_Call_Broker_Task_Reminder__c</field>
        <formula>NOW() + ((6 * 60) )/1440</formula>
        <name>Set Call Broker Task Reminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Set Case Status = Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RAG_to_AMBER</fullName>
        <description>Set RAG to AMBER</description>
        <field>bkrCase_RAG__c</field>
        <literalValue>Amber</literalValue>
        <name>Set RAG to AMBER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RAG_to_GREEN</fullName>
        <description>Set RAG to GREEN</description>
        <field>bkrCase_RAG__c</field>
        <literalValue>Green</literalValue>
        <name>Set RAG to GREEN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RAG_to_RED</fullName>
        <description>Set the RAG value to RED</description>
        <field>bkrCase_RAG__c</field>
        <literalValue>Red</literalValue>
        <name>Set RAG to RED</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RecordTypeId_to_Commercial</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CI_New_Business</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set RecordTypeId to Commercial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCase</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>UpdateCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_to_UW_Date</fullName>
        <description>This field update operation will update the Assigned to UW Date field to todays date.</description>
        <field>bkrCase_Assigned_to_UW_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Assigned to UW Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Num_Ext_Id</fullName>
        <field>Case_Number_Ext_Id__c</field>
        <formula>CaseNumber</formula>
        <name>Update Case Num Ext Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Record_Type_When_Case_Status</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CI_Closed_SPRNT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Case Record Type When Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Transaction_Type</fullName>
        <field>bkrCase_Subm_Type__c</field>
        <literalValue>New Business</literalValue>
        <name>Update Case Transaction Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_SPRNT_Case_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CI_Closed_SPRNT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Closed SPRNT Case RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Description_Field_on_Referral</fullName>
        <description>Update Case Description  to the value in the Referral Reason value automatically when a referral case is created in Salesforce</description>
        <field>Description</field>
        <formula>CASE(TEXT(Referral_Reason__c),

&quot;License Referral&quot;, $Label.CPQ_Referral_License_Referral,
&quot;Underwriting Strategy Referral&quot;, $Label.CPQ_Referral_Underwriting_Strategy_Referral,
&quot;Other&quot;, $Label.CPQ_Referral_Other,

TEXT(Referral_Reason__c))</formula>
        <name>Update Description Field on Referral</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offering_to_HUB</fullName>
        <field>Offering_Project__c</field>
        <literalValue>HUB</literalValue>
        <name>Update Offering to HUB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Approval_Date_when_refer</fullName>
        <description>Update Referral Approval Date when referral status changes</description>
        <field>Referral_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Referral Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Rejection_Date</fullName>
        <field>Referral_Rejection_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Referral Rejection Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_Field_on_Referral</fullName>
        <description>Update Case Subject = &quot;New Referral&quot; field value automatically when a referral case is created in Salesforce</description>
        <field>Subject</field>
        <formula>$Label.CPQ_Referral_New_Referral</formula>
        <name>Update Subject Field on Referral</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_for_Renewal_Exceptions</fullName>
        <description>Update the subject fields on the case for renewal exceptions for cases created via integrations.</description>
        <field>Subject</field>
        <formula>bkrCase_Policy__c + &apos;_&apos; +  TRIM( Insured_Name__c ) + &apos;_&apos; +  TEXT(Transaction_Effective_Date__c)</formula>
        <name>Update Subject for Renewal Exceptions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCI_Update_NPS_Survey_Sent_to_TRUE</fullName>
        <description>Updates the NPS Survey Sent field to TRUE, acting as a failsafe to ensure that the survey can only be sent once per case.</description>
        <field>bkrSurveySent_NPS__c</field>
        <literalValue>1</literalValue>
        <name>Update &quot;NPS Survey Sent&quot; to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrChangeOwnerToMMGSLQueue</fullName>
        <description>Changes the owner of the case from the system account to the Mid-Market &amp; GSL queue.</description>
        <field>OwnerId</field>
        <lookupValue>bkrMidMarketGSL</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to Mid-Market &amp; GSL Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_Case_ChangeRegionToQuebec</fullName>
        <description>Changes the Case Region to Quebec</description>
        <field>bkrCase_Region__c</field>
        <literalValue>Quebec</literalValue>
        <name>Change Region to Quebec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_Case_ChangeTypeToOtherProcess</fullName>
        <description>Changes the Case Type field to &quot;Other Processing or Amendment Request&quot;</description>
        <field>Type</field>
        <literalValue>Other Processing or Policy Amendment</literalValue>
        <name>Change Case Type to &quot;Other Processing...</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseOriginToUWMailbox</fullName>
        <description>Changes the Case origin to reflect that the case originated from uw@rsagroup.ca</description>
        <field>Origin</field>
        <literalValue>Mid-Market &amp; GSL: uw@rsagroup.ca</literalValue>
        <name>Change Case Origin to uw@rsagroup.ca</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseRegionToOntario</fullName>
        <field>bkrCase_Region__c</field>
        <literalValue>Ontario</literalValue>
        <name>Change Region to Ontario</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseRegionToPacific</fullName>
        <field>bkrCase_Region__c</field>
        <literalValue>Pacific</literalValue>
        <name>Change Case Region to Pacific</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseRegionToPrairies</fullName>
        <field>bkrCase_Region__c</field>
        <literalValue>Prairies</literalValue>
        <name>Change Case Region to Prairies</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseStatusToInProgress</fullName>
        <description>Updates the case status to &quot;In Progress&quot;</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Change Case Status to &quot;In Progress&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseStatusToNew</fullName>
        <description>Changes the Case status to New</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Change Case Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeCaseTypeToBlank</fullName>
        <description>Changes the Case Type to Blank</description>
        <field>Type</field>
        <name>Change Case Type to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_ChangeRecordTypeToMMGSL</fullName>
        <description>Changes the Case Record Type to Mid-Market &amp; GSL</description>
        <field>RecordTypeId</field>
        <lookupValue>bkrMidMarketGSL</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Mid-Market &amp; GSL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_Change_the_Record_Type_to_MMGSL</fullName>
        <description>Changes the Case&apos;s record type to &quot;Mid-Market &amp; GSL&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>bkrMidMarketGSL</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>MMGSL: Change the Record Type to MMGSL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrMMGSL_UpdateRegionToAtlantic</fullName>
        <field>bkrCase_Region__c</field>
        <literalValue>Atlantic</literalValue>
        <name>Update Region to Atlantic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CI%3A On Bind%2FNew Business Status %3D Closed</fullName>
        <actions>
            <name>bkrCI_SendNetPromoterScoreSurveyEmail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>bkrCI_Update_NPS_Survey_Sent_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CI Portfolio,CI Processing,CI Retention,CI New Business,CI Triage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrSurveySent_NPS__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Runs a workflow when a case with a transaction type of &quot;Bind&quot; or &quot;New Business&quot; is set to &quot;Closed&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CI%3A On Case Assigned to UW</fullName>
        <actions>
            <name>Set_Assigned_to_UW_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RecordTypeId_to_Commercial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a workflow to send a Broker a notification email</description>
        <formula>AND(NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)), ISPICKVAL( bkrCase_Subm_Type__c, &apos;New Business&apos; ), ISCHANGED( OwnerId ), LEFT( OwnerId, 3) = &apos;005&apos;, ISNULL( bkrCase_Assigned_to_UW_Date__c ), NOT(CONTAINS( RecordType.DeveloperName ,&quot;SPRNT&quot;)), NOT(CONTAINS( RecordType.DeveloperName ,&quot;PI&quot;)), ISPICKVAL(Contact.bkrContact_PreferredLanguage__c, &quot;English&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CI%3A On Case Assigned to UW - SPRNT</fullName>
        <actions>
            <name>Set_Assigned_to_UW_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a workflow to send a Broker a notification email for SPRNT cases</description>
        <formula>AND(           NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),           ISPICKVAL(bkrCase_Subm_Type__c, &apos;New Business&apos;),           LEFT(OwnerId, 3) = &apos;005&apos;,           ISNULL(bkrCase_Assigned_to_UW_Date__c),           CONTAINS(RecordType.DeveloperName ,&quot;SPRNT&quot;),           OR(             AND(               SIC_Code_Referral_Level__c!=&apos;DECLINE&apos;,               VALUE(RIGHT(UW_Referral_Level__c,1))&gt;= VALUE(RIGHT(SIC_Code_Referral_Level__c,1))             ),             TRUE           ),           ISPICKVAL( Contact.bkrContact_PreferredLanguage__c  , &apos;English&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CI%3A On Case Assigned to UW FR</fullName>
        <actions>
            <name>Set_Assigned_to_UW_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RecordTypeId_to_Commercial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a workflow to send a Broker a notification email in French</description>
        <formula>AND(           NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),           ISPICKVAL( bkrCase_Subm_Type__c, &apos;New Business&apos; ),           ISCHANGED( OwnerId ),           LEFT( OwnerId, 3) = &apos;005&apos;,           ISNULL( bkrCase_Assigned_to_UW_Date__c ),           NOT(CONTAINS( RecordType.DeveloperName ,&quot;SPRNT&quot;)),           NOT(CONTAINS( RecordType.DeveloperName ,&quot;PI&quot;)),           ISPICKVAL(Contact.bkrContact_PreferredLanguage__c, &quot;French&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CI%3A On Case Assigned to UW FR - SPRNT</fullName>
        <actions>
            <name>Set_Assigned_to_UW_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a workflow to send a Broker a notification email in French for SPRNT cases</description>
        <formula>AND(           NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),           ISPICKVAL(bkrCase_Subm_Type__c, &apos;New Business&apos;),           LEFT(OwnerId, 3) = &apos;005&apos;,           ISNULL(bkrCase_Assigned_to_UW_Date__c),           CONTAINS(RecordType.DeveloperName ,&quot;SPRNT&quot;),           OR(             AND(               SIC_Code_Referral_Level__c!=&apos;DECLINE&apos;,               VALUE(RIGHT(UW_Referral_Level__c,1))&gt;=VALUE(RIGHT(SIC_Code_Referral_Level__c,1))             ),             TRUE           ),           ISPICKVAL( Contact.bkrContact_PreferredLanguage__c ,&quot;French&quot;)           )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CI%3A On Case Updated Quote Issued</fullName>
        <actions>
            <name>Set_Call_Broker_Task_Reminder</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a workflow to set Call Broker Task Reminder datetime field on Case.</description>
        <formula>OR(        AND(              ISPICKVAL(Status,&apos;Quoted&apos;),              ISCHANGED(Status)        ),     AND(         NOT ISNULL( bkrCase_Quote_Date__c ),         ISCHANGED(bkrCase_Quote_Date__c )        )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Referral Approval Date and close the case when referral status changes to Approved</fullName>
        <actions>
            <name>Update_Referral_Approval_Date_when_refer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Referral</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Update Referral Approval Date and close the case when referral status changes to Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_the_mid_market_referral_case_1</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Referral_Approval_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Capture Referral Rejection Date and close the case when referral status changes to Rejected</fullName>
        <actions>
            <name>Update_Referral_Rejection_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Referral</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Capture Referral Rejection Date and close the case when referral status changes to Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_the_mid_market_referral_case</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Referral_Rejection_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Assign to CI Open SPRNT RT to Current User</fullName>
        <actions>
            <name>Case_Update_Case_RT_to_Open_SPRNT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),  NOT(CONTAINS( $Profile.Name ,&quot;Integration&quot;)), RecordType.DeveloperName =&quot;CI_New_Business&quot;|| RecordType.DeveloperName =&quot;CI_Triage&quot;,  ISPICKVAL(bkrCase_Subm_Type__c , &quot;Broker Referral&quot;)||ISPICKVAL( bkrCase_Subm_Type__c , &quot;New Business&quot;), NOT(ISBLANK(TEXT(Offering_Project__c))), NOT(CONTAINS(SIC_Code_Referral_Level__c ,&quot;DECLINE&quot;)), VALUE(RIGHT( SIC_Code_Referral_Level__c ,1))&lt;= VALUE(RIGHT(UW_Referral_Level__c,1)) )   ||   AND( NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)), ISPICKVAL( bkrCase_Subm_Type__c , &quot;Simple Amendment&quot;)||ISPICKVAL( bkrCase_Subm_Type__c , &quot;Complex Amendment&quot;)||ISPICKVAL( bkrCase_Subm_Type__c , &quot;Renewal&quot;),NOT(ISBLANK(TEXT(Offering_Project__c))), NOT(ISPICKVAL($User.uwUser_Level__c, &quot;&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Assign to SME Quote Queue</fullName>
        <actions>
            <name>Case_Assign_case_to_SME_Broker_UW_Queu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Case_RT_to_Open_SPRNT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(             ISPICKVAL(bkrCase_Subm_Type__c , &quot;Broker Referral&quot;) || ISPICKVAL( bkrCase_Subm_Type__c , &quot;New Business&quot;),             ISPICKVAL(Status , &quot;New&quot;)||ISPICKVAL(Status , &quot;In Progress&quot;),             RecordType.DeveloperName =&quot;CI_Open_SPRNT&quot;||  RecordType.DeveloperName =&quot;CI_New_Business&quot;|| RecordType.DeveloperName =&quot;CI_Triage&quot;,             NOT(CONTAINS(SIC_Code_Referral_Level__c ,&quot;DECLINE&quot;)),             NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),             OR(             VALUE(RIGHT(SIC_Code_Referral_Level__c ,1))&gt; VALUE(RIGHT(UW_Referral_Level__c,1)),             Case_Origin_Compatible__c =FALSE             ),             $Profile.Name  &lt;&gt; &quot;ESB Integration Profile&quot;             )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Close case after Quoted for duration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Quoted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>New Business,Broker Referral</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CI Open SPRNT,CI Closed SPRNT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Close_Reason_No_Action</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_Lock_Case_Status_After_Quoted</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Lock Case Record</fullName>
        <actions>
            <name>Update_Closed_SPRNT_Case_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed,Quoted,Bound,Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CI Open SPRNT</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - PI UW %3A Assigned</fullName>
        <actions>
            <name>Update_Assigned_to_UW_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When case ownership changes to a user (from a queue or another user), reset the Assigned to UW Date.</description>
        <formula>AND(  ISCHANGED(OwnerId),  LEFT(OwnerId,3) = &apos;005&apos;, OR( 				RecordType.DeveloperName =&quot;PI_Underwriting&quot;, 				RecordType.DeveloperName =&quot;PI_Pacific_Underwriting&quot;, 				RecordType.DeveloperName =&quot;PI_FA_Underwriting&quot; 				) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - PI UW %3A Set RAG to AMBER</fullName>
        <actions>
            <name>Set_RAG_to_AMBER</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PI Underwriting,PI Pacific Underwriting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>inspections</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>AMBER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>RED</value>
        </criteriaItems>
        <description>Set RAG to AMBER for PI inspections</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - PI UW %3A Set RAG to GREEN</fullName>
        <actions>
            <name>Set_RAG_to_GREEN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PI Underwriting,PI Pacific Underwriting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>inspections</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>GREEN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>RED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>AMBER</value>
        </criteriaItems>
        <description>Set RAG to GREEN for PI inspections</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - PI UW %3A Set RAG to RED</fullName>
        <actions>
            <name>Set_RAG_to_RED</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PI Underwriting,PI Pacific Underwriting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>inspections</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>RED</value>
        </criteriaItems>
        <description>Set RAG to RED for PI inspections</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - SIC Code and UW Level</fullName>
        <actions>
            <name>Case_Assign_case_to_SME_Broker_UW_Queu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)), CONTAINS(RecordType.DeveloperName,&apos;SPRNT&apos;), NOT(CONTAINS(SIC_Code_Referral_Level__c ,&quot;DECLINE&quot;)), VALUE(RIGHT(SIC_Code_Referral_Level__c ,1))&gt; VALUE(RIGHT(UW_Referral_Level__c,1)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - SPRNT Cases not created by Broker</fullName>
        <actions>
            <name>Case_Set_Quote_Handover</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Set_SPRNT_Offering</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( NOT(CONTAINS(Owner:User.Profile.Name,&quot;Broker&quot;)), ISPICKVAL(CreatedBy.UserType,&quot;Standard&quot;), Owner:Queue.QueueName = &quot;SME_Broker_UW&quot;), Quote_application_handed_over_to_UW__c = FALSE,  NOT(CONTAINS(TEXT(Origin),&quot;HUB&quot;)), CONTAINS(RecordType.DeveloperName,&apos;SPRNT&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Offering to HUB</fullName>
        <actions>
            <name>Case_Set_Quote_Handover</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Transaction_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Offering_to_HUB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToOntario</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND NOT 5</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>HUB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Offering_Project__c</field>
            <operation>contains</operation>
            <value>SPRNT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Offering_Project__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CI Open SPRNT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>contains</operation>
            <value>Complex Amendment,Renewal,Renewal Re-visit,Simple Amendment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Update Offering to HUB - R%26A</fullName>
        <actions>
            <name>Case_Set_Quote_Handover</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Offering_to_HUB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToOntario</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>HUB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Offering_Project__c</field>
            <operation>contains</operation>
            <value>SPRNT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Offering_Project__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>Complex Amendment,Renewal,Renewal Re-visit,Simple Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CI Open SPRNT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update COM%23 with Policy %23</fullName>
        <actions>
            <name>Case_Populate_COM_with_Policy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(bkrCase_Policy__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MMGSL%3A On Case Assigned to Ontario_MM_GSL_Triage</fullName>
        <actions>
            <name>bkrChangeOwnerToMMGSLQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseOriginToUWMailbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToOntario</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseStatusToNew</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseTypeToBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeRecordTypeToMMGSL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Ontario MM/GSL Triage</value>
        </criteriaItems>
        <description>Runs a workflow when a case is assigned to the depreciated Ontario_MM_GSL_Triage queue.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MMGSL%3A On Origin Contains PrairiesCI%40rsagroup%2Eca</fullName>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToPrairies</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: PrairiesCI@rsagroup.ca</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A All Cases%3A Case Assigned to %22Mid-Market %26 GSL%22 Queue</fullName>
        <actions>
            <name>bkrMMGSL_Change_the_Record_Type_to_MMGSL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL</value>
        </criteriaItems>
        <description>Runs a workflow whenever a case is assigned to the queue &quot;Mid-Market &amp; GSL&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A Atlantic Region %28All%29</fullName>
        <actions>
            <name>bkrChangeOwnerToMMGSLQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_Change_the_Record_Type_to_MMGSL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_UpdateRegionToAtlantic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Atlantic Region</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A Atlantic Region %28External%29</fullName>
        <actions>
            <name>bkrMMGSL_ATL_SendConfirmationEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Atlantic Region</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A New Case%3A All Regions</fullName>
        <actions>
            <name>bkrChangeOwnerToMMGSLQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>,Phone,Web,Email</value>
        </criteriaItems>
        <description>Runs a workflow when any new case is created within the Mid-Market &amp; GSL record type.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A New Case%3A Pacific Region</fullName>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToPacific</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Pacific Region</value>
        </criteriaItems>
        <description>Runs a workflow when the case origin is &quot;Mid-Market &amp; GSL: Pacific Region&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A New Case%3A Pacific Region%3A External</fullName>
        <actions>
            <name>bkrMMGSL_PAC_SendCaseReceivedNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Pacific Region</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <description>Runs a workflow when an external trigger (i.e. an email from a broker) creates a new case.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A New Case%3A Prairies Region</fullName>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToPrairies</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) AND (5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: PrairiesCI@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>%Out Of Office%</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>%Automatic Reply%</value>
        </criteriaItems>
        <description>Runs a workflow when the case origin is &quot;Mid-Market &amp; GSL: PrairiesCI@rsagroup.ca&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A On New Underwriting Request</fullName>
        <actions>
            <name>bkrChangeOwnerToMMGSLQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeRecordTypeToMMGSL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Underwriter Request</value>
        </criteriaItems>
        <description>Runs a workflow when the Case Origin equals &quot;Mid-Market &amp; GSL: Underwriting Request&quot; (these requests are created by a quick action on the Opportunity screen).</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A Ontario Region</fullName>
        <actions>
            <name>bkrMMGSL_SendCaseCreatedNotificationOntGSL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeCaseRegionToOntario</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: OntGSL@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <description>Runs a workflow when the case origin is &quot;Mid-Market &amp; GSL: Ontario Region&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A Quebec Region %28All%29</fullName>
        <actions>
            <name>bkrChangeOwnerToMMGSLQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_Case_ChangeRegionToQuebec</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrMMGSL_ChangeRecordTypeToMMGSL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Quebec Region (MTL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Quebec Region (QUE)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Quebec Region (MTL-A)</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid-Market %26 GSL%3A Quebec Region %28MTL-A%29</fullName>
        <actions>
            <name>bkrMMGSL_Case_ChangeTypeToOtherProcess</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mid-Market &amp; GSL: Quebec Region (MTL-A)</value>
        </criteriaItems>
        <description>Runs a specific workflow for amendments received by the e-mail address: attestations-avenants@rsagroup.ca</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>On %22Broker Connect%22 Case Status equals %22Closed%22</fullName>
        <actions>
            <name>Broker_Connect_Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Broker Connect - Inbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notContain</operation>
            <value>@rsagroup.ca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.bkrContact_NPSLastUpdate__c</field>
            <operation>lessThan</operation>
            <value>LAST 30 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.bkrContact_NPSLastUpdate__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Runs a workflow when a Broker Connect case status changes to closed, and triggers the deployment of the net promoter score survey only if the customer has not been surveyed in the past 30 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Subject For Renewal Exceptions</fullName>
        <actions>
            <name>Update_Subject_for_Renewal_Exceptions</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>HUON,ePolicy,Sigma</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the subject on the case for renewal exception records created via integration</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Number Ext Id</fullName>
        <actions>
            <name>Update_Case_Num_Ext_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Record Type When Case Status is closed</fullName>
        <actions>
            <name>Update_Case_Record_Type_When_Case_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Bound</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CI Open SPRNT</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Subject and description fields for Referral Cases</fullName>
        <actions>
            <name>Update_Description_Field_on_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subject_Field_on_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When &quot;Mid-Market &amp; GSL: Referral&quot; case is created, update Subject and description fields on the case.</description>
        <formula>RecordType.DeveloperName  = &apos;Mid_Market_GSL_Referral&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
