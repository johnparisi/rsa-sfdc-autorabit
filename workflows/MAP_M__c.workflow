<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Id_on_Map_M</fullName>
        <description>Set the External Id field &apos;MAP_M_UNIQUE_ID__c&apos; when the record is being created or updated.</description>
        <field>MAP_M_UNIQUE_ID__c</field>
        <formula>TEXT(SUBCOY__c)+TEXT(BRANCH__c)</formula>
        <name>Update Unique Id on Map_M</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Unique Id for Map_M</fullName>
        <actions>
            <name>Update_Unique_Id_on_Map_M</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MAP_M__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to set the External Id field on the MAP_M__c object.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
