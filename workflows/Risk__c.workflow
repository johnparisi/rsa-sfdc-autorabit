<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Rental_Revenue</fullName>
        <field>Annual_Rental_Revenue__c</field>
        <formula>1</formula>
        <name>Update Rental Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Risk - Rental Revenue Default</fullName>
        <actions>
            <name>Update_Rental_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( CONTAINS(TEXT( Does_the_company_collect_rental_revenue__c ),&quot;No&quot;), CONTAINS(TEXT( Quote__r.Case__r.Offering_Project__c),&quot;HUB&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
