<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Federation_Id</fullName>
        <description>Updates the related quote&apos;s Federation Id to the latest policy history&apos;s Federation Id.</description>
        <field>Last_Federation_Id__c</field>
        <formula>Federation_ID__c</formula>
        <name>Update Federation Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Related_Quote__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Policy History - Updated Related Quote EP Reference Number</fullName>
        <actions>
            <name>Update_Federation_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow evaluates whenever a new policy history record is created. It will update the related quote&apos;s last FederationId number to the policy history&apos;s FederationId of the last transaction.</description>
        <formula>Federation_ID__c  &lt;&gt; Related_Quote__r.Last_Federation_Id__c</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
