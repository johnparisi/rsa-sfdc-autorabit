<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Query_Filter_Value_Field_Update</fullName>
        <field>Query_Filter_Value__c</field>
        <formula>LEFT( Name + &apos;_&apos; +  TEXT(Type__c)  , 255)</formula>
        <name>Query Filter Value Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Query Filter Value</fullName>
        <actions>
            <name>Query_Filter_Value_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
