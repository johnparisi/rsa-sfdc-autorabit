<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Ex_Gratia_Approved</fullName>
        <description>Ex Gratia - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrExGratia/Ex_Gratia_Approved</template>
    </alerts>
    <alerts>
        <fullName>Ex_Gratia_Rejected</fullName>
        <description>Ex Gratia - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrExGratia/Ex_Gratia_Declined</template>
    </alerts>
</Workflow>
