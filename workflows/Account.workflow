<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>E_mail_on_Account_Flag</fullName>
        <description>E-mail on Account Flag</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrWorkflowDriven/bkrAccountFlagisAdded</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Account_Record_Type_to_MG_or_ML</fullName>
        <field>RecordTypeId</field>
        <lookupValue>bkrAccount_RecordTypeBkrMGML</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Account Record Type to &quot;MG or ML&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Record_Type_to_MM_Level</fullName>
        <field>RecordTypeId</field>
        <lookupValue>bkrAccount_RecordTypeBkrMM</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Account Record Type to &quot;MM-Level&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Site_to_Prospect</fullName>
        <field>Site</field>
        <formula>&quot;Prospect&quot;</formula>
        <name>Change Account Site to &quot;Prospect&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Site_to_Prospective</fullName>
        <field>Site</field>
        <formula>&quot;Prospective Broker&quot;</formula>
        <name>Change Account Site to &quot;Prospective...&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Site_to_Supplier</fullName>
        <field>Site</field>
        <formula>&quot;Supplier or Vendor&quot;</formula>
        <name>Change Account Site to &quot;Supplier...&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Account_Status_to_Active</fullName>
        <description>Changes the Account Status to Active.</description>
        <field>bkrAccount_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Change Account Status to &quot;Active&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Billing_Country_to_Canada</fullName>
        <field>BillingCountry</field>
        <formula>&quot;Canada&quot;</formula>
        <name>Change the Billing Country to Canada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_the_Broker_to_the_Account</fullName>
        <description>Updates the account number to match the HUON broker number.</description>
        <field>AccountNumber</field>
        <formula>bkrAccount_HuonBrokerNumber__c</formula>
        <name>Copy the Broker # to the Account #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ultimate_Parent_Brokerage_Account_Site</fullName>
        <description>Set Broker Account Site for Ultimate Parent Brokerage. Move value over from hidden auto generated field Auto_Generated_Broker_Number__c and append &quot; (BRKG)&quot;</description>
        <field>Site</field>
        <formula>Auto_Generated_Broker_Number__c &amp; &quot; (BRKG)&quot;</formula>
        <name>Ultimate Parent Brokerage Account Site</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ultimate_Parent_Brokerage_Broker_Number</fullName>
        <description>Set Broker Number for Ultimate Parent Brokerage. Move value over from hidden auto generated field Auto_Generated_Broker_Number__c</description>
        <field>bkrAccount_HuonBrokerNumber__c</field>
        <formula>Auto_Generated_Broker_Number__c</formula>
        <name>Ultimate Parent Brokerage Broker Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Executive_Briefing_Last_Updated</fullName>
        <field>bkrAccount_ExecBriefLastUpdate__c</field>
        <formula>TODAY()</formula>
        <name>Update Executive Briefing Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Description_Last_Updated_Date</fullName>
        <field>bkrAccount_DescriptionLastUpdate__c</field>
        <formula>TODAY()</formula>
        <name>Update the Description Last Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrAccount_ActivateMarketingEmails</fullName>
        <description>Sets the marketing e-mails field to &quot;TRUE&quot;</description>
        <field>bkrAccount_MarketingEmails__c</field>
        <literalValue>1</literalValue>
        <name>Activate Marketing E-mails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrAccount_SitetoCompetitor</fullName>
        <field>Site</field>
        <formula>&quot;Competitor&quot;</formula>
        <name>Change Site to &quot;Competitor&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrAccount_UpdatePolicyholderSiteField</fullName>
        <description>Updates the site field to read: &quot;Client Account/Compte client&quot;</description>
        <field>Site</field>
        <formula>&quot;Client Account/Compte client&quot;</formula>
        <name>Update the Policyholder Site Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>All Lines%3A On Record Type equals Policyholder</fullName>
        <actions>
            <name>bkrAccount_UpdatePolicyholderSiteField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Policyholder</value>
        </criteriaItems>
        <description>Runs a workflow when the account record type is equal to &quot;Policyholder&quot; (or client account).</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On %22Account Flag%22 has a Follow-up Date</fullName>
        <actions>
            <name>Follow_up_Required</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.bkrAccount_FlagFollowup__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Detects when a brokerage Account has a follow-up date set.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account Aged by 60 Days</fullName>
        <active>false</active>
        <description>Detects when an account&apos;s last modified date exceeds 90 days.</description>
        <formula>LastModifiedDate &lt; NOW()-60</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On Account Billing Country Equals %22%22</fullName>
        <actions>
            <name>Change_the_Billing_Country_to_Canada</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Changes the &quot;Country&quot; field to &quot;Canada&quot; if an account is created without a country.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account Flag Is Not Blank</fullName>
        <actions>
            <name>E_mail_on_Account_Flag</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Detects when a brokerage Account has been flagged.</description>
        <formula>ISCHANGED( bkrAccount_Flag__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account Record Type Equals %22Competitor%22</fullName>
        <actions>
            <name>bkrAccount_SitetoCompetitor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Competitor</value>
        </criteriaItems>
        <description>Detects when a &quot;Competitor&quot; account has been created and changes the &quot;Site&quot; field to read &quot;Competitor&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account Record Type equals %22Prospect%22</fullName>
        <actions>
            <name>Change_Account_Site_to_Prospective</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <description>Detects when a &quot;Prospect&quot; account has been created and changes the &quot;Site&quot; field to read &quot;Prospective Brokerage&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account Record Type equals %22Supplier or Vendor%22</fullName>
        <actions>
            <name>Change_Account_Site_to_Supplier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supplier or Vendor</value>
        </criteriaItems>
        <description>Detects when a &quot;Supplier or Vendor&quot; account has been created and changes the &quot;Site&quot; field to read &quot;Supplier or Vendor&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account Site Contains %22%28MG%29%22 or %22%28ML%29%22</fullName>
        <actions>
            <name>Change_Account_Record_Type_to_MG_or_ML</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Site</field>
            <operation>contains</operation>
            <value>(MG),(ML),(HUON)</value>
        </criteriaItems>
        <description>Assigns the appropriate account record type based on data stored in the &quot;Site&quot; field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account contains %22Broker Number%22</fullName>
        <actions>
            <name>Copy_the_Broker_to_the_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.bkrAccount_HuonBrokerNumber__c</field>
            <operation>notEqual</operation>
            <value></value>
        </criteriaItems>
        <description>Runs a workflow when the account contains any value in the broker number field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Account is Created</fullName>
        <actions>
            <name>Change_Account_Status_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrAccount_ActivateMarketingEmails</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Ultimate Parent Brokerage</value>
        </criteriaItems>
        <description>Detects when a new account is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>On Site Contains %22%28MM%29%22</fullName>
        <actions>
            <name>Change_Account_Record_Type_to_MM_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Site</field>
            <operation>contains</operation>
            <value>(MM)</value>
        </criteriaItems>
        <description>Assigns the appropriate account record type based on data stored in the &quot;Site&quot; field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Update to Account Description</fullName>
        <actions>
            <name>Update_the_Description_Last_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Description )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Update to Executive Briefing</fullName>
        <actions>
            <name>Update_Executive_Briefing_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( bkrAccount_ExecutiveBriefing__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ultimate Parent Brokerage</fullName>
        <actions>
            <name>Ultimate_Parent_Brokerage_Account_Site</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Ultimate_Parent_Brokerage_Broker_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ultimate Parent Brokerage</value>
        </criteriaItems>
        <description>Set Broker Number for Ultimate Parent Brokerage. Move values over from hidden auto generated field Auto_Generated_Broker_Number__c</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Follow_up_Required</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a reminder that an account you manage has been flagged for a follow-up on this date.  Please review and update the account as required, and reset or remove the follow-up date if it is no longer required.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.bkrAccount_FlagFollowup__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Follow-up Required</subject>
    </tasks>
</Workflow>
