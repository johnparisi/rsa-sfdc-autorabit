<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Duplicate_Record_Set_Created</fullName>
        <ccEmails>brokerconnect@rsagroup.ca.invalid</ccEmails>
        <description>New Duplicate Record Set Created</description>
        <protected>false</protected>
        <senderAddress>brokerconnect@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrWorkflowDriven/Duplicate_Record_Set</template>
    </alerts>
    <rules>
        <fullName>On New Duplicate Record Set</fullName>
        <actions>
            <name>New_Duplicate_Record_Set_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>DuplicateRecordSet.Name</field>
            <operation>notEqual</operation>
            <value></value>
        </criteriaItems>
        <description>Runs a workflow when a new duplicate record set is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
