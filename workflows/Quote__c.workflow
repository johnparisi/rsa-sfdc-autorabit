<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Bind_Details_to_Quebec_Triage</fullName>
        <ccEmails>entrerprises.montreal@rsagroup.ca.invalid</ccEmails>
        <description>Send Bind Details to Quebec Triage</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>carole.desmarais@rsagroup.ca</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>uw@rsagroup.ca</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>bkrCommercialInsurance/Send_Bind_Details_FR</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Populate_Bound_Premium</fullName>
        <field>bkrCase_Bound_Premium__c</field>
        <formula>IF(ISPICKVAL(Status__c,&quot;Finalized&quot;)|| ISPICKVAL(Status__c,&quot;Bind&quot;)|| ISPICKVAL(Status__c,&quot;Policy Issued – Closed&quot;), Premium__c,NULL
)</formula>
        <name>Case - Populate Bound Premium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Policy</fullName>
        <field>bkrCase_Policy__c</field>
        <formula>ePolicy__c</formula>
        <name>Case - Populate Policy #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Policy_Issued_Date</fullName>
        <field>bkrCase_Pol_Issued_Dt__c</field>
        <formula>Issued_Date__c</formula>
        <name>Case - Populate Policy Issued Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Quote_Version</fullName>
        <field>bkrCase_Quote_Version__c</field>
        <formula>Quote_Number__c</formula>
        <name>Case - Populate Quote Version</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Quoted_Date</fullName>
        <field>bkrCase_Quoted_Dt__c</field>
        <formula>Quote_Date__c</formula>
        <name>Case - Populate Quoted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Quoted_Premium</fullName>
        <field>bkrCase_Quoted_Premium__c</field>
        <formula>Premium__c</formula>
        <name>Case - Populate Quoted Premium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Policy_Populate_Expiry_Date</fullName>
        <field>Quote_Expiry_Date__c</field>
        <formula>IF(ISPICKVAL(Case__r.bkrCase_Subm_Type__c,&quot;Renewal&quot;) || (ISPICKVAL(Case__r.bkrCase_Subm_Type__c,&quot;Renewal Re-visit&quot;)), Today()+365, 
NULL)</formula>
        <name>Policy - Populate Expiry Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Policy_Populate_Finalized_Date</fullName>
        <field>Finalized_Date__c</field>
        <formula>IF(ISPICKVAL(Status__c,&quot;Finalized&quot;), Today(),
NULL)</formula>
        <name>Policy - Populate Finalized Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Qt_Policy_Sync_Case_Reason_with_Qt_Amen</fullName>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>Amendment Issued</literalValue>
        <name>Qt/Policy: Sync Case Reason with Qt Amen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Qt_Policy_Sync_Case_Reason_with_Qt_Ren</fullName>
        <description>Syncing Case Close Reason when Renewal transaction is completed</description>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>Renewal Issued</literalValue>
        <name>Qt/Policy: Sync Case Reason with Qt Ren</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Qt_Sync_Case_Owner_with_Qt_Bind_Central</fullName>
        <field>OwnerId</field>
        <lookupValue>Central_Processing_Noida</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Qt -Sync Case Owner with Qt Bind Central</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Qt_Sync_Case_Owner_with_Qt_Bind_Quebec</fullName>
        <field>OwnerId</field>
        <lookupValue>Quebec_Processing</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Qt - Sync Case Owner with Qt Bind Quebec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Case_Policy_Issued_Date</fullName>
        <field>bkrCase_Pol_Issued_Dt__c</field>
        <formula>TODAY()</formula>
        <name>Quote/Case- Policy Issued Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Quote - Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Policy_Issued_Date</fullName>
        <field>Issued_Date__c</field>
        <formula>TODAY()</formula>
        <name>Quote - Policy Issued Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Policy_Update_Closed_Policy_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Policy</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quote/Policy: Update Closed Policy RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Populate_Expiry_Date</fullName>
        <field>Quote_Expiry_Date__c</field>
        <formula>IF(ISPICKVAL(Status__c,&quot;Finalized&quot;), Today()+90,
IF(CONTAINS(TEXT(Status__c),&quot;Bind&quot;), PRIORVALUE( Quote_Expiry_Date__c ), NULL))</formula>
        <name>Quote - Populate Expiry Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Populate_Finalized_Date</fullName>
        <field>Finalized_Date__c</field>
        <formula>IF(ISPICKVAL(Status__c,&quot;Finalized&quot;), Today(),
IF(CONTAINS(TEXT(Status__c),&quot;Bind&quot;), PRIORVALUE(Finalized_Date__c), NULL))</formula>
        <name>Quote - Populate Finalized Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Owner_with_Quote_Bind</fullName>
        <field>OwnerId</field>
        <lookupValue>Pacific_Processing</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Quote - Sync Case Owner with Quote Bind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Reason_with_Quote_Abor</fullName>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>Unsuccessful – not accepted by Broker</literalValue>
        <name>Quote - Sync Case Reason with Quote Abor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Reason_with_Quote_Clos</fullName>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>Completed</literalValue>
        <name>Quote - Sync Case Reason with Quote Clos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Reason_with_Quote_Decl</fullName>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>Declined</literalValue>
        <name>Quote - Sync Case Reason with Quote Decl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Status_Comments</fullName>
        <field>bkrCase_Subm_Stat_Rsn__c</field>
        <formula>Status_Comments__c</formula>
        <name>Quote - Sync Case Status Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Status_with_Quote</fullName>
        <field>Status</field>
        <literalValue>Quoted</literalValue>
        <name>Quote - Sync Case Status with Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Status_with_Quote_Abor</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Quote - Sync Case Status with Quote Abor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Status_with_Quote_Bind</fullName>
        <field>Status</field>
        <literalValue>Bound</literalValue>
        <name>Quote - Sync Case Status with Quote Bind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Status_with_Quote_Clos</fullName>
        <field>Status</field>
        <literalValue>Policy Issued</literalValue>
        <name>Quote - Sync Case Status with Quote Clos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Sync_Case_Status_with_Quote_Decl</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Quote - Sync Case Status with Quote Decl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Update_Closed_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quote - Update Closed RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Update_Premium_to_Premium_Selected</fullName>
        <field>Premium__c</field>
        <formula>Premium_Selected__c</formula>
        <name>Quote-Update Premium to Premium Selected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Update_Total_Revenue</fullName>
        <field>Total_Revenue__c</field>
        <formula>NULLVALUE(Canadian_Revenue__c, 0) +
NULLVALUE(US_Revenue__c, 0) +
NULLVALUE(Foreign_Revenue__c, 0)</formula>
        <name>Quote - Update Total Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sync_Case_Status_with_Ren_Amend_Complete</fullName>
        <description>Updating Case Status to Completed when renewal/amendment transaction is completed</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Sync Case Status with Ren/Amend Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCaseClosureReason</fullName>
        <field>bkrCase_Close_Reason__c</field>
        <literalValue>No Reply from Broker</literalValue>
        <name>UpdateCase Closure Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCaseQuote</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>UpdateQuote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateQCase</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>UpdateCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateQuote</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>UpdateQuote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatingCase</fullName>
        <description>Case Updating to closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>UpdateCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case - Update Quote Information Section</fullName>
        <actions>
            <name>Case_Populate_Bound_Premium</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Populate_Policy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Populate_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Populate_Quote_Version</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Populate_Quoted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Populate_Quoted_Premium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>In-Progress,Finalized,Bind,Policy Issued – Closed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Closed Quote</fullName>
        <actions>
            <name>Quote_Populate_Expiry_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Populate_Finalized_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Update_Closed_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Finalized,Bind,Policy Issued – Closed,Declined,Aborted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - On bind send details to Quebec Triage Outlook</fullName>
        <actions>
            <name>Send_Bind_Details_to_Quebec_Triage</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Bind</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Region__c</field>
            <operation>equals</operation>
            <value>Quebec</value>
        </criteriaItems>
        <description>Runs a workflow to send bind details to Quebec Triage team&apos;s outlook when Quebec region Case is Bound (temporary workaround till Quebec Queues are implemented)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Aborted</fullName>
        <actions>
            <name>Quote_Sync_Case_Reason_with_Quote_Abor</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Abor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Status__c), ISPICKVAL(Status__c, &quot;Aborted&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Bind</fullName>
        <actions>
            <name>Quote_Sync_Case_Owner_with_Quote_Bind</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Bind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),ISCHANGED(Status__c), ISPICKVAL(Status__c, &quot;Bind&quot;),ISPICKVAL( Case__r.bkrCase_Region__c ,&quot;Pacific&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Bind - Central</fullName>
        <actions>
            <name>Qt_Sync_Case_Owner_with_Qt_Bind_Central</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Bind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),  ISCHANGED(Status__c), ISPICKVAL(Status__c, &quot;Bind&quot;), OR( ISPICKVAL( Case__r.bkrCase_Region__c ,&quot;Atlantic&quot;), ISPICKVAL( Case__r.bkrCase_Region__c ,&quot;Ontario&quot;),  ISPICKVAL( Case__r.bkrCase_Region__c ,&quot;Prairies&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Bind - Quebec</fullName>
        <actions>
            <name>Qt_Sync_Case_Owner_with_Qt_Bind_Quebec</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Bind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(CONTAINS( $Profile.Name ,&quot;Broker&quot;)),ISCHANGED(Status__c), ISPICKVAL(Status__c, &quot;Bind&quot;),ISPICKVAL( Case__r.bkrCase_Region__c ,&quot;Quebec&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Bind for HUB</fullName>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Bind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Status__c), ISPICKVAL(Status__c, &quot;Bind&quot;), NOT(ISPICKVAL(Case__r.bkrCase_Region__c,&quot;Pacific&quot;)), ISPICKVAL(Case__r.Offering_Project__c,&quot;HUB&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Closed</fullName>
        <actions>
            <name>Quote_Case_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Reason_with_Quote_Clos</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Clos</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Policy Issued – Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Declined</fullName>
        <actions>
            <name>Quote_Sync_Case_Reason_with_Quote_Decl</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_Comments</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote_Decl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Status__c), ISPICKVAL(Status__c, &quot;Declined&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Sync Case Status when Quote Finalized</fullName>
        <actions>
            <name>Quote_Sync_Case_Status_with_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Finalized</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>Quote</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Quote_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Quote__c.Quote_Expiry_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Quote - Update Premium</fullName>
        <actions>
            <name>Quote_Update_Premium_to_Premium_Selected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Premium_Selected__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Update Total Revenue</fullName>
        <actions>
            <name>Quote_Update_Total_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( NOT(CONTAINS(LastModifiedBy.Profile.Name,&quot;Broker&quot;)), OR( ISCHANGED(Canadian_Revenue__c),  ISCHANGED(US_Revenue__c),  ISCHANGED(Foreign_Revenue__c), ISNEW() ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote%2FPolicy - Sync Case Status when Amendment Closed</fullName>
        <actions>
            <name>Qt_Policy_Sync_Case_Reason_with_Qt_Amen</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Case_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sync_Case_Status_with_Ren_Amend_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Policy Issued – Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>Complex Amendment,Simple Amendment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote%2FPolicy - Sync Case Status when Renewal Closed</fullName>
        <actions>
            <name>Qt_Policy_Sync_Case_Reason_with_Qt_Ren</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Case_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Policy_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sync_Case_Status_with_Ren_Amend_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Policy Issued – Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>Renewal Re-visit,Renewal</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote%3A Closed Policy %28Amendment%29</fullName>
        <actions>
            <name>Policy_Populate_Finalized_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Policy_Update_Closed_Policy_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Finalized</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>Complex Amendment,Simple Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Policy</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote%3A Closed Policy %28R%26A%29</fullName>
        <actions>
            <name>Policy_Populate_Expiry_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Policy_Populate_Finalized_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Policy_Update_Closed_Policy_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Finalized</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.bkrCase_Subm_Type__c</field>
            <operation>equals</operation>
            <value>Renewal Re-visit,Renewal,Complex Amendment,Simple Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Policy</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
