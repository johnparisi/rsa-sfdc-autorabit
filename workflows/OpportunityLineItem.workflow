<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdatePremiumVolume</fullName>
        <field>Custom_Premium_Volume__c</field>
        <formula>TotalPrice</formula>
        <name>UpdatePremiumVolume</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>On Opportunity Product Total Price not equal to %22%22</fullName>
        <actions>
            <name>UpdatePremiumVolume</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.TotalPrice</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Runs a workflow when the opportunity product&apos;s total price is not equal to zero.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
