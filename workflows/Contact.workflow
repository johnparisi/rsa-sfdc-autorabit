<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capitalize_Contact_First_Name</fullName>
        <field>FirstName</field>
        <formula>UPPER(LEFT(FirstName, 1)) &amp;
MID(FirstName, 2, LEN(FirstName))</formula>
        <name>Capitalize the Contact&apos;s First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capitalize_the_Contact_s_Last_Name</fullName>
        <description>Capitalizes the Contact&apos;s Last Name field.</description>
        <field>LastName</field>
        <formula>UPPER(LEFT(LastName, 1)) &amp;
MID(LastName, 2, LEN(LastName))</formula>
        <name>Capitalize the Contact&apos;s Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Contact_Status_to_Active</fullName>
        <description>Changes the contact&apos;s status to &quot;Active&quot;</description>
        <field>bkrContact_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Change Contact Status to &quot;Active&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Contact_RT_to_Competitor</fullName>
        <description>Changes the contact&apos;s record type to &quot;Competitor&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>Competitor</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change the Contact RT to &quot;Competitor&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Contact_RT_to_Employee</fullName>
        <description>Changes the contact&apos;s record type to &quot;Employee&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>RSA_Employee</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change the Contact RT to &quot;Employee&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Contact_RT_to_Supplier</fullName>
        <description>Changes the contact record type to &quot;Supplier or Vendor&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>Supplier_or_Vendor</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change the Contact RT to &quot;Supplier...&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_the_Contact_Record_Type_to_Broker</fullName>
        <description>Changes the Contact Record Type to &quot;Broker&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>bkrContact_BrokerRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change the Contact RT to &quot;Broker&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Mailing_City_to_Contact</fullName>
        <field>MailingCity</field>
        <formula>Account.BillingCity</formula>
        <name>Copy Mailing City to Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Mailing_Country_to_Contact</fullName>
        <field>MailingCountry</field>
        <formula>Account.BillingCountry</formula>
        <name>Copy Mailing Country to Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Mailing_Postal_Code_to_Contact</fullName>
        <field>MailingPostalCode</field>
        <formula>Account.BillingPostalCode</formula>
        <name>Copy Mailing Postal Code to Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Mailing_Province_to_Contact</fullName>
        <field>MailingState</field>
        <formula>Account.BillingState</formula>
        <name>Copy Mailing Province to Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Mailing_Street_to_Contact</fullName>
        <field>MailingStreet</field>
        <formula>Account.BillingStreet</formula>
        <name>Copy Mailing Street to Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_NPS_Last_Updated_Field</fullName>
        <field>bkrContact_NPSLastUpdate__c</field>
        <formula>TODAY()</formula>
        <name>Update the &quot;NPS Last Updated&quot; Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrContact_CopyEmailtoUniqueExtID</fullName>
        <description>Copies the email field into the Email Unique External ID field to prevent the creation of records with duplicate email addresses.</description>
        <field>bkrContact_EmailUniqueExtID__c</field>
        <formula>Email</formula>
        <name>Copy the Email to Unique Ext. ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrContact_EmailOptOut</fullName>
        <description>Opts out of all e-mail communications.</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <name>Opt Out of All E-mail Communications</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCopyAliasfromUser</fullName>
        <field>bkrContact_BRAVO_ID__c</field>
        <formula>bkrContact_RelatedSalesforceUser__r.Alias</formula>
        <name>Copy Alias from User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCopyDepartmentfromUser</fullName>
        <field>Department</field>
        <formula>bkrContact_RelatedSalesforceUser__r.Department</formula>
        <name>Copy Department from User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCopyEmailfromUser</fullName>
        <field>Email</field>
        <formula>bkrContact_RelatedSalesforceUser__r.Email</formula>
        <name>Copy Email from User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCopyFirstNamefromUser</fullName>
        <field>FirstName</field>
        <formula>bkrContact_RelatedSalesforceUser__r.FirstName</formula>
        <name>Copy First Name from User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCopyLastNamefromUser</fullName>
        <field>LastName</field>
        <formula>bkrContact_RelatedSalesforceUser__r.LastName</formula>
        <name>Copy Last Name from User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrCopyTitlefromUser</fullName>
        <field>Title</field>
        <formula>bkrContact_RelatedSalesforceUser__r.Title</formula>
        <name>Copy Title from User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>On Contact Net Promoter Score Field is Updated</fullName>
        <actions>
            <name>Update_the_NPS_Last_Updated_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Executes a workflow rule when the NPS field on the Contacts page is updated by getfeedback.com</description>
        <formula>ISCHANGED( bkrContact_NPS__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Contact Record Type equals %22Competitor%22</fullName>
        <actions>
            <name>bkrContact_EmailOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Competitor</value>
        </criteriaItems>
        <description>Detects when a contact record type is a competitor.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On Contact has %22First Name%22</fullName>
        <actions>
            <name>Capitalize_Contact_First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a rule when the first name field is populated for any contact.</description>
        <formula>AND(     NOT(ISBLANK(FirstName)),     OR(         ISNEW(),         ISCHANGED(FirstName)     ),     LEFT(FirstName, 1) &lt;&gt; UPPER(LEFT(FirstName, 1)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Contact has %22Last Name%22</fullName>
        <actions>
            <name>Capitalize_the_Contact_s_Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs a workflow when a contact has a last name.</description>
        <formula>AND(     NOT(ISBLANK(LastName)),     OR(         ISNEW(),         ISCHANGED(LastName)     ),     LEFT(LastName, 1) &lt;&gt; UPPER(LEFT(LastName, 1)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Contact has Salesforce User ID</fullName>
        <actions>
            <name>bkrCopyAliasfromUser</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrCopyDepartmentfromUser</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrCopyEmailfromUser</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrCopyFirstNamefromUser</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrCopyLastNamefromUser</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>bkrCopyTitlefromUser</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>RSA Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.bkrContact_RelatedSalesforceUser__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a contact is associated with a Salesforce User ID, use the user record as a source for all duplicate contact information.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Contact is Created</fullName>
        <actions>
            <name>Change_Contact_Status_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
            <value></value>
        </criteriaItems>
        <description>Detects when a new contact is a created and assigns an &quot;Active&quot; status.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>On Contact is Created or Edited</fullName>
        <actions>
            <name>Copy_Mailing_City_to_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Mailing_Country_to_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Mailing_Postal_Code_to_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Mailing_Province_to_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Mailing_Street_to_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Broker,Competitor,Employee,Supplier or Vendor</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Email is not blank</fullName>
        <actions>
            <name>bkrContact_CopyEmailtoUniqueExtID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Runs a workflow when the Email field is not blank</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On New or Edited %22Broker%22 or %22Prospect%22 Contact</fullName>
        <actions>
            <name>Change_the_Contact_Record_Type_to_Broker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Brokerage (MM-Level),Brokerage (MG or ML-Level),Prospect</value>
        </criteriaItems>
        <description>Assigns a broker record type when the parent account record type contains the word &quot;brokerage&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On New or Edited %22Competitor%22 Contact</fullName>
        <actions>
            <name>Change_the_Contact_RT_to_Competitor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Competitor</value>
        </criteriaItems>
        <description>Detects when the parent account record type is equal to &quot;Competitor&quot; when a contact is created or edited.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On New or Edited %22Employee%22 Contact</fullName>
        <actions>
            <name>Change_the_Contact_RT_to_Employee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>RSA Regional or Head Office</value>
        </criteriaItems>
        <description>Detects when an employee contact is created or added to any of RSA&apos;s regional or head offices.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On New or Edited %22Supplier or Vendor%22 Contact</fullName>
        <actions>
            <name>Change_the_Contact_RT_to_Supplier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supplier or Vendor</value>
        </criteriaItems>
        <description>Detects when a contact is created or edited where the parent account record type is equal to &quot;Supplier or Vendor&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
