<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>bkrUpdateEntitlemenName</fullName>
        <description>Updates the name of the entitlement to a standardized set.</description>
        <field>Name</field>
        <formula>&quot;Priority Status&quot; &amp; &quot; &quot; &amp; &quot;(Ending &quot;&amp; TEXT(EndDate) &amp; &quot;)&quot;</formula>
        <name>Update Entitlement Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>On Entitlement is Created or Edited</fullName>
        <actions>
            <name>bkrUpdateEntitlemenName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Entitlement.AccountId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the name of the entitlement when it is created or edited to a standardized name.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
