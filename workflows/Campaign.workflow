<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Campaign_Record_Type_to_Enabler</fullName>
        <field>RecordTypeId</field>
        <lookupValue>bkrCampaign_EnablerRecordType</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Campaign Record Type to &quot;Enabler&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
