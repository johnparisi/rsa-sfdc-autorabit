<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SMEQ_RESOURCE_BUNDLES_ID_UPDATE</fullName>
        <field>SMEQ_RESOURCE_BUNDLES_ID__c</field>
        <formula>string_id__c+language_code__c+bundle__c+country_code__c+variant__c+TEXT(Region__c)</formula>
        <name>SMEQ_RESOURCE_BUNDLES_ID_UPDATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SMEQ RESOURCE BUNDLES ID Populate</fullName>
        <actions>
            <name>SMEQ_RESOURCE_BUNDLES_ID_UPDATE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate SMEQ RESOURCE BUNDLES id when it&apos;s created/updated</description>
        <formula>NOT(ISBLANK(string_id__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
