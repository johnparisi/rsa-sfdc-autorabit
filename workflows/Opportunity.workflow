<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Finance</fullName>
        <description>Email to Finance</description>
        <protected>false</protected>
        <recipients>
            <recipient>tadey@rsagroup.ca</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CLM/PTBA_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Broker_Appointment_In_Closed_Won_Stage1</fullName>
        <ccEmails>brokerconnect@rsagroup.ca.invalid</ccEmails>
        <description>New Broker Appointment In Closed Won Stage</description>
        <protected>false</protected>
        <recipients>
            <recipient>karen.smith@rsagroup.ca</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrWorkflowDriven/New_Broker_Appointment_In_Closed_Won_Stage</template>
    </alerts>
    <alerts>
        <fullName>New_Broker_Appointment_In_Committed_Stage</fullName>
        <ccEmails>brokerconnect@rsagroup.ca.invalid</ccEmails>
        <description>New Broker Appointment In Committed Stage</description>
        <protected>false</protected>
        <recipients>
            <recipient>karen.smith@rsagroup.ca</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrWorkflowDriven/New_Broker_Appointment_In_Committed_Stage</template>
    </alerts>
    <alerts>
        <fullName>Notify_Opportunity_Owner_of_Upcoming_Close_Date</fullName>
        <description>Notify Opportunity Owner of Upcoming Close Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>bkrWorkflowDriven/Opportunity_Close_Date_Approaching</template>
    </alerts>
    <alerts>
        <fullName>bkrMMGSL_NotifyUnderwriterOnPolicyIssued</fullName>
        <description>MMGSL: Notify the Underwriter that the Policy Has Been Issued</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>bkrWorkflowDriven/bkrMMGSL_PolicyIssuedNoticeToUnderwriter</template>
    </alerts>
    <fieldUpdates>
        <fullName>CLM_Approved_FU</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CLM - Approved FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLM_In_Progress_FU</fullName>
        <field>Approval_Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>CLM - In Progress FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLM_Recalled_FU</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>CLM - Recalled FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLM_Rejection_FU</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CLM Rejection FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bkrOpportunity_UpdateNameToMatchClientAc</fullName>
        <description>Updates the opportunity&apos;s name to match the name of the associated Client Account record.</description>
        <field>Name</field>
        <formula>LEFT(
If( 
bkrClientAccount__r.ParentId&lt;&gt;null, 
bkrClientAccount__r.Parent.Name&amp;&quot; - &quot;, 
null) 
&amp; bkrClientAccount__r.Name,120)</formula>
        <name>Update the Name Per the Client Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>All Commercial Lines%3A If Client Account %21%3D Blank</fullName>
        <actions>
            <name>bkrOpportunity_UpdateNameToMatchClientAc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Detects if the bkrClientAccount__c field is blank on either a Commercial Insurance or Mid-Market &amp; GSL record type.</description>
        <formula>AND((NOT(ISBLANK(bkrClientAccount__c))),OR((RecordTypeId = &quot;012o0000000CE8k&quot;),(RecordTypeId = &quot;012o0000000xtgu&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Broker Appointment In Closed Won Stage</fullName>
        <actions>
            <name>New_Broker_Appointment_In_Closed_Won_Stage1</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Portfolio-Level - PI/CI</value>
        </criteriaItems>
        <description>Once a new broker appointment opportunity has the stage field changed to closed won, an email will be sent to brokerconnect@rsagroup.ca to create a case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Broker Appointment In Committed Stage</fullName>
        <actions>
            <name>New_Broker_Appointment_In_Committed_Stage</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Commitment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Portfolio-Level - PI/CI</value>
        </criteriaItems>
        <description>Once a new broker appointment opportunity has the stage field changed to committed, an email will be sent to brokerconnect@rsagroup.ca to create a case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On %22Close Date%22 is not blank</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Personal or Commercial Insurance</value>
        </criteriaItems>
        <description>Detects when a close date is entered on a portfolio opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Opportunity_Owner_of_Upcoming_Close_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>On Quote Required Date is set or changed</fullName>
        <actions>
            <name>Quote_Required_Date</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.bkrQuoteRequiredDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Policy-Level - MM/GSL</value>
        </criteriaItems>
        <description>Sets a follow-up task for the owner of an MM/GSL opportunity when a quote required date is set.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Quote_Required_Date</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.bkrQuoteRequiredDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Quote Required: Have you issued the quote as per the broker&apos;s request?</subject>
    </tasks>
</Workflow>
