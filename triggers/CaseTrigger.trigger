trigger CaseTrigger on Case (before insert, after insert, before update, after update) {  

    //after delete, after insert, after undelete, before delete, 
    CaseTriggerHandler handler = new CaseTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isBefore){
        handler.beforeInsert(Trigger.new);
        //handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
    for(Case OCase : Trigger.New){
            if(OCase.IsChildCase__c == true){
                system.debug('Inside CHild Case creation:');
                if(OCase.parentid !=null)
                {
                CloneAttachmentsOnParentCase.cloneParentCaseAttachments(OCase.Id, OCase.ParentId);
                }
            }
        }
        handler.afterInsert(Trigger.new);
         
    }
    
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        //handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
            }
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
        handler.afterUpdate(Trigger.new, Trigger.oldMap); //Fix this - to be consolidated into one call.
        //CaseTriggerHandler.OnAfterUpdateAsync(Trigger.newMap.keySet());
    }
    
    /*else if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    else if(Trigger.isDelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        CaseTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
    }
    
    else if(Trigger.isUnDelete){
        handler.OnUndelete(Trigger.new);    
    }*/
            handler.afterInsert(Trigger.new);
            
}