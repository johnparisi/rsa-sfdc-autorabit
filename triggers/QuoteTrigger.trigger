/**
 * Author: James Lee
 * Created At: September 19 2016
 * 
 * Quote trigger
 */

trigger QuoteTrigger on Quote__c (before insert, after insert, before update, after update)
{
   
    QuoteHandler handler = new QuoteHandler();
    if (Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            handler.beforeInsert(Trigger.new);
        }
        else if (Trigger.isUpdate)
        {
            handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    else //isAfter
    {
        if (Trigger.isInsert)
        {
            handler.afterInsert(Trigger.new);
        }
        else if (Trigger.isUpdate)
        {
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }   
    }

}