/**
 * Author: Alexander Luksidadi
 * Created At: April 25, 2016
 * 
 * Account trigger
 */
trigger AccountTrigger on Account (before insert, after insert, before update, after update) {
    
	AccountHandler handler = new AccountHandler();
	if(Trigger.isBefore){
		if(Trigger.isInsert){
			handler.beforeInsert(Trigger.new);
		}
		else if(Trigger.isUpdate){
			handler.beforeUpdate(Trigger.new, Trigger.oldMap);
		}
	}
	//isAfter
	else{
		if(Trigger.isInsert){
			handler.afterInsert(Trigger.new);
		}
		else if(Trigger.isUpdate){
			handler.afterUpdate(Trigger.new, Trigger.oldMap);
		}	
	}

}