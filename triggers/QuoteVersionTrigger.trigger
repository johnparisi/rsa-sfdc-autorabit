trigger QuoteVersionTrigger on Quote_Version__c (before insert, after insert, before update, after update) {
    //after delete, after insert, after undelete, after update, before delete, 
    QuoteVersionTriggerHandler handler = new QuoteVersionTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter){
        System.Debug('~~~QuoteVersionTrigger:AfterUpdate');
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
        //AttachmentTriggerHandler.OnAfterUpdateAsync(Trigger.newMap.keySet());
    }
    
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    else if(Trigger.isDelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        AttachmentTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
    }
    
    else if(Trigger.isUnDelete){
        handler.OnUndelete(Trigger.new);    
    }*/
}