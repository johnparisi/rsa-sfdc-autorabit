trigger AttachmentTrigger on Attachment (after delete, after insert, after undelete, 
	after update, before delete, before insert, before update) {
	AttachmentTriggerHandler handler = new AttachmentTriggerHandler(Trigger.isExecuting, Trigger.size);
	
	/*if(Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	else*/ if(Trigger.isInsert && Trigger.isAfter){
		//handler.OnAfterInsert(Trigger.new);
		if(AttachmentTriggerHandler.firstRun == true){
			Set<Id> newAttachmentIds = new Set<ID>();
	        string strParentId;
	        for(Attachment att : Trigger.new){
	            strParentId = att.ParentId;
	            if(strParentId != null 
	            	&& strParentId.substring(0,3) == '00T'){ // is related to a Case
	                newAttachmentIds.add(att.Id);
	            }
	        }		
			if( newAttachmentIds.size()>0 )  // we only want Task attachments (from Outlook)                                      
				AttachmentTriggerHandler.OnAfterInsertAsync(newAttachmentIds);
        }
	}
	
	/*else if(Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
	}
	else if(Trigger.isUpdate && Trigger.isAfter){
		//handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
		AttachmentTriggerHandler.OnAfterUpdateAsync(Trigger.newMap.keySet());
	}
	
	else if(Trigger.isDelete && Trigger.isBefore){
		handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
	}
	else if(Trigger.isDelete && Trigger.isAfter){
		handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
		AttachmentTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
	}
	
	else if(Trigger.isUnDelete){
		handler.OnUndelete(Trigger.new);	
	}*/
}