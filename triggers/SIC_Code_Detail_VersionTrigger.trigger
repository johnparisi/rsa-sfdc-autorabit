trigger SIC_Code_Detail_VersionTrigger on SIC_Code_Detail_Version__c (before insert, before update) {
    SIC_Code_Detail_VersionHandler handler = new SIC_Code_Detail_VersionHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.beforeInsert(Trigger.new);
        }else if(Trigger.isUpdate){
            handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }       
    }
}