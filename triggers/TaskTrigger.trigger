trigger TaskTrigger on Task (after insert) {  //after delete, after undelete, after update, before delete, before insert, before update
	TaskTriggerHandler handler = new TaskTriggerHandler(Trigger.isExecuting, Trigger.size);
	
	/*if(Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	else*/ if(Trigger.isInsert && Trigger.isAfter){
		// only fire the TaskTriggerHandler when Type=Email, Case (associated from Outlook)
        
        //handler.OnAfterInsert(Trigger.new);
		Set<Id> newTaskIds = new Set<ID>();
        string strWhatId;
        for(Task t : Trigger.new){
            strWhatId = t.WhatId;
            if(strWhatId != null 
            	&& strWhatId.substring(0,3) == '500' 
            	&& t.Type == 'Email'){ // is related to a Case
                newTaskIds.add(t.Id);
            }
        }
		//TaskTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
		if(newTaskIds.size()>0){
            TaskTriggerHandler.OnAfterInsertAsync(newTaskIds);
        }
	}
	
	/*else if(Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
	}
	else if(Trigger.isUpdate && Trigger.isAfter){
		//handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
		TaskTriggerHandler.OnAfterUpdateAsync(Trigger.newMap.keySet());
	}
	
	else if(Trigger.isDelete && Trigger.isBefore){
		handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
	}
	else if(Trigger.isDelete && Trigger.isAfter){
		handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
		TaskTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
	}
	
	else if(Trigger.isUnDelete){
		handler.OnUndelete(Trigger.new);	
	}*/
}