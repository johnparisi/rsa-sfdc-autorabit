/**
 * Author: James Lee
 * Created At: November 10 2016
 * 
 * Coverage trigger
 */
trigger CoverageTrigger on Coverages__c (before insert, after insert, before update, after update)
{
	CoverageHandler handler = new CoverageHandler();
	if (Trigger.isBefore)
    {
		if (Trigger.isInsert)
        {
			handler.beforeInsert(Trigger.new);
		}
		else if (Trigger.isUpdate)
        {
			handler.beforeUpdate(Trigger.new, Trigger.oldMap);
		}
	}
	else //isAfter
    {
		if (Trigger.isInsert)
        {
			handler.afterInsert(Trigger.new);
		}
		else if (Trigger.isUpdate)
        {
			handler.afterUpdate(Trigger.new, Trigger.oldMap);
		}	
	}
}