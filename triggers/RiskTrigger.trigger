/**
 * Author: James Lee
 * Created At: August 25 2016
 * 
 * Risk trigger
 */

trigger RiskTrigger on Risk__c (before insert, after insert, before update, after update)
{
   
	RiskHandler handler = new RiskHandler();
	if (Trigger.isBefore)
    {
		if (Trigger.isInsert)
        {
			handler.beforeInsert(Trigger.new);
		}
		else if (Trigger.isUpdate)
        {
			handler.beforeUpdate(Trigger.new, Trigger.oldMap);
		}
	}
	else //isAfter
    {
		if (Trigger.isInsert)
        {
			handler.afterInsert(Trigger.new);
		}
		else if (Trigger.isUpdate)
        {
			handler.afterUpdate(Trigger.new, Trigger.oldMap);
		}	
	}

}