/**
 * Author: James Lee
 * Created At: April 28 2017
 * 
 * SicAnswer trigger
 */
trigger SicAnswerTrigger on SIC_Answer__c (before insert, after insert, before update, after update)
{
    SicAnswerHandler handler = new SicAnswerHandler();
    if (Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            handler.beforeInsert(Trigger.new);
        }
        else if (Trigger.isUpdate)
        {
            handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    else //isAfter
    {
        if (Trigger.isInsert)
        {
            handler.afterInsert(Trigger.new);
        }
        else if (Trigger.isUpdate)
        {
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }	
    }
}