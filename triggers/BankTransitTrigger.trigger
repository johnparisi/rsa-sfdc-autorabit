/**
* Overall trigger implementation for the Bank Transit object. Implementation is done in individual domain files.
*/

trigger BankTransitTrigger on Bank_Transit_Number__c (after insert, after update) {
   	if(Label.IstriggerlogicONOFF =='ON'){
   	BankTransitHandler handler = new BankTransitHandler();
   	// place a handler on after insert and update
	if(Trigger.isAfter){
		if(Trigger.isInsert){
			handler.afterInsert(Trigger.new);
		}
		else if(Trigger.isUpdate){
			handler.afterUpdate(Trigger.new, Trigger.oldMap);
		}
	}
	}
}