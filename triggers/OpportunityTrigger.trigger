trigger OpportunityTrigger on Opportunity (before insert, after insert, before update, after update) {
    
    OpportunityTriggerHandler handler = new OpportunityTriggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.beforeInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
            handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    //isAfter
    else{
        if(Trigger.isInsert){
            handler.afterInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }   
    }

}