trigger UserTrigger on User (before insert, before update) {
    // ***
    // copy the LanguageLocaleKey into a custom field so we can use it for filtering records (SIC Code)
    // ***          

	for(Integer x = 0; x<Trigger.new.size(); x++) {
        trigger.new[x].Language__c = trigger.new[x].LanguageLocaleKey;
    }

}