trigger PolicyTrigger on Policy__c (before insert, after insert, before update, after update) {
    
    PolicyTriggerHandler handler = new PolicyTriggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.beforeInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
            handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    //isAfter
    else{
        if(Trigger.isInsert){
            handler.afterInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }   
    }

}