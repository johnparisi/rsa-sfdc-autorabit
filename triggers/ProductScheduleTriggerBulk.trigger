trigger ProductScheduleTriggerBulk on OpportunityLineItem (after update) {
    Date currentDate;
    Decimal numberPayments;
    Decimal paymentAmount;
    Decimal totalPaid;
    Decimal totalPrice;
    List<OpportunityLineItemSchedule> newScheduleObjects = new List<OpportunityLineItemSchedule>();
    List<OpportunityLineItemSchedule> oldScheduleObjects = new List<OpportunityLineItemSchedule>();

    Set<Id> pbeIds = new Set<Id>();
    Set<Id> opptyIds = new Set<Id>();
    Set<Id> olis = new Set<Id>();

    for (OpportunityLineItem oli : Trigger.new) {
        pbeIds.add(oli.pricebookentryid);
        opptyIds.add(oli.OpportunityId);
        olis.add(oli.Id);
    }
    
    Map<Id, Opportunity> Opptys = new Map<Id, Opportunity>([SELECT Id, CloseDate, IsClosed, RecordType.DeveloperName FROM Opportunity WHERE id in :opptyIds]);
    
    for (OpportunityLineItem item : trigger.new) {
        if((item.ServiceDate == trigger.oldmap.get(item.Id).ServiceDate && item.Custom_Premium_Volume__c == trigger.oldmap.get(item.Id).Custom_Premium_Volume__c && item.Term__c == trigger.oldmap.get(item.Id).Term__c) || Opptys.get(item.OpportunityId).RecordType.DeveloperName != 'bkrMMGSL_Opportunity' && Opptys.get(item.OpportunityId).IsClosed) {continue;}
        if (item.Custom_Premium_Volume__c != null){
            paymentAmount = item.Custom_Premium_Volume__c;
        }
        else {
            paymentAmount = item.TotalPrice;
        }
        if (item.Term__c == null || item.Term__c == 0) {
            numberPayments = 12;
        }
        else {
            numberPayments = item.Term__c;
        }
        totalPrice = paymentAmount;
        paymentAmount = paymentAmount.divide(numberPayments,2);
        if (item.ServiceDate == NULL) {
            currentDate = Opptys.get(item.OpportunityId).CloseDate;
        }
        else {
            currentDate = item.ServiceDate;
        }
        
        totalPaid = 0;
        
        for (Integer i = 1;i < numberPayments ;i++) {
            OpportunityLineItemSchedule s = new OpportunityLineItemSchedule();
            s.Revenue = paymentAmount;
            s.ScheduleDate = currentDate;
            s.OpportunityLineItemId = item.id;
            s.Type = 'Revenue'; 
            newScheduleObjects.add(s); 
            totalPaid = totalPaid + paymentAmount;
            currentDate = currentDate.addMonths(1); 
        }
        
        paymentAmount = totalPrice - totalPaid;
        OpportunityLineItemSchedule s = new OpportunityLineItemSchedule();
        s.Revenue = paymentAmount;
        s.ScheduleDate = currentDate;
        s.OpportunityLineItemId = item.id;
        s.Type = 'Revenue'; 
        newScheduleObjects.add(s);               
    } 
    
    if (newScheduleObjects.size() > 0) {
        try {
            delete([SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId in :olis]);
            insert(newScheduleObjects);
        } 
        catch (System.DmlException e) {
            for (Integer ei = 0; ei < e.getNumDml(); ei++) {
                System.debug(e.getDmlMessage(ei));
            } 
        }
    }
}