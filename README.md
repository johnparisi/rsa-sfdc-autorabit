### What is this repository for? ###

This repository contains RSA Salesforce & Vlocity AutoRABIT deployment demo components

### How do I get set up? ###

See the Customer Relationship Centre of Excellence Environment Setup documentation.

### Contribution guidelines ###

AutoRABIT demo session items only

### Who do I talk to? ###

* Tristan Adey - tadey@johnson.ca
* John Parisi - jparisi@johnson.ca