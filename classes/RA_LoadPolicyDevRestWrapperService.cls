@RestResource(urlMapping='/load/*')

global with sharing class RA_LoadPolicyDevRestWrapperService {
    
    @HttpPost
    global static SearchResponse loadPolicy(string caseNumber) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type','application/json');
        }
           
        SearchResponse wrapper = FP_AngularPageController.loadPolicyTransactionRest(caseNumber);
     
        return wrapper;       
	}
}