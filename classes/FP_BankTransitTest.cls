/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FP_BankTransitTest {

	private static Bank_Transit_Number__c bankTransitOne;	
	private static Bank_Transit_Number__c bankTransitTwo;
	private static Bank_Transit_Number__c bankTransitThree;
	private static Bank_Transit_Number__c bankTransitFour;
	private static Bank_Transit_Number__c bankTransitFive;
	

    @isTest
    private static void testBankTransitService() {
    	
    	setupTestData();
    	
    	// SOSL queries don't work when run as unit tests - they always return a zero length string
        // sending in a string should return a zero list array
 
        List<Bank_Transit_Number__c> bankList = new List<Bank_Transit_Number__c>();
        FP_AngularPageController.BankTransitResponse bankTransitResponse = FP_AngularPageController.searchForTransitNumbers('');
        System.assert(bankTransitResponse.bankTransitNumberList.size() == 0, 'expecting no results back');          
    }
    
    private static void setupTestData() {
    	
    	// record one
    	bankTransitOne = new Bank_Transit_Number__c();
    	bankTransitOne.Effective_Date__c = Date.today().addDays(-1);
    	bankTransitOne.Role_Type__c = 'FI';
    	bankTransitOne.Client__c = 123456781;
    	bankTransitOne.Active_to__c = Date.today().addYears(1);
    	bankTransitOne.Active_From__c = Date.today().addDays(-1);
    	bankTransitOne.Inter_Code__c = '000304821';
    	bankTransitOne.Client_Reference__c = 123456781;
    	bankTransitOne.Role_Code__c = '000304821';
    	bankTransitOne.Financial_Institution_Name__c = 'Bank One';
    	bankTransitOne.Address1__c = 'Unit 14';
    	bankTransitOne.Address2__c = '1 West Street';
    	bankTransitOne.City__c = 'Toronto';
    	bankTransitOne.Province__c = 'ON';
    	bankTransitOne.Postal_Code__c = 'M4S2P2';
    	bankTransitOne.Client_Type__c ='FI';
    	bankTransitOne.Withdrawal_Date__c = null;
    	insert bankTransitOne;
    	
    	// record two
    	bankTransitTwo = new Bank_Transit_Number__c();
    	bankTransitTwo.Effective_Date__c = Date.today().addDays(-1);
    	bankTransitTwo.Role_Type__c = 'FI';
    	bankTransitTwo.Client__c = 123456781;
    	bankTransitTwo.Active_to__c = Date.today().addYears(1);
    	bankTransitTwo.Active_From__c = Date.today().addDays(-1);
    	bankTransitTwo.Inter_Code__c = '000304822';
    	bankTransitTwo.Client_Reference__c = 123456782;
    	bankTransitTwo.Role_Code__c = '000304822';
    	bankTransitTwo.Financial_Institution_Name__c = 'Bank Two';
    	bankTransitTwo.Address1__c = 'Unit 25';
    	bankTransitTwo.Address2__c = '1 East Street';
    	bankTransitTwo.City__c = 'Toronto';
    	bankTransitTwo.Province__c = 'ON';
    	bankTransitTwo.Postal_Code__c = 'M4S1P2';
    	bankTransitTwo.Client_Type__c ='FI';
    	bankTransitTwo.Withdrawal_Date__c = null;
    	insert bankTransitTwo;
    	    	 
    	// record three
    	bankTransitThree = new Bank_Transit_Number__c();
    	bankTransitThree.Effective_Date__c = Date.today().addDays(-1);
    	bankTransitThree.Role_Type__c = 'FI';
    	bankTransitThree.Client__c = 123456781;
    	bankTransitThree.Active_to__c = Date.today().addYears(1);
    	bankTransitThree.Active_From__c = Date.today().addDays(-1);
    	bankTransitThree.Inter_Code__c = '000304823';
    	bankTransitThree.Client_Reference__c = 123456783;
    	bankTransitThree.Role_Code__c = '000304823';
    	bankTransitThree.Financial_Institution_Name__c = 'Bank Three';
    	bankTransitThree.Address1__c = 'Unit 25';
    	bankTransitThree.Address2__c = '1 East Street';
    	bankTransitThree.City__c = 'Toronto';
    	bankTransitThree.Province__c = 'ON';
    	bankTransitThree.Postal_Code__c = 'M4S1P2';
    	bankTransitThree.Client_Type__c ='FI';
    	bankTransitThree.Withdrawal_Date__c = null;
    	insert bankTransitThree;    	

    	// record four
    	bankTransitFour = new Bank_Transit_Number__c();
    	bankTransitFour.Effective_Date__c = Date.today().addDays(30);
    	bankTransitFour.Role_Type__c = 'FI';
    	bankTransitFour.Client__c = 123456781;
    	bankTransitFour.Active_to__c = Date.today().addYears(1);
    	bankTransitFour.Active_From__c = Date.today().addDays(-2);
    	bankTransitFour.Inter_Code__c = '000304824';
    	bankTransitFour.Client_Reference__c = 123456784;
    	bankTransitFour.Role_Code__c = '000304824';
    	bankTransitFour.Financial_Institution_Name__c = 'Bank Four';
    	bankTransitFour.Address1__c = 'Unit 25';
    	bankTransitFour.Address2__c = '1 Northeast Street';
    	bankTransitFour.City__c = 'Toronto';
    	bankTransitFour.Province__c = 'ON';
    	bankTransitFour.Postal_Code__c = 'M4S1P2';
    	bankTransitFour.Client_Type__c ='FI';
    	bankTransitFour.Withdrawal_Date__c = null;
    	insert bankTransitFour;    	 	

    	// record five
    	bankTransitFive = new Bank_Transit_Number__c();
    	bankTransitFive.Effective_Date__c = Date.today().addDays(-1);
    	bankTransitFive.Role_Type__c = 'FI';
    	bankTransitFive.Client__c = 123456781;
    	bankTransitFive.Active_to__c = Date.today().addYears(-1);
    	bankTransitFive.Active_From__c = Date.today().addDays(-2);
    	bankTransitFive.Inter_Code__c = '000304825';
    	bankTransitFive.Client_Reference__c = 123456785;
    	bankTransitFive.Role_Code__c = '000304825';
    	bankTransitFive.Financial_Institution_Name__c = 'Bank Five';
    	bankTransitFive.Address1__c = 'Unit 25';
    	bankTransitFive.Address2__c = '1 South Street';
    	bankTransitFive.City__c = 'Toronto';
    	bankTransitFive.Province__c = 'ON';
    	bankTransitFive.Postal_Code__c = 'M4S1P2';
    	bankTransitFive.Client_Type__c ='FI';
    	bankTransitFive.Withdrawal_Date__c = null;
    	insert bankTransitFive;    	
    }
    
}