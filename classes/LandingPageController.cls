/*
Controller for the Landing page(SMEQ_LandingPage.vfp), retrieves components to be displayed on the landing page and manages navigation when user takes action. 
Author: Sara Al Mouaswas
Date: July 16, 2017
*/
public with sharing class LandingPageController{

    public List<Landing_Page_Component__c> incomingRecords {get;set;}
    public String logo {get;set;}
    public String pageLanguage {get;set;}
    public String currentLanguage {get;set;}
    public Boolean currentLanguageEN {get;set;}
    public Boolean currentLanguageFR {get;set;}

    
    public LandingPageController()
    {
        retrieveComponent();
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
        getRegionLogo();
        setLanguageToggle();
    }
    
    //The favicon displayed depends on the variant/offering of the user 
    public String getFaviconUrl() { 
         String favIconUrl = 'assets/images/sme/favicon.ico';
         if('hub'.equalsIgnoreCase(getVariant())) {
             favIconUrl = 'assets/images/hub/favicon.ico';
         } 
         return favIconUrl ;
     }
     
    //Determines variant used to set favicon url in method above 
    public String getVariant() {
         String pageBasePath = URL.getCurrentRequestUrl().getPath();
         String variant = 'sme';
         if(pageBasePath.contains('hubsme')) {
             variant = 'hub';
         } else if(pageBasePath.contains('hubbroker')) {
             variant = 'hub';
         } 
         return variant;
     }
    
    //User will be able to navigate to other pages or pages within the broker tool application
    public PageReference navigateToNextPage() {
        String pageValue = ApexPages.currentPage().getParameters().get('pageValue');
        String brokerApplicationRoute = ApexPages.currentPage().getParameters().get('brokerApplicationRoute'); 
        PageReference navigateTo = ApexPages.currentPage();
        //The parameters passed from VF page correspond to an existing page in the application 
        if (brokerApplicationRoute != null){
            if (brokerApplicationRoute.contains('search-policies')) {
                brokerApplicationRoute = brokerApplicationRoute + '?lang=' + this.currentLanguage;
            }
            navigateTo = new PageReference (URL.getSalesforceBaseUrl().toExternalForm() + brokerApplicationRoute);   
        }
        //The parameters passed from VF page correspond to logout page, or other pages outside of the current broker tool 
        else if (pageValue != null){
            navigateTo = new PageReference(pageValue);   
        }
        return navigateTo; 
    }
        
     
    //Set logo based on user profile region
    public void getRegionLogo() {  
         String region = UserService.getRegion(UserInfo.getUserId())[0];
        this.logo = 'assets/img/RSA-pro.svg';
         if(region == 'Pacific') {
            this.logo = 'assets/img/CNS-pro-logo.svg';
         }
     }
     
    //Set logo based on user profile region
    public void getRegion() {
        Id userId = UserInfo.getUserId();
        User u = [Select Contact.bkrContact_Region__c From User Where Id = :UserInfo.getUserId()][0];
        String region = u.Contact.bkrContact_Region__c;
    }
    
    //Components to be displayed will differ based on user offering
    public List<Landing_Page_Component__c> retrieveComponent(){
        String user_offering = UserService.getOffering(UserInfo.getUserId())[0];
        incomingRecords = [SELECT Instructions__c, icon__c, Page_Value__c, Navigate_To__c, Offering_Project__c
                             FROM Landing_Page_Component__c
                             WHERE Offering_Project__c = :user_offering
                             ORDER by Position__c Asc
                          ]; 
        return incomingRecords;
    }
    
    public void setLanguageToggle() {
        if (this.currentLanguage == null){
            this.currentLanguage = UserService.getFormattedUserLanguage();
        }
        if(this.currentLanguage=='EN') { 
            this.pageLanguage = 'en';
            this.currentLanguageEN = true;
            this.currentLanguageFR = false;
        } else if (this.currentLanguage=='FR') { 
            this.pageLanguage = 'fr';
            this.currentLanguageEN = false;
            this.currentLanguageFR = true;
        }
    }
    
    public pageReference toggleLanguage() {
        if(this.currentLanguage=='EN') { 
            this.currentLanguage = 'FR';
            this.pageLanguage = 'fr';
        } else if (this.currentLanguage=='FR') { 
            this.currentLanguage = 'EN';
            this.pageLanguage = 'en';
        } 
        setLanguageToggle();
        return null;
        
    }
    
 }