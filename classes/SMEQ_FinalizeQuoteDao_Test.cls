@isTest
public class SMEQ_FinalizeQuoteDao_Test
{
    @testSetup
    static void setupTestData()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }

	 /**
     * Method to test savePageState request with valid quoteID and valid lastVistedPageNum is 
     * saved successfully
     * Assertions
     * 1. Assert Broker_Application_Stage__c is updated from value in the request that maps to correct Broker Application Stage
     * 2. Assert Status__c is updated from value in the request that maps to correct Status
     * 3. Assert Response message is set to success
     * 
     */
    @isTest 
    static void testfinalizeQuoteWithValidParameter()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_FinalizeQuoteRequestModel  reqModel  = new SMEQ_FinalizeQuoteRequestModel ();
            SMEQ_FinalizeQuoteResponseModel respModel = new SMEQ_FinalizeQuoteResponseModel();
            SMEQ_FinalizeQuoteDao finalizeQuoteDao = new SMEQ_FinalizeQuoteDao();
            
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_testDataGenerator.getOpenQuote(c);
            
            reqModel  = buildMockFinalizeQuoteRequest(aQuote.Id);
            respModel = finalizeQuoteDao.finalizeQuote(reqModel);       
            
            Quote__c updatedQuote = [
                SELECT Id, Package_Type_Selected__c, Status__c
                FROM Quote__c
                WHERE ID = :aQuote.ID
                LIMIT 1
            ];
            System.assertEquals(RiskService.PACKAGE_OPTION_1, updatedQuote.Package_Type_Selected__c);
        }
   	}

   	/**
     * Method to test a finalize quote request with valid quoteID and null packageID
     * Assertions
     * 1. Assert response message is set to null with invalid request
     */
    @isTest 
    static void testfinalizeQuoteWithoutPageNumber()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_FinalizeQuoteRequestModel  reqModel  = new SMEQ_FinalizeQuoteRequestModel ();
            SMEQ_FinalizeQuoteResponseModel respModel = new SMEQ_FinalizeQuoteResponseModel();
            SMEQ_FinalizeQuoteDao finalizeQuoteDao = new SMEQ_FinalizeQuoteDao();
            
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_testDataGenerator.getOpenQuote(c);
            
            reqModel = buildMockFinalizeQuoteRequest(aQuote.Id);
            reqModel.packageID = null;
            respModel = finalizeQuoteDao.finalizeQuote(reqModel);
            Quote__c updatedQuote = [
                SELECT Id, Package_Type_Selected__c, Status__c
                FROM Quote__c
                WHERE ID = :aQuote.ID
                LIMIT 1
            ];
            System.assertNotEquals(RiskService.PACKAGE_OPTION_2, updatedQuote.Package_Type_Selected__c);
            System.assertNotEquals('Finalized', updatedQuote.Status__c);
        }
   	}

   	/**
     * Method to test a finalize quote request with invalid quoteID and valid packageID
     * Assertions
     * 1. Assert response message is set to null with invalid request
     */
    @isTest 
    static void testfinalizeQuoteWithoutQuoteID() {
       
       SMEQ_FinalizeQuoteRequestModel  reqModel  = new SMEQ_FinalizeQuoteRequestModel ();
       SMEQ_FinalizeQuoteResponseModel respModel = new SMEQ_FinalizeQuoteResponseModel();
       SMEQ_FinalizeQuoteDao finalizeQuoteDao = new SMEQ_FinalizeQuoteDao();
       
       reqModel = buildMockFinalizeQuoteRequest(null);
       respModel = finalizeQuoteDao.finalizeQuote(reqModel);
       System.assert(respModel.finalizeDate == null);
       System.assert(respModel.ePolicyNumber == null);
   	}

   	 /**
     * Method to test savePageState request with valid quoteID and an invalid package id
     * Assertions
     * 1. Assert response message is set to null
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithInvalidPackage()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_FinalizeQuoteRequestModel  reqModel  = new SMEQ_FinalizeQuoteRequestModel ();
            SMEQ_FinalizeQuoteResponseModel respModel = new SMEQ_FinalizeQuoteResponseModel();
            SMEQ_FinalizeQuoteDao finalizeQuoteDao = new SMEQ_FinalizeQuoteDao();
            
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_testDataGenerator.getOpenQuote(c);
            
            reqModel = buildMockFinalizeQuoteRequest(aQuote.Id);
            reqModel.packageID = 'P-INVALID';
            respModel = finalizeQuoteDao.finalizeQuote(reqModel);
            Quote__c updatedQuote = [
                SELECT Id, Package_Type_Selected__c, Status__c
                FROM Quote__c
                WHERE ID = :aQuote.ID
                LIMIT 1
            ];
            System.assert(updatedQuote.Package_Type_Selected__c != 'P-INVALID');
            System.assert(updatedQuote.Status__c != 'Finalized');
        }
    }
    
   	//use to populate mock finalize quote Request
   	public static SMEQ_FinalizeQuoteRequestModel buildMockFinalizeQuoteRequest(String quoteID)
	{
		SMEQ_FinalizeQuoteRequestModel mockRequest = new  SMEQ_FinalizeQuoteRequestModel();
        mockRequest.quoteID = quoteID;
        mockRequest.packageID = 'P2';
        return mockRequest;
	}
}