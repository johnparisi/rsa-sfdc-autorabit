/**
 * ProcessingError
 * Author: James Lee
 * Date: January 10 2017
 * Feature: FP-748
 * 
 * The purpose of this class is to store the validation errors of a single record.
 */
public class ProcessingError
{
	private String errorMessage;
    
    /**
     * Create new error.
     * @param error: The error text found.
     */
    public ProcessingError(String error)
    {
        this.errorMessage = error;
    }
    
    public String getError()
    {
        return errorMessage;
    }
}