public class SMEQ_FinalizeQuoteDao {  
	
	private static String FINALIZED = 'Finalized';

	/**
     * Finalizes the quote by settting updating status to finalized and updating selected package
     * @param  SMEQ_FinalizeQuoteResponseModel request
     * @return SMEQ_FinalizeQuoteResponseModel
     */
    @TestVisible 
    public SMEQ_FinalizeQuoteResponseModel finalizeQuote(SMEQ_FinalizeQuoteRequestModel request) {
        SMEQ_FinalizeQuoteResponseModel response = new SMEQ_FinalizeQuoteResponseModel();
        SMEQ_CustomSettingSearchUtility cSettingSearch = new SMEQ_CustomSettingSearchUtility();
        System.debug('Request being recieved is ********' + request);
        
        if(request.quoteID !=null && request.packageID !=null) {        	
        	List<Quote__c> quoteList = [SELECT Id FROM Quote__c WHERE id =:request.quoteID];
        	Map<String,String> packagesLookup = new Map<String,String>();
        	packagesLookup = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Package_Type_Selected__c');            
            String selectedPackage = packagesLookup.get(request.packageID); 
            String language = request.language;
                     
	        if(!quoteList.isEmpty() && selectedPackage!=null) {
	            Quote__c aQuote = quoteList[0];
	            aQuote.Package_Type_Selected__c = selectedPackage; //to be updated after integration update only once callout to esb has been confirmed
	            aQuote.Language__c = language;
	            Savepoint sp = Database.setSavepoint();
	            try {
	            	upsert aQuote;
                    response.ePolicyNumber = 'EP-12345'; //aQuote.ePolicy__c;
                    response.finalizeDate  = String.valueOf(Date.today());
	            }
	    		catch(Exception e){
		           	Database.rollback(sp); throw new SMEQ_ServiceException(e.getMessage(), e);
        		} 	    	
	        }	       
	    }
        return response;
    }
}