/**
  * @author        Saumil Bapat
  * @date          11/16/2016
  * @description   Helper class to create renewal cases.
  * @Process       Invoked on before update
*/
public with sharing class SLID_CaseToQuoted {
  public static void updateStatusToQuoted(List<Case> newCases, Map<Id, Case> oldCases)
  {
    System.Debug('~~~updateStatusToQuoted');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_CaseToQuoted: ' + Limits.getQueries());
    //If the integration log id is not set, it is not running in a webservice context
    if (Util_Logging.integrationLogId == null)
    {
        System.Debug('~~~Integration Id is null');
        System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_CaseToQuoted: ' + Limits.getQueries());
        return;
    }
    //Query the new cases with child quoteVersions
    Map<Id,Case> newCasesMap = new Map<Id,Case>();
    for (Case newCase : newCases)
    {
        newCasesMap.put(newCase.Id, newCase);
    }
    newCases = [Select Id, Status, (Select Id from Policy_Quotes__r where Status__c = 'Quoted') from Case where Id IN :newCasesMap.KeySet()];
    
    //Loop through the queries cases
    for (Case newCase : newCases)
    {
      
      Case oldCase = oldCases.get(newCase.Id);
      System.Debug('~~~newCase: ' + newCasesMap.get(newCase.Id));
      System.Debug('~~~oldCase: ' + oldCase);
      System.Debug('~~~Policy Quotes: ' + newCase.Policy_Quotes__r.size());
      
      //If a case has moved out of Referred status and still has active quotes, set the status to quoted
      if (oldCase.Status == 'Referred' && newCasesMap.get(newCase.Id).Status != 'Referred')
      {
        if(newCase.Policy_Quotes__r.size() > 0)
        {
            System.Debug('~~~Updating Status: newCase to Quoted');
            newCasesMap.get(newCase.Id).Status = 'Quoted';
        }
        else
        {
            System.Debug('~~~Updating Status: newCase to In Progress');
            newCasesMap.get(newCase.Id).Status = 'In Progress';
        }
      }
    }
    System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_CaseToQuoted: ' + Limits.getQueries());
  }
}