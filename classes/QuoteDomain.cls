/**
* QuoteDomain
* Author: James Lee
* Date: September 19 2016
* 
* The object domain is where the real work happens, this is where we will apply the actual business logic. 
* We have the main Object Domain which should handle all common processing. 
* Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
* Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
* just implement logic in the appopriate method. 
*/
public with sharing class QuoteDomain
{
    private QuoteService service = new QuoteService();
    
    public void beforeInsert(List<Quote__c> records)
    {
    }
    
    public void beforeUpdate(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
    }
    
    public void afterInsert(List<Quote__c> records)
    {
    }
    
    public void afterUpdate(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
    }
    
    //Quote with Record Type of Open Quote or Close Quote
    public class SMEQDomain
    {
        private QuoteService service = new QuoteService();
        
        public void beforeInsert(List<Quote__c> records)
        {
            
        }
        
        public void beforeUpdate(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
        {
            service.setDeviatePackagesForUnderwriter(records, oldRecords);
            system.debug('beforeUpdate'); 
        }
        
        public void afterInsert(List<Quote__c> records)
        {
            service.insertCaseRoleSharing(records);
            service.insertLocationRisk(records,null);
            service.insertDefaultRisks(records);
              system.debug('AfterInsert'); 
        }
        
        public void afterUpdate(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
        {
            service.updateSelectedCoverages(records, oldRecords);
            service.populateCaseInfo(records, oldRecords);
            service.insertLocationRisk(records,oldRecords);
            service.updateCaseWithLLPReferralTriggerReason(records, oldRecords);
             system.debug('UfterUpdate'); 
        }
    }
}