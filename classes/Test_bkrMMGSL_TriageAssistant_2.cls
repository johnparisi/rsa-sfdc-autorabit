@isTest
private class Test_bkrMMGSL_TriageAssistant_2 {
    
    static testmethod void TriageAssistantTest() {
        
        // Create a Sample Client Account
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        insert brokerageAcc;
        
        // Create a Broker to Work at the Brokerage Account
        Contact brokerCntc = new Contact();
        brokerCntc.FirstName= 'John';
        brokerCntc.LastName='Smith';
        brokerCntc.AccountId=brokerageAcc.Id;
        insert brokerCntc;
        
        // Create a Case from the Broker Contact
        Case c = new Case();
        c.AccountId = brokerageAcc.Id;
        c.ContactId = brokerCntc.Id;
       // c.bkrCase_Insured_Client__c=clientAcc.id;
        c.Status='New';
        c.Origin='Email';
        c.Type='Request for a Quote';
        insert c;
        
        Test.setCurrentPage(Page.bkrMMGSL_TradingAssistant_TriageCases);
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        bkrMMGSL_TriageAssistant vfp = new bkrMMGSL_TriageAssistant(sc);
        
        vfp.Name='test';
        vfp.Address ='';
        vfp.City ='';
        vfp.State ='ON';
        vfp.PostalCode='';
        vfp.CountryISOCode ='CA';
        vfp.searchDnB();
        
        DNBCallout.dnbEntry fnt= new  DNBCallout.dnbEntry();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293813';
        fnt.Company='test'; // Added by me becz the this variable is the use to give Account Name
        vfp.results.add(fnt);
        vfp.myDUNS='255293813';
        vfp.selectRecordAction();
        
        vfp.Name='test';
        vfp.Address ='';
        vfp.City ='';
        vfp.State ='ON';
        vfp.PostalCode='';
        vfp.CountryISOCode ='CA';
        vfp.searchDnB();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293814';
        vfp.results.add(fnt);
        vfp.myDUNS='255293814';
       
        vfp.selectRecordAction();
        vfp.closePage=true;
        vfp.selectRecordAction();
        
        vfp.Name='';
        vfp.Address ='';
        vfp.City ='';
        vfp.State ='ON';
        vfp.PostalCode='';
        vfp.CountryISOCode ='CA';
        vfp.searchDnB();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293814';
        vfp.results.add(fnt);
        vfp.myDUNS='255293814';
        vfp.record=fnt;
        vfp.selectRecordAction();
        vfp.closePage=true;
        vfp.selectRecordAction();
        
        
        // --------- code to cover Exception------------
        
        
        Case c1 = new Case();
        c1.AccountId = brokerageAcc.Id;
        c1.ContactId = brokerCntc.Id;
       // c.bkrCase_Insured_Client__c=clientAcc.id;
        c1.Status='New';
        c1.Origin='Email';
        c1.Type='Request for a Quote';
        insert c1;
        ApexPages.StandardController sc1 = new ApexPages.StandardController(c1);
        bkrMMGSL_TriageAssistant vfp1 = new bkrMMGSL_TriageAssistant(sc1);
        
        vfp1.Name='test';
        vfp1.Address ='';
        vfp1.City ='';
        vfp1.State ='ON';
        vfp1.PostalCode='';
        vfp1.CountryISOCode ='CA';
        vfp1.searchDnB();
        
        DNBCallout.dnbEntry fnt1= new  DNBCallout.dnbEntry();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt1.DUNSNumber='255293813';
      //  fnt.Company='test'; // Added by me
        vfp1.results.add(fnt);
        vfp1.myDUNS='255293813';
        vfp1.selectRecordAction();
        
        vfp1.Name='test';
        vfp1.Address ='';
        vfp1.City ='';
        vfp.State ='ON';
        vfp1.PostalCode='';
        vfp1.CountryISOCode ='CA';
        vfp1.searchDnB();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293814';
        vfp.results.add(fnt);
        vfp.myDUNS='255293814';
       
        vfp1.selectRecordAction();
        vfp1.closePage=true;
        vfp1.selectRecordAction();
        
        vfp1.Name='';
        vfp1.Address ='';
        vfp1.City ='';
        vfp1.State ='ON';
        vfp1.PostalCode='';
        vfp1.CountryISOCode ='CA';
        vfp1.searchDnB();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt1.DUNSNumber='255293814';
        vfp1.results.add(fnt);
        vfp1.myDUNS='255293814';
        vfp1.record=fnt;
        vfp1.selectRecordAction();
        vfp1.closePage=true;
        vfp1.selectRecordAction();
        
        
        
        
     }
    
}