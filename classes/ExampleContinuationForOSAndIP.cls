global with sharing class ExampleContinuationForOSAndIP extends vlocity_ins.VlocityContinuationIntegration {
    global override Object invokeMethod(String methodName, Map < String, Object > inputMap, Map < String, Object > outMap, Map < String, Object > options) {
        Boolean result = true;
        try {
            // the custom methods can have any customized signature, but you *must* pass in the "options" parameter
            if (methodName.equals('myLongRunningCalloutMethod')) {
                // Returns Continuation Object
                return myLongRunningCalloutMethod(5, options, inputMap);
            } else if (methodName.equals('myCallback')) {
                myCallback(inputMap, outMap, options);
            } else {
                result = false;
            }
        } catch (System.Exception e) {
            result = false;
        }
        
        return result;
    }
    
    // You can have other parameters, but make sure you always pass in Map<String, Object> options
    
    private Object myLongRunningCalloutMethod(Integer count, Map < String, Object > options, Map <String, Object> inputMap) {
        // Make a normal HTTP Request. Any HTTP Request can be made as a Continuation call
        String url = 'https://node-count.herokuapp.com/' + count;
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('callout:M2V_GetRate');
        Map<String, Object> xmlMap = (Map<String, Object>)inputMap.get('RQXML');
        String XMLRequest = (String)xmlMap.get('result');
        req.setBody(XMLRequest);
        
        // Create a Continuation for the HTTP Request      
        Continuation con = new Continuation(60);
        
        // Set Continuation info for OmniScript
        con.continuationMethod = 'myCallback';
        String label = con.addHttpRequest(req);
        VlocitySetAsyncCallbackState(con, label, options);
        
        // Set Continuation info for Integration Procedure
        options.put('continuationState', label);
        options.put('continuationCallback', 'myCallback');
        
        // Return the Continuation Object
        return con;
    }
    
    // This method does NOT return the Continuation Object
    // It returns the response to OmniScript, therefore needs to set outMap.
    
    private Object myCallback(Map < String, Object > inputMap, Map < String, Object > outMap, Map < String, Object > options) {
        // This is how you access the state and labels passed by the Continuation Object. You can maintain more complex State.
        Object state = inputMap.get('vlcContinuationCallbackState');
        Object labels = inputMap.get('vlcContinuationCallbackLabels');
        
        HttpResponse response = Continuation.getResponse((String) state);
        Integer statusCode = response.getStatusCode();
        
        if (statusCode >= 2000) {
            // Handle error case. See Salesforce Continuation documentation for details.
        }
        
        // This map is returned to OmniScript or Integration Procedure
        outMap.put('myLongRunningCalloutMethod2Response', response.getBody());
        return outMap;
    }
}