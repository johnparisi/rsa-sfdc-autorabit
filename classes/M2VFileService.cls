public with sharing class M2VFileService {
    public static final Id SMEDocumentRecordTypeId = 
        Schema.SObjectType.ContentVersion.getRecordTypeInfosByDeveloperName().get('SME_Document').getRecordTypeId();
    /*
    fileName: full name of the file includes the extension (i.e. 0Q0A0000000lRX0KAM)

    return: newly created contentDocumentID
    */
    public static Id insertFile(String fileName, Id partentId, String body){

        Id contentDocumentId;
        ContentVersion cvf = new ContentVersion();
        cvf.VersionData = EncodingUtil.base64Decode(body);
        cvf.ContentLocation= 's';
        cvf.PathOnClient= fileName;
        cvf.RecordTypeId = SMEDocumentRecordTypeId;
        insert cvf;
        
        contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cvf.Id].ContentDocumentId;
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = contentDocumentId;
        cdl.LinkedEntityId = partentId;
        cdl.ShareType = 'V';
        cdl.Visibility = 'AllUsers';
        insert cdl;
        
        return contentDocumentId;
    }
}