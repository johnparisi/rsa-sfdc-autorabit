/**
* @File Name    :   BatchRenewalOpportunityCreateTest
* @Description  :   Test class for BatchRenewalOpportunityCreate
* @Date Created :   01/28/2016
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   testing class
* @Modification Log:
*****************************************************************
* Ver       Date        Author              Modification
* 1.0       01/28/2016  Habiba Zaman        Created the file/class
**/

@isTest
private class BatchRenewalOpportunityCreateTest {

	// CRON expression: midnight on March 15.
	// Because this is a test, job executes
	// immediately after Test.stopTest().
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	
	
	@isTest static void TestRenewalOpportunityCreation() {
        //Custom setting
        insert new bkrMMGSL_CustomSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Auto_Renewal_ThresholdDays__c=12);

        // Create a Sample Client Account
        PolicyService pservice = new PolicyService();
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        brokerageAcc.bkrAccount_HuonBrokerNumber__c='001231242';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testu1', Email='testuser1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', FirstName='Test',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg.com');
    
        insert u;

        //Policy to Client Mapping

        Policy_to_Client_Mapping__c pcm = new Policy_to_Client_Mapping__c();
        pcm.Policy_number__c = '000123123';
        pcm.Client_Account__c = clientAcc.Id;
        insert pcm;

        Policy_to_Client_Mapping__c pcm2 = new Policy_to_Client_Mapping__c();
        pcm.Policy_number__c = '000123122';
        pcm.Client_Account__c = clientAcc.Id;
        insert pcm2;

        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();

        // Policy Data

        Policy__c policy2 = new Policy__c();
        policy2.Name ='000123122';
        policy2.Client_Name__c = clientAcc.Name;
        policy2.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy2.Master_Broker_Account__c = brokerageAcc.Id;
        policy2.Strategic_Segment__c = 'Entertainment';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Product_Code__c='COM';
        policy2.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c= Date.today().addDays(-15);
        policy2.Term_Expiry_Date__c = Date.today().addDays(13);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy2.Control_Field__c = pservice.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        insert policy2;
        Test.startTest();         

        bkrMMGSL_CustomSettings__c cs = bkrMMGSL_CustomSettings__c.getOrgDefaults();
        System.assertEquals(cs.Auto_Renewal_ThresholdDays__c,12);
       


        BatchRenewalOpportunityCreate bd = new BatchRenewalOpportunityCreate();
		database.executebatch(bd);



        Test.stopTest();
       	Policy__c newPolicy2 = [Select Policy_Stage__c,Qualified_Renewal__c from Policy__c where Id =:policy2.Id];
        System.assertEquals(newPolicy2.Policy_Stage__c,'Waiting for Next Renewal Cycle');


    	}

    	@isTest static void runSchedule(){
		Test.startTest();
			//Run job
			String jobId = System.schedule('ScheduleApexClassTest_RenewalOppTest', CRON_EXP, new BatchRenewalOpportunityCreateSchedule());

		Test.stopTest();
	}
	
}