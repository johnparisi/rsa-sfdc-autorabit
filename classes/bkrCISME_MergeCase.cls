public class bkrCISME_MergeCase {

    public Case sourceCase {get; set;}
    public Case destinationCase {get; set;}
    public string sourceCaseId {get; set;}
    public string destinationCaseId {get; set;}
    public string sourceCaseTabId {get; set;}
    public string destinationCaseTabId {get; set;}
    
    public Flow.Interview.CI_SME_Merge_Case myflow { get; set; }
    
    public bkrCISME_MergeCase (){
        sourceCase = new Case();
        destinationCase = new Case();      
    }
    
    public Boolean getvarCompleteProcess() {
      if (myFlow == null) return null;
      return (Boolean)(myFlow.getVariableValue('varCompleteProcess')); 

   }
    
    public void validateCaseIds(){
        myflow = null;            
        sourceCase = new Case();
        destinationCase = new Case();  
        if(sourceCaseId != '' ){
            sourceCase =  new Case(ParentId = sourceCaseId); // use the ParentId field as a dummy lookup <apex:inputfield   >
        }
        if(destinationCaseId != ''){
            destinationCase =  new Case(ParentId = destinationCaseId); // use the ParentId field as a dummy lookup <apex:inputfield   >
        }
        return;
    }    
    public void startFlow(){

        if(sourceCase.ParentId != null && destinationCase.ParentId != null){ // use the ParentId field as a dummy lookup <apex:inputfield   >
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('varSourceCaseId',sourceCase.ParentId);
            myMap.put('varDestinationCaseId',destinationCase.ParentId);
            myflow = new Flow.Interview.CI_SME_Merge_Case(myMap);               
        }
        return;
    }
}