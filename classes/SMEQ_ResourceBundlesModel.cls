global with sharing class SMEQ_ResourceBundlesModel extends SMEQ_RESTResponseModel{
	public List<SMEQ_KeyValueModel> resourceBundleEnglish;
    public List<SMEQ_KeyValueModel> resourceBundleFrench;
}