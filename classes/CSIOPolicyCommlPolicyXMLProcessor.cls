/**
This class decomposes the CSIO Policy XML into a set of SObjects
*/

public with sharing class CSIOPolicyCommlPolicyXMLProcessor {
    
    public List<Claim__c> claimList = null;
    public Quote__c policy = null;
    
    public Map<Quote_Risk_Relationship__c, Account> accountQrrMap = null;
    public Map<Quote_Risk_Relationship__c, Contact> contactQrrMap = null;
    
    public list<Policy_History__c> policyHistoryList = null;
        
    /**
    Process the Xml stream representing the CommlPolicy element
    @param xmlStreamReader The CommlPolicy in xml stream format
    */
    
    public void process(Dom.XmlNode xmlNode, case newCase) {
        policy = new Quote__c();
        // policy level attributes first
        // Policy Number, Air Miles, Annual Sales, Current Term Amount, Claims Free years
        policy.ePolicy__c  = xmlNode.getChildElement('PolicyNumber',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
        System.debug('#### policy number in the CSIOPolicyCommlPolicyXMLPRocessor is ' + policy.ePolicy__c );
        policy.Air_Miles_Loyalty_Points_Standard__c = Integer.valueOf(xmlNode.getChildElement('AirMilesPoints', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText());
        //policy.Claim_Free_Years__c = Integer.valueOf(xmlNode.getChildElement('YearsClaimsFree', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText());
        /*
        // RA-1540
        if (CSIOPolicyXMLProcessor.evaluatePath(xmlNode.getChildElement('YearsClaimsFree', CSIOPolicyXMLProcessor.ACORD_NAMESPACE)) == null) {
        	policy.Claim_Free_Years__c = 0;
        } else {
            policy.Claim_Free_Years__c = Integer.valueOf(xmlNode.getChildElement('YearsClaimsFree', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText());
        }
        */
        policy.Claim_Free_Years__c = Integer.valueOf(xmlNode.getChildElement('YearsClaimsFree', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText()); // RA-1540
        policy.Quote_Number__c = xmlNode.getChildElement('QuoteInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('CompanysQuoteNumber',  CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
        policy.Quote_Expiry_Date__c = Date.valueOf(xmlNode.getChildElement('QuoteInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('QuoteValidUntilDt',  CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
        policy.Number_of_claims_in_the_last_5_years__c = xmlNode.getChildElement('NumLosses', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
        policy.Name_Insured__c = xmlNode.getChildElement('CompanyCd', CSIOPolicyXMLProcessor.CSIO_NAMESPACE).getText();

        if(xmlNode.getChildElement('OtherOrPriorPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != NULL){
            if(xmlNode.getChildElement('OtherOrPriorPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('PolicyCd',  CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() == 'Prior'){
                policy.Currently_Insured__c = true; // RA-1540 & RA-1651
            }
        }

        // defaulting the package
        policy.Package_Type_Selected__c = 'Standard';
       
        
        // collections - Claims, Additional Interests
        // initialize all the lists and maps.
        claimList = new List<Claim__c>();
        accountQrrMap = new Map<Quote_Risk_Relationship__c, Account>();
        contactQrrMap = new Map<Quote_Risk_Relationship__c, Contact>();
        
        // loop through the nodes and find the repeating ones
        for (Dom.XmlNode childXmlNode : xmlNode.getChildElements()) {
            if (childXmlNode.getName() == 'Loss') {
                Claim__c claim = new Claim__c();
                //CSIOPolicyXMLProcessor.debugChildren(childXmlNode);
                String lossDate = CSIOPolicyXMLProcessor.evaluatePath(childXmlNode.getChildElement('LossDt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                lossDate = lossDate + ' 01:00:00';
                Date claimDate = Date.valueOf(lossDate);
                claim.Year__c = claimDate.year();
                claim.Month__c = claimDate.month() + '';
                String claimTypeCd = CSIOPolicyXMLProcessor.evaluatePath(childXmlNode.getChildElement('LossCauseCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                Map<String, String> claimTypeMap = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Type_of_Claim__c');  
                if (claimTypeCd != null) {
                	claim.Type_of_Claim__c = claimTypeMap.get(claimTypeCd);
                }
                String indentifier = CSIOPolicyXMLProcessor.evaluatePath(childXmlNode.getChildElement('ItemIdInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                if (indentifier == null) {
                    claim.Amount__c = Double.valueOf(childXmlNode.getChildElement('LossPayment', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('TotalPaidAmt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
                    if(claim.Amount__c == null){
                        claim.Amount__c = 0.00;
                    }
                } 
                
                else {
                    Dom.XmlNode payments = childXmlNode.getChildElement('LossPayment', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                    Double paymentTotal = 0;
                    Double outstandingTotal = 0;
                    for (Dom.XmlNode paymentType: payments.getChildElements()){
                        if(paymentType.getName() == 'LossPaymentAmt'){
                            paymentTotal = paymentTotal + Double.valueOf(paymentType.getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
                        }
                        if(paymentType.getName() == 'AmountOutstanding'){
                            outstandingTotal = outstandingTotal + Double.valueOf(paymentType.getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
                        }
                    }
                    claim.Amount__c = paymentTotal;
                    claim.Amount_Outstanding__c = outstandingTotal;
                    claim.HUON_Claim__c = true;
                    
                }
                claimList.add(claim);
            }
            Map<String, String> esbNatureInterestCd = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbParty_Type__c');
             // if it is a QRR / additional interest then delegate it to the CSIOPolicyAdditionalPartyProcessor
            if (childXmlNode.getName() == 'AdditionalInterest') {
                String natureInterestCD = childXmlNode.getChildElement('AdditionalInterestInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('NatureInterestCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
                CSIOPolicyAdditionalPartyProcessor additionalPartyProcessor = new CSIOPolicyAdditionalPartyProcessor();
                // TBC: association between QRR and account / contact
                additionalPartyProcessor.process(childXmlNode);
                additionalPartyProcessor.quoteRiskRelationship.Party_Type__c = esbNatureInterestCd.get(natureInterestCD);
                String deletedParty = childXmlNode.getChildElement('Deleted', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
                if (deletedParty == '1'){
                    additionalPartyProcessor.quoteRiskRelationship.Deleted__c = true;
                } else {
                    additionalPartyProcessor.quoteRiskRelationship.Deleted__c = false;
                }
                Integer additionalInsuredIndex;
                Integer premierFinanceIndex;
                if(additionalPartyProcessor.quoteRiskRelationship.Party_Type__c != RelationshipControllerExtension.PREMIER_FINANCIER){
                    if(additionalInsuredIndex == null){
                       additionalInsuredIndex = 0;
                    }
                    additionalPartyProcessor.quoteRiskRelationship.Party_Number__c = additionalInsuredIndex;
                    additionalInsuredIndex ++; 
                } else {
                    if(premierFinanceIndex == null){
                        premierFinanceIndex = 0;
                    }
                    additionalPartyProcessor.quoteRiskRelationship.Party_Number__c = premierFinanceIndex;
                    premierFinanceIndex ++;
                }
                if (additionalPartyProcessor.isAccount) {
                    accountQrrMap.put(additionalPartyProcessor.quoteRiskRelationship, additionalPartyProcessor.partyAccount);
                }
                else {
                    contactQrrMap.put(additionalPartyProcessor.quoteRiskRelationship, additionalPartyProcessor.partyContact);
                }
            } 
        }
        
        policyHistoryList = new List<Policy_History__c>();
        // pull the recent transactions
        String lastTransactionPremium = '';

        //Set the expiry date properly [in the case if there's something pending]
        Date currentExpiryDate = policy.Quote_Expiry_Date__c;
        
        //Set the effective date properly [in the case if the renewal has been committed]
        Date currentEffectiveDate = policy.Effective_Date__c;

        for (Dom.XMLNode policyHistoryXmlNode: xmlNode.getChildElement('RecentTransactions',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElements()) {
            Policy_History__c policyHistory = new Policy_History__c();
            policyHistory.Change_Name__c = policyHistoryXmlNode.getChildElement('ChangeDescription',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
            policyHistory.Federation_Id__c = policyHistoryXmlNode.getChildElement('FederationID',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
            policyHistory.Change_Date__c = Date.valueOf(policyHistoryXmlNode.getChildElement('EffectiveDate',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText()  + ' 01:00:00');

            //Need to check status here (was in code further below)
            String policyHistoryStatusCd = policyHistoryXmlNode.getChildElement('Status',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
            Map<String, String> policyHistoryStatusType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('ePolicy_Transaction_Status__c');
            policyHistory.Status__c = policyHistoryStatusType.get(policyHistoryStatusCd);

            //Set the expiry date properly as it is set incorrectly from QuoteInfo|QuoteValidUntilDt and should be set from the transaction history to not have pending dates being included in the expiry
            if (policyHistory.Change_Name__c == 'Renew' && policyHistory.Status__c !='Committed'){ currentExpiryDate = Date.valueOf(policyHistoryXmlNode.getChildElement('EffectiveDate',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText()  );}

            //set the effective date properly if the renewal is committed
            if (policyHistory.Change_Name__c == 'Renew' && policyHistory.Status__c =='Committed'){ currentEffectiveDate = Date.valueOf(policyHistoryXmlNode.getChildElement('EffectiveDate',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText()  );}
                         
            if (lastTransactionPremium != null && lastTransactionPremium != '') {
                policyHistory.Old_Premium__c = Double.valueOf(lastTransactionPremium);
            }
            lastTransactionPremium = policyHistoryXmlNode.getChildElement('TermPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null? policyHistoryXmlNode.getChildElement('TermPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElement('Amt',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() : null;

            String newPremiumText = policyHistoryXmlNode.getChildElement('AnnualPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null? policyHistoryXmlNode.getChildElement('AnnualPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElement('Amt',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() : null;
            if (newPremiumText != null && newPremiumText != '' ) {  
                policyHistory.New_Premium__c = Double.valueOf(newPremiumText);
            }
            String transactionPremiumText = policyHistoryXmlNode.getChildElement('ProratedCharge',CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null? policyHistoryXmlNode.getChildElement('ProratedCharge',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElement('Amt',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() : null;
            if (transactionPremiumText != '' && transactionPremiumText != null){
                policyHistory.Transaction_Premium__c = Double.valueOf(transactionPremiumText);
            }
            String termPremiumText = policyHistoryXmlNode.getChildElement('TermPremium ',CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null? policyHistoryXmlNode.getChildElement('TermPremium ',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElement('Amt',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() : null;
            if (termPremiumText != '' && termPremiumText != null){
                policyHistory.Term_Premium__c = Double.valueOf(termPremiumText);
            }
            policyHistory.Federation_ID__c = policyHistoryXmlNode.getChildElement('FederationID',CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null? policyHistoryXmlNode.getChildElement('FederationID',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText() : null;

            for (Dom.XmlNode childNode: policyHistoryXmlNode.getChildElements()){
                if (childNode.getName() == 'TransactionReason'){
                    for (Dom.XmlNode transactionNode: childNode.getChildElements()){
				 		policyHistory.Change_Description__c += transactionNode.getText();  
                        policyHistory.Change_Description__c += ',';
                    }
                }
            }
            if(CSIOPolicyXMLProcessor.evaluatePath(policyHistoryXmlNode.getChildElement('TransactionReason', CSIOPolicyXMLProcessor.RSA_NAMESPACE))== null){
                policyHistory.Change_Description__c = policyHistory.Change_Name__c;
            }
            policyHistoryList.add(policyHistory);
        }

        newCase.ePolicy_Transaction_Status__c = policyHistoryList.get(policyHistoryList.size()-1).Status__c;
        newCase.ePolicy_Transaction_Type__c = policyHistoryList.get(policyHistoryList.size()-1).Change_Name__c;
        policy.Last_Federation_Id__c =  policyHistoryList.get(policyHistoryList.size()-1).Federation_ID__c;
        newCase.Segment__c = xmlNode.getChildElement('Segment', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText(); 
        
        if (newCase.ePolicy_Transaction_Status__c == 'Pending') {
            policy.Transaction_Effective_Date__c = policyHistoryList.get(policyHistoryList.size()-1).Change_Date__c;
        } else if (newCase.bkrCase_Subm_Type__c != null) {
            if (newCase.bkrCase_Subm_Type__c.contains('Renewal')) {
            	policy.Transaction_Effective_Date__c = policy.Quote_Expiry_Date__c;
        	}
        }
        
        //set the effective date properly if the renewal is committed
        policy.Effective_Date__c = currentEffectiveDate ;
                
        //set the expiry date properly as it is set incorrectly from QuoteInfo|QuoteValidUntilDt and should be set from the transaction history to not have pending dates being included in the expiry
        policy.Quote_Expiry_Date__c = currentExpiryDate;

        String packageType = CSIOPolicyXMLProcessor.evaluatePath(xmlNode.getChildElement('Package', CSIOPolicyXMLProcessor.RSA_NAMESPACE));
        if (packageType != null && (packageType.toLowerCase().contains('monoline') || packageType.toLowerCase().contains('micro'))){

            throw new RSA_ExceedsToolLimitsException(Label.Exceeds_limit_of_tool);
        }
        policy.Retrieved_Package_Code__c = packageType;
        //Update the case to a amendment if the broker was hoping to start a renewal process but there is a pending endorsement on this policy
        if (newCase.ePolicy_Transaction_Type__c == 'Endorse' && newCase.ePolicy_Transaction_Status__c == 'Pending' && newCase.bkrCase_Subm_Type__c != null && newCase.bkrCase_Subm_Type__c.toLowerCase().contains('Renew')){
            newCase.bkrCase_Subm_Type__c = 'Complex Amendment';
        }
    }
    
}