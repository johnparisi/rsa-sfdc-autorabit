@isTest
public class SMEQ_CustomSettingSearchUtility_Test
{
     /**
     * Method to test getCustomSetting returns valid list with a valid parameter
     * Assertions
     * 1. Assert returned custom setting maps size is > 1 after setUpTest values have been inserted
     */ 
    @isTest static void testCustomSettingSearchByType()
    {
        
        
        Map<String,String> resultsMap = new Map<String,String>();
        resultsMap = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Broker_Application_Stage__c');
        System.assert(resultsMap.size() == 5);
    }
    
    /**
     * Method to test getCustomSetting returns valid list with a non-existant parameter
     * Assertions
     * 1. Assert returned custom setting maps size is 0
     */ 
    @isTest static void testCustomSettingSearchByTypeWithInvalidType()
    {
        
       
        Map<String,String> resultsMap = new Map<String,String>();
        resultsMap = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('non-existant');
        System.assert(resultsMap.size() == 0);
    }
    
    
    /**
     * Method to test getCustomSetting returns valid list with a null parameter
     * Assertions
     * 1. Assert returned custom setting maps size is 0
     */ 
    @isTest static void testCustomSettingSearchByTypeWithNullParameter()
    {
        
        
        Map<String,String> resultsMap = new Map<String,String>();
        resultsMap = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType(null);
        System.assert(resultsMap.size() == 0);
    }
    
     @testSetup 
     static void setupTestData() {
      
      //insert SME_Mapping__c Broker Applicaiton Stage

        SME_Mapping__c status1  = new SME_Mapping__c(Name = ' Broker-Application-Stage-01',
                                                                    Field__c = 'Broker_Application_Stage__c',
                                                                    externalValue__c = '1',
                                                                    SFKey__c =  'Getting Started'
                                                                );
        insert status1;


        SME_Mapping__c status2  = new SME_Mapping__c(Name = ' Broker-Application-Stage-02',
                                                                    Field__c = 'Broker_Application_Stage__c',
                                                                    externalValue__c = '2',
                                                                    SFKey__c =  'About the Business'
                                                                );
        insert status2;



         SME_Mapping__c status3  = new SME_Mapping__c(Name = ' Broker-Application-Stage-03',
                                                                    Field__c = 'Broker_Application_Stage__c',
                                                                    externalValue__c = '3',
                                                                    SFKey__c =  'Customize Quote'
                                                                );
        insert status3;

         SME_Mapping__c status4  = new SME_Mapping__c(Name = ' Broker-Application-Stage-04',
                                                                    Field__c = 'Broker_Application_Stage__c',
                                                                    externalValue__c = '4',
                                                                    SFKey__c =  'Review'
                                                                );
        insert status4;
     
        SME_Mapping__c status5  = new SME_Mapping__c(Name = ' Broker-Application-Stage-05',
                                                                    Field__c = 'Broker_Application_Stage__c',
                                                                    externalValue__c = '5',
                                                                    SFKey__c =  'Buy'
                                                                );
        insert status5;
  
  }

}