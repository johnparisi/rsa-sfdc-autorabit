/**
Wrapper class for the Remote call from the Angular client. Keeps the relationship between a quote risk relationship
and account / contact intact.
*/

global with sharing class BindQuoteRiskContactAccountWrapper {
    
    public Quote_Risk_Relationship__c quoteRiskRelationship;
    public Account account;
    public Contact contact;
    
    public boolean isAccount() {
    	if (account != null) {
    		return true;
    	}
    	return false;
    }
       
}