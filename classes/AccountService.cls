/*
 * Account Service Layer
 * Contains processing service methods related to the Account Object
*/
public class AccountService {

    public static Set<Id> centralBoostIds;
    public static Set<Id> pacificIslandSavingsIds;
	public static Set<Id> pacificOtherIds;

    public static Set<Id> getCentralBoostIds(Set<String> centralBoostAccountNumbers){
        if (AccountService.centralBoostIds == null){
            AccountService.centralBoostIds = new Map<Id, Account>([SELECT Id, Name 
                                                        FROM Account 
                                                        WHERE AccountNumber in: centralBoostAccountNumbers]).keySet();
        }

        return AccountService.centralBoostIds;
    }
	public static Set<Id> getPacificOtherIds(){
        if (AccountService.pacificOtherIds == null){
            AccountService.pacificOtherIds = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE BKRACCOUNT_REGION__C='Pacific' and (Name LIKE 'CapriCMW%' OR  Name LIKE 'Waypoint Insurance%' OR Name LIKE 'HUB INTERNATIONAL%' OR Name LIKE 'Envision Insurance%' OR Name LIKE 'Valley First Insurance%') ]).keySet();
        }

        return AccountService.pacificOtherIds ;
    }

    public static Set<Id> getPacificIslandSavingsIds(){
        if (AccountService.pacificIslandSavingsIds == null){
            AccountService.pacificIslandSavingsIds = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Name LIKE 'ISLAND SAVINGS INSURANCE%']).keySet();
        }

        return AccountService.pacificIslandSavingsIds;
    }
	/**
    * updateSegmentationWhenParentChange
    * @param	records		Accounts that were inserted/updated
    * @param	oldRecords	Previous state of the accounts that were inserted/updated mapped by id
    *
    * Update segmentation when the Parent-Child relationship on the brokerage changes.
    */
    public void updateSegmentationWhenParentChange(List<Account> records, Map<Id, Account> oldRecords) {
        // BULKIFY: Unique parent account ids
        Set<Id> parentIds = new Set<Id>();
        Set<Id> idsToUpdate = new Set<Id>();
        
        for (Account a : records) {
            // If it has a parent
            if (a.bkrMasterBrokerageReference__c != null) {
                // old state of the updated account
                Account old = oldRecords != null && oldRecords.containsKey(a.Id) ? oldRecords.get(a.Id) : null;
                Boolean hasNewParent = True; // if its insert, then its a new parent by default
                if (old != null) {
                    // Check if previous parent is the same than the current parent (If its changed)
                    if (a.bkrMasterBrokerageReference__c == old.bkrMasterBrokerageReference__c) {
                        hasNewParent = False;
                    }
                }
                
                if (hasNewParent) {
                    parentIds.add(a.bkrMasterBrokerageReference__c);
                    IdsToUpdate.add(a.Id);
                }
            }
        }
        
        // If theres accounts to update            
        if (parentIds.size() > 0) {
            // BULKIFY: Get all parents of changed account and map it by id
            Map<Id, Account> parentsById = new Map<Id, Account>([select Id,
                                                                 bkrMasterBrokerageReference__c,
                                                                 bkrAccount_Broker_Segment__c,
                                                                 bkrAccount_Estimated_Size__c,
                                                                 bkrAccount_Estimated_Avg_Growth_L3_years__c,
                                                                 bkrAccount_Footprint__c,
                                                                 bkrAccount_Channel__c,
                                                                 bkrAccount_Estimated_RSA_Share_of_Wallet__c,
                                                                 bkrAccount_Fin_Interest_From_Competitor__c,
                                                                 bkrAccount_Acquirer__c,
                                                                 bkrAccount_Are_we_Top_3__c,
                                                                 bkrAccount_Value_Added_Service__c,
                                                                 bkrAccount_Training_and_Education__c,
                                                                 bkrAccount_Marketing_and_Sales_Support__c,
                                                                 bkrAccount_Financial_Partnership__c,
                                                                 bkrAccount_Service_Quality__c,
                                                                 bkrAccount_Ease_of_doing_Business__c,
                                                                 bkrAccount_Compensation__c,
                                                                 bkrAccount_Value_to_Customer__c,
                                                                 Name, 
                                                                 OwnerId
                                                                 from Account where id in :parentIds]);
            
            for (Account a : records) {          
                // If it has a parent
                if (idsToUpdate.contains(a.Id) && parentsById.containsKey(a.bkrMasterBrokerageReference__c)) {
                    // Get parent
                    Account parent = parentsById.get(a.bkrMasterBrokerageReference__c);
                    
                    // Update segmentation
                    a.bkrAccount_Broker_Segment__c = parent.bkrAccount_Broker_Segment__c;
                    a.bkrAccount_Estimated_Size__c = parent.bkrAccount_Estimated_Size__c;
                    a.bkrAccount_Estimated_Avg_Growth_L3_years__c = parent.bkrAccount_Estimated_Avg_Growth_L3_years__c;
                    a.bkrAccount_Footprint__c = parent.bkrAccount_Footprint__c;
                    a.bkrAccount_Channel__c = parent.bkrAccount_Channel__c;
                    a.bkrAccount_Estimated_RSA_Share_of_Wallet__c = parent.bkrAccount_Estimated_RSA_Share_of_Wallet__c;
                    a.bkrAccount_Fin_Interest_From_Competitor__c = parent.bkrAccount_Fin_Interest_From_Competitor__c;
                    a.bkrAccount_Acquirer__c = parent.bkrAccount_Acquirer__c;
                    a.bkrAccount_Are_we_Top_3__c = parent.bkrAccount_Are_we_Top_3__c;
                    a.bkrAccount_Value_Added_Service__c = parent.bkrAccount_Value_Added_Service__c;
                    a.bkrAccount_Training_and_Education__c = parent.bkrAccount_Training_and_Education__c;
                    a.bkrAccount_Marketing_and_Sales_Support__c = parent.bkrAccount_Marketing_and_Sales_Support__c;
                    a.bkrAccount_Financial_Partnership__c = parent.bkrAccount_Financial_Partnership__c;
                    a.bkrAccount_Service_Quality__c = parent.bkrAccount_Service_Quality__c;
                    a.bkrAccount_Ease_of_doing_Business__c = parent.bkrAccount_Ease_of_doing_Business__c;
                    a.bkrAccount_Compensation__c = parent.bkrAccount_Compensation__c;
                    a.bkrAccount_Value_to_Customer__c = parent.bkrAccount_Value_to_Customer__c;
                }
            }
        }
    }
        
        /**
    * assignTasksToParentRecordOwner
    * @param	records		Accounts that were inserted/updated
    * @param	oldRecords	Previous state of the accounts that were inserted/updated mapped by id
    *
    * Assign a task to the Parent Account records Owner when the Parent-Child relationship on the brokerage changes.
    * Only create notification if the parent's owner hasnt been notified previously
    */
        /*private void assignTasksToParentRecordOwner(List<Account> records, Map<Id, Account> oldRecords) {
    // BULKIFY: Unique parent account ids - so that we're not creating duplicate tasks for same parent account
    Set<Id> notifiedParentIds = new Set<Id>();
    
    for (Account a : records) {
    // If it has a parent
    if (a.bkrMasterBrokerageReference__c != null) {
    // if the parent hasnt been notified
    if (!notifiedParentIds.contains(a.bkrMasterBrokerageReference__c)) {
    // old state of the updated account
    Account old = oldRecords != null && oldRecords.containsKey(a.Id) ? oldRecords.get(a.Id) : null;
    Boolean hasNewParent = True; // if its insert, then its a new parent by default
    if (old != null) {
    // Check if previous parent is the same than the current parent (If its changed)
    if (a.bkrMasterBrokerageReference__c == old.bkrMasterBrokerageReference__c) {
    hasNewParent = False;
    }
    }
    
    if (hasNewParent) {
    notifiedParentIds.add(a.bkrMasterBrokerageReference__c); // notify parent
    }
    }
    }
    }
    
    // BULKIFY: Get all parent account objects
    List<Account> parents = [select Id, Name, OwnerId, bkrMasterBrokerageReference__c from Account where id in :notifiedParentIds];
    
    // BULKIFY: List of tasks that need to be inserted at the end of the trigger
    List<Task> tasksToCreate = new List<Task>();
    
    for (Account p : parents) {
    // Create task for parent's owner
    Task t = new Task();
    t.OwnerId = p.OwnerId;
    t.Type = 'Follow-up Task or Deliverable';
    t.ActivityDate = Date.today().addDays(7);
    t.Priority = 'Medium';
    t.Status = 'Open';
    t.WhatId = p.Id;
    if (UserInfo.getLanguage() == 'fr' || UserInfo.getLanguage() == 'fr_CA') {
    t.Subject = 'Revoir les details sur le segment de courtiers';
    } else {
    t.Subject = 'Review the Broker Segmentation Details';
    }
    //t.Description = 'Comments here';
    
    tasksToCreate.add(t);
    }
    
    if (tasksToCreate.size() > 0) {
    insert tasksToCreate;
    }
    }*/
        
        /**
    * updateChildWithParentSegmentation
    * @param	records	All the accounts that were updated
    *
    * Update childs with Parent's segmentation.
    */
    public void updateChildWithParentSegmentation(List<Account> records, Map<Id, Account> oldRecords) {
        List<Account> toProcess = new List<Account>();
        // Iterate accounts, only process when the segmentation changes or insert operation
        for (Account a : records) {
            Account old = oldRecords != null && oldRecords.containsKey(a.Id) ? oldRecords.get(a.Id) : null;
            if (old == null) {
                toProcess.add(a); // always process on insert
            } else {
                // only process if one of these field changes on update
                if (a.bkrAccount_Broker_Segment__c != old.bkrAccount_Broker_Segment__c ||
                    a.bkrAccount_Estimated_Size__c != old.bkrAccount_Estimated_Size__c ||
                    a.bkrAccount_Estimated_Avg_Growth_L3_years__c != old.bkrAccount_Estimated_Avg_Growth_L3_years__c ||
                    a.bkrAccount_Footprint__c != old.bkrAccount_Footprint__c ||
                    a.bkrAccount_Channel__c != old.bkrAccount_Channel__c ||
                    a.bkrAccount_Estimated_RSA_Share_of_Wallet__c != old.bkrAccount_Estimated_RSA_Share_of_Wallet__c ||
                    a.bkrAccount_Fin_Interest_From_Competitor__c != old.bkrAccount_Fin_Interest_From_Competitor__c ||
                    a.bkrAccount_Acquirer__c != old.bkrAccount_Acquirer__c ||
                    a.bkrAccount_Are_we_Top_3__c != old.bkrAccount_Are_we_Top_3__c ||
                    a.bkrAccount_Value_Added_Service__c != old.bkrAccount_Value_Added_Service__c ||
                    a.bkrAccount_Training_and_Education__c != old.bkrAccount_Training_and_Education__c ||
                    a.bkrAccount_Marketing_and_Sales_Support__c != old.bkrAccount_Marketing_and_Sales_Support__c ||
                    a.bkrAccount_Financial_Partnership__c != old.bkrAccount_Financial_Partnership__c) {
                    } {
                        toProcess.add(a);
                    }
            }
        }
        
        if (toProcess.size() > 0) {
            // BULKIFY: Get all the childs and map it by parent id
            Map<Id, List<Account>> childAccountsByParentId = new Map<Id, List<Account>>();
            List<Account> childAccounts = [select Id,
                                           bkrMasterBrokerageReference__c,
                                           bkrAccount_Broker_Segment__c,
                                           bkrAccount_Estimated_Size__c,
                                           bkrAccount_Estimated_Avg_Growth_L3_years__c,
                                           bkrAccount_Footprint__c,
                                           bkrAccount_Channel__c,
                                           bkrAccount_Estimated_RSA_Share_of_Wallet__c,
                                           bkrAccount_Fin_Interest_From_Competitor__c,
                                           bkrAccount_Acquirer__c,
                                           bkrAccount_Are_we_Top_3__c,
                                           bkrAccount_Value_Added_Service__c,
                                           bkrAccount_Training_and_Education__c,
                                           bkrAccount_Marketing_and_Sales_Support__c,
                                           bkrAccount_Financial_Partnership__c,
                                           bkrAccount_Service_Quality__c,
                                           bkrAccount_Ease_of_doing_Business__c,
                                           bkrAccount_Compensation__c,
                                           bkrAccount_Value_to_Customer__c
                                           from Account where bkrMasterBrokerageReference__c in :toProcess];
            for (Account child : childAccounts) {
                List<Account> childs = childAccountsByParentId.containsKey(child.bkrMasterBrokerageReference__c) ?
                    childAccountsByParentId.get(child.bkrMasterBrokerageReference__c) :
                new List<Account>();
                childs.add(child);
                childAccountsByParentId.put(child.bkrMasterBrokerageReference__c, childs);
            }
            
            // BULKIFY: List of accounts that need to be updated at the end of the trigger
            List<Account> accountsToUpdate = new List<Account>();
            
            for (Account a : toProcess) {                
                // If it has childs then update the childs with parent segmentation
                if (childAccountsByParentId.containsKey(a.Id)) {                    
                    List<Account> childs = childAccountsByParentId.get(a.Id);
                    if (childs.size() > 0) {
                        for (Account child : childs) {
                            child.bkrAccount_Broker_Segment__c = a.bkrAccount_Broker_Segment__c;
                            child.bkrAccount_Estimated_Size__c = a.bkrAccount_Estimated_Size__c;
                            child.bkrAccount_Estimated_Avg_Growth_L3_years__c = a.bkrAccount_Estimated_Avg_Growth_L3_years__c;
                            child.bkrAccount_Footprint__c = a.bkrAccount_Footprint__c;
                            child.bkrAccount_Channel__c = a.bkrAccount_Channel__c;
                            child.bkrAccount_Estimated_RSA_Share_of_Wallet__c = a.bkrAccount_Estimated_RSA_Share_of_Wallet__c;
                            child.bkrAccount_Fin_Interest_From_Competitor__c = a.bkrAccount_Fin_Interest_From_Competitor__c;
                            child.bkrAccount_Acquirer__c = a.bkrAccount_Acquirer__c;
                            child.bkrAccount_Are_we_Top_3__c = a.bkrAccount_Are_we_Top_3__c;
                            child.bkrAccount_Value_Added_Service__c = a.bkrAccount_Value_Added_Service__c;
                            child.bkrAccount_Training_and_Education__c = a.bkrAccount_Training_and_Education__c;
                            child.bkrAccount_Marketing_and_Sales_Support__c = a.bkrAccount_Marketing_and_Sales_Support__c;
                            child.bkrAccount_Financial_Partnership__c = a.bkrAccount_Financial_Partnership__c;
                            child.bkrAccount_Service_Quality__c = a.bkrAccount_Service_Quality__c;
                            child.bkrAccount_Ease_of_doing_Business__c = a.bkrAccount_Ease_of_doing_Business__c;
                            child.bkrAccount_Compensation__c = a.bkrAccount_Compensation__c;
                            child.bkrAccount_Value_to_Customer__c = a.bkrAccount_Value_to_Customer__c;
                            
                            accountsToUpdate.add(child);
                        }
                    }
                }
            }
            
            if (accountsToUpdate.size() > 0) {
                update accountsToUpdate;
            }
        }
    }
}