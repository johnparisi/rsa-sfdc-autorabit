/**
* @File Name    :   OpportunityDomain
* @Description  :   Opportunity Domain Layer
* @Date Created :   02/02/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Domain
* @Modification Log:
**************************************************************************************
* Ver       Date        Author           Modification
* 1.0       02/02/2017  Habiba Zaman  Created the file/class
 * 
 * The object domain is where the real work happens, this is where we will apply the actual business logic. 
 * We have the main Object Domain which should handle all common processing. 
 * Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
 * Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
 * just implement logic in the appopriate method. 
 */
public with sharing class OpportunityDomain {
    
    private OpportunityService service = new OpportunityService();
    
    public void beforeInsert(List<Opportunity> records){
    }

    public void beforeUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
    }

    public void afterInsert(List<Opportunity> records){
    }

    public void afterUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
    }

    //Opportunities for GSL Renewals
    public class GSLRenewalDomain{
    private OpportunityService service = new OpportunityService();
        public void beforeInsert(List<Opportunity> records){
            service.updateDecileQuartile(records);
        }

        public void beforeUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
            service.updateDecileQuartile(records);
        }

        public void afterInsert(List<Opportunity> records){
            service.addOpportunityTeamMembers(records);
        }

        public void afterUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
            service.updateRenewalOpportunityAfterUpdate(records, oldRecords);
        }
    }

    //Opportunities for GSL New Business
    public class GSLNewBusinessDomain{
    private OpportunityService service = new OpportunityService();

        public void afterUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
            service.updateNewBusinessOpportunityAfterUpdate(records, oldRecords);

        }
    }

    //Opportunities for Specialty Line
    public class SpecialtyLineDomain{
    private OpportunityService service = new OpportunityService();

        public void beforeUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
            service.validateSpecialtyLineStageChange(records, oldRecords);

        }
    }
}