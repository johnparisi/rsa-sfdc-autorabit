//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

@IsTest
public class DNBMatch_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'		\"ResponseCode\": \"OK\",'+
		'		\"DNBResponse\" : '+
		'								'+
		'    {\"GetCleanseMatchResponse\": {'+
		'     \"TransactionDetail\":    {'+
		'      \"ServiceTransactionID\": \"Id-7b446fd951de8345064b3c0a-1\",'+
		'      \"TransactionTimestamp\": \"2013-07-11T06:04:54\"'+
		'     }'+
		'		}'+
		'		}'+
		''+
		'}';
		DNBMatch obj = DNBMatch.parse(json);
		System.assert(obj != null);
	}
}