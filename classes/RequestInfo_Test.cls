/**
 * Author: James Lee
 * Created At: November 5, 2016
 * 
 * Unit tests for RequestInfo.
 */
@isTest
public class RequestInfo_Test
{
	@testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    static testMethod void testServiceType()
    {
        Map<String, String> csm = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbService');
        Map<String, String> csmReverse = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbService');
        
        for (RequestInfo.ServiceType st : RequestInfo.ServiceType.values())
        {
            RequestInfo ri = new RequestInfo(st, 'Standard', 'Construction, Erection, Installation');
            System.assertEquals(ri.svcType, RequestInfo.getServiceType(csmReverse.get(ri.getServiceType())));
        }
        
        System.assertEquals(null, RequestInfo.getServiceType('INVALID_SERVICE'));
    }
    
    static testMethod void testFlavorType()
    {
        Map<String, String> csm = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbFlavor');
        
        for (String s : csm.keySet())
        {
            RequestInfo ri = new RequestInfo(RequestInfo.ServiceType.GET_QUOTE, s, 'Construction, Erection, Installation');
            System.assertEquals(csm.get(s), ri.getPackageType());
        }
    }
    
    static testMethod void testSegmentType()
    {
        Map<String, String> csm = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
        
        for (String s : csm.keySet())
        {
            RequestInfo ri = new RequestInfo(RequestInfo.ServiceType.GET_QUOTE, 'Standard', s);
            System.assertEquals(csm.get(s), ri.getSegmentType());
        }
    }
}