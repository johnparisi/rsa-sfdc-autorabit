global class SMEQ_ConfirmBrokerHandover
{
    @testVisible
    private static String RESULT_BROKER_YET_TO_HANDOVER = 'Broker has not clicked on the handover button yet.';
    @testVisible
    private static String RESULT_UNDERWRITER_ALREADY_CONFIRMED = 'You have already confirmed the handover from the broker.';
    
	webservice static String assignOwner(String quoteId)
    {
        Quote__c q = [
            SELECT id, case__c, Quote_application_handed_over_to_UW__c
            FROM Quote__c
            WHERE id = :quoteId
            LIMIT 1
        ];
        
        if (!q.Quote_application_handed_over_to_UW__c)
        {
            return RESULT_BROKER_YET_TO_HANDOVER;
        }
        
        Case c = [
            SELECT id, OwnerId
            FROM Case
            WHERE id = :q.case__c
            LIMIT 1
        ];
        
        if (c.OwnerId != UserInfo.getUserId())
        {
            c.OwnerId = UserInfo.getUserId();
            update c;
            return '';
        }
        else
        {
            return RESULT_UNDERWRITER_ALREADY_CONFIRMED;
        }
    }
}