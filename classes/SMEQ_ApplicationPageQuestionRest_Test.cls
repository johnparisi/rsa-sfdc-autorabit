@isTest
public class SMEQ_ApplicationPageQuestionRest_Test {
    
     /***
     * Method to test the response that gathers SIC Questions for a given Sic Code 
     */ 
    @isTest
    static void testgetApplicationQuestionBySic()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            SIC_Code_Detail_Version__c scdvs = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
            SMEQ_TestDataGenerator.generateSICQuestions(scdvs);
                        
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse(); 
            req.addParameter('sicCode', sicCode.SIC_Code__c);
            RestContext.request = req;
            RestContext.response = res;
            
            SMEQ_ApplicationPageQuestionRest.SMEQ_ApplicationQuestionResponseModel response = SMEQ_ApplicationPageQuestionRest.getApplicationQuestionBySic();
            System.assert(response!=null);
            System.assert(response.getResponseCode()!=null);
            System.assertEquals('OK', response.getResponseCode());
            system.debug('+++++ ' + response);
            system.debug('+++++ ' + response.payload);
        }
    }
    /***
     * Method to test the response if no sic code is given
     * Should return an error
     */ 
    @isTest
    static void testNoSicCode()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            SIC_Code_Detail_Version__c scdvs = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            SMEQ_TestDataGenerator.generateSICQuestions(scdvs);
                        
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            RestContext.request = req;
            RestContext.response = res;
            
            SMEQ_ApplicationPageQuestionRest.SMEQ_ApplicationQuestionResponseModel response = SMEQ_ApplicationPageQuestionRest.getApplicationQuestionBySic();
            System.assert(response!=null);
            System.assert(response.getResponseCode()!=null);
            System.assertEquals('ERROR', response.getResponseCode());
        }
    }
}