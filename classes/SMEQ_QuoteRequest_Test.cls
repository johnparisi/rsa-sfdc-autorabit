@isTest

Public class SMEQ_QuoteRequest_Test{

    static testmethod void requestTest(){
    SMEQ_QuoteRequest qr = new SMEQ_QuoteRequest();
    qr.referenceNumber = '123';
    qr.language = 'En';
    qr.policyNumber = 'Q123';
    qr.brokerID = 'B01';
    qr.agentID = 'a01';
    qr.caseId = 'c01';
    qr.quoteID = 'q01';
    qr.insuredClientID = 'i01';
    qr.liveChatRequired = true;
    qr.kickOutRequired = true;
    qr.liveChatTriggerReasons = 'test reason';
    qr.kickoutTriggerReasons = new List<String>{'Reason1', 'Reason2'};
    qr.affirmativeConditionAnsweredYes = true;
    qr.finalizedDate = '01/01/18';
    qr.quoteNumber = 'q123';
    qr.selectedPackageID = 'Option 1';
    qr.selectedAnnualPremium = 10;
    qr.selectedAirMilesEarned = 150;
    qr.firstLocationAddrSameAsBusAddr = true;
    qr.performedDandBSearch = true;
    qr.hasDAndBMatch = true;
    qr.coverages = new List<SMEQ_CoverageDetailsModel>();
    qr.businessDetails = new SMEQ_BusinessDetailsModel();
    qr.packagesRequireRating = new List<SMEQ_QuotePackage>();
    qr.nameInsured = 'Tom Jones';
    qr.policyEffectiveDate = '01/05/18';
    qr.policyPaymentplan = 'ppp';
    qr.policyBusinessDescription = 'pbdesc';
    qr.isAdditionalInsured = true;
    qr.isLossPayee = false;
    qr.policyNotes = 'policyNotes';
    qr.issuedDate = '01/01/18';
    qr.isBindLocationAddrSameAsMailAddr = true;
    qr.caseNumber = 'c123';
    qr.liability = new List<SMEQ_QuoteRequest.Liability>();
    qr.expiryDate = '01/01/19';
    qr.caseStatus = 'In Progress';
    qr.isPolicyActive = true;
    qr.ePolicyTransactionType = 'Endorse';
    qr.ePolicyTransactionStatus = 'Pending';
    qr.newAnnualPremium = '10';
    qr.premium = '10';
    qr.newProratedPremium = '12';
    qr.isLocked = 'Yes';
    qr.lastTransaction = '01/01/18';
    qr.amendmentEffectiveDate = '01/06/18';
    qr.reasonCodes = '0102';
    qr.policyHistories = new List<String>();
    qr.isCondo = false;
    qr.additionalInsureds = new List<String>();
    // qr.bindMailingAddress = new SMEQ_QuoteRequest.BindMailingAddress(); 
    qr.isRenewalAmendmentFlow = true;
    qr.isHUB = false;
    /*
    public String quoteNumber {public get; public set;}
    public String selectedPackageID {public get; public set;}
    public Integer selectedAnnualPremium{public get; public set;}
    public Integer selectedAirMilesEarned{public get; public set;}
    public Boolean firstLocationAddrSameAsBusAddr {public get; public set;}
    public Boolean performedDandBSearch {public get; public set;}
    public Boolean hasDAndBMatch {public get; public set;}
    public List<SMEQ_CoverageDetailsModel> coverages {public get; public set;}
    public SMEQ_BusinessDetailsModel businessDetails {public get; public set;}
    public List<SMEQ_QuotePackage> packagesRequireRating {public get; public set;}
    public String nameInsured {public get; public set;}
    public String policyEffectiveDate {public get; public set;}
    public String policyPaymentplan {public get; public set;}
    public String policyBusinessDescription {public get; public set;}
    public Boolean isAdditionalInsured {public get; public set;}
    public Boolean isLossPayee {public get; public set;}
    public String policyNotes {public get; public set;}
    public String issuedDate {public get; public set;}
    public Boolean isBindLocationAddrSameAsMailAddr {public get; public set;}
    public String caseNumber {public get; public set;}
    public List<Liability> liability {public get; public set;}
    public String expiryDate {public get; public set;} 
    public String caseStatus {public get; public set;} 
    public Boolean isPolicyActive {public get; public set;} 
    public String ePolicyTransactionType {public get; public set;} 
    public String ePolicyTransactionStatus {public get; public set;}  
    public String newAnnualPremium {public get; public set;} 
    public String premium {public get; public set;} 
    public String newProratedPremium {public get; public set;} 
    public String isLocked {public get; public set;} 
    public String lastTransaction {public get; public set;} 
    public String amendmentEffectiveDate {public get; public set;} 
    public String reasonCodes {public get; public set;} 
    public List<String> policyHistories {public get; public set;} 
    public Boolean isCondo {public get; public set;}
    public List<String> additionalInsureds {public get; public set;} // Added to match angular data model
    public BillingInformation billingInformation {public get; public set;} // Added to match angular data model
    public BindMailingAddress bindMailingAddress {public get; public set;} // Added to match angular data model
    public Boolean isRenewalAmendmentFlow {public get; public set;} // Added to match angular data model
    public Boolean isHUB {public get; public set;} // Added to match angular data model    
    
    qr.billingInformation= new SMEQ_QuoteRequest.BillingInformation();
    */
    
    }
}