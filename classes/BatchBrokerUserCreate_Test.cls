/**
* @File Name    :   SMEQ_BrokerUserCreateService_Test
* @Description  :   Batch Broker User Creation Service test class
* 					
* @Date Created :   10/08/2017
* @Author       :   Habiba Zaman @ Deloitte [hzaman@deloitte.ca]
* @group        :   Test
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       06/09/2017  Habiba Zaman  	Created the file/class
*/
@isTest
private class BatchBrokerUserCreate_Test {
	
	// CRON expression: midnight on March 15.
	// Because this is a test, job executes
	// immediately after Test.stopTest().
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	
	public static final String BROKER_ACCOUNT_RECORD_TYPE = 'bkrAccount_RecordTypeBkrMGML';
    public static final String CONTACT_RECORD_TYPE = 'bkrContact_BrokerRecordType';

	@isTest static void BatchBrokerUserCreation() {
        //Custom setting
    

		User portalAccountOwner1 = UserService_Test.getNonPortalUserWithRole();
		System.runAs(portalAccountOwner1){
            SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = smq.accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '1000770001',
				OwnerId = portalAccountOwner1.Id
			);
			Database.insert(portalAccount1);
			portalAccount1.IsPartner = true;
			Database.update(portalAccount1);

			//Create Contact
			Contact theContact = new Contact(
    			FirstName = 'Test',
			    Lastname = 'Contact',
			    AccountId = portalAccount1.Id,
                RecordTypeId = smq.contactTypes.get(CONTACT_RECORD_TYPE),
			    Email = 'test@testingclass.com',
			    EP_Reference_Number__c = '1000000001'
			);

			Insert(theContact);

			//Create Broker Stage data
			Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();

			//Create User Stage object
			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='testusername@testin.com',
				Federation_ID__c = 'XPUWEY1',
                EP_Reference_Number__c = '1000000001',
				Broker_Account__c = portalAccount1.Id,
				bkrAccount_HuonBrokerNumber__c='1000770001',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);

			test.startTest();
		
			BatchBrokerUserCreate bd = new BatchBrokerUserCreate();
			database.executebatch(bd);

			test.stopTest();

			User_Staging__c usResult = new User_Staging__c();

			List<User> createdUser = [SELECT Username, Contact.Id 
										FROM User 
										WHERE Contact.Id =: theContact.Id];
			User u = new User();
			if(createdUser.size() > 0){
				u = createdUser.get(0);
			}

			System.assertEquals(us.Broker_Username__c  , u.Username);

		}


    }
 
    @isTest static void runSchedule(){
		Test.startTest();
			//Run job
			String jobId = System.schedule('ScheduleApexClassTest_BrokerUserCreation', CRON_EXP, new BatchBrokerUserCreateSchedule());

		Test.stopTest();
	}
	
}