@isTest
public class SMEQ_BrokerMessageRestService_Test
{
        /**
         * Method to test save broker message rest service with valid case ID
         * 1. Assert response is not null
         * 2. Assert success message is set on response
         * 3. Assert response code is ok.
         * 
         */ 
        @isTest static void testSaveBrokerMessageWithValidCaseIDAndValidComment()
        {
            Case caseToComment = new Case();

            Map<String, Id> CaseTypes = Utils.GetRecordTypeIdsByDeveloperName(Case.SObjectType, true);
            caseToComment.recordTypeId = caseTypes.get('bkrCase_RecordType_Broker_Connect');
            caseToComment.subject = '';
            insert caseToComment;

            caseToComment = [
                SELECT id, caseNumber
                FROM Case
                WHERE id = :caseToComment.Id
            ];

            SMEQ_BrokerMessageRequestModel brokerMsgRequest = buildBrokerMessageRequest(caseToComment.caseNumber, 'NB');
            SMEQ_BrokerMessageResponseModel brokerMsgResponse = SMEQ_BrokerMessageRestService.saveBrokerMessage(brokerMsgRequest);

            System.assert(brokerMsgResponse!=null);
            System.assert(brokerMsgResponse.successMessage=='SUCCESS');
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, brokerMsgResponse.responseCode);
        }
    
    /**
     * Method to test save broker message rest service with invalid case ID
     * 1. Assert response is not null
     * 2. Assert success message is set on response
     * 
     * 
     */
    @isTest static void testSaveBrokerMessageWithInValidCaseNumberAndValidComment()
    {
        User u;
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        System.runAs(u)
        {
            SMEQ_BrokerMessageRequestModel brokerMsgRequest = new SMEQ_BrokerMessageRequestModel();
            brokerMsgRequest = buildBrokerMessageRequest(null, 'AM');
            SMEQ_BrokerMessageResponseModel brokerMsgResponse = SMEQ_BrokerMessageRestService.saveBrokerMessage(brokerMsgRequest);

            System.assert(brokerMsgResponse != null);
            System.assert(brokerMsgResponse.successMessage == 'SUCCESS');
        }
    }


    /**
         * Method to test save broker message rest service with invalid case ID and invalid message
         * 1. Assert response is not null
         * 2. Assert success message is set on response
         * 
         * 
         */
        @isTest static void testSaveBrokerMessageWithInvalidCaseNumberAndInvalidComment() {
        
       
        SMEQ_BrokerMessageRequestModel  brokerMsgRequest = new  SMEQ_BrokerMessageRequestModel();
        brokerMsgRequest  = buildBrokerMessageRequest(null, 'RE');
        brokerMsgRequest.comment = null;
        SMEQ_BrokerMessageResponseModel brokerMsgResponse = SMEQ_BrokerMessageRestService.saveBrokerMessage(brokerMsgRequest);
        
        System.assert(brokerMsgResponse!=null);
        System.assert(brokerMsgResponse.successMessage=='ERROR');
    }

   
    //build mock broker request
    public static SMEQ_BrokerMessageRequestModel buildBrokerMessageRequest(String caseNumber, String bundle)
    {
        SMEQ_BrokerMessageRequestModel brokerMessageReq = new SMEQ_BrokerMessageRequestModel();
        brokerMessageReq.email = 'test@testmail.com';
        brokerMessageReq.extension = '123';
        brokerMessageReq.brokerID = '008998989';
        brokerMessageReq.caseNumber = caseNumber;
        brokerMessageReq.comment = 'test message to UnderWriter';
        brokerMessageReq.bundle = bundle;
        return brokerMessageReq;
    }
}