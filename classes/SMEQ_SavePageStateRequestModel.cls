global with sharing class SMEQ_SavePageStateRequestModel
{
	public String quoteID{public get; public set;}
	public Integer lastVisitedPageNum{public get; public set;}
}