@isTest
Public class FP_QuoteByIDRestWrapperService_Test{

    static testmethod void FP_QuoteByIDRest(){
        case c = new case();
        c.status = 'Open';
        c.origin = 'Phone';
        insert c;

        quote__c qut = new quote__c();
        qut.case__c = c.id;
        qut.status__c = 'open';
        insert qut;

        string quoteid = qut.id;

        QuoteRequestWrapper response= FP_QuoteByIDRestWrapperService.getQuoteByID(quoteid);
    }
}