@RestResource(urlMapping='/quoteSearch/*')
global with sharing class SMEQ_QuoteSearchRestService
{

  @HttpPost
    global static SMEQ_QuoteSearchResponseModel searchQuote(SMEQ_QuoteSearchRequestModel quoteSearchRequest){
        RestResponse res = RestContext.response;
        String msgResponse = '';
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Quote Search Service  >>>>>>>' + quoteSearchRequest);
        }


        SMEQ_QuoteSearchResponseModel response = new SMEQ_QuoteSearchResponseModel();
        SMEQ_QuoteSearchDao searchDAO = new SMEQ_QuoteSearchDao ();

        try{
            //response = getMockResponse();
            response = searchDAO.searchQuote(quoteSearchRequest);


        }
        catch(Exception se){
             System.debug('SMEQ_ServiceException Caught!!');
          response.responseCode = SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR;
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                        = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_600, 'Unexpected System Exception');
            errors.add(error);
            response.setErrors(errors);

        }

        return response;
    }

    public static SMEQ_QuoteSearchResponseModel getMockResponse(){

            SMEQ_QuoteSearchResponseModel responseModel = new SMEQ_QuoteSearchResponseModel();
            SMEQ_QuoteSearchResponseModel.SearchResult searchResult1 = new     SMEQ_QuoteSearchResponseModel.SearchResult();
            SMEQ_QuoteSearchResponseModel.SearchResult searchResult2 = new     SMEQ_QuoteSearchResponseModel.SearchResult();
            List<SMEQ_QuoteSearchResponseModel.SearchResult> searchResults = new     List<SMEQ_QuoteSearchResponseModel.SearchResult>();
            searchResult1.quoteStatus = '1';
            searchResult1.caseId = 'MOCK-referenceNumber';
            searchResult1.quoteNumber = 'MOCK-quoteID';
            searchResult1.businessName = 'MOCK-businessName';
            searchResult1.dateCreated = '2016-01-01';
            searchResult1.quoteExpiryDate = '2020-01-01';
            searchResult2.quoteStatus = '2';
            searchResult2.caseId = 'MOCK-referenceNumber2';
            searchResult2.quoteNumber = 'MOCK-quoteID2';
            searchResult2.businessName = 'MOCK-businessName2';
            searchResult2.dateCreated = '2016-02-01';
            searchResult2.quoteExpiryDate = '2020-02-01';
            searchResults.add(searchResult1);
            searchResults.add(searchResult2);
            responseModel.payload = searchResults;
            return responseModel;
    }

    
}