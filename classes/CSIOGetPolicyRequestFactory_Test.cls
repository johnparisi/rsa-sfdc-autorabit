@isTest
public class CSIOGetPolicyRequestFactory_Test
{
   
    static testMethod void test1()
    {
         
         
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            
            Case c = [Select id,CaseNumber from Case limit 1];
            Quote__c q =  [SELECT id, Package_Type_Selected__c, Case__r.Segment__c FROM Quote__c LIMIT 1];
             Session_Information__c session = new Session_Information__c(session_id__c = '123',Case__c = c.id, User__c = UserInfo.getUserId());
            insert session;
        
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riGetQuote, q.Id);
            String request = res.getRequestXML();
            SOQLDataSet sds = new SOQLDataSet(q.Id, riGetQuote );
            try{
            CSIOGetPolicyRequestFactory objCSIOGet1 = new CSIOGetPolicyRequestFactory(c.CaseNumber,'test',riGetQuote ,'test','test');
            }catch(exception ex){}
           
        }
        
        
    }
    static testMethod void test2()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            
            Case c = [Select id,CaseNumber from Case limit 1];
            Quote__c q =  [SELECT id, Package_Type_Selected__c, Case__r.Segment__c FROM Quote__c LIMIT 1];
             Session_Information__c session = new Session_Information__c(session_id__c = '123',Case__c = c.id, User__c = UserInfo.getUserId());
            insert session;
        
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riGetQuote, q.Id);
            String request = res.getRequestXML();
            SOQLDataSet sds = new SOQLDataSet(q.Id, riGetQuote );
            try{
            CSIOGetPolicyRequestFactory objCSIOGet1 = new CSIOGetPolicyRequestFactory(sds,riGetQuote );
            objCSIOGet1.buildXMLRequest();
            }catch(exception ex){}
          }  
    }
    
 }