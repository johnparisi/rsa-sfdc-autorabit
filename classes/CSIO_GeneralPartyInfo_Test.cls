/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_GeneralPartyInfo.
 */
@isTest
public class CSIO_GeneralPartyInfo_Test
{
    static testMethod void testConvertBrokerage()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();        
            Account a = SMEQ_TestDataGenerator.getBrokerageAccount();
            Contact c = SMEQ_TestDataGenerator.getProducerContact(a);
            Case testCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            testCase.accountid = a.id;
            upsert testCase;
            CSIO_GeneralPartyInfo cgpi = new CSIO_GeneralPartyInfo(c);
            String output = '';
            output += cgpi;
            
            system.assertEquals('Ontario',a.BillingState);
            
            System.assertEquals(
                '<GeneralPartyInfo>' +
                '<NameInfo>' +
                ' <CommlName>' +
                '  <CommercialName>' + XMLHelper.toCData(c.FirstName) +' '+ XMLHelper.toCData(c.LastName)+ '</CommercialName>' +
                ' </CommlName>' +
                '</NameInfo>' +
                '</GeneralPartyInfo>',
                output
            );
        }
    }
    
    static testMethod void testConvertInsured()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuotev2();

            Quote__c objQuote = SMEQ_TestDataGenerator.getOpenQuote();

            Risk__c locationRisk = [
                SELECT Id, Building_Value__c
                FROM Risk__c
                WHERE recordtype.developername = :RiskService.RISK_RECORDTYPE_LOCATION
            ];

            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk();
            
            Contact objCon = new contact(lastname ='test',AccountId = a.Id);
            insert objCon;
            
            list<Quote_Risk_Relationship__c> lstQRel = new list<Quote_Risk_Relationship__c>();
            Quote_Risk_Relationship__c quoteRiskRelation = new Quote_Risk_Relationship__c();
            quoteRiskRelation.Account__c = a.Id;
            quoteRiskRelation.Party_Number__c = 1;
            quoteRiskRelation.Party_Type__c = 'Loss Payee';
            quoteRiskRelation.Quote__c = objQuote.Id;
            quoteRiskRelation.Related_Risk__c = locationRisk.Id;
            quoteRiskRelation.Relationship_Type__c ='First Mortgagee';
            insert quoteRiskRelation;
            lstQRel.add(quoteRiskRelation);
            
            //SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            
            
            //SMEQ_TestDataGenerator.initializeSMEMappings();
            CSIO_GeneralPartyInfo cgpi = new CSIO_GeneralPartyInfo(a, r);
            CSIO_GeneralPartyInfo cgpi1 = new CSIO_GeneralPartyInfo(a);
            CSIO_GeneralPartyInfo cgpi2 = new CSIO_GeneralPartyInfo(objCon);
            CSIO_GeneralPartyInfo cgpi3 = new CSIO_GeneralPartyInfo(objCon,r);
            CSIO_GeneralPartyInfo cgpi4 = new CSIO_GeneralPartyInfo(a,quoteRiskRelation);
            CSIO_GeneralPartyInfo cgpi5 = new CSIO_GeneralPartyInfo(objcon,quoteRiskRelation);
            CSIO_GeneralPartyInfo cgpi6 = new CSIO_GeneralPartyInfo(a,lstQRel,null,null);
            
            String output = '';
            output += cgpi;
            system.assertEquals('Ontario', r.Province__c);
            /*
            System.assertEquals(
                '<GeneralPartyInfo>' +
                '<NameInfo>' +
                ' <CommlName>' +
                '  <CommercialName>' + XMLHelper.toCData(a.Name) + '</CommercialName>' +
                ' </CommlName>' +
                '</NameInfo>' +
                new CSIO_Addr(a) +
                '</GeneralPartyInfo>',
                output
            );
            */
        }
    }
}