global class SMEQ_UWCoverageConsoleExt
{
    public Map<String, String> optionOnePickListValues{get; set;}
    public Map<String, String> optionTwoPickListValues{get; set;}
    public Map<String, String> optionPolicyPicklistValues{get; set;}
    public String coveragesSelectedForOptionOne {get; set;}
    public String coveragesSelectedForOptionTwo {get; set;}
    public String coveragesSelectedForPolicy {get; set;}
    public Integer optionOneListSize{get; set;}
    public Integer optionTwoListSize{get; set;}
    public Integer optionPolicyListSize{get; set;}
    public String currentRecordID{get; set;}
    public String currencyFormat {public get; public set;}
    public Set<String> defaultCoverages {get; set;}
    public Set<String> defaultCoveragesFR {get; set;}
    public Set<String> defaultCoverageLabels {get; set;}
    public Boolean FrenchUser {get; set;}
    public Boolean renewalAndAmendment {get; set;}
    public Risk__c currentRecord {get; set;}
    
    public List<Coverages__c> riskCoverages {get; set;}
    public List<Coverages__c> riskCoveragesOption1 {get; set;}
    public List<Coverages__c> riskCoveragesOption2 {get; set;}
    public List<Coverages__c> riskCoveragesPolicy {get; set;}
    
    public Map<String, String> coveragesSelectedForOption1 {get; set;}
    
    private static final String DROPDOWN_UNSELECTED = '';
    private static final String DROPDOWN_SELECTED = 'selected';
     
    
    public SMEQ_UWCoverageConsoleExt(ApexPages.StandardController controller)
    {
        this.currentRecordID = controller.getRecord().Id;
        this.currentRecord = [
            SELECT id, name, quote__r.case__c, related_COM__c, Coverage_Selected_Policy__c, quote__r.case__r.bkrCase_SIC_Code__c
            FROM risk__c
            WHERE id = :this.currentRecordId
        ];
        
        // This section retrieves all default coverages.
        if(this.currentRecord.quote__r.case__r.bkrCase_SIC_Code__c != NULL){
            this.defaultCoverageLabels = CoverageService.getDefaultCoverageLabels(currentRecord.quote__r.case__c);
            this.defaultCoverageLabels = CoverageService.removeNonLocationCoverages(this.defaultCoverageLabels);
        }
        else{ // Added when there is no SIC Code associated to the case
            this.defaultCoverageLabels = getDefaultCoveragesWithoutSICCode();
        }
        Set<id> renewalAndAmendmentRisks = RiskService.isRenewalAndAmendment(new List<Risk__c>{this.currentRecord});
        
        this.renewalAndAmendment = renewalAndAmendmentRisks.contains(currentRecord.id);
        // This section retrieves coverage types selected for each package.
        if(this.renewalAndAmendment){
            this.optionPolicyPicklistValues = getPickListValuesForPolicy(currentRecordId); 
        } else {
			this.optionOnePickListValues = getPickListValuesForOption1(currentRecordId);
        	this.optionTwoPickListValues = getPickListValuesForOption2(currentRecordId);  
        }
        
        // This section retrieves the coverage record details for each package.
        this.riskCoverages = [
            SELECT id, name, Deductible_Value__c, Limit_Value__c, toLabel(Package_ID__c), Selected__c, toLabel(Type__c)
            FROM Coverages__c
            WHERE Risk__c = :currentRecord.Id
            ORDER BY Selected__c DESC, Package_ID__c ASC, name ASC
        ];
        
        this.riskCoveragesOption1 = new List<Coverages__c>();
        this.riskCoveragesOption2 = new List<Coverages__c>();
        this.riskCoveragesPolicy = new List<Coverages__c>();
        for (Coverages__c c : this.riskCoverages)
        {
            if(this.renewalAndAmendment){
                riskCoveragesPolicy.add(c);
            }
            else {
                if (c.Package_ID__c == RiskService.PACKAGE_OPTION_1) {
                    riskCoveragesOption1.add(c);
                }
                else if (c.Package_ID__c == RiskService.PACKAGE_OPTION_2)
                {
                    riskCoveragesOption2.add(c);
                }
            }
                
        }
        
        // This section manages the currency output format.
        if (UserService.getFormattedUserLanguage() == UserService.LANGUAGE_FRENCH)
        {
            this.FrenchUser = true;
            this.currencyFormat = '{0, number, 0¤}';
        }
        else
        {
            this.FrenchUser = false;
            this.currencyFormat = '{0, number, currency}';
        }
    }
    
    /**
     * Retrieve the map used for the coverage picklist in the coverage console.
     * 
     * @param riskID: The riskID to retrieve coverages from.
     * @return: A map of coverage types to whether they have been selected for the Option 1 package.
     */
    @RemoteAction
    global static Map<String, String> getPickListValuesForOption1(String riskID)
    {
        Risk__c currentRisk = [
            SELECT id, name, quote__r.case__c, Coverage_Selected_Option_1__c, quote__r.case__r.bkrCase_SIC_Code__c
            FROM risk__c
            WHERE id = :riskID
        ];
        
        Map<String, String> option1Values = new Map<String, String>();
        Map<String, String> toLabel = CoverageService.getCoverageLabel();

        Set<String> defaultCoverages; Set<String> addonCoverages;
        if(currentRisk.quote__r.case__r.bkrCase_SIC_Code__c != NULL){
            defaultCoverages = CoverageService.getDefaultCoverages(currentRisk.quote__r.case__c);
            defaultCoverages = CoverageService.removeNonLocationCoverages(defaultCoverages);
            addonCoverages = CoverageService.getAddOnCoverages(currentRisk.quote__r.case__c);
            addonCoverages = CoverageService.removeNonLocationCoverages(addonCoverages);
        }
        else{ // Added when there is no SIC Code associated to the case
            defaultCoverages = getDefaultCoveragesWithoutSICCode();
            addonCoverages = getAddonCoveragesWithoutSICCode();
        }
        for (String addon : addonCoverages)
        {
            if (!defaultCoverages.contains(addon))
            {
                option1Values.put(toLabel.get(addon), DROPDOWN_UNSELECTED);
            }
        }
        
        List<String> currentlySelectedOptionOne = new List<String>();
        if(currentRisk.Coverage_Selected_Option_1__c != null){
        	currentlySelectedOptionOne = currentRisk.Coverage_Selected_Option_1__c.split(';');  
        }
        
        
        for (String opt : currentlySelectedOptionOne)
        {
            for (String addon : addonCoverages)
            {
                if (addon == opt &&
                    !defaultCoverages.contains(addon))
                {
                    option1Values.put(toLabel.get(addon), DROPDOWN_SELECTED);
                }
            }
        }
        return option1Values;
    }
    
    /**
     * Retrieve the map used for the coverage picklist in the coverage console.
     * 
     * @param riskID: The riskID to retrieve coverages from.
     * @return: A map of coverage types to whether they have been selected for the Option 2 package.
     */
    @RemoteAction
    global static Map<String, String> getPickListValuesForOption2(String riskID)
    {
        Risk__c currentRisk = [
            SELECT id, name, quote__r.case__c, Coverage_Selected_Option_2__c, quote__r.case__r.bkrCase_SIC_Code__c
            FROM risk__c
            WHERE id = :riskID
        ];
        
        Map<String, String> option2Values = new Map<String, String>();
        Map<String, String> toLabel = CoverageService.getCoverageLabel();

        Set<String> defaultCoverages; Set<String> addonCoverages;
        if(currentRisk.quote__r.case__r.bkrCase_SIC_Code__c != NULL){
            defaultCoverages = CoverageService.getDefaultCoverages(currentRisk.quote__r.case__c);
            defaultCoverages = CoverageService.removeNonLocationCoverages(defaultCoverages);
            addonCoverages = CoverageService.getAddOnCoverages(currentRisk.quote__r.case__c);
            addonCoverages = CoverageService.removeNonLocationCoverages(addonCoverages);
        }
        else{ // Added when there is no SIC Code associated to the case
            defaultCoverages = getDefaultCoveragesWithoutSICCode();
            addonCoverages = getAddonCoveragesWithoutSICCode();
        }
        for (String addon : addonCoverages)
        {
            if (!defaultCoverages.contains(addon))
            {
                option2Values.put(toLabel.get(addon), DROPDOWN_UNSELECTED);
            }
        }
        List<String> currentlySelectedOptionTwo = new List<String>();
        if(currentRisk.Coverage_Selected_Option_2__c != null){
            currentlySelectedOptionTwo = currentRisk.Coverage_Selected_Option_2__c.split(';');
        }
        
        for (String opt : currentlySelectedOptionTwo)
        {
            for (String addon : addonCoverages)
            {
                if (addon == opt &&
                    !defaultCoverages.contains(addon))
                {
                    option2Values.put(toLabel.get(addon), DROPDOWN_SELECTED);
                }
            }
        }
        return option2Values;
    }
    
    /**
     * Retrieve the map used for the coverage picklist in the coverage console.
     * 
     * @param riskID: The riskID to retrieve coverages from.
     * @return: A map of coverage types to whether they have been selected for Policy.
     */
    @RemoteAction
    global static Map<String, String> getPickListValuesForPolicy(String riskID)
    {
        Risk__c currentRisk = [
            SELECT id, name, quote__r.case__c, Coverage_Selected_Policy__c, quote__r.case__r.bkrCase_SIC_Code__c
            FROM risk__c
            WHERE id = :riskID
        ];
        Map<String, String> optionPolicyValues = new Map<String, String>();
        Map<String, String> toLabel = CoverageService.getCoverageLabel();

        Set<String> defaultCoverages; Set<String> addonCoverages;
        if(currentRisk.quote__r.case__r.bkrCase_SIC_Code__c != NULL){
            defaultCoverages = CoverageService.getDefaultCoverages(currentRisk.quote__r.case__c);
            defaultCoverages = CoverageService.removeNonLocationCoverages(defaultCoverages);
            addonCoverages = CoverageService.getAddOnCoverages(currentRisk.quote__r.case__c);
            addonCoverages = CoverageService.removeNonLocationCoverages(addonCoverages);
        }
        else{ // Added when there is no SIC Code associated to the case
            defaultCoverages = getDefaultCoveragesWithoutSICCode();
            addonCoverages = getAddonCoveragesWithoutSICCode();
        }
        List<String> currentlySelectedPolicy = new List<String>();
        if( currentRisk.Coverage_Selected_Policy__c != null){
             currentlySelectedPolicy = currentRisk.Coverage_Selected_Policy__c.split(';');
    
        }
            
        Set<String> allCoverages = new Set<String>();
        allCoverages.addAll(addonCoverages);
        system.debug('add on Coverages' + addonCoverages);
        allCoverages.addAll(defaultCoverages);
        
        for (String addon : allCoverages)
        {
            optionPolicyValues.put(toLabel.get(addon), DROPDOWN_UNSELECTED);
        }
        for (String opt : currentlySelectedPolicy)
        {
            for (String addon : allCoverages)
            {
                if (addon == opt)
                {
                    optionPolicyValues.put(toLabel.get(addon), DROPDOWN_SELECTED);
                }
            }
        }
        return optionPolicyValues;
    }
    
    /*
     * This helper method provides the option for getting salesforce picklist values for labels.
     */
    private static String getPicklistValueForLabel(String coverageAPIName, String picklistLabel)
    {
        String picklistValue = '';
        String[] currentlySelectedOptionOne = picklistLabel.split(';');
        List<String> options = new List<String>();
        
        List<Schema.PicklistEntry> ple = new List<Schema.PicklistEntry>();
          
        if (coverageAPIName.equals('Coverage_Selected_Option_1__c'))
        {
             Schema.DescribeFieldResult fieldResult = Risk__c.Coverage_Selected_Option_1__c.getDescribe();
             ple = fieldResult.getPicklistValues();
        }
        if (coverageAPIName.equals('Coverage_Selected_Option_2__c'))
        {
            Schema.DescribeFieldResult fieldResult = Risk__c.Coverage_Selected_Option_2__c.getDescribe();
            ple = fieldResult.getPicklistValues();

        }
        else if (coverageAPIName.equals('Coverage_Selected_Policy__c'))
        {
            Schema.DescribeFieldResult fieldResult = Risk__c.Coverage_Selected_Policy__c.getDescribe();
            ple = fieldResult.getPicklistValues();

        }
        for (String opt : currentlySelectedOptionOne)
        {
            for (Schema.PicklistEntry f : ple)
            {
                if(f.getLabel().equals(opt))
                    options.add(f.getValue());
            } 
        }

        for (String opt : options)
        {
            picklistValue += opt +';';
        }

        picklistvalue = picklistValue.removeEnd(';');

        return picklistValue;
    }
    
    public PageReference updateCoverages()
    {
        System.debug('update coverages being called new');
        System.Debug('#### UpdatingCoverages : in coveragesConsole option 1' + this.coveragesSelectedForOptionOne);
        System.Debug('#### UpdatingCoverages : in coveragesConsole option 2' + this.coveragesSelectedForOptionTwo);
        
        if(this.renewalAndAmendment){
            currentRecord.Coverage_Selected_Policy__c = getPicklistValueForLabel('Coverage_Selected_Policy__c',this.coveragesSelectedForPolicy);
        }
        else {
            currentRecord.Coverage_Selected_Option_1__c = getPicklistValueForLabel('Coverage_Selected_Option_1__c',this.coveragesSelectedForOptionOne);
            currentRecord.Coverage_Selected_Option_2__c = getPicklistValueForLabel('Coverage_Selected_Option_2__c',this.coveragesSelectedForOptionTwo);
        }
        
        update currentRecord;
        
        System.debug('### currentRecord ' + currentRecord);
        
        return Apexpages.currentPage();
    }
    
   	public void requeryCoverages()
    {
        this.riskCoverages = [
            SELECT id, name, Deductible_Value__c, Limit_Value__c, toLabel(Package_ID__c), Selected__c, toLabel(Type__c)
            FROM Coverages__c
            WHERE Risk__c = :currentRecord.Id
            ORDER BY Selected__c DESC, Package_ID__c ASC, name ASC
        ];
    }    

    /*
    *   Name        : getDefaultCoveragesWithoutSICCode
    *   @description: This method would return the list of default coverages for the Risk when the case has
    *                 No SIC Code associated with it. This method is implemented only for Policy Works implementation
    *                 since cases coming from Policy Works has max probability of of not having any SIC Code associated
    *   @param      : None
    *   @return     : List<Set> - The list of default coverages
    */
    public static Set<String> getDefaultCoveragesWithoutSICCode(){
        Set<String> setDefaultCoverages = new Set<String>();
        setDefaultCoverages.add(RiskService.COVERAGE_BUILDING);
        setDefaultCoverages.add(RiskService.COVERAGE_CONTENTS);
        setDefaultCoverages.add(RiskService.COVERAGE_EQUIPMENT);
        setDefaultCoverages.add(RiskService.COVERAGE_STOCK);
        return setDefaultCoverages;
    }

    /*
    *   Name        : getAddonCoveragesWithoutSICCode
    *   @description: This method would return the list of addon coverages for the Risk when the case has
    *                 No SIC Code associated with it. This method is implemented only for Policy Works implementation
    *                 since cases coming from Policy Works has max probability of of not having any SIC Code associated
    *   @param      : None
    *   @return     : List<Set> - The list of addon coverages
    */
    public static Set<String> getAddonCoveragesWithoutSICCode(){
        Set<String> setAddonCoverages = new Set<String>();
        setAddonCoverages.add(RiskService.COVERAGE_EARTHQUAKE);
        setAddonCoverages.add(RiskService.COVERAGE_FLOOD);
        setAddonCoverages.add(RiskService.COVERAGE_SEWER);
        return setAddonCoverages;
    }
}