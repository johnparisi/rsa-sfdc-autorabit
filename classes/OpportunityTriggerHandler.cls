/**
* @File Name    :   OpportunityTriggerHandler
* @Description  :   Opportunity Object Handler class
* @Date Created :   02/02/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Trigger Handler
* @Modification Log:
**************************************************************************************
* Ver       Date        Author           Modification
* 1.0       01/14/2017  Habiba Zaman  Created the file/class
* 
* The new Object Handler classes will follow this pattern.
* 
* It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
* Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
* 
* No need to update this unless adding a business unit or record types. 
*/

public with sharing class OpportunityTriggerHandler {

	private List<Opportunity> renewalOpportunities = new List<Opportunity>();
    private List<Opportunity> newBusinessOpportunities = new List<Opportunity>();
    private List<Opportunity> specialtyLineOpportunities = new List<Opportunity>();


	public void beforeInsert(List<Opportunity> records){
		new OpportunityDomain().beforeInsert(records);

        initOpportunities(records);

        if(renewalOpportunities.size() > 0){
            new OpportunityDomain.GSLRenewalDomain().beforeInsert(renewalOpportunities);
        }

    }

    public void beforeUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
 		
 		new OpportunityDomain().beforeUpdate(records, oldRecords);

        initOpportunities(records);

        if(renewalOpportunities.size() > 0){
            new OpportunityDomain.GSLRenewalDomain().beforeUpdate(renewalOpportunities, oldRecords);
        }


        if(specialtyLineOpportunities.size() > 0){
            new OpportunityDomain.SpecialtyLineDomain().beforeUpdate(specialtyLineOpportunities, oldRecords);
        }
    }

    public void afterInsert(List<Opportunity> records){
    	new OpportunityDomain().afterInsert(records);

        initOpportunities(records);

        if(renewalOpportunities.size() > 0){
            new OpportunityDomain.GSLRenewalDomain().afterInsert(renewalOpportunities);
        }

    }
    public void afterUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
    	new OpportunityDomain().afterUpdate(records, oldRecords);

        initOpportunities(records);

        if(renewalOpportunities.size() > 0){
            new OpportunityDomain.GSLRenewalDomain().afterUpdate(renewalOpportunities, oldRecords);
        }

        if(newBusinessOpportunities.size() > 0){
            new OpportunityDomain.GSLNewBusinessDomain().afterUpdate(newBusinessOpportunities, oldRecords);
        }
    
    }

    private void initOpportunities(List<Opportunity> records){
        Map<String,Id> OpportunityTypes = Utils.GetRecordTypeIdsByDeveloperName(Opportunity.SObjectType, true);
        

        // only load opportunities those are renewal record type.
        for (Opportunity o : records){
            if(o.RecordTypeId == OpportunityTypes.get('bkrMMGSL_Opportunity_Renewals')){
                renewalOpportunities.add(o);
            }
            else if(o.RecordTypeId == OpportunityTypes.get('bkrMMGSL_Opportunity')){
                newBusinessOpportunities.add(o);
            }
            else if(o.RecordTypeId == OpportunityTypes.get('Commercial_Insurance_Submission')){
                specialtyLineOpportunities.add(o);

            }
        }
    }
}