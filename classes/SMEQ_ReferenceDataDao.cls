public class SMEQ_ReferenceDataDao {
	
	public SMEQ_ReferenceDataModel getReferenceData(String variant){
		SMEQ_ReferenceDataModel referenceDataModel = new SMEQ_ReferenceDataModel();
		List<SMEQ_REFERENCE_DATA__c> referenceDataFromDataStore = queryReferenceData(variant);
		Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap = new Map<String,List<SMEQ_KeyValueModel>>();
		Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap = new Map<String,List<SMEQ_KeyValueModel>>();
		populateReferenceDataMapforEnglishAndFrench(referenceDataFromDataStore,referenceDataEnglishMap,referenceDataFrenchMap);
		referenceDataModel.referenceDataListingEnglish = buildReferenceDataListingByLanguageModel('EN', referenceDataEnglishMap);
		referenceDataModel.referenceDataListingFrench = buildReferenceDataListingByLanguageModel('FR', referenceDataFrenchMap);
		return referenceDataModel;
	}

	@TestVisible
	private SMEQ_ReferenceDataListingByLanguageModel buildReferenceDataListingByLanguageModel(String language, Map<String,List<SMEQ_KeyValueModel>> referenceDataMap){
		SMEQ_ReferenceDataListingByLanguageModel  referenceDataListingByLanguageModel = new SMEQ_ReferenceDataListingByLanguageModel();
		referenceDataListingByLanguageModel.language = language;
		List<SMEQ_ReferenceDataByKeyListsModel> referenceDataByKeyListsModelList = new List<SMEQ_ReferenceDataByKeyListsModel>();
		for (String key : referenceDataMap.keySet()) {
			//Split the key
			String[] keySplit = key.split('_');
			referenceDataByKeyListsModelList.add(buildReferenceDataByKeyListModel(keySplit[0], referenceDataMap.get(key)));
		}
		referenceDataListingByLanguageModel.referenceDataByKeyLists = referenceDataByKeyListsModelList;
		return referenceDataListingByLanguageModel;
	}


	@TestVisible
	private SMEQ_ReferenceDataByKeyListsModel buildReferenceDataByKeyListModel(String type, List<SMEQ_KeyValueModel> keyValueModelList){
		SMEQ_ReferenceDataByKeyListsModel  referenceDataByKeyListsModel = new SMEQ_ReferenceDataByKeyListsModel();
		referenceDataByKeyListsModel.referenceDataKey = type;
		referenceDataByKeyListsModel.keyValues = keyValueModelList;
		return referenceDataByKeyListsModel;
	}



	@TestVisible
	private void populateReferenceDataMapforEnglishAndFrench(List<SMEQ_REFERENCE_DATA__c> referenDataFromDatabase,Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap, Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap){
		if(referenDataFromDatabase!=null && referenDataFromDatabase.size()>0){
			for(SMEQ_REFERENCE_DATA__c referenceData:referenDataFromDatabase){
				buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceData, referenceDataEnglishMap, referenceDataFrenchMap);
			}
		}
	}


 	@TestVisible
    private List<SMEQ_REFERENCE_DATA__c> queryReferenceData(String variant){
        String queryGenerated = buildReferenceDataQueryString(variant);
        List<SMEQ_REFERENCE_DATA__c> referenceDataList = Database.query(queryGenerated);
        return referenceDataList;
    }


    @TestVisible
    private String buildReferenceDataQueryString(String variant){
    	String referenceDataQueryString = 'SELECT code__c, type__c, language__c, display_order__c, value__c, variant__c from SMEQ_REFERENCE_DATA__c';
        if(variant!=null && variant.length()>0){
            referenceDataQueryString = referenceDataQueryString + ' where variant__C= :variant';
        }
        referenceDataQueryString = referenceDataQueryString + ' order by display_order__c';
        return referenceDataQueryString;
    }


    


	@TestVisible
	private void buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(SMEQ_REFERENCE_DATA__c referenceData,
		Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap, Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap){
		SMEQ_KeyValueModel keyValue = new SMEQ_KeyValueModel();
		keyValue.key = referenceData.code__c;
		keyValue.value = referenceData.value__c;
		List<SMEQ_KeyValueModel> keyValueList;
		if(referenceData.language__c == 'EN'){
			keyValueList = referenceDataEnglishMap.get(referenceData.type__c + '_' + referenceData.language__c);
		}
		else {
			keyValueList = referenceDataFrenchMap.get(referenceData.type__c + '_' + referenceData.language__c);
		}

		if(keyValueList == null){
			keyValueList = new List<SMEQ_KeyValueModel>();
			if(referenceData.language__c == 'EN'){
				referenceDataEnglishMap.put(referenceData.type__c + '_' + referenceData.language__c,keyValueList);
			}
			else{
				referenceDataFrenchMap.put(referenceData.type__c + '_' + referenceData.language__c, keyValueList);
			}
		}

		keyValueList.add(keyValue);

	}

}