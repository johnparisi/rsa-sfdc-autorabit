global class testRating2 implements vlocity_ins.VlocityOpenInterface2 {
   
    global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        ratingQuote rQ = new ratingQuote();
        rQ.output = new List<ratingQuote.output>();
        ratingQuote.output op = new ratingQuote.output(); 
        op.calculationResults.add(new ratingQuote.CalculationResults());
        rQ.Output.add(op);
       // output.put('output','{"aggregationResults":{},"calculationResults":{"ID":"output","premiumBODO1":16.19,"premiumBLDB1":59.32,"premiumEQPB1":0,"premiumCTSB1":60.25,"premiumSTKB1":246,"premiumSBUE1":0,"premiumFLDE1":0,"premiumEQKE1":0,"productKey":"01tc0000006ucZIAAY","parentProdKey":"01tc0000006ucZIAAY"}}');
        system.debug('!@output '+ rQ); 
        output.put('output',rQ.Output);
        // output.put('output','rQ.Output');

        return 'null';
    }
    
    global Object testR(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        
        system.debug('!@methodName '+ methodName);
        system.debug('!@inputs '+ inputs.keySet());
        system.debug('!@inputs '+ inputs.values());
        system.debug('!@output '+ output);
        system.debug('!@options '+ options);
        
        return null;
    }
    
   
}