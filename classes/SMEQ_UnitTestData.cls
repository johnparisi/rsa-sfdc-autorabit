/*
This Class will be used to create mock test data for unit test cases.
*/
public class SMEQ_UnitTestData
{
    
    public static void removeSICCodeCategory(String sicCode)
    {
        SIC_Code_Detail_Version__c sicCodeDetailVersion = [
            SELECT category__c, subcategory__c, SIC_Code__r.SIC_Code__c 
            FROM SIC_Code_Detail_Version__c 
            WHERE SIC_Code__r.SIC_Code__c = :sicCode
            LIMIT 1
        ];
                
        sicCodeDetailVersion.category__c = null;
        sicCodeDetailVersion.subcategory__c = null;
        update sicCodeDetailVersion;
    }
    
    public static void removeApplicationEnabled(String sicCode)
    {
        SIC_Code_Detail_Version__c sicCodeDetailVersion = [
            SELECT Application_Enabled__c, SIC_Code__r.SIC_Code__c 
            FROM SIC_Code_Detail_Version__c 
            WHERE SIC_Code__r.SIC_Code__c = :sicCode
            LIMIT 1
        ];
        
        sicCodeDetailVersion.Application_Enabled__c = false;
        update sicCodeDetailVersion;
    }
    
    public static void applyMonolineTrue(String sicCode)
    {
        SIC_Code_Detail_Version__c sicCodeDetailVersion = [
            SELECT Application_Enabled__c, SIC_Code__r.SIC_Code__c 
            FROM SIC_Code_Detail_Version__c 
            WHERE SIC_Code__r.SIC_Code__c = :sicCode
            LIMIT 1
        ];
        
        sicCodeDetailVersion.Monoline__c = true;
        update sicCodeDetailVersion;
    }
    
    public static List<SIC_Code_Detail_Version__c> createSICCodeDetailData(String sicCode)
    {
        List<SIC_Code_Detail_Version__c> listSicCodeDetails = new List<SIC_Code_Detail_Version__c>();
        
        SIC_Code__c sc = new SIC_Code__c();
        sc.SIC_Code__c = sicCode;
        insert sc;
        
        SIC_Code_Detail_Version__c sicCodeDetail = new SIC_Code_Detail_Version__c(
            Action__c = 'Refer to UW',
            Application_Enabled__c = true,
            Canadian_Liability__c = '1',
            Category__c = 'Wholesale',
            Crime_Hazard__c = '1',
            Description_Operations_En__c = 'Test Desc',
            Description_Operations_Fr__c = 'Test Desc',
            E_O_Liability__c = '1',
            Fid_Hazard__c = '1',
            Mid_Market__c = true,
            Non_RSA__c = true,
            Poll_Liability__c = '1',
            Property_Hazard__c = '1',
            RAG__c = 'Amber',
            Referral_Level__c = 'LEVEL 3',
            Segment__c = 'Agriculture, Farms & Services',
            Short_Description_En__c = 'Short Desc En',
            Short_Description_Fr__c = 'Short Desc Fr',
            SIC_Code__c = sc.Id,
            SME__c = true,
            Subcategory__c = 'Wood, Wax & Paper Products',
            Subsegment__c = 'Oil & Gas Production',
            USA_Liability__c = '1',
            Offering__c = 'SPRNT v1',
            Region__c = 'Pacific',
            Coverage_Defaults__c = RiskService.COVERAGE_CGL + ';' + RiskService.COVERAGE_BUILDING + ';' + RiskService.COVERAGE_EQUIPMENT + ';' + RiskService.COVERAGE_STOCK
        );
        listSicCodeDetails.add(sicCodeDetail);
        try
        {
            insert listSicCodeDetails;
        }
        catch (DmlException dmle)
        {
            System.debug(dmle.getMessage());
        }
        List<SIC_Code_Detail_Version__c> sicCodeDetailList = [
            SELECT SIC_Code__r.SIC_Code__c, Region__c, Offering__c
            FROM SIC_Code_Detail_Version__c 
            WHERE Id = : listSicCodeDetails.get(0).Id
        ];
        
        if (!sicCodeDetailList.isEmpty())
        {
            createSIC_Code_Question_AssociationData (sicCodeDetailList.get(0).SIC_Code__r.SIC_Code__c);
            createSIC_Terms_And_Condition_AssociationData (sicCodeDetailList.get(0).SIC_Code__r.SIC_Code__c);
            //createSIC_Code_Question_Affirmative_Condition();
            return sicCodeDetailList;
        }
        else
        {
            return null;
        }
    }
    
    public static List<SIC_Question__c> createSIC_Question()
    {
        Map<String,Id> SicQuestionTypes = Utils.GetRecordTypeIdsByDeveloperName(SIC_Question__c.SObjectType, true);
        
        List<SIC_Question__c> sicQList = new List<SIC_Question__c>();
        SIC_Question__c sicQ = new SIC_Question__c(
            liveChatRequired__c = true,
            questionId__c = 11,
            questionText_en__c = 'Ques Test En',
            questionText_fr__c = 'Ques Test Fr',
            recordtypeId = sicQuestionTypes.get(SicQuestionService.RECORDTYPE_ELIGIBILITY)
        );
        sicQList.add(sicQ);
        try
        {
            insert sicQList;
        }
        catch (DmlException dmle)
        {
            System.debug(dmle.getMessage());
        } 
        return sicQList;
    }
    
    public static List<SIC_Code_Question_Association__c> createSIC_Code_Question_AssociationData(String sicCode)
    {
        if (String.isNotBlank(sicCode))
        {
            List<SIC_Code_Detail_Version__c> sicCodeList = [
                SELECT Id, SIC_Code__r.SIC_Code__c
                FROM SIC_Code_Detail_Version__c 
                WHERE SIC_Code__r.SIC_Code__c = : sicCode
            ];
            List<SIC_Question__c> listSICQID = createSIC_Question();
            List<SIC_Question__c> listSicQ = [
                SELECT Id 
                FROM SIC_Question__c
                WHERE Id = : listSICQID.get(0).Id
            ];
            List<SIC_Code_Question_Association__c> listSICCodeQA = 
                new List<SIC_Code_Question_Association__c>();   
            
            if (!sicCodeList.isEmpty() && !listSicQ.isEmpty())
            {
                SIC_Code_Question_Association__c sicCodeQA = 
                    new SIC_Code_Question_Association__c(
                        order__c = 11,
                        questionId__c = listSicQ.get(0).Id,
                        SIC_Code_Detail_Version__c = sicCodeList.get(0).Id);
                listSICCodeQA.add(sicCodeQA);
                try
                {
                    insert listSICCodeQA;
                }
                catch (DmlException dmle)
                {
                    system.debug(dmle.getMessage());
                }
            }
            return listSICCodeQA;
        }
        else
        {
            return null;
        }
    }
    
    public static List<SIC_Terms_And_Condition__c> createSIC_Terms_And_Condition()
    {
        List<SIC_Terms_And_Condition__c> sicTACList = new List<SIC_Terms_And_Condition__c>();
        SIC_Terms_And_Condition__c sicTAC = new SIC_Terms_And_Condition__c (    
            Name = 'SICCODETerms&Cond1001',
            conditionText_en__c = 'Cond Text Test En',
            conditionText_fr__c = 'Cond Text Test Fr', 
            tcId__c = '1001');
        sicTACList.add(sicTAC);
        try
        {
            insert sicTACList;
        }
        catch (DmlException dmle)
        {
            System.debug(dmle.getMessage());
        }
        return sicTACList;
    }
    
    public static List<SIC_Terms_And_Condition_Association__c> createSIC_Terms_And_Condition_AssociationData(String sicCode)
    {
        if (String.isNotBlank(sicCode))
        {
            List<SIC_Code_Detail_Version__c> sicCodeList = [
                SELECT Id, SIC_Code__r.SIC_Code__c
                FROM SIC_Code_Detail_Version__c 
                WHERE SIC_Code__r.SIC_Code__c = : sicCode
            ];
            List<SIC_Terms_And_Condition__c> sicTACList = createSIC_Terms_And_Condition();
            List<SIC_Terms_And_Condition__c> listSicTAC = [
                SELECT Id 
                FROM SIC_Terms_And_Condition__c
                WHERE Id = : sicTACList.get(0).Id
            ];
            
            List<SIC_Terms_And_Condition_Association__c> listSICCodeTAC = 
                new List<SIC_Terms_And_Condition_Association__c>();
            if (!sicCodeList.isEmpty() && !listSicTAC.isEmpty())
            {
                SIC_Terms_And_Condition_Association__c sicCodeTACAssociation = 
                    new SIC_Terms_And_Condition_Association__c(
                        order__c = 11,
                        tcId__c = listSicTAC.get(0).Id,
                        SIC_Code_Detail_Version__c = sicCodeList.get(0).Id);
                listSICCodeTAC.add(sicCodeTACAssociation);
                try
                {
                    insert listSICCodeTAC;
                }
                catch (DmlException dmle)
                {
                    System.debug(dmle.getMessage());
                }
            }
            return listSICCodeTAC;
        }
        else
        {
            return null;
        }
    }
    
    public static List<SIC_Code_Affirmative_Condition__c> createSIC_Code_Affirmative_Condition()
    {
        List<SIC_Code_Affirmative_Condition__c> listSicCodeAC = new List<SIC_Code_Affirmative_Condition__c>();
        SIC_Code_Affirmative_Condition__c sicCodeAC = new SIC_Code_Affirmative_Condition__c(
            Name = 'SICCODEAC1001',
            affConditionId__c = '11', 
            affConditionText_en__c = 'SICODEAC TEST EN',
            affConditionText_fr__c = 'SICODEAC TEST FR');
        listSicCodeAC.add(sicCodeAC);
        try
        {
            insert listSicCodeAC;
        }
        catch (DmlException dmle)
        {
            System.debug(dmle.getMessage());
        }
        return listSicCodeAC;
    }
    
    public static List<SIC_Code_Question_Affirmative_Condition__c> createSIC_Code_Question_Affirmative_Condition()
    {
        List<SIC_Question__c> sicQList = createSIC_Question();
        
        List<SIC_Question__c> listSicQ = [
            SELECT Id 
            FROM SIC_Question__c
            WHERE Id = : sicQList.get(0).Id
        ];
        
        List<SIC_Code_Affirmative_Condition__c> listSICCodeAC = createSIC_Code_Affirmative_Condition();
        
        List<SIC_Code_Affirmative_Condition__c> listSICCodeACId = [
            SELECT Id 
            FROM SIC_Code_Affirmative_Condition__c
            WHERE Id = : listSICCodeAC.get(0).Id
        ];
        
        List<SIC_Code_Question_Affirmative_Condition__c> listSICCodeQAC = new List<SIC_Code_Question_Affirmative_Condition__c>();
        
        SIC_Code_Question_Affirmative_Condition__c sicCodeQAC = new SIC_Code_Question_Affirmative_Condition__c(
            Name = 'SICCODEQAC1001',
            affConditionId__c = listSICCodeACId.get(0).Id,
            order__c = 11,
            questionId__c = listSicQ.get(0).Id);
        
        listSICCodeQAC.add(sicCodeQAC);
        try
        {
            insert listSICCodeQAC;
        }
        catch (DmlException dmle)
        {
            System.debug(dmle.getMessage());
        }
        return listSICCodeQAC;
    }
    
    // public static SIC_Code_Question_Affirmative_Condition__c
    public static List<SIC_Code_Question_Affirmative_Condition__c> createSIC_Code_Question_Affirmative_ConditionWithQuesions(List<SIC_Question__c> questions)
    {
        List<SIC_Question__c> sicQList = questions;
        
        List<SIC_Question__c> listSicQ = [
            SELECT Id 
            FROM SIC_Question__c
            WHERE Id = : sicQList.get(0).Id
        ];
        
        List<SIC_Code_Affirmative_Condition__c> listSICCodeAC = createSIC_Code_Affirmative_Condition();
        
        List<SIC_Code_Affirmative_Condition__c> listSICCodeACId = [
            SELECT Id 
            FROM SIC_Code_Affirmative_Condition__c
            WHERE Id = : listSICCodeAC.get(0).Id
        ];
        
        List<SIC_Code_Question_Affirmative_Condition__c> listSICCodeQAC = new List<SIC_Code_Question_Affirmative_Condition__c>();
        
        SIC_Code_Question_Affirmative_Condition__c sicCodeQAC = new SIC_Code_Question_Affirmative_Condition__c(
            Name = 'SICCODEQAC1001',
            affConditionId__c = listSICCodeACId.get(0).Id,
            order__c = 11,
            questionId__c = listSicQ.get(0).Id);
        listSICCodeQAC.add(sicCodeQAC);
        
        try
        {
            insert listSICCodeQAC;
        }
        catch (DmlException dmle)
        {
            System.debug(dmle.getMessage());
        }
        return listSICCodeQAC;
    }
}