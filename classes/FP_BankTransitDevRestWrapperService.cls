/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/bankTransit/*')

global with sharing class FP_BankTransitDevRestWrapperService {
	
	@HttpPost
    global static FP_AngularPageController.BankTransitResponse searchForTransitNumbers(String searchText) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        List<Bank_Transit_Number__c> bankTransitNumberList = null;
        FP_AngularPageController.BankTransitResponse bankTransitResponse = null;
		try {
			bankTransitResponse = FP_AngularPageController.searchForTransitNumbers(searchText);
		}
		catch (Exception e) {
			System.debug('##### caught exception ' + e.getMessage());
			bankTransitResponse = new FP_AngularPageController.BankTransitResponse();
			bankTransitResponse.success = false;
			bankTransitResponse.errorMessages.add(e.getMessage());
		}
        return bankTransitResponse;
    }
    
}