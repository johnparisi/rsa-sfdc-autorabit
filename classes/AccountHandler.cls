/**
 * AccountHandler
 * Author: Stephen Piercey
 * Date: April 26 2016
 * 
 * The new Object Handler classes will follow this pattern.
 * 
 * It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
 * Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
 * 
 * You should never need to update this unless you are adding a business unit or record types. 
 */
public with sharing class AccountHandler {
	private List<Account> brokerAccounts = new List<Account>();

    public void beforeInsert(List<Account> records){
        
        new AccountDomain().beforeInsert(records);

        initAccounts(records);

        if(brokerAccounts.size() > 0){
            new AccountDomain.BrokerDomain().beforeInsert(brokerAccounts);
        }
    }

    public void beforeUpdate(List<Account> records, Map<Id, Account> oldRecords){
        
        new AccountDomain().beforeUpdate(records, oldRecords);

        initAccounts(records);

        if(brokerAccounts.size() > 0){
            new AccountDomain.BrokerDomain().beforeUpdate(brokerAccounts, oldRecords);
        }
    }

    public void afterInsert(List<Account> records){
        new AccountDomain().afterInsert(records);

        initAccounts(records);

        if(brokerAccounts.size() > 0){
            new AccountDomain.BrokerDomain().afterInsert(brokerAccounts);
        }
    }
    public void afterUpdate(List<Account> records, Map<Id, Account> oldRecords){
        new AccountDomain().afterUpdate(records, oldRecords);

        initAccounts(records);
        
        if(brokerAccounts.size() > 0){
            new AccountDomain.BrokerDomain().afterUpdate(brokerAccounts, oldRecords);
        }
    }

    private void initAccounts(List<Account> records){
        Map<String,Id> AccountTypes = Utils.getRecordTypeIdsByDeveloperName(Account.SObjectType, true);
        Set<Id> brokerRecordTypes = new Set<Id>();

        brokerRecordTypes.add(AccountTypes.get('bkrAccount_RecordTypeBkrMM'));
        brokerRecordTypes.add(AccountTypes.get('bkrAccount_RecordTypeBkrMGML'));

        for (Account a : records){
            if(brokerRecordTypes.contains(a.RecordTypeId)){
                brokerAccounts.add(a);
            }
        }
    }
}