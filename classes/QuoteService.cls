/*
* Quote__c Service Layer
* Contains processing service methods related to the Quote__c Object
*/
public class QuoteService
{
    public static final String DEVIATION_TYPE_DISCOUNT = 'Discount';
    public static final String DEVIATION_TYPE_SURCHARGE = 'Surcharge';
    public static final String DEVIATION_BROKER_REASON = 'Broker Management';
    
    public static final String STATUS_FINALIZED = 'Finalized';
    public static final String STATUS_BIND = 'Bind'; 
    
    @TestVisible
    private static final String LLP_REFERRAL_TRIGGER_REASON = 'REF-0016 This quote has a Large Loss Propensity score of 10 on one of the locations with deviation discount applied, please complete this quote in ePolicy.';
    @TestVisible
    public static final Integer LLP_REFERRAL_TRIGGER_THRESHOLD = 10;
    
    /**
     * updateSelectedCoverages
     * @param   records     Quote__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     * Updates the coverages selected field on a quote's risks based on their coverages which are marked as selected.
     */
    public void updateSelectedCoverages(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        Set<Id> packageSelectedUpdated = new Set<Id>();
        
        for (Quote__c q : (List<Quote__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Package_Type_Selected__c'}))
        {
            packageSelectedUpdated.add(q.Id);
        }
        
        if (!packageSelectedUpdated.isEmpty())
        {
            Map<Id, Risk__c> risks = new Map<Id, Risk__c>([
                SELECT Id, Quote__r.Package_Type_Selected__c
                FROM Risk__c
                WHERE Quote__c IN :packageSelectedUpdated
            ]);
            
            List<Coverages__c> coverages = [
                SELECT Id, Type__c, Risk__c, Package_ID__c
                FROM Coverages__c
                WHERE Risk__c IN :risks.keySet()
            ];
            
            for (Coverages__c c : coverages)
            {
                Risk__c r = risks.get(c.Risk__c);
                c.Selected__c = c.Package_ID__c == r.Quote__r.Package_Type_Selected__c;
            }
            
            update coverages;
        }
    }
    
    /**
     * updateCaseWithLLPReferralTriggerReason
     * @param   records     Quote__c that were inserted/updated
     * @param   oldRecords  Previous state of the quote that were inserted/updated mapped by id
     *
     * Updates the Referral Trigger Reason quote's parent based on whether there is a package deviated when LLP exceeds threshold.
     */
    public void updateCaseWithLLPReferralTriggerReason(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        Map<Id, Boolean> llpUpdated = new Map<Id, Boolean>();
        
        for (Quote__c q : (List<Quote__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Packages_Exceeded_Large_Loss_Propensity__c'}))
        {
            llpUpdated.put(q.case__c, q.Packages_Exceeded_Large_Loss_Propensity__c != null); // Set to true if there is a package selected for deviation.
        }
        
        if (!llpUpdated.isEmpty())
        {
            List<Case> cs = [
                SELECT Id, Referral_Trigger_Reason_s__c
                FROM Case
                WHERE Id IN :llpUpdated.keySet()
            ];
            
            for (Case c : cs)
            {
                // Retrieve the list of reasons.
                Set<String> reasons = new Set<String>();
                if (c.Referral_Trigger_Reason_s__c != null)
                {
                    reasons.addAll(c.Referral_Trigger_Reason_s__c.split(';'));
                }
                
                // Add or remove based on Packages_Exceeded_Large_Loss_Propensity__c.
                if (llpUpdated.get(c.Id))
                {
                    reasons.add(LLP_REFERRAL_TRIGGER_REASON);
                }
                else
                {
                    reasons.remove(LLP_REFERRAL_TRIGGER_REASON);
                }
                c.Referral_Trigger_Reason_s__c = Utils.commaDelimit(new List<String>(reasons));
            }
            
            update cs;
        }
    }
    
    /**
     * Insert all non-location risks for a quote's SIC Code Detail Version to increase underwriter efficiency.
     */
    public void insertDefaultRisks(List<Quote__c> records)
    {
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        
        Map<ID, SIC_Code_Detail_Version__c> quote2scdvs = getSicCodeDetailVersionMapping(records);
        
        List<Risk__c> defaultRisks = new List<Risk__c>();
        
        for (Quote__c q : records)
        {
            SIC_Code_Detail_Version__c scdv = quote2scdvs.get(q.Id);

            if (scdv != NULL){
                // Insert a CGL coverage if applicable.
                if (scdv.Coverage_Defaults__c.contains(RiskService.COVERAGE_CGL)) {
                    Risk__c r = createRisk(q.Id, RiskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY), RiskService.COVERAGE_CGL);
                    defaultRisks.add(r);
                }

                // Insert an property aggregate package coverage if applicable.
                if (scdv.Coverage_Defaults__c.contains(RiskService.COVERAGE_AGGREGATE)) {
                    Risk__c r = createRisk(q.Id, RiskTypes.get(RiskService.RISK_RECORDTYPE_AGGREGATE), RiskService.COVERAGE_AGGREGATE);
                    defaultRisks.add(r);
                }

                // Insert a privacy breach package coverage if applicable.
                if (scdv.Coverage_Defaults__c.contains(RiskService.COVERAGE_CYBER)) {
                    Risk__c r = createRisk(q.Id, RiskTypes.get(RiskService.RISK_RECORDTYPE_CYBER), RiskService.COVERAGE_CYBER);
                    defaultRisks.add(r);
                }
            }
            else{ // Defect : PWP-363: Starts
                /*
                    Function  : Logic implemented to create liability risk when No Sic code is associated with the Case.
                                This logic is implemented only for Policy Works implementation since from Policy Works
                                there are 90% possibilities where Cases case come without Sic Code.
                */
                if(q.Business_Source_ID__c == 'PWEB'){
                    system.debug('RiskService.COVERAGE_CGL -->'+RiskService.COVERAGE_CGL);
                    Risk__c r = createRisk(q.Id, RiskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY), RiskService.COVERAGE_CGL);
                    defaultRisks.add(r);
                }
            } // Defect : PWP-363: Ends
        }
        
        insert defaultRisks;
    }
    
    /**
     * Create a risk with a specific recordtype with default coverage type set.
     */
    private static Risk__c createRisk(Id quoteId, Id riskRecordtypeId, String coverageType)
    {
        Risk__c r = new Risk__c();
        r.RecordTypeId = riskRecordtypeId;
        r.Quote__c = quoteId;
        
        r.Coverage_Selected_Option_1__c = coverageType;
        r.Coverage_Selected_Option_2__c = coverageType;
        
        return r;
    }
    
    /**
     * FP-58
     * getSicCodeDetailVersionMapping
     * 
     * @param records: The list of new or updated records.
     * @return: A map from the quote record ID to its parent case's SIC Code Detail Version.
     * 
     * Create a mapping from a quote ID to its SIC Code Detail Version. Leverages the SMEQ_SICCodeService.
     */
    private Map<ID, SIC_Code_Detail_Version__c> getSicCodeDetailVersionMapping(List<Quote__c> records)
    {
        Map<ID, SIC_Code_Detail_Version__c> quote2scdv = new Map<Id, SIC_Code_Detail_Version__c>();
        
        // Extract the fields needed to retrieve the SIC Code Detail Version.
        List <Quote__c> quoteCase = [
            SELECT id, Case__c, Case__r.bkrCase_SIC_Code__r.SIC_Code__c, 
            Case__r.Offering_Project__c, Case__r.bkrCase_Region__c
            FROM Quote__c
            WHERE id IN :records
        ];
        
        List<ID> caseIds = new List<ID>();
        
        // Get the risk's quote's case.
        for (Quote__c q : quoteCase)
        {
            caseIds.add(q.Case__c);
        }
        
        // Retrieve the SIC Code Detail Versions.
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicCodesInAppetiteForCases(caseIds);
        
        // Match the risks to their SIC Code Detail Version.
        for (Quote__c q : quoteCase)
        {
            for (SIC_Code_Detail_Version__c scdv : scdvs)
            {
                if(scdv != null) { // Null checker implemented for defect PWP:363 since from Policy Works cases can come without Sic Code
                    if (scdv.SIC_Code__r.SIC_Code__c == q.Case__r.bkrCase_SIC_Code__r.SIC_Code__c &&
                            scdv.Offering__c.contains(q.Case__r.Offering_Project__c) &&
                            scdv.Region__c.contains(q.Case__r.bkrCase_Region__c)) {
                        quote2scdv.put(q.id, scdv);
                    }
                }
            }
        }
        
        return quote2scdv;
    }
    
    /**
     * FP-1534
     * 
     * setDeviatePackagesForUnderwriter
     * @param   records     Quote__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     * 
     * Updates the packages selected for deviation field when a deviation type percentage is set.
     * Only updated for underwriters when no package was set previously.
     */
    public void setDeviatePackagesForUnderwriter(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        if (UserInfo.getUserType() == UserService.USERTYPE_STANDARD)
        {
            Schema.DescribeFieldResult packageIDResult = Quote__c.Selected_Packages_to_Deviate__c.getDescribe();
            List<Schema.PicklistEntry> packageTypes = packageIDResult.getPicklistValues();
            
            for (Quote__c q : (List<Quote__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {
                'Deviation_Type__c', 'Deviation_Percentage__c'}))
            {
                Quote__c qold = oldRecords.get(q.Id);
                
                if ((q.Selected_Packages_to_Deviate__c == null || q.Selected_Packages_to_Deviate__c == '') && 
                    (qold.Selected_Packages_to_Deviate__c == null || qold.Selected_Packages_to_Deviate__c == ''))
                {
                    q.Selected_Packages_to_Deviate__c = '';
                    
                    // Iterate over the package IDs listed in the Selected Packages to Deviate field.
                    for (Schema.PicklistEntry pkg : packageTypes)
                    {
                        q.Selected_Packages_to_Deviate__c += pkg.getLabel() + ';';
                    }
                    q.Selected_Packages_to_Deviate__c.removeEnd(';'); // Remove trailing semicolon.
                }
            }
        }
    }
    
    /**
     * insert role sharing for brokers (users) who belong to the same brokage (accounts)
     * this will allow all brokers from the same brokage to see and quote on the PDF level
     * this only need to be used by quote insert. For quote update, it would be the broker from the same brokerage in real case. 
     */
    public void insertCaseRoleSharing(List<Quote__c> records)
    {   
        //check if the current running user is a broker (community partner)         
        if (UserInfo.getUserType() == UserService.USERTYPE_POWERPARTNER) {
            User runningUser = [select id, contactId from user where id =: userInfo.getuserid()];
            if(runningUser.contactId != null){
                List<contact> runningContact = [select id, AccountId from contact where Id =: runningUser.contactId];
                if(runningContact != null && runningContact[0] != null && runningContact[0].AccountId != null){
                    List<Account> brokerages = [select Id from Account where id =: runningContact[0].AccountId];
                    if(brokerages != null){
                        Map<Id, Contact> contactMap = new Map<Id, Contact>([select Id from Contact where AccountId =: brokerages[0].Id]);
                        List<User> usersFromSameBrokerage = [select Id from User where ContactId in: contactMap.keySet()];
                        List<CaseShare> caseShareBrokers = new List<CaseShare>();
                        for(User broker: usersFromSameBrokerage){//create sharing rules between case and brokers
                            //in real case, if the quote dml operation is trigger by a broker, it should only has one reocrd (no bulk insert or update)
                            for(Quote__c quote: records){
                                CaseShare caseShareBroker = new CaseShare();
                                caseShareBroker.UserOrGroupId = broker.Id;
                                caseShareBroker.CaseId = quote.Case__c;
                                caseShareBroker.CaseAccessLevel = 'Read';
                                caseShareBrokers.add(caseShareBroker);
                            }
                        }
                        if(caseShareBrokers.size() > 0){
                            Database.insert(caseShareBrokers, false);//insert case access permission, if the broker already have access, the partial insert will fail peacefully
                        }
                    }
                }
                

            }
        }
                
    }
    
    public void populateCaseInfo(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        Map<Id, Quote__c> mapCaseIdtoQuote = new map<Id,Quote__c>();
        List<Case> caseListTobeUpdated = new List<Case>();
        
        for (Quote__c q : (List<Quote__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'status__c'}))
        {
            {
                mapCaseIdtoQuote.put(q.Case__c, q);
            }
        }
        
        if (!mapCaseIdtoQuote.isEmpty())
        {
            List<Case> caseList = [
                SELECT Id, Airmiles_from_Quote__c, bkrCase_Quoted_Premium__c, bkrCase_Bound_Premium__c, bkrCase_Policy__c
                FROM Case
                WHERE Id IN :mapCaseIdtoQuote.keySet()
            ];
            
            Map<Id,case> mapIdTocase = new Map<Id, Case>();
            
            for (case c : caseList)
            {
                mapIdTocase.put(c.Id, c);
            }
            
            for (Id cs : mapCaseIdtoQuote.keySet())   
            {
                Quote__c QT = mapCaseIdtoQuote.get(cs);
                Case objCase = mapIdTocase.get(cs);
                
                if (QT.status__c == STATUS_FINALIZED)
                {
                    objCase.Airmiles_from_Quote__c = QT.Air_Miles_Loyalty_Points_Earned__c;
                    objCase.bkrCase_Quoted_Premium__c = QT.Premium__c;
                    caseListTobeUpdated.add(objCase);
                }
                
                if (QT.status__c == STATUS_BIND)
                {
                    objCase.bkrCase_Bound_Premium__c = QT.Premium__c;
                    objCase.bkrCase_Policy__c = QT.ePolicy__c;
                    caseListTobeUpdated.add(objCase);
                }
            }
            
            update caseList;
        }
    }
    
    /**
     * insertLocationRisk
     * @param   records     Quote__c that were inserted/updated
     * @param   oldRecords  Previous state of the Quotes that were inserted/updated mapped by id
     *
     * This method will create a new location Risk if the 'Copy_Address_From_Insured_Client__c' field is checked in the Quote__c object
     */
    public void insertLocationRisk(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        if (oldRecords == null)
        {
            insertUpdateLocationRisk(records);
        }
        else
        {
            List<Quote__c> recordsToProcess = new List<Quote__c>();  
            for (Quote__c quote: (List<Quote__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Identical_Location_And_Business_Address__c'}))
            {
                recordsToProcess.add(quote); 
            }
            insertUpdateLocationRisk(recordsToProcess);
        }
    }
    
    public void insertUpdateLocationRisk(List<Quote__c> records)
    {
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        List<Risk__c> locationRisks = new List<Risk__c>();
        
        Map<Id, Id> mapQuotetoAccount = new Map<Id,Id>();
        Set<Id>  caseIds = new Set<Id>();
        Map<Id,Id> quoteIdtoCaseId = new Map<Id,Id>();
        for (Quote__c quote : records)
        {
            if (quote.Identical_Location_And_Business_Address__c)
            {
                if (!quoteIdtoCaseId.containsKey(quote.Id))
                {
                    quoteIdtoCaseId.put(quote.Id, quote.case__c);
                }
            }
        }
        
        if (!quoteIdtoCaseId.isEmpty())
        {
            List<case> caseList = [
                SELECT Id, bkrCase_Insured_Client__c
                FROM case
                WHERE Id IN :quoteIdtoCaseId.values()
            ];
            
            Map<Id, Id> mapCaseIdtoAccountId = new Map<Id,Id>();
            for (Case cse : caseList)
            {
                if (cse.bkrCase_Insured_Client__c != null)
                {
                    if (!mapCaseIdtoAccountId.containsKey(cse.Id))
                    {
                        mapCaseIdtoAccountId.put(cse.Id, cse.bkrCase_Insured_Client__c);
                    }   
                }
            }
            List<Account> accountList = new List<Account>();
            if (!mapCaseIdtoAccountId.isEmpty())
            {
                accountList = [
                    SELECT Id, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode 
                    FROM Account 
                    WHERE Id IN :mapCaseIdtoAccountId.values()
                ];
            }
            
            Map<Id, Account> mapIdtoAccount = new Map<Id, Account>();
            for (Account acc : accountList)
            {
                if (!mapIdtoAccount.containsKey(acc.Id))
                {
                    mapIdtoAccount.put(acc.Id, acc);
                }
            }
            
            for (Id quoteId : quoteIdtoCaseId.keySet())
            {
                Id caseId = quoteIdtoCaseId.get(quoteId);
                if (mapCaseIdtoAccountId.get(caseId) != null)
                {
                    Id accountId = mapCaseIdtoAccountId.get(caseId);
                    if (mapIdtoAccount.get(accountId) != null)
                    {
                        Account a = mapIdtoAccount.get(accountId);
                        
                        if (a != null)
                        {
                            Risk__c r = new Risk__c();
                            r.RecordTypeId = RiskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION);
                            r.Quote__c = quoteId;
                            r.Address_Line_1__c = a.BillingStreet;
                            r.City__c = a.BillingCity;
                            r.Postal_Code__c = a.BillingPostalCode;
                            r.Province__c =  formatBillingInfo(a.BillingState,'Province__c');
                            r.Country__c = formatBillingInfo(a.BillingCountry,'Country__c');
                            
                            locationRisks.add(r); 
                        } 
                    }
                }
            }
            insert locationRisks;
        }
        
    }
    
    
     public static String formatBillingInfo(String BillingInfo, String FieldName){
    	Map<string,string> mapStateCodetoName = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType(FieldName);
    	if(mapStateCodetoName.containsKey(BillingInfo)){
    		return mapStateCodetoName.get(BillingInfo);
    	}
    	else{
    		return BillingInfo;
    	}
    	
    }
}