/**
* @File Name    :   PolicyService
* @Description  :   Policy Service Layer
* 					Contains processing service methods related to the Policy Object
* @Date Created :   01/14/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Service
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       01/14/2017  Habiba Zaman  	Created the file/class
* 1.1		01/20/2017	Habiba Zaman	Moved Policy queue functionality to this class
*/

public with sharing class PolicyService {

	
    // Map for Account Record Type
    public Map<String,Id> accountTypes{
		get{

			return Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);
		}

        private set;
    }
	private static final Integer RENEWAL_THESHOLD_DAYS = 120;
	private static final String MMGSL_PROFILE = 'RSA Standard User [Regional Mid-Market & GSL, Policy-Level]';
    /**
    * @method   processPolicies
    * @param	records Policy records that need to be inserted
    * @param	oldRecords Old Policy records
    * @return 	N/A
    *
    * Process Policy records before inserting
    * 
    */
	public void processPolicies(List<Policy__c> records, Map<Id, Policy__c> oldRecords) {
		
		Id policyQueueId = getPolicyQueue().Id;
		// Map for Policy Record Type
		Map<String,Id> policyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
		

		List<Policy__c> policies = new List<Policy__c>();
		policies = getRecordsToProcess(records, oldRecords);
		// Map between Policy control field (unique) and master broker huon number
		Map<String, String> policyToBrokerMap = new Map<String,String>();

		// Map between Policy Control field (unique) and Underwriter
		Map<String, String> policyToUWMap = new Map<String,String>();

		// Map Between Policy Control field (unique) and Insured/client account.
		Map<String, String> policyToClientMap = new Map<String,String>();
				
		// differentiate between uploaded from Data loader or  manual entry
		for(Policy__c p : records){
			if(String.isBlank(p.Control_Field__c)){
				p.Control_Field__c = createPolicyControlField(p.Name,p.Effective_Date__c);
				if(p.Force_Renewal__c){
					p.Force_Renewal__c = false;
				}
			}
			p.Name = formatHUONNumber(p.Name);
			p.Master_Broker__c = formatHUONNumber(p.Master_Broker__c);
			p.Client_Number__c = formatHUONNumber(p.Client_Number__c);
			policyToUWMap.put(p.Control_Field__c, formatUWName(p.Policy_Underwriter__c));
			policyToBrokerMap.put(p.Control_Field__c, p.Master_Broker__c);
			policyToClientMap.put(p.Control_Field__c,p.Name);

		}

		Map<String, Id> underwriterInfo = findUnderwriter(policyToUWMap);
		Map<String, Id> clientInfo = findClientInfo(policyToClientMap);
		Map<String, Id> brokerInfo = findBrokerAccount(policyToBrokerMap);
		


		// update values stage and lookup fields 
		for(Policy__c p : policies){
			p.RecordTypeId = policyTypes.get('bkrMMGSL_Policy_Renewals');
			p.Policy_Stage__c = updatePolicyStage(p);
			p.bkrPolicy_Opportunity__c = null;
			p.Policy_Underwriter__c = formatUWName(p.Policy_Underwriter__c);
			p.Master_Broker_Account__c = brokerInfo.get(p.Control_Field__c) != null ? brokerInfo.get(p.Control_Field__c): null;
			p.OwnerId = underwriterInfo.get(p.Control_Field__c) !=null ? underwriterInfo.get(p.Control_Field__c) : policyQueueId;
			p.Client_Account__c = clientInfo.get(p.Control_Field__c) != null ? clientInfo.get(p.Control_Field__c): null;
		
			
		}
	}

	/**
    * @method   processPoliciesBeforeUpdate
    * @param	records Policy records that need to be updated
    * @param	oldRecords Old Policy records
    * @return 	N/A
    *
    * Process Policy records before updating
    * 
    */
	public void processPoliciesBeforeUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords) {

		List<Policy__c> policies = new List<Policy__c>();
		policies = getRecordsToProcess(records, oldRecords);
		// Map between opportunity Id and new Owner Id.
		Map<Id, Id> policyOwnerToUpdate = new Map<Id, Id>();

		for(Policy__c p : policies){

			p.Name = formatHUONNumber(p.Name);
			p.Master_Broker__c = formatHUONNumber(p.Master_Broker__c); 
			//For Force Renewal
			if(p.Policy_Stage__c!='Renewal Opportunity Created' && p.Policy_Stage__c != 'Renewal Cycle Initialized (Forced)' && p.Force_Renewal__c){
				p.Policy_Stage__c = 'Renewal Cycle Initialized (Forced)';
			}

			if(p.Qualified_Renewal__c != oldRecords.get(p.Id).Qualified_Renewal__c){
				p.Policy_Stage__c = 'Policy Record Created';

				p.Policy_Stage__c = updatePolicyStage(p);
			}

			
		}

	}

/**
    * @method   processPoliciesBeforeUpdate
    * @param	records Policy records that need to be updated
    * @param	oldRecords Old Policy records
    * @return 	N/A
    *
    * Process Policy records before updating
    * 
    */
	public void processPoliciesAfterUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords) {

		List<Policy__c> policies = new List<Policy__c>();
		policies = getRecordsToProcess(records, oldRecords);
		// Map between opportunity Id and new Owner Id.
		Map<Id, Id> policyOwnerToUpdate = new Map<Id, Id>();

		for(Policy__c p : policies){

			if(p.OwnerId != oldRecords.get(p.Id).OwnerId && p.Policy_Stage__c=='Renewal Opportunity Created'){
				if(p.bkrPolicy_Opportunity__c <> null){
					policyOwnerToUpdate.put(p.bkrPolicy_Opportunity__c, p.OwnerId);
				}
			}
		}

		if(policyOwnerToUpdate.size() > 0){
			reassignOpportunity(policyOwnerToUpdate);
		}
	}
	/**
    * @method   getRecordsToProcess
    * @param	records 		Policy records that need to be updated
    * @param	oldRecords 		Old Policy records
    * @return	List<Policy__c> return list of policy records need to be updated
    *
    * Process Policy records before based on the changes on certain fields
    * 
    */
	public List<Policy__c> getRecordsToProcess(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
		List<Policy__c> toProcess = new List<Policy__c>();
		for (Policy__c p : records) {
            Policy__c old = oldRecords != null && oldRecords.containsKey(p.Id) ? oldRecords.get(p.Id) : null;
            if (old == null) {
                toProcess.add(p); // always process on insert
            } else {
               	// only process if one of these field changes on update
                if (p.Force_Renewal__c != old.Force_Renewal__c ||
                    p.OwnerId!= old.OwnerId || 
                    p.Qualified_Renewal__c != old.Qualified_Renewal__c)
                 	{
                        toProcess.add(p);
                    }
            }
        }

        return toProcess;
	}

	/**
    * @method   formatHUONNumber
    * @param	needFormat 		Huon number to be formatted
    * @return	String 			formatted Huon Number
    *
    * Huon number from CI portfolio analysis need to be formatted in Salesforce
    * 
    */
	public String formatHUONNumber(String needFormat){
		String formattedPolicyNumber= '';
		if(String.isNotBlank(needFormat)){
			formattedPolicyNumber = needFormat.leftPad(10, ' ').replace(' ','0');
		}

		return formattedPolicyNumber;
	}


	/**
    * @method   updatePolicyStage
    * @param	policy 		Map contains Policy Control Field and Broker Account Huon Number
    * @return	String 		Return Policy Stage based on different criteria.   
    *
    * Method for checking different criteria for a policy and determine what will be the stage.
    * 
    */
	public String updatePolicyStage(Policy__c policy){
		String policyStage = 'Policy Record Created';

		if(Trigger.isInsert && policy.Policy_Stage__c != ''){
			policy.Policy_Stage__c = policyStage;
		}// when policies are getting cloned.

		if(!policy.Qualified_Renewal__c || policy.Term_Expiry_Date__c < Date.today().addDays(-30) ){
			policyStage='Renewal Not Supported';
		}
		else{
			if(PolicyService.isExpiryDateWithinThreshold(policy.Term_Expiry_Date__c)){
				if(policy.Policy_Stage__c == 'Policy Record Created'){
					policyStage='Renewal Cycle Initialized';
				}
			}
			else{
				if(!policy.Force_Renewal__c){
					policyStage = 'Waiting for Next Renewal Cycle';
				}
			} 
		}

		return policyStage;

	}

	/**
    * @method   isExpiryDateWithinThreshold
    * @param	expiryDate 		Map contains Policy Control Field and Broker Account Huon Number
    * @return	Boolean 		Returns true if the current policy expiry date is within threshold.   
    *
    * Method for checking if provided date in with the renewal threshold.
    * 
    */
	public static Boolean isExpiryDateWithinThreshold(Date expiryDate){		

		bkrMMGSL_CustomSettings__c mhc = bkrMMGSL_CustomSettings__c.getOrgDefaults();
		Date currentDate = Date.today();
		Integer threshold =  mhc.Auto_Renewal_ThresholdDays__c != null ? (Integer)mhc.Auto_Renewal_ThresholdDays__c : RENEWAL_THESHOLD_DAYS;
		Integer numberDaysDue = Math.abs(expiryDate.daysBetween(currentDate));
		if(expiryDate >= currentDate &&  numberDaysDue <= threshold){
			return true;
		}
		else if(expiryDate < currentDate){
			return expiryDate >= currentDate.addDays(-30) ? true : false; // also including 30 days old policy
		}

		return false;
	}


	/**
    * @method   findBrokerAccount
    * @param	underWriterName 	Map contains Policy Control Field and Broker Account Huon Number
    * @return	Map<String,Id> 		Returns Map between Policy Control field and SFDC Id
    *								of Broker Account.   
    *
    * Method for getting the Broker account SFDC Id for each policy to add in broker account lookup field.
    * 
    */
	private Map<String,Id> findBrokerAccount(Map<String,String> brokerHUONNumber){

		Map<String, Id> brokerInfo = new Map<String, Id>();
		Map<String, Id> brokerToPolicyMap = new Map<String,id>();

		List<String> huonList = new List<String>();
		if(brokerHUONNumber.size() > 0)
		{
			huonList = brokerHUONNumber.values();
		}

		List<Account> brokerAccountList = [Select Id, bkrAccount_HuonBrokerNumber__c from account where RecordTypeId =: accountTypes.get('bkrAccount_RecordTypeBkrMM') and bkrAccount_HuonBrokerNumber__c IN :huonList ];
		if(brokerAccountList.size()>0){
			for(Account a : brokerAccountList){
				brokerInfo.put(a.bkrAccount_HuonBrokerNumber__c,a.Id);			
			}
		}

		for(String mapKey : brokerHUONNumber.keySet()){

			Id brokerId = brokerInfo.get(brokerHUONNumber.get(mapKey));
			brokerToPolicyMap.put(mapKey, brokerId);
		}
		
		return brokerToPolicyMap;
	}

	/**
    * @method   findBrokerAccountsForOpportunity
    * @param	underWriterName 	Map contains Policy Control Field and Broker Account Huon Number
    * @return	Map<String,Id> 		Returns Map between Policy Control field and SFDC Id
    *								of Broker Account.   
    *
    * Method for getting the Broker account SFDC Id for each policy to add in broker account lookup field.
    * 
    */
	public Map<Id, Account> findBrokerAccountsForOpportunity(List<Id> accountIdList){

		Map<Id, Account> brokerInfoMap = new Map<Id, Account>();

		
		List<Account> brokerInfo = [Select Id, bkrAccount_AssignedKAM__c, bkrMMGSL_UnderwritingManager__c from account where RecordTypeId =: accountTypes.get('bkrAccount_RecordTypeBkrMM') and Id IN :accountIdList ];

		if(brokerInfo.size() > 0){
			for(Account a : brokerInfo){
				brokerInfoMap.put(a.Id, a);
			}
		}
		
		return brokerInfoMap;
	}

	/**
    * @method   findUnderwriter
    * @param	underWriterName 	Map contains Policy Control Field and underwriter name
    * @return	Map<String,Id> 		Returns Map between Policy Control field and SFDC Id
    *								of active UnderWriter   
    *
    * Method for getting the active underwriter SFDC id for each policy to add in Underwriter lookup field.
    * 
    */
	private Map<String,Id> findUnderwriter(Map<String,String> underWriterName){

		Map<String, Id> uwInfo = new Map<String, Id>();
		Map<String, Id> uwToPolicyMap = new Map<String,id>();

		List<String> underWriter = new List<String>();
		if(underWriterName.size()>0){
			underWriter = underWriterName.values();
			
		}
		
		List<User> userList = [Select Id, Name from User where Profile.Name =:PolicyService.MMGSL_PROFILE and IsActive = true and Name IN : underWriter];

		if(userList.size() > 0){
			for(User u : userList){
				uwInfo.put(u.Name,u.Id);			
			}
		}


		for(String mapKey : underWriterName.keySet()){
			Id brokerId = uwInfo.get(underWriterName.get(mapKey));
			uwToPolicyMap.put(mapKey, brokerId);
		}
		
		return uwToPolicyMap;
		
	}

	/**
    * @method   formatUWName
    * @param	uwName 	Underwrite name from file LastName FirstName / LastName, FirstName
    * @return	String 	Returns Formated underwriter name as FirstName LastName 
    *
    * Method for formatting UnderWriter Name.
    * 
    */
	private String formatUWName(String uwName){
		String policyUWName = '';
		if(String.isNotBlank(uwName)){
			if(uwName.contains(',')){
				uwName = uwName.replace(',', ' ');
			}
            uwName = uwName.trim();
			uwName = uwName.replaceAll('(\\s+)', ' ');
			String[] nameSplit = uwName.split(' ');
			if(nameSplit.size() > 1){
				policyUWName = nameSplit[1]+' '+nameSplit[0]; //CI portfolio User anem format is Last Name space First Name
			}
		}
		return policyUWName;
	}

	/**
    * @method   findClientInfo
    * @param	policyClientNo 	Map contains Policy Control Field and Insured Name
    * @return	Map<String,Id> 	Returns Map between Policy Control field and SFDC Id
    *							of the Insured Name(Client)   
    *
    * Method for getting the Client SFDC id for each policy to add in client lookup field.
    * 
    */
	private Map<String,Id> findClientInfo(Map<String,String> policyClientNo){

		Map<String, Id> clientInfo = new Map<String, Id>();
		Map<String, Id> clientToPolicyMap = new Map<String,id>();

		List<String> policyList = new List<String>();
		if(policyClientNo.size() > 0)
		{
			policyList = policyClientNo.values();
		}

		List<Policy_to_Client_Mapping__c> clientAccountMap = [Select Policy_number__c, Client_Account__c From Policy_to_Client_Mapping__c where Policy_number__c IN :policyList];

		if(clientAccountMap.size()>0){
			for(Policy_to_Client_Mapping__c p : clientAccountMap){
				clientInfo.put(p.Policy_number__c, p.Client_Account__c);
			}
		}

		for(String mapKey : policyClientNo.keySet()){

			Id clientId = clientInfo.get(policyClientNo.get(mapKey));
			clientToPolicyMap.put(mapKey, clientId);
		}

		return clientToPolicyMap;
	}


	/**
    * @method   processPoliciesForOpportunity
    * @param	records 	Policy records from which Opportunity will be created
    * @param	oldRecords 	Old Policy records		
    * @return	N/A   
    *Added the condition to handle inactive users
    * Method for calling opportunity service for creating opportunities.
    * 
    */
	public void processPoliciesForOpportunity(List<Policy__c> records){

		OpportunityService oppService = new OpportunityService();
		List<Policy__c> policyList = new List<Policy__c>();
		Id policyQueueId = getPolicyQueue().Id;
		List<Id> userIds = new List<Id>();
		for(Policy__c p : records){
			userIds.add(p.OwnerId);
		}

		List<User> active=[select Id,isActive from user where id in :userIds];
		Map<Id, User> inActiveUsers = new Map<Id, User>();

		if(!active.isEmpty() && active.size() > 0){
			for(User u : active){
				if(u.IsActive){
					inActiveUsers.put(u.Id, u);
				}
			}
		}

		for(Policy__c p : records){

			if(!inActiveUsers.containsKey(p.OwnerId) && p.Policy_Stage__c=='Waiting for Next Renewal Cycle'  && PolicyService.isExpiryDateWithinThreshold(p.Term_Expiry_Date__c) )
			{
				p.OwnerId=policyQueueId;
				
			}else if(p.OwnerId != policyQueueId && p.bkrPolicy_Opportunity__c == null){
					policyList.add(p);
			}
		}
		if(policyList.size() > 0){
			oppService.createRenewalOpportunities(policyList);
		}

	}

	/**
    * @method   getPolicyQueue
    * @param	N/A 		
    * @return	Group   Policy Queue
    *
    * Return Policy queue information.
    * 
    */
	public Group getPolicyQueue(){
		Group queue = new Group();
		String groupType = 'Queue';
		String policyQueueName = 'bkrMMGSL_Policy';
		List<Group> queueList = [Select Id, DeveloperName from Group where Type=:groupType and DeveloperName Like :policyQueueName];
		if(queueList.size() > 0){
			queue = queueList[0];
		}
		return queue;
	}

	/**
    * @method   createPolicyControlField
    * @param	policyNo 		policy number
    * @param	effectiveDate   Effective date for current policy
    * @return	String 			formatted Control field
    *
    * Control_Field__c in Policy object is currently acting as Primary key. It is a concatenation of 
    * Policy Number and effective date.
    * 
    */
	public String createPolicyControlField(String policyNo, Date effectiveDate){
		String controlField = '';
		if(String.isNotBlank(policyNo) && effectiveDate != null){
			Datetime dt = datetime.newInstance(effectiveDate.year(), effectiveDate.month(), effectiveDate.day());
			String dateFormat = dt.format('yyyyMMdd');
			controlField = policyNo.leftPad(12,' ').replace(' ','0')+ '-' + dateFormat;
		}

		return controlField;
	}

	/**
    * @method   isPolicyQualifiedForRenewal
    * @param	policy 		policy 
    * @return	Boolean 	return whether policy is qualified or not
    *
    * To create an opportunity for a policy, certain values are needed along with
    * expiry date. This method checks for a policy if these value are correct.
    * 
    */
	public Boolean isPolicyQualifiedForRenewal(Policy__c policy){
		String policyProduct = policy.Product_Code__c;
		String policyStatus = policy.Policy_Status__c;
		String region = policy.Region_Group__c;
		String regionalProgramCode = policy.Regional_Program_Code__c;
		String strategicSegment = policy.Strategic_Segment__c;
		
		Boolean isQualifiedForProduct = false;
		Boolean isQualifiedForStatus = false;
		Boolean isQualifiedForRegion = false;
		Boolean isRegionCodeValid=false;
		Boolean isStrategicSegmentValid = false;
		// check for product code
		if(String.isNotBlank(policyProduct) 
			&&(policyProduct.equalsIgnoreCase('COM') 
				||policyProduct.equalsIgnoreCase('CAP') 
				|| policyProduct.equalsIgnoreCase('GAR') ))
		{
			isQualifiedForProduct = true;
		}
		//check for Policy status
		if(String.isNotBlank(policyStatus) 
			&&(policyStatus.equalsIgnoreCase('Future') 
				||policyStatus.equalsIgnoreCase('Inforce') 
				|| policyStatus.equalsIgnoreCase('Unknown') ))
		{
			isQualifiedForStatus = true;
		}

		//check for regional code. Regional code can be empty
		if(regionalProgramCode == null || !regionalProgramCode.equalsIgnoreCase('9514') 
			&& !regionalProgramCode.equalsIgnoreCase('9187') 
				)
		{
			isRegionCodeValid = true;
		}

		//check for region 
		if(String.isNotBlank(region) 
			&&(region.equalsIgnoreCase('Atlantic') 
				||region.equalsIgnoreCase('Quebec') 
				||region.equalsIgnoreCase('Ontario') 
				||region.equalsIgnoreCase('Prairies')
				||region.equalsIgnoreCase('Pacific')
				))
		{
			isQualifiedForRegion = true;
		}

		//check for Strategic System
		if(String.isNotBlank(strategicSegment)){
			OpportunityService oppService = new OpportunityService();
			if(oppService.getStrategicSegmentMapping().get(strategicSegment) !=null)
				isStrategicSegmentValid = true;
		}
		
		return isQualifiedForProduct && isStrategicSegmentValid && isRegionCodeValid
		&& isQualifiedForStatus && isQualifiedForRegion;
	}

	/**
    * @method   updateOpportunityPolicyNumber
    * @param	records 		policy list
    * @return	void	
    *
    * After successfully creating opportunity from a policy, opportunity needs to be updated with 
    * policy number.
    * 
    */
	public void updateOpportunityPolicyNumber(List<Policy__c> records){
		
		List<Opportunity> oppToUpdateList = new List<Opportunity>();
		//Map < Opp id, Policy Id>
		Map<Id,Id> policyToOppMap = new Map<Id,Id>();
		if(records.size()>0){
			for(Policy__c p : records){
				if(p.bkrPolicy_Opportunity__c != null){
					policyToOppMap.put(p.bkrPolicy_Opportunity__c,p.Id);
				}
			}

			
			Set<Id> oppIdList = policyToOppMap.keySet();
			List<Opportunity> oppList = [Select Id, Renewal_Policy__c,AccountId From Opportunity where Id IN :oppIdList];
			if(oppList.size() > 0){
				for(Opportunity opp : oppList){
					opp.Renewal_Policy__c = policyToOppMap.get(opp.Id);
					oppToUpdateList.add(opp);
				}
			}

			if(oppToUpdateList.size() > 0){
				update oppToUpdateList;
			}
		}
	}

	/**
    * @method   updateOpportunityPolicyNumber
    * @param	records 		policy list
    * @return	void	
    *
    * After successfully creating opportunity from a policy, opportunity needs to be updated with 
    * policy number.
    * 
    */
	public void reassignOpportunity(Map<Id, Id> records){
		
		List<Opportunity> oppToUpdateList = new List<Opportunity>();
			
		Set<Id> oppIdList = records.keySet();
		List<Opportunity> oppList = [Select Id, OwnerId,AccountId From Opportunity where Id IN :oppIdList];

		if(oppList.size() > 0){
			for(Opportunity opp : oppList){
				Id oppOwnerId = records.get(opp.Id);
				if(opp.OwnerId != oppOwnerId){
					opp.OwnerId = oppOwnerId;
					oppToUpdateList.add(opp);
				}
			}
		}

		if(oppToUpdateList.size() > 0){
			update oppToUpdateList;
		}
		
	}
    /**
    * @method   processCancelledPolicies
    * @param	records 		policy list
    * @return	void	
    *
    * On change Policy status field to Cancel, delete the opportunity related to that
    * policy number.
    * 
    */
	public void processCancelledPolicies(List<Policy__c> records){
      	List<Id> opportunityIds = new List<Id>();
        for(Policy__c p : records){
            if(p.bkrPolicy_Opportunity__c != null && p.Policy_Status__c == 'Cancelled'){
                opportunityIds.add(p.bkrPolicy_Opportunity__c);
                p.bkrPolicy_Opportunity__c = null;
            }
        }
		
        List<Opportunity> opp = [Select Id from Opportunity where Id in :opportunityIds];
        if(!opp.isEmpty() &&  opp != null){
            Database.delete( opp);
        }
	}

}