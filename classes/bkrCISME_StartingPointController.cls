// Controller for Visualforce Page: bkrCISME_StartingPoint
public with sharing class bkrCISME_StartingPointController {
    
    // Variables and list names for "Brokerage Snapshots" and "KAM Scorecards"
    public String soql 									{get;set;}
    public List<bkrBrokerageSnapshot__c> snapshots 		{get;set;}
    public List<bkrBrokerageSnapshot__c> scorecards 	{get;set;}
    
    // Variables and list names for "Trading Desk"
    public String soqlCases 							{get;set;}
    public String soqlNBOpps 							{get;set;}
    public String soqlNBTeamOpps 						{get;set;}
    public String soqlRenewalOpps 						{get;set;}
    public String soqlOpenTasks							{get;set;}
    public String CurrentUserId 						{get;set;}
    public String NewBusiness 							{get;set;}  
    public String Renewal 								{get;set;}
    public List<case> openCaseLists 					{get;set;}
    public List<opportunity> openNBOpps 				{get;set;}
    public List<opportunityteammember> openNBTeamOpps 	{get;set;}
    public List<opportunity> openRenewalOpps 			{get;set;}
    public List<task> openTasks 						{get;set;}
    
    // Variables and list names for the "Plan vs. Actual" Progress Graph
    SObject sumQuota;
    SObject sumClosedOpps;
    
    // Sort the "Brokerage Snapshot" and "Kam Scorecard" Lists
    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }
    public String sortField {
        get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
        set;
    }
    
    // Sort the "Trading Desk" Lists 
    public String sortTdDir {
        get  { if (sortTdDir == null) {  sortTdDir = 'asc'; } return sortTdDir;  }
        set;
    }
    
    public String sortTdField {
        get  { if (sortTdField == null) {sortTdField = 'bkrPriorityScore__c'; } return sortTdField;}
        set;
    }
    
    public String sortTaskDir {
        get  { if (sortTaskDir == null) {  sortTaskDir = 'asc'; } return sortTaskDir;  }
        set;
    }
    
    public String sortTaskField {
        get  { if (sortTaskField == null) {sortTaskField = 'ActivityDate'; } return sortTaskField;}
        set;
    }
    
    // Initializes this controller and runs all queries when the page is loaded
    public bkrCISME_StartingPointController() {
        // SOQL string for "Brokerage Snapshots" and "KAM Scorecards"
        soql = 'SELECT bkrControl_DataRetreived__c,bkrControl_ExternalId__c,bkrControl_KAM__c,bkrControl_Region__c,bkrMMGSL_CFY_NewBusinessPlanVariance__c,bkrMMGSL_CFY_NewBusinessPlan__c,bkrMMGSL_CYTD_EarnedPremium__c,bkrMMGSL_CYTD_IncurredLosses__c,bkrMMGSL_CYTD_NewPoliciesBound__c,bkrMMGSL_CYTD_NewPremiumBound__c,bkrMMGSL_CYTD_QuotesIssued__c,bkrMMGSL_CYTD_QuoteToBindRatio__c,bkrMMGSL_CYTD_SubmissionsReceived__c,bkrMMGSL_CYTD_SubmissionToQuoteRatio__c,bkrMMGSL_CYTD_UndevelopedLossRatio__c,bkrMMGSL_CYTD_WrittenPremiumGrowth__c,bkrMMGSL_CYTD_WrittenPremium__c,bkrMMGSL_PYTD_EarnedPremium__c,bkrMMGSL_PYTD_UndevelopedLossRatio__c,bkrMMGSL_PYTD_WrittenPremium__c,brkMMGSL_PYTD_IncurredLosses__c,Id,Name FROM bkrBrokerageSnapshot__c where name != null';
        // SOQL string for "Trading Desk"
        CurrentUserId = Userinfo.getUserId();
        NewBusiness = 'New Business';
        Renewal = 'Renewal';
        soqlCases = 'SELECT id,contact.name,account.name,bkrPriorityScore__c,CreatedDate,CaseNumber,IsClosed,OwnerId,Status,Subject,Type FROM Case WHERE IsClosed != TRUE AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlNBOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,bkrProductLineEffectiveDate__c,account.name,bkrPriorityScore__c,bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,OwnerId,StageName,Type FROM Opportunity WHERE StageName != \'Closed Won - Pending Processing\' AND StageName != \'Closed Won - Policy Issued\' AND StageName != \'Closed Declined\' AND StageName != \'Closed Lost\' AND Type LIKE \''+String.escapeSingleQuotes(NewBusiness)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlNBTeamOpps = 'SELECT id,opportunity.bkrOpportunity_NextStepDate__c,opportunity.bkrProductLineEffectiveDate__c,opportunity.account.name,UserId,bkrPriorityScore__c,opportunity.bkrPriorityStatus_Colour__c,Opportunity.bkrClientAccount__c,Opportunity.bkrQuoteRequiredDate__c,Opportunity.CloseDate,Opportunity.Name,Opportunity.OwnerId,Opportunity.StageName,Opportunity.Type FROM OpportunityTeamMember WHERE Opportunity.IsClosed != TRUE AND Opportunity.Type LIKE \''+String.escapeSingleQuotes(NewBusiness)+'\' AND UserId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlRenewalOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,bkrProductLineEffectiveDate__c,account.name,bkrPriorityScore__c,bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,OwnerId,StageName,Type FROM Opportunity WHERE IsClosed != TRUE AND Type LIKE \''+String.escapeSingleQuotes(Renewal)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlOpenTasks = 'SELECT ActivityDate,Id,IsClosed,OwnerId,Status,Subject,WhatId,What.Name,WhoId FROM Task WHERE IsClosed = false AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        // Query for "Brokerage Snapshots" and "KAM Scorecards"
        runQuery();
        runKamQuery();
        // Query for "Trading Desk"
        runQueryCases();
    }
    
    // Query to Display the User's New Business Quota
    public SObject getSumQuota(){
        CurrentUserId = Userinfo.getUserId();
    	sumQuota = [SELECT SUM(QuotaAmount) sumMyQuota FROM ForecastingQuota WHERE QuotaOwnerId = :CurrentUserId];   
    	return sumQuota;
    }
    
    // Query to Display the User's Closed Opportunities
    public SObject getSumClosedOpps(){
        CurrentUserId = Userinfo.getUserId();
    	sumClosedOpps = [SELECT SUM(ForecastAmount) sumMyClosedOpps FROM ForecastingItem WHERE ForecastCategoryName = 'Closed' AND OwnerId = :CurrentUserId];   
    	return sumClosedOpps;
    }
    
    // Toggles the Sort Column for "KAM Scorecards"
    public void toggleSort() {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        runKamQuery();
    }

    // Toggles the Sort Column for "Trading Desk" 
    public void toggleTdSort() {
        sortTdDir = sortTdDir.equals('asc') ? 'desc' : 'asc';
        runQueryCases();
    }
    
    // Toggles the Sort Column for "Trading Desk (Tasks Only)" 
    public void toggleTaskSort() {
        sortTaskDir = sortTaskDir.equals('asc') ? 'desc' : 'asc';
        runQueryCases();
    }
    
    // Creates the "Brokerage Snapshot"
    public void runQuery() {    
        try {
            snapshots = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 1');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while loading the Brokerage Snapshot.'));
        }        
    }
    
    // Creates the "KAM Scorecard"
    public void runKamQuery() {    
        try {
            scorecards = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 50');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while loading the KAM Scorecard.'));
        }        
    }
    
    // Creates the "Trading Desk"
    public void runQueryCases() {    
        try {
            openCaseLists = Database.query(soqlCases + ' order by ' + sortTdField + ' ' + sortTdDir + ' limit 100');
            openNBOpps = Database.query(soqlNBOpps + ' order by ' + sortTdField + ' ' + sortTdDir + ' limit 100');
            openNBTeamOpps = Database.query(soqlNBTeamOpps + ' order by ' + sortTdField + ' ' + sortTdDir + ' limit 100');
            openRenewalOpps = Database.query(soqlRenewalOpps + ' order by ' + sortTdField + ' ' + sortTdDir + ' limit 100');
            openTasks = Database.query(soqlOpenTasks + ' order by ' + sortTaskField + ' ' + sortTaskDir + ' limit 100');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while loading your Trading Desk.'));
        }        
    }
    
    // "Brokerage Snapshot"
    // Sets the variables, "accountName" and "region" which will perform the Javascript search "runSearch" on our Visualforce page
    public PageReference runSearch() {        
        String accountName = Apexpages.currentPage().getParameters().get('accountName');
        String region = Apexpages.currentPage().getParameters().get('region');
        soql = 'SELECT bkrControl_DataRetreived__c,bkrControl_ExternalId__c,bkrControl_KAM__c,bkrControl_Region__c,bkrMMGSL_CFY_NewBusinessPlanVariance__c,bkrMMGSL_CFY_NewBusinessPlan__c,bkrMMGSL_CYTD_EarnedPremium__c,bkrMMGSL_CYTD_IncurredLosses__c,bkrMMGSL_CYTD_NewPoliciesBound__c,bkrMMGSL_CYTD_NewPremiumBound__c,bkrMMGSL_CYTD_QuotesIssued__c,bkrMMGSL_CYTD_QuoteToBindRatio__c,bkrMMGSL_CYTD_SubmissionsReceived__c,bkrMMGSL_CYTD_SubmissionToQuoteRatio__c,bkrMMGSL_CYTD_UndevelopedLossRatio__c,bkrMMGSL_CYTD_WrittenPremiumGrowth__c,bkrMMGSL_CYTD_WrittenPremium__c,bkrMMGSL_PYTD_EarnedPremium__c,bkrMMGSL_PYTD_UndevelopedLossRatio__c,bkrMMGSL_PYTD_WrittenPremium__c,brkMMGSL_PYTD_IncurredLosses__c,Id,Name FROM bkrBrokerageSnapshot__c WHERE name != null';
        if (!accountName.equals(''))
            soql += ' and name LIKE \'%'+String.escapeSingleQuotes(accountName)+'%\'';
        if (!region.equals(''))
            soql += ' and bkrControl_Region__c includes (\''+region+'\')';
        else soql += ' and bkrControl_Region__c = null';
        // Runs the query again with the updated values entered by our user in the Visualforce page
        runQuery();
        return null;
    }
    
    // "KAM Scorecard"
    // Sets the variables "accountName" and "region" which will perform the Javascript search "runKamSearch" on our Visualforce page
    public PageReference runKamSearch() {        
        String kamName = Apexpages.currentPage().getParameters().get('kamName');
        soql = 'SELECT bkrControl_DataRetreived__c,bkrControl_ExternalId__c,bkrControl_KAM__c,bkrControl_Region__c,bkrMMGSL_CFY_NewBusinessPlanVariance__c,bkrMMGSL_CFY_NewBusinessPlan__c,bkrMMGSL_CYTD_EarnedPremium__c,bkrMMGSL_CYTD_IncurredLosses__c,bkrMMGSL_CYTD_NewPoliciesBound__c,bkrMMGSL_CYTD_NewPremiumBound__c,bkrMMGSL_CYTD_QuotesIssued__c,bkrMMGSL_CYTD_QuoteToBindRatio__c,bkrMMGSL_CYTD_SubmissionsReceived__c,bkrMMGSL_CYTD_SubmissionToQuoteRatio__c,bkrMMGSL_CYTD_UndevelopedLossRatio__c,bkrMMGSL_CYTD_WrittenPremiumGrowth__c,bkrMMGSL_CYTD_WrittenPremium__c,bkrMMGSL_PYTD_EarnedPremium__c,bkrMMGSL_PYTD_UndevelopedLossRatio__c,bkrMMGSL_PYTD_WrittenPremium__c,brkMMGSL_PYTD_IncurredLosses__c,Id,Name FROM bkrBrokerageSnapshot__c WHERE name !=null';
        if (!kamName.equals(''))
            soql += ' and bkrControl_KAM__c includes (\''+kamName+'\')';
        else soql += ' and bkrControl_KAM__c = null';
        // Runs the query again with the updated values entered by our user in the Visualforce page
        runKamQuery();
        return null;
    }
    
    // "Trading Desk"
    // Enables the trading desk to be refreshed by the user 
    public PageReference runTradingDeskSearch() {        
        CurrentUserId = Userinfo.getUserId();
        NewBusiness = 'New Business';
        Renewal = 'Renewal';
        soqlCases = 'SELECT id,contact.name,account.name,bkrPriorityScore__c,CreatedDate,CaseNumber,IsClosed,OwnerId,Status,Subject,Type FROM Case WHERE IsClosed != TRUE AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlNBOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,bkrProductLineEffectiveDate__c,account.name,bkrPriorityScore__c,bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,OwnerId,StageName,Type FROM Opportunity WHERE StageName != \'Closed Won - Pending Processing\' AND StageName != \'Closed Won - Policy Issued\' AND StageName != \'Closed Declined\' AND StageName != \'Closed Lost\' AND Type LIKE \''+String.escapeSingleQuotes(NewBusiness)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlNBTeamOpps = 'SELECT id,opportunity.bkrOpportunity_NextStepDate__c,opportunity.account.name,UserId,bkrPriorityScore__c,opportunity.bkrPriorityStatus_Colour__c,Opportunity.bkrClientAccount__c,Opportunity.bkrQuoteRequiredDate__c,Opportunity.CloseDate,Opportunity.Name,Opportunity.OwnerId,Opportunity.StageName,Opportunity.Type FROM OpportunityTeamMember WHERE Opportunity.IsClosed != TRUE AND Opportunity.Type LIKE \''+String.escapeSingleQuotes(NewBusiness)+'\' AND UserId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlRenewalOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,account.name,bkrPriorityScore__c,bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,OwnerId,StageName,Type FROM Opportunity WHERE IsClosed != TRUE AND Type LIKE \''+String.escapeSingleQuotes(Renewal)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlOpenTasks = 'SELECT ActivityDate,Id,IsClosed,OwnerId,Status,Subject,WhatId,What.Name,WhoId FROM Task WHERE IsClosed = false AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        runQueryCases();
        return null;
    }    
    
	// "Brokerage Snapshot"
    // Creates a picklist from values in the "bkrBrokerageSnapshot__c.bkrControl_Region__c" field
    // Sets the name of our new variable to kamNames
    public List<String> regions {
        get {
            if (regions == null) {                
                regions = new List<String>();
                Schema.DescribeFieldResult field = bkrBrokerageSnapshot__c.bkrControl_Region__c.getDescribe();                
                for (Schema.PicklistEntry f : field.getPicklistValues())
                    regions.add(f.getLabel());               
            }
            return regions;          
        }
        set;
    }
    
    // "KAM Scorecard"
    // Creates a picklist from values in the "bkrBrokerageSnapshot__c.bkrControl_KAM__c" field
    // Sets the name of our new variable to "kamNames"
    public List<String> kamNames {
        get {
            if (kamNames == null) {                
                kamNames = new List<String>();
                Schema.DescribeFieldResult field = bkrBrokerageSnapshot__c.bkrControl_KAM__c.getDescribe();                
                for (Schema.PicklistEntry f : field.getPicklistValues())
                    kamNames.add(f.getLabel());               
            }
            return kamNames;          
        }
        set;
    }
}