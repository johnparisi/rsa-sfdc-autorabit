/**
  * @author        Saumil Bapat
  * @date          10/25/2016
  * @description   Test class for SLID_ESB_MappingUtils
*/
@isTest
private Class SLID_ESB_MappingUtils_Test {

    static testMethod void testMappingFramework() {
        List<String> keyVars = new List<String>();
        keyVars.add('Key1');
        keyVars.add('Key2');

        List<String> typeVars = new List<String>();
        typeVars.add('Owner');
        typeVars.add('CaseStatus');

        List<SLID_Mapping_KeyValue__c> keyVals = new List<SLID_Mapping_KeyValue__c>();
        keyVals.add(new SLID_Mapping_KeyValue__c(Name = 'Key1', Type__c = 'Owner', Value__c = 'Val1'));
        keyVals.add(new SLID_Mapping_KeyValue__c(Name = 'Key2', Type__c = 'Owner', Value__c = 'Val2'));
        keyVals.add(new SLID_Mapping_KeyValue__c(Name = 'Key1', Type__c = 'CaseStatus', Value__c = 'Val3'));
        keyVals.add(new SLID_Mapping_KeyValue__c(Name = 'Key2', Type__c = 'CaseStatus', Value__c = 'Val4'));
        insert keyVals;

        List<String> keyValFilters = new List<String>();
        keyValFilters.add('Key1_Owner');
        keyValFilters.add('Key2_CaseStatus');

        //Should query and add 2 values into the keyValueMap in SLID_ESB_MappingUtils
        SLID_ESB_MappingUtils.instantiateWithFilterValues(keyValFilters);

        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key1','Owner'), 'Val1');
        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key2','Owner'), null);
        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key1','CaseStatus'), null);
        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key2','CaseStatus'), 'Val4');

        //Should query and add all 4 values into the keyValueMap in SLID_ESB_MappingUtils
        SLID_ESB_MappingUtils.instantiateWithKeyType(keyVars, typeVars);

        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key1','Owner'), 'Val1');
        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key2','Owner'), 'Val2');
        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key1','CaseStatus'), 'Val3');
        System.AssertEquals(SLID_ESB_MappingUtils.getMappedValue('Key2','CaseStatus'), 'Val4');
    }

    static testMethod void testExceptionHandling() {
        List<String> keyVars = new List<String>();
        keyVars.add('Key1');

        List<String> typeVars = new List<String>();
        typeVars.add('Owner');

        List<String> keyValFilters = new List<String>();
        keyValFilters.add('Key1_Owner');

        //Should query and add 2 values into the keyValueMap in SLID_ESB_MappingUtils
        SLID_ESB_MappingUtils.instantiateWithFilterValues(keyValFilters);

        //Should query and add all 4 values into the keyValueMap in SLID_ESB_MappingUtils
        SLID_ESB_MappingUtils.instantiateWithKeyType(keyVars, typeVars);

        String retrieveValue = SLID_ESB_MappingUtils.getMappedValue('Key1','Owner');

    }
    
    static testMethod void testExceptions_instantiateWithKeyType() {
        //Instantiate failure break point
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'instantiateWithKeyType';
        
        try 
        {
            List<String> keyVars = new List<String>();
            keyVars.add('Key1');
    
            List<String> typeVars = new List<String>();
            typeVars.add('Owner');
    
            List<String> keyValFilters = new List<String>();
            keyValFilters.add('Key1_Owner');
    
            //Should query and add 2 values into the keyValueMap in SLID_ESB_MappingUtils
            SLID_ESB_MappingUtils.instantiateWithFilterValues(keyValFilters);
    
            //Should query and add all 4 values into the keyValueMap in SLID_ESB_MappingUtils
            SLID_ESB_MappingUtils.instantiateWithKeyType(keyVars, typeVars);
    
            String retrieveValue = SLID_ESB_MappingUtils.getMappedValue('Key1','Owner');
        }
        
        catch (Exception ex)
        {
            System.AssertEquals(ex.getMessage() , 'Test Exception');
        }
    }
    static testMethod void testExceptions_instantiateWithFilterValues() {
        //Instantiate failure break point
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'instantiateWithFilterValues';
        
        try
        {
            List<String> keyVars = new List<String>();
            keyVars.add('Key1');
    
            List<String> typeVars = new List<String>();
            typeVars.add('Owner');
    
            List<String> keyValFilters = new List<String>();
            keyValFilters.add('Key1_Owner');
    
            //Should query and add 2 values into the keyValueMap in SLID_ESB_MappingUtils
            SLID_ESB_MappingUtils.instantiateWithFilterValues(keyValFilters);
    
            //Should query and add all 4 values into the keyValueMap in SLID_ESB_MappingUtils
            SLID_ESB_MappingUtils.instantiateWithKeyType(keyVars, typeVars);
    
            String retrieveValue = SLID_ESB_MappingUtils.getMappedValue('Key1','Owner');
        }
        catch (Exception ex)
        {
            System.AssertEquals(ex.getMessage() , 'Test Exception');
        }
    }
    static testMethod void testExceptions_getMappedValue() {
        //Instantiate failure break point
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'getMappedValue';
        
        try
        {
            List<String> keyVars = new List<String>();
            keyVars.add('Key1');
    
            List<String> typeVars = new List<String>();
            typeVars.add('Owner');
    
            List<String> keyValFilters = new List<String>();
            keyValFilters.add('Key1_Owner');
    
            //Should query and add 2 values into the keyValueMap in SLID_ESB_MappingUtils
            SLID_ESB_MappingUtils.instantiateWithFilterValues(keyValFilters);
    
            //Should query and add all 4 values into the keyValueMap in SLID_ESB_MappingUtils
            SLID_ESB_MappingUtils.instantiateWithKeyType(keyVars, typeVars);
    
            String retrieveValue = SLID_ESB_MappingUtils.getMappedValue('Key1','Owner');
        }
        catch (Exception ex)
        {
            System.AssertEquals(ex.getMessage() , 'Test Exception');
        }
    }
}