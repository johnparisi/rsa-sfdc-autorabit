/**
  * @author        Saumil Bapat
  * @date          11/3/2016
  * @description   Batch class to create new broker mapping records
*/
global class SLID_CreateBrokerMapping implements Database.Batchable<sObject>
{
   private final Boolean isManuallyTriggered;
   
   //Query of logs to delete
   global String query;
   
   private static final String batchCompleteMessage = 'Batch Process to Update Broker Mapping Data is completed';
   private static final String noErrorEncounteredMessage = 'No Errors encountered in the batch job';

   //Type__c value for scrambled Broker records in the object SLID_Mapping_KeyValue__c 
   public Static Final String brokerTypeValue = 'Broker';
   
   //Type__c value for unscrambled Broker number mapping to account Id in the object SLID_Mapping_KeyValue__c
   public Static Final String brokerTypeUnscrambled = 'Broker_US';
    
   //Constructor to instantiate the query
   global SLID_CreateBrokerMapping(Boolean manuallyTriggered)
   {
   	  this.isManuallyTriggered = manuallyTriggered;
      this.query = 'Select Id, Account__c, Account_US__c, AGENT__c, PARENT1_BROKER_SCRAMBLED__c ,PARENT2_BROKER_SCRAMBLED__c , PARENT3_BROKER_SCRAMBLED__c , BROKER_NUMBER__c, PARENT1_BROKER_NUMBER__c, PARENT2_BROKER_NUMBER__c, PARENT3_BROKER_NUMBER__c from Broker_Stage__c';
   }
   //Start method for the batch
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   //Execute method for the batch
   global void execute(Database.BatchableContext BC,List<SObject> scope)
   {
   		createMappingsWithUnscrambledBrokerNumbers(scope);   
   		createMappingsWithScrmabledBrokerNumbers(scope);
   }

    //Finish method for the batch
    global void finish(Database.BatchableContext BC)
    {
      System.Debug('~~~Broker Mapping Creation Complete');
	  
	  if(isManuallyTriggered == false)
	  {
  		 AsyncApexJob job = [Select Id , Status , NumberOfErrors
                      From AsyncApexJob
                      Where Id =: BC.getJobId()];
	
	      if(job != null && (job.NumberOfErrors == 0 || job.NumberOfErrors == null))
	      {
	          //Initiate a create broker mapping batch job
	          System.Debug('~~~Create Broker Mapping Complete');
	          SLID_UpdateBrokerFieldsFromMI updateMIFieldsBatch = new SLID_UpdateBrokerFieldsFromMI();
	          Id updateMIfieldsBatchId = Database.ExecuteBatch(updateMIFieldsBatch);
	          system.debug('~~~createBrokerMappingBatchId: ' + updateMIfieldsBatchId);
	      }
	      else
	      {
	        //Check if the admin email has been defined
	        String adminEmail = SLID_PurgeBrokerMapping.brokerMappingSettings.Batch_Job_Admin_Email__c;
	        if (adminEmail != null && adminEmail != '')
	        {
	          NotificationEmailForErrors(job.NumberOfErrors);
	        }
	      }
	  }
	  else
	  {
      //Send batch job completion email
      	sendCompletionEmail(BC);
	  }
    }
    
    
    private void createMappingsWithUnscrambledBrokerNumbers(List<Sobject> scope)
    {
    	//Initialize a set of brokerNumbers to be used as a filter on the account query
      Set<String> BrokerNumbers = new Set<String>();
      
      //list of accounts which reference this set of broker records
      List<Account> brokerAccounts = new List<Account>();
      
      //Create a map from brokerNumber  to account
      Map<String, Id> brokerNumberAccountMap = new Map<String, Id>();
      
      //Create a list for the SLID mapping object
      List<SLID_Mapping_KeyValue__c> brokerKeyValueRecords = new List<SLID_Mapping_KeyValue__c>();
      List<Broker_Stage__c> brokerStageRecordsToupdate = new List<Broker_Stage__c>();

      try 
      {
          for (SObject o : scope)
          {
            //Case the generic sObject from scope into a broker record. 
            Broker_Stage__c broker = (Broker_Stage__c) o;
            
            //Add add the broker numbers into the filter set
            BrokerNumbers.add(broker.BROKER_NUMBER__c);
            BrokerNumbers.add(broker.PARENT1_BROKER_NUMBER__c);
            BrokerNumbers.add(broker.PARENT2_BROKER_NUMBER__c);
            BrokerNumbers.add(broker.PARENT3_BROKER_NUMBER__c);
          }
          
          if(!BrokerNumbers.isEmpty())
          {
              brokerAccounts = [Select Id, bkrAccount_HuonBrokerNumber__c 
                                from Account 
                                where bkrAccount_HuonBrokerNumber__c IN :BrokerNumbers];
        
              for (Account brokerAccount : brokerAccounts)
              {
                //Add pair of brokerNumber->Account.Id to the map
                brokerNumberAccountMap.put(brokerAccount.bkrAccount_HuonBrokerNumber__c, brokerAccount.Id);
              }
              
              //Iterate over the list of brokers keys to be initialized in SLID mapping object
              for (Sobject o : scope)
              {
                //Case the generic sObject from scope into a broker record. 
                Broker_Stage__c broker = (Broker_Stage__c) o;
                
                //Key value pair for this broker record
                SLID_Mapping_KeyValue__c brokerKeyValueRecord;
        
                //Check if a account has this broker number
                if (broker.BROKER_NUMBER__c != null && brokerNumberAccountMap.get(broker.BROKER_NUMBER__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(broker.BROKER_NUMBER__c, brokerNumberAccountMap.get(broker.BROKER_NUMBER__c),brokerTypeUnscrambled)
                  ); 
                  broker.Account_US__c = brokerNumberAccountMap.get(broker.BROKER_NUMBER__c);
                  brokerStageRecordsToupdate.add(broker);
                }
        
                //else check if a account has the parent1 broker number
                else if (broker.BROKER_NUMBER__c != null && broker.PARENT1_BROKER_NUMBER__c != null && brokerNumberAccountMap.get(broker.PARENT1_BROKER_NUMBER__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(broker.BROKER_NUMBER__c, brokerNumberAccountMap.get(broker.PARENT1_BROKER_NUMBER__c),brokerTypeUnscrambled)
                  );  
                  broker.Account_US__c = brokerNumberAccountMap.get(broker.PARENT1_BROKER_NUMBER__c);
                  brokerStageRecordsToupdate.add(broker);
                }
                
                //else check if a account has the parent2 broker number
                else if (broker.BROKER_NUMBER__c != null && broker.PARENT2_BROKER_NUMBER__c != null && brokerNumberAccountMap.get(broker.PARENT2_BROKER_NUMBER__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(broker.BROKER_NUMBER__c, brokerNumberAccountMap.get(broker.PARENT2_BROKER_NUMBER__c),brokerTypeUnscrambled)
                  ); 
                  broker.Account_US__c = brokerNumberAccountMap.get(broker.PARENT2_BROKER_NUMBER__c);
                  brokerStageRecordsToupdate.add(broker);
                }
                
                //else check if a account has the parent3 broker number
                else if (broker.BROKER_NUMBER__c != null && broker.PARENT3_BROKER_NUMBER__c != null && brokerNumberAccountMap.get(broker.PARENT3_BROKER_NUMBER__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(broker.BROKER_NUMBER__c, brokerNumberAccountMap.get(broker.PARENT3_BROKER_NUMBER__c),brokerTypeUnscrambled)
                  ); 
                  broker.Account_US__c = brokerNumberAccountMap.get(broker.PARENT3_BROKER_NUMBER__c);
                  brokerStageRecordsToupdate.add(broker);
                }
              } 
              if (brokerKeyValueRecords.size() > 0)
              {
                insert brokerKeyValueRecords;
              } 
              if(!brokerStageRecordsToupdate.isEmpty())
                update brokerStageRecordsToupdate;
          }
      }
      catch (Exception e)
      {
        //Create a custom exception record
        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_CreateBrokerMapping','createMappingsWithUnscrambledBrokerNumbers','',UTIL_Logging.DEBUG_LEVEL_ERROR);
        UTIL_Logging.logException(log); 
        throw e;
      }
    	
    }
    
    private void createMappingsWithScrmabledBrokerNumbers(List<Sobject> scope)
    {
    	    	//Initialize a set of brokerNumbers to be used as a filter on the account query
      Set<Double> BrokerNumbers = new Set<Double>();
      
      //list of accounts which reference this set of broker records
      List<Account> brokerAccounts = new List<Account>();
      
      //Create a map from brokerNumber  to account
      Map<Double, Id> brokerNumberAccountMap = new Map<Double, Id>();
      
      //Create a list for the SLID mapping object
      List<SLID_Mapping_KeyValue__c> brokerKeyValueRecords = new List<SLID_Mapping_KeyValue__c>();
      List<Broker_Stage__c> brokerStageRecordsToupdate = new List<Broker_Stage__c>();

      try 
      {
          for (SObject o : scope)
          {
            //Case the generic sObject from scope into a broker record. 
            Broker_Stage__c broker = (Broker_Stage__c) o;
            
            //Add add the broker numbers into the filter set
            BrokerNumbers.add(broker.AGENT__c);
            BrokerNumbers.add(broker.PARENT1_BROKER_SCRAMBLED__c);
            BrokerNumbers.add(broker.PARENT2_BROKER_SCRAMBLED__c);
            BrokerNumbers.add(broker.PARENT3_BROKER_SCRAMBLED__c);
          }
          
          if(!BrokerNumbers.isEmpty())
          {
              brokerAccounts = [Select Id, AGENT__c
                                from Account 
                                where AGENT__c IN :BrokerNumbers];
        
              for (Account brokerAccount : brokerAccounts)
              {
                //Add pair of brokerNumber->Account.Id to the map
                brokerNumberAccountMap.put(brokerAccount.AGENT__c, brokerAccount.Id);
              }
              
              //Iterate over the list of brokers keys to be initialized in SLID mapping object
              for (Sobject o : scope)
              {
                //Case the generic sObject from scope into a broker record. 
                Broker_Stage__c broker = (Broker_Stage__c) o;
                
                //Key value pair for this broker record
                SLID_Mapping_KeyValue__c brokerKeyValueRecord;
        
                //Check if a account has this broker number
                if (broker.AGENT__c != null && brokerNumberAccountMap.get(broker.AGENT__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(String.valueOf(broker.AGENT__c), brokerNumberAccountMap.get(broker.AGENT__c),brokerTypeValue)
                  ); 
                  broker.Account__c = brokerNumberAccountMap.get(broker.AGENT__c);
                  brokerStageRecordsToupdate.add(broker);
                }
        
                //else check if a account has the parent1 broker number
                else if (broker.AGENT__c != null && broker.PARENT1_BROKER_SCRAMBLED__c != null && brokerNumberAccountMap.get(broker.PARENT1_BROKER_SCRAMBLED__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(String.valueOf(broker.AGENT__c), brokerNumberAccountMap.get(broker.PARENT1_BROKER_SCRAMBLED__c),brokerTypeValue)
                  );  
                  broker.Account__c = brokerNumberAccountMap.get(broker.PARENT1_BROKER_SCRAMBLED__c);
                  brokerStageRecordsToupdate.add(broker);
                }
                
                //else check if a account has the parent2 broker number
                else if (broker.AGENT__c != null && broker.PARENT2_BROKER_SCRAMBLED__c != null && brokerNumberAccountMap.get(broker.PARENT2_BROKER_SCRAMBLED__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(String.valueOf(broker.AGENT__c), brokerNumberAccountMap.get(broker.PARENT2_BROKER_SCRAMBLED__c),brokerTypeValue)
                  ); 
                  broker.Account__c = brokerNumberAccountMap.get(broker.PARENT2_BROKER_SCRAMBLED__c);
                  brokerStageRecordsToupdate.add(broker);
                }
                
                //else check if a account has the parent3 broker number
                else if (broker.AGENT__c != null && broker.PARENT3_BROKER_SCRAMBLED__c != null && brokerNumberAccountMap.get(broker.PARENT3_BROKER_SCRAMBLED__c) != null)
                {
                  brokerKeyValueRecords.add(
                    createBrokerKeyValueRecord(String.valueOf(broker.AGENT__c), brokerNumberAccountMap.get(broker.PARENT3_BROKER_SCRAMBLED__c),brokerTypeValue)
                  ); 
                  broker.Account__c = brokerNumberAccountMap.get(broker.PARENT3_BROKER_SCRAMBLED__c);
                  brokerStageRecordsToupdate.add(broker);
                }
              } 
              if (brokerKeyValueRecords.size() > 0)
              {
                insert brokerKeyValueRecords;
              } 
              if(!brokerStageRecordsToupdate.isEmpty())
                update brokerStageRecordsToupdate;
          }
      }
      catch (Exception e)
      {
        //Create a custom exception record
        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_CreateBrokerMapping','createMappingsWithScrmabledBrokerNumbers','',UTIL_Logging.DEBUG_LEVEL_ERROR);
        UTIL_Logging.logException(log); 
        throw e;
      }
    	
    }

    //Create a new mapping record for this broker based on the key & value
    private SLID_Mapping_KeyValue__c createBrokerKeyValueRecord(String key, String value , String brokerType)
    {
      SLID_Mapping_KeyValue__c brokerKeyValueRecord;
      try 
      {
        brokerKeyValueRecord = new SLID_Mapping_KeyValue__c();
        brokerKeyValueRecord.Name = key;
        brokerKeyValueRecord.Value__c = value;
        brokerKeyValueRecord.Type__c = brokerType;
        brokerKeyValueRecord.Query_Filter_Value__c = key + '_' + brokerTypeValue;
      }
      catch (Exception e)
      {
        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_CreateBrokerMapping','createBrokerKeyValueRecord','','Error');
        UTIL_Logging.logException(log); 
        throw e; 
      }
      return brokerKeyValueRecord;
    }
   
   private void sendCompletionEmail(Database.BatchableContext bc)
   {
      //Retrieve the email to send to
      SLID_Broker_Mapping_Settings__c brokerMappingSettings = SLID_Broker_Mapping_Settings__c.getInstance();
      
      AsyncApexJob job = [Select Id , Status , NumberOfErrors 
                          From AsyncApexJob 
                          Where Id =: bc.getJobId()];
                          
      String emailBody = batchCompleteMessage + '<br/>';
      if(job.NumberOfErrors == 0 || job.NumberOfErrors == null)
        emailBody += noErrorEncounteredMessage;
      else if(job.NumberOfErrors > 0)
        emailBody += 'The batch job encountered ' + job.NumberOfErrors + ' errors.';
      
      

      //Reserve a email capacity
      Messaging.reserveSingleEmailCapacity(1);
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {brokerMappingSettings.Batch_Job_Admin_Email__c}; 
      mail.setToAddresses(toAddresses);
      mail.setSenderDisplayName('Salesforce Batch Apex');
      mail.setSubject('Broker Mapping Data Update Complete');
      mail.setBccSender(false);
      mail.setUseSignature(false);
      mail.setHTMLBody(emailBody);
      if(!Test.isRunningTest())
      	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   } 
   
   public void NotificationEmailForErrors(Integer noOfErrors)
   {
      String emailBody = 'Batch Process to Create Broker Mapping Data is completed' + '<br/>';
      emailBody += 'The batch job encountered ' + noOfErrors + ' errors.';

        //Reserve a email capacity
      Messaging.reserveSingleEmailCapacity(1);
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {SLID_PurgeBrokerMapping.brokerMappingSettings.Batch_Job_Admin_Email__c};
      mail.setToAddresses(toAddresses);
      mail.setSenderDisplayName('Salesforce Batch Apex');
      mail.setSubject('Broker Mapping Data Update Complete');
      mail.setBccSender(false);
      mail.setUseSignature(false);
      mail.setHTMLBody(emailBody);
      if(!Test.isRunningTest())
      	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }
}