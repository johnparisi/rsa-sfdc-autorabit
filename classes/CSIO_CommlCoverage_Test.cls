/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_CommlCoverage.
 */
@isTest
public class CSIO_CommlCoverage_Test
{
    @testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    static testMethod void testConvertForDefaultCoverage()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            
            Risk__c location = SMEQ_TestDataGenerator.getLocationRisk();
            location = [
                SELECT id, Building_Value__c, Annual_Rental_Revenue__c
                FROM Risk__c
                WHERE id = :location.Id
            ];
            location.Building_Value__c = 5000;
            location.Annual_Rental_Revenue__c = 500;
            update location;
            
            String coveragePackage = 'Standard';
            String coverageType = 'Building';
            Coverages__c c = SMEQ_TestDataGenerator.getCoverage(location, coveragePackage, coverageType);
            c.Selected__c = true;
            
            Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageCd');
            Map<String, String> esbCoverageDesc = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageDesc');
            
            CSIO_CommlCoverage ccc = new CSIO_CommlCoverage(c);
            String output = '';
            output += ccc;
            
            System.assertEquals(
                '<CommlCoverage>' +
                ' <CoverageCd>' + esbCoverageCd.get(coverageType) + '</CoverageCd>' +
                ' <CoverageDesc>' + esbCoverageDesc.get(coverageType) + '</CoverageDesc>' +
                ' <Limit><FormatCurrencyAmt> <Amt>' + location.Building_Value__c + '</Amt></FormatCurrencyAmt> </Limit>' + 
                ' <Deductible><FormatCurrencyAmt> <Amt>' + c.Deductible_Value__c + '</Amt></FormatCurrencyAmt> </Deductible>' + 
                ' <rsa:Indicator>1</rsa:Indicator>' +
                '</CommlCoverage>',
                output
            );
        }
    }
    
    static testMethod void testConvertForExtraCoverage()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            
            Risk__c location = SMEQ_TestDataGenerator.getLocationRisk();
            location = [
                SELECT id, Building_Value__c, Annual_Rental_Revenue__c
                FROM Risk__c
                WHERE id = :location.Id
            ];
            location.Building_Value__c = 5000;
            location.Annual_Rental_Revenue__c = 500;
            update location;
            
            String coveragePackage = 'Standard';
            String coverageType = 'Flood';
            Coverages__c c = SMEQ_TestDataGenerator.getExtraCoverage(location, coveragePackage, coverageType);
            c.Selected__c = true;
            
            Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageCd');
            Map<String, String> esbCoverageDesc = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageDesc');
            
            CSIO_CommlCoverage ccc = new CSIO_CommlCoverage(c);
            String output = '';
            output += ccc;
            
            System.assertEquals(
                output, 
                ''
            );
            
            c.Limit_Value__c = 1000000;
            c.Deductible_Value__c = 1000;
            ccc = new CSIO_CommlCoverage(c);
            output = '';
            output += ccc;
            
            System.assertEquals(
                '<CommlCoverage>' +
                ' <CoverageCd>' + esbCoverageCd.get(coverageType) + '</CoverageCd>' +
                ' <CoverageDesc>' + esbCoverageDesc.get(coverageType) + '</CoverageDesc>' +
                ' <Limit><FormatCurrencyAmt> <Amt>' + c.Limit_Value__c + '</Amt></FormatCurrencyAmt> </Limit>' + 
                ' <Deductible><FormatCurrencyAmt> <Amt>' + c.Deductible_Value__c + '</Amt></FormatCurrencyAmt> </Deductible>' + 
                ' <rsa:Indicator>1</rsa:Indicator>' +
                '</CommlCoverage>',
                output
            );
        }
    }
    
    static testMethod void testRevert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            CSIOXMLProcessor.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            Risk__c location = SMEQ_TestDataGenerator.getLocationRisk();
            
            Map<String, String> esbCoverageCdReverse = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageCd');
            Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbCoverageCd');
            
            String coverageCd = esbCoverageCdReverse.get('Equipment');
            Integer qLimit = 1000;
            Integer qDeductible = 1000;
            String riskId = location.Id;
            String packageId = 'Standard';
            
            CSIO_CommlCoverage ccc = new CSIO_CommlCoverage(coverageCd, qLimit, qDeductible, riskId, packageId);
            Coverages__c c = ccc.revert();
            System.assertEquals(riskId, c.Risk__c);
            System.assertEquals(packageId, c.Package_ID__c);
            System.assertEquals(qLimit, c.Limit_Value__c);
            System.assertEquals(qDeductible, c.Deductible_Value__c);
            System.assertEquals(esbCoverageCd.get(coverageCd), c.Type__c);
        }
    }
    
    static testMethod void testRevertXML()
    {
        Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbCoverageCd');
        
        String coverageCd = 'csio:Building';
        Integer limitValue = 1000000;
        Integer deductibleValue = 1000;
        
        String xml = '';
        xml += '<CommlCoverage>';
        xml += '    <CoverageCd>' + coverageCd + '</CoverageCd>';
        xml += '    <Limit>';
        xml += '        <FormatCurrencyAmt>';
        xml += '            <Amt>' + limitValue + '</Amt>';
        xml += '        </FormatCurrencyAmt>';
        xml += '    </Limit>';
        xml += '    <Deductible>';
        xml += '        <FormatCurrencyAmt>';
        xml += '            <Amt>' + deductibleValue + '</Amt>';
        xml += '        </FormatCurrencyAmt>';
        xml += '    </Deductible>';
        xml += '</CommlCoverage>';
        
        Coverages__c c = CSIO_CommlCoverage.revert(null, xml);
        System.assertEquals(esbCoverageCd.get(coverageCd), c.Type__c);
        System.assertEquals(limitValue, c.Limit_Value__c);
        System.assertEquals(deductibleValue, c.Deductible_Value__c);
    }
}