public with sharing class CSIOPolicyLocationXMLProcesser {

    // namespaces

    public static final String ACORD_NAMESPACE = 'http://www.ACORD.org/standards/PC_Surety/ACORD1/xml/';
    public static final String SOAP_NAMESPACE = 'http://schemas.xmlsoap.org/soap/envelope/';
    public static final String RSA_NAMESPACE = 'http://www.rsagroup.ca/schema/custom/xml/';
    public static final String CSIO_NAMESPACE = 'http://www.CSIO.org/standards/PC_Surety/CSIO1/xml/';

    public Risk__c location = null;

    public Map < Quote_Risk_Relationship__c, Account > accountQrrMap = null;
    public Map < Quote_Risk_Relationship__c, Contact > contactQrrMap = null;
    
    public integer index;

    /**
    Process the Xml stream representing the CommlPolicy element
    @param xmlStreamReader The CommlPolicy in xml stream format
    */

    public void process(Dom.XmlNode xmlLocationNode, Dom.XmlNode xmlCommlSublocationNode) {
       
        location = new Risk__c();
        CSIOPolicyAddrXMLProcessor addressProcessor = new CSIOPolicyAddrXMLProcessor();
        String deletedFlag = xmlLocationNode.getChildElement('Deleted', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
        if (deletedFlag == '1'){
            location.Deleted__c = true;
        } else {
            location.Deleted__c = false;
        }
        accountQrrMap = new Map < Quote_Risk_Relationship__c, Account > ();
        contactQrrMap = new Map < Quote_Risk_Relationship__c, Contact > ();

        // loop through the nodes and find the repeating ones
        index = 1;
        Map<String, String> esbRelationshipType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Relationship_Type__c');
        for (Dom.XmlNode childXmlNode: xmlLocationNode.getChildElements()) {
            // if it is a QRR / additional interest then delegate it to the CSIOPolicyAdditionalPartyProcessor
            CSIOPolicyAdditionalPartyProcessor partyProcessor = new CSIOPolicyAdditionalPartyProcessor();  
            if (childXmlNode.getName() == 'AdditionalInterest') {
                String natureInterestCD = childXmlNode.getChildElement('AdditionalInterestInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('NatureInterestCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
                CSIOPolicyXMLProcessor.debugChildren(childXmlNode.getChildElement('GeneralPartyInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                // association between QRR and account / contact
                partyProcessor.process(childXmlNode);
                partyProcessor.quoteRiskRelationship.Party_Number__c = index;
                partyProcessor.quoteRiskRelationship.Party_Type__c = 'Loss Payee';
                partyProcessor.quoteRiskRelationship.Relationship_Type__c = esbRelationshipType.get(natureInterestCD);
                partyProcessor.quoteRiskRelationship.index__c = xmlLocationNode.getAttribute('id', null);
                String deletedLossPayee = childXmlNode.getChildElement('Deleted', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
                if (deletedLossPayee == '1'){
                    partyProcessor.quoteRiskRelationship.Deleted__c = true;
                } else {
                    partyProcessor.quoteRiskRelationship.Deleted__c = false;
                }
                if (partyProcessor.isAccount) {
                    accountQrrMap.put(partyProcessor.quoteRiskRelationship, partyProcessor.partyAccount);
                } else {
                    contactQrrMap.put(partyProcessor.quoteRiskRelationship, partyProcessor.partyContact);
                }
                index ++; 
            }

        }
        
        //mapping out the CommlSublocation element 
        //mapping out construction elements 
        Map<String, String> esbConstructionType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbBldgConstructionCd');
        DOM.XmlNode constructionNode = xmlCommlSublocationNode.getChildElement('Construction', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        location.Year_Built__c = constructionNode.getChildElement('YearBuilt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? constructionNode.getChildElement('YearBuilt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
        location.Number_of_Stories__c = Integer.valueOf(constructionNode.getChildElement('NumStories', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText()) >1? Integer.valueOf(constructionNode.getChildElement('NumStories', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText()): 1;
        String constructionCd = constructionNode.getChildElement('BldgConstructionCd', CSIOPolicyXMLProcessor.CSIO_NAMESPACE).getText();
        Dom.XmlNode bldgImprovementsNode = xmlCommlSublocationNode.getChildElement('BldgImprovements', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlCommlSublocationNode.getChildElement('BldgImprovements', CSIOPolicyXMLProcessor.ACORD_NAMESPACE): null;
        if (bldgImprovementsNode != null && bldgImprovementsNode.getChildElements() != null){
            location.Renovated_Building__c = true;
            String heatingAmount = bldgImprovementsNode.getChildElement('HeatingImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? bldgImprovementsNode.getChildElement('HeatingImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
            if (heatingAmount != null){
                location.Heating_Renovated_Year__c = Integer.valueOf(heatingAmount);
            }
            String plumbingAmount = bldgImprovementsNode.getChildElement('PlumbingImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? bldgImprovementsNode.getChildElement('PlumbingImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
            if (plumbingAmount != null){
                location.Plumbing_Renovated_Year__c = Integer.valueOf(plumbingAmount);
            }
            String buildingAmount = bldgImprovementsNode.getChildElement('RoofingImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? bldgImprovementsNode.getChildElement('RoofingImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
            if (buildingAmount != null){
                location.Roof_Renovated_Year__c = Integer.valueOf(buildingAmount);
            }
            String wiringAmount = bldgImprovementsNode.getChildElement('WiringImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? bldgImprovementsNode.getChildElement('WiringImprovementYear', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
            if (wiringAmount != null){
                location.Electrical_Renovated_Year__c = Integer.valueOf(wiringAmount);
            }
        }
        location.Construction_Type__c = esbConstructionType.get(constructionCd);
        
        //mapping out the building protection 
        DOM.XmlNode blgdProtectionNode = xmlCommlSublocationNode.getChildElement('BldgProtection', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        
        Map<String, String> distanceToHydrantType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbDistanceToHydrant');
        String distanceToHydrantCd = blgdProtectionNode.getChildElement('DistanceToHydrant', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? blgdProtectionNode.getChildElement('DistanceToHydrant', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('NumUnits', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() :null;
        location.Distance_To_Fire_Hydrant__c = distanceToHydrantType.get(distanceToHydrantCd);
        
        Map<String, String> distanceToStationType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbDistanceToFireStation');
        String distanceToStationCd = blgdProtectionNode.getChildElement('DistanceToFireStation', CSIOPolicyXMLProcessor.ACORD_NAMESPACE)!= null? blgdProtectionNode.getChildElement('DistanceToFireStation', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('NumUnits',  CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() :null;
        location.Distance_To_Fire_Station__c = distanceToStationType.get(distanceToStationCd);
        
        location.Sprinkler_Coverage__c = blgdProtectionNode.getChildElement('SprinkleredPct', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? (blgdProtectionNode.getChildElement('SprinkleredPct', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() == '100'? true : false) : false; 
        
        location.LLP_Score__c = xmlLocationNode.getChildElement('LLPScore', CSIOPolicyXMLProcessor.RSA_NAMESPACE)!=null ? Decimal.valueOf(xmlLocationNode.getChildElement('LLPScore', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText()) :null;
        //mapping out the building occupancy 
        DOM.XmlNode bldgOccupancy = xmlCommlSublocationNode.getChildElement('BldgOccupancy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        location.Total_Occupied_Area__c = bldgOccupancy.getChildElement('AreaOccupied', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? Integer.valueOf(bldgOccupancy.getChildElement('AreaOccupied', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('NumUnits', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText()): null; 
    }
}