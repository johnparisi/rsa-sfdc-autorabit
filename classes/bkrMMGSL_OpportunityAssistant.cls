public class bkrMMGSL_OpportunityAssistant {
    // Set Variables 
    public Opportunity o;
    public String Name { get; set; }
    public String Address { get; set; }
    public String City { get; set; }
    public String State { get; set; }
    public String PostalCode { get; set; }
    public String CountryISOCode { get; set; }
    public String myDUNS { get; set; }
    public Boolean refreshPage {get; set; }
    public Boolean closePage {get; set; }
    // Set List Name Variables 
    public List<DNBCallout.dnbEntry> results { get; set; }
    public DNBCallout.dnbEntry record { get; set; }
    public class bkrMMGSL_OpportunityAssistantException extends Exception {}
    // Activate the Controller 
    public bkrMMGSL_OpportunityAssistant(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()){  
            controller.addFields(new List<String>{'RecordTypeId','bkrClientAccount__r.Sic','bkrClientAccount__r.SicDesc','OwnerId'});
        }
        o = (Opportunity)controller.getRecord(); 
        results = new List<DNBCallout.dnbEntry>();
        Map<String, Object> myMap = new Map<String, Object>();
    }
    // Perform Basic D&B Search 
    public void searchDnB(){
        results = new List<DNBCallout.dnbEntry>();
        if(Name == null || Name == '')
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'A company name is required to search D&B.'));
        else
            try{
                results = DNBCallout.LookupByCompanyName(Name,CountryISOCode,Address,City,State,PostalCode,null);
                if(Test.isRunningTest())
                    Integer i = 10/0;
            }
        
        catch (exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }   
    //Associate the Results with a Client Account Record
    public void selectRecordAction(){
        refreshPage = false;
        // Searches for the correct Client Account RecordType ID
        List<RecordType> lstRecordType = new List<RecordType>([Select Id From RecordType  Where SobjectType = 'Account' and RecordType.Name = 'Policyholder' LIMIT 1]); 
        for(DNBCallout.dnbEntry r : results){
            // Searches for existing Client Account Matches by DUNS Number
            // If a Client Account match is found, the opportunity is updated with the existing Client Account 
            List<Account> ClientAccounts = new List<Account>([Select Id From Account Where bkrAccount_DUNS__c = :r.DUNSNumber LIMIT 1]); 
            if(!ClientAccounts.isEmpty()){
                o.bkrClientAccount__c = ClientAccounts[0].id;
                update o;
                refreshpage = true;
                return;
            }
            if(r.DUNSNumber == myDUNS){
                Account a = new Account();
                if(!lstRecordType.isEmpty())                   
                    a.RecordTypeId = lstRecordType[0].Id;
                a.Name = r.Company;
                a.bkrAccount_DUNS__c = r.DUNSNumber;
                a.BillingStreet = r.Address;
                a.BillingCity = r.City;
                a.BillingState = r.State;
                a.BillingPostalCode = r.ZipCode;
                a.Phone = r.Phone;           
                try{
                    insert a;
                }catch (exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    return;
                }  
                // Add the Client Account to the Opportunity
                o.bkrClientAccount__c = a.Id;
                update o;
                refreshPage = true;
                return;
            }
            
        }
        //return null; 
    }
}