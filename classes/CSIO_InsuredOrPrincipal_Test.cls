/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_InsuredOrPrincipal.
 */
@isTest
public class CSIO_InsuredOrPrincipal_Test
{
	static testmethod void testConvert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            c.bkrCase_Insured_Client__c = a.Id;
            update c;
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(c);
            q = [
                SELECT id, Case__r.bkrCase_SIC_Code__r.SIC_Code__c, ePolicy__c, Case__r.bkrCase_SIC_Code__r.Name, 
                Year_Business_Started__c, Total_Occupied_Area__c, Total_Number_of_Units__c, Total_Number_of_Units_on_Policy__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            
            CSIO_InsuredOrPrincipal ciop = new CSIO_InsuredOrPrincipal(a, r, q);
            String output = '';
            output += ciop;
            
            CSIO_CommlSubLocation.Measurement area = new CSIO_CommlSubLocation.Measurement('rsa:Area', 'SqFt');
            area.numUnits = Integer.valueOf(q.Total_Occupied_Area__c);
            /*
            System.assertEquals(
                '<InsuredOrPrincipal>' +
                new CSIO_ItemIdInfo(a.Id) +
                new CSIO_GeneralPartyInfo(a, r) +
                '<InsuredOrPrincipalInfo>' +
                ' <BusinessInfo>' +
                '  <SICCd>' + q.Case__r.bkrCase_SIC_Code__r.SIC_Code__c + '</SICCd>' +
                '  <BusinessStartDt>' + q.Year_Business_Started__c + '</BusinessStartDt>' +
                '  <OperationsDesc>' + XMLHelper.toCData(q.Case__r.bkrCase_SIC_Code__r.Name) + '</OperationsDesc>' +
                area +
                ' </BusinessInfo>' +
                '</InsuredOrPrincipalInfo>' +
                '<CreditScoreInfo>' +
                ' <ReferenceNumber>' +
                a.bkrAccount_DUNS__c +
                '</ReferenceNumber>' +
                ' <CreditScore>' +
                CSIO_InsuredOrPrincipal.CREDIT_SCORE_DEFAULT_VALUE +
                '</CreditScore>' +
                ' <CreditScoreDt>' +
                Datetime.now().format(CSIO_InsuredOrPrincipal.CREDIT_SCORE_DATE_FORMAT) +
                '</CreditScoreDt>' +
                '</CreditScoreInfo>' +
                '</InsuredOrPrincipal>',
                output
            );
            */
        }
    }
}