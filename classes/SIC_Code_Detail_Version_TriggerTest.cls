@isTest
private class SIC_Code_Detail_Version_TriggerTest {

    static testMethod void testInsertUpdateSIC_Code_Detail_Version() {
        test.startTest();
        SIC_Code__c testSICCode = new SIC_Code__c(name='test sic code', SIC_Code__c = '9999');
        insert testSICCode;
        SIC_Code_Detail_Version__c version = new SIC_Code_Detail_Version__c(name = 'test SIC Code Detail Version', SIC_Code__c = testSICCode.Id, Region__c = 'Ontario', Offering__c = 'SPRNT v1');
        insert version;
        version.Region__c = 'Quebec';
        update version;
        test.stopTest();
        
        SIC_Code_Detail_Version__c testVersion = [select SIC_Code_Detail_Version_id__c from SIC_Code_Detail_Version__c where id=:version.id];
        system.AssertEquals(TestVersion.SIC_Code_Detail_Version_id__c.toUpperCase(), '9999SPRNT V1QUEBEC');        
    }
}