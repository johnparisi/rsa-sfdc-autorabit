public class CSIO_CommlScheduleEstablishment
{
    //private Account otherClientBillAccount;
    //private Contact otherClientBillContact;
    private Quote__c quote;
    
    private String creditCardType;
    private String creditCardExpiryDate;
    private String creditCardName;
    private String creditCardFrequency;
    
    private Double paymentAmount;
    private String bankTransitNumber;
    private String bankAccountNumber;
    private String preferredDay;
    //This is always the default value that needs to be passed to e-policy
    private String paymentInterval = 'csio:1';
    //To get past ESB rules, accountNumberId needs to be passed as this default 
    private String accountNumberId = 'XXXX';
    
    private String billingMethod;
    
    public static String BILLING_METHOD_CREDIT_CARD = 'Credit Card';
    public static String BILLING_METHOD_DIRECT_BILL = 'Direct Bill (One Time Payment)';
    public static String BILLING_METHOD_AGENCY_BILL = 'Agency Bill';
    public static String BILLING_METHOD_MONTHLY_WITHDRAWAL_PLAN = 'Monthly Withdrawal Plan';
    
    public CSIO_CommlScheduleEstablishment(Quote__c quote)//, Contact otherClientBillContact, Account otherClientBillAccount)
    {
        //this.otherClientBillContact = otherClientBillContact;
        //this.otherClientBillAccount = otherClientBillAccount;
        this.quote = quote;
        //System.assert(otherClientBillAccount == null || otherClientBillContact == null,'Should not be passed both an account and contact');
        billingMethod = quote.Policy_Payment_Plan_Type__c;
        convert();
    }
    
    public void convert()
    {
        //boolean partyIsAccount = otherClientBillAccount != null;
        
        // billing method. Convert this from the Salesforce picklist format into the billing code
        if (billingMethod == BILLING_METHOD_CREDIT_CARD)
        {
            Map<String, String> esbCreditCardType = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Credit_Card_Type__c');
            Map<String, String> esbCreditCardPaymentFrequencyCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCreditCardPaymentFrequencyCd');
            
            creditCardType = esbCreditCardType.get(quote.Credit_Card_Type__c);
            
            // put the leading zero on the credit card expiry month
            String creditCardExpiryMonth = quote.Credit_Card_Expiration_Month__c;
            if (creditCardExpiryMonth.length() == 1)
            {
                creditCardExpiryMonth = '0' + creditCardExpiryMonth;
            }
            creditCardExpiryDate = '20' + quote.Credit_Card_Expiration_Year__c + '-' + creditCardExpiryMonth;
            creditCardFrequency = esbCreditCardPaymentFrequencyCd.get(quote.Payment_Frequency__c);
            creditCardName = quote.Credit_Card_Name__c;
        }
        else if (billingMethod == BILLING_METHOD_MONTHLY_WITHDRAWAL_PLAN)
        {
            paymentAmount = quote.Downpayment_Amount__c;
            if(quote.Bank_Transit_ID__c != null) {
                bankTransitNumber = quote.Bank_Transit_ID__c.split(' ')[0];
            }
            bankAccountNumber = quote.Bank_Account_Number__c;
            preferredDay = quote.Monthly_Withdrawal_Preferred_Day__c.toPlainString();
        }
        else if (billingMethod == BILLING_METHOD_DIRECT_BILL)
        {
            paymentAmount = quote.Downpayment_Amount__c;
        }
        else if (billingMethod == BILLING_METHOD_AGENCY_BILL)
        {
            // nothing to add
        }
    }   
        
    public override String toString()
    {
        String xml = '';
        if (billingMethod != BILLING_METHOD_AGENCY_BILL)
        {
            xml += '<CommlScheduleEstablishment>';
            xml += ' <PaymentOption>';
            if (billingMethod == BILLING_METHOD_CREDIT_CARD)
            {
                if (preferredDay != null){
                    xml += '  <DayMonthDue>' + preferredDay + '</DayMonthDue>';
                }
                xml += '  <MethodPaymentCd>' + creditCardType + '</MethodPaymentCd>';
                xml += '  <ElectronicFundsTransfer>';
                xml += '   <FromAcct>';
                xml += '     <AccountNumberId>' +  accountNumberId + '</AccountNumberId>';
                xml += '     <CreditCardExpirationDt>' + creditCardExpiryDate + '</CreditCardExpirationDt>';
                xml += '     <CommercialName>' +  creditCardName + '</CommercialName>';
                xml += '     <rsa:CreditCardPaymentFrequencyCd>' + creditCardFrequency + '</rsa:CreditCardPaymentFrequencyCd>';
                xml += '   </FromAcct>';
                xml += '   <TransferAmt>';
                xml += '     <Amt>' + 0 + '</Amt>';
                xml += '   </TransferAmt>';
                xml += '  </ElectronicFundsTransfer>';
            }
            else if (billingMethod == BILLING_METHOD_MONTHLY_WITHDRAWAL_PLAN)
            {
                xml += '  <DayMonthDue>' + preferredDay + '</DayMonthDue>';
                xml += '  <ElectronicFundsTransfer>';
                xml += '    <FromAcct>';
                xml += '      <AccountNumberId>' + bankAccountNumber + '</AccountNumberId>';
                xml += '      <BankInfo>';
                xml += '        <BankId>' + bankTransitNumber + '</BankId>';
                xml += '      </BankInfo>';
                xml += '    </FromAcct>';
                xml += '    <TransferAmt>';
                xml += '      <Amt>' + paymentAmount + '</Amt>';
                xml += '     </TransferAmt>';
                xml += '  </ElectronicFundsTransfer>';
            }
            else if (billingMethod == BILLING_METHOD_DIRECT_BILL)
            {
                xml += '  <PaymentIntervalCd>' + paymentInterval + '</PaymentIntervalCd>';
                xml += '  <ElectronicFundsTransfer>';
                xml += '   <TransferAmt>';
                xml += '    <Amt>' + paymentAmount + '</Amt>';
                xml += '   </TransferAmt>';
                xml += '  </ElectronicFundsTransfer>';
            }
            xml += ' </PaymentOption>';
            xml += '</CommlScheduleEstablishment>';
        }
        
        return xml;
    }
}