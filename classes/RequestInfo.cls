public virtual class RequestInfo
{
    public enum ServiceType {GET_QUOTE, GET_3_QUOTE, UPLOAD_QUOTE, FINALIZE_QUOTE, DOWNLOAD_QUOTE, BIND_QUOTE, DOWNLOAD_BINDER, GET_POLICY, LOAD_POLICY, LOAD_POLICY_X, ABORT_TRANSACTION, SAVE_QUOTE, RABIND_QUOTE, RAGET_QUOTE, RAFINALIZE_QUOTE}
    
    // Packages
    //private Schema.DescribeFieldResult packageIDResult = Coverages__c.Package_ID__c.getDescribe();
    //private List<Schema.PicklistEntry> packageTypes = packageIDResult.getPicklistValues();
    // Segments
    //private Schema.DescribeFieldResult segmentResult = SIC_Code_Detail__c.Segment__c.getDescribe();
    //private List<Schema.PicklistEntry> segmentTypes = segmentResult.getPicklistValues();
    
    public ServiceType svcType;
    public String pkgType;
    public String sgmtType;
    
    public RequestInfo(ServiceType svct, String pkgt, String sgmt)
    {
        this.svcType = svct;
        this.pkgType = pkgt;
        this.sgmtType = sgmt;
        
        System.assert(this.sgmtType != null, 'Segment Type should not be null at RequestInfo constructor');
        System.assert(this.pkgType != null, 'Package type should not be null at RequestInfo constructor');
        
        Map<String, String> esbSegment = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
        
        System.assert(esbSegment.size() > 0, 'esbSegment size should not be zero');
        
        if (this.sgmtType != null && esbSegment.get(this.sgmtType) == null)
        {
            throw new RSA_ESBException('SIC Code Segment (' + this.sgmtType + ') is not currently supported by the ESB.');
        }
        
        Map<String, String> esbFlavor = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbFlavor');
        if (esbFlavor.get(this.pkgType) == null)
        {
            throw new RSA_ESBException('Package Type (' + this.pkgType + ') is not currently supported by the ESB.');
        }
    }

    //Using this method when calling GetPolicy when there is no segment, package before the policy is retrieved 
    public RequestInfo(ServiceType svct){
        this.svcType = svct;
    }
    
       
    public String getServiceType()
    {
        Map<String, String> csm = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbService');
        return csm.get(this.svcType.name());
    }
    
    public String getPackageType()
    {
        Map<String, String> esbFlavor = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbFlavor');
        return esbFlavor.get(this.pkgType);
    }
    
    public String getSegmentType()
    {
        Map<String, String> esbFlavor = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
        return esbFlavor.get(this.sgmtType);
    }
    
    public static ServiceType getServiceType(String svct)
    {
        for (ServiceType st : ServiceType.values())
        {
            if (st.name() == svct)
            {
                return st;
            }
        }
        
        return null;
    }
    
    public User getUnderwriterId() {
        List<User> underwriterId = [ SELECT id, FederationIdentifier 
                                    FROM User 
                                    WHERE id = :UserInfo.getUserId() AND IsPortalEnabled = false ];
        if ( !underwriterId.isEmpty() )
            return underwriterId.get(0);
        else
            return null;
    }
}