/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/acceptInitialTerms')
global without sharing class SMEQ_AcceptTermsDevRestWrapperService {
    @HttpPut
    global static FP_AngularPageController.OutcomeResponse acceptInitialTerms() {
        System.debug('Hit wrapper');
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        FP_AngularPageController.OutcomeResponse outcomeResponse = null;
    try {
      outcomeResponse = FP_AngularPageController.acceptInitialTerms();
       outcomeResponse.success = true;
    }
    catch (Exception e) {
      System.debug('##### caught exception ' + e.getMessage());
      System.debug('##### stack trace ' + e.getStackTraceString());
      outcomeResponse = new FP_AngularPageController.OutcomeResponse();
      outcomeResponse.success = false;
      outcomeResponse.errorMessages.add(e.getMessage());
    }
      return outcomeResponse;
    }
    
    
}