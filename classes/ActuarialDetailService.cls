/**
* @File Name    :   ActuarialDetailService
* @Description  :   Actuarial Detail Service Layer
* 					Contains processing service methods related to the Actuarial Detail Object
* @Date Created :   12/12/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Service
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       12/12/2017  Habiba Zaman  	Created the file/class
*/

public class ActuarialDetailService {
	
    /**
    * @method   updatePolicyRecordsWithId
    * @param	records Actuarial Detail records that need to be updated
    * @return 	N/A
    *
    * On insert new actuarial data, update id in related policy record.
    * 
    */
	public void updatePolicyRecordsWithId(List<Actuarial_Detail__c> records) {
		//Actuarial id with related Policy Id
		Map<Id, Id> policyIds = new Map<Id, Id>();
        
        for(Actuarial_Detail__c ad : records){
            if(ad.Policy_Number__c != null){
                policyIds.put(ad.Policy_Number__c,ad.Id );
            }
        }

        List<Policy__c> toUpdate = [Select Id, Policy_Actuarial_Detail__c from Policy__c where Id in : policyIds.keySet()];
        List<Policy__c> policies = new List<Policy__c>();
        if(!toUpdate.isEmpty() && toUpdate != null){
            for(Policy__c p : toUpdate){
                if(p.Policy_Actuarial_Detail__c == null){
                    p.Policy_Actuarial_Detail__c = policyIds.get(p.Id);
                    policies.add(p);
                }
            }
        }
        
        if(!policies.isEmpty() && policies != null){
            update policies;
        }
	}
}