/**
 * AutoRABIT test
 * AccountDomain
 * Author: Alexander Luksidadi
 * Date: April 26 2016
 * 
 * The object domain is where the real work happens, this is where we will apply the actual business logic. 
 * We have the main Object Domain which should handle all common processing. 
 * Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
 * Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
 * just implement logic in the appopriate method. 
 */
public with sharing class AccountDomain {
    
    private AccountService service = new AccountService();
    
    public void beforeInsert(List<Account> records){
    }

    public void beforeUpdate(List<Account> records, Map<Id, Account> oldRecords){
    }

    public void afterInsert(List<Account> records){
    }

    public void afterUpdate(List<Account> records, Map<Id, Account> oldRecords){
    }

    //Account with Record Type of bkrAccount_RecordTypeBkrMM or bkrAccount_RecordTypeBkrMGML
    public class BrokerDomain{
        private AccountService service = new AccountService();
        public void beforeInsert(List<Account> records){
            service.updateSegmentationWhenParentChange(records, null);
        }

        public void beforeUpdate(List<Account> records, Map<Id, Account> oldRecords){
            service.updateSegmentationWhenParentChange(records, oldRecords);
        }

        public void afterInsert(List<Account> records){
            service.updateChildWithParentSegmentation(records, null);
        }

        public void afterUpdate(List<Account> records, Map<Id, Account> oldRecords){
            service.updateChildWithParentSegmentation(records, oldRecords);
        }
    }
}