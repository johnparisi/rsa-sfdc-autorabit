global class CI_UW_MilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator
{   
    private List<Boolean> businessDay = new Boolean[7];
    private List<Time> startHours = new Time [7];
    private List<Time> endHours = new Time [7];        
    private Date knownSunday = date.newInstance(2013, 1, 6);
    private final integer CONSTANT_TRIAGE_SLA_MINUTES = 30;
    
    // The Case, MilestoneType and BusinessHours results are stored into these static maps.
    // This will cache the result by ID so that re-retrievals are checked here first within the same Apex transaction.
    private static Map<Id, Case> retrievedCases = new Map<Id, Case>();
    private static Map<Id, MilestoneType> retrievedMilestoneTypes = new Map<Id, MilestoneType>();
    private static Map<Id, BusinessHours> retrievedBusinessHours = new Map<Id, BusinessHours>();
    
    public class applicationException extends Exception {}
    
    global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId)
    {
        ID businessHoursId;
        integer intDayOfWeek;
        Datetime nextStart;
        Datetime tempCreatedDate;
        Long minutesRemaining;
        datetime myEndDateTime;
        Boolean isWithin;
        BusinessHours bh;
        
        // First check if this caseId had been retrieved previously.
        Case c = retrievedCases.get(caseId);
        if (c == null)
        {
            // ***
            // SFDC-20 - modify to use Case.BusinessHoursId instead of Entitlement.
            // ***               
            c = [
                SELECT CreatedDate, BusinessHoursId, bkrCase_Subm_Type__c, bkrCase_RAG__c, bkrCase_Issued_Milestone_SLA__c 
                FROM Case
                WHERE Id = :caseId
            ];
            // This case is retrieved for the first time. Store it in the cache map.
            retrievedCases.put(caseId, c);
        }
        
        // First check if this milestoneTypeId had been retrieved previously.
        MilestoneType ms = retrievedMilestoneTypes.get(milestoneTypeId);
        if (ms == null)
        {
            ms = [
                SELECT Id, Name
                FROM MilestoneType
                WHERE Id = :milestoneTypeId
            ];
            // This milestoneType is retrieved for the first time. Store it in the cache map.
            retrievedMilestoneTypes.put(milestoneTypeId, ms);
        }
        
        try
        {
            businessHoursId = c.BusinessHoursId; //(c.Entitlement.BusinessHoursId != null ? c.Entitlement.BusinessHoursId : c.Entitlement.SlaProcess.BusinessHoursId);            
            // end SFDC-20 //
            // First check if this businessHoursId had been retrieved previously.
            bh = retrievedBusinessHours.get(businessHoursId);
            if (bh == null)
            {
                bh = [
                    SELECT
                    SundayStartTime, MondayStartTime, TuesdayStartTime,
                    WednesdayStartTime, ThursdayStartTime, FridayStartTime,
                    SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime,
                    WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime,
                    TimeZoneSidKey
                    FROM BusinessHours
                    WHERE ID = :businessHoursId
                ];
                // This businessHours is retrieved for the first time. Store it in the cache map.
                retrievedBusinessHours.put(businessHoursId, bh);
            }
            
            businessDay[0] = (bh.SundayStartTime != null);
            businessDay[1] = (bh.MondayStartTime != null);
            businessDay[2] = (bh.TuesdayStartTime != null);
            businessDay[3] = (bh.WednesdayStartTime != null);
            businessDay[4] = (bh.ThursdayStartTime != null);
            businessDay[5] = (bh.FridayStartTime != null);
            businessDay[6] = (bh.SaturdayStartTime != null);
            
            startHours[0] = bh.SundayStartTime;
            startHours[1] = bh.MondayStartTime;
            startHours[2] = bh.TuesdayStartTime;
            startHours[3] = bh.WednesdayStartTime;
            startHours[4] = bh.ThursdayStartTime;
            startHours[5] = bh.FridayStartTime;
            startHours[6] = bh.SaturdayStartTime;
            
            endHours[0] = bh.SundayEndTime;
            endHours[1] = bh.MondayEndTime;
            endHours[2] = bh.TuesdayEndTime;
            endHours[3] = bh.WednesdayEndTime;
            endHours[4] = bh.ThursdayEndTime;
            endHours[5] = bh.FridayEndTime;
            endHours[6] = bh.SaturdayEndTime;
            
        }
        catch (exception e)
        {
            throw new applicationException('Unable to find Business Hours record for Entitlement: ' + c.Entitlement);        	
        }
        
        Timezone tz = Timezone.getTimeZone(bh.TimeZoneSidKey);
        integer offset = tz.getOffset(datetime.now());
        if (ms.Name == 'Triage')
        {
            // do an initial check for the 1 hr grace period to see if it falls during business hours
            tempCreatedDate = c.CreatedDate.addhours(1);
            // retrieve the next business start date (if iswithin, then same day, otherwise, next business day)
            nextStart = BusinessHours.nextStartDate(bh.id, tempCreatedDate);
            if(nextStart.date() == c.CreatedDate.date()){
                nextStart = c.CreatedDate; // reset to actual datetime
            }
            // add a standard 30 minute SLA to the startdate (could be next day's start of shift)
            myEndDateTime = BusinessHours.add(bh.id, nextStart, CONSTANT_TRIAGE_SLA_MINUTES * 1000 * 60);
            // calculate the diference betweeen created & due date
            minutesRemaining = ((BusinessHours.diff(bh.id, c.CreatedDate, myEndDateTime) / 1000) / 60);
        }else{
            // Quote Issued 'SAME_DAY' SLA
            // do an initial check for the 1 hr grace period to see if it falls during business hours
            tempCreatedDate = c.CreatedDate.addhours(1);
            // retrieve the next business start date (if iswithin, then same day, otherwise, next business day)
            nextStart = BusinessHours.nextStartDate(bh.id, tempCreatedDate);
            if(nextStart.date() == c.CreatedDate.date()){
                nextStart = c.CreatedDate; // reset to actual datetime
            }
            // get Businesshours as local time zone
            myEndDateTime = Datetime.newInstanceGmt(nextStart.date().year(), nextStart.date().month(), nextStart.date().day(), getEndTime(nextStart.date()).hour(), getEndTime(nextStart.date()).minute(), getEndTime(nextStart.date()).second());
            // convert myEndDateTime to GMT
            myEndDateTime = myEndDateTime.addSeconds( (0-offset) / 1000);
            // calculate the diference betweeen created & due date
            minutesRemaining = ((BusinessHours.diff(bh.id, c.CreatedDate, myEndDateTime) / 1000) / 60);
        }
        return minutesRemaining.intValue();
    }
    
    // Check if today is a business day
    global Boolean isBusinessDay(Date inputDate) {
        // index i is index into the businessDay array based on inputDate
        Integer i = Math.mod(Math.abs(this.knownSunday.daysBetween(inputDate)),7);
        return (businessDay[i]);
    } 
    
    // Get the start time
    global Time getStartTime(Date inputDate) {
        Integer i = Math.mod(Math.abs(this.knownSunday.daysBetween(inputDate)),7);
        return (startHours[i]);
    }    
    
    // Get the end time
    global Time getEndTime(Date inputDate) {
        Integer i = Math.mod(Math.abs(this.knownSunday.daysBetween(inputDate)),7);
        return (endHours[i]);
    }       
}