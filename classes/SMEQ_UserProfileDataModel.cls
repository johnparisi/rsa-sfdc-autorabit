global with sharing class SMEQ_UserProfileDataModel {

  public BrokerAccount brokerageHierarchy;
  public BrokerAccount ownBrokerage;
  public string contactId;
  public string contactLoginId; // AKA BRAVO Id : User.Federation Id
  public string phone;
  public string phoneExtn;
  public string email;
  public string firstName;
  public string lastName;
  public boolean acceptedTerms;
  public String language;
  public string epReferenceNumber;
  public String region;
  
   public class BrokerAccount {
        public String accountName;
        public String accountId;
        public BrokerAccount childBrokerAccount;
    }
}