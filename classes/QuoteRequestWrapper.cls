/**
 * Author: Sandip Sen
 * Created At: April 3, 2017
 * 
 * Description: This is a wrapper class for populating list of sObjects records which will be used to pass the information to Angular Application.
 *              This class is created for FP-3488
 *
 */
 
global with sharing class QuoteRequestWrapper
{
    public Quote__c quote;
    public Case cse;
    public List<Account> accList;
    public List<Contact> contactList;
    public List<Risk__c> liabilityRisks;
    public List<Risk__c> locationRisks;
    public List<Risk__c> nonLiabilityRisks;
    public List<Coverages__c> locationCoverage;
    public List<Coverages__c> liabilityCoverage;
    public List<Coverages__c> nonLiabilityCoverage;
    public List<Claim__c> claims;
    public List<Quote_Risk_Relationship__c> quoteRiskMapping;
    public List<SIC_Question__c> dynamicQuestions;
    public Map<String, Map<String, SIC_Answer__c>> dynamicAnswers;
    public string quoteStatus;
    public List<Policy_History__c> policyHistories;
    public boolean success;
    public Boolean isLiveChatIndicator; // FRZR-123
    public List<String> errorMessages = new List<String>();
    public string continuationReferenceID;
    public string caseId;
    public string quoteID;
    public String xmlRequest {get; set;}
    public Boolean esbErrorFlag;
    public String esbErrorMessage;
    public Sic_Code_Detail_Version__c sicCodeDetailVersion;
}