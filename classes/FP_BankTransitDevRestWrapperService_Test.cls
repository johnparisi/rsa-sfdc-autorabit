@isTest
Public class FP_BankTransitDevRestWrapperService_Test{

    static testmethod void FP_BankTransitService(){
        Bank_Transit_Number__c ba = new Bank_Transit_Number__c();
        ba.city__c = 'test';
        ba.Active_From__c = system.today();
        ba.Active_To__c = system.today()+1;
        ba.Address1__c = 'Address';
        ba.Client_Reference__c = 9878;
        ba.Client_Type__c ='2a';
        ba.Client__c = 97678;
        ba.Financial_Institution_Name__c = 'Finat';
        ba.Inter_Code__c = '342fdsd';
        ba.Postal_code__c = '23adsas';
        ba.Province__c='hreh';
        ba.Role_Code__c = '99087nnb';
        ba.Role_Type__c = '3a';
        insert ba;
        string searchText = ba.name;
        FP_AngularPageController.BankTransitResponse response= FP_BankTransitDevRestWrapperService.searchForTransitNumbers(searchText);
    }
}