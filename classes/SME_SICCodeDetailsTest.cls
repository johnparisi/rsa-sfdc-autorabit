@isTest
public class SME_SICCodeDetailsTest {
    public Static Map<String,Object> inputMap= new Map<String,Object>();
    public Static Map<String,Object> outputMap = new Map<String,Object>();
    public Static Map<String, Object> options = new Map<String,Object>();
    @testsetup static void testData(){
        //SIC codes
        List<SIC_Code__c> SIC_CodesList = new List<SIC_Code__c>();
        SIC_Code__c code = new SIC_Code__c(Name='TestCode',SIC_Code__c='0110A');
        SIC_CodesList.add(code);
        SIC_Code__c code1 = new SIC_Code__c(Name='TestCode1',SIC_Code__c='8500');
        SIC_CodesList.add(code1);
        insert SIC_CodesList;
        //SIC code Deatail Version
        SIC_Code_Detail_Version__c codeDetail = New SIC_Code_Detail_Version__c(Name='TestCode_Detail',
                                                                               Application_Enabled__c=true, 
                                                                               Canadian_Liability__c='1', USA_Liability__c='1',
                                                                               Crime_Hazard__c='3', Fid_Hazard__c='4', Property_Hazard__c='4',
                                                                               E_O_Liability__c='3', Poll_Liability__c='3',
                                                                               Coverage_Add_ons__c='Sewer Backup', Coverage_Defaults__c='Commercial General Liability;Building;Equipment;Stock;Property Aggregate Package;Privacy Breach;Rental Revenue',
                                                                               RAG__c='Red', Referral_Level__c='DECLINE',
                                                                               Segment__c='Oil & Gas Production', Subsegment__c='Oil & Gas Production', 
                                                                               SME__c=false, Non_RSA__c=true, Mid_Market__c=false,
                                                                               SIC_Code__c=SIC_CodesList[0].Id ,
                                                                               Short_Description_En__c='Test', Short_Description_Fr__c='Sociétés Pétrolières Nationales - Gaz Naturel [1391]',
                                                                               Description_Operations_En__c='Testinggg', Description_Operations_Fr__c='TestOp', 
                                                                               Category__c='Natural Resources', Subcategory__c='Oil & Gas Operations', Monoline__c=false,
                                                                               Region__c='Atlantic', Offering__c='SPRNT');
        insert codeDetail;
        //SIC question
        Id QuesRecTypeId = Schema.SObjectType.SIC_Question__c.getRecordTypeInfosByDeveloperName().get('Eligibility').getRecordTypeId();
        SIC_Question__c sicQues = new SIC_Question__c(RecordTypeId=QuesRecTypeId,questionId__c=90008,liveChatRequired__c=true,questionText_en__c='TestQuestion',questionText_fr__c='QuestionTest');
        insert sicQues;
        //SIC Code question Association
        SIC_Code_Question_Association__c sicQuesAssociation =  new SIC_Code_Question_Association__c(order__c = 3,SIC_Code_Detail_Version__c=codeDetail.Id,questionId__c = sicQues.Id,Name='TestquesAss');
        insert sicQuesAssociation;
    }
    @istest static void testInvokeMethod(){
        String methodName = 'getSICDetails';
        String methodNameTest = 'getSICetails';
        SME_SICCodeDetails sme = new SME_SICCodeDetails();
        sme.invokeMethod(methodName,inputMap,outputMap,options);
        sme.invokeMethod(methodNameTest,inputMap,outputMap,options);
        system.assert(methodName =='getSICDetails');
        system.assert(methodNameTest == 'getSICetails');
    }
    @istest static void testGetSICDetails(){
        SME_SICCodeDetails sme = new SME_SICCodeDetails();
        inputMap = (Map<String, Object>)JSON.deserializeUntyped('{\"user\":{\"region\":\"Atlantic\",\"name\":\"Harsha Vardhan\"},\"GettingStarted\":{\"Offerings\":\"SPRNT\",\"SearchBy\":\"Search By SIC Code\",\"SelectBusinessType\":null,\"SelectBusinessTypeHub\":null,\"SelectBusinessCategory\":null,\"SelectConstructionCategory\":null,\"SelectRetailCategory\":null,\"SelectBusinessCategoryHub\":null,\"SelectConstructionCategoryHub\":null,\"SelectHospitalityCategoryHub\":null,\"SelectManufacturingCategoryHub\":null,\"SelectNaturalResourcesCategoryHub\":null,\"SelectPublicSectorCategoryHub\":null,\"SelectRealtyCategoryHub\":null,\"SelectRetailCategoryHub\":null,\"SelectWarehouseAndLogisticsCategoryHub\":null,\"SelectWholesaleCategoryHub\":null,\"Type\":null,\"SubType\":null,\"TypeAheadSIC-Block\":null,\"Alittlebitabouttheindustry\":\"test test\",\"SICLookups-Block\":null,\"SICcodeId\":\"0110A\",\"SICRecordId\":null,\"SICName\":null,\"isSICValid\":null,\"Segment\":null,\"SICCheck\":null}}');   
        sme.getSICDetails(inputMap,outputMap);
        system.assert(!outputMap.isEmpty());
    }
}