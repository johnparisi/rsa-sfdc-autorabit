global with sharing class InvokableGetSICCodeDetails {
    
    @InvocableMethod(label='Update SIC Code Details For Case' description='Update SIC Code Details For Case.')
    global static List<SIC_Code_Detail__c> InvokableGetSICCodeDetailsForCase( List<UpdateSICDetails> req)
    {  
            List<SIC_Code_Detail_Version__c> relatedSicDetailsList = new  List<SIC_Code_Detail_Version__c>();
            List<SIC_Code_Detail__c> sicCodeDefaultList = new List<SIC_Code_Detail__c>();
            String caseID = '';
            if(req[0].caseID != null)
            {
                caseID = req[0].caseID;

            }
            relatedSicDetailsList = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(req[0].caseID);        
            List<SIC_Code_Detail__c> sicCodeDetailList = new List<SIC_Code_Detail__c>();
            //convert sic code detail version to sic code detail object
           

            if(!relatedSICDetailsList.isEmpty())
            {
                system.debug('first IF');
                SIC_Code_Detail__c sicDetails = new SIC_Code_Detail__c();
                sicDetails.Property_Hazard__c = relatedSicDetailsList[0].Property_Hazard__c;
                sicDetails.Crime_Hazard__c    = relatedSicDetailsList[0].Crime_Hazard__c;
                sicDetails.Fid_Hazard__c      =  relatedSicDetailsList[0].Fid_Hazard__c;
                sicDetails.Canadian_Liability__c =  relatedSicDetailsList[0].Canadian_Liability__c;
                sicDetails.USA_Liability__c = relatedSicDetailsList[0].USA_Liability__c;
                sicDetails.E_O_Liability__c = relatedSicDetailsList[0].E_O_Liability__c;
                sicDetails.Crime_Hazard__c = relatedSicDetailsList[0].Crime_Hazard__c;
                sicDetails.Poll_Liability__c = relatedSicDetailsList[0].Poll_Liability__c;
                sicDetails.Segment__c = relatedSicDetailsList[0].Segment__c;
                sicDetails.Subsegment__c = relatedSicDetailsList[0].Subsegment__c; 
                sicDetails.Mid_Market__c = relatedSicDetailsList[0].Mid_Market__c;
                sicDetails.Referral_Level__c = relatedSicDetailsList[0].Referral_Level__c;
                sicDetails.Non_RSA__c = relatedSicDetailsList[0].Non_RSA__c;
                sicDetails.SME__c = relatedSicDetailsList[0].SME__c;
                sicDetails.RAG__c = relatedSicDetailsList[0].RAG__c;
                sicCodeDetailList.add(sicDetails);
                
                
            }

            else
            {
                System.debug('Entering else');
                sicCodeDefaultList = SMEQ_SicCodeService.getDefaultSicCodesForCase(caseID);
             
                if(!sicCodeDefaultList.isEmpty())
                {
                    SIC_Code_Detail__c sicDetails = new SIC_Code_Detail__c();
                    sicDetails.Property_Hazard__c = sicCodeDefaultList[0].Property_Hazard__c;
                    sicDetails.Crime_Hazard__c    = sicCodeDefaultList[0].Crime_Hazard__c;
                    sicDetails.Fid_Hazard__c      =  sicCodeDefaultList[0].Fid_Hazard__c;
                    sicDetails.Canadian_Liability__c =  sicCodeDefaultList[0].Canadian_Liability__c;
                    sicDetails.USA_Liability__c = sicCodeDefaultList[0].USA_Liability__c;
                    sicDetails.E_O_Liability__c = sicCodeDefaultList[0].E_O_Liability__c;
                    sicDetails.Crime_Hazard__c = sicCodeDefaultList[0].Crime_Hazard__c;
                    sicDetails.Poll_Liability__c = sicCodeDefaultList[0].Poll_Liability__c;
                    sicDetails.Segment__c = sicCodeDefaultList[0].Segment__c;
                    sicDetails.Subsegment__c = sicCodeDefaultList[0].Subsegment__c; 
                    sicDetails.Mid_Market__c = sicCodeDefaultList[0].Mid_Market__c;
                    sicDetails.Referral_Level__c = sicCodeDefaultList[0].Referral_Level__c;
                    sicDetails.Non_RSA__c = sicCodeDefaultList[0].Non_RSA__c;
                    sicDetails.SME__c = sicCodeDefaultList[0].SME__c;
                    sicDetails.RAG__c = sicCodeDefaultList[0].RAG__c;
                    sicCodeDetailList.add(sicDetails);
                }   
            }
            
            return sicCodeDetailList;
    }

    global class UpdateSICDetails {
        @InvocableVariable(required=true)
        public ID caseId;
      }    
    
}