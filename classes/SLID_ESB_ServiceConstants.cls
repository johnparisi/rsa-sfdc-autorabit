/**
  * @author        Anmol Bakshi
  * @date          10/18/2016
  * @description   Main class to receive requests from ESB for SLID processes
*/
public class SLID_ESB_ServiceConstants 
{
    public static boolean isSLID = false; // Variable added for FP-6578
    public static boolean throwException = false;
    public static string breakPoint = '';
    public Static void throwTestingException(String methodName)
    {
        if (throwException == true && methodName == breakPoint)
        {
            Exception ex = UTIL_Logging.createMappingException('Test Exception');
            throw ex;
        }
    }
    
    //Record Type Names on Integration Log Object 
    public static final String INBOUND_STRING = 'INBOUND';
    public static final String OUTBOUND_STRING = 'OUTBOUND';
    
    //Custom Response Failure Messsage
    public static final String DML_FAIL_MESSAGE = 'One or more records encountered DML Exception.';
    
    //Custom Exception Types for Custom Exceptions in UTIL_Logging class
    public static final String MAPPING_ERROR_TYPE = UTIL_Logging.MAPPING_ERROR_TYPE;
    public static final String DML_ERROR_TYPE = UTIL_Logging.DML_ERROR_TYPE;
    
    //Custom Exception Messages for SLID_Mapping_Exception
    public static final String NO_MAPPING_FOR_SERVICE_PROCESS_MSG = 'No Service Process record defined for the process name set in the web service call';
    public static final String NO_UNIQUE_MAPPING_FOR_SERVICE_PROCESS_MSG = 'More than 1 Service Process record defined for the process name set in the web service call';
    public static final String NO_TARGET_OBJECT_FOR_SERVICE_PROCESS_MSG = 'No Target Obejct defined for the Service Process';
    public static final String NO_TARGET_FIELD_FOR_TARGET_OBJECT_MSG = 'No Target Fields defined for Target objects';
    
    public static final String targetFieldRecTypeNameForDefaultValue = 'DEFAULT VALUE';
    public static final String targetFieldRecTypeNameForMappedValue = 'Mapped Value From Source';
    
    //DML Operations on UPSERT
    public static final String UPDATE_RECORD = 'UPDATE';
    public static final String CREATE_RECORD = 'CREATE';
    
}