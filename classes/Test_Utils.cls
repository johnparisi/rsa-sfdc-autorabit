@isTest
private class Test_Utils {
	/*
	@Purpose: Test Utility Method for Getting Changed Records. 
	@Testing: 
	1. If a field is changed and passed into the method the record will be returned. 
	@Author: Stephen Piercey
	@Date: May 9, 2016
	*/
	@isTest static void test_FieldChanged() {
		Account a = new Account(Name='Test', Site='original');

		insert a;

		Map<Id, Account> oldRecords = new Map<Id,Account>();
		oldRecords.put(a.Id, a);

		Account updated = [SELECT Name, Site FROM Account WHERE Id = :a.Id];

		updated.Site = 'New Site';

		List<Account> currentRecords = new List<Account>{updated};


		Test.startTest();
 			List<SObject> updatedRecords = Utils.getChangedObjects(currentRecords, oldRecords, new List<String>{'Site'});
		Test.stopTest();

		System.assertEquals(1, updatedRecords.size());
 		System.assertEquals(a.Id, updatedRecords[0].get('Id'));
	}

	/*
	@Purpose: Test Utility Method for Getting Changed Records. 
	@Testing: 
	1. If a field is changed and passed into the method, without testing that field, the record will not return. 
	@Author: Stephen Piercey
	@Date: May 9, 2016
	*/
	@isTest static void test_FieldChangedNotTracked() {
		Account a = new Account(Name='Test', Site='original');

		insert a;

		Map<Id, Account> oldRecords = new Map<Id,Account>();
		oldRecords.put(a.Id, a);

		Account updated = [SELECT Name, Site FROM Account WHERE Id = :a.Id];

		updated.Site = 'New Site';

		List<Account> currentRecords = new List<Account>{updated};

		Test.startTest();
 			List<SObject> updatedRecords = Utils.getChangedObjects(currentRecords, oldRecords, new List<String>());
		Test.stopTest();
		System.assertEquals(0, updatedRecords.size());
	}

	/*
	@Purpose: Test Utility Method for Getting Changed Records. 
	@Testing: 
	1. If you don't change a tracked field, the record will not return. 
	@Author: Stephen Piercey
	@Date: May 9, 2016
	*/
	@isTest static void test_FieldNotChanged() {
		Account a = new Account(Name='Test', Site='original');

		insert a;

		Map<Id, Account> oldRecords = new Map<Id,Account>();
		oldRecords.put(a.Id, a);

		Account updated = [SELECT Name, Site FROM Account WHERE Id = :a.Id];
		//Just in case a workflow or something changes this. 
        updated.Site = 'original';
		List<Account> currentRecords = new List<Account>{updated};

		Test.startTest();
 			List<SObject> updatedRecords = Utils.getChangedObjects(currentRecords, oldRecords, new List<String>{'Site'});
		Test.stopTest();

		System.assertEquals(0, updatedRecords.size());
	}
}