public class TESTCommunityFooterController {
    @AuraEnabled
    public static String getCookieInfo() {
        Cookie c = ApexPages.currentPage().getCookies().get('/hubbroker/s/counter');
        system.debug(c);
        return c.getValue();
    }
    
    @AuraEnabled
    public static void setCookieInfo() {
        Cookie counter = new Cookie('counter', '1111111111111111111111111111111111111', null, 100, false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
    }
}