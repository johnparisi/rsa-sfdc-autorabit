@isTest
public  class SMEQ_ResourceBundlesRestService_Test {
    
    static testMethod void testGetPathVariablesForVariantAndBundle(){
        String requestURIStringWithBundle = '/resourceBundles/www/Quote';
        Map<integer,String> pathVariablesMap = SMEQ_ResourceBundlesRestService.getPathVariablesIfPresent(requestURIStringWithBundle);
        System.debug('variant:' + pathVariablesMap.get(1));
        System.debug('bundle:' + pathVariablesMap.get(2));
        System.assertEquals('www', pathVariablesMap.get(2));
        System.assertEquals('Quote', pathVariablesMap.get(3));
    }
    
    static testMethod void testGetPathVariablesForVariantOnly(){
        String requestURIStringWithBundle = '/resourceBundles/www';
        Map<integer,String> pathVariablesMap = SMEQ_ResourceBundlesRestService.getPathVariablesIfPresent(requestURIStringWithBundle);
        System.debug('variant:' + pathVariablesMap.get(1));
        System.debug('bundle:' + pathVariablesMap.get(2));
        System.assertEquals('www', pathVariablesMap.get(2));
        System.assertEquals(null, pathVariablesMap.get(3));
    }
    
    static testMethod void testGetPathVariablesWithoutVariantAndBundle(){
        String requestURIStringWithBundle = '/resourceBundles';
        Map<integer,String> pathVariablesMap = SMEQ_ResourceBundlesRestService.getPathVariablesIfPresent(requestURIStringWithBundle);
        System.debug('variant:' + pathVariablesMap.get(1));
        System.debug('bundle:' + pathVariablesMap.get(2));
        System.assertEquals(null, pathVariablesMap.get(2));
        System.assertEquals(null, pathVariablesMap.get(3));
    }
    
    static testMethod void testGetResourceBundles(){
        generateResourceBundlesTestData();
        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/resourceBundles/www/Bundle1';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
		SMEQ_ResourceBundlesModel smeResourceBundlesModel = SMEQ_ResourceBundlesRestService.getResourceBundles();
		System.assert(smeResourceBundlesModel!=null);
        System.debug('Resource bundle being returend ****' + smeResourceBundlesModel.resourceBundleEnglish);
        System.assertEquals(5, smeResourceBundlesModel.resourceBundleEnglish.size());
        System.assertEquals(5, smeResourceBundlesModel.resourceBundleFrench.size());
    }
    
    static testMethod void testGetResourceBundlesWithVarint1AndBundle1(){
        generateResourceBundlesTestData();
        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/resourceBundles/variant1/';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
		SMEQ_ResourceBundlesModel smeResourceBundlesModel = SMEQ_ResourceBundlesRestService.getResourceBundles();
		System.assert(smeResourceBundlesModel!=null);
        System.assertEquals(5, smeResourceBundlesModel.resourceBundleEnglish.size());
        System.assertEquals(5, smeResourceBundlesModel.resourceBundleFrench.size());
        for(SMEQ_KeyValueModel keyValue: smeResourceBundlesModel.resourceBundleEnglish){
            System.debug('The label for key:' + keyValue.key + ' Value:' + keyValue.value);
            if(keyValue.key == 'test.label2'){
                System.assertEquals('TEST 2 for bundle 2',keyValue.value);
            }
        }
        for(SMEQ_KeyValueModel keyValue: smeResourceBundlesModel.resourceBundleFrench){
            System.debug('The label for key:' + keyValue.key + ' Value:' + keyValue.value);
            if(keyValue.key == 'test.label2'){
                System.assertEquals('TEST 2 for bundle 2_FR',keyValue.value);
            }
        }
    }
    
    static void generateResourceBundlesTestData(){
    	List<SMEQ_RESOURCE_BUNDLES__c> smeResourceList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST for bundle 1', 'www', 'test.label'));
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST 1 for bundle 1', 'www', 'test.label1'));
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST 2 for bundle 2', 'www', 'test.label2'));
        smeResourceList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST for bundle 2', 'www', 'test.label'));
        smeResourceList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST for bundle 2', 'www', 'test.label1'));
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST Variant for bundle 1', 'variant1', 'test.label2'));
        smeResourceList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST Variant for bundle 2', 'variant1', 'test.label'));
        
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST for bundle 1_FR', 'www', 'test.label'));
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST 1 for bundle 1_FR', 'www', 'test.label1'));
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST 2 for bundle 2_FR', 'www', 'test.label2'));
        smeResourceList.add(insertResourceBundles('Bundle2', 'CA', 'FR', 'TEST for bundle 2_FR', 'www', 'test.label'));
        smeResourceList.add(insertResourceBundles('Bundle2', 'CA', 'FR', 'TEST for bundle 2_FR', 'www', 'test.label1'));
        smeResourceList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST Variant for bundle 1_FR', 'variant1', 'test.label2'));
        smeResourceList.add(insertResourceBundles('Bundle2', 'CA', 'FR', 'TEST Variant for bundle 2_FR', 'variant1', 'test.label'));
        insert smeResourceList;
    }
    
     static SMEQ_RESOURCE_BUNDLES__c insertResourceBundles (string bundleId, string countryCode, string languageCode, string value, string variant, string stringId){
        SMEQ_RESOURCE_BUNDLES__c smeResourceBundles = new SMEQ_RESOURCE_BUNDLES__c();
        smeResourceBundles.bundle__c = bundleId;
        smeResourceBundles.country_code__c = countryCode;
        smeResourceBundles.language_code__c = languageCode;
        smeResourceBundles.value__c = value;
        smeResourceBundles.variant__c = variant;
        smeResourceBundles.string_id__c = stringId;
        return smeResourceBundles;
    }

}