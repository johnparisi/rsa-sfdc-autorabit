/*
* Risk__c Service Layer
* Contains processing service methods related to the Risk__c Object
*/

public without sharing class RiskService
{
    public static final String RISK_RECORDTYPE_LOCATION = 'Location';
    public static final String RISK_RECORDTYPE_LIABILITY = 'Liability';
    public static final String RISK_RECORDTYPE_AGGREGATE = 'Annual_Aggregate';
    public static final String RISK_RECORDTYPE_CYBER = 'Privacy_Breach';

    public static final String COVERAGE_BUILDING = 'Building';
    public static final String COVERAGE_EQUIPMENT = 'Equipment';
    public static final String COVERAGE_STOCK = 'Stock';
    public static final String COVERAGE_RENTAL = 'Rental Revenue';
    public static final String COVERAGE_CONTENTS = 'Contents';
    public static final String COVERAGE_CGL = 'Commercial General Liability';
    public static final String COVERAGE_AGGREGATE = 'Property Aggregate Package';
    public static final String COVERAGE_CYBER = 'Privacy Breach';
    public static final String COVERAGE_PROPERTY = 'Property';
    public static final String COVERAGE_FLOOD = 'Flood';
    public static final String COVERAGE_SEWER = 'Sewer Backup';
    public static final String COVERAGE_EARTHQUAKE = 'Earthquake';

    public static final String PACKAGE_STANDARD = 'Standard';
    public static final String PACKAGE_OPTION_1 = 'Option 1';
    public static final String PACKAGE_OPTION_2 = 'Option 2';
    public static Set<Id> renewalAndAmendmentRisks = new Set<id>();

    // The purpose of this list is to store the list of coverages for all risks being processed in each batch.
    List<Coverages__c> coveragesList;
    

    /**
     * insertDefaultCoverages
     * @param   records     Risk__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     * Set the default coverages for the risk.
     */
    public void insertDefaultCoverages(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
        Map<ID, SIC_Code_Detail_Version__c> quote2scdv = getSicCodeDetailVersionMapping(records);

        if (oldRecords == null)
        {
            for (Risk__c r : records)
            {
            	//The following condition is put into place for every trigger to ensure that the defaulted values are not populated when a new risk is created
            	//for an RA transaction 
                if (!renewalAndAmendmentRisks.contains(r.Id))
                {
                	SIC_Code_Detail_Version__c scdv = quote2scdv.get(r.Quote__c);
	                if (scdv != null)
	                {
	                    updateRiskForDefaultCoverages(r, scdv);
	                 }
            	}
            }
        }
        else
        {
            for (Risk__c r : (List<Risk__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Coverage_Selected_Option_1__c', 'Coverage_Selected_Option_2__c'}))
            {
                SIC_Code_Detail_Version__c scdv = quote2scdv.get(r.Quote__c);
                if (scdv != null)
                {
                    updateRiskForDefaultCoverages(r, scdv);
                }
            }
        }
    }

    /*
     * Helper method to set the default coverages for the risk.
     */
    private void updateRiskForDefaultCoverages(Risk__c r, SIC_Code_Detail_Version__c scdv)
    {
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);

        if (r.recordTypeId == RiskTypes.get(RISK_RECORDTYPE_LOCATION))
        {
            Set<String> availableCoverages = new Set<String>();
            for (String coverage : getAllLocationCoverageTypes(scdv.Coverage_Defaults__c).split(';'))
            {
                availableCoverages.add(coverage);
            }

            r.Coverage_Selected_Option_1__c = addRequiredCoverages(r.Coverage_Selected_Option_1__c, availableCoverages);
            r.Coverage_Selected_Option_2__c = addRequiredCoverages(r.Coverage_Selected_Option_2__c, availableCoverages);
        }
        else if (r.recordTypeId == RiskTypes.get(RISK_RECORDTYPE_LIABILITY))
        {
            r.Coverage_Selected_Option_1__c = COVERAGE_CGL;
            r.Coverage_Selected_Option_2__c = COVERAGE_CGL;
        }
        else if (r.recordTypeId == RiskTypes.get(RISK_RECORDTYPE_AGGREGATE))
        {
            r.Coverage_Selected_Option_1__c = COVERAGE_AGGREGATE;
            r.Coverage_Selected_Option_2__c = COVERAGE_AGGREGATE;
        }
        else if (r.recordTypeId == RiskTypes.get(RISK_RECORDTYPE_CYBER))
        {
            r.Coverage_Selected_Option_1__c = COVERAGE_CYBER;
            r.Coverage_Selected_Option_2__c = COVERAGE_CYBER;
        }

    }

    /*
     * Helper method to add any required coverages to the risk.
     */
    private String addRequiredCoverages(String coveragesSelected, Set<String> availableCoverages)
    {
        if (coveragesSelected == null)
        {
            coveragesSelected = '';
        }

        // Set to store all coverages.
        Set<String> requiredCoverages = new Set<String>();
        // Add all coverages already selected.
        requiredCoverages.addAll(coveragesSelected.split(';'));
        // Add all coverages enabled.
        requiredCoverages.addAll(availableCoverages);

        String coverages = '';
        for (String coverage : requiredCoverages)
        {
            coverages = coverage + ';' + coverages;
        }
        return coverages;
    }

    /**
     * removeUnselectedCoverages
     * @param   records     Risk__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     * Remove coverages depending on the selection made on the Risk__c level
     */
    public void removeUnselectedCoverages(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
        Set<Id> updatedCoveragePackages = new Set<Id>();
        Map<Id, Map<String, List<String>>> riskCoveragesSelected = new Map<Id, Map<String, List<String>>>();
        
        for (Risk__c r : (List<Risk__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Coverage_Selected_Option_1__c', 'Coverage_Selected_Option_2__c'}))
        {
            updatedCoveragePackages.add(r.Id);
            
            String option1Selected = r.Coverage_Selected_Option_1__c;
            String option2Selected = r.Coverage_Selected_Option_2__c;
            Map<String, List<String>> options = new Map<String, List<String>>();
            
            options.put(PACKAGE_OPTION_1, option1Selected != null ? option1Selected.split(';') : new List<String>());
            options.put(PACKAGE_OPTION_2, option2Selected != null ? option2Selected.split(';') : new List<String>());
            riskCoveragesSelected.put(r.Id, options);
        }
        
        initializeExistingCoverages(updatedCoveragePackages);
        if (!updatedCoveragePackages.isEmpty())
        {
            List<Coverages__c> coveragesToRemove = new List<Coverages__c>();
            for (Coverages__c c : [
                SELECT id, Type__c, Risk__c, Package_ID__c, Risk__r.RecordType.DeveloperName
                FROM Coverages__c
                WHERE Risk__c IN :updatedCoveragePackages
            ])
            {
                if (c.Risk__r.RecordType.DeveloperName != RiskService.RISK_RECORDTYPE_LOCATION)
                {
                    coveragesToRemove.add(c); // Non-locations need to be cleared.
                }
                else if (c.Package_ID__c != PACKAGE_STANDARD && !isCoverageSelected(c, riskCoveragesSelected))
                {
                    coveragesToRemove.add(c); // Locations need to keep the standard package coverages.
                }
            }
            delete coveragesToRemove;
        }
    }
    /**
     * removeUnselectedCoverages
     * @param   records     Risk__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     * Remove coverages depending on the selection made on the Risk__c level
     */
    public void removeUnselectedCoveragesFromPolicy(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
        Set<Id> updatedCoveragePackages = new Set<Id>();
        Map<Id, Map<String, List<String>>> riskCoveragesSelected = new Map<Id, Map<String, List<String>>>();

        for (Risk__c r : (List<Risk__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Coverage_Selected_Policy__c'}))
        {
            updatedCoveragePackages.add(r.Id);

            String optionsSelected = r.Coverage_Selected_Policy__c;
            Map<String, List<String>> options = new Map<String, List<String>>();
            
            options.put(PACKAGE_STANDARD, optionsSelected != null ? optionsSelected.split(';') : new List<String>());
            riskCoveragesSelected.put(r.Id, options);
        }

        initializeExistingCoverages(updatedCoveragePackages);
        if(!updatedCoveragePackages.isEmpty()){
            List<Coverages__c> coveragesToRemove = new List<Coverages__c>();
            for (Coverages__c c : [
                SELECT id, Type__c, Risk__c, Package_ID__c
                FROM Coverages__c
                WHERE Risk__c IN :updatedCoveragePackages
            ])
            {
                if (!isCoverageSelected(c, riskCoveragesSelected))
                {
                    coveragesToRemove.add(c);
                }
            }
            delete coveragesToRemove;
        }
    }
    

    /*
     * Determines whether or not a coverages is selected based on the package selected on the quote.
     */
    private boolean isCoverageSelected(Coverages__c c, Map<Id, Map<String, List<String>>> m)
    {
        for (String s : m.get(c.Risk__c).get(c.Package_ID__c))
        {
            if (c.Type__c == s)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * insertNewCoverages
     * @param   records     Risk__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     * Insert new coverages depending on the selection made on the Risk__c level
     */
    public void insertNewCoverages(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
        Set<Id> updatedCoveragePackages = new Set<Id>();
        Set<Id> quotes = new Set<Id>();
        List<Risk__c> newPolicyRecords = new List<Risk__c>();
        List<Risk__c> newQuoteRecords = new List<Risk__c>();
        Map<Id, Risk__c> newRecords = new Map<Id, Risk__c>();

        isRenewalandAmendment(records);
        Map<ID, SIC_Code_Detail_Version__c> quote2scdv = getSicCodeDetailVersionMapping(records);
        
        if (oldRecords == null)
        {
            for (Risk__c r : records)
            {
                updatedCoveragePackages.add(r.Id);
                quotes.add(r.Quote__c);
                newRecords.put(r.Id, r);
            }
        }
        
        else
        {
            for(Risk__c r: records){
                //The following condition is put into place to ensure that the right list is populated 
            	//depending on the type of transaction
                if(!renewalAndAmendmentRisks.contains(r.Id))
                {
                    newQuoteRecords.add(r);
                }     
                else {
                    newPolicyRecords.add(r);
                }
            }
            if(newQuoteRecords != null || !newQuoteRecords.isEmpty()){
                system.debug('updating new records' + newQuoteRecords);
                for (Risk__c r : (List<Risk__c>) Utils.getChangedObjects(newQuoteRecords, oldRecords, new List<String> {'Coverage_Selected_Option_1__c', 'Coverage_Selected_Option_2__c'}))
                {
                    updatedCoveragePackages.add(r.Id);
                    quotes.add(r.Quote__c);
                }
            }
            if(newPolicyRecords != null || !newPolicyRecords.isEmpty()){
                system.debug('updating new policy records' + newPolicyRecords);
                for (Risk__c r : (List<Risk__c>) Utils.getChangedObjects(newPolicyRecords, oldRecords, new List<String> {'Coverage_Selected_Policy__c'}))
            	{
                	updatedCoveragePackages.add(r.Id);
                	quotes.add(r.Quote__c);
            	}
            }
            for (Risk__c r : records)
            {
                newRecords.put(r.Id, r);
            }
        }
        
        if (!updatedCoveragePackages.isEmpty())
        {
            updateCoverages(updatedCoveragePackages, quotes, newRecords, quote2scdv);
        }
    }

    /*
     * Helper method to manage coverages under the risk.
     */
    private void updateCoverages(Set<Id> updatedCoveragePackages, Set<Id> quotes, Map<Id, Risk__c> newRecords,
                                 Map<ID, SIC_Code_Detail_Version__c> quote2scdv)
    {
        if (quotes != null && !quotes.isEmpty())
        {
            initializeExistingCoverages(updatedCoveragePackages);

            Schema.DescribeFieldResult packageIDResult = Coverages__c.Package_ID__c.getDescribe();
            List<Schema.PicklistEntry> packageTypes = packageIDResult.getPicklistValues();

            List<Coverages__c> coverages = new List<Coverages__c>();
            for (Risk__c r : newRecords.values())
            {
                // Iterate over the package IDs listed in the Coverages object.
                // The following condition is put into place to ensure that all the default coverages are not added
            	//for all packages in RA transactions
                if(!renewalAndAmendmentRisks.contains(r.Id)){
                    for (Schema.PicklistEntry pkg : packageTypes)
                    {
                        String defaultCoverages = '';
                        String addonCoverages = '';
                        SIC_Code_Detail_Version__c scdv = quote2scdv.get(r.quote__c);
    
                        if (scdv != null)
                        {
                            defaultCoverages = getAllLocationCoverageTypes(scdv.Coverage_Defaults__c);
                            addonCoverages = getAllLocationCoverageTypes(scdv.Coverage_Add_ons__c);
                        }
                        else{ // This is implemented for Policy Works from where cases can come without SIC Code
                            defaultCoverages = RiskService.COVERAGE_BUILDING+';'+RiskService.COVERAGE_EQUIPMENT+';'+RiskService.COVERAGE_STOCK+';'+RiskService.COVERAGE_CONTENTS;
                            addonCoverages = RiskService.COVERAGE_EARTHQUAKE+';'+RiskService.COVERAGE_FLOOD+';'+RiskService.COVERAGE_SEWER;
                        }
                        system.debug('coverages before we add all' + coverages) ;
    
                        coverages.addAll(getCoverages(newRecords.get(r.id), pkg.getValue(), defaultCoverages, addonCoverages));
                    }
                }
                else {
                    Schema.PicklistEntry pkg = packageTypes.get(0);
                    String defaultCoverages = '';
                    String addonCoverages = '';
                    SIC_Code_Detail_Version__c scdv = quote2scdv.get(r.quote__c);
                    if (scdv != null)
                    {
                        defaultCoverages = getAllLocationCoverageTypes(scdv.Coverage_Defaults__c);
                        addonCoverages = getAllLocationCoverageTypes(scdv.Coverage_Add_ons__c);
                    }

                    coverages.addAll(getCoverages(newRecords.get(r.id), pkg.getValue(), defaultCoverages, addonCoverages));
                }
            }

            Map<Id, Quote__c> quotePackage = new Map<Id, Quote__c>([
                SELECT id, Package_Type_Selected__c
                FROM Quote__c
                WHERE id IN :quotes
            ]);

            Map<Id, Quote__c> riskToQuote = new Map<Id, Quote__c>();
            for (Risk__c r : newRecords.values())
            {
                riskToQuote.put(r.Id, quotePackage.get(r.Quote__c));
            }

            for (Coverages__c c : coverages)
            {
                Risk__c r = newRecords.get(c.Risk__c);
                Quote__c q = riskToQuote.get(r.Id);

                if (c.Type__c == COVERAGE_BUILDING)
                {
                    c.Limit_Value__c = r.Building_Value__c;
                }
                else if (c.Type__c == COVERAGE_RENTAL)
                {
                    c.Limit_Value__c = r.Annual_Rental_Revenue__c;
                }

                if (q != null && q.Package_Type_Selected__c == c.Package_ID__c)
                {
                    c.Selected__c = true;
                }

                System.debug('coverage limit value: ' + c.Limit_Value__c + ' c.Selected__c: ' + c.Selected__c);
            }

            try
            {
                insert coverages;
            }
            catch (Exception e)
            {
                System.debug('RiskService - insert coverage failed: ' + e.getMessage());
            }
        }
    }

    /**
     * getCoverages
     * @param   risk             Coverage__c to be referenced as parent
     * @param   packageId        The package ID to get for the coverages
     *
     * Helper method to retrieve a list of all new coverages related to the package.
     */
    private List<Coverages__c> getCoverages(Risk__c risk, String packageId, String defaultCoverages, String addonCoverages)
    {
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);

        List<Coverages__c> coverages = new List<Coverages__c>();

        String coveragesSelected = '';
        if (RiskTypes.get(RISK_RECORDTYPE_LOCATION) == risk.RecordTypeId)
        {
            //The following condition is put into place to ensure that all the defaults are only 
            //added in for NB transactions 
            if(!renewalAndAmendmentRisks.contains(risk.Id)){
                if (packageId == PACKAGE_STANDARD)
                {
                    String allCoverages = defaultCoverages + ';' + addonCoverages;
                    coveragesSelected = getAllLocationCoverageTypes(allCoverages);
                }
            	else if (packageId == PACKAGE_OPTION_1)
                {
                    coveragesSelected = risk.Coverage_Selected_Option_1__c;
                }
                else if (packageId == PACKAGE_OPTION_2)
                {
                    coveragesSelected = risk.Coverage_Selected_Option_2__c;
                }
                coveragesSelected = addRequiredCoverages(coveragesSelected, new Set<String>(defaultCoverages.split(';')));
            }
            else{
                coveragesSelected = risk.Coverage_Selected_Policy__c;
            }
        }
        else if (RiskTypes.get(RISK_RECORDTYPE_LIABILITY) == risk.RecordTypeId)
        {
            coveragesSelected = COVERAGE_CGL;
        }
        else if (RiskTypes.get(RISK_RECORDTYPE_AGGREGATE) == risk.RecordTypeId)
        {
            coveragesSelected = COVERAGE_AGGREGATE;
        }
        else if (RiskTypes.get(RISK_RECORDTYPE_CYBER) == risk.RecordTypeId)
        {
            coveragesSelected = COVERAGE_CYBER;
        }

        
        system.debug('coveragesSelected' + coveragesSelected);
        
        // Only keep the coverages which are not created for the risk.
        for (String coverageType : coveragesSelected.split(';'))
        {
            //The following condition is put into place to ensure that the following code is not
            //run for a GP transaction (but run for any other RA transaction & NB transaction)
            if (!coverageExists(risk.id, coverageType, packageId)) 
            {
                coverages.add(getNewCoverage(risk, coverageType, packageId));
            }
        }

        return coverages;
    }

    /**
     * getCoverage
     * @param   risk            Risk__c to be referenced as parent
     * @param   coverageType    Coverage type
     *
     * Helper method to create a new default coverage for a Risk__c with a certain coverage type.
     */
    private Coverages__c getNewCoverage(Risk__c risk, String coverageType, String packageId)
    {
        Coverages__c c = new Coverages__c();

        c.Risk__c = risk.Id;
        c.Type__c = coverageType;
        c.Package_ID__c = packageId;

        return c;
    }

    /*
     * coverageExists
     * @param   riskId          The ID of the risk which the coverage belongs to
     * @param   coverageType    The type of the coverage. i.e. Building
     * @param   packageId       The package ID of the coverage. i.e. Standard, Option 1
     *
     * Helper method to determine if the coverage already exists.
     * The three fields, riskId, coverageType, and packageId are sufficient to derive a unique coverage record.
     */
    private boolean coverageExists(Id riskId, String coverageType, String packageId)
    {
        for (Coverages__c cov : coveragesList)
        {
            if (cov.risk__c == riskId && cov.type__c == coverageType && cov.package_ID__c == packageId)
            {
                return true;
            }
        }

        return false;
    }

    /*
     * initializeExistingCoverages
     * @param risks         A set of risk IDs used to retrieve the risks.
     *
     * Helper method to retrieve the set of all coverages for a given set of risk IDs.
     */
    private Void initializeExistingCoverages(Set<Id> risks)
    {
        if(!risks.isEmpty()){
            coveragesList = [
                SELECT id, Risk__c, package_ID__c, type__c, Limit_Value__c, Deductible_Value__c
                FROM Coverages__c
                WHERE Risk__c IN :risks
            ];
        }

    }

    /**
     * Filter out non-location coverage types.
     */
    @testVisible
    public static String getAllLocationCoverageTypes(String coverageList)
    {
        // Use set to enforce uniqueness.
        Set<String> coverages = new Set<String>();

        if (coverageList != null)
        {
            for (String coverage : coverageList.split(';'))
            {
                // Add only location coverages.
                if (coverage != 'null' &&
                    coverage != '' &&
                    coverage != COVERAGE_CGL &&
                    coverage != COVERAGE_AGGREGATE &&
                    coverage != COVERAGE_CYBER)
                {
                    coverages.add(coverage);
                }
            }
        }

        // Covert set to list.
        List<String> standardCoverages = new List<String>(coverages);

        return Utils.commaDelimit(standardCoverages);
    }

    /*
     * updateDefaultCoverageLimits
     * @param   records     Risk__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     * Update default building coverage limits based on parent risk value.
     */
    public void updateDefaultCoverageLimits(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
        Map<Id, Risk__c> updatedLimits = new Map<Id, Risk__c>();

        for (Risk__c r : (List<Risk__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Building_Value__c', 'Annual_Rental_Revenue__c'}))
        {
            updatedLimits.put(r.Id, r);
        }
        
        if (!updatedLimits.isEmpty())
        {
            List<Coverages__c> coveragesToUpdate = [
                SELECT id, Limit_Value__c, Type__c, Risk__c
                FROM Coverages__c
                WHERE risk__c IN :updatedLimits.keySet()
            ];
            
            for (Coverages__c c : coveragesToUpdate)
            {
                Risk__c r = updatedLimits.get(c.Risk__c);
                if (c.Type__c == COVERAGE_BUILDING)
                {
                    c.Limit_Value__c = r.Building_Value__c;
                }
                else if (c.Type__c == COVERAGE_RENTAL)
                {
                    c.Limit_Value__c = r.Annual_Rental_Revenue__c;
                }
            }
            
            update coveragesToUpdate;
        }
    }

    /**
     * FP-58
     * getSicCodeDetailVersionMapping
     *
     * @param records: The list of new or updated records.
     * @return: A map from the quote record ID to its parent quote's case's SIC Code Detail Version.
     *
     * Create a mapping from a risk ID to its SIC Code Detail Version. Leverages the SMEQ_SICCodeService.
     */
    private Map<ID, SIC_Code_Detail_Version__c> getSicCodeDetailVersionMapping(List<Risk__c> records)
    {
        List<Id> quoteIds = new List<Id>();
        // Get the parent quotes
        for (Risk__c record : records)
        {
            quoteIds.add(record.Quote__c);
        }

        // Retrieve the SIC Code Detail Versions.
        return SMEQ_SicCodeService.getSicCodesInAppetiteForQuotes(quoteIds);
    }
    
        /**
     *      
     * @param risk: A risk record. 
     * @return: Boolean depending on if the risk sits on a RA or NB Quote 
     *
     */
    public static Set<Id> isRenewalAndAmendment(List<Risk__c> risks){
        for(Risk__c risk: risks){
            if (risk.Related_COM__c != null){
                renewalAndAmendmentRisks.add(risk.Id);
            }
        }
        return renewalAndAmendmentRisks;
    }
    
    /*
     * checkRiskReferralTrigger
     * @param   records     Risk__c that were inserted/updated
     * @param   oldRecords  Previous state of the risks that were inserted/updated mapped by id
     *
     */
    public void checkRiskReferralTrigger(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
        List<Risk__c> modifiedPropDetailsRisk;
        List<Risk__c> modifiedAddressRisk = (List<Risk__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Address_Line_1__c', 'Address_Line_2__c'});
		        
        if (modifiedAddressRisk.size() == 0) {
        	modifiedPropDetailsRisk = (List<Risk__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Construction_Type__c', 'Number_of_Stories__c', 'Year_Built__c', 'Sprinkler_Coverage__c'});

            if (modifiedPropDetailsRisk.size() > 0)
            {
                Map<Id, Id> riskToQuote = new Map<Id, Id>();
                
                for (Risk__c r : modifiedPropDetailsRisk) {
                    riskToQuote.put(r.Id, r.quote__c);
                }
                
                Map<Id, Id> quoteToCase = new Map<Id, Id>();
                
                List<Quote__c> qs = [
                    SELECT id, Case__c, Case__r.Referral_Trigger_Reason_s__c
                    FROM Quote__c
                    WHERE id IN :riskToQuote.values()
                ];
                
                for (Quote__c q : qs) {
                    quoteToCase.put(q.Id, q.case__c);
                }
                
                Map<Id, Case> riskToCase = new Map<Id, Case>();
                
                Map<Id, Case> cases = new Map<Id, Case>([
                    SELECT id, Referral_Trigger_Reason_s__c, Offering_Project__c
                    FROM Case
                    WHERE Id IN :quoteToCase.values()
                ]);
                

                
                for(Risk__c risks : modifiedPropDetailsRisk) {
                    for (Id qid : quoteToCase.keySet()) {
                        if(risks.quote__c == qid) {
                            riskToCase.put(risks.Id, cases.get(quoteToCase.get(qid)));
                        }
                    }
                }
                                             
                Map<Id, List<String>> caseReferralTriggerReasons = ProcessReferralTriggerRules.runReferralTriggerRules(modifiedPropDetailsRisk, oldRecords, riskToCase);

                for (Case c : cases.values()) {
                    if (caseReferralTriggerReasons.get(c.Id) != null) {
                    	c.Referral_Trigger_Reason_s__c = Utils.commaDelimit(caseReferralTriggerReasons.get(c.Id));
                    }
                }
                
                update cases.values();
            }            
        }
    }

}