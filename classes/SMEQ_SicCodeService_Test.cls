/**
 * Author: Aakash Dang
 * Created At: November 4, 2016
 * 
 * Unit tests for SMEQ_SicCodeService
 */
@isTest
public class SMEQ_SicCodeService_Test
{
    @isTest
    static void testGetDefaultSicCodesWithValidSIC()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SIC_Code_Detail__c sicCodeDetails = SMEQ_TestDataGenerator.getSicCodeDetail();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode(sicCodeDetails);
            List<SIC_Code_Detail__c> sicCodeDetailList = SMEQ_SicCodeService.getDefaultSicCodes(sicCode.ID);
            System.assert(!sicCodeDetailList.isEmpty());
            System.assertEquals(1, sicCodeDetailList.size());
            
            sicCodeDetailList = SMEQ_SicCodeService.getDefaultSicCodes(null);
            List<SIC_Code_Detail__c> scds = [SELECT id FROM Sic_Code_Detail__c];
            System.assert(!sicCodeDetailList.isEmpty());
            System.assertEquals(scds.size(), sicCodeDetailList.size());
        }
    }
    
    @isTest
    static void testGetSicCodesByCaseIDWithNoCase()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Test.startTest();
            SIC_Code_Detail__c sicCodeDetails = SMEQ_TestDataGenerator.getSicCodeDetail();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode(sicCodeDetails);
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase(sicCode);
            List<SIC_Code_Detail__c> sicCodeVersion = new List<SIC_Code_Detail__c>();
            sicCodeVersion = SMEQ_SicCodeService.getDefaultSicCodesForCase(null);
            System.assert(sicCodeVersion.isEmpty());
            Test.stopTest();
        }
    }
    
    @isTest
    static void testGetSicCodesByCaseIDWithCase()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SIC_Code_Detail__c sicCodeDetails = SMEQ_TestDataGenerator.getSicCodeDetail();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode(sicCodeDetails);
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase(sicCode);
            List<SIC_Code_Detail__c> sicCodeVersion = new List<SIC_Code_Detail__c>();
            sicCodeVersion = SMEQ_SicCodeService.getDefaultSicCodesForCase(cse.ID);
            System.assert(!sicCodeVersion.isEmpty());
        }
    }
    
    @isTest
    static void testGetSicCodesInAppetiteByCaseIDWithNoCase()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Test.startTest();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase(sicCode);
            Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote(cse);
            List<SIC_Code_Detail_Version__c> sicCodeVersion = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(null);
            System.assert(sicCodeVersion.isEmpty());
            Test.stopTest();
        }
    }
}