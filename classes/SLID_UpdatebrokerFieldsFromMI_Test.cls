/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SLID_UpdatebrokerFieldsFromMI_Test 
{

    static testMethod void testUpdateBrokerFieldsFromMIforNATIONAL_BROKER_GROUP() 
    {
    	//Create Test Data
    	createMappingSettings();
        createBrokerStageRecords();
        createMAP_NATLTERRrecords();
        createMAP_Mrecords();
        create_MAP_CT01598Recs();
        
        test.startTest();
        SLID_UpdateBrokerFieldsFromMI updateFromMIBatch = new SLID_UpdateBrokerFieldsFromMI();
      	Id BatchProcessId = Database.ExecuteBatch(updateFromMIBatch);
      	test.stopTest();
      	
      	List<Broker_Stage__c> bstageRecs = [Select Id , NATLTERR__c , NATIONAL_BROKER_GROUP__c , ACTIVTO__c , ACTIVE_TO__c , BRANCH__c , TEAM__c , SUBCOY__c , REGION_GROUP__c ,
      										NEW_STRATEGIC_SEGMENT__c , NSS__c , BRAND__c , STREET__c , STREETNO__c , STREETSU__c , STREETDIR__c , ADDRNO1__c , ADDRNO2__c , AGENT_STREET__c
      										from Broker_Stage__c];
      	for(Broker_Stage__c bStage : bstageRecs)
      	{
      		//Check NATIONAL_BROKER_GROUP__c
      		if(bStage.NATLTERR__c == 'NAT1')
      			system.assertEquals('NATIONAL BROKER GROUP 1',bStage.NATIONAL_BROKER_GROUP__c);
      		if(bStage.NATLTERR__c == 'NAT2')
      			system.assertEquals('NATIONAL BROKER GROUP 2',bStage.NATIONAL_BROKER_GROUP__c);
      		if(bStage.NATLTERR__c == 'NAT3')
      			system.assertEquals('NATIONAL BROKER GROUP 3',bStage.NATIONAL_BROKER_GROUP__c);
      		if(bStage.NATLTERR__c == 'NAT4')
      			system.assertEquals('NATIONAL BROKER GROUP 4',bStage.NATIONAL_BROKER_GROUP__c);
      		if(bStage.NATLTERR__c == 'NAT5')
      			system.assertEquals('NATIONAL BROKER GROUP 5',bStage.NATIONAL_BROKER_GROUP__c);
      		if(bStage.NATLTERR__c == 'NAT6')
      			system.assertEquals('NATIONAL BROKER GROUP 6',bStage.NATIONAL_BROKER_GROUP__c);
      		
      		//Check ACTIVE_TO__c
      		if(bStage.ACTIVTO__c == 99999999)
      			system.assertEquals(Date.newInstance(3000,12,31),bStage.ACTIVE_TO__c);
      		if(bStage.ACTIVTO__c == 20210610)	
      			system.assertEquals(Date.newInstance(2021,06,10),bStage.ACTIVE_TO__c);
      			
      		//Check REGION_GROUP__c
      		if(bStage.BRANCH__c == 8088 && bStage.TEAM__c == '7088')
      			system.assertEquals('Prairies' , bStage.REGION_GROUP__c);
      		if(bStage.BRANCH__c == 7088 && bStage.TEAM__c == '8088')
      			system.assertEquals('Pacific' , bStage.REGION_GROUP__c);
      		if(bStage.SUBCOY__c == 4 && bStage.BRANCH__c == 6801)
      		{
      			system.assertEquals('Ontario' , bStage.REGION_GROUP__c);
      			system.assertEquals('PI',bStage.NEW_STRATEGIC_SEGMENT__c);
      			system.assertEquals('PI',bStage.NSS__c);
      			system.assertEquals('WA',bStage.BRAND__c);
      		}
      		if(bStage.SUBCOY__c == 4 && bStage.BRANCH__c == 6988)
      		{
      			system.assertEquals('Ontario' , bStage.REGION_GROUP__c);
      			system.assertEquals('PI',bStage.NEW_STRATEGIC_SEGMENT__c);
      			system.assertEquals('PI',bStage.NSS__c);
      			system.assertEquals('WA',bStage.BRAND__c);
      		}
      		if(bStage.SUBCOY__c == 6 && bStage.BRANCH__c == 188)
      		{
      			system.assertEquals('Atlantic' , bStage.REGION_GROUP__c);
      			system.assertEquals('MID MKT',bStage.NEW_STRATEGIC_SEGMENT__c);
      			system.assertEquals('GSL',bStage.NSS__c);
      			system.assertEquals('RSA',bStage.BRAND__c);
      		}
      		
      		system.debug('----AGENT_STREET__c--------' + bStage.AGENT_STREET__c);
      	}
    }
    
    static testMethod void testExceptionsForUpdateBrokerFieldsFromMIforNATIONAL_BROKER_GROUP()
    {
    	createMappingSettings();
    	createBrokerStageRecords();
        createMAP_NATLTERRrecords();
        createMAP_Mrecords();
        create_MAP_CT01598Recs();
        
    	test.startTest();
    	SLID_UpdateBrokerFieldsFromMI updateFromMIBatch1 = new SLID_UpdateBrokerFieldsFromMI();
      	SLID_UpdateBrokerFieldsFromMI.IS_TEST = true;
      	Database.ExecuteBatch(updateFromMIBatch1);
      	test.stopTest();
    }
    
    static testMethod void testNullExceptionForUpdateBrokerFieldsFromMIforNATIONAL_BROKER_GROUP()
    {
    	createMappingSettings();
    	createBrokerStageRecords();
        createMAP_NATLTERRrecords();
        createMAP_Mrecords();
        create_MAP_CT01598Recs();
        
    	test.startTest();
		SLID_UpdateBrokerFieldsFromMI updateFromMIBatch2 = new SLID_UpdateBrokerFieldsFromMI();
		SLID_UpdateBrokerFieldsFromMI.IS_TEST = true;
      	updateFromMIBatch2.setNATIONAL_BROKER_GROUPfield(null,null);
      	test.stopTest();    	
    }
    
    static testMethod void testExceptionForUpdateACTIVETOfield()
    {
    	createMappingSettings();
    	createBrokerStageRecords();
        createMAP_NATLTERRrecords();
        createMAP_Mrecords();
        create_MAP_CT01598Recs();
    	
    	test.startTest();
    	SLID_UpdateBrokerFieldsFromMI updateFromMIBatch3 = new SLID_UpdateBrokerFieldsFromMI();
    	updateFromMIBatch3.setACTIVE_TOfieldOnBrokerStageRec(null);
      	test.stopTest();
    }
    
    static testMethod void testExceptionForREGION_GROUP()
    {
    	createMappingSettings();
    	createBrokerStageRecords();
        createMAP_NATLTERRrecords();
        createMAP_Mrecords();
        create_MAP_CT01598Recs();
        
        test.startTest();
    	SLID_UpdateBrokerFieldsFromMI updateFromMIBatch4 = new SLID_UpdateBrokerFieldsFromMI();
    	updateFromMIBatch4.setBranchAndSubcoyBasedFieldsonBrokerStageRecs(null,null,null,null);
    	updateFromMIBatch4.setREGION_GROUPforPrairiesAndPacific(null,null);
      	test.stopTest();
    }
    
    
    private static void createBrokerStageRecords()
    {
    	List<Broker_Stage__c> brokerRecords = new List<Broker_Stage__c>();

        Broker_Stage__c childBroker = new Broker_Stage__c(
                                            Name = 'childBroker', 
                                            AGENT__c = 1,
                                            BROKER_NUMBER__c='1',
                                            PARENT1_BROKER_NUMBER__c='2',
                                            PARENT1_BROKER_SCRAMBLED__c = 2,
                                            PARENT2_BROKER_NUMBER__c='3',
                                            PARENT2_BROKER_SCRAMBLED__c = 3,
                                            PARENT3_BROKER_NUMBER__c='4',
                                            PARENT3_BROKER_SCRAMBLED__c = 4,
                                            NATLTERR__c = 'NAT1',
                                            ACTIVTO__c = 99999999,
                                            BRANCH__c = 8088,
                                            TEAM__c = '7088',
                                            SUBCOY__c = 4,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78',
                                            ADDRIND1__c = 'A',
                                            ADDRIND2__c = 'B'
                                        );
        brokerRecords.add(childBroker);

        Broker_Stage__c parent1Broker = new Broker_Stage__c(
                                            Name = 'parent1Broker', 
                                            AGENT__c = 2,
                                            BROKER_NUMBER__c='2',
                                            PARENT1_BROKER_NUMBER__c='3',
                                            PARENT1_BROKER_SCRAMBLED__c = 3,
                                            PARENT2_BROKER_NUMBER__c='4',
                                            PARENT2_BROKER_SCRAMBLED__c = 4,
                                            NATLTERR__c = 'NAT2',
                                            ACTIVTO__c = 99999999,
                                            BRANCH__c = 7088,
                                            TEAM__c = '8088',
                                            SUBCOY__c = 5,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78',
                                            ADDRIND1__c = 'A',
                                            ADDRIND2__c = 'B'
                                        );
        brokerRecords.add(parent1Broker);

        Broker_Stage__c parent2Broker = new Broker_Stage__c(
                                            Name = 'parent2Broker',
                                            AGENT__c = 3, 
                                            BROKER_NUMBER__c='3',
                                            PARENT1_BROKER_NUMBER__c='4',
                                            NATLTERR__c = 'NAT3',
                                            ACTIVTO__c = 20210610,
                                            BRANCH__c = 6801,
                                            TEAM__c = '8088',
                                            SUBCOY__c = 4,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78',
                                            ADDRIND1__c = 'A',
                                            ADDRIND2__c = 'B'
                                            //PARENT1_BROKER_SCRAMBLED__c = '4'
                                        );
        brokerRecords.add(parent2Broker);

        Broker_Stage__c parent3Broker = new Broker_Stage__c(
                                            Name = 'parent3Broker',
                                            AGENT__c = 4, 
                                            BROKER_NUMBER__c='4',
                                            NATLTERR__c = 'NAT4',
                                            ACTIVTO__c = 20210610,
                                            BRANCH__c = 6988,
                                            TEAM__c = '8088',
                                            SUBCOY__c = 4,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78',
                                            ADDRIND1__c = 'A',
                                            ADDRIND2__c = 'B'
                                        );
        brokerRecords.add(parent3Broker);
        
        Broker_Stage__c nonExistingBroker = new Broker_Stage__c(
                                            Name = 'nonExistingBroker',
                                            AGENT__c=5, 
                                            BROKER_NUMBER__c='5',
                                            PARENT1_BROKER_NUMBER__c='',
                                            //PARENT1_BROKER_SCRAMBLED__c='',
                                            PARENT2_BROKER_NUMBER__c='',
                                            //PARENT2_BROKER_SCRAMBLED__c='',
                                            PARENT3_BROKER_NUMBER__c='4',
                                            PARENT3_BROKER_SCRAMBLED__c=4,
                                            NATLTERR__c = 'NAT5',
                                            ACTIVTO__c = 20210610,
                                            BRANCH__c = 188,
                                            TEAM__c = '8088',
                                            SUBCOY__c = 6,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78',
                                            ADDRIND1__c = 'A',
                                            ADDRIND2__c = 'B'
                                        );
        
         brokerRecords.add(nonExistingBroker);
        
        Broker_Stage__c nonExistingBroker1 = new Broker_Stage__c(
                                            Name = 'nonExistingBroker', 
                                            AGENT__c=6,
                                            BROKER_NUMBER__c='6',
                                            PARENT1_BROKER_NUMBER__c='3',
                                            PARENT1_BROKER_SCRAMBLED__c=3,
                                            PARENT2_BROKER_NUMBER__c='',
                                            //PARENT2_BROKER_SCRAMBLED__c='',
                                            PARENT3_BROKER_NUMBER__c='',
                                            NATLTERR__c = 'NAT6',
                                            ACTIVTO__c = 20210610,
                                             BRANCH__c = 188,
                                            TEAM__c = '8088',
                                            SUBCOY__c = 6,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78',
                                            ADDRIND1__c = '',
                                            ADDRIND2__c = ''
                                            //PARENT3_BROKER_SCRAMBLED__c=''
                                        );
                                        
        brokerRecords.add(nonExistingBroker1);
        
        Broker_Stage__c nonExistingBroker2 = new Broker_Stage__c(
                                            Name = 'nonExistingBroker', 
                                            AGENT__c=7,
                                            BROKER_NUMBER__c='7',
                                            PARENT1_BROKER_NUMBER__c='',
                                            //PARENT1_BROKER_SCRAMBLED__c='',
                                            PARENT2_BROKER_NUMBER__c='2',
                                            PARENT2_BROKER_SCRAMBLED__c=2,
                                            PARENT3_BROKER_NUMBER__c='',
                                            NATLTERR__c = 'NAT1',
                                            ACTIVTO__c = 20210610,
                                             BRANCH__c = 188,
                                            TEAM__c = '8088',
                                            SUBCOY__c = 6,
                                            STREETNO__c = '12',
                                            STREET__c = '34',
                                            STREETSU__c = '56',
                                            STREETDIR__c = '78'
                                            //PARENT3_BROKER_SCRAMBLED__c=''
                                        );
        
        
        brokerRecords.add(nonExistingBroker2);

        //Insert list of new broker records
        insert(brokerRecords);
    	
    }
    
    private static void createMAP_NATLTERRrecords()
    {
    	List<MAP_NATLTERR__c> recsToInsert = new List<MAP_NATLTERR__c>();
    	
    	MAP_NATLTERR__c rec1 = new MAP_NATLTERR__c();
    	rec1.NATL_TERR_CD__c = 'NAT1';
    	rec1.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 1';
    	recsToInsert.add(rec1);
    	
    	MAP_NATLTERR__c rec2 = new MAP_NATLTERR__c();
    	rec2.NATL_TERR_CD__c = 'NAT2';
    	rec2.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 2';
    	recsToInsert.add(rec2);
    	
    	MAP_NATLTERR__c rec3 = new MAP_NATLTERR__c();
    	rec3.NATL_TERR_CD__c = 'NAT3';
    	rec3.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 3';
    	recsToInsert.add(rec3);
    	
    	MAP_NATLTERR__c rec4 = new MAP_NATLTERR__c();
    	rec4.NATL_TERR_CD__c = 'NAT4';
    	rec4.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 4';
    	recsToInsert.add(rec4);
    	
    	MAP_NATLTERR__c rec5 = new MAP_NATLTERR__c();
    	rec5.NATL_TERR_CD__c = 'NAT5';
    	rec5.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 5';
    	recsToInsert.add(rec5);
    	
    	MAP_NATLTERR__c rec6 = new MAP_NATLTERR__c();
    	rec6.NATL_TERR_CD__c = 'NAT6';
    	rec6.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 6';
    	recsToInsert.add(rec6);
    	
    	insert recsToInsert;
    }
    
    private static void createMAP_Mrecords()
    {
    	List<MAP_M__c> 	recsToInsert = new List<MAP_M__c>();
    	
    	MAP_M__c rec1 = new MAP_M__c();
    	rec1.SUBCOY__c = 4;
    	rec1.BRANCH__c = 6801;
    	rec1.REGION_GROUP__c = 'Ontario';
    	rec1.NEW_STRATEGIC_SEGMENT__c = 'PI';
    	rec1.NSS__c = 'PI';
    	rec1.BRAND__c = 'WA';
    	recsToInsert.add(rec1);
    	
    	MAP_M__c rec2 = new MAP_M__c();
    	rec2.SUBCOY__c = 4;
    	rec2.BRANCH__c = 6988;
    	rec2.REGION_GROUP__c = 'Ontario';
    	rec2.NEW_STRATEGIC_SEGMENT__c = 'PI';
    	rec2.NSS__c = 'PI';
    	rec2.BRAND__c = 'WA';
    	recsToInsert.add(rec2);
    	
    	MAP_M__c rec3 = new MAP_M__c();
    	rec3.SUBCOY__c = 5;
    	rec3.BRANCH__c = 3205;
    	rec3.REGION_GROUP__c = 'FA';
    	rec3.NEW_STRATEGIC_SEGMENT__c = 'FA';
    	rec3.NSS__c = 'FA';
    	rec3.BRAND__c = 'FA';
    	recsToInsert.add(rec3);
    	
    	MAP_M__c rec4 = new MAP_M__c();
    	rec4.SUBCOY__c = 6;
    	rec4.BRANCH__c = 188;
    	rec4.REGION_GROUP__c = 'Atlantic';
    	rec4.NEW_STRATEGIC_SEGMENT__c = 'MID MKT';
    	rec4.NSS__c = 'GSL';
    	rec4.BRAND__c = 'RSA';
    	recsToInsert.add(rec4);
    	
    	MAP_M__c rec5 = new MAP_M__c();
    	rec5.SUBCOY__c = 6;
    	rec5.BRANCH__c = 488;
    	rec5.REGION_GROUP__c = 'Atlantic';
    	rec5.NEW_STRATEGIC_SEGMENT__c = 'GSL';
    	rec5.NSS__c = 'GSL';
    	rec5.BRAND__c = 'RSA';
    	recsToInsert.add(rec5);
    	
    	MAP_M__c rec6 = new MAP_M__c();
    	rec6.SUBCOY__c = 6;
    	rec6.BRANCH__c = 1101;
    	rec6.REGION_GROUP__c = 'Quebec';
    	rec6.NEW_STRATEGIC_SEGMENT__c = 'PI';
    	rec6.NSS__c = 'PI';
    	rec6.BRAND__c = 'RSA';
    	recsToInsert.add(rec6);
    	
    	insert recsToInsert;
    	
    }
    
    private static void create_MAP_CT01598Recs()
    {
    	list<MAP_CT01598__c> recsToInsert = new List<MAP_CT01598__c>();
    	
    	MAP_CT01598__c rec1 = new MAP_CT01598__c();
    	rec1.CT01598_ARGUMENT__c = 'A';
    	rec1.CT01598_RESULT__c = 'Arg A';
    	recsToInsert.add(rec1);
    	
    	MAP_CT01598__c rec2 = new MAP_CT01598__c();
    	rec2.CT01598_ARGUMENT__c = 'B';
    	rec2.CT01598_RESULT__c = 'Arg B';
    	recsToInsert.add(rec2);
    	
    	insert recsToInsert;
    	
    }
    
    private static void createMappingSettings()
    {
        String adminEmail = 'someone@testorg.com';
         
        SLID_Broker_Mapping_Settings__c brokerMappingSetting = new SLID_Broker_Mapping_Settings__c();
        brokerMappingSetting.Batch_Job_Admin_Email__c = adminEmail;
        
        insert brokerMappingSetting;
        
    }
}