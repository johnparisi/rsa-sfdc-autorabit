public class SLID_ESB_MappingUtils
{
    //Map of relevant keys to values in the SLID_Mapping_KeyValue__c object
    private Static Map<String, String> keyValueMap = new Map<String, String>();

    //Instantiate the map based on a set of possible keys and a set of possible types
    public static void instantiateWithKeyType(List<String> keys, List<String> types)    
    {
        List<SLID_Mapping_KeyValue__c> keyValuePairs;
        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('instantiateWithKeyType');
            
            //Query based on the queryFilterValues initialized earlier. 
            keyValuePairs = [select Query_Filter_Value__c, Value__c from SLID_Mapping_KeyValue__c where Name IN :keys AND Type__c IN :types];
            for (SLID_Mapping_KeyValue__c keyValuePair : keyValuePairs)
            {
                //Set the key_type->value mapping in the keyValueMap
                SLID_ESB_MappingUtils.keyValueMap.put(keyValuePair.Query_Filter_Value__c, keyValuePair.Value__c);
            }
        }
        catch (Exception e)
        {
            //Create a custom exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'instantiateWithKeyType','getValue','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log); 
            throw e;
        }
    }

    //Instantiate the map based on the SLID_Mapping_KeyValue__c.Query_Filter_Value__c field (more selective)
    public static void instantiateWithFilterValues(List<String> queryFilterValues)  
    {
        for (String filterValue : queryFilterValues)
        {
            System.Debug('~~~queryFilterValues: ' + filterValue);
        }
        
        List<SLID_Mapping_KeyValue__c> keyValuePairs;
        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('instantiateWithFilterValues');
            
            //Query based on the queryFilterValues initialized earlier. 
            keyValuePairs = [select Query_Filter_Value__c, Value__c from SLID_Mapping_KeyValue__c where Query_Filter_Value__c IN :queryFilterValues];
            if(keyValuePairs.size() > 0)
            {
            for (SLID_Mapping_KeyValue__c keyValuePair : keyValuePairs)
            {
                //Set the key_type->value mapping in the keyValueMap
                SLID_ESB_MappingUtils.keyValueMap.put(keyValuePair.Query_Filter_Value__c, keyValuePair.Value__c);
            }
            }
            system.debug('keyValueMap -->'+keyValueMap);
        }   
        catch (Exception e)
        {
            //Create a custom exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'instantiateWithFilterValues','getValue','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log); 
            throw e;
        }
        System.Debug('~~~Pairs: ' + SLID_ESB_MappingUtils.keyValueMap);
    }

    public static String getMappedValue(String key, String type)
    {
        String mappedValue;
        if(type == null)
        {
            return key;
        }
        else if (key != null)
        {
            try
            {
                //Throw a exception if testing (to test catch statements)
                SLID_ESB_ServiceConstants.throwTestingException('getMappedValue');
            
                mappedValue = SLID_ESB_MappingUtils.keyValueMap.get(key + '_' + type);
            }
            catch (Exception e)
            {
                //Create a custom exception record
                UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_MappingUtils','getValue','',UTIL_Logging.DEBUG_LEVEL_ERROR);
                UTIL_Logging.logException(log); 
                throw e;
            }
        }
        
        return mappedValue;
    }


}