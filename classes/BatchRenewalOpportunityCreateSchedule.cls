/**
* @File Name    :   BatchRenewalOpportunityCreateSchedule
* @Description  :   Schedule Class for BatchRenewalOpportunityCreate
* @Date Created :   01/28/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Schedule
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       01/28/2017  Habiba Zaman  	Created the file/class
*/
global class BatchRenewalOpportunityCreateSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		BatchRenewalOpportunityCreate b = new BatchRenewalOpportunityCreate();
		database.executebatch(b);
	}
}