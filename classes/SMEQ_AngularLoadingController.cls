public class SMEQ_AngularLoadingController
{

    public String ngResourceName;
    public String ngResourceFileName;
    public String ngResourcePath;
    
  public void setNgResourcePath (String s) {
            ngResourcePath= s;
  }
    
  public String getNgResourcePath() {
    return ngResourcePath;
  } 
    
    
    
   public void setNgResourceFileName (String s) {
            ngResourceFileName = s;
  }
    
  public String getNgResourceFileName() {
    return ngResourceFileName;
  } 
    
    
      public void setNgResourceName (String s) {
            ngResourceName = s;
  }
    
  public String getNgResourceName() {
    return ngResourceName;
  } 
    public  String getMainJSPath()
    {
        return getResourceURL(ngResourceName,ngResourceFileName);

    }
    //Pass the resource name
    public  String getResourceURL(String resourceName,String searchName)
    {
        //Fetching the resource
        System.Debug('getResourceURL called');
        List<StaticResource> resourceList = [SELECT Name,Description FROM StaticResource WHERE Name = :resourceName];
         for (StaticResource sResouce : resourceList) {
            System.Debug(sResouce.Name +'-'+ sResouce.Description);
             String descr = sResouce.Description;
             String[] sfile = descr.split('\\|');
             for(String fName : sfile)
             {
                 System.Debug('fName='+fName);
                 if(fName.containsIgnoreCase(searchName))
                 {
                     String foundPath = ngResourcePath+resourceName+'/'+fName;
                     System.Debug('foundPath='+foundPath);
                     return foundPath;
                 }
             }
        }                           

            
            return '';
    }
}