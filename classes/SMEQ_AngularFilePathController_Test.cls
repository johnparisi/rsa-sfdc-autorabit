@isTest
public class SMEQ_AngularFilePathController_Test{
    
    public static testMethod void testAngularFilePathController() { 
        PageReference pageRef = Page.FPWL_SME_Broker;
        Test.setCurrentPage(pageRef);
       
        SMEQ_AngularFilePathController controller = new SMEQ_AngularFilePathController();
        String nextPage = controller.getMainJSPath();

        System.assertEquals('', nextPage);
        ApexPages.currentPage().getParameters().put('qp', 'yyyy');
       
        // Instantiate a new controller with all parameters in the page
        controller = new SMEQ_AngularFilePathController(); 
        controller.setNgResourceFileName('FPWL_SME_Broker');
        controller.setNgResourceName('resource');
        controller.setNgResourcePath('/acme');
        controller.getNgResourcePath();
        controller.getNgResourceFileName();
        controller.getNgResourceName();
        controller.getMainJSPath();
        System.assertEquals('', nextPage);
    }
    
    public static testMethod void testAngularResourcePath() {
        PageReference pageRef = Page.FPWL_SME_Broker;
        Test.setCurrentPage(pageRef);
        /*
        StaticResource staticR = new StaticResource(Name = 'TestR', Description = 'DescR');
        
        try {
            Insert staticR;
        } Catch (DmlException dmle) {
            System.debug(dmle.getMessage());
        }
        */
        List<StaticResource> resourceList = [SELECT Id, Name, Description FROM StaticResource WHERE Name = : 'AngularResource'];
        
        SMEQ_AngularFilePathController controller = new SMEQ_AngularFilePathController();
        //controller.getResourceURL(resourceList.get(0).Name, resourceList.get(0).Description);
        System.Assert(resourceList.size()>=0);
    }
}