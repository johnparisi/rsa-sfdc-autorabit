@isTest
public class XMLHelperTest
{
    private static String TEST_XML = '<A xmlns:rsa="http://www.rsagroup.ca/schema/custom/xml/">  <B>  <D attr="123">abc</D>  <C attr="abc">cef</C> <rsa:E>234</rsa:E> </B>   <B>   <D attr="456">def</D> <F>fff</F>   </B></A>';
    private static String SELF_CLOSING_TAG_TEST_XML = '<A><B><C /></B></A>';
    private static String CDATA_TEST_XML = '<A><B>' + XMLHelper.toCData('TEST') + '</B></A>';
    
    public testmethod static void testToCData()
    {
        String test = 'test';
        String result = XMLHelper.toCData(test);
        System.assertEquals('<![CDATA[test]]>', result);
    }
    
    public testmethod static void testEncodeAndDecode()
    {
        String test = 'testtesttesttest';
        String testEncode = XMLHelper.encodeUID(test);
        System.assertEquals('74657374-7465-7374-7465-737474657374', testEncode);
        
        String testDecode = XMLHelper.decodeUID(testEncode);
        System.assertEquals(test, testDecode);
    }
    
    public testmethod static void testRetrieveValue1()
    {
        String result = XMLHelper.retrieveValue(TEST_XML, '/A/B/D');
        System.assertEquals('abc', result);
    }
    
    public testmethod static void testRetrieveValue2()
    {
        String result = XMLHelper.retrieveValue(TEST_XML, '/A/B/E');
        System.assertEquals('234', result);
    }
    
    public testmethod static void testRetrieveValueSelfClosingTag()
    {
        String result = XMLHelper.retrieveValue(SELF_CLOSING_TAG_TEST_XML, '/A/B/C');
        System.assertEquals(null, result);
    }
    
    public testmethod static void testRetrieveCDataValue()
    {
        String result = XMLHelper.retrieveValue(CDATA_TEST_XML, '/A/B');
        System.assertEquals('TEST', result);
    }
    
    public testmethod static void testRetrieveValueInvalidPath1()
    {
        String result = XMLHelper.retrieveValue(TEST_XML, '/A/B/G');
        System.assertEquals(null, result);
    }
    
    public testmethod static void testRetrieveValueInvalidPath2()
    {
        String result = XMLHelper.retrieveValue(TEST_XML, '/Invalidpath');
        System.assertEquals(null, result);
    }
    
    public testmethod static void testExtractBlock()
    {
        String result = XMLHelper.extractBlock(TEST_XML, 'C');
        System.assertEquals('<C attr="abc">cef</C>', result);
    }
    
    public testmethod static void testExtractBlocks()
    {
        List<String> result = XMLHelper.extractBlocks(TEST_XML, 'D');
        System.assertEquals('<D attr="123">abc</D>', result[0]);
        System.assertEquals('<D attr="456">def</D>', result[1]);
    }
    
    public testmethod static void testGetAttributes()
    {
        List<String> result = XMLHelper.getAttributes(TEST_XML, 'D', 'attr');
        System.assertEquals('123', result[0]);
        System.assertEquals('456', result[1]);
    }
    
    public testmethod static void testRemoveTags()
    {
        String result = XMLHelper.removeTags(TEST_XML, 'D');
        System.assertEquals('<A xmlns:rsa="http://www.rsagroup.ca/schema/custom/xml/">  <B>    <C attr="abc">cef</C> <rsa:E>234</rsa:E> </B>   <B>    <F>fff</F>   </B></A>', result);
    }
    
    public testmethod static void testStripNamespace()
    {
        String result = XMLHelper.stripNamespace(TEST_XML, 'rsa');
        System.assertEquals('<A xmlns:rsa="http://www.rsagroup.ca/schema/custom/xml/">  <B>  <D attr="123">abc</D>  <C attr="abc">cef</C> <E>234</E> </B>   <B>   <D attr="456">def</D> <F>fff</F>   </B></A>', result);
    }
}