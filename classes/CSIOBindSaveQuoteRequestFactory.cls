/**
 * CSIOBindSaveQuoteRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this abstract class is to generate the xml Request for the BIND or SAVE service 
 */
public abstract class CSIOBindSaveQuoteRequestFactory extends CSIORequestDocumentFactory implements CSIORequestFactory {
    public Map<Id, List<Quote_Risk_Relationship__c>> qrrsByRisk = new Map<Id, List<Quote_Risk_Relationship__c>>();
    public Map<Id, List<Account>> accountBindPartiesByRisk = new Map<Id, List<Account>>();
    public Map<Id, List<Contact>> contactBindPartiesByRisk = new Map<Id, List<Contact>>();
    
    public CSIOBindSaveQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds.requestingUser.FederationIdentifier, sds.smeCase.New_Business_Source__c, sds.quote.Business_Source_ID__c, ri);
        rsaOperation = ri.getServiceType();
        this.soqlDataSet = sds; 
        this.ri = ri;
        rsaContextId = esbContext.get(this.soqlDataSet.smeCase.Offering_Project__c);
    }
    
    public void generateData(){
        /**
         * * The purpose of this loop is to group the QRR, account, and contact lists by risk.
        */
        for (Quote_Risk_Relationship__c qrr : soqlDataSet.quoteRiskRelationships)
        {
	        // Retrieve the QRR list for this risk.
	        List<Quote_Risk_Relationship__c> qrrs = qrrsByRisk.get(qrr.Related_Risk__c);
	        if (qrrs == null)
	        {
	            // No List, no problem.
	            qrrs = new List<Quote_Risk_Relationship__c>();
	        }
	        qrrs.add(qrr);
	        qrrsByRisk.put(qrr.Related_Risk__c, qrrs);
	        // This is a QRR with an account bind party.
	        if (qrr.Account__c != null)
	        {
	            List<Account> accs = accountBindPartiesByRisk.get(qrr.Related_Risk__c);
	            if (accs == null)
	            {
	                accs = new List<Account>();
	            }
	            accs.add(soqlDataSet.accountBindParties.get(qrr.Account__c));
	            accountBindPartiesByRisk.put(qrr.Related_Risk__c, accs);
	        }
	        // This is a QRR with a contact bind party.
	        else if (qrr.Contact__c != null)
	        {
	            List<Contact> cons = contactBindPartiesByRisk.get(qrr.Related_Risk__c);
	            if (cons == null)
	            {
	                cons = new List<Contact>();
	            }
	            cons.add(soqlDataSet.contactBindParties.get(qrr.Contact__c));
	            contactBindPartiesByRisk.put(qrr.Related_Risk__c, cons);
        	}
      	}
      	 //additional named insured
      	{
	        List<Quote_Risk_Relationship__c> quoteRiskRelationships = qrrsByRisk.get(null);
	        List<Account> accountsForLocation = accountBindPartiesByRisk.get(null);
	        List<Contact> contactsForLocation = contactBindPartiesByRisk.get(null);
	        this.insuredOrPrincipal = new CSIO_InsuredOrPrincipal(
            this.soqlDataSet.insured, 
            this.soqlDataSet.locationRisks[0], 
            this.soqlDataSet.quote,
            accountsForLocation == null ? new List<Account>() : accountsForLocation,
            contactsForLocation == null ? new List<Contact>() : contactsForLocation,
            quoteRiskRelationships == null ? new List<Quote_Risk_Relationship__c>() : quoteRiskRelationships);
        }

        If(this.ri.svcType != RequestInfo.ServiceType.BIND_QUOTE) {
                insuredOrPrincipal = new CSIO_InsuredOrPrincipal(soqlDataSet.insured, soqlDataSet.locationRisks[0], soqlDataSet.quote);
        }            
        // CommlPolicy
        {
            List<Quote_Risk_Relationship__c> quoteRiskRelationships = qrrsByRisk.get(null);
            List<Account> accountsForLocation = accountBindPartiesByRisk.get(null);
            List<Contact> contactsForLocation = contactBindPartiesByRisk.get(null);
            this.commlPolicy = new CSIO_CommlPolicy(
                soqlDataSet.ri,
                soqlDataSet.claims,
                soqlDataSet.quote,
                soqlDataSet.policyRisksByPackage.get(ri.pkgType),
                accountsForLocation == null ? new List<Account>() : accountsForLocation,
                contactsForLocation == null ? new List<Contact>() : contactsForLocation,
                quoteRiskRelationships == null ? new List<Quote_Risk_Relationship__c>() : quoteRiskRelationships
            );
        }
            
        // Location(s)
        this.locations = new List<CSIO_Location>();
        for (Risk__c risk : soqlDataSet.locationRisks)
        {
            List<Quote_Risk_Relationship__c> quoteRiskRelationships = qrrsByRisk.get(risk.Id);
            List<Account> accountsForLocation = accountBindPartiesByRisk.get(risk.Id);
            List<Contact> contactsForLocation = contactBindPartiesByRisk.get(risk.Id);
            this.locations.add(new CSIO_Location(
                soqlDataSet.insured,
                risk,
                accountsForLocation == null ? new List<Account>() : accountsForLocation,
                contactsForLocation == null ? new List<Contact>() : contactsForLocation,
                quoteRiskRelationships == null ? new List<Quote_Risk_Relationship__c>() : quoteRiskRelationships
            ));
        
        }
            
        // GeneralLiabilityLineBusiness
        {
        	 Id riskId = soqlDataSet.liabilityRiskByPackage.get(ri.pkgType).risk.Id;
            
            //Id riskId = soqlDataSet.liabilityRisk.id;
            
            /*soqlDataSet.Risk liabilityRisk = new soqlDataSet.Risk();
    		//rsk.packageId = packageId;
    		liabilityRisk.risk = soqlDataSet.liabilityRisk;
            liabilityRisk.coverages = soqlDataSet.liabilityRiskCoverages;*/
            List<Quote_Risk_Relationship__c> quoteRiskRelationships = qrrsByRisk.get(riskId);
            List<Account> accountsForLocation = accountBindPartiesByRisk.get(riskId);
            List<Contact> contactsForLocation = contactBindPartiesByRisk.get(riskId);
            this.generalLiabilityLineBusiness = new CSIO_GeneralLiabilityLineBusiness(
                soqlDataSet.liabilityRiskByPackage.get(ri.pkgType), 
                //liabilityRisk,
                soqlDataSet.quote,
                soqlDataSet.questionAnswerMap.get(soqlDataSet.quote.Id),
                accountsForLocation == null ? new List<Account>() : accountsForLocation, 
                contactsForLocation == null ? new List<Contact>() : contactsForLocation, 
                quoteRiskRelationships == null ? new List<Quote_Risk_Relationship__c>() : quoteRiskRelationships
            );
                
        }
            
            // CommlScheduleEstablishment
            this.commlScheduleEstablishment = new CSIO_CommlScheduleEstablishment(soqlDataSet.quote);
        	
        	producer = new CSIO_Producer(soqlDataSet.brokerage, soqlDataSet.producer, soqlDataSet.getAgencyId(), soqlDataSet.caseId);            
        	caseCreateDateLong = soqlDataSet.smeCase.CreatedDate.format(dateFormatShort);
        	caseCreateDateShort = soqlDataSet.smeCase.CreatedDate.format(dateFormatShort);
    }
    
    public String buildXMLRequest(){
        generateData();
        getHeaderXML();
        xml += CSIO128Header;
        xml += ' <RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
        if (this.ri.svcType == RequestInfo.ServiceType.BIND_QUOTE) {
        	xml += '<CommlNamedInsuredAndLocationScheduleSubmitRq>';
        	xml += ' <RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
            xml += ' <CurCd>CAD</CurCd>';
            xml += producer;
            //if (sds.quote.Policy_Payment_Plan_Type__c == CSIO_CommlScheduleEstablishment.BILLING_METHOD_MONTHLY_WITHDRAWAL_PLAN)
            if (soqlDataSet.quote.Policy_Payment_Plan_Type__c != null)
            {
                xml += commlScheduleEstablishment;
            }
            xml += insuredOrPrincipal;
            xml += '<Location><ItemIdInfo></ItemIdInfo><Addr></Addr></Location>'; // Required by CSIO but not processed by ESB.
            xml += '</CommlNamedInsuredAndLocationScheduleSubmitRq>';
        }
            
        
            xml += '   <CommlPkgPolicyQuoteInqRq>';
            xml += '    <RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
            xml += '    <TransactionRequestDt>' + caseCreateDateShort + '</TransactionRequestDt>'; //Fixed?
            // added for bind - policy date is selectable
            String policyEffectiveDate = todayDateShort;
            if (soqlDataSet.quote.Effective_Date__c != null)
            {
                Date d = soqlDataSet.quote.Effective_Date__c;
                Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
                policyEffectiveDate = dt.format(dateFormatShort);
            }
            xml += '    <TransactionEffectiveDt>' + policyEffectiveDate + '</TransactionEffectiveDt>'; //Fixed?           
            xml += '    <CurCd>CAD</CurCd>';
            xml += producer;
            xml += this.insuredOrPrincipal;
        if (this.ri.svcType == RequestInfo.ServiceType.SAVE_QUOTE) {
    			String editCommlPolicy = this.commlPolicy.toString();
    			editCommlPolicy = XMLHelper.removeTags(editCommlPolicy, 'SupplementaryNameInfo');
    			xml += editCommlPolicy;
    		} else {
    			xml += this.commlPolicy;
    		}
        // now the location based parties
        for (CSIO_Location location : this.locations)
        {
        		xml += location;
        }
        // parties linked to the CGL
        xml += this.generalLiabilityLineBusiness;
		xml += '   </CommlPkgPolicyQuoteInqRq>';
            xml += ' <rsa:ServiceOperationType>' + rsaOperation + '</rsa:ServiceOperationType>';
            xml += ' <rsa:Context>' + rsaContextId + '</rsa:Context>';
        if(this.ri.svcType == RequestInfo.ServiceType.SAVE_QUOTE || this.ri.svcType == RequestInfo.ServiceType.RABIND_QUOTE){
            xml += ' <rsa:SessionId>' + this.soqlDataSet.sessionId + '</rsa:SessionId>';
            
        }
        xml += CSIO128Footer;
        return xml;
    }
    
}