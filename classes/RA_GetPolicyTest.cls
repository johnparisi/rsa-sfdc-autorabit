/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RA_GetPolicyTest extends RA_AbstractTransactionTest {

	@testSetup static void testSetup() {
		RA_AbstractTransactionTest.setup();
	}
    
    /**
    Test the entire first XML document
    */
    
    static testMethod void testGetPolicy() {
		// find the case (shouldn't need to do this !)  
        Case parentCase = [SELECT Id, Com__c from Case][0];
        
        // process the XML policy document
        CSIOPolicyXMLProcessor csioXMLProcessor = new CSIOPolicyXMLProcessor(parentCase, RA_AbstractTransactionTest.xmlPolicy);
        csioXMLProcessor.process();
        
        // test that we can find the document in the database
        Quote__c policy = [SELECT Id, Air_Miles_Loyalty_Points_Standard__c, Total_Revenue__c, Standard_Premium__c, Claim_Free_Years__c, Case__c  from Quote__c WHERE ePolicy__c = 'COM810101133'][0];
        System.assert(policy !=null , 'Policy not found');
        System.assert(policy.Case__c == parentCase.Id, 'Case ID on policy ' +policy.Case__c + ' should be the same as the case ID on the parent case ' +  parentCase.Id);
                
        FP_AngularPageController.getPolicy(parentCase.Com__c, parentCase.Id);
              
    }
}