@isTest
private class SMEQ_ReferenceDataService_Test {
	
	static testMethod void testGetReferenceData() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/refrenceData';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
		generateResourceBundlesTestData();
		SMEQ_ReferenceDataModel referenceDataModel = SMEQ_ReferenceDataService.getReferenceData();
		System.assertEquals('EN', referenceDataModel.referenceDataListingEnglish.language);
		System.assertEquals('FR', referenceDataModel.referenceDataListingFrench.language);
		System.assertEquals(2, referenceDataModel.referenceDataListingFrench.referenceDataByKeyLists.size());
		for(SMEQ_ReferenceDataByKeyListsModel referenceDataKeyListModel:referenceDataModel.referenceDataListingFrench.referenceDataByKeyLists){
			System.debug('Referece Data Key: ' + referenceDataKeyListModel.referenceDataKey);
			for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.debug('Key: ' + keyValue.key);
					System.debug('Vlaue: ' + keyValue.value);
			}

			if(referenceDataKeyListModel.referenceDataKey == 'GENDER'){
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='M' || keyValue.key == 'F'));
				}
			}
			else{
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='Y' || keyValue.key == 'N'));
				}
			}
		}

		System.assertEquals(2, referenceDataModel.referenceDataListingEnglish.referenceDataByKeyLists.size());
		for(SMEQ_ReferenceDataByKeyListsModel referenceDataKeyListModel:referenceDataModel.referenceDataListingEnglish.referenceDataByKeyLists){
			if(referenceDataKeyListModel.referenceDataKey == 'GENDER'){
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='M' || keyValue.key == 'F'));
				}
			}
			else{
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='Y' || keyValue.key == 'N'));
				}
			}
		}

	}
	
	static public void generateResourceBundlesTestData(){
        List<SMEQ_REFERENCE_DATA__c> referenceDataList = new List<SMEQ_REFERENCE_DATA__c>();
        referenceDataList.add(buildReferenceDataObject('M','GENDER', 'EN','Male', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('F','GENDER', 'EN','Female', 2, 'www'));
        referenceDataList.add(buildReferenceDataObject('M','GENDER', 'FR','Male_FR', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('F','GENDER', 'FR','Female_FR', 2, 'www'));

        referenceDataList.add(buildReferenceDataObject('Y','YesNo', 'EN','Yes', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('N','YesNo', 'EN','No', 2, 'www'));
        referenceDataList.add(buildReferenceDataObject('Y','YesNo', 'FR','Yes_FR', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('N','YesNo', 'FR','No_FR', 2, 'www'));
        insert referenceDataList;
    }
    static public void generateExceptionResourceBundlesTestData(){
        List<SMEQ_REFERENCE_DATA__c> referenceDataList = new List<SMEQ_REFERENCE_DATA__c>();
        referenceDataList.add(buildReferenceDataObject('M','GENDER', 'EN','Male', 1, '0abc0'));
        referenceDataList.add(buildReferenceDataObject('F','GENDER', 'EN','Female', 2, '0abc0'));
        referenceDataList.add(buildReferenceDataObject('M','GENDER', 'FR','Male_FR', 1, '0abc0'));
        referenceDataList.add(buildReferenceDataObject('F','GENDER', 'FR','Female_FR', 2, '0abc0'));

        referenceDataList.add(buildReferenceDataObject('Y','YesNo', 'EN','Yes', 1, '0abc0'));
        referenceDataList.add(buildReferenceDataObject('N','YesNo', 'EN','No', 2, '0abc0'));
        referenceDataList.add(buildReferenceDataObject('Y','YesNo', 'FR','Yes_FR', 1, '0abc0'));
        referenceDataList.add(buildReferenceDataObject('N','YesNo', 'FR','No_FR', 2, '0abc0'));
        insert referenceDataList;
    }
    

    static SMEQ_REFERENCE_DATA__c buildReferenceDataObject(String code, String type, String language, String value, Integer displayOrder, String variant){
    	SMEQ_REFERENCE_DATA__c referenceDataObject = new SMEQ_REFERENCE_DATA__c();
    	referenceDataObject.code__c = code;
    	referenceDataObject.type__c = type;
    	referenceDataObject.language__c = language;
    	referenceDataObject.value__c = value;
    	referenceDataObject.display_order__c = displayOrder;
    	referenceDataObject.variant__c = variant;
    	return referenceDataObject;
    }
    
    static testMethod void testExceptionGetReferenceData() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/refrenceData';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;    
        generateExceptionResourceBundlesTestData();
        SMEQ_ReferenceDataModel referenceDataModel = SMEQ_ReferenceDataService.getReferenceData();

    }
    
}