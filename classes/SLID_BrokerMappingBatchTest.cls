/**
  * @author        Saumil Bapat
  * @date          10/31/2016
  * @description   Test class for PurgeLogs batch jobs
*/
@isTest
private class SLID_BrokerMappingBatchTest{


    static testmethod void testPurgeBrokerStageBatch() {

        //Initiate the list of old broker records which should be purged
        createNewBrokerMappingData();

        //Create custom settings
        createMappingSettings();

        List<Broker_Stage__c> brokerRecords = [Select Id from Broker_Stage__c];
        //Validate the broker records were created
        System.Assert(brokerRecords.size() == 7, 'Broker records not created');

        Test.StartTest();
        SLID_PurgeBrokerStage purgeStageBatch = new SLID_PurgeBrokerStage();
        Id BatchProcessId = Database.ExecuteBatch(purgeStageBatch);
        Test.StopTest();

        //Validate the broker records were created
        System.Assert([Select Id from Broker_Stage__c].size() == 0, 'Broker records were deleted');

    }

    static testmethod void testPurgeBrokerMappingBatch() {

        Map<String,String> expectedBrokerMapping = new Map<String,String>();
        createDataToTestPurgeBrokerMapping(expectedBrokerMapping);

        Test.StartTest();
        SLID_PurgeBrokerMapping purgeBatch = new SLID_PurgeBrokerMapping(true);
        Id BatchProcessId = Database.ExecuteBatch(purgeBatch);
        purgeBatch.NotificationEmailForErrors(2);
        Test.StopTest();

        List<SLID_Mapping_KeyValue__c> brokerKeyValueRecords = [Select Id ,Name , Value__c  from SLID_Mapping_KeyValue__c where Type__c = :SLID_CreateBrokerMapping.brokerTypeUnscrambled];

        for(SLID_Mapping_KeyValue__c keyValue : brokerKeyValueRecords)
        {
            system.assertEquals(keyValue.Value__c , expectedBrokerMapping.get(keyValue.Name));
        }
    }

    private static void createDataToTestPurgeBrokerMapping(Map<String,String> expectedBrokerMapping)
    {
        //Initiate the list of old broker records which should be purged
        createOldBrokerMappingData();

        //Create custom settings
        createMappingSettings();

        Map<String,String> keyValueMapping = createNewBrokerMappingData();

        List<Account> brokerAccounts = [Select Id, bkrAccount_HuonBrokerNumber__c from Account];
        //Validate the account records were created
        System.Assert(brokerAccounts.size() == 4, 'Account records not created');


        for (Account brokerAccount : brokerAccounts)
        {
            expectedBrokerMapping.put(
                    brokerAccount.bkrAccount_HuonBrokerNumber__c,
                    brokerAccount.Id
            );

            if(brokerAccount.bkrAccount_HuonBrokerNumber__c == '3')
            {
                expectedBrokerMapping.put(
                        '6',
                        brokerAccount.Id
                );
            }

            if(brokerAccount.bkrAccount_HuonBrokerNumber__c == '2')
            {
                expectedBrokerMapping.put(
                        '7',
                        brokerAccount.Id
                );
            }

            if(brokerAccount.bkrAccount_HuonBrokerNumber__c == '4')
            {
                expectedBrokerMapping.put(
                        '5',
                        brokerAccount.Id
                );
            }
        }
        List<Broker_Stage__c> brokerRecords = [Select Id from Broker_Stage__c];
        //Validate the broker records were created
        System.Assert(brokerRecords.size() == 7, 'Broker records not created');

    }

    private static Map<String,String> createNewBrokerMappingData()
    {
        list<Account> brokerAccounts = new List<Account>();

        //Create account referencing child broker
        Account childBrokerAccount = new Account(
                Name = 'childBrokerAccount',
                bkrAccount_HuonBrokerNumber__c = '1',
                AGENT__c = 1
        );
        brokerAccounts.add(childBrokerAccount);

        //Create account referencing child broker
        Account parent1BrokerAccount = new Account(
                Name = 'parent1BrokerAccount',
                bkrAccount_HuonBrokerNumber__c = '2',
                AGENT__c = 2
        );
        brokerAccounts.add(parent1BrokerAccount);

        //Create account referencing child broker
        Account parent2BrokerAccount = new Account(
                Name = 'parent2BrokerAccount',
                bkrAccount_HuonBrokerNumber__c = '3',
                AGENT__c = 3
        );
        brokerAccounts.add(parent2BrokerAccount);

        //Create account referencing child broker
        Account parent3BrokerAccount = new Account(
                Name = 'parent3BrokerAccount',
                bkrAccount_HuonBrokerNumber__c = '4',
                AGENT__c = 4
        );
        brokerAccounts.add(parent3BrokerAccount);

        //Insert list of broker accounts
        insert brokerAccounts;



        List<Broker_Stage__c> brokerRecords = new List<Broker_Stage__c>();

        Broker_Stage__c childBroker = new Broker_Stage__c(
                Name = 'childBroker',
                AGENT__c = 1,
                BROKER_NUMBER__c='1',
                PARENT1_BROKER_NUMBER__c='2',
                PARENT1_BROKER_SCRAMBLED__c = 2,
                PARENT2_BROKER_NUMBER__c='3',
                PARENT2_BROKER_SCRAMBLED__c = 3,
                PARENT3_BROKER_NUMBER__c='4',
                PARENT3_BROKER_SCRAMBLED__c = 4
        );
        brokerRecords.add(childBroker);

        Broker_Stage__c parent1Broker = new Broker_Stage__c(
                Name = 'parent1Broker',
                AGENT__c = 2,
                BROKER_NUMBER__c='2',
                PARENT1_BROKER_NUMBER__c='3',
                PARENT1_BROKER_SCRAMBLED__c = 3,
                PARENT2_BROKER_NUMBER__c='4',
                PARENT2_BROKER_SCRAMBLED__c = 4
        );
        brokerRecords.add(parent1Broker);

        Broker_Stage__c parent2Broker = new Broker_Stage__c(
                Name = 'parent2Broker',
                AGENT__c = 3,
                BROKER_NUMBER__c='3',
                PARENT1_BROKER_NUMBER__c='4'
                //PARENT1_BROKER_SCRAMBLED__c = '4'
        );
        brokerRecords.add(parent2Broker);

        Broker_Stage__c parent3Broker = new Broker_Stage__c(
                Name = 'parent3Broker',
                AGENT__c = 4,
                BROKER_NUMBER__c='4'
        );
        brokerRecords.add(parent3Broker);

        Broker_Stage__c nonExistingBroker = new Broker_Stage__c(
                Name = 'nonExistingBroker',
                AGENT__c=5,
                BROKER_NUMBER__c='5',
                PARENT1_BROKER_NUMBER__c='',
                //PARENT1_BROKER_SCRAMBLED__c='',
                PARENT2_BROKER_NUMBER__c='',
                //PARENT2_BROKER_SCRAMBLED__c='',
                PARENT3_BROKER_NUMBER__c='4',
                PARENT3_BROKER_SCRAMBLED__c=4
        );

        brokerRecords.add(nonExistingBroker);

        Broker_Stage__c nonExistingBroker1 = new Broker_Stage__c(
                Name = 'nonExistingBroker',
                AGENT__c=6,
                BROKER_NUMBER__c='6',
                PARENT1_BROKER_NUMBER__c='3',
                PARENT1_BROKER_SCRAMBLED__c=3,
                PARENT2_BROKER_NUMBER__c='',
                //PARENT2_BROKER_SCRAMBLED__c='',
                PARENT3_BROKER_NUMBER__c=''
                //PARENT3_BROKER_SCRAMBLED__c=''
        );

        brokerRecords.add(nonExistingBroker1);

        Broker_Stage__c nonExistingBroker2 = new Broker_Stage__c(
                Name = 'nonExistingBroker',
                AGENT__c=7,
                BROKER_NUMBER__c='7',
                PARENT1_BROKER_NUMBER__c='',
                //PARENT1_BROKER_SCRAMBLED__c='',
                PARENT2_BROKER_NUMBER__c='2',
                PARENT2_BROKER_SCRAMBLED__c=2,
                PARENT3_BROKER_NUMBER__c=''
                //PARENT3_BROKER_SCRAMBLED__c=''
        );


        brokerRecords.add(nonExistingBroker2);

        //Insert list of new broker records
        insert(brokerRecords);

        //Create expected mapping for the created broker and accounts
        Map<String,String> keyValueMap = new Map<String,String>();
        keyValueMap.put('1', childBrokerAccount.Id);
        keyValueMap.put('2', parent1BrokerAccount.Id);
        keyValueMap.put('3', parent2BrokerAccount.Id);
        keyValueMap.put('4', parent3BrokerAccount.Id);
        keyValueMap.put('5', parent3BrokerAccount.Id);

        return keyValueMap;
    }

    static testmethod void testExceptions_execute() {

        //Instantiate failure break point
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'execute';

        try
        {
            //Initiate the list of old broker records which should be purged
            createNewBrokerMappingData();

            //Create custom settings
            createMappingSettings();

            List<Broker_Stage__c> brokerRecords = [Select Id from Broker_Stage__c];
            //Validate the broker records were created
            System.Assert(brokerRecords.size() == 7, 'Broker records not created');

            Test.StartTest();
            SLID_PurgeBrokerStage purgeStageBatch = new SLID_PurgeBrokerStage();
            Id BatchProcessId = Database.ExecuteBatch(purgeStageBatch);
            Test.StopTest();

            //Validate the broker records were created
            System.Assert([Select Id from Broker_Stage__c].size() == 0, 'Broker records were deleted');
        }
        catch (Exception ex)
        {
            System.AssertEquals(ex.getMessage() , 'Test Exception');
        }

    }

    private static void createMappingSettings()
    {
        String adminEmail = 'someone@testorg.com';

        SLID_Broker_Mapping_Settings__c brokerMappingSetting = new SLID_Broker_Mapping_Settings__c();
        brokerMappingSetting.Batch_Job_Admin_Email__c = adminEmail;

        insert brokerMappingSetting;

    }

    //Create broker mapping data to be deleted by the purge batch
    private static void createOldBrokerMappingData()
    {
        List<SLID_Mapping_KeyValue__c> brokerKeyValueRecords = new List<SLID_Mapping_KeyValue__c>();
        for (Integer i = 1; i<=10; i++)
        {
            SLID_Mapping_KeyValue__c brokerKeyValueRecord = new SLID_Mapping_KeyValue__c(
                    Name='Test' + String.ValueOf(i),
                    Value__c='Test' + String.ValueOf(i),
                    Type__c=SLID_CreateBrokerMapping.brokerTypeValue,
                    Query_Filter_Value__c='Test' +  + String.ValueOf(i) + '_Test' + String.ValueOf(i)
            );
            brokerKeyValueRecords.add(brokerKeyValueRecord);
        }
        insert brokerKeyValueRecords;
    }

}