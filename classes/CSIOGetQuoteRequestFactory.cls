/**
 * CSIOGetQuoteRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to generate the xml Request for the GETQUOTE service 
 */
public class CSIOGetQuoteRequestFactory extends CSIOGetUploadFinalizeRequestFactory implements CSIORequestFactory {
    
    public CSIOGetQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds, ri);
        this.soqlDataSet = sds; 
        this.ri = ri;
    }
    
    public String buildXMLRequest(){
        getHeaderXml();
        xml += CSIO128Header;
        xml += super.buildStandardRequestDocument();
        xml += CSIO128Footer;
        return xml;
    }
}