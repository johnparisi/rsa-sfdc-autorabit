/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_Location.
 */
@isTest
public class CSIO_Location_Test
{
	@testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    static testMethod void testConvert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            Risk__c location = SMEQ_TestDataGenerator.getLocationRisk();

            CSIO_Location cl = new CSIO_Location(a, location);
            String output = '';
            output += cl;
            
            System.assertEquals(
                '<Location id="' + location.Id + '">' + 
                new CSIO_ItemIdInfo((String) a.Id) +
                new CSIO_Addr(location) +
                '<rsa:Deleted>' + (location.Deleted__c ? '1': '0') + '</rsa:Deleted>' +
                '</Location>',
                output
            );
        }
    }
}