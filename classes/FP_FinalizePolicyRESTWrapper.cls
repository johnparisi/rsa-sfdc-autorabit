/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/finalize/*')
global with sharing class FP_FinalizePolicyRESTWrapper {
    
	@HttpPost
    global static QuoteRequestWrapper finalizeTransaction(String caseId) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        QuoteRequestWrapper finalizeResponse = null;
		try {
			finalizeResponse = FP_AngularPageController.finalizePolicyREST(caseId);
		 	finalizeResponse.success = true;
		}
		catch (Exception e) {
            finalizeResponse = new QuoteRequestWrapper();
			finalizeResponse.success = false;
			finalizeResponse.errorMessages.add(e.getMessage());
			finalizeResponse.errorMessages.add(e.getStackTraceString());
		}
    	return finalizeResponse;
    }
}