global with sharing class SMEQ_AddressModel {
	public String addressLine1 {public get; public set;}
	public String addressLine2 {public get; public set;}
	public String city {public get; public set;}
	public String province {public get; public set;}
	public String postalCode {public get; public set;}
	public String country {public get; public set;}
}