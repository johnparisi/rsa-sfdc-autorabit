/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_Addr.
 */
@isTest
public class RSA_CalloutAuthentication_Test
{
    static testMethod void testEncodeDecode()
    {
        String context = 'context';
        String username = 'username';
        String password = 'password';
        String endpoint = 'endpoint';
        
        RSA_CalloutAuthentication rca = new RSA_CalloutAuthentication(context);
        rca.userName = username;
        rca.password = password;
        rca.endpoint = endpoint;
        rca.encode();
        
        String headerResult = RSA_CalloutAuthentication.getAuthorizationHeader(context);
        System.assertEquals('Basic ' + EncodingUtil.base64Encode(Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(rca.userName)) + ':' + EncodingUtil.base64Encode(Blob.valueOf(rca.password)))), headerResult);
        
        String endpointResult = RSA_CalloutAuthentication.getEndpoint(context);
        System.assertEquals(endpoint, endpointResult);
        
        rca.decode();
        System.assertEquals(username, rca.userName);
        System.assertEquals(password, rca.password);
        System.assertEquals(endpoint, rca.endPoint);
    }
}