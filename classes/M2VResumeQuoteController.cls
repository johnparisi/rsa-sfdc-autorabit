/**
* @author Gopinath Perumal CloudSynApps.Inc
* @date 08-Dec-2019
*
* @group Quote
* @group-content ../../ApexDocContent/Quote.htm
*
* @description Created as part of JIRA User Story # M2V-14 Save Create Quote at application step
*/
public class M2VResumeQuoteController 
{
    public String currentNetworkId = Network.getNetworkId();
	public String getResumeUrl()
    {
        String savedSessionId = ApexPages.currentPage().getParameters().get('Id');
        String resumeUrl = '';
        if(String.isNotBlank(currentNetworkId))
        {
            List<Network> currentNetworkLst = [SELECT Id, Name, Status, UrlPathPrefix 
                                      FROM Network 
                                      WHERE Id = :currentNetworkId
                                      LIMIT 1];
            List<vlocity_ins__OmniScriptInstance__c> savedSessionLst = [SELECT Id,vlocity_ins__ObjectId__c,vlocity_ins__RelativeResumeLink__c,vlocity_ins__ResumeLink__c
                                                                       FROM vlocity_ins__OmniScriptInstance__c
                                                                       WHERE vlocity_ins__Status__c = 'In Progress' 
                                                                       AND Id = :savedSessionId
                                                                       ORDER BY vlocity_ins__LastSaved__c DESC 
                                                                       LIMIT 1];
            if(!currentNetworkLst.isEmpty() && !savedSessionLst.isEmpty() && String.isNotBlank(currentNetworkLst.get(0).UrlPathPrefix))
            {
                resumeUrl = '/'+currentNetworkLst.get(0).UrlPathPrefix+savedSessionLst.get(0).vlocity_ins__RelativeResumeLink__c;
            }
            else
            {
                resumeUrl = savedSessionLst.get(0).vlocity_ins__RelativeResumeLink__c;
            }
        }
    	return resumeUrl;
    }
}