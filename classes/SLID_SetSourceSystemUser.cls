/**
  * @author        Saumil Bapat
  * @date          12/6/2016
  * @description   Helper class to Source System User on Sigma Processes
*/
public with sharing class SLID_SetSourceSystemUser {

  public static Map<String, User> userMap = new Map<String,User>(); 
  
  public static void SetSourceSystemUser(List<Case> newRecords)
  {
    System.Debug('~~~SLID_SetSourceSystemUser(List<Case>)');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_SetSourceSystemUser: ' + Limits.getQueries());
    //If the integration log id is not set, it is not running in a webservice context
    if (Util_Logging.integrationLogId == null)
    {
        System.Debug('~~~Integration Id is null');
        System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_SetSourceSystemUser: ' + Limits.getQueries());
        return;
    }
    List<String> sourceUserIds = new List<String>(); 
    for (Case record : newRecords)
    {
      if (record.Source_System_User_Id__c != null && userMap.get(record.Source_System_User_Id__c) == null)
      {
        sourceUserIds.add(record.Source_System_User_Id__c);
      }
    }
    if(sourceUserIds.size() > 0)
    {
      SLID_SetSourceSystemUser.queryUsers(sourceUserIds);
    }
    for(Case record : newRecords)
    {
      User u = SLID_SetSourceSystemUser.userMap.get(record.Source_System_User_Id__c);
      if (u != null) 
      {
        record.Source_System_User__c = u.Id;
      }
    }
  }

  public static void SetSourceSystemUser(List<Quote_Version__c> newRecords)
  {
    System.Debug('~~~SLID_SetSourceSystemUser(List<Quote_Version__c>)');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_SetSourceSystemUser: ' + Limits.getQueries());
    //If the integration log id is not set, it is not running in a webservice context
    if (Util_Logging.integrationLogId == null)
    {
        System.Debug('~~~Integration Id is null');
        System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_SetSourceSystemUser: ' + Limits.getQueries());
        return;
    }
    List<String> sourceUserIds = new List<String>(); 
    for (Quote_Version__c record : newRecords)
    {
      if (record.Source_System_User_Id__c!= null && userMap.get(record.Source_System_User_Id__c) == null)
      {
        sourceUserIds.add(record.Source_System_User_Id__c);
      }
    }
    if(sourceUserIds.size() > 0)
    {
      SLID_SetSourceSystemUser.queryUsers(sourceUserIds);
    }
    for(Quote_Version__c record : newRecords)
    {
      User u = SLID_SetSourceSystemUser.userMap.get(record.Source_System_User_Id__c);
      if (u != null) 
      {
        record.Source_System_User__c = u.Id;
      }
    }
  }
  
  public static void queryUsers(List<String> sourceUserIds)
  {
    System.Debug('~~~sourceUserIds: ' + sourceUserIds);
    try 
    {
      for ( User queriedUser :[Select Id, Ext_User__c from User where Ext_User__c IN  :sourceUserIds] )
      {
        System.Debug('~~~queriedUser: ' + queriedUser);
        SLID_SetSourceSystemUser.userMap.put(queriedUser.Ext_User__c, queriedUser);
      }
    }
    catch (Exception e)
    {
      UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_SetSourceSystemUser','queryUsers','',UTIL_Logging.DEBUG_LEVEL_ERROR);
      UTIL_Logging.logException(log);
      throw e;
    }
  }
}