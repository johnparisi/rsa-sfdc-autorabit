/**
* @File Name    :   ActuarialDetailDomain
* @Description  :   Actuarial Detail Domain Layer
* @Date Created :   12/12/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Domain
* @Modification Log:
**************************************************************************************
* Ver       Date        Author           Modification
* 1.0       12/12/2017  Habiba Zaman  Created the file/class
 * 
 * The object domain is where the real work happens, this is where we will apply the actual business logic. 
 * We have the main Object Domain which should handle all common processing. 
 * Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
 * Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
 * just implement logic in the appopriate method. 
 */
public with sharing class ActuarialDetailDomain {
    
    private ActuarialDetailService service = new ActuarialDetailService();
    
	public void beforeInsert(List<Actuarial_Detail__c> records){
	}

	public void beforeUpdate(List<Actuarial_Detail__c> records, Map<Id, Actuarial_Detail__c> oldRecords){
       			
	}

	public void afterInsert(List<Actuarial_Detail__c> records){
        service.updatePolicyRecordsWithId(records);
	}

	public void afterUpdate(List<Actuarial_Detail__c> records, Map<Id, Actuarial_Detail__c> oldRecords){
	}
	
}