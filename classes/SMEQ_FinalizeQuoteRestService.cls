@RestResource(urlMapping='/finalizeQuote/*')
global with sharing class SMEQ_FinalizeQuoteRestService {
    @HttpPost
    global static SMEQ_FinalizeQuoteResponseModel saveFinalizedQuote(SMEQ_FinalizeQuoteRequestModel finalizeQuoteRequest){
        RestResponse res = RestContext.response;
        String msgResponse = '';
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        SMEQ_FinalizeQuoteResponseModel response = new  SMEQ_FinalizeQuoteResponseModel();
        SMEQ_FinalizeQuoteDao finalizeQuoteDao = new SMEQ_FinalizeQuoteDao();
		try{            
            response = finalizeQuoteDao.finalizeQuote(finalizeQuoteRequest);
            response.finalizeDate = String.valueOf(Date.today());
            response.epolicyNumber = 'QPC0065185-01';
            response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_OK); 
        }
        catch(SMEQ_ServiceException se){
            response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>(); SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception'); errors.add(error);response.setErrors(errors);
        }
        return response;
    }
}