global with sharing class SMEQ_FinalizeQuoteRequestModel
{
    public String quoteID{public get; public set;}
    public String packageID{public get; public set;}
    public String language{public get; public set;}
    //public String lastVistedPageNum{public get; public set;}
}