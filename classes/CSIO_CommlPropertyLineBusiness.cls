public class CSIO_CommlPropertyLineBusiness
{
    private List<SOQLDataSet.Risk> risks;
    
    public String LOBCd;
    public PropertyInfo propertyInfo;
    
	public CSIO_CommlPropertyLineBusiness(List<SOQLDataSet.Risk> risks)
    {
        this.risks = risks;
        convert();
    }
    
    public void convert()
    {
		this.LOBCd = 'csio:1';
        
        this.propertyInfo = new PropertyInfo(this.risks);
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<CommlPropertyLineBusiness>';
        xml += '<LOBCd>' + this.LOBCd + '</LOBCd>';
        xml += this.propertyInfo;
        xml += '</CommlPropertyLineBusiness>';
        
        return xml;
    }
    
    public class PropertyInfo
    {
        public List<CSIO_CommlPropertyInfo> commlPropertyInfo;
        
        public PropertyInfo(List<SOQLDataSet.Risk> risks)
        {
            this.commlPropertyInfo = new List<CSIO_CommlPropertyInfo>();
            
            if (risks != null)
            {
                for (SOQLDataSet.Risk risk : risks)
                {
                    CSIO_CommlPropertyInfo property = new CSIO_CommlPropertyInfo(risk);
                    commlPropertyInfo.add(property);
                }
            }
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<PropertyInfo>';
            for (CSIO_CommlPropertyInfo propertyInfo : this.commlPropertyInfo)
            {
                xml += propertyInfo;
                CSIO_CommlCoverage.locationCoverageCodes = ''; // FP-6460
            }
            xml += '</PropertyInfo>';
            
            return xml;
        }
    }
}