public class SMEQ_AngularFilePathController {

  public String ngResourceName;
  public String ngResourceFileName;
  public String ngResourcePath;

  public void setNgResourcePath (String s) {
    ngResourcePath= s;
  }

  public String getNgResourcePath() {
    return ngResourcePath;
  } 

  public void setNgResourceFileName (String s) {
    ngResourceFileName = s;
  }

  public String getNgResourceFileName() {
  return ngResourceFileName;
  } 

  public void setNgResourceName (String s) {
    ngResourceName = s;
  }

  public String getNgResourceName() {
    return ngResourceName;
  } 

  public  String getMainJSPath() {
    return getResourceURL(ngResourceName,ngResourceFileName);
  }
  //Pass the resource name
  public  String getResourceURL(String resourceName,String searchName) {
    //Fetching the resource
    List<StaticResource> resourceList = [SELECT Name,Description FROM StaticResource WHERE Name = :resourceName];
    for (StaticResource sResouce : resourceList) {
      String descr = sResouce.Description;
      String[] sfile = descr.split('\\|');
      for(String fName : sfile) {
        if(fName.containsIgnoreCase(searchName)) {
          String foundPath = ngResourcePath+ '/'+fName;
          return foundPath;
        }
      }
    }
    return '';
  }
}