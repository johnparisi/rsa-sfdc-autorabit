// Controller for Visualforce Page: bkrMMGSL_StartingPoint
public with sharing class bkrMMGSL_StartingPointController {
    
    // Variables and list names for "Brokerage Snapshots" and "KAM Scorecards"
    public String soql                                  {get;set;}
    public List<bkrBrokerageSnapshot__c> snapshots      {get;set;}
    public List<bkrBrokerageSnapshot__c> scorecards     {get;set;}
    
    // Variables and list names for "Trading Desk"
    public String soqlCases                             {get;set;}
    public String soqlNBOpps                            {get;set;}
    public String soqlRenewalOpps                       {get;set;}
    public String soqlOpenTasks                         {get;set;}
    public String CurrentUserId                         {get;set;}
    public String NewBusiness                           {get;set;}  
    public String Renewal                               {get;set;}
    public List<case> openCaseLists                     {get;set;}
    public List<opportunity> openNBOpps                 {get;set;}
    public List<opportunity> openRenewalOpps            {get;set;}
    public List<task> openTasks                         {get;set;}
    public Boolean isForcastSetForUser                  {get;set;}
    
    // Variables and list names for the "Plan vs. Actual" Progress Graph
    SObject sumQuota;
    SObject sumQuotaYearly;
    SObject sumClosedOpps;
    SObject sumClosedOppsYearly;
    
    // Sort the "Brokerage Snapshot" and "Kam Scorecard" Lists
    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }
    public String sortField {
        get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
        set;
    }
    
    // Sort the "Trading Desk" Lists 
    public String sortTdDirOpps {
        get  { if (sortTdDirOpps == null) {  sortTdDirOpps = 'asc'; } return sortTdDirOpps;  }
        set;
    }
    
    public String sortTdFieldOpps {
        get  { if (sortTdFieldOpps == null) {sortTdFieldOpps = 'CloseDate'; } return sortTdFieldOpps;}
        set;
    }
    
    public String sortTdDirCases {
        get  { if (sortTdDirCases == null) {  sortTdDirCases = 'asc'; } return sortTdDirCases;  }
        set;
    }
    
    public String sortTdFieldCases {
        get  { if (sortTdFieldCases == null) {sortTdFieldCases = 'CreatedDate'; } return sortTdFieldCases;}
        set;
    }
    
    public String sortTaskDir {
        get  { if (sortTaskDir == null) {  sortTaskDir = 'asc'; } return sortTaskDir;  }
        set;
    }
    
    public String sortTaskField {
        get  { if (sortTaskField == null) {sortTaskField = 'ActivityDate'; } return sortTaskField;}
        set;
    }
    
    // Initializes this controller and runs all queries when the page is loaded
    public bkrMMGSL_StartingPointController() {
        // SOQL string for "Brokerage Snapshots" and "KAM Scorecards"
        soql = 'SELECT bkrControl_DataRetreived__c,bkrControl_ExternalId__c,bkrControl_KAM__c,bkrControl_Region__c,bkrMMGSL_CFY_NewBusinessPlanVariance__c,bkrMMGSL_CFY_NewBusinessPlan__c,bkrMMGSL_CYTD_EarnedPremium__c,bkrMMGSL_CYTD_IncurredLosses__c,bkrMMGSL_CYTD_NewPoliciesBound__c,bkrMMGSL_CYTD_NewPremiumBound__c,bkrMMGSL_CYTD_QuotesIssued__c,bkrMMGSL_CYTD_QuoteToBindRatio__c,bkrMMGSL_CYTD_SubmissionsReceived__c,bkrMMGSL_CYTD_SubmissionToQuoteRatio__c,bkrMMGSL_CYTD_UndevelopedLossRatio__c,bkrMMGSL_CYTD_WrittenPremiumGrowth__c,bkrMMGSL_CYTD_WrittenPremium__c,bkrMMGSL_PYTD_EarnedPremium__c,bkrMMGSL_PYTD_UndevelopedLossRatio__c,bkrMMGSL_PYTD_WrittenPremium__c,brkMMGSL_PYTD_IncurredLosses__c,Id,Name FROM bkrBrokerageSnapshot__c where name != null';
        // SOQL string for "Trading Desk"

        //TO-DO: update recordtype id instead of type.
        CurrentUserId = Userinfo.getUserId();
        NewBusiness = 'New Business';
        Renewal = 'Renewal';
        soqlCases = 'SELECT id,contact.name,account.name,bkrPriorityScore__c,CreatedDate,CaseNumber,IsClosed,OwnerId,Status,Subject,Type FROM Case WHERE IsClosed != TRUE AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        
        soqlNBOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,bkrProductLineEffectiveDate__c,account.name,bkrPriorityScore__c,'+ 
                    ' bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,OwnerId,StageName,'+
                    ' Type,NextStep FROM Opportunity WHERE  StageName != \'Closed Declined\' AND StageName != \'Closed Lost\' AND '+
                    ' StageName != \'Closed Won - Pending Processing\' AND StageName != \'Closed Won - Policy Issued\' AND Type '+
                    ' LIKE \''+String.escapeSingleQuotes(NewBusiness)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        
        soqlRenewalOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,bkrProductLineEffectiveDate__c,account.name,bkrPriorityScore__c,'+
                         ' bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,OwnerId,StageName,'+
                         ' Type,NextStep FROM Opportunity WHERE StageName != \'Closed Lost - Policy Transferred\' AND StageName != \'Closed Lapsed\' AND StageName != \'Closed Lost\' AND '+
                         ' StageName != \'Closed Won - Pending Processing\' AND StageName != \'Closed Won - Policy Issued\' AND '+
                         ' Type LIKE \''+String.escapeSingleQuotes(Renewal)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        
        soqlOpenTasks = 'SELECT ActivityDate,Id,IsClosed,OwnerId,Status,Subject,WhatId,What.Name,WhoId FROM Task WHERE IsClosed = false AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        // Query for "Brokerage Snapshots" and "KAM Scorecards"
        runQuery();
        runKamQuery();
        // Query for "Trading Desk"
        runQueryCases();
    }
    

    // Query to Display the User's New Business Quota
    public SObject getSumQuota(){
        CurrentUserId = Userinfo.getUserId();
        Date currentDate = Date.today();
        List<Id> periodIdList = new List<Id>();
        List<Period> periodList = [Select Id from Period where StartDate <=:currentDate and EndDate >= :currentDate and Type = 'Quarter'];
        if(periodList.size()>0){
            for(Period p : periodList){
                periodIdList.add(p.Id);
            }
        }

        sumQuota = [SELECT SUM(QuotaAmount) sumMyQuota FROM ForecastingQuota WHERE QuotaOwnerId = :CurrentUserId and PeriodId in :periodIdList ];   
        if(sumQuota.get('sumMyQuota') == null){
            isForcastSetForUser = false;
        }
        else{
            isForcastSetForUser = true;
        }
        return sumQuota;
    }

    public SObject getSumQuotaYearly(){
        CurrentUserId = Userinfo.getUserId();
        Integer currentYear = System.Today().year();
        List<Id> periodIdList = new List<Id>();
        List<Period> periodList = [Select Id from Period where CALENDAR_YEAR(EndDate) = :currentYear and Type = 'Quarter'];
        if(periodList.size()>0){
            for(Period p : periodList){
                periodIdList.add(p.Id);
            }
        }
        
        sumQuotaYearly = [SELECT SUM(QuotaAmount) sumMyQuotaYearly FROM ForecastingQuota WHERE QuotaOwnerId = :CurrentUserId and PeriodId in :periodIdList ];   
        return sumQuotaYearly;
    }
    
    // Query to Display the User's Closed Opportunities
    public SObject getSumClosedOpps(){
        CurrentUserId = Userinfo.getUserId();
        Date currentDate = Date.today();
        Integer currentYear = System.Today().year();
        sumClosedOpps = [SELECT SUM(ForecastAmount) sumMyClosedOpps FROM ForecastingItem WHERE ForecastCategoryName = 'Closed' AND OwnerId = :CurrentUserId and Period.StartDate <=:currentDate and Period.EndDate >= :currentDate];   
        return sumClosedOpps;
    }
    // Query to Display the User's Closed Opportunities Yearly
   
    public SObject getSumClosedOppsYearly(){
        CurrentUserId = Userinfo.getUserId();
        Integer currentYear = System.Today().year();
        sumClosedOppsYearly = [SELECT SUM(ForecastAmount) sumMyClosedOppsYearly FROM ForecastingItem WHERE ForecastCategoryName = 'Closed' AND OwnerId = :CurrentUserId and Period.Type = 'Quarter' and CALENDAR_YEAR(Period.EndDate) = :currentYear];   
        return sumClosedOppsYearly;
    }
    
    // Toggles the Sort Column for "KAM Scorecards"
    public void toggleSort() {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        runKamQuery();
    }

    // Toggles the Sort Column for "Trading Desk (Opportunities)" 
    public void toggleTdSortOpps() {
        sortTdDirOpps = sortTdDirOpps.equals('asc') ? 'desc' : 'asc';
        runQueryCases();
    }
    
    // Toggles the Sort Column for "Trading Desk (Cases)" 
    public void toggleTdSortCases() {
        sortTdDirCases = sortTdDirCases.equals('asc') ? 'desc' : 'asc';
        runQueryCases();
    }
    
    // Toggles the Sort Column for "Trading Desk (Tasks Only)" 
    public void toggleTaskSort() {
        sortTaskDir = sortTaskDir.equals('asc') ? 'desc' : 'asc';
        runQueryCases();
    }
    
    // Creates the "Brokerage Snapshot"
    public void runQuery() {    
        try {
            snapshots = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 1');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while loading the Brokerage Snapshot.'));
        }        
    }
    
    // Creates the "KAM Scorecard"
    public void runKamQuery() {    
        try {
            scorecards = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 50');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while loading the KAM Scorecard.'));
        }        
    }
    
    // Creates the "Trading Desk"
    public void runQueryCases() {    
        try {
            openCaseLists = Database.query(soqlCases + ' order by ' + sortTdFieldCases + ' ' + sortTdDirCases + ' limit 100');
            openNBOpps = Database.query(soqlNBOpps + ' order by ' + sortTdFieldOpps + ' ' + sortTdDirOpps + ' limit 100');
            openRenewalOpps = Database.query(soqlRenewalOpps + ' order by ' + sortTdFieldOpps + ' ' + sortTdDirOpps + ' limit 100');
            openTasks = Database.query(soqlOpenTasks + ' order by ' + sortTaskField + ' ' + sortTaskDir + ' limit 100');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while loading your Trading Desk.'));
        }        
    }
    
    // "Brokerage Snapshot"
    // Sets the variables, "accountName" and "region" which will perform the Javascript search "runSearch" on our Visualforce page
    public PageReference runSearch() {        
        String accountName = Apexpages.currentPage().getParameters().get('accountName');
        String region = Apexpages.currentPage().getParameters().get('region');
        soql = 'SELECT bkrControl_DataRetreived__c,bkrControl_ExternalId__c,bkrControl_KAM__c,bkrControl_Region__c,bkrMMGSL_CFY_NewBusinessPlanVariance__c,bkrMMGSL_CFY_NewBusinessPlan__c,bkrMMGSL_CYTD_EarnedPremium__c,bkrMMGSL_CYTD_IncurredLosses__c,bkrMMGSL_CYTD_NewPoliciesBound__c,bkrMMGSL_CYTD_NewPremiumBound__c,bkrMMGSL_CYTD_QuotesIssued__c,bkrMMGSL_CYTD_QuoteToBindRatio__c,bkrMMGSL_CYTD_SubmissionsReceived__c,bkrMMGSL_CYTD_SubmissionToQuoteRatio__c,bkrMMGSL_CYTD_UndevelopedLossRatio__c,bkrMMGSL_CYTD_WrittenPremiumGrowth__c,bkrMMGSL_CYTD_WrittenPremium__c,bkrMMGSL_PYTD_EarnedPremium__c,bkrMMGSL_PYTD_UndevelopedLossRatio__c,bkrMMGSL_PYTD_WrittenPremium__c,brkMMGSL_PYTD_IncurredLosses__c,Id,Name FROM bkrBrokerageSnapshot__c WHERE name != null';
        if (!accountName.equals(''))
            soql += ' and name LIKE \'%'+String.escapeSingleQuotes(accountName)+'%\'';
        if (!region.equals(''))
            soql += ' and bkrControl_Region__c includes (\''+region+'\')';
        else soql += ' and bkrControl_Region__c = null';
        // Runs the query again with the updated values entered by our user in the Visualforce page
        runQuery();
        return null;
    }
    
    // "KAM Scorecard"
    // Sets the variables "accountName" and "region" which will perform the Javascript search "runKamSearch" on our Visualforce page
    public PageReference runKamSearch() {        
        String kamName = Apexpages.currentPage().getParameters().get('kamName');
        soql = 'SELECT bkrControl_DataRetreived__c,bkrControl_ExternalId__c,bkrControl_KAM__c,bkrControl_Region__c,bkrMMGSL_CFY_NewBusinessPlanVariance__c,bkrMMGSL_CFY_NewBusinessPlan__c,bkrMMGSL_CYTD_EarnedPremium__c,bkrMMGSL_CYTD_IncurredLosses__c,bkrMMGSL_CYTD_NewPoliciesBound__c,bkrMMGSL_CYTD_NewPremiumBound__c,bkrMMGSL_CYTD_QuotesIssued__c,bkrMMGSL_CYTD_QuoteToBindRatio__c,bkrMMGSL_CYTD_SubmissionsReceived__c,bkrMMGSL_CYTD_SubmissionToQuoteRatio__c,bkrMMGSL_CYTD_UndevelopedLossRatio__c,bkrMMGSL_CYTD_WrittenPremiumGrowth__c,bkrMMGSL_CYTD_WrittenPremium__c,bkrMMGSL_PYTD_EarnedPremium__c,bkrMMGSL_PYTD_UndevelopedLossRatio__c,bkrMMGSL_PYTD_WrittenPremium__c,brkMMGSL_PYTD_IncurredLosses__c,Id,Name FROM bkrBrokerageSnapshot__c WHERE name !=null';
        if (!kamName.equals(''))
            soql += ' and bkrControl_KAM__c includes (\''+kamName+'\')';
        else soql += ' and bkrControl_KAM__c = null';
        // Runs the query again with the updated values entered by our user in the Visualforce page
        runKamQuery();
        return null;
    }
    
    // "Trading Desk"
    // Enables the trading desk to be refreshed by the user 
    public PageReference runTradingDeskSearch() {        
        CurrentUserId = Userinfo.getUserId();
        NewBusiness = 'New Business';
        Renewal = 'Renewal';
        soqlCases = 'SELECT id,contact.name,account.name,bkrPriorityScore__c,CreatedDate,CaseNumber,'+
                    ' IsClosed,OwnerId,Status,Subject,Type FROM Case WHERE IsClosed != TRUE '+ 
                    ' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        
        soqlNBOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,bkrProductLineEffectiveDate__c,account.name,' + 
                    ' bkrPriorityScore__c,bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,' + 
                    ' CloseDate,Name,OwnerId,StageName,Type FROM Opportunity '+
                    ' WHERE StageName != \'Closed Won - Pending Processing\' AND StageName != \'Closed Won - Policy Issued\' AND'+
                    ' StageName != \'Closed Declined\' AND StageName != \'Closed Lost\' AND Type '+
                    ' LIKE \''+String.escapeSingleQuotes(NewBusiness)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';

        soqlRenewalOpps = 'SELECT id,bkrOpportunity_NextStepDate__c,account.name,bkrPriorityScore__c,'+
                          ' bkrPriorityStatus_Colour__c,bkrClientAccount__c,bkrQuoteRequiredDate__c,CloseDate,Name,'+
                          ' OwnerId,StageName,Type FROM Opportunity WHERE StageName != \'Closed Lost - Policy Transferred\' AND StageName != \'Closed Won - Pending Processing\' AND StageName != \'Closed Won - Policy Issued\' AND '+ 
                          ' StageName != \'Closed Declined\' AND StageName != \'Closed Lost\' AND Type LIKE \''+String.escapeSingleQuotes(Renewal)+'\' AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        soqlOpenTasks = 'SELECT ActivityDate,Id,IsClosed,OwnerId,Status,Subject,WhatId,What.Name,WhoId FROM Task WHERE IsClosed = false AND OwnerId = \''+String.escapeSingleQuotes(CurrentUserId)+'\'';
        runQueryCases();
        return null;
    }    
    
    // "Brokerage Snapshot"
    // Creates a picklist from values in the "bkrBrokerageSnapshot__c.bkrControl_Region__c" field
    // Sets the name of our new variable to kamNames
    public List<String> regions {
        get {
            if (regions == null) {                
                regions = new List<String>();
                Schema.DescribeFieldResult field = bkrBrokerageSnapshot__c.bkrControl_Region__c.getDescribe();                
                for (Schema.PicklistEntry f : field.getPicklistValues())
                    regions.add(f.getLabel());               
            }
            return regions;          
        }
        set;
    }
    
    // "KAM Scorecard"
    // Creates a picklist from values in the "bkrBrokerageSnapshot__c.bkrControl_KAM__c" field
    // Sets the name of our new variable to "kamNames"
    public List<String> kamNames {
        get {
            if (kamNames == null) {                
                kamNames = new List<String>();
                Schema.DescribeFieldResult field = bkrBrokerageSnapshot__c.bkrControl_KAM__c.getDescribe();                
                for (Schema.PicklistEntry f : field.getPicklistValues())
                    kamNames.add(f.getLabel());               
            }
            return kamNames;          
        }
        set;
    }
}