/**
  * @author        Saumil Bapat
  * @date          10/25/2016
  * @description   Test class for SLID_ESB_Service
*/
@isTest
private Class SLID_ESB_Service_Test {
    static testMethod void testLogging() {

    }
    static testMethod void testReferralProcess()
    {
        System.debug('~~~Starting test 1: testReferralProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;
        //Execute the webservice call
        SLID_ESB_Service.ServiceResponse serviceResponse = SLID_ESB_Service.handleESBPayload(referralProcessData);
        
        //Validate the records were properly created
        validationRecordCreation();
    }
    static testMethod void testNoTargetFields()
    {
        System.debug('~~~Starting test 2: testNoTargetFields');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;

        //delete target fields and execute the webservice call to test for exceptions
        delete SLID_TestDataFactory.referralProcessTargetFields;
        SLID_ESB_Service.ServiceResponse serviceResponseNoTargetFields = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testNoTargetObjects()
    {
        System.debug('~~~Starting test 3: testNoTargetObjects');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;

        //delete target objects and execute the webservice call to test for exceptions
        delete SLID_TestDataFactory.referralProcessTargetObjects;
        SLID_ESB_Service.ServiceResponse serviceResponseNoTargetObjects = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testNoServiceProcess()
    {
        System.debug('~~~Starting test 4: testNoServiceProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;

        //change the source to a non-existing one and execute webservice call to test for exceptions
        referralProcessData.processName = 'Non-Existing Referral Process';
        SLID_ESB_Service.ServiceResponse serviceResponseNoServiceProcess = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testExceptions_instantiateDescribeMap()
    {
        System.debug('~~~Starting test 1: testReferralProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'instantiateDescribeMap';
        //Execute the webservice call
        SLID_ESB_Service.ServiceResponse serviceResponse = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testExceptions_setFieldValue()
    {
        System.debug('~~~Starting test 1: testReferralProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'setFieldValue';
        //Execute the webservice call
        SLID_ESB_Service.ServiceResponse serviceResponse = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testExceptions_castFieldValue()
    {
        System.debug('~~~Starting test 1: testReferralProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'castFieldValue';
        //Execute the webservice call
        SLID_ESB_Service.ServiceResponse serviceResponse = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testExceptions_returnNewSObjectInstance()
    {
        System.debug('~~~Starting test 1: testReferralProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'returnNewSObjectInstance';
        //Execute the webservice call
        SLID_ESB_Service.ServiceResponse serviceResponse = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    static testMethod void testExceptions_upsertGenericSObjectList()
    {
        System.debug('~~~Starting test 1: testReferralProcess');
        //Setup for successful referral process test
        Exception_Logging__c exceptionLoggingSettings = SLID_TestDataFactory.exceptionLoggingSettings;
        SLID_Mapping_ServiceProcess__c referralProcess = SLID_TestDataFactory.referralProcess;
        SLID_ESB_Service.CallData referralProcessData = SLID_TestDataFactory.referralProcessData;
        SLID_ESB_ServiceConstants.throwException = true;
        SLID_ESB_ServiceConstants.breakPoint = 'upsertGenericSObjectList';
        //Execute the webservice call
        SLID_ESB_Service.ServiceResponse serviceResponse = SLID_ESB_Service.handleESBPayload(referralProcessData);
    }
    private static void validationRecordCreation()
    {
        for (Case c : [Select Id, CaseNumber, Case_Number_Ext_Id__c, parent.CaseNumber, Status, bkrCase_Policy__c, bkrCase_Subm_Stat_Rsn__c from Case])
        {
            System.Debug('~~~c: ' + c);
        }
        Case referralParentCase1 = [select Id, Status, bkrCase_Policy__c, bkrCase_Subm_Stat_Rsn__c from Case where bkrCase_Policy__c = 'COM80001234'];
        System.Assert(referralParentCase1.Status == 'Referred', 'referralParentCase1 not created');
    }
}