/**
 * Created by t903018 on 6/14/2016.
 */

/**
*Domain obect for SIC data - matches structure used by clients
 */
global with sharing class SMEQ_SICDataModel {

    public String sicCode;

    public String shortDescription;
    public String descriptionOperations;

    public String shortDescription_EN;
    public String shortDescription_FR;
    public String descriptionOperations_EN;
    public String descriptionOperations_FR;
    public String shortDescriptionEN;
    public String shortDescriptionFR;
    public String descriptionOperationsEN;
    public String descriptionOperationsFR;
    public String referral;
    public Boolean buildingRequired;
    public Boolean hasAppetite;
    public Boolean isComplexSIC;
    public Boolean isNormalSIC;
    public String category;
    public String category_FR;
    public String categoryFR;
    public String subCategory;
    public String subCategory_FR;
    public String subCategoryFR;
    public String coverageDefaults;
    public String coverageAddOns;
    public String disabledPolicyActions;
    
    public List<Question> questions = new List<Question>();
    public List<TermsAndConditions> termsAndConditions;


    public class Question {
        public Integer questionId{get;set;}

        public String questionText;

        public String questionText_EN;
        public String questionText_FR;
        public String questionTextEN;
        public String questionTextFR;
        public Boolean liveChatRequired;
        public String answer = 'No';
        public List<AffirmativeCondition> affirmativeConditions;
    }

    public class AffirmativeCondition {
        public Integer affConditionId;
        public String affirmativeText;

        public String affirmativeText_EN;
        public String affirmativeText_FR;
        public String affirmativeTextEN;
        public String affirmativeTextFR;
    }

    public class TermsAndConditions {
        public Integer tcId;
        public String conditionText;

        public String conditionText_EN;
        public String conditionText_FR;
        public String conditionTextEN;
        public String conditionTextFR;
    }
}