@isTest
public class RA_SaveUpdatedPolicyDevRestWrapper_Test{

    static testmethod void mytest23(){
		opportunity op = new opportunity();
		op.Opportunity_Renewal_Status__c = 'Renewed Liability Lapsed';

		op.bkrProductLineEffectiveDate__c = system.today();
		op.stagename = 'Cold Lead';
		op.closedate = system.today();
		op.name = 'testop';

		insert op;

		Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();
		Account a = SMEQ_TestDataGenerator.getBrokerageAccount();
		Contact con = SMEQ_TestDataGenerator.getProducerContact(a);

		case c = new case();
		c.status ='Open';
		c.origin = 'Phone';
		c.bkrCase_Opportunity__c = op.id;
		 c.accountid = a.id;
		c.ContactId = con.Id;
		insert c;
		string caseid= [Select id,caseNumber from case where id =:c.Id].CaseNumber;
		Session_Information__c session = new Session_Information__c(session_id__c = '123',Case__c = c.id, User__c = UserInfo.getUserId());
		insert session;

		string effdate = string.valueof(op.bkrProductLineEffectiveDate__c);
		string reneew = op.Opportunity_Renewal_Status__c;

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response= res;
		try{
			RA_SaveUpdatedPolicyDevRestWrapper.saveUpdatedPolicy(caseid,effdate,reneew);
		}catch(exception ex){
			system.debug('Exception -->'+ex);
		}
    }
}