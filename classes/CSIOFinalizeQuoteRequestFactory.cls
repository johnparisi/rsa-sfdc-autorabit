/**
 * CSIOFinalizeQuoteRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to generate the xml Request for the FINALIZE service 
 */
public class CSIOFinalizeQuoteRequestFactory extends CSIOGetUploadFinalizeRequestFactory implements CSIORequestFactory {
    
    public CSIOFinalizeQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds, ri);
        this.soqlDataSet = sds; 
        this.ri = ri;
    }
    
    public String buildXMLRequest(){
        getHeaderXml();
        xml += CSIO128Header;
        xml += super.buildStandardRequestDocument();
        xml += CSIO128Footer;
        return xml;
    }
}