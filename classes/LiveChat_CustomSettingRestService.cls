@RestResource(urlMapping='/searchLiveChatSettings/*')
global with sharing class LiveChat_CustomSettingRestService
{
    private static final String LIVE_CHAT_FIELD_TYPE = 'livechat';
    
    @HttpGet
    global static SMEQ_CustomSettingResponseModel searchCustomSettingsByType() {        
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        SMEQ_CustomSettingResponseModel cSettingResponseModel = new SMEQ_CustomSettingResponseModel();
        LiveChat_CustomSettingUtility cSettingUtil = New LiveChat_CustomSettingUtility();
        
        String settingFieldType = 'livechat';  
        String settingVariant = '';      
        String settingBundle = '';
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');        

        if(req.params.get('variant')!=null){
            settingVariant= EncodingUtil.urlDecode(req.params.get('variant'),'UTF-8');
        }        

        if(req.params.get('bundle')!=null){
            settingBundle= EncodingUtil.urlDecode(req.params.get('bundle'),'UTF-8');
        }

system.debug(settingFieldType + '|' + settingVariant + '|' + settingBundle);

        //search by both variant and and field type and bundle
        if(settingFieldType!='' && settingVariant!='' && settingBundle!='')
        {
            try{
                
                cSettingResponseModel = cSettingUtil.getCustomSettingsForLiveChat(settingFieldType,settingVariant,settingBundle);
            }
            catch(Exception se){
system.debug('error:' + SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR );            
                cSettingResponseModel.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR); 
                List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
                SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');errors.add(error);cSettingResponseModel.setErrors(errors);
            }
        }

        return cSettingResponseModel;
    }

}