/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RA_FinalizeIssueTest extends RA_AbstractTransactionTest {

	@testSetup static void testSetup() {
		RA_AbstractTransactionTest.setup();
	}
    
    /**
    Test the entire first XML document
    */
    
    static testMethod void testLoad() {
		// find the case (shouldn't need to do this !)  
        Case parentCase = [
            SELECT Id, Com__c, bkrCase_Subm_Type__c
            FROM Case
            LIMIT 1
        ];
        
        // process the XML policy document
        CSIOPolicyXMLProcessor csioXMLProcessor = new CSIOPolicyXMLProcessor(parentCase, RA_AbstractTransactionTest.xmlPolicy);
        csioXMLProcessor.process();
        
        // test that we can find the document in the database
        Quote__c policy = [
            SELECT Id, Renewal_Reason_s__c, ePolicy__c, Air_Miles_Loyalty_Points_Standard__c, 
            Total_Revenue__c, Standard_Premium__c, Claim_Free_Years__c, Case__c
            FROM Quote__c
            WHERE ePolicy__c = 'COM810101133'
            LIMIT 1
        ];
        
        parentCase.COM__c = policy.ePolicy__c;
        policy.Renewal_Reason_s__c = 'Flowthrough';
        update parentCase;
        update policy;
        
        System.assertNotEquals(null, policy, 'Policy not found');
        System.assertEquals(parentCase.Id, policy.Case__c, 'Case ID on policy should be the same as the case ID on the parent case');
        System.assertNotEquals(null, policy.ePolicy__c, 'Comnumber not set on policy');
        System.assertEquals(parentCase.COM__c, policy.ePolicy__c, 'Policy and Case policy numbers do not match');
        
        ApexPages.Standardcontroller standardQuoteController = new ApexPages.Standardcontroller(policy);
        SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(standardQuoteController);
        
        smeQuoteController.IssueQuote();
    }
}