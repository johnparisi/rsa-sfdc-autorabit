/**
 * CSIORequestDocumentFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to store all common header and footer XML to be used in the service 
 * specific factory classes for the services that use CSIO version 1_28
 */

public abstract class CSIORequestDocumentFactory extends CSIORequestAbstractFactory {
    protected String CSIO128Footer = ''; 
    protected String CSIO128Header = '';
    protected String language;
    protected String quoteSource;
    protected String businessSource;
    protected String federationId;
    protected Map<String, String> transactionType;
    protected CSIO_Producer producer;
    protected CSIO_InsuredOrPrincipal insuredOrPrincipal;
    protected CSIO_CommlPolicy commlPolicy;
    protected List<CSIO_Location> locations;
    protected List<CSIO_CommlSubLocation> commlSubLocations;
    protected CSIO_CommlPropertyLineBusiness commlProperyLineBusiness;
    protected CSIO_GeneralLiabilityLineBusiness generalLiabilityLineBusiness;
    protected CSIO_CommlScheduleEstablishment commlScheduleEstablishment;
    protected CSIORequestDocumentFactory(String federationId, String existingBusinessSource, String businessSource, RequestInfo ri)
    {
        //The policy will have the same business source value for its entire duration, the existingBusinessSource will be pulled from ePolicy
        //and populated in the subsequent requests accordingly 
        this.businessSource = existingBusinessSource != null ? existingBusinessSource : businessSource;
        this.quoteSource = businessSource;
        language = UserService.getFormattedUserLanguage();
        //generating a uniqueId to be sent to ESB for each  request
        this.uniqueId = EncodingUtil.convertToHex(Crypto.GenerateAESKey(128));
        this.federationId = federationId;
        this.ri = ri;
        
        transactionType = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('bkrCase_Subm_Type__c');
    }
    
    
    protected Void getHeaderXML(){
        //generate the Header 
            CSIO128Header += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
            CSIO128Header += '<soapenv:Header />';
            CSIO128Header += '<soapenv:Body>';
            CSIO128Header += '<op>';
            CSIO128Header += ' <ACORD xsi:schemaLocation="http://www.ACORD.org/standards/PC_Surety/ACORD1/xml/ ./schemas/ACORD-ca-v1-28-0-yescode.xsd" xmlns="http://www.ACORD.org/standards/PC_Surety/ACORD1/xml/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:csio="http://www.CSIO.org/standards/PC_Surety/CSIO1/xml/" xmlns:rsa="http://www.rsagroup.ca/schema/custom/xml/">';
            CSIO128Header += '  <SignonRq>';
            CSIO128Header += '   <SignonTransport>';
            CSIO128Header += '    <CustId>';
            CSIO128Header += '     <SPName>' + SERVICE_PROVIDER_NAME + '</SPName>';
            CSIO128Header += '     <CustPermId>' + this.federationId + '</CustPermId>';
            CSIO128Header += '    </CustId>';
            CSIO128Header += '   </SignonTransport>';
            CSIO128Header += '   <ClientDt>' + todayDateLong + '</ClientDt>'; //Fixed? Mandatory field in CSIO, not required by ESB.
            CSIO128Header += '    <CustLangPref>' + language + '</CustLangPref>';      
            CSIO128Header += '   <ClientApp>';
            CSIO128Header += '    <Org>' + UserInfo.getOrganizationId() + '</Org>';
            CSIO128Header += '    <Name>' + this.businessSource + '</Name>';
            CSIO128Header += '    <Version>1</Version>';
            if(this.quoteSource != null && this.ri.svcType != RequestInfo.ServiceType.LOAD_POLICY && this.ri.svcType != RequestInfo.ServiceType.LOAD_POLICY_X ) {
                CSIO128Header += '    <rsa:QuoteSource>' + this.quoteSource + '</rsa:QuoteSource>';
            }
            CSIO128Header += '   </ClientApp>';
            CSIO128Header += '  </SignonRq>';
            CSIO128Header += '  <InsuranceSvcRq>';
        
          // common to all non-download quote types
          
            CSIO128Footer += '</InsuranceSvcRq>';
            CSIO128Footer += ' </ACORD>';
            CSIO128Footer += '</op>';
            CSIO128Footer += '</soapenv:Body>';
            CSIO128Footer += '</soapenv:Envelope>';
      
    }
}