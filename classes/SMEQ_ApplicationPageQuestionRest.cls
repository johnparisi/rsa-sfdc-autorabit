/**
* SMEQ_ApplicationPageQuestions Rest Class
* Author: Yasmin Shash
* Date: January 12 2013
* Feature: FP-754
* The purpose of this rest class is return a list of dynamic questions associated with a sicCode. 
* 
**/

@RestResource(urlMapping='/getDynamicSicQuestions/*')
global with sharing class SMEQ_ApplicationPageQuestionRest {
    
   
    @HttpGet
    global static SMEQ_ApplicationQuestionResponseModel getApplicationQuestionBySic() {        
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        String sicCode = '';

        SMEQ_ApplicationQuestionResponseModel appQueResponseModel = new SMEQ_ApplicationQuestionResponseModel();
        List<SIC_Code_Detail_Version__c> sicDetailVersionList = null;
        List<SMEQ_ApplicationQuestionWrapper> applicationQuestionsList = new List<SMEQ_ApplicationQuestionWrapper>();
        
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');

        if(req.params.get('sicCode')!=null)

        {
            sicCode = EncodingUtil.urlDecode(req.params.get('sicCode'),'UTF-8');
            
            ID sicCodeID;
            
            List<SIC_Code__c> sicCodeList = [
                SELECT ID
                FROM SIC_Code__c
                WHERE SIC_Code__c = :sicCode
                LIMIT 1
            ];
           
            if (!sicCodeList.isEmpty())
            {
                 
                 //appQueResponseModel.payload = generateDummyApplicationResponse();
                sicCodeID = sicCodeList[0].id;
                sicDetailVersionList = SMEQ_SicCodeService.getSicDetailsBySicProjectRegion(
                        Utils.commaDelimit(UserService.getOffering(UserInfo.getUserId())),
                        Utils.commaDelimit(UserService.getRegion(UserInfo.getUserId())),
                    	sicCodeID
                    );

                if(!sicDetailVersionList.isEmpty())
                {    
                    List<SIC_Question__c> applicationPageQuestions = SicQuestionService.getSicQuestions(sicDetailVersionList[0]);
                	appQueResponseModel.payload = wrapApplicationQuestions(applicationPageQuestions);
                }
                
            }
        }

       //sic code parameter has not been specified
        else
        {
             appQueResponseModel.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR); 
             List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
             SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(
                                                            SMEQ_RESTResponseModel.ERROR_CODE_699, 
                                                            'Undefined sic code parameter: Unexpected System Exception');errors.add(error);
                                                            appQueResponseModel.setErrors(errors);
        }
                
        return appQueResponseModel;
    }

    global static List<SMEQ_ApplicationQuestionWrapper> wrapApplicationQuestions(List<SIC_Question__c> sicQuestions)
    {
        List<SMEQ_ApplicationQuestionWrapper> sicQuestionWrapperRecords = new List<SMEQ_ApplicationQuestionWrapper>();
        Map<String,Id> SICQuestionTypes = Utils.GetRecordTypeIdsByDeveloperName(SIC_Question__c.SObjectType, true);
        Map<String, Map<String,String>> validationMessageMap = new  Map<String, Map<String,String>>();
        Set<ID> queIDS = new Set<ID>();

        Map<String,String> errorMessagesLookup =  new Map<String,String>();
        errorMessagesLookup.put('required', 'quote.application.location.equipmentValue.required.error.label');
        errorMessagesLookup.put('InvalidAmount', 'quote.application.location.equipmentValue.amount.error.label');
        errorMessagesLookup.put('InvalidMinAmount', 'quote.application.equipmentValue.minAmount.error.label');


        //create set of questionIDs from List
        for(SIC_Question__c q:sicQuestions)
        {
            queIDS.add(q.Id);
        }
        //get validation message maps available for questions
        validationMessageMap = SicQuestionService.getValidationMessagesMapForBrokerView(queIDS);

        for(SIC_Question__c question: sicQuestions)
        {
             List<String> dropDownValuesEng = new List<String>();
             List<String> dropDownValuesFr = new List<String>();
        
            if (question.RecordTypeId == SICQuestionTypes.get(SicQuestionService.RECORDTYPE_APPLICATION))
            {
                SMEQ_ApplicationQuestionWrapper questionWrapper = new SMEQ_ApplicationQuestionWrapper();
    			questionWrapper.questionText = question.Question_Text__c;
    			questionWrapper.questionCode = String.valueOf(question.questionId__c);
    			questionWrapper.category = question.Category__c;
    			questionWrapper.subCategory = String.valueOf(question.Sub_Category__c);
    			questionWrapper.frontEndVisibility = question.FrontEndVisibility__c;
    			questionWrapper.resourceBundleLabelKey = question.Resource_Bundle_Label__r.string_id__c;
    			questionWrapper.resourceBundlePlaceHolderKey = question.Resource_Bundle_Place_Holder_Key__r.string_id__c;
    			questionWrapper.requiredField = question.IsRequired__c;
                questionWrapper.minimumValidationAmount = question.Minimum_Validation_Amount__c;
                questionWrapper.minimumValidationRequired = question.Minimum_Validation_Required__c;
                questionWrapper.errorMessagesLookup = validationMessageMap.get(question.Id);
                questionWrapper.inputType = question.Input_Type__c.deleteWhiteSpace(); //remove white spaces to prevent any formatting errors on front end

                /*add mock map values for currecnty
                if(questionWrapper.inputType == 'Currency' && questionWrapper.questionCode == '90001')
                {
                    questionWrapper.errorMessagesLookup =  errorMessagesLookup;
                    questionWrapper.minimumValidationRequired = true;
                    questionWrapper.minimumValidationAmount = 100;
                }

                 //add mock map values for currecnty
                if(questionWrapper.inputType == 'Currency' && questionWrapper.questionCode == '90002')
                {
                    Map<String,String> errorMessagesLookup3 =  new Map<String,String>();
                    errorMessagesLookup3.put('required', 'quote.application.location.stockValue.required.error.label');
                    errorMessagesLookup3.put('InvalidAmount', 'quote.application.location.stockValue.amount.error.label');
                    errorMessagesLookup3.put('InvalidMinAmount', 'quote.application.stockValue.minAmount.error.label');

                    questionWrapper.errorMessagesLookup =  errorMessagesLookup3;
                    questionWrapper.minimumValidationRequired = true;
                    questionWrapper.minimumValidationAmount = 200;
                }

                if(questionWrapper.inputType == 'Currency' && questionWrapper.questionCode == '90007')
                {
                   Map<String,String> errorMessagesLookup2 =  new Map<String,String>();
                   errorMessagesLookup2.put('InvalidAmount', 'quote.application.usRevenue.amount.error.label');
                   questionWrapper.errorMessagesLookup = errorMessagesLookup2;
                
                }*/

                
                //check if any values for drop downs are available
                if(question.AvailableValuesEn__c!=null)
                {
                    dropDownValuesEng = question.AvailableValuesEn__c.split(';');
                    for(String value:dropDownValuesEng)
                    {
                        value = value.trim();
                    }
                }

                if(question.AvailableValuesFr__c!=null)
                {
                    dropDownValuesFr = question.AvailableValuesEn__c.split(';');
                    for(String value:dropDownValuesFr)
                    {
                        value = value.trim();
                    }
                }

                questionWrapper.dropDownValues   = dropDownValuesEng;
                questionWrapper.dropDownValuesFr = dropDownValuesFr;
    			sicQuestionWrapperRecords.add(questionWrapper);
            }
		}
		return sicQuestionWrapperRecords;

	}

    global class SMEQ_ApplicationQuestionResponseModel extends SMEQ_RestResponseModel
    {
        public List<SMEQ_ApplicationQuestionWrapper> payload {public get; public set;} 
    }

    global class SMEQ_ApplicationQuestionWrapper
    {
        public String questionText {public get; public set;}
        public String questionCode {public get; public set;}
        public String category {public get; public set;}
        public String subCategory {public get; public set;}
        public String inputType {public get; public set;}
        public Boolean frontEndVisibility {public get; public set;}
        public String resourceBundleLabelKey {public get; public set;}
        public String resourceBundlePlaceHolderKey {public get; public set;}
        public List<String> dropDownValues {public get; public set;}
        public List<String> dropDownValuesFr {public get; public set;}
        public String validationErrorMessageLabelEn {public get; public set;}
        public String validationErrorMessageLabelFr {public get; public set;}
        public Boolean requiredField {public get; public set;}
        public Boolean minimumValidationRequired {public get; public set;}
        public Double minimumValidationAmount {public get; public set;}
        public Map<String,String> errorMessagesLookup {public get; public set;} 
 
    }
    

}