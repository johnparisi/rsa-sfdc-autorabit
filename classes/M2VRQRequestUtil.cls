/*
    Description     :   Utility class for RQ Service. This class adds ClaimsMadeInd node to RQ XML request, removes if there are multiple CommlPolicy nodes and keeps only one.
    Developer       :   CSA
    Created Date    :   Dec 2019
*/
global class M2VRQRequestUtil implements vlocity_ins.VlocityOpenInterface2 {
    global Map<String, Object> invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        try {
            Datetime startTime = datetime.now();
            String noOfClaimsLast5Yrs;
            String FoodRevenue;
            String LiquorRevenue;
            List<String> onsiteFacilities = new List<String>();
            List<String> onsitefacilitiesForRQ = new List<String>();
            
            //system.debug('JSON @@@@@ -: '+JSON.serializePretty(input));
            
            String RQXML = (String)input.get('RequestXML');
            system.debug('RQXML : '+RQXML);
            system.debug('QuoteId : '+RQXML);
            
            // Parse JSON response to get all the totalPrice field values.
            JSONParser parser = JSON.createParser(JSON.serializePretty(input.get('getSFData')));
            while (parser.nextToken() != null) {
                if (parser.getText() == 'noOfClaimsLast5Yrs') {
                    parser.nextToken();
                    system.debug('noOfClaimsLast5Yrs:'+parser.getText());
                    noOfClaimsLast5Yrs = parser.getText();
                    //break;
                }else if(parser.getText() == 'FoodRevenue') {
                    parser.nextToken();
                    system.debug('FoodRevenue:'+parser.getText());
                    FoodRevenue = parser.getText();
                }else if(parser.getText() == 'LiquorRevenue') {
                    parser.nextToken();
                    system.debug('FoodRevenue:'+parser.getText());
                    LiquorRevenue = parser.getText();
                }else if(parser.getText() == 'locationOnsiteFacility') {
                    parser.nextToken();
                    system.debug('locationOnsiteFacility:'+parser.getText());
                    if(String.isNotBlank(parser.getText()))
                        onsiteFacilities.addAll(parser.getText().split(';'));
                    System.debug(onsiteFacilities);
                }else {
                    
                }
            }
            
            if(onsiteFacilities.size() >= 0) {
                for(SME_Mapping__c mapping : [Select Id, externalValue__c, SFKey__c, Field__c From SME_Mapping__c Where Field__c = 'dynamicQuestionOnsiteFacilities' AND SFKey__c IN: onsiteFacilities]) {
                    onsitefacilitiesForRQ.add(mapping.externalValue__c);
                }
            }
            
            DOM.Document doc = new DOM.Document();
            doc.load(RQXML);
            
            DOM.Document claimsDoc = new DOM.Document();
            DOM.XMLNode CommlCoverage = claimsDoc.createRootElement('CommlCoverage', null, null);
            DOM.XmlNode CoverageCd = CommlCoverage.addChildElement('CoverageCd', null, null).addTextNode('csio:2');
            DOM.XmlNode CommlCoverageSupplement = CommlCoverage.addChildElement('CommlCoverageSupplement', null, null);
            DOM.XMLNode ClaimsMadeInfo = CommlCoverageSupplement.addChildElement('ClaimsMadeInfo', null, null);
            ClaimsMadeInfo.addChildElement('ClaimsMadeInd', null, null).addTextNode(noOfClaimsLast5Yrs == null ? '0' : noOfClaimsLast5Yrs);
            
            system.debug(doc.getRootElement().getChildElements()[0].getChildElements()[0].getChildElements()[0].getChildElements()[1].getChildElements()[1]);
            Integer commlPolicyNodeCount = 0;
            DOM.XMLNode parentNodeOfCommlPolicyNode = doc.getRootElement().getChildElements()[0].getChildElements()[0].getChildElements()[0].getChildElements()[1].getChildElements()[1];
            for(DOM.XMLNode node : parentNodeOfCommlPolicyNode.getChildren()) {
                system.debug('************'+node.getName());
                if(node.getName() == 'CommlPolicy') {
                    system.debug('-------------'+node);
                    if(commlPolicyNodeCount > 0) { 
                        parentNodeOfCommlPolicyNode.removeChild(node);           
                    }
                    system.debug('-------------'+node);
                    DOM.XMLNode firstCoverageNode = null;
                    DOM.XMLNode commlPolicyNode;
                    DOM.XMLNode commlCoverageNodeToRemove;
                    system.debug('-------------'+node);
                    for(DOM.XMLNode childNode : node.getChildElements()) {
                        if(childNode.getName() == 'CommlCoverage') {
                            if(firstCoverageNode == null)
                                firstCoverageNode = childNode;
                            if(childNode.getChildElements().size() == 1) {
                                system.debug('================single childed nodes================'+childNode);
                                commlCoverageNodeToRemove = childNode;
                                commlPolicyNode = node;
                            }
                        }
                    }
                    
                    if(firstCoverageNode == null) { //Means that there are no CommlCoverage nodes under CommlPolicy node. In this case add Claims node after CompanyCd node
                        firstCoverageNode = node.getChildElements()[3];
                    }
                    node.insertBefore(CommlCoverage, firstCoverageNode);
                    system.debug('-------------'+node);
                    
                    //  Removes empty commlCoverage node
                    if(commlCoverageNodeToRemove != null)
                        commlPolicyNode.removeChild(commlCoverageNodeToRemove);
                    system.debug('-------------'+node);
                    commlPolicyNodeCount++;
                    system.debug('-------------'+node);
                }else if(node.getName() == 'GeneralLiabilityLineBusiness') {
                    
                    DOM.XMLNode GLCNode_Food = node.getChildElements()[1].addChildElement('GeneralLiabilityClassification', null, null);
                    DOM.XMLNode ExposureInfo_Food = GLCNode_Food.addChildElement('ExposureInfo', null, null);
                    ExposureInfo_Food.addChildElement('PremiumBasisCd', null, null).addTextNode('rsa:food');
                    ExposureInfo_Food.addChildElement('Exposure', null, null).addTextNode(FoodRevenue == null ? '0' : FoodRevenue);
                    
                    DOM.XMLNode GLCNode_Liquor = node.getChildElements()[1].addChildElement('GeneralLiabilityClassification', null, null);
                    DOM.XMLNode ExposureInfo_Liquor = GLCNode_Liquor.addChildElement('ExposureInfo', null, null);
                    ExposureInfo_Liquor.addChildElement('PremiumBasisCd', null, null).addTextNode('csio:7');
                    ExposureInfo_Liquor.addChildElement('Exposure', null, null).addTextNode(LiquorRevenue == null ? '0' : LiquorRevenue);
                    
                }else if(node.getName() == 'CommlSubLocation') {
                    for(String onsiteFacility : onsitefacilitiesForRQ) {
                        DOM.XMLNode exposureNode = node.addChildElement('ExposureInfo', null, null);
                        exposureNode.addChildElement('ExposureCd', null, null).addTextNode(onsiteFacility);
                    }
                }else if(node.getName() == 'InsuredOrPrincipal') {
                    for(DOM.XMLNode childNode : node.getChildElements()) {
                        if(childNode.getName() == 'InsuredOrPrincipalInfo') {
                            Integer BusinessInfoNodeCount = 0;
                            for(DOM.XMLNode businessInfoNode : childNode.getChildElements()) {
                                if(businessInfoNode.getName() == 'BusinessInfo') {
                                    if(BusinessInfoNodeCount >= 1) {
                                        childNode.removeChild(businessInfoNode);
                                    }
                                    
                                    BusinessInfoNodeCount++;
                                }
                            }
                        }
                    }
                }
            }
            
            system.debug(doc.toxmlstring());
            output.put('result', doc.toxmlstring());
            
            return output;
        }catch(Exception e){
            system.debug('Exception: '+e.getStackTraceString());
            return null;
        }
    }
}