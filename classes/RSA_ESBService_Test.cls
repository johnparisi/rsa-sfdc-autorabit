/**
 * Author: James Lee
 * Created At: November 5, 2016
 * 
 * Unit tests for RSA_ESBService.
 */
@isTest
public class RSA_ESBService_Test
{
	static void postUserDataSetup()
    {
        SMEQ_TestDataGenerator.generateFullTestQuoteV2();
    }
    
    private static Quote__c getQuote()
    {
        return [
            SELECT id, Package_Type_Selected__c, Case__r.Segment__c
            FROM Quote__c
            LIMIT 1
        ];
    }
    
    static testMethod void testCSIOXMLGeneratorGetQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riGetQuote, q.Id);
            String request = res.getRequestXML();
            System.assert(request.contains(riGetQuote.getServiceType()));
        }
    }
    
    static testMethod void testCSIOXMLGeneratorGet3Quote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riGet3Quote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_3_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riGet3Quote, q.Id);
            String request = res.getRequestXML();
            System.assert(request.contains(riGet3Quote.getServiceType()));
        }
    }
    
    static testMethod void testCSIOXMLGeneratorUploadQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riUploadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.UPLOAD_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riUploadQuote, q.Id);
            String request = res.getRequestXML();
            System.assert(request.contains(riUploadQuote.getServiceType()));
        }
    }
    
    static testMethod void testCSIOXMLGeneratorFinalizeQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riFinalizeQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.FINALIZE_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riFinalizeQuote, q.Id);
            String request = res.getRequestXML();
            System.assert(request.contains(riFinalizeQuote.getServiceType()));
        }
    }
    
    static testMethod void testCSIOXMLGeneratorDownloadQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riDownloadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riDownloadQuote, q.Id);
            String request = res.getRequestXML();
            System.assert(request != null);
        }
    }
    
    static testMethod void testHttpRequest()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riDownloadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, q);
            RSA_ESBService res = new RSA_ESBService(riDownloadQuote, q.Id);
            HttpRequest req = res.getHttpRequest('');
            System.assertEquals('POST', req.getMethod());
            System.assertEquals('text/xml;charset=UTF-8', req.getHeader('Content-Type'));
        }
    }
    
    static testMethod void testContext()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Quote__c q = getQuote();
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_QUOTE, q);
            String context = RSA_ESBService.getContext(riGetQuote);
            System.assertEquals(RSA_ESBService.RSA_ESB_CONTEXT_1_28, context);
            
            RequestInfo riGet3Quote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_3_QUOTE, q);
            context = RSA_ESBService.getContext(riGet3Quote);
            System.assertEquals(RSA_ESBService.RSA_ESB_CONTEXT_1_28, context);
            
            RequestInfo riUploadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.UPLOAD_QUOTE, q);
            context = RSA_ESBService.getContext(riUploadQuote);
            System.assertEquals(RSA_ESBService.RSA_ESB_CONTEXT_1_28, context);
            
            RequestInfo riFinalizeQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.FINALIZE_QUOTE, q);
            context = RSA_ESBService.getContext(riFinalizeQuote);
            System.assertEquals(RSA_ESBService.RSA_ESB_CONTEXT_1_28, context);
            
            RequestInfo riDownloadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, q);
            context = RSA_ESBService.getContext(riDownloadQuote);
            System.assertEquals(RSA_ESBService.RSA_ESB_CONTEXT_1_7_1, context);
        }
    }
}