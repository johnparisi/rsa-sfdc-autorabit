public with sharing class FP_BindModel {
  	public String quoteID {public get; public set;}
  	public String continuationReferenceID {get; set;}
  	public String policyNumber {get; set;}
  	public String xmlRequest {get; set;}
  	public Map<String, String> logWrappers {get; set;}
    
}