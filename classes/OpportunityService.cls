/**
* @File Name    :   OpportunityService
* @Description  :   Opportunity Service Layer
*                   Contains processing service methods related to the Opportunity Object
* @Date Created :   01/14/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Service
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       01/14/2017  Habiba Zaman    Created the file/class
* 1.1       01/20/2017  Habiba Zaman    Movded policy queue related logic to Policy Service.
* 1.2       10/03/2017  Priyanka Sirohi Changed the opportunity Stage to Underwriting instead of Renewal Assigned in line 71
*/

public with sharing class OpportunityService {
    
    /**
    * @method   createRenewalOpportunities
    * @param    policies
    * @return   N/A
    *
    * Create Renewal Opportunities for policies those are qualified and update policy
    * with opportunity reference and status.
    */
    
    public void createRenewalOpportunities(List<Policy__c> policies){
        
        PolicyService policyService = new PolicyService();
        List<Policy__c> policyToQueue = new List<Policy__c>();
        List<Policy__c> policyList = new List<Policy__c>();

        //Map to relate policy with Opprtunity
        Map<String,Opportunity> policyToOppMap = new Map<String,Opportunity>();
        
        //Opportunity Product list
        Map<String, Id> productInfo = new Map<String, Id>();
        productInfo = getProductInformation();

        Map<String, String> strategicSegmentMap = new Map<String, String>();
        strategicSegmentMap = getStrategicSegmentMapping();

        // For opportunity Kam and Underwriter Manager.
        Map<Id,Account> brokerAccountMap = new Map<Id,Account>();
        List<Id> brokerIdList = new List<Id>();
        for(Policy__c p : policies){
            if(p.Master_Broker_Account__c != null){
                brokerIdList.add(p.Master_Broker_Account__c);
            }
        }
brokerAccountMap = policyService.findBrokerAccountsForOpportunity(brokerIdList);
        
        for(Policy__c policy : policies){
            String policyStage = policy.Policy_Stage__c;
            //only create opportunities for qualified renewals or already opportunity is not created.
            if(policyStage.contains('Renewal Cycle Initialized')){  
                    Opportunity opp = new Opportunity();
                    opp.RecordTypeId = getRenewalOpportunityRecordType();
                    opp.Name = policy.Client_Name__c;
                    opp.bkrClientAccount__c = policy.Client_Account__c;
                    opp.AccountId = policy.Master_Broker_Account__c;
                    if(policy.Master_Broker_Account__c != null){
					    try{
                        opp.bkrMMGSL_KAM__c = brokerAccountMap.get(policy.Master_Broker_Account__c).bkrAccount_AssignedKAM__c;
                        opp.bkrMMGSL_UnderwritingManager__c = brokerAccountMap.get(policy.Master_Broker_Account__c).bkrMMGSL_UnderwritingManager__c;
						}
                        catch(Exception e){
                               //KAM not found or not Active, causing failures. 
                        }

                    }
                    opp.bkrMMGSL_StrategicSegment__c = strategicSegmentMap.size() > 0 ?  strategicSegmentMap.get(policy.Strategic_Segment__c) : '';
                    opp.bkrOpportunity_Region__c = policy.Region_Group__c;
                    opp.bkrProductLineEffectiveDate__c = policy.Term_Expiry_Date__c;
                    opp.CloseDate  = policy.Term_Expiry_Date__c;
                    opp.StageName = 'Underwriting';
                    opp.Expiry_Premium__c = policy.bkrPolicy_Premium__c;
                    opp.bkrPremium__c = policy.bkrPolicy_Premium__c;
                    opp.bkrProduct__c = productInfo.size() > 0 ? productInfo.get(policy.Product__c) : null;
                    opp.Type = 'Renewal';
                    opp.OwnerId = policy.OwnerId;
                    policyList.add(policy);
                    policyToOppMap.put(policy.Control_Field__c, opp);
                
            }
        }

        //Map<Opp Id, Opportunity>
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        if(policyToOppMap.size() > 0){
            insert policyToOppMap.values();

            for(Opportunity o :policyToOppMap.values() ){
                oppMap.put(o.Id, o);
            }
        }

        //update status for those policies opportunity is created
        if(policyList.size() >0){
            for(Policy__c p : policyList){
                p.bkrPolicy_Opportunity__c = policyToOppMap.get(p.Control_Field__c).Id;
                if(p.bkrPolicy_Opportunity__c != null){
                    p.Policy_Stage__c = 'Renewal Opportunity Created';
                }                               
            }
        }
        
    }

    /**
    * @method   getStrategicSegmentMapping
    * @param    n/a
    * @return   Map<String,String>  Return mapping between policy strategic segment and 
    *                               opportunity segment
    *
    * Currently CI Portfolio Spreadsheet for policy has different values than Opportunity
    * Strategic Segement.
    */

    public Map<String, String> getStrategicSegmentMapping(){
        Map<String, String> strategicSegmentMap = new Map<String, String>();

        strategicSegmentMap.put('B&p Services','Business & Professional Services');
        strategicSegmentMap.put('Construction','Construction');
        strategicSegmentMap.put('Entertainment','Entertainment');
        strategicSegmentMap.put('Farming','Farming');
        strategicSegmentMap.put('Hospitality','Hospitality');
        strategicSegmentMap.put('Manufacturing','Manufacturing & Wholesaling');
        strategicSegmentMap.put('Mining Industry','Mining');
        strategicSegmentMap.put('Oil & Gas','Oil & Gas');
        strategicSegmentMap.put('Project Construction','Project Construction');
        strategicSegmentMap.put('Realty','Realty');
        strategicSegmentMap.put('Recreation','Recreation');
        strategicSegmentMap.put('Renewable Energy','Renewable Energy');
        strategicSegmentMap.put('Retail','Retail');
        strategicSegmentMap.put('Transportation/warehousing','Transportation & Warehousing');

        return strategicSegmentMap;
    }


    /**
    * @method   getProductInformation
    * @param    n/a
    * @return   Map<String,Id>  Return Information about product.
    *
    * Map between Mid-Market GSL Products and Id.
    */
    private Map<String, Id> getProductInformation(){

        Map<String, Id> productMap = new Map<String, Id>();
        String mmGSLProductFamily='Mid-Market & Global Specialty Lines';

        List<Product2> productList = [Select Id, Name from Product2 where Family=:mmGSLProductFamily and IsActive = true];

        if(productList.size() >0){
            for(Product2 p : productList){
                String productName = p.Name;
                if(productName.contains('Property & Casualty')){
                    productMap.put('Property/Casualty', p.Id);
                }
                else if(productName.contains('Fleet & Garage')){
                    productMap.put('Fleet And Garage', p.Id);
                }
            }
        }

        return productMap;
    }


    
    /**
    * @method   getRenewalOpportunityRecordType
    * @param    n/a
    * @return   Id  Return Opportunity record type id for Renewal
    *
    */
    public Id getRenewalOpportunityRecordType(){
        Map<String,Id> oppType = Utils.GetRecordTypeIdsByDeveloperName(Opportunity.SObjectType, true);

        Id oppRecordTypeId = oppType.get('bkrMMGSL_Opportunity_Renewals');

        return oppRecordTypeId;
    }

    /**
    * @method   getPriceBookInfo
    * @param    n/a
    * @return   Map<Id,Id>  Return map between product Id and PricebookEntry Id
    *
    */
    public Map<Id,Id> getPriceBookEntry(){
        Id pricebookEntry;

        List<Pricebook2> pb2= [ Select Id from Pricebook2 where IsActive = true];
        if(pb2.size() > 0){
            pricebookEntry = pb2.get(0).Id;
        }
        Map<Id,Id> priceBookEntryProductMap = new Map<Id,Id>();

        List<PricebookEntry> pbe = [Select Id, Pricebook2Id,Product2Id from PricebookEntry where Pricebook2Id =:pricebookEntry];
        if(pbe.size() > 0){
            for(PricebookEntry pb : pbe){
                priceBookEntryProductMap.put(pb.Product2Id, pb.Id);
            }
        }

        return priceBookEntryProductMap;
    }

    /**
    * @method   updateRenewalOpportunityBeforeUpdate
    * @param    records     Contain opportunity record for Trigger.New
    * @param    oldRecords  Contain opportunity record for Trigger.Old
    * @return   N/A
    *
    * Performs field update before saving the record.
    *
    */
    public void updateRenewalOpportunityBeforeUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
        //Map for bulk update for OpportunityLine item

        // Iterate Opportunity, only process when certain fields are updated
        for (Opportunity o : records) {
            Opportunity old = oldRecords != null && oldRecords.containsKey(o.Id) ? oldRecords.get(o.Id) : null;
            if (old != null) {
                // only process if one of these field changes on update
                if (o.Target_Premium__c != old.Target_Premium__c)  {
                        //o.Amount = o.Target_Premium__c;
                    }
            }
        }
    }

    /**
    * @method   updateRenewalOpportunityAfterUpdate
    * @param    records     Contain opportunity record for Trigger.New
    * @param    oldRecords  Contain opportunity record for Trigger.Old
    * @return   N/A
    *
    * Performs insert/update/delete operation on dependent records after opportunity is updated.
    *
    */
    public void updateRenewalOpportunityAfterUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
        //Map for bulk update for OpportunityLine item
        Map<Id, Id> policyUWChangeList = new Map<Id, Id>();
        List<Opportunity> toProcess = new List<Opportunity>();
        Integer teamMemeberUpdateCount = 0;
        // Iterate Opportunity, only process when certain fields are updated
        for (Opportunity o : records) {
            Opportunity old = oldRecords != null && oldRecords.containsKey(o.Id) ? oldRecords.get(o.Id) : null;
            if (old != null) {
                if(o.bkrMMGSL_KAM__c != old.bkrMMGSL_KAM__c ||
                     o.bkrMMGSL_UnderwritingManager__c != o.bkrMMGSL_UnderwritingManager__c){
                    teamMemeberUpdateCount ++;              
                }

                if(o.OwnerId != old.OwnerId)
                { 
                    if(o.Renewal_Policy__c != null){
                        policyUWChangeList.put(o.Renewal_Policy__c, o.OwnerId);
                    }
                }
                
            }
        }

        if(teamMemeberUpdateCount > 0){     
            updateOpportunityTeamMembers(records, oldRecords);
        }

        if(policyUWChangeList.size() > 0){

            updatePolicyOwner(policyUWChangeList);
        }

    }
    
    /**
    * @method   addOpportunityTeamMembers
    * @param    oppList OpportunityList for which Team Member needs to be created.
    * @return   N/A
    *
    * Create Opportunity Team Member based on the KAM and Underwriter Manager of the Opportunity 
    *
    */
    public void addOpportunityTeamMembers(List<Opportunity> oppList){
        if(oppList.size() > 0){
            List<OpportunityTeamMember> oppTeam = new List<OpportunityTeamMember>();
            for(Opportunity o :oppList ){
                //create Kam record
                if(o.bkrMMGSL_KAM__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.UserId = o.bkrMMGSL_KAM__c;
                    otm.TeamMemberRole = 'Key Account Manager';
                    otm.OpportunityAccessLevel = 'Edit';
                    otm.OpportunityId = o.Id;
                    oppTeam.add(otm);
                }
                //create UWM
                if(o.bkrMMGSL_UnderwritingManager__c != null){
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.UserId = o.bkrMMGSL_UnderwritingManager__c;
                    otm.TeamMemberRole = 'Underwriting Manager';
                    otm.OpportunityAccessLevel = 'Edit';
                    otm.OpportunityId = o.Id;
                    oppTeam.add(otm);
                }       

            }
            if(oppteam.size() > 0){
                upsert oppTeam;
            }
        }
    }

    
    /**
    * @method   updateOpportunityTeamMembers
    * @param    records     Contain opportunity record for Trigger.New
    * @param    oldRecords  Contain opportunity record for Trigger.Old
    *
    * Create Opportunity Team Member with new values and delete old team member based on the value of the Opportunity 
    *
    */
    public void updateOpportunityTeamMembers(List<Opportunity> records, Map<Id, Opportunity> oldRecords){

        List<String> fieldsToProcess = new List<String>{
                // Key Account Manager
                'bkrMMGSL_KAM__c',
                // UnderWriter Manager
                'bkrMMGSL_UnderwritingManager__c'
            };
        List<OpportunityTeamMember> otmToDelete = new List<OpportunityTeamMember>();

        Map<Id, List<Id>> teamMembersToRemove = new Map<Id, List<Id>>();
        for(Opportunity o : records){
            //Check each field to see if they changed
            //If they did change, remove the old one from the team
            if(oldRecords != null){
                Opportunity old = oldRecords.get(o.Id);
                for(String field : fieldsToProcess){
                    if(old.get(field) != o.get(field) && old.get(field) != null){
                        List<Id> userIds;
                        if(!teamMembersToRemove.keySet().contains(o.Id)){
                            userIds = new List<Id>();
                        }
                        else{
                            userIds = teamMembersToRemove.get(o.Id);
                        }
                        //The old team member to remove
                        userIds.add((Id)old.get(field));
                        teamMembersToRemove.put(o.Id, userIds);
                    }
                }
            }
        }//end of for loop

        //Delete, build out a query to find the changed(removed) users
        if(teamMembersToRemove.keySet().size() > 0){
            String deleteQuery = 'SELECT Id FROM OpportunityTeamMember WHERE ';
            String condition = '';
            for (Id oppId : teamMembersToRemove.keySet()){
                for(Id userId : teamMembersToRemove.get(oppId)){
                    if(condition != ''){
                        condition += ' OR ( OpportunityId = \''+ oppId +'\' AND UserId = \''+ userId +'\' )';
                    }
                    else{
                        condition += ' ( OpportunityId = \''+ oppId +'\' AND UserId = \''+ userId +'\' )';
                    }
                }
            }
            deleteQuery += condition;
            otmToDelete = Database.query(deleteQuery);
            //Delete them
            delete otmToDelete;
        }

        addOpportunityTeamMembers(records);
        
    }
    
    /**
    * @method   updatePolicyOwner
    * @param    policyToUWMap   Contain map between policy id and new owner Id.
    *
    * Update Policy owner when Opportunity owner is changed. 
    *
    */
    public void updatePolicyOwner(Map<Id, Id> policyToUWMap){

        List<Policy__c> policies=[Select Id, OwnerId from Policy__c where Id in : policyToUWMap.keySet()];
        List<Policy__c> policiesToUpdate = new List<Policy__c>();

        if(policies.size() > 0){
            for(Policy__c p : policies){
                if(p.OwnerId != policyToUWMap.get(p.Id))
                {
                    p.OwnerId = policyToUWMap.get(p.Id);
                    policiesToUpdate.add(p);
                }
            }
            update policiesToUpdate;
        }


    }


    /**
    * @method   updateNewBusinessOpportunityAfterUpdate
    * @param    records     Contain opportunity record for Trigger.New
    * @param    oldRecords  Contain opportunity record for Trigger.Old
    * @return   N/A
    *
    * Performs insert/update/delete operation on dependent records after opportunity is updated.
    *
    */
    public void updateNewBusinessOpportunityAfterUpdate(List<Opportunity> records, Map<Id, Opportunity> oldRecords){
        //Map for bulk update for OpportunityLine item
        List<Opportunity> toProcess = new List<Opportunity>();
        // Iterate Opportunity, only process when certain fields are updated
        for (Opportunity o : records) {
            Opportunity old = oldRecords != null && oldRecords.containsKey(o.Id) ? oldRecords.get(o.Id) : null;
            if (old != null) {
                if(o.bkrMMGSL_PolicyRecordCreated__c != old.bkrMMGSL_PolicyRecordCreated__c){
                    toProcess.add(o);               
                }
                
            }
        }

        if(toProcess.size() > 0){     
            createNewBusinessPolicy(toProcess);
        }

    }

    /**
    * @method   createNewBusinessPolicy
    * @param    oppList OpportunityList for which Policies needed to be created.
    * @return   N/A
    *
    * Create New Business policy when a policy number entered.
    *
    */
    public void createNewBusinessPolicy(List<Opportunity> oppList){
        if(oppList.size() > 0){
            Map<String,Id> policyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
            List<Policy__c> policies = new List<Policy__c>();
            for(Opportunity o :oppList ){
                if(o.bkrMMGSL_PolicyRecordCreated__c && String.isNotBlank(o.bkrPolicyNumber__c)){
                    Policy__c policy = new Policy__c();
                    policy.RecordTypeId = policyTypes.get('bkrMMGSL_Policy_NewBusiness');
                    policy.Broker__c = o.AccountId;
                    policy.Client_Account__c = o.bkrClientAccount__c;
                    policy.Effective_Date__c = o.bkrProductLineEffectiveDate__c;
                    policy.Name = o.bkrPolicyNumber__c;
                    policy.Type__c = 'New Business';
                    policy.bkrPolicy_Opportunity__c = o.Id;
                    policy.bkrPolicy_Premium__c = o.bkrPremium__c;
                    policy.bkrPolicy_Producer__c = o.bkrOpportunity_Producer__c;
                    policy.bkrPolicy_Product__c = o.bkrProduct__c;

                    policies.add(policy);
                }   

            }
            if(policies.size() > 0){
                insert policies;
            }
        }
    }

    /**
    * @method   validateSpecialtyLineStageChange
    * @param    records     Contain opportunity record for Trigger.New
    * @param    oldRecords  Contain opportunity record for Trigger.Old
    * @return   N/A
    *
    * Performs insert/update/delete operation on dependent records after opportunity is updated.
    *
    */
    public void validateSpecialtyLineStageChange(List<Opportunity> records, Map<Id, Opportunity> oldRecords){

        List<Opportunity> toProcess = new List<Opportunity>();
        Map<Id, Integer> oppTaskCompleted = new Map<Id,Integer>();
        oppTaskCompleted = getCompletedTaskCountForOpportunity(records);
        // Iterate Opportunity, only process when certain fields are updated
        for (Opportunity o : records) {
            Opportunity old = oldRecords != null && oldRecords.containsKey(o.Id) ? oldRecords.get(o.Id) : null;
            if (old != null) {
                if(o.StageName!= old.StageName){
                    if(o.StageName == 'Active Lead'){
                        if(oppTaskCompleted.get(o.Id) < 2)
                        {
                            o.addError('To Select this stage at least two touchpoints need to be completed');
                        }

                    }
                }
                
            }
        }

    }

    /**
    * @method   getCompletedTaskCountForOpportunity
    * @param    records     Contain opportunity record for Trigger.New
    * @param    oldRecords  Contain opportunity record for Trigger.Old
    * @return   N/A
    *
    * Performs insert/update/delete operation on dependent records after opportunity is updated.
    *
    */
    public Map<Id, Integer> getCompletedTaskCountForOpportunity(List<Opportunity> records){
        Map<Id, Integer> oppTaskCompleted = new Map<Id,Integer>();
        // Iterate Opportunity, only process when certain fields are updated
        if (records.size() > 0){
            for(Opportunity o : records){
                oppTaskCompleted.put(o.Id, 0);
            }
        }
        
   
        List<AggregateResult> groupedResults= [SELECT WhatId, count(Id) countTask
      FROM Task where WhatId != null and Status = 'Completed' and WhatId in : oppTaskCompleted.keySet() 
      GROUP BY WhatId];
        
        for (AggregateResult ar : groupedResults)  {
            oppTaskCompleted.put((Id)ar.get('WhatId'), (Integer)ar.get('countTask'));
        }

        return oppTaskCompleted;

    }
    
    /**
    Updates the decile and quartile fields based on the related actuarial record
    @param records The opportunities to be updated
    */
    
    public void updateDecileQuartile(List<Opportunity> records) {
         System.debug('#### updateDecilQuartile called with ' + records.size() + ' records');       
        // get the related policy ids
    
        List<Id> policyList = new List<Id>();
        for (Opportunity record : records) {
            if (record.Renewal_Policy__c != null) {
                policyList.add(record.Renewal_Policy__c);
            }
        }
        
        // and the related actuarial records
        
        List<Actuarial_Detail__c> actuarialList = [SELECT Policy_Number__r.Id, Id, Decile__c, Quartile__c FROM Actuarial_Detail__c WHERE Policy_Number__r.Id IN :policyList];
        Map<Id, Actuarial_Detail__c> policyActuarialDetailMap = new Map<Id, Actuarial_Detail__c>();
        
        for (Actuarial_Detail__c actuarialDetail : actuarialList) {
            policyActuarialDetailMap.put(actuarialDetail.Policy_Number__r.Id, actuarialDetail);
        }
        
        System.debug('#### ' + policyActuarialDetailMap.size() + ' records in policyDetailActuarialMap');
               
        // for each opportunity record, find the related detail and update the opportunity.
        for (Opportunity record : records) {
            if (record.Renewal_Policy__c != null) {
                System.debug('#### evaluating opportunity ' + record.Id);
                // get the related actuarial record
                Actuarial_Detail__c detail = policyActuarialDetailMap.get(record.Renewal_Policy__c);
                if(detail != null) {                
                    record.Quartileopportunity__c = String.valueOf(detail.Quartile__c);
                    record.Decileopportunity__c = String.valueOf(detail.Decile__c);
                    System.debug('#### setting opportuntiy ' + record.Id + ' to quartile ' + detail.Quartile__c + ' and decile ' + detail.Decile__c);
                }
            }
        }     
        
    }
 
}