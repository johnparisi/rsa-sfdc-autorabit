public without sharing class ApplicationLog {

public static String DEBUG = 'Debug';
public static String ERROR = 'Error';
public static String INFO = 'Info';
public static String WARNING = 'Warning';

// creates an ApplicationLogWrapper object from a given set of parameters
public static ApplicationLogWrapper createLogWrapper(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, String payLoad, Exception ex, DateTime startTime, DateTime endTime) {

        ApplicationLogWrapper msg = new ApplicationLogWrapper();
        
        msg.source = sourceClass;
        msg.logMessage = logMessage;
        msg.sourceFunction = sourceFunction;
        msg.referenceId = referenceId;
        msg.referenceInfo = referenceInfo;
        msg.payload = payLoad;
        msg.debugLevel = logLevel;
    	if(ex != null){
        	msg.ex = ex.getStackTraceString();
        }
	    else{
        	msg.ex = null;
    	}
    	if(startTime != null){
				msg.startTime = startTime;
    	}
    	if(endTime != null){
        		msg.endTime = endTime;
    	}
    	if(startTime != null && endTime != null){
        	msg.Timer = endTime.getTime() - startTime.getTime();
		}
    	return msg;        
    }    

	// Inserts a log record into the database given an ApplicationLogWrapper object    
    public static void logMessage(ApplicationLogWrapper appLog)
    {
        List<ApplicationLogWrapper> appLogs = new List<ApplicationLogWrapper>();
        appLogs.add ( appLog );        
        logMessage ( appLogs );
    }
    
    
    // Turns a LogWrapper into a serialized JSON object
     public static String serializeLogWrapper(ApplicationLogWrapper appLog){
        String serializedAppLog = JSON.serialize(appLog); 
        return serializedAppLog;
    }
    
	// Extracts a logwrapper from its serialized form
    public static ApplicationLogWrapper deserializeLogWrapper(String serializedAppLog){
        ApplicationLogWrapper appLog = new ApplicationLogWrapper();
        appLog = (ApplicationLogWrapper) JSON.deserialize(serializedAppLog, ApplicationLogWrapper.class);
        return appLog;
        
    }

    // Inserts a log record into the database given a Map<String, String> object (used in FP_AngularPageController for message passing)   
    public static void logMessage(Map<String, String> appLogs){
        List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();
        for(String appLog : appLogs.values()){
            logs.add(deserializeLogWrapper(appLog));
        }
		logMessage(logs);

    }
    
	// Inserts a log record into the database given a List<ApplicationLogWrapper> object
    public static void logMessage(List<ApplicationLogWrapper> appLogs){
        List<String> toLog = new List<String>();
        for(ApplicationLogWrapper appLog : appLogs){
            toLog.add(serializeLogWrapper(appLog));
        }
        logMessage(toLog);
    
    }
    
	// Future method that logs a list of serialized ApplicationLogWrappers, inserting new log records into the database
    @future
    public static void logMessage(List<String> appLogs)
    {
        List<Application_Log__c> logs = new List<Application_Log__c>();
        for (String inputLog : appLogs)
        {
            ApplicationLogWrapper appLog = deserializeLogWrapper(inputLog);
            Application_Log__c log = new Application_Log__c();
            log.Source__c = appLog.source;
            log.Source_Function__c = appLog.sourceFunction;
            log.Reference_Id__c = appLog.referenceId;
            log.Reference_Info__c = appLog.referenceInfo;
            log.Message__c = appLog.logMessage;
            log.Integration_Payload__c = appLog.payload != null ? 
                appLog.payload.subString(0, Math.min(appLog.payload.length(), 131072)) : null;
            log.Start_Timestamp__c = appLog.startTime;
            log.Finish_Timestamp__c = appLog.endTime;
            if (appLog.ex != null)
            {
                log.Stack_Trace__c = appLog.ex;
            }
            log.Debug_Level__c = appLog.debugLevel;
            log.Timer__c = appLog.timer;

            logs.add(log);
            
            System.Debug('Inserted Log from ' + log.source__c + ' debug level: ' + log.Debug_Level__c);
        }
        
        insert logs;
    }
       
}