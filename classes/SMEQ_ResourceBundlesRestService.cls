@RestResource(urlMapping='/resourceBundles/*')
global with sharing class SMEQ_ResourceBundlesRestService {
    
    
    @HttpGet
    global static SMEQ_ResourceBundlesModel getResourceBundles(){
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');
        SMEQ_ResourceBundlesModel resourceBundlesModel = new SMEQ_ResourceBundlesModel();
        String restURIString = RestContext.request.requestURI;
        if(!restURIString.endsWith('/')){
            restURIString = restURIString + '/';
        }
       Map<integer, String> pathVariablesMap = getPathVariablesIfPresent(restURIString);
       SMEQ_ResourceBundlesDao resourceDao = new SMEQ_ResourceBundlesDao();
       System.debug('The variant provided:' + pathVariablesMap.get(2));
       System.debug('The Bundle provided:' + pathVariablesMap.get(3));
       System.debug('The region provided: ' + pathVariablesMap.get(4));

       SMEQ_ResourceBundlesModel resourceBundleModel = new SMEQ_ResourceBundlesModel();
       try{
            resourceBundleModel = resourceDao.getResourceBundlesByVariantAndRegion(pathVariablesMap.get(2), getRegion());
            system.debug('resourceBundleModel : ' + resourceBundleModel);
           
       }
        catch(Exception se){
            resourceBundleModel.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                        = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            resourceBundleModel.setErrors(errors);
        }
        return resourceBundleModel;
    }
    
    
    @TestVisible
    private static  Map<integer, String> getPathVariablesIfPresent(String requestURIString){
        System.debug('request URI String ' + requestURIString);
        Map<integer, String> pathVariableMap = new Map<integer, String>();
        String[] pathStringSplit = requestURIString.split('/');
        System.debug('Path split ****' + pathStringSplit);
        integer count = 0;
        for(String pathVariable: pathStringSplit){
            pathVariableMap.put(count, pathVariable);
            count++;
        }
        return pathVariableMap;
    }


    private static String getRegion() {
        String region = null;
        List<String> regions = UserService.getRegion(UserInfo.getUserId());
        if(regions != null && !regions.isEmpty()) {
            region = regions.get(0);
        }
        return region;
    }


    
}