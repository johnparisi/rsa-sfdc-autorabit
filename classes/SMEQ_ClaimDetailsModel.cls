global with sharing class SMEQ_ClaimDetailsModel {
	public String id {public get; public set;}
	public String month {public get; public set;}
	public Integer year {public get; public set;}
	public Double amount {public get; public set;}
    public String claimType {public get; public set;}	
}