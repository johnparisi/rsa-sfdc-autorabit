/**
Continuation transport object for the abort transaction.
*/

public with sharing class RA_AbortModel {
  	public String continuationReferenceID {get; set;}
  	public String policyNumber {get; set;}
    public Id caseId {get; set;}
  	public String xmlRequest {get; set;}  
}