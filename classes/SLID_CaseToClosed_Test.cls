/**
  * @author        Saumil Bapat
  * @date          11/16/2016
  * @description   Test class for SLID_CaseToClosed
*/
@isTest
private Class SLID_CaseToClosed_Test {
    static testMethod void testSomeQuotesClosed()
    {

      Case parentCase = SLID_TestDataFactory.createTestCase();
      
      //Create a two test Quote Versions
      Quote_Version__c quoteVersion1 = SLID_TestDataFactory.returnQuoteVersion();
      quoteVersion1.Case__c = parentCase.Id;
      insert quoteVersion1;
      
      Quote_Version__c quoteVersion2 = SLID_TestDataFactory.returnQuoteVersion();
      quoteVersion2.Case__c = parentCase.Id;
      quoteVersion2.Primary__c = false;
      insert quoteVersion2;

      //Create a Reason mapping for the case closed reason
      SLID_Mapping_KeyValue__c ownerKeyValue = new SLID_Mapping_KeyValue__c(
                                                      Type__c = SLID_CaseToClosed.DECLINED_COMMENT_MAPPING_TYPE,
                                                      Name = 'Test Quote Closure',
                                                      Value__c = 'Test Case Closure');
      insert ownerKeyValue;

      Test.StartTest();
      quoteVersion1.Status__c = 'Declined';
      quoteVersion1.Declined_Closed_Reason__c = 'Test Quote Closure';
      update quoteVersion1;
      
      parentCase = [Select Id, isClosed, Status, bkrCase_Close_Reason__c from case where Id = :parentCase.Id];
      System.Assert(parentCase.isClosed == false, '~~~The parent case was closed even with an active quote version');
      
      /*
      quoteVersion2.Status__c = 'Closed';
      quoteVersion2.Declined_Closed_Reason__c = 'Test Quote Closure';
      update quoteVersion2;
      
      parentCase = [Select Id, isClosed, Status, bkrCase_Close_Reason__c from case where Id = :parentCase.Id];
      System.Assert(parentCase.isClosed == true && parentCase.Status == 'Closed', '~~~The parent case was not closed');  
      */
      
      Test.StopTest();
      
      
    }
    
        static testMethod void testAllQuotesClosed()
    {

      Case parentCase = SLID_TestDataFactory.createTestCase();
      
      //Create a two test Quote Versions
      Quote_Version__c quoteVersion1 = SLID_TestDataFactory.returnQuoteVersion();
      quoteVersion1.Case__c = parentCase.Id;
      insert quoteVersion1;
      
      Quote_Version__c quoteVersion2 = SLID_TestDataFactory.returnQuoteVersion();
      quoteVersion2.Case__c = parentCase.Id;
      quoteVersion2.Primary__c = false;
      insert quoteVersion2;

      //Create a Reason mapping for the case closed reason
      SLID_Mapping_KeyValue__c ownerKeyValue = new SLID_Mapping_KeyValue__c(
                                                      Type__c = SLID_CaseToClosed.DECLINED_COMMENT_MAPPING_TYPE,
                                                      Name = 'Test Quote Closure',
                                                      Value__c = 'Test Case Closure');
      insert ownerKeyValue;

      
      Test.StartTest();
      quoteVersion1.Status__c = 'Declined';
      quoteVersion1.Declined_Closed_Reason__c = 'Test Quote Closure';
      quoteVersion2.Status__c = 'Closed';
      quoteVersion2.Declined_Closed_Reason__c = 'Test Quote Closure';
      
      List<Quote_Version__c> quoteVersions = new List<Quote_Version__c>();
      quoteVersions.add(quoteVersion1);
      quoteVersions.add(quoteVersion2);
      update quoteVersions;
      
      parentCase = [Select Id, isClosed, Status, bkrCase_Close_Reason__c from case where Id = :parentCase.Id];
      System.Assert(parentCase.isClosed == true && parentCase.Status == 'Closed', '~~~The parent case was not closed');  
      Test.StopTest();
      
      
    }
}