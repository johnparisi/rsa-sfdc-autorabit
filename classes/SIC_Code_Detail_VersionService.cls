public class SIC_Code_Detail_VersionService {
     public void updateSIC_Code_Detail_Version_Id(List<SIC_Code_Detail_Version__c> records){
        if(records.size() > 0){
          Set<Id> SICCodeIds = new Set<Id>();
            for (SIC_Code_Detail_Version__c sicCodeVersion : records) {
              if(sicCodeVersion.SIC_Code__c != null){
                SICCodeIds.add(sicCodeVersion.SIC_Code__c);
              }        
            }
            Map<Id,SIC_Code__c> sicCodeMap = new Map<Id, SIC_Code__c>([select Id, SIC_Code__c from SIC_Code__c where id in: SICCodeIds]);
            for(SIC_Code_Detail_Version__c sicCodeVersion : records){
              if(sicCodeVersion.SIC_Code__c != null){
                sicCodeVersion.SIC_Code_Detail_Version_Id__c = sicCodeMap.get(sicCodeVersion.SIC_Code__c).SIC_Code__c 
                                        + sicCodeVersion.Offering__c 
                                        + sicCodeVersion.Region__c;
              }
            }
        }
    }
}