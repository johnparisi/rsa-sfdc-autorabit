/**
 * RiskHandler
 * Author: James Lee
 * Date: April 28 2017
 * 
 * The new Object Handler classes will follow this pattern.
 * 
 * It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
 * Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
 * 
 * You should never need to update this unless you are adding a business unit or record types. 
 */
public with sharing class SicAnswerHandler
{
    private List<SIC_Answer__c> smeqSicAnswers = new List<SIC_Answer__c>();

    public void beforeInsert(List<SIC_Answer__c> records)
    {
        new SicAnswerDomain().beforeInsert(records);

        initRisks(records);

        if (smeqSicAnswers.size() > 0)
        {
            new SicAnswerDomain.SMEQDomain().beforeInsert(smeqSicAnswers);
        }
    }

    public void beforeUpdate(List<SIC_Answer__c> records, Map<Id, SIC_Answer__c> oldRecords)
    {
        new SicAnswerDomain().beforeUpdate(records, oldRecords);

        initRisks(records);

        if (smeqSicAnswers.size() > 0)
        {
            new SicAnswerDomain.SMEQDomain().beforeUpdate(smeqSicAnswers, oldRecords);
        }
    }

    public void afterInsert(List<SIC_Answer__c> records)
    {
        new SicAnswerDomain().afterInsert(records);

        initRisks(records);

        if (smeqSicAnswers.size() > 0)
        {
            new SicAnswerDomain.SMEQDomain().afterInsert(smeqSicAnswers);
        }
    }
    
    public void afterUpdate(List<SIC_Answer__c> records, Map<Id, SIC_Answer__c> oldRecords)
    {
        new SicAnswerDomain().afterUpdate(records, oldRecords);

        initRisks(records);
        
        if (smeqSicAnswers.size() > 0)
        {
            new SicAnswerDomain.SMEQDomain().afterUpdate(smeqSicAnswers, oldRecords);
        }
    }

    private void initRisks(List<SIC_Answer__c> records)
    {
        for (SIC_Answer__c a : records)
        {
            smeqSicAnswers.add(a);
        }
    }
}