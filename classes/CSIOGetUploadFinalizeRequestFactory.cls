/**
 * CSIOGetUploadFinalizeRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to generate the xml Request for the GETQUOTE, FINALIZE, UPLOAD, GET3QUOTE service 
 */
public abstract class CSIOGetUploadFinalizeRequestFactory extends CSIORequestDocumentFactory{

    public CSIOGetUploadFinalizeRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds.requestingUser.FederationIdentifier, sds.smeCase.New_Business_Source__c, sds.quote.Business_Source_ID__c, ri);
        soqlDataSet = sds;
        ri = ri;
        caseCreateDateLong = sds.smeCase.CreatedDate.format(dateFormatShort);
        caseCreateDateShort = sds.smeCase.CreatedDate.format(dateFormatShort);
        rsaOperation = ri.getServiceType();
        rsaContextId = esbContext.get(sds.smeCase.Offering_Project__c);
        producer = new CSIO_Producer(sds.brokerage, sds.producer, sds.getAgencyId(), sds.caseId);
        insuredOrPrincipal = new CSIO_InsuredOrPrincipal(sds.insured, sds.locationRisks[0], sds.quote);
        commlPolicy = new CSIO_CommlPolicy(sds.ri, sds.claims, sds.quote, sds.policyRisksByPackage.get(ri.pkgType));
        locations = new List<CSIO_Location>();
        for (Risk__c r : sds.locationRisks) {
            locations.add(new CSIO_Location(sds.insured, r));
        }
        commlSubLocations = new List<CSIO_CommlSubLocation>();
        for (Risk__c r : sds.locationRisks)
        {
            commlSubLocations.add(new CSIO_CommlSubLocation(r, soqlDataSet.questionAnswerMap.get(r.Id)));
        }
        commlProperyLineBusiness = new CSIO_CommlPropertyLineBusiness(sds.locationRisksByPackage.get(ri.pkgType));
        generalLiabilityLineBusiness = new CSIO_GeneralLiabilityLineBusiness(sds.liabilityRiskByPackage.get(ri.pkgType), sds.quote, sds.questionAnswerMap.get(sds.quote.Id)); 
    }
    
    protected String buildStandardRequestDocument(){
        xml = '';
        xml += '   <RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
        xml += '   <CommlPkgPolicyQuoteInqRq>';
        xml += '    <RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
        xml += '    <TransactionRequestDt>' + caseCreateDateLong + '</TransactionRequestDt>'; 
        xml += '    <TransactionEffectiveDt>' + caseCreateDateShort + '</TransactionEffectiveDt>';
        xml += '    <CurCd>CAD</CurCd>';
        xml += producer;
        xml += insuredOrPrincipal;
        if (ri.svcType == RequestInfo.ServiceType.GET_3_QUOTE){
            String flv = commlPolicy.toString();
            flv = XMLHelper.removeTags(flv, 'rsa:Flavour');
            xml += flv;
        }
        else {
        	xml += commlPolicy;
        }
        for (CSIO_Location location : this.locations){
            xml += location;
        }
        for (CSIO_CommlSubLocation commlSubLocation : commlSubLocations)
        {
            xml += commlSubLocation;
        }
        if (ri.svcType == RequestInfo.ServiceType.GET_3_QUOTE){
         String lag = commlProperyLineBusiness.toString();
                    lag = XMLHelper.removeTags(lag, 'Deductible');
                    xml += lag;
        }
        else {
        	xml += commlProperyLineBusiness;
        }
        // Remove limits and deductibles for R3Q
        if (ri.svcType == RequestInfo.ServiceType.GET_3_QUOTE)
        {
            String lag = generalLiabilityLineBusiness.toString();
            lag = XMLHelper.removeTags(lag, 'Limit');
            lag = XMLHelper.removeTags(lag, 'Deductible');
            xml += lag;
        }
        else {
            
            xml += this.generalLiabilityLineBusiness;
        }

        xml += '   </CommlPkgPolicyQuoteInqRq>';
       
            xml += ' <rsa:ServiceOperationType>' + rsaOperation + '</rsa:ServiceOperationType>';
            xml += ' <rsa:Context>' + rsaContextId + '</rsa:Context>';
            if (this.soqlDataSet.smeCase.Com__c != null){
                xml += ' <rsa:SessionId>' + this.soqlDataSet.sessionId + '</rsa:SessionId>';
            }
        
        return xml;
    }
}