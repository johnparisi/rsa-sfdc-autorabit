/**
This class decomposes the Addr element from the CSIO Policy response into its consistuent attributes.
*/

public with sharing class CSIOPolicyAddrXMLProcessor {
    
    public String streetAddress;
    public String streetNumber;
    public String streetName; 
    public String city;
    public String province;
    public String postalCode;
    public String country;
    
    
    /**
    This method takes in an xmlNode and populates the five address attributes
    @param xmlNode The xmlNode containing the Add element
    */
    
    public void process(Dom.XmlNode xmlNode) {
        city = xmlNode.getChildElement('City', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlNode.getChildElement('City', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
        
        postalCode = xmlNode.getChildElement('PostalCode', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlNode.getChildElement('PostalCode', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
        //UNTIL EPOLICY DATA IS FIXED
        if (postalCode == 'PostalCode'){
            postalCode = 'A0A0A0';
        }
        Map<String, String> countryName = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbCountry');
        String countryCd = xmlNode.getChildElement('CountryCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlNode.getChildElement('CountryCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
        country = countryName.get(countryCd);
        
        Map<String, String> provinceName = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Province__c');
        String provinceCd = xmlNode.getChildElement('StateProvCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlNode.getChildElement('StateProvCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
        province = provinceName.get(provinceCd);
        if(xmlNode.getChildElement('DetailAddr', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null){
           streetNumber = xmlNode.getChildElement('DetailAddr',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('StreetNumber', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlNode.getChildElement('DetailAddr',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('StreetNumber', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
           streetName = xmlNode.getChildElement('DetailAddr',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('StreetName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? xmlNode.getChildElement('DetailAddr',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('StreetName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText(): null;
            streetAddress = streetNumber + ' ' + streetName;
        }
    }
    
}