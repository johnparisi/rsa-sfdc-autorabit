/**
Carries the response object for the Abort transaction.
@author Tim Hopkins Test123
*/

global with sharing class AbortResponse {
	
	public boolean success;
    public List<String> errorMessages = new List<String>();
    
    
    
}