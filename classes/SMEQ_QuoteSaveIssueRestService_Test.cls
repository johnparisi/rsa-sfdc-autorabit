@isTest
private class SMEQ_QuoteSaveIssueRestService_Test
{
    //--------------------------------------------------------------------
    // Test constants for issue tests
    //--------------------------------------------------------------------
    public static String ISSUE_QUOTE_TEST_POLICY_NAME = 'TEST POLICY NAME';
    public static String ISSUE_QUOTE_TEST_POLICY_PAYMENT_PLAN = 'Direct Bill';
    public static String ISSUE_QUOTE_TEST_POLICY_BUSINESS_DESCRIPTION = 'TEST POLICY_BUSINESS_DESCRIPTION';
    public static String ISSUE_QUOTE_TEST_POLICY_NOTES = 'TEST POLICY NOTES';
    public static Boolean ISSUE_QUOTE_TEST_IS_ADDITIONAL_INSURED = false;
    public static Boolean ISSUE_QUOTE_TEST_IS_LOSS_PAYEE = false;

    /**
     * Invalid Request Parameters provided
     * 1. Assertion that ERROR condtion is detected by REST service (exception handled)
     */
    @isTest
    static void testUpdateIssueQuoteWithException()
    {
        SMEQ_QuoteRequest quoteRequest = new SMEQ_QuoteRequest();
        SMEQ_QuoteSaveIssueRestService.ResponseObjectWrapper response = SMEQ_QuoteSaveIssueRestService.saveIssueQuote(quoteRequest);
        System.assertNotEquals(null, response);
        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, response.responseCode);
    }

    /**
     * Method to test that Quote is updated successfully with a populated quote Issue request
     *
     * Assertions:
     * 1.Issued quote. Issued_Date__c has not been updated
     */
    @isTest
    static void testUpdateQuoteNoIssue()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_QuoteDao quoteDao = new SMEQ_QuoteDao();
            SMEQ_QuoteRequest quoteRequest = populateQuoteRequestForInsertingData();
            quoteDao.insertOrUpdateAccount(quoteRequest);
            String caseID = quoteDao.insertOrUpdateCase(quoteRequest, quoteRequest.insuredClientID);
            quoteDao.insertOrUpdateQuote(quoteRequest, caseID);
            // Assert values before update
            Quote__c quote = [
                SELECT id, Name_Insured__c, Effective_Date__c, Policy_Payment_Plan_Type__c,
                Policy_Business_Description__c, Additional_Insured__c,
                Loss_Payee__c, Policy_Notes__c, Issued_Date__c
                FROM Quote__c
                WHERE id = :quoteRequest.quoteID
            ];
            
            // Create Request with attributes
            quoteRequest.nameInsured = ISSUE_QUOTE_TEST_POLICY_NAME;
            Date testEffectiveDate = Date.newInstance(System.Today().year() - 1, 11, 25);
            DateTime effDateTime = dateTime.newInstance(testEffectiveDate, Time.newInstance(0, 0, 0, 0));
            quoteRequest.policyEffectiveDate = effDateTime.format('yyyy-MM-dd', 'America/New_York');
            quoteRequest.policyPaymentplan = ISSUE_QUOTE_TEST_POLICY_PAYMENT_PLAN;
            quoteRequest.policyBusinessDescription = ISSUE_QUOTE_TEST_POLICY_BUSINESS_DESCRIPTION;
            quoteRequest.policyNotes = ISSUE_QUOTE_TEST_POLICY_NOTES;
            quoteRequest.isAdditionalInsured = ISSUE_QUOTE_TEST_IS_ADDITIONAL_INSURED;
            quoteRequest.isLossPayee = ISSUE_QUOTE_TEST_IS_LOSS_PAYEE;
            SMEQ_QuoteSaveIssueRestService.ResponseObjectWrapper response = SMEQ_QuoteSaveIssueRestService.saveIssueQuote(quoteRequest);
            SMEQ_QuoteRequest responseQR = response.getPayload().quoteRequest;
            // Issued date should be updated
            System.assertEquals(null, responseQR.issuedDate);
            System.assertNotEquals(null, response);
            System.assertNotEquals(null, response.getPayload());
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, response.getResponseCode());
        }
    }
    
    static public SMEQ_QuoteRequest populateQuoteRequestForInsertingData()
    {
    	
    	// SIC_Code__c sicCodeObject = SMEQ_TestDataGenerator.getSicCode();

        SIC_Code__c sicCodeObject = SMEQ_TestDataGenerator.getSicCode();
        SMEQ_TestDataGenerator.generateSICQuestions(SMEQ_TestDataGenerator.getSICCodeDetailVersion());

        SMEQ_QuoteRequest quoteRequest = new SMEQ_QuoteRequest();
        SMEQ_BusinessDetailsModel businessDetails = new SMEQ_BusinessDetailsModel();
        businessDetails.businessName = 'Simple Business';
        SMEQ_AddressModel address = new SMEQ_AddressModel();
        address.addressLine1 = 'Address Line 1';
        address.city = 'Ottawa';
        address.postalCode = 'K2G 6R4';
        address.province = 'Ontario';
        businessDetails.mailingAddress = address;
        businessDetails.noOfClaims = 6;
        businessDetails.yearBusinessStarted = 2007;
        businessDetails.sicCode = sicCodeObject.SIC_Code__c;
        quoteRequest.businessDetails = businessDetails;
        quoteRequest.firstLocationAddrSameAsBusAddr = true;
        quoteRequest.performedDandBSearch = true;
        quoteRequest.hasDAndBMatch = true;

        quoteRequest.businessDetails.DUNSNumber = '1234567';
        
        List<SMEQ_QuestionAnswerModel> questionAnswersList = new List<SMEQ_QuestionAnswerModel>();
        SMEQ_QuestionAnswerModel cnRevenue = new SMEQ_QuestionAnswerModel();
        cnRevenue.questionCode = SicQuestionService.CODE_CANADIAN_REVENUE;
        cnRevenue.answer = '1000';
        questionAnswersList.add(cnRevenue);
        businessDetails.questionAnswers = questionAnswersList;

        SMEQ_BusinessDetailsModel.LiabilityRisk liabilityRisk = new SMEQ_BusinessDetailsModel.LiabilityRisk();
        SMEQ_CoverageDetailsModel liabilityCoverage = new SMEQ_CoverageDetailsModel();
        liabilityCoverage.limitValue = 10000;
        liabilityCoverage.deductibleValue = 100;
        List<SMEQ_CoverageDetailsModel> liabilityCoverageList = new List<SMEQ_CoverageDetailsModel>();
        liabilityCoverageList.add(liabilityCoverage);
        liabilityRisk.coverages = liabilityCoverageList;

        List<SMEQ_LocationDetailsModel> locationRisks = new List<SMEQ_LocationDetailsModel>();
        locationRisks.add(buildLocationDetailsModel(1));
        locationRisks.add(buildLocationDetailsModel(2));
        quoteRequest.businessDetails.locationRisks = locationRisks;

        List<SMEQ_ClaimDetailsModel> claims = new List<SMEQ_ClaimDetailsModel>();
        claims.add(buildClaimDetailsModel());
        claims.add(buildClaimDetailsModel());
        claims.add(buildClaimDetailsModel());
        quoteRequest.businessDetails.claims = claims;
        return quoteRequest;
    }

    private static SMEQ_ClaimDetailsModel buildClaimDetailsModel()
    {
        SMEQ_ClaimDetailsModel claimsDetail = new SMEQ_ClaimDetailsModel();
        claimsDetail.amount = 100.50;
        claimsDetail.month = '11';
        claimsDetail.year = System.Today().year() - 4;
        return claimsDetail;
    }

    public static SMEQ_LocationDetailsModel buildLocationDetailsModel(Integer index)
    {
        SMEQ_LocationDetailsModel locationDetailsModel = new SMEQ_LocationDetailsModel();
        locationDetailsModel.buildingValue = 100.50;
        locationDetailsModel.constructionType = '1';
        locationDetailsModel.hasSprinklerCoverage = true;
        locationDetailsModel.distanceToFireHydrant = 150;
        locationDetailsModel.distanceToFireStation = 5;
        locationDetailsModel.equipmentValue = 350.50;
        locationDetailsModel.isBuildingRenovated = true;
        locationDetailsModel.numberOfStories = 6;
        locationDetailsModel.stockValue = 450.50;
        locationDetailsModel.totalOccupiedArea = 1000;
        locationDetailsModel.yearBuilt = 1970;
        locationDetailsModel.electricalRenovatedYear = 2007;
        locationDetailsModel.roofingRenovatedYear = 2007;
        locationDetailsModel.plumbingRenovatedYear = 2007;
        locationDetailsModel.heatingRenovatedYear = 2007;
        SMEQ_AddressModel address = new SMEQ_AddressModel();
        address.addressLine1 = 'Test Line 1';
        address.addressLine2 = 'Test Line 2';
        address.city = 'Ottawa';
        address.province = 'Ontario';
        address.postalCode = 'L9T6S7';
        locationDetailsModel.address = address;
        return locationDetailsModel;
    }
}