public without sharing class SMEQ_QuoteSearchDao
{

    private static final String ABOUT_THE_BUSINESS = 'About the Business';
    private static final String CUSTOMIZE_QUOTE = 'Customize Quote';

    private static final String FINALIZED = 'Finalized';
    private static final String BOUND = 'Bind';
    private static final String BUY = 'Buy';
    private static final String CLOSED= 'Policy Issued – Closed';
    private static final String EXPIRED = 'Expired';
    private static final String HANDOFF = 'Handoff';
    private static final  Map<String, String> statusMap = new Map<String, String>();
	private static final String NEW_STATUS = 'New';
    private static final String ABORTED = 'Aborted';
    private static final String DECLINED = 'Declined';


    private static final String CI_OPEN_SPRNT = 'CI_OPEN_SPRNT';
    private static final String CI_CLOSED_SPRNT = 'CI_CLOSED_SPRNT';
    public SMEQ_QuoteSearchDao(){
        statusMap.put(ABOUT_THE_BUSINESS, '1');
        statusMap.put(CUSTOMIZE_QUOTE, '2');
        statusMap.put(FINALIZED, '3');
        statusMap.put(BOUND, '4');
        statusMap.put(BUY, '5');
        statusMap.put(CLOSED, '5');
        statusMap.put(EXPIRED, '6');
        statusMap.put(HANDOFF, '7');
        statusMap.put(ABORTED, '7');
        statusMap.put(DECLINED, '7');
    }
    /**
     * Searching case and quote table base on incoming search term
     * @param  SMEQ_SavePageStateRequestModel request
     * @return SMEQ_SavePageStateResponseModel 
     */
    @TestVisible 
    public SMEQ_QuoteSearchResponseModel searchQuote(SMEQ_QuoteSearchRequestModel request)
    {
        SMEQ_QuoteSearchResponseModel response = new SMEQ_QuoteSearchResponseModel ();
        List<SMEQ_QuoteSearchResponseModel.SearchResult> searchResults = new List<SMEQ_QuoteSearchResponseModel.SearchResult> ();
        System.debug('Quote request for search' + request);
            try{
                
                    if(request.searchText!=null){
                                        String newSearchText = '%'+request.searchText+'%';
                                        String brokerageAccountId = request.brokerID;
                                        List <Quote__c> quotes = [SELECT id,
                                              Quote_application_handed_over_to_UW__c,Case__r.caseNumber,
                                              Status__c,
                                              Broker_Application_Stage__c,
                                              CreatedDate,
                                                Case__r.bkrCase_Insured_Client__r.Name,
                                              Case__c, Quote_Number__c,
                                              Quote_Expiry_Date__c 
                                            FROM Quote__c  WHERE 
                                              Status__c != null and Status__c != :NEW_STATUS and
                                              ( Case__r.RecordType.DeveloperName = :CI_OPEN_SPRNT or Case__r.RecordType.DeveloperName = :CI_CLOSED_SPRNT ) and
                                              Case__r.Account.id = :brokerageAccountId and
                                            (Case__r.caseNumber like :newSearchText
                                            or Quote_Number__c like :newSearchText
                                            or Case__r.bkrCase_Insured_Client__r.Name like :newSearchText)
                                        ORDER BY CreatedDate DESC
                                        LIMIT 200];
                                        
                                            for(Quote__c quote : quotes){
                                                
                                                SMEQ_QuoteSearchResponseModel.SearchResult searchResult = new SMEQ_QuoteSearchResponseModel.SearchResult ();
                                                searchResult.businessName = quote.Case__r.bkrCase_Insured_Client__r.Name;
                                                searchResult.caseId = quote.Case__c;
                                                searchResult.caseNumber = quote.Case__r.caseNumber;
                                                searchResult.quoteNumber = quote.Quote_Number__c;
                                                searchResult.dateCreated = quote.CreatedDate.format('yyyy-MM-dd','America/New_York');
                                                searchResult.quoteExpiryDate = quote.Quote_Expiry_Date__c == null? null : datetime.newInstance(quote.Quote_Expiry_Date__c.year(), quote.Quote_Expiry_Date__c.month(),quote.Quote_Expiry_Date__c.day()).format('yyyy-MM-dd','America/New_York');
                                                searchResult.quoteID = quote.id;
                                                searchResult.quoteStatus = getQuoteStatus(quote);
                                                searchResults.add(searchResult);
                    
                                            }
                                            response.payload = searchResults;
                                            System.debug('Quote Search for search term ['+request.searchText+'] Found' + quotes.size() + 'result(s)');
                         }
            }catch(Exception e){
                 System.debug('SMEQ_QuoteSearchDao-searchQuote exception *******************'+e.getMessage());
                throw new SMEQ_ServiceException(e.getMessage(), e);
            } 

       

        return response;
    }
	/*Mapping broker status to underwriter status
     * @param  Quote__c quote
     * @return status in string
	*/
    @TestVisible 
    public static String getQuoteStatus(Quote__c quote)
    {
        System.debug('quote for status= ' + quote);
        System.debug('### status ' + quote.status__c);
        System.debug('### application stage ' + quote.Broker_Application_Stage__c);
        String status = null;
        if(quote.Quote_Expiry_Date__c != null && Date.today() > quote.Quote_Expiry_Date__c)
        {
            status = statusMap.get(EXPIRED);
        }
        else if(quote.Quote_application_handed_over_to_UW__c != null &&
                quote.Quote_application_handed_over_to_UW__c &&
                quote.Broker_Application_Stage__c != null)
        {
            System.debug('### HANDOFF');
            status = statusMap.get(HANDOFF);
        }
        else if(quote.Broker_Application_Stage__c != null &&
                quote.Broker_Application_Stage__c.equalsIgnoreCase(ABOUT_THE_BUSINESS))
        {
            status = statusMap.get(ABOUT_THE_BUSINESS);
        }
        else if(quote.Broker_Application_Stage__c != null &&
                quote.Broker_Application_Stage__c.equalsIgnoreCase(CUSTOMIZE_QUOTE))
        {
            status = statusMap.get(CUSTOMIZE_QUOTE);
        }
        else if(quote.Broker_Application_Stage__c == null &&
                quote.Status__c != null &&
                quote.Status__c.equalsIgnoreCase(FINALIZED))
        {
            status = statusMap.get(FINALIZED);
        }
        else if(quote.Broker_Application_Stage__c != null &&
                        quote.Status__c != null &&
                quote.Broker_Application_Stage__c.equalsIgnoreCase(BUY) &&
                quote.Status__c.equalsIgnoreCase(BOUND))
        {
            status = statusMap.get(BOUND);
        }
        else if(quote.Broker_Application_Stage__c != null &&
                quote.Status__c != null &&
                quote.Status__c.equalsIgnoreCase(BOUND))
        {
            status = statusMap.get(BOUND);
        }
        else if(quote.Broker_Application_Stage__c == null &&
                quote.Status__c != null &&
                quote.Status__c.equalsIgnoreCase(ABORTED))
        {
            status = statusMap.get(ABORTED);
        }
        else if(quote.Broker_Application_Stage__c == null &&
                quote.Status__c != null &&
                quote.Status__c.equalsIgnoreCase(DECLINED))
        {
            status = statusMap.get(DECLINED);
        }
        else if(quote.Broker_Application_Stage__c == null &&
                        quote.Status__c != null &&
                quote.Status__c.equalsIgnoreCase(CLOSED))
        {
            status = statusMap.get(CLOSED);
        }

        return status;

    }
   
}