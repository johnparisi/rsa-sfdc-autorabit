/**
 * Business Service to handle SIC data queries
 * Invoked by SMEQ_SICRestService
 *
 */
public with sharing class SMEQ_SICService {
    

    /**
     * Fetches the full list of SIC codes and their short descriptions only
     *
     * TODO - Add try catch for all queries
     * TODO change order by based on Language
     * shortDescription_en__c
     * @param language 'en' or 'fr'
     * @return List of SMEQ_SICDataModel, partially populated
     */
    public List<SMEQ_SICDataModel> getAllSICCodes(String targetLanguage) {
        String language;
        try {
            
            List<SIC_Code_Detail__c> sicCodeDetailList = [SELECT Id, SIC_Code__c, Short_Description_En__c, Short_Description_Fr__c FROM SIC_Code_Detail__c WHERE Application_Name__c = 'SME'];
            return convertSicCodeDetailListToSICDataModel(sicCodeDetailList);
        } catch (Exception se) {
            System.debug(LoggingLevel.ERROR,
                    'Unexpected processing exception invoking getAllSICCodes(' + language + '): '
                            + se.getMessage() + ','
                            + se.getCause() + ','
                            + 'line:' + se.getLineNumber()
                            + se.getStackTraceString());
            throw new SMEQ_ServiceException(se.getMessage(), se);
        }
    }


    public List<SMEQ_SICDataModel> convertSicCodeDetailListToSICDataModel(List<SIC_Code_Detail__c> sicCodeDetailList){
        List<SMEQ_SICDataModel> sicDataModelList = new List<SMEQ_SICDataModel>();
        if(sicCodeDetailList!=null){
            for(SIC_Code_Detail__c sicCodeDetail:sicCodeDetailList){
                SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
                sicDataModel.shortDescription_EN = sicCodeDetail.Short_Description_En__c;
                sicDataModel.shortDescription_FR = sicCodeDetail.Short_Description_Fr__c;
                sicDataModel.referral = 'N/A';
                sicDataModel.sicCode = sicCodeDetail.SIC_Code__c;
                sicDataModelList.add(sicDataModel);
            }
        }
        return sicDataModelList;
    }

    /**
    * Fetch full SIC data structure for a single SIC code
    * @param language 'en' or 'fr'
    * @param sicCode
    * @returns SMEQ_SICDataModel if found or null if no records found
    */
    public SMEQ_SICDataModel getSICDetail(String sicCode, String targetLanguage) {
       /* String language;
        try {
            if (targetLanguage == null) {
                language = 'en';
            } else {
                language = targetLanguage;
            }*/

            SMEQ_SICDataModel sicResult = new SMEQ_SICDataModel();
            return sicResult;
            /*List<SME_SIC__c> sicObjects;
            try {
                sicObjects = [SELECT Id, SICCode__c, shortDescription_en__c, shortDescription_fr__c, descriptionOperations_fr__c, descriptionOperations_en__c, referral__c FROM SME_SIC__c WHERE SICCode__c = :sicCode];
            } catch (QueryException qe) {
                // log me
                System.debug(LoggingLevel.ERROR, 'Unexpected error:' + qe);
                // rethrow  & rewrap exception to caller
            }

            // Handle if no records found
            if (sicObjects.size() != 1) {
                return null;
            }

            // otherwise continue using found SIC code
            SME_SIC__c sicObject = sicObjects[0];

            if (sicObject == null) {
                return null;
            }

            sicResult = translateSFtoSICDataModel(sicObject, language, false);

            return sicResult;
        } catch (Exception se) {
            System.debug(LoggingLevel.ERROR,
                    'Unexpected processing exception invoking getSICDetail(' + language + ',' + sicCode + '): '
                            + se.getMessage() + ','
                            + se.getCause() + ','
                            + 'line:' + se.getLineNumber()
                            + se.getStackTraceString());
            throw new SMEQ_ServiceException(se.getMessage(), se);
        }*/
    }

    // -----------------------------------------------------------\
    // HELPER METHODS
    // ------------------------------------------------------------

    // SIC conversion
    /*public static List<SMEQ_SICDataModel> translateSFtoDataModelSICList(List<SME_SIC__c> tempList, String language, Boolean retrieveListAttributesOnly) {
        List<SMEQ_SICDataModel> sicList = new List<SMEQ_SICDataModel>();
        for (SME_SIC__c nextSIC: tempList) {
            sicList.add(SMEQ_SICService.translateSFtoSICDataModel(nextSIC, language, retrieveListAttributesOnly));
        }
        return sicList;
    }*/

    /*private static SMEQ_SICDataModel translateSFtoSICDataModel(SME_SIC__c nextSIC, String language, Boolean retrieveListAttributesOnly) {
        SMEQ_SICDataModel modelSIC = new SMEQ_SICDataModel();
        modelSIC.sicCode = nextSIC.SICCode__c;
        modelSIC.shortDescription = (language == 'fr') ? nextSIC.shortDescription_fr__c : nextSIC.shortDescription_en__c;

        modelSIC.shortDescription_EN = nextSIC.shortDescription_en__c;
        modelSIC.shortDescription_FR = nextSIC.shortDescription_fr__c;

        if (!retrieveListAttributesOnly) {
            modelSIC.descriptionOperations = (language == 'fr') ? nextSIC.descriptionOperations_fr__c : nextSIC.descriptionOperations_en__c;
            modelSIC.descriptionOperations_FR = nextSIC.descriptionOperations_fr__c;
            modelSIC.descriptionOperations_EN = nextSIC.descriptionOperations_en__c;
            modelSIC.referral = nextSIC.referral__c;
        }
        return modelSIC;
    }*/
}