@isTest
public class SMEQ_SavePageStateDao_Test
{
    @testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    /**
     * Method to test savePageState request with valid quoteID and valid lastVistedPageNum is 
     * saved successfully
     * Assertions
     * 1. Assert Broker_Application_Stage__c is updated from value in the request that maps to correct Broker Application Stage
     * 2. Assert Status__c is updated from value in the request that maps to correct Status
     * 3. Assert Response message is set to success
     * 
     */
    @isTest
    static void testSaveLastVistedPageOnQuoteWithValidRequest()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
         
        System.runAs(u)
        {
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao();
            
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(c);
            
            reqModel  = buildMockSavePageStateRequest(aQuote.Id);
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            
            Quote__c updatedQuote = [SELECT Id, Broker_Application_Stage__c, Status__c FROM Quote__c WHERE ID=:aQuote.ID LIMIT 1];
            System.assert(updatedQuote.Broker_Application_Stage__c == 'About the Business');
            System.assert(updatedQuote.Status__c == 'In-Progress');
            //System.assert(respModel.responseMessage == 'SUCCESS');
        }
   	}
    
   	/**
     * Method to test savePageState request with valid quoteID and lastVistedPageNum is not saved
     * Assertions
     * 1. Assert response message is set to ERROR with invalid request
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithoutPageNumber()
    {
       User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
         
        System.runAs(u)
        {
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao();
            
            Case aCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(aCase);
            
            reqModel  = buildMockSavePageStateRequest(aQuote.Id);
            reqModel.lastVisitedPageNum = null;
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            System.assert(respModel.responseMessage == 'ERROR');
        }
   	}

    /**
     * Method to test savePageState request with valid quoteID and lastVistedPageNum is not saved
     * Assertions
     * 1. Assert response message is set to ERROR with invalid request
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithInvalidPageNumber()
    {
       User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
         
        System.runAs(u)
        {
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao();
            
            Case aCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(aCase);
            
            reqModel  = buildMockSavePageStateRequest(aQuote.Id);
            reqModel.lastVisitedPageNum = 9;
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            System.assert(respModel.responseMessage == 'ERROR');
        }
    }

   	/**
     * Method to test savePageState request with invalid quoteID and valid lastVistedPageNum is not saved
     * Assertions
     * 1. Assert response message is set to ERROR with invalid request
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithoutQuoteID()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
         
        System.runAs(u)
        {
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao(); 
            
            reqModel  = buildMockSavePageStateRequest(null);
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            System.assert(respModel.responseMessage == 'ERROR');
        }
   	}

    /**
     * Method to test savePageState request with invalid quoteID and valid lastVistedPageNum is not saved
     * Assertions
     * 1. Assert response message is set to ERROR with invalid request
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithoutNonExistantQuoteID()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao(); 
            
            reqModel  = buildMockSavePageStateRequest('1234');
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            System.assert(respModel.responseMessage == 'ERROR');
        }
    }

   	/**
     * Method to test savePageState request with invalid quoteID and invalid lastVistedPageNum is not saved
     * Assertions
     * 1. Assert response message is set to ERROR with invalid request
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithoutQuoteIDAndWithoutPageNumber()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao(); 
            
            reqModel  = buildMockSavePageStateRequest(null);
            reqModel.lastVisitedPageNum = null;
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            System.assert(respModel.responseMessage == 'ERROR');
        }
   	}

   	/**
     * Method to test savePageState request with valid quoteID and lastVistedPageNum is 
     * saved successfully
     * Assertions
     * 1. Assert Broker_Application_Stage__c value is updated with value in the request
     * 
     */
    @isTest 
    static void testSaveLastVistedPageOnQuoteWithUpdate()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {   
            SMEQ_SavePageStateRequestModel  reqModel  = new SMEQ_SavePageStateRequestModel();
            SMEQ_SavePageStateResponseModel respModel = new SMEQ_SavePageStateResponseModel();
            SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao();
            
            Case aCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(aCase);
            aQuote.Status__c = 'In-Progress';
            aQuote.Broker_Application_Stage__c = 'About the Business';
            update aQuote;
            
            reqModel  = buildMockSavePageStateRequest(aQuote.Id);
            reqModel.lastVisitedPageNum = 3;
            respModel = savePageDao.saveApplicationStageOnQuote(reqModel);
            
            Quote__c updatedQuote = [SELECT Broker_Application_Stage__c, Status__c FROM Quote__c WHERE ID=:aQuote.ID LIMIT 1];
            System.debug('Application stage *****' + updatedQuote.Broker_Application_Stage__c);
            System.assert(updatedQuote.Broker_Application_Stage__c == 'Customize Quote');
            //System.assert(respModel.responseMessage == 'SUCCESS');
        }
   	}

    @isTest
    /* Method to test getQuoteStatus returns correct repsonse for 'About the Business' param
    * 1.Assert status returned is 'In-Progress' when value is About the Business
    * 2.Assert status returned is 'In-Progress' when value is Customize Quote
    * 3.Assert status returned is 'In-Progress' when value is Getting Started
    * 4.Assert status returned is 'Finalized'   when value is Review
    * 5.Assert status returned is 'Bind'        when value is Buy
    * 6.Asssert status returned is 'New'        when value is Empty
    * 7.Asssert status returned is 'New'        for any other value
    */
    static void testGetQuoteStatus()
    {
       String Status = '';
       SMEQ_SavePageStateDao savePageDao = new SMEQ_SavePageStateDao();
       
       status = savePageDao.getQuoteStatus('About the Business');
       System.assert(status == 'In-Progress');

       status = savePageDao.getQuoteStatus('Customize Quote');
       System.assert(status == 'In-Progress');

       status = savePageDao.getQuoteStatus('Getting Started');
       System.assert(status == 'In-Progress');

       status = savePageDao.getQuoteStatus('Review');
       System.assert(status == 'Finalized');

       status = savePageDao.getQuoteStatus('Buy');
       System.assert(status == 'Bind');

       status = savePageDao.getQuoteStatus('');
       System.assert(status == 'New');

       status = savePageDao.getQuoteStatus('non-existant');
       System.assert(status == 'New');

       status = savePageDao.getQuoteStatus(null);
       System.assert(status == null);
     }


    //use to populate mock PageStateSaveRequest
    public static SMEQ_SavePageStateRequestModel buildMockSavePageStateRequest(String quoteID)
    {
        SMEQ_SavePageStateRequestModel mockRequest = new  SMEQ_SavePageStateRequestModel();
        mockRequest.quoteID = quoteID;
        mockRequest.lastVisitedPageNum = 2;
        return mockRequest;
    }
}