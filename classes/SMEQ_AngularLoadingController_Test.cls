@isTest
public class SMEQ_AngularLoadingController_Test{

    public static testmethod void doTest () {
       // Test.setCurrentPage(Page.ThePage);  // not really needed to test the component itself
           /**
    *This function tests SMEQ_AngularLoadingController, to make sure the controller can load files from static resource
    *Assertions:
    *1. Tests if static resource path can be set, true if setting works, false if setter fails
    *2. Tests if searching file name can be set, true if setting works, false if setter fails.
    *3. Tests if static resource zip file name can be set, true if setting works, false if setter fails.
    *4. Tests if static resource full path can be retrieved, true if getter works, false if getter fails.
    *5. Tests the scenario where a static resource not found, true if the scenario is handled, false not handled.
    *6. Tests if the program is able to retrieve the full URI, true if succeeded, false failed.
    **/
        SMEQ_AngularLoadingController cont = new SMEQ_AngularLoadingController();
        //cont.ngResourceName= 'SMEQ_Resource_Test';
        //cont.ngResourceFileName= 'test1';
        //cont.ngResourcePath ='/resource/';  // simulate the assignTo operation on the apex:attribute
        //System.assertEquals(cont.getNgResourcePath(), '/resource/');
        //System.assertEquals(cont.getNgResourceFileName(), 'test1');
        //System.assertEquals(cont.getNgResourceName(), 'SMEQ_Resource_Test');
           
        cont.setNgResourcePath('/resource/'); 
        String s = cont.getNgResourcePath();
        System.assertEquals('/resource/',s);
        
        cont.setNgResourceFileName('test1');
        s = cont.getNgResourceFileName();
        System.assertEquals('test1',s);
        
        cont.setNgResourceName('SMEQ_Resource_Test');
        s = cont.getNgResourceName();
        System.assertEquals('SMEQ_Resource_Test',s);   
        

        
        s = cont.getResourceURL('SMEQ_Angular','main');
        

        System.assert(s.contains('/resource/SMEQ_Angular/main')); 
        
        s = cont.getResourceURL('SMEQ_Resource_Test','fileNotFound');
        System.assertEquals('',s); 
        System.debug('s='+s);
        cont.setNgResourceFileName('main');
        cont.setNgResourceName('SMEQ_Angular');
        System.assert(cont.getMainJSPath().contains('/resource/SMEQ_Angular/main')); 
    
        
        
        
    }
}