public with sharing class AttachmentTriggerHandler {
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public static boolean firstRun = true;
    
    public AttachmentTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        
    /*public void OnBeforeInsert(Attachment[] newAttachments){
        //Example usage
        for(Attachment newAttachment : newAttachments){

        }
    }
    
    public void OnAfterInsert(Attachment[] newAttachments){


    }*/
    
    @future public static void OnAfterInsertAsync(Set<ID> newAttachmentIDs){
        //Example usage
        if (firstRun) {
            firstRun = false;
        }        
        AttachmentTriggerHandler handler = new AttachmentTriggerHandler(null, null);
		handler.copyAttachmentToEmail(newAttachmentIDs);	           
    }
    
    /*public void OnBeforeUpdate(Attachment[] oldAttachments, Attachment[] updatedAttachments, Map<ID, Attachment> AttachmentMap){
        //Example usage
        for (integer i=0;i<oldAttachments.size();i++){            
                updatedAttachments[i].addError('');
        }           
    }
    
    public void OnAfterUpdate(Attachment[] oldAttachments, Attachment[] updatedAttachments, Map<ID, Attachment> AttachmentMap){
        
        if (firstRun) {
            firstRun = false;
        }
        else {
            System.debug('Already ran!');
            return;
        }       
        
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedAttachmentIDs){
        AttachmentTriggerHandler handler = new AttachmentTriggerHandler(null, null);
		handler.copyAttachmentToEmail(updatedAttachmentIDs);
    }
    
    public void OnBeforeDelete(Attachment[] AttachmentsToDelete, Map<ID, Attachment> AttachmentMap){
        
    }
    
    public void OnAfterDelete(Attachment[] deletedAttachments, Map<ID, Attachment> AttachmentMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedAttachmentIDs){
        
    }
    
    public void OnUndelete(Attachment[] restoredAttachments){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }*/
    
    public void copyAttachmentToEmail(Set<ID> newAttachmentIDs){
        
        Map<ID,Attachment> mapParentIdAttachment = new Map<ID,Attachment>();
        Map<ID,ID> mapActivityIdEmailMessageId = new Map<ID,ID>();
        string strParentId;
        
        for(Attachment atts : [SELECT Body, ContentType, Name, Description, ParentId
            						FROM Attachment 
                               		WHERE Id IN :newAttachmentIDs]){
			strParentId = atts.ParentId;
			if(strParentId.substring(0,3) == '00T' )  // we only want Task attachments (from Outlook)                                      
				mapParentIdAttachment.put(atts.ParentId,atts);
		} 
        
        // Assume the TaskTriggerHandler already created an EmailMessage record (for CI-SME)
        // Query matching EmailMessage to clone the attachment
        for(EmailMessage e : [Select Id, ActivityId from EmailMessage where ActivityId in: mapParentIdAttachment.keyset()]){
            mapActivityIdEmailMessageId.put(e.ActivityId, e.Id);
        }
              
		List<Attachment> newAttachments = new List<Attachment>(); 
		Attachment a;
		for(ID attsId : mapParentIdAttachment.keyset()){
            if(mapActivityIdEmailMessageId.get(attsId) != null){
                a = mapParentIdAttachment.get(attsId).clone();
                a.ParentId = mapActivityIdEmailMessageId.get(attsId); // get the new emailId
                newAttachments.add(a);			            							
            }
		}   
        if(newAttachments.size() > 0)
	    	insert newAttachments;    
    }	    	
}