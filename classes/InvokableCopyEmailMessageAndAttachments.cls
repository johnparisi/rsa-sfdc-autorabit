global with sharing class InvokableCopyEmailMessageAndAttachments {

    public class InvokableCopyEmailMessageAndAttachmentsException extends Exception {}

    @InvocableMethod(label='Copy EmailMessages to a Case' description='Retrieves and copies the list of EmailMessages of a Case from one to another Case.')
    global static void InvokableCopyEmailMessageAndAttachments( List<CopyEmailRequest> lstRequests )
    {  

        Map<ID,EmailMessage> newEmailMessages = new Map<ID,EmailMessage>();
        Map<ID,ID> mapCopyOnlyLatest = new Map<ID,ID>();
		Map<ID,ID> mapOldIds = new Map<ID,ID>();
        EmailMessage e;
        Boolean copyOnlyLatest;
        
        // create a bulk query for all requests
        for(CopyEmailRequest request : lstRequests){        
        	mapOldIds.put(request.oldParentId, request.newParentId);
        	copyOnlyLatest = request.copyOnlyLatest;
        }
		Set<ID> setOldIds = mapOldIds.keySet();
		string querystr = 'SELECT FromAddress,FromName,HasAttachment,HtmlBody,TextBody,Id,Incoming,MessageDate,ParentId,ReplyToEmailMessageId,Status,Subject,ToAddress ' 
				+ 'FROM EmailMessage ' 
				+ 'WHERE ParentId in: setOldIds '
				+ 'AND Incoming = true '
				+ 'ORDER BY CreatedDate ' + (copyOnlyLatest ? 'DESC' : 'ASC');
        // query all the related email addresses
		for(EmailMessage ems : Database.query(querystr)
				/*
				// SFDC-13 changed to include all EmailMessages in order of received/sent
				*/				
				//LIMIT 1
				/* end SFDC-13 */
				){
					/*
					// SFDC-13 changed to include only latest received
					*/		                    
	                if(copyOnlyLatest==true && mapCopyOnlyLatest.containskey(ems.ParentId)){	                
	                    break;
					}

	                /* end SFDC-13 */					
	                e = ems.clone();
	                if(e.HtmlBody != null)
                        e.HtmlBody = e.HtmlBody.left(32000);
	                e.Status = '1'; // make it "READ" so we don't process it again as a new inbound handler
	                e.ParentId = mapOldIds.get(ems.ParentId);
					newEmailMessages.put(ems.Id,e);
					mapCopyOnlyLatest.put(ems.ParentId,null);
		}
        if(newEmailMessages.size() > 0)
	        try{
	            insert newEmailMessages.values();
	        }catch(exception ex){
	            throw new InvokableCopyEmailMessageAndAttachmentsException (ex.getMessage());
	        }		
	        
        List<Attachment> newAttachments = new List<Attachment>();        
        Attachment a;
        for(Attachment atts :
            [SELECT Body, ContentType, Name, Description, ParentId
            FROM Attachment
            WHERE ParentId in: newEmailMessages.keyset() ]){
                a = atts.clone();
                a.ParentId = newEmailMessages.get(atts.ParentId).Id; // get the new emailId //newEmailMessages[0].Id;
                newAttachments.add(a);
                //oldIds.add(atts); 
        }
        if(newAttachments.size() > 0)
	        try{
	            insert newAttachments;
	        }catch(exception ex){
	            throw new InvokableCopyEmailMessageAndAttachmentsException (ex.getMessage());
	        }
	   
    }    


	  global class CopyEmailRequest {
	    @InvocableVariable(required=true)
	    public string oldParentId;

	    @InvocableVariable(required=true)
	    public string newParentId;

	    @InvocableVariable(required=true)
	    public boolean copyOnlyLatest; 

	  }    
}