/**
  * @author        Saumil Bapat
  * @date          11/18/2016
  * @description   Test class for SLID_SetReferralCaseOwner
*/
@isTest
private Class SLID_SetReferralCaseOwner_Test {
    static testMethod void testSetReferralCaseOwnerWithLanId()
    {
        //Create User with LanId
        User adminUser = SLID_TestDataFactory.adminUser;
        adminUser.userName = 'TestAdmin3@Testing.com';
        adminUser.Email = 'TestAdmin@Testing.com';
        adminUser.alias = 'Test';
        insert adminUser;
        System.Debug('~~~adminUser: ' + adminUser);

        User adminUser2 = SLID_TestDataFactory.adminUser2;
        adminUser2.userName = 'TestAdmin2@Testing.com';
        adminUser2.Email = 'TestAdmin2@Testing.com';
        adminUser2.alias = 'Test2';
        insert adminUser2;
        System.Debug('~~~adminUser2: ' + adminUser2);
        
        System.runAs(adminUser2)
        {
          //Set Integration Log Id
          SLID_TestDataFactory.initializeIntegrationLogId();
        
          //Create testProduct 
          Product2 testProduct = SLID_TestDataFactory.returnTestProduct();
          insert testProduct;
          System.Debug('~~~testProduct: ' + testProduct);

          //Create a test Account
          Account testAccount = SLID_TestDataFactory.returnTestAccount();
          insert testAccount;
          System.Debug('~~~testAccount: ' + testAccount);

          //Create a test Contact
          Contact testContact = SLID_TestDataFactory.returnTestContact();
          testContact.AccountId = testAccount.Id;
          insert testContact;
          System.Debug('~~~testContact: ' + testContact);

          //Create a test parent Case
          Case parentCase = SLID_TestDataFactory.returnTestNewBusinessCase();
          parentCase.AccountId = testAccount.Id;
          parentCase.ContactId = testContact.Id;
          insert parentCase;
          System.Debug('~~~parentCase: ' + parentCase);
          
          //Create child Referral Case with LanId
          Case referralCaseWithLanId = SLID_TestDataFactory.returnTestReferralCase();
          referralCaseWithLanId.Parent_Case__c = parentCase.Id;
          referralCaseWithLanId.LanId__c = 'Test';
          insert referralCaseWithLanId;
          referralCaseWithLanId = [Select Id, LanId__c, OwnerId, AccountId, ContactId from Case where id = :referralCaseWithLanId.Id];
          System.Debug('~~~referralCaseWithLanId: ' + referralCaseWithLanId);
          System.Assert(referralCaseWithLanId.OwnerId == adminUser.Id, '~~~Test owner was not set properly with Lan Id, OwnerId: ' + referralCaseWithLanId.OwnerId);
        }
      }
        static testMethod void testSetReferralCaseOwnerWithoutLanId()
        {

            //Create User with LanId
            User adminUser = SLID_TestDataFactory.adminUser;
            adminUser.userName = 'TestAdmin4@Testing.com';
            adminUser.Email = 'TestAdmin@Testing.com';
            adminUser.alias = 'Test';
            insert adminUser;
            System.Debug('~~~adminUser: ' + adminUser);

            //Create User with LanId
            User adminUser2 = SLID_TestDataFactory.adminUser2;
            adminUser2.userName = 'TestAdmin2@Testing.com';
            adminUser2.Email = 'TestAdmin2@Testing.com';
            adminUser2.alias = 'Test2';
            insert adminUser2;
            System.Debug('~~~adminUser2: ' + adminUser2);

            System.runAs(adminUser2)
            {
              //Set Integration Log Id
              SLID_TestDataFactory.initializeIntegrationLogId();
              
              //Create testProduct 
              Product2 testProduct = SLID_TestDataFactory.returnTestProduct();
              insert testProduct;
              System.Debug('~~~testProduct: ' + testProduct);

              //Create a test Account
              Account testAccount = SLID_TestDataFactory.returnTestAccount();
              insert testAccount;
              System.Debug('~~~testAccount: ' + testAccount);

              //Create a test Contact
              Contact testContact = SLID_TestDataFactory.returnTestContact();
              testContact.AccountId = testAccount.Id;
              insert testContact;
              System.Debug('~~~testContact: ' + testContact);

              //Create a test parent Case
              Case parentCase = SLID_TestDataFactory.returnTestNewBusinessCase();
              parentCase.AccountId = testAccount.Id;
              parentCase.ContactId = testContact.Id;
              parentCase.bkrCase_Region__c = 'Ontario';
              parentCase.productId = testProduct.Id;
              parentCase.Level__c = '1';
              insert parentCase;
              System.Debug('~~~parentCase: ' + parentCase);

              //Get the 'CI Referral' RecordTypeId
              Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
              Id referralRecordTypeId = caseRecordTypes.get('CI Referral').getRecordTypeId();
    
              //Create Owner Key Value Mapping
              SLID_Mapping_KeyValue__c ownerMapping = new SLID_Mapping_KeyValue__c(
                                                              Type__c = 'Owner',
                                                              Name = 'Ontario,' + String.ValueOf(referralRecordTypeId).subString(0,14) + ',' + String.ValueOf(testProduct.Id).subString(0,14) + ',' + parentCase.Level__c,
                                                              Value__c = adminUser.Id
                                                            );
              insert ownerMapping;
              System.Debug('~~~ownerMapping: ' + ownerMapping);
                       
              //Create child Referral Case without LanId
              Case referralCaseWithoutLanId = SLID_TestDataFactory.returnTestReferralCase();
              referralCaseWithoutLanId.Parent_Case__c = parentCase.Id;
              referralCaseWithoutLanId.bkrCase_Region__c = 'Ontario';
              referralCaseWithoutLanId.productId = testProduct.Id;
              referralCaseWithoutLanId.Level__c = '1';
              insert referralCaseWithoutLanId;
              referralCaseWithoutLanId = [Select Id, OwnerId, AccountId, ContactId, bkrCase_Region__c, ProductId, Parent_Case__c from Case where Id = :referralCaseWithoutLanId.Id];
              System.Debug('~~~referralCaseWithoutLanId.bkrCase_Region__c: ' + referralCaseWithoutLanId.bkrCase_Region__c);
              System.Assert(referralCaseWithoutLanId.OwnerId == adminUser.Id, '~~~Test owner was not set properly with Mapping, ownerId: ' + referralCaseWithoutLanId.OwnerId);
            }
    }
}