/**
 * SMEQ_UWQuoteEdit
 * Author: James Lee
 * Date: December 30 2016
 * Feature: FP-748
 * 
 * The purpose of this extension class is to provide support for displaying and saving values for fields in a quote record.
 */
public with sharing class SMEQ_UWQuoteEdit
{
    public Case cse {get;set;}
    public Quote__c quote {get;set;}
    
    public List<SIC_Question__c> questions {get;set;}
    public Map<SIC_Question__c, SIC_Answer__c> fields {get;set;}
    public Map<SIC_Question__c, List<SelectOption>> picklistOptions {get;set;}
    public Map<SIC_Question__c, Integer> picklistSize  {get;set;}
    public Map<SIC_Question__c, List<String>> multipicklistValues {get;set;}
    
    public RecordType rt {get;set;}
    public Boolean hideBindSections {get;set;}
    
    public String errors {get;set;}
    public Boolean hideEdit {get;set;}
    
    /**
     * Constructor for quote edit.
     */
    public SMEQ_UWQuoteEdit(apexPages.standardController con)
    {
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        
    	this.quote = (Quote__c) con.getRecord();
        
        this.cse = [
            SELECT id, CaseNumber,
            Offering_Project__c, bkrCase_Region__c, bkrCase_SIC_Code__r.Sic_Code__c
            FROM Case
            WHERE id = :this.quote.Case__c
        ];
        
        this.rt = [
            SELECT id, Name
            FROM RecordType
            WHERE Id = :this.quote.RecordTypeId
        ];
        
        if (this.rt.Id == QuoteTypes.get(QuoteHandler.RECORDTYPE_CLOSED_QUOTE))
        {
            this.hideBindSections = true;
        }
        initialize();
    }
    
    /**
     * Constructor for quote insert.
     */
    public SMEQ_UWQuoteEdit(ApexPages.StandardSetController setCon)
    {
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        
        this.quote = new Quote__c();
        this.quote.RecordTypeId = QuoteTypes.get(QuoteHandler.RECORDTYPE_OPEN_QUOTE);
        this.quote.Case__c = ApexPages.currentPage().getParameters().get('id');
        
        this.cse = [
            SELECT id, CaseNumber,
            Offering_Project__c, bkrCase_Region__c, bkrCase_SIC_Code__r.Sic_Code__c, QUOTE_Count__c
            FROM Case
            WHERE id = :this.quote.Case__c
        ];
        
        if (this.cse.QUOTE_Count__c > 0)
        {
            this.hideEdit = true;
        }
        else
        {
            this.rt = [
                SELECT id, Name
                FROM RecordType
                WHERE Id = :this.quote.RecordTypeId
            ];
            
            initialize();
        }
    }
    
    private void initialize()
    {
        SIC_Code_Detail_Version__c scdv = retrieveQuoteSICCodeDetailVersion(this.cse);
        if(scdv != NULL ){
            this.questions = SicQuestionService.getAllApplicationQuestions(scdv, SicQuestionService.CATEGORY_QUOTE);
        }
        else{ // Else part is implemented only for Policy Works project where cases can come without SIC Code
            this.questions = getSICQuestionsWithoutSICCode(quote);
        }
        this.fields = initializeAnswerMapping(questions);
        this.picklistOptions = generatePicklistOptions(questions);
    }
    
    /**
     * FP-748
     * 
     * Retrieve a quote's case's SIC Code Detail Version using the SMEQ_SicCodeService.
     * 
     * @param q: The quote to retrieve it's case's SIC Code Detail Version record.
     * @return: The referenced SIC Code Detail Version record.
     */
    private SIC_Code_Detail_Version__c retrieveQuoteSICCodeDetailVersion(Case c)
    {
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicDetailsBySicProjectRegion(
            c.Offering_Project__c, 
            c.bkrCase_Region__c, 
            c.bkrCase_SIC_Code__r.Sic_Code__c
        );
        
        if (scdvs.size() > 0)
        {
        	return scdvs[0];
        }
        else
        {
            return null;
        }
    }
    
    /**
     * FP-748
     * 
     * Initialize missing answers for a given list of questions.
     * Questions mapped to null answers will be remapped to a default answer.
     * 
     * @param sqs: The list of SIC Code questions.
     * @return: The map from SIC Questions to SIC Answers
     */
    private Map<SIC_Question__c, SIC_Answer__c> initializeAnswerMapping(List<SIC_Question__c> sqs)
    {
        if (sqs == null || sqs.isEmpty())
        {
            return new Map<SIC_Question__c, SIC_Answer__c>();
        }
        
        Map<SIC_Question__c, SIC_Answer__c> fields = SICQuestionService.getSicAnswers(quote.Id, sqs);
        this.multipicklistValues = new Map<SIC_Question__c, List<String>>();
        
        for (SIC_Question__c sq : fields.keySet())
        {
            // Fix this - multiselect issue.
            if (sq.Input_Type__c == SICQuestionService.INPUT_TYPE_MULTI_SELECT_DROPDOWN)
            {
                SIC_Answer__c sa = fields.get(sq);
                
                if (sa.Long_Value__c != null && 
                    sa.Long_Value__c != '' && 
                    sa.Long_Value__c != SICQuestionService.EMPTY_MULTIPICKLIST_VALUE)
                {
                    this.multipicklistValues.put(sq, sa.Long_Value__c.split(';'));
                }
            }
        }
        
        return fields;
    }
    
    /**
     * FP-748
     * 
     * Generates a map for a list of questions for their available picklist options.
     * 
     * @param sqs: The list of SIC Code questions.
     * @return: The map from SIC Questions to lists of picklist options.
     */
    private Map<SIC_Question__c, List<SelectOption>> generatePicklistOptions(List<SIC_Question__c> sqs)
    {
        Map<SIC_Question__c, List<SelectOption>> picklistOptions = new Map<SIC_Question__c, List<SelectOption>>();
        Map<SIC_Question__c, Integer> picklistSize = new Map<SIC_Question__c, Integer>();
        
        for (SIC_Question__c sq : sqs)
        {
            if (sq.AvailableValuesEn__c != null)
            {
                List<SelectOption> sos = new List<SelectOption>();
                if (!sq.IsRequired__c) // Required value questions will not have a blank value as a choice.
                {
                    sos.add(new SelectOption('', '---')); // The initial base value.
                }
                
                for (String s : sq.AvailableValuesEn__c.split(';'))
                {
                    SelectOption so = new SelectOption(s, s);
                    sos.add(so);
                }
                
                picklistOptions.put(sq, sos);
                picklistSize.put(sq, sos.size());
            }
        }
        
        return picklistOptions;
    }
    
    /**
     * This helper function converts the default saved values from VFP 
     * and stores it in the more standard semi-comma delimited string.
     */
    private void updateMultiPicklistValues()
    {
        for (SIC_Question__c sq : fields.keySet())
        {
            if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_MULTI_SELECT_DROPDOWN)
            {
                SIC_Answer__c sa = fields.get(sq);
                sa.Long_Value__c = String.join(sa.Long_Value__c.substringBetween('[',']').split(', '), ';');
            }
        }
    }
    
    /**
     * FP-748
     * 
     * Validates and saves the quotes values and answers for dynamic questions.
     */
    public PageReference saveQuote()
    {
        updateMultiPicklistValues();
        ResultSet rs = SICQuestionService.validateSicAnswers(fields.values());
        
        this.errors = '';
        
        if (rs.getErrorCount() > 0)
        {
            Map<Id, String> errMap = new Map<Id, String>();
            
            Map<SIC_Answer__c, SIC_Question__c> sas2sqs = new Map<SIC_Answer__c, SIC_Question__c>();
            for (SIC_Question__c sq : fields.keySet())
            {
                sas2sqs.put(fields.get(sq), sq);
            }
            
            for (ProcessingResult pr : rs.getResults())
            {
                String key = pr.getKey();
                for (SIC_Answer__c sa : fields.values())
                {
                    if (key == SicQuestionService.getAnswerRecordKey(sa))
                    {
                        String errMsg = '';
                        for (ProcessingError pe : pr.getErrors())
                        {
                            if (errMsg != '')
                            {
                                errMsg += ', ';
                            }
                            errMsg += pe.getError();
                        }
                        errMap.put(sas2sqs.get(sa).Id, errMsg);
                    }
                }
            }
            this.errors = (String)JSON.serialize(errMap);
            
            return null;
        }
        
        upsert quote;
        
        for (SIC_Answer__c sa : fields.values())
        {
            if (sa.Quote__c == null)
            {
            	sa.Quote__c = quote.Id;
            }
        }
        
        SicQuestionService.saveSicAnswers(fields.values());
        
        PageReference redirectSuccess = new ApexPages.StandardController(quote).view();
        return redirectSuccess;
    }

    /*
    *   Defect No    : FP-6566
    *   Name         : cancelQuote
    *   @description : This method is for the new JS button for Quote Creation. This method is implemented for the Cancel button on
    *                  on page to function properly.
    *   @param       : NA
    *   @return      : NA
    */
    public PageReference cancelQuote(){
        return null;
    }

    /*
    *   Defect No   : RA-1322
    *   Name        : updateMailingAddress
    *   @description: This method would update the mailing address field on Quote record with the primary risk address details
    *                 if the checkbox Mailing_Address_Is_The_Same__c on quote record is checked else if it is uncheckd then
    *                 the previous values would again be populated back onto the mailing address specific fields on quote
    *   @param      : NA
    *   @return     : NA
    */
    public void updateMailingAddress(){
        if(quote.Mailing_Address_Is_The_Same__c){
            Risk__c objPrimaryRisk = new Risk__c();
            if(quote.Id != null){
                objPrimaryRisk = [SELECT Address_Line_1__c, Address_Line_2__c, City__c, Postal_Code__c, Province__c, Country__c
                                  FROM Risk__c
                                  WHERE Quote__c =: quote.Id
                                  AND Primary_Location__c = true
                                  LIMIT 1];
                if(objPrimaryRisk != null){
                    quote.Address_Line_1__c = objPrimaryRisk.Address_Line_1__c;
                    quote.Address_Line_2__c = objPrimaryRisk.Address_Line_2__c;
                    quote.City__c = objPrimaryRisk.City__c;
                    quote.Postal_Code__c = objPrimaryRisk.Postal_Code__c;
                    quote.State_Province__c = objPrimaryRisk.Province__c;
                    quote.Country__c = objPrimaryRisk.Country__c;
                }
            }
        }
    }

    /*
    *   Name        : getSICQuestionsWithoutSICCode
    *   @description: This method would return the list of questions associated with the Quote when the case has
    *                 No SIC Code associated with it. This method is implemented only for Policy Works implementation
    *                 since cases coming from Policy Works has max probability of of not having any SIC Code associated
    *   @param      : Quote__c objQuote - The associated Quote Object
    *   @return     : List<SIC_Question__c> - The list of SIC Question records associated with the Quote
    */
    public List<SIC_Question__c> getSICQuestionsWithoutSICCode(Quote__c objQuote){
        List<SIC_Question__c> listSicQuestions = new List<SIC_Question__c>();
        Set<Id> setSicQuestionId = new Set<Id>();
        List<SIC_Answer__c> listSicAnswers = new List<SIC_Answer__c>([SELECT Question_Code__c FROM SIC_Answer__c WHERE Quote__c =: objQuote.Id]);
        for(SIC_Answer__c tmp: listSicAnswers)
            setSicQuestionId.add(tmp.Question_Code__c);

        listSicQuestions = [SELECT Id, QuestionId__c, AvailableValuesEn__c, AvailableValuesFr__c, Category__c, Sub_Category__c, Input_Type__c, IsRequired__c, QuestionText_en__c,
                            QuestionText_fr__c, Question_Text__c,Xpath__c, Resource_Bundle_Label__r.string_id__c, Resource_Bundle_Place_Holder_Key__r.string_id__c, RecordtypeId,
                            RecordType.DeveloperName, Question_Code__c, FrontEndVisibility__c,Order__c, Minimum_Validation_Amount__c, Minimum_Validation_Required__c
                            FROM SIC_Question__c
                            WHERE Id IN: setSicQuestionId];

        return listSicQuestions;
    }
}