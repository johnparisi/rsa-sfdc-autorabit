global class SMEQ_DnBResponseModel extends SMEQ_RESTResponseModel
{
       public List<DnBEntry> payload {public get; public set;}

       public class DnBEntry {
       		public String streetAddress {public get; public set;}
       		public String city {public get; public set;}
       		public String province {public get; public set;}
       		public String postalCode {public get; public set;}
       		public String DUNSNumber {public get; public set;}
       		public String organizationName {public get; public set;}
       }

        //public List<DNBCallout.dnbEntry> payload {public get; public set;}
}