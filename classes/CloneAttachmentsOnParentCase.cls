public with sharing class CloneAttachmentsOnParentCase {
   
public static void cloneParentCaseAttachments(ID caseId , ID ParentCaseId){

      system.debug('caseID' + caseId);
      system.debug('Parent CAse Id '+ ParentCaseId);
      
      List<emailmessage> listemailmessage = new List<emailmessage>();
     Set<Id> EmailMessageId=new Set<ID>();
    if(ParentCaseId !=null)
    {
     listemailmessage=[Select id,Parentid from emailmessage where ParentId =:ParentCaseId and ParentId!=null];   
    }
    
    system.debug('Parent Clistemailmessage'+ listemailmessage);
    
        for(emailmessage emi:listemailmessage)
        {
            if(emi.ParentId!=null && emi.ParentId ==ParentCaseId)
            {
                EmailMessageId.add(emi.id);
            }
        }
      
     // Fetch all the attachments related to the child case
     List<Attachment> attachmentList = new List<Attachment>();
     List<Attachment> attachmentToBeCloned = new List<Attachment>();
     attachmentList = [Select Id, Name, ParentId, Body From Attachment Where ParentId =:ParentCaseId];
     
     if(EmailMessageId !=null)
    {
         attachmentList = [Select Id, Name, ParentId, Body From Attachment Where ParentId =:EmailMessageId];
    }
    
    system.debug('attachmentList-->'+ attachmentList);
     for(Attachment att : attachmentList){
         Attachment a = att.clone();
         system.debug('a-->'+ a);
        // if(att.ParentId == ParentCaseId){
            a.ParentId = caseId;
            attachmentToBeCloned.add(a); 
         //} 
    }
    system.debug('attachmentToBeCloned-->'+ attachmentToBeCloned);
    if(attachmentToBeCloned!=null && !attachmentToBeCloned.isEmpty()){
        system.debug('attachmentToBeCloned-->'+ attachmentToBeCloned);
        upsert attachmentToBeCloned;
    }    
}
}