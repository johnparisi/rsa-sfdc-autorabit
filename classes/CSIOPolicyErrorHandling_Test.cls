@isTest
public class CSIOPolicyErrorHandling_Test
{
   /* @testSetup static void testSetup() {
    RA_AbstractTransactionTest.setup();
  }*/

    public static testMethod void testRevertXML()
    {
         User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
         System.runAs(uw)
        {
            RA_AbstractTransactionTest.setup();
        String xml = '';
        
        xml += '<soap:Envelope>\n';
        xml += '</soap:Envelope>\n';
        xml += '--uuid:61fd7781-7413-428c-a8d2-891c258abc1c\n';
        xml += 'Content-Type: application/pdf\n';
        xml += 'Content-Transfer-Encoding: binary\n';
        xml += 'Content-ID: <Effective 21-10-2016 QPC0056011-01 10_30_40.819.pdf>\n';
        xml += 'fileName: Effective 21-10-2016 QPC0056011-01 10_30_40.819.pdf\n';
        xml += '\n';
        xml += EncodingUtil.base64Encode(Blob.valueOf('TESTTEST'));
        
        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(RA_AbstractTransactionTest.xmlPolicy);
       // CSIOPolicyErrorHandling processor = new CSIOPolicyErrorHandling();
         Dom.XMLNode soapNode = xmlDocument.getRootElement().getChildElement('Body', CSIOPolicyXMLProcessor.SOAP_NAMESPACE);
        Dom.XMLNode acordNode = soapNode.getChildElement('ACORD', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode insuranceSvcRsNode = acordNode.getChildElement('InsuranceSvcRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode policySyncRsNode = insuranceSvcRsNode.getChildElement('PolicySyncRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlPkgPolicyQuoteInqRs = policySyncRsNode.getChildElement('CommlPkgPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode locationNode = commlPkgPolicyQuoteInqRs.getChildElement('Location', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlSublocationNode = commlPkgPolicyQuoteInqRs.getChildElement('CommlSubLocation', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
       Case testCaseOne = new Case();
      //  processor.process();
        
        CSIOPolicyErrorHandling objCSIOPolicyErrorHandling = new CSIOPolicyErrorHandling(RA_AbstractTransactionTest.xmlPolicy);
        objCSIOPolicyErrorHandling.process();
        }
        
    }
    
     static testMethod void testProcessEmptyResponse()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
             RA_AbstractTransactionTest.setup();
           
            
            Test.startTest();
           
            
            try
            {
                 CSIOPolicyErrorHandling objCSIOPolicyErrorHandling = new CSIOPolicyErrorHandling(null);
                // objCSIOPolicyErrorHandling.process();
               
            }
            catch(RSA_ESBException ree)
            {
                
            }
            Test.stopTest();
        }
    }
    
  }