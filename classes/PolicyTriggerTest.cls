/**
* @File Name    :   PolicyTriggerTest
* @Description  :   Test class for PolicyTrigger
* @Date Created :   01/20/2016
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   testing class
* @Modification Log:
*****************************************************************
* Ver       Date        Author              Modification
* 1.0       01/20/2016  Habiba Zaman        Created the file/class
**/
@isTest
private class PolicyTriggerTest {
    

    // Test for inserting policy 
    @isTest
	static void TestBeforeInsert()
	{
        //custom setting
        
        // Create a Sample Client Account
        PolicyService pservice = new PolicyService();
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        brokerageAcc.bkrAccount_HuonBrokerNumber__c='001231242';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='test1', Email='test1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', FirstName='Test',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg1.com');
    
        insert u;

        //Policy to Client Mapping

        Policy_to_Client_Mapping__c pcm = new Policy_to_Client_Mapping__c();
        pcm.Policy_number__c = '000123123';
        pcm.Client_Account__c = clientAcc.Id;
        insert pcm;

        Policy_to_Client_Mapping__c pcm2 = new Policy_to_Client_Mapping__c();
        pcm.Policy_number__c = '000123122';
        pcm.Client_Account__c = clientAcc.Id;
        insert pcm2;

        // Policy Data
        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();


        Policy__c policy = new Policy__c();
        policy.Name ='000000023';
        policy.Client_Name__c = clientAcc.Name;
        policy.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy.Strategic_Segment__c = 'Contracting';
        policy.Region_Group__c = 'GRS';
        policy.Effective_Date__c=  Date.today().addDays(-30);
        policy.Term_Expiry_Date__c = Date.today().addDays(1);
        //policy.Product_Code__c='COM';
        policy.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy.bkrPolicy_Premium__c = 123.23;
        policy.Product__c = 'Property/Casualty';
        policy.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy.Control_Field__c = pservice.createPolicyControlField(policy.Name, policy.Effective_Date__c);

        Policy__c policy2 = new Policy__c();
        policy2.Name ='000000022';
        policy2.Client_Name__c = clientAcc.Name;
        policy2.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy2.Strategic_Segment__c = 'Construction';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Product_Code__c='COM';
        policy2.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c=  Date.today().addDays(-30);
        policy2.Term_Expiry_Date__c = Date.today().addDays(130);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy2.Control_Field__c = pservice.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        Test.startTest(); 
 

        PolicyService ps = new PolicyService();
        System.assertEquals(false, ps.isPolicyQualifiedForRenewal(policy));
        insert policy;
        insert policy2;
       
        System.assertEquals(false, ps.isPolicyQualifiedForRenewal(policy));
        System.assertEquals(true, ps.isPolicyQualifiedForRenewal(policy2));
        Policy__c newPolicy = [Select Policy_Stage__c,Qualified_Renewal__c from Policy__c where Id =:policy.Id];
        Policy__c newPolicy2 = [Select Policy_Stage__c,Qualified_Renewal__c from Policy__c where Id =:policy2.Id];
        
        System.assertEquals('Renewal Not Supported', newPolicy.Policy_Stage__c);
        System.assertEquals('Waiting for Next Renewal Cycle', newPolicy2.Policy_Stage__c);

        Test.stopTest();

    }
    //For testing before update
    @isTest
    static void TestBeforeUpdate()
    {
        //Custom setting
        insert new bkrMMGSL_CustomSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Auto_Renewal_ThresholdDays__c=12);

        // Create a Sample Client Account
        PolicyService pservice = new PolicyService();
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        brokerageAcc.bkrAccount_HuonBrokerNumber__c='001231242';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testu1', Email='testu1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', FirstName='Test',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg2.com');
    
        insert u;

        User u2 = new User(Alias='testu2', Email='testu2@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Tespting', LanguageLocaleKey='en_US', FirstName='Tepst',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testr1g4egas4qgj6kl8r@testorg3.com');
    
        insert u2;

        //Policy to Client Mapping


        Policy_to_Client_Mapping__c pcm2 = new Policy_to_Client_Mapping__c();
        pcm2.Policy_number__c = '0000000122';
        pcm2.Client_Account__c = clientAcc.Id;
        insert pcm2;

        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();

        // Policy Data

        Policy__c policy2 = new Policy__c();
        policy2.Name ='0000000122';
        policy2.Client_Name__c = clientAcc.Name;
        policy2.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy2.Master_Broker_Account__c = brokerageAcc.Id;
        policy2.Strategic_Segment__c = 'Construction';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Product_Code__c='COM';
        policy2.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c= Date.today().addDays(-300);
        policy2.Term_Expiry_Date__c = Date.today().addDays(130);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy2.Control_Field__c = pservice.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        Test.startTest();         

        bkrMMGSL_CustomSettings__c cs = bkrMMGSL_CustomSettings__c.getOrgDefaults();
        System.assertEquals(cs.Auto_Renewal_ThresholdDays__c,12);

        insert policy2;

        policy2.OwnerId = u2.Id;
        policy2.Force_Renewal__c = true;
        update policy2;

        Policy__c newPolicy2 = [Select Policy_Stage__c,Qualified_Renewal__c from Policy__c where Id =:policy2.Id];
        System.assertEquals(newPolicy2.Policy_Stage__c,'Renewal Opportunity Created');



        Test.stopTest();

    }

    


    //For update policy stage
    @isTest
    static void TestIsQualifiedForRenewal()
    {
        PolicyService ps = new PolicyService();
        // Policy Data

        Policy__c policy2 = new Policy__c();
        policy2.Name ='000123122';
        policy2.Client_Name__c = 'Test Client';
        policy2.Master_Broker__c = '002342342';
        policy2.Strategic_Segment__c = 'Construction';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Product_Code__c='COM';

        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c= Date.today().addDays(-200);
        policy2.Term_Expiry_Date__c = Date.today().addDays(100);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Control_Field__c = ps.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        Test.startTest();         

        Boolean isQualified = ps.isPolicyQualifiedForRenewal(policy2);
        System.assertEquals(isQualified,true);



        Test.stopTest();

    }

    //For update policy stage
    @isTest
    static void TestIsNotQualifiedForRenewal()
    {
        
        // Policy Data
        PolicyService ps = new PolicyService();

        Policy__c policy2 = new Policy__c();
        policy2.Name ='000123122';
        policy2.Client_Name__c = 'Test Client';
        policy2.Master_Broker__c = '002342342';
        policy2.Strategic_Segment__c = 'Construction';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c= Date.today().addDays(-200);
        policy2.Term_Expiry_Date__c = Date.today().addDays(100);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Control_Field__c = ps.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        Test.startTest();         

        Boolean isQualified = ps.isPolicyQualifiedForRenewal(policy2);
        System.assertEquals(isQualified,false);



        Test.stopTest();

    }

    //For Renewal threshold (-30, 120)

    @isTest
    static void TestIsExpiryDateWithinThreshold()
    {
        
        //
       // PolicyService ps = new PolicyService();

        // Date more than 30 days old
        Date oldExpiryDate = Date.today().addDays(-40);
        Date oldValidExpiryDate = Date.today().addDays(-20);
        Date withinThresholdDate = Date.today().addDays(5);
        Date outsideThresholdDate = Date.today().addDays(150);
        Test.startTest();         

        
        System.assertEquals(PolicyService.isExpiryDateWithinThreshold(oldExpiryDate),false);
        System.assertEquals(PolicyService.isExpiryDateWithinThreshold(oldValidExpiryDate),true);
        System.assertEquals(PolicyService.isExpiryDateWithinThreshold(withinThresholdDate),true);
        System.assertEquals(PolicyService.isExpiryDateWithinThreshold(outsideThresholdDate),false);





        Test.stopTest();

    }

    // For testing different policy Stage 
    @isTest
    static void TestPolicyStageUpdate()
    {
        
       //Custom setting
        insert new bkrMMGSL_CustomSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Auto_Renewal_ThresholdDays__c=12);

        // Create a Sample Client Account
        PolicyService pservice = new PolicyService();
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        brokerageAcc.bkrAccount_HuonBrokerNumber__c='001231242';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testu1', Email='testu1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', FirstName='Test',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg2.com');
    
        insert u;

       

        //Policy to Client Mapping


        Policy_to_Client_Mapping__c pcm2 = new Policy_to_Client_Mapping__c();
        pcm2.Policy_number__c = '0000000122';
        pcm2.Client_Account__c = clientAcc.Id;
        insert pcm2;

        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();

        // Policy Data

        Policy__c policy2 = new Policy__c();
        policy2.Name ='0000000122';
        policy2.Client_Name__c = clientAcc.Name;
        policy2.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy2.Master_Broker_Account__c = brokerageAcc.Id;
        policy2.Strategic_Segment__c = 'Construction';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Product_Code__c='COM';
        policy2.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c= Date.today().addDays(-300);
        policy2.Term_Expiry_Date__c = Date.today().addDays(100);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy2.Control_Field__c = pservice.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        Policy__c policy3 = new Policy__c();
        policy3.Name ='0000000123';
        policy3.Client_Name__c = clientAcc.Name;
        policy3.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy3.Master_Broker_Account__c = brokerageAcc.Id;
        policy3.Strategic_Segment__c = 'Construction';
        policy3.Region_Group__c = 'Atlantic';
        policy3.Product_Code__c='COM';
        policy3.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy3.Policy_Status__c='Future';
        policy3.Effective_Date__c= Date.today().addDays(-300);
        policy3.Term_Expiry_Date__c = Date.today().addDays(-32);
        policy3.bkrPolicy_Premium__c = 123.23;
        policy3.Product__c = 'Property/Casualty';
        policy3.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy3.Control_Field__c = pservice.createPolicyControlField(policy3.Name, policy3.Effective_Date__c);

        Test.startTest();        
        insert policy2;
        insert policy3;
        Policy__c p = [Select Policy_Stage__c from Policy__c where Id =:policy2.Id];
        System.assertEquals( p.Policy_Stage__c,'Waiting for Next Renewal Cycle');

        Policy__c p2 = [Select Policy_Stage__c from Policy__c where Id =:policy3.Id];
        System.assertEquals(p2.Policy_Stage__c,'Renewal Not Supported');
        p2.Force_Renewal__c = true;
        update p2;
        Policy__c p3 = [Select Policy_Stage__c from Policy__c where Id =:p2.Id];
        System.assertEquals(p3.Policy_Stage__c,'Renewal Opportunity Created');


        Test.stopTest();

    }


    
}