/**
 * ProcessingResult
 * Author: James Lee
 * Date: January 10 2017
 * Feature: FP-748
 * 
 * The purpose of this class is to store the validation results of a single record.
 */
public class ProcessingResult
{
    private String recordKey;
    
    private List<ProcessingError> errors;
    
    /**
     * Create a new Processing Result.
     * 
     * @param Id: The Id of the record being validated.
     */
    public ProcessingResult(String recordKey)
    {
        this.recordKey = recordKey;
        this.errors = new List<ProcessingError>();
    }
    
    /**
     * Add a new error to the list of errors.
     * 
     * @param pe: The ProcessingError object which stores the 
     */
    public void addError(ProcessingError pe)
    {
        this.errors.add(pe);
    }
    
    /**
     * Return the record ID being validated.
     */
    public String getKey()
    {
        return this.recordKey;
    }
    
    /**
     * Return the list of ProcessingErrors.
     */
    public List<ProcessingError> getErrors()
    {
        return this.errors;
    }
}