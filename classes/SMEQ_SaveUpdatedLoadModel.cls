global with sharing class SMEQ_SaveUpdatedLoadModel {
    
    public String caseId {get; set;}
    public String newEffectiveDate {get; set;}
    public String isRenewal {get; set;}
}