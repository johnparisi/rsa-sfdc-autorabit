public class ReassignCase_Controller {
    public Map<String,String> qName = new Map<String,String>();
    @AuraEnabled
    public static Map<id,String> getQueueNames(){
        Map<id,String> queueListOptions = new  Map<id,String>();
        // Case item = [Select Id, OwnerId from Case where Id =: caseId];
        for(QueueSobject q : [SELECT QueueId,Queue.DeveloperName,Queue.Name 
                              FROM QueueSobject
                              WHERE Queue.Email = 'a@a.com' // using the email field as the unique identifier for CI-SME cases
                              // also ensures that notificaiton emails don't get sent to users of queue, ever
                              and SobjectType = 'Case'
                              ORDER BY Queue.DeveloperName]){
                                  queueListOptions.put(q.QueueId,q.Queue.Name);
                                 
                              }
        // returnWrapper ret = new returnWrapper(item,queueListOptions);
        //return ret;   
        return queueListOptions;     
    }
    /* @AuraEnabled
public static Case getCase(String caseId, String QueueId){
Case c;
for(Case item : [Select Id, OwnerId from Case where Id =: caseId]){
c = item;
}  

c = new Case();
Case item = [Select Id, OwnerId from Case where Id =: caseId];



ID currentOwner = item.OwnerId;

if(queueId!=null && queueId!=''){
c.OwnerId = queueId;
try{
update c;
system.debug(c);
Case c1 = [Select Id, OwnerId from Case where Id =: c.Id];
if(c1.OwnerId != currentOwner){
//closePage = true;    
}
//refreshPage = true;           
}catch(exception e){
ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
}       
}
return c;
}*/
    @AuraEnabled
    public static  Boolean saveCase(Case caseToUpdate,String SelectedValue){
        system.debug('caseToUpdate '+caseToUpdate+''+SelectedValue);
        
        caseToUpdate.OwnerId = SelectedValue;
        try{
             update caseToUpdate;
            return true;
        }catch(exception e){
             return false;
        }
       
        
       
    }
    @AuraEnabled
    public static Case getCase(String caseId){
        Case c;
        for(Case item : [Select Id, Owner.name,OwnerId from Case where Id =: caseId]){
            c = item;
        }
        return c;
    }
    public class returnWrapper{
        public Case caseRec;
        public Map<id,String> queues;
        
        public returnWrapper(Case caseRec, Map<id,String> queues){
            this.caseRec=caseRec;
            this.queues=queues;
        }
    }
    
}