public class CSIO_GeneralLiabilityLineBusiness
{
	private Risk__c template;
    private List<Coverages__c> coverages;
    private Quote__c quote;
    private Map<String, String> sas;
    
    public String lobcd;
    public LiabilityInfo liabilityInfo;
    public List<CSIO_AdditionalInterest> additionalInterestList;
    
    private List<Contact> contactList = new List<Contact>();
    private List<Account> accountList = new List<Account>();
    private List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = new List<Quote_Risk_Relationship__c>();

    public CSIO_GeneralLiabilityLineBusiness(SOQLDataSet.Risk risk, Quote__c quote, Map<String, String> sas)
    {
        this.template = risk.risk;
        this.coverages = risk.coverages;
        this.quote = quote;
        this.sas = sas;
        convert();
    }
    
    public CSIO_GeneralLiabilityLineBusiness(SOQLDataSet.Risk risk, Quote__c quote, Map<String, String> sas, List<Account> accountList, List<Contact> contactList, List<Quote_Risk_Relationship__c> quoteRiskRelationshipList)
    {
        this.template = risk.risk;
        this.coverages = risk.coverages;
        this.quote = quote;
        this.sas = sas;
        this.accountList = accountList;
        this.contactList = contactList;
        this.quoteRiskRelationshipList = quoteRiskRelationshipList;
        convert();
    }
    
    public void convert()
    {
        this.lobcd = 'csio:2';
        
        this.liabilityInfo = new LiabilityInfo();
        
        Map<String, String> dynamicQuestionRevenue = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionRevenue');
        
        for (Coverages__c c : coverages)
        {
            this.liabilityInfo.commlCoverage = new CSIO_CommlCoverage(c);
        }
        
        for (String qCode : dynamicQuestionRevenue.keySet())
        {
            if (!sas.containsKey(qCode))
            {
                sas.put(qCode, '0');
            }
            this.liabilityInfo.generalLiabilityClassification.add(new CSIO_GeneralLiabilityClassification(sas, qCode));
        }
        
        if (this.quoteRiskRelationshipList != null)
        {
            this.additionalInterestList = new List<CSIO_AdditionalInterest>();
            for (Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationshipList)
            {
                System.assert(!(quoteRiskRelationship.Account__r.Id == null && quoteRiskRelationship.Contact__r.Id == null), 'Both contact and account should not be null');
                // find the related account or contact
                for (Account account : accountList)
                {
                    if (quoteRiskRelationship.Account__r.Id == account.Id)
                    {
                        System.assert(quoteRiskRelationship.Account__r != null, 'Account should not be null');
                        additionalInterestList.add(new CSIO_AdditionalInterest(account, quoteRiskRelationship));
                        break;
                    }
                }
                // if we've got this far then its a contact
                for (Contact contact : contactList)
                {
                    if (quoteRiskRelationship.Contact__r.Id == contact.Id)
                    {
                        System.assert(quoteRiskRelationship.Contact__r != null, 'Contact should not be null');
                        System.assert(contact.LastName != null, 'Last Name should not be null');
                        //System.assert(contact.MailingStreet != null, 'Mailing street should not be null');
                        additionalInterestList.add(new CSIO_AdditionalInterest(contact, quoteRiskRelationship));
                    }
                }
            }
        }
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<GeneralLiabilityLineBusiness>';
        xml += '<LOBCd>' + this.lobcd + '</LOBCd>';
        if (additionalInterestList != null)
        {
            for (CSIO_AdditionalInterest additionalInterest : additionalInterestList)
            {
                xml += additionalInterest.toString();
            }
        }
        xml += this.liabilityInfo;
        xml += '</GeneralLiabilityLineBusiness>';
        
        return xml;
    }
    
    public class LiabilityInfo
    {
		public CSIO_CommlCoverage commlCoverage;
        public List<CSIO_GeneralLiabilityClassification> generalLiabilityClassification;
        
        public LiabilityInfo()
        {
			generalLiabilityClassification = new List<CSIO_GeneralLiabilityClassification>();
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<LiabilityInfo>';
            xml += this.commlCoverage;
            for (CSIO_GeneralLiabilityClassification glc : this.generalLiabilityClassification)
            {
                xml += glc;
            }
            xml += '</LiabilityInfo>';
            return xml;
        }
    }
    
}