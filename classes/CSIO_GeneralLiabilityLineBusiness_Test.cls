/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_GeneralLiabilityLineBusiness.
 */
@isTest
public class CSIO_GeneralLiabilityLineBusiness_Test
{
	static testMethod void testConvert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            String packageId = RiskService.PACKAGE_STANDARD;
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            SOQLDataSet.Risk rs = CSIO_CommlPropertyInfo_Test.getRisk(r, packageId);
                        
            Map<String, String> dynamicQuestionRevenue = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionRevenue');
            
            Map<String, String> sas = new Map<String, String>();
            sas.put(SicQuestionService.CODE_CANADIAN_REVENUE, '1000');
            sas.put(SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE, '4000');
            sas.put(SicQuestionService.CODE_FOOD_REVENUE, '5000');
            for (String qCode : dynamicQuestionRevenue.keySet())
            {
                if (!sas.containsKey(qCode))
                {
                    sas.put(qCode, '0');
                }
            }
            
            CSIO_GeneralLiabilityLineBusiness cglc = new CSIO_GeneralLiabilityLineBusiness(rs, q, sas);
            String output = '';
            output += cglc;
            
            System.assertEquals(
                '<GeneralLiabilityLineBusiness>' +
                '<LOBCd>csio:2</LOBCd>' + 
                '<LiabilityInfo>' +
                new CSIO_CommlCoverage(rs.coverages[rs.coverages.size()-1]) +
                new CSIO_GeneralLiabilityClassification(sas, SicQuestionService.CODE_CANADIAN_REVENUE) +
                new CSIO_GeneralLiabilityClassification(sas, SicQuestionService.CODE_US_REVENUE) +
                new CSIO_GeneralLiabilityClassification(sas, SicQuestionService.CODE_FOREIGN_REVENUE) +
                new CSIO_GeneralLiabilityClassification(sas, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE) +
                new CSIO_GeneralLiabilityClassification(sas, SicQuestionService.CODE_FOOD_REVENUE) +
                '</LiabilityInfo>' +
                '</GeneralLiabilityLineBusiness>',
                output
            );
        }
    }
}