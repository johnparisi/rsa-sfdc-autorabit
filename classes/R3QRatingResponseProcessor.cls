//This is for Prototype - R3Q rating response processor 
//Author: RSA Prototype dev Team 
//26-APR-2019
global class R3QRatingResponseProcessor implements vlocity_ins.VlocityOpenInterface2 {
    
    global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output1, Map<String,Object> options) {
        try{
            String productKey ='01tc0000006ucZIAAY',parentProdKey='01tc0000006ucZIAAY',productName = 'Standard';
            String productCode ='FL1';
           
            system.debug('!@methodName '+ methodName);
            system.debug('!inputs'+inputs);
            system.debug('!@input '+ inputs.get('input_1'));
            system.debug('!@options '+ options);
            system.debug('!@input key '+  JSON.serializePretty(inputs.get('input_1')));
            system.debug('R3QResponseJSON '+ inputs.get('R3QResponseJSON'));
           
            if(methodName == 'parseR3QResposne'){
                
            }
            
            List<vlocity_ins.PricingCalculationService.CalculationProcedureResults> calList = new  List<vlocity_ins.PricingCalculationService.CalculationProcedureResults>();
           
            if(inputs.size() > 0){
                for(String key : inputs.keySet()){
                    system.debug('!@input key '+  key); 
                    if(String.isNotBlank(key) && key.contains('input_')){
    
                        List<Object> prodConfigRatingInput = (List<Object>) inputs.get(key);
                        inputs.get(key);
                        
                        for(Object mapSet: prodConfigRatingInput){
                            Map<String,Object> prodConfigRatingInputMap = (Map<String,Object>) mapSet;
                            if(null != prodConfigRatingInputMap.get('productKey') || null != prodConfigRatingInputMap.get('parentProdKey')
                               || null != prodConfigRatingInputMap.get('productCode')){
                                   productKey = (String) prodConfigRatingInputMap.get('productKey');
                                   parentProdKey = (String) prodConfigRatingInputMap.get('parentProdKey');
                                   productCode = (String) prodConfigRatingInputMap.get('productCode');
                                   
                                   system.debug('!@productCode '+ productCode);
                               }
                        }
                        
                        if(null != inputs.get('R3QResponseJSON') ){
    
                            Map<String,Object> ePolicyQuotes = (Map<String,Object>)inputs.get('R3QResponseJSON');
    
                            for(String ePolicyQuote : ePolicyQuotes.keySet()){
    
                                List<Map<String,Object>> listR = (List<Map<String,Object>>)ePolicyQuotes.get(ePolicyQuote);

                                for(Map<String,Object> quoteMap :listR){
                                    Map<String,Object> outputKeyValues = new Map<String,Object>();
                             
                                    if( String.isNotBlank(productCode) && null != quoteMap.get('Option') 
                                       && ((String) quoteMap.get('Option')) == productCode){
                                           
                                           String premium = (String) quoteMap.get('premium');
                                           List<Map<String,Object>> outputRList = new List<Map<String,Object>>();
                                           outputKeyValues.put('ID',0);
                                           outputKeyValues.put('productKey',productKey);
                                           outputKeyValues.put('parentProdKey',parentProdKey);
                                           outputKeyValues.put('premiumBLDB',premium);
                                           outputRList.add(outputKeyValues);
                                           system.debug('outputRList::'+outputRList);
                                           vlocity_ins.PricingCalculationService.CalculationProcedureResults cal = new vlocity_ins.PricingCalculationService.CalculationProcedureResults(outputRList,null);
                                           calList.add(cal);    
                                           
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
            system.debug('calList::'+calList);
            output1.put('output', calList);
        }
        catch(exception e){
            system.debug('exception e:'+e.getMessage());
        }
        return null;
    }
    
    global Object parseR3QResposne(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        
        system.debug('!@methodName '+ methodName);
        system.debug('!@inputs '+ inputs.keySet());
        system.debug('!@inputs '+ inputs.values());
        system.debug('!@output '+ output);
        system.debug('!@options '+ options);
        
        return null;
    }
}