/**
 * Author: James Lee
 * Created At: November 2, 2016
 * 
 * Unit tests for CSIO_GeneralLiabilityClassification.
 */
@isTest
public class CSIO_GeneralLiabilityClassification_Test
{
    static testMethod void testConvert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            String qCode = '90006';
            String exposure = '10000';
            
            Map<String, String> sas = new Map<String, String>();
            sas.put(qCode, exposure);
            
            Map<String, String> esbPremiumBasisCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbPremiumBasisCd');
            Map<String, String> esbTerritoryCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbTerritoryCd');
            
            CSIO_GeneralLiabilityClassification cglc = new CSIO_GeneralLiabilityClassification(sas, qCode);
            String output = '';
            output += cglc;
            
            System.assertEquals(
                '<GeneralLiabilityClassification>' +
                (esbTerritoryCd.containsKey(qCode) ?
                 (' <TerritoryCd>' + esbTerritoryCd.get(qCode) + '</TerritoryCd>') : ''
                ) +
                '<ExposureInfo>' +
                ' <PremiumBasisCd>' + esbPremiumBasisCd.get(qCode) + '</PremiumBasisCd>' +
                ' <Exposure>' + exposure + '</Exposure>' +
                '</ExposureInfo>' +
                '</GeneralLiabilityClassification>',
                output
            );
        }
    }
    
    static testMethod void testConvertEmptyExposureValue()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            String qCode = '90006';
            String exposure = null;
            
            Map<String, String> sas = new Map<String, String>();
            sas.put(qCode, exposure);
            
            Map<String, String> esbPremiumBasisCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbPremiumBasisCd');
            Map<String, String> esbTerritoryCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbTerritoryCd');
            
            CSIO_GeneralLiabilityClassification cglc = new CSIO_GeneralLiabilityClassification(sas, qCode);
            String output = '';
            output += cglc;
            
            System.assertEquals(
                '<GeneralLiabilityClassification>' +
                (esbTerritoryCd.containsKey(qCode) ?
                 (' <TerritoryCd>' + esbTerritoryCd.get(qCode) + '</TerritoryCd>') : ''
                ) +
                '<ExposureInfo>' +
                ' <PremiumBasisCd>' + esbPremiumBasisCd.get(qCode) + '</PremiumBasisCd>' +
                ' <Exposure>0</Exposure>' +
                '</ExposureInfo>' +
                '</GeneralLiabilityClassification>',
                output
            );
        }
    }
}