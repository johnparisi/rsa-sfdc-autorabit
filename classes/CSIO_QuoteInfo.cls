/**
Renders out the QuoteInfo element for the Bind transaction XML.
*/

public with sharing class CSIO_QuoteInfo
{
	public String ePolicyQuoteNumber;
	
	public CSIO_QuoteInfo(String quoteNumber)
    {
		this.ePolicyQuoteNumber = quoteNumber;
		convert();
	}
	
	private void convert()
    {
		// Nothing to do yet.
	}
	
	public override String toString()
    {
		String xml = '';
		if (ePolicyQuoteNumber != null)
        {
            xml += '<QuoteInfo>';
            xml += ' <CompanysQuoteNumber>' + this.ePolicyQuoteNumber + '</CompanysQuoteNumber>';
            xml += '</QuoteInfo>';
		}
        
        return xml;
	}
	
    
}