@isTest (SeeAllData=true)
private class test_bkrMMGSL_StartingPointController {
    
    public static testMethod void testMyController() {
        ForecastingType Ftype =[select id from ForecastingType where ForecastingType.IsActive=True limit 1];
        
        // Create an Account
        Account testAcct = new Account (Name = 'My Test Account');
        insert testAcct;
        
        // Create a Contact 
        Contact cnt= new Contact();
        cnt.Lastname = 'test';
        cnt.Accountid=testAcct.id;
        
        // Create a New Business Opportunity 
        Opportunity oppt = new Opportunity(Name ='New mAWS Deal',
                                           AccountID = testAcct.ID,
                                           StageName = 'Customer Won',
                                           Amount = 3000,
                                           Type='New Business',
                                           CloseDate = System.today());
        insert oppt;
        
        // Create a Renewal Opportunity 
        Opportunity rnwl = new Opportunity( Name = 'mAWS Usage',
                                                AccountId = testAcct.Id,
                                                StageName = 'Negotiations',
                                                CloseDate = System.today(),
                                                Type = 'Rewenal',
                                                Amount = 555888555);
        insert  rnwl;
        
        // Create an Opportunity Task 
        Task tsk = new Task(Subject='Something Important to Do',
                            Status = 'Open',
                            ActivityDate = System.today(),
                            Type = 'Follow-up Task or Deliverable',
                            WhatId = oppt.Id);
        
        // Create A bkrBrokerageSnapshot__c object
        bkrBrokerageSnapshot__c bks= new bkrBrokerageSnapshot__c();
        bks.Name='test';
        bks.bkrControl_Region__c='Ontario';
        bks.bkrControl_KAM__c='Jay Szpala';
        bks.bkrControl_ExternalId__c='123456';
        bks.bkrMMGSL_CYTD_SubmissionsReceived__c=1000;
        bks.bkrMMGSL_CYTD_NewPremiumBound__c=1000;
        insert bks;
        
        // Create a Case
        Case c = new Case();
        c.Contactid=cnt.id;
        insert c;
        
        // Create a Forecast Quota 
        ForecastingQuota fcq = new ForecastingQuota();
        fcq.StartDate= System.Today();
        fcq.ForecastingTypeid=Ftype.id;
        //insert fcq;
        
        // Run Tests
        list<string> str = new list<string>();
        str.add('Ontario');
        
        PageReference pageRef = Page.bkrMMGSL_StartingPoint;
        Test.setCurrentPage(pageRef);
        bkrMMGSL_StartingPointController controller = new bkrMMGSL_StartingPointController();
        ApexPages.currentPage().getParameters().put('accountName','null12');
        ApexPages.currentPage().getParameters().put('region','null12');
        ApexPages.currentPage().getParameters().put('kamName','null12');
        
        controller.runTradingDeskSearch();
        List<String> testPropertyCall = controller.regions ;
        List<String> testPropertyCall1= controller.kamNames;
        
        controller.toggleSort();
        controller.toggleTdSortOpps();
        controller.toggleTdSortCases();
        controller.toggleTaskSort();
        controller.runSearch();
        controller.runKamSearch();
        controller.getSumQuota();
        controller.getSumClosedOpps();
    }
}