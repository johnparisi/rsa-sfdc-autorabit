/**
* @File Name    :   BatchBrokerUserCreateSchedule
* @Description  :   Schedule Class for BatchBrokerUserCreate [FP-4996]
* @Date Created :   08/08/2017
* @Author       :   Habiba Zaman @ Deloitte [hzaman@deloitte.ca]
* @group        :   Schedule
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       08/08/2017  Habiba Zaman  	Created the file/class
*/

global class BatchBrokerUserCreateSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		BatchBrokerUserCreate b = new BatchBrokerUserCreate();
		database.executebatch(b);
	}
}