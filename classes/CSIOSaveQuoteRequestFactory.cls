/**
 * CSIOSaveQuoteRequestFactory
 * Author: Jess Asuncion
 * Date: October 2, 2017
 * 
 * The purpose of this class is to generate the xml Request for the SAVE service 
 */
public class CSIOSaveQuoteRequestFactory extends CSIOBindSaveQuoteRequestFactory {
    
    public CSIOSaveQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds,ri);
	}
}