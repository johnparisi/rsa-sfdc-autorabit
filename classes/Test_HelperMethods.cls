public with sharing class Test_HelperMethods {

	public static User createUser(String fname, String lname, String profileId){ 

        return new User(
                            FirstName = fname,
                            LastName = lname,
                            Alias = fname+'_'+lname,
                            Email = fname+'.'+lname+'@rsa.ca',
                            Username = fname+'.'+lname+'@rsa.ca'+Test_HelperMethods.generateRandomString(7),
                            CommunityNickname = fname+'_'+lname,
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            isActive=true,
                            EmailEncodingKey = 'UTF-8',
                            ProfileId = profileId);
    }

    public static User createUser(String fname, String lname, String profileId, String language){ 

        User u = createUser(fname,lname,profileId);
        u.LocaleSidKey = language;
        u.LanguageLocaleKey = language;
        return u;
    }


    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }	  
}