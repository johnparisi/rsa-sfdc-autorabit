global class SMEQ_QuoteSearchResponseModel extends SMEQ_RESTResponseModel
{
  public List<SearchResult> payload {public get; public set;}

       public class SearchResult{
           public String caseId{public get; public set;}
           public String quoteNumber{public get; public set;}
           public String businessName {public get; public set;}
           public String dateCreated{public get; public set;}
           public String quoteExpiryDate{public get; public set;}
           public String quoteStatus{public get; public set;}
           public String quoteID{public get; public set;}
           public String caseNumber{public get; public set;}
       }
       

}