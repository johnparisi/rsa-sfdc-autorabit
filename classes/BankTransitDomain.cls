/*
* Business logic implementation for the Bank Transit trigger.
*/

public with sharing class BankTransitDomain {
    
    
    public void afterInsert(List<Bank_Transit_Number__c> records)
    {   	
		// in this situation we simply need to create the new accounts
		Id fiRecordTypeId = getFIRecordTypeId() ;
    	List<Account> financialInstitutionAccountList = new List<Account>();
		for (Bank_Transit_Number__c bankTransit : records) {
			Account fiAccount = new Account();
			mapBankTransitToMap(bankTransit, fiAccount, fiRecordTypeId);
			financialInstitutionAccountList.add(fiAccount);
		}
		insert financialInstitutionAccountList;
    }
    
    public void afterUpdate(List<Bank_Transit_Number__c> records, Map<Id, Bank_Transit_Number__c> oldRecords)
    {

		// find the old related accounts and update them
		Id fiRecordTypeId = getFIRecordTypeId() ;
		List<Account> financialInstitutionAccountList = new List<Account>();
		List<Account> bankTransitAccountList = [SELECT Bank_Transit_Number__r.Id, Id, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Bank_Transit_Number__c FROM Account WHERE Bank_Transit_Number__r.Id IN :oldRecords.keySet()];
		
		// create a map of Bank Transit IDs to Accounts
		Map<Id, Account> bankTransitAccountMap = new Map<Id, Account>();
		for (Account myAccount : bankTransitAccountList) {
			bankTransitAccountMap.put(myAccount.Bank_Transit_Number__r.Id, myAccount);
		}
	

		for (Bank_Transit_Number__c bankTransit : records) {
			Account fiAccount = bankTransitAccountMap.get(bankTransit.id);
			mapBankTransitToMap(bankTransit, fiAccount, fiRecordTypeId);
			financialInstitutionAccountList.add(fiAccount);
		}		
		update financialInstitutionAccountList;
    }
    
    private Id getFIRecordTypeId() {
    	return [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' and DeveloperName = 'Financial_Institution'].Id;
    }
    
    
    private void mapBankTransitToMap(Bank_Transit_Number__c bankTransitNumber, Account account, Id fiRecordType) {
    	account.RecordTypeId = fiRecordType;
    	String bankAccountNameConcatentation = '';
    	if (bankTransitNumber.Address2__c != null) {
    		bankAccountNameConcatentation = bankTransitNumber.Financial_Institution_Name__c  + ' ' + bankTransitNumber.Address1__c + ' ' + bankTransitNumber.Address2__c;
    	}
    	else {
    		bankAccountNameConcatentation = bankTransitNumber.Financial_Institution_Name__c  + ' ' + bankTransitNumber.Address1__c;
    	}
    	account.Name = bankAccountNameConcatentation;
    	account.BillingStreet = bankTransitNumber.Address1__c;
    	if (bankTransitNumber.Address2__c != null) {
    		account.BillingStreet = bankTransitNumber.Address1__c + ' ' + bankTransitNumber.Address2__c;    		
    	}
    	account.BillingCity = bankTransitNumber.City__c;
    	account.BillingState = bankTransitNumber.Province__c;
    	account.BillingPostalCode = bankTransitNumber.Postal_Code__c;
    	account.Bank_Transit_Number__c = bankTransitNumber.Id;
    }
}