/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SLID_ESB_BrokerStageService_Test {

    static testMethod void testBrokerStageService() 
    {
        test.startTest();
        
        //Create test data for Broker Mapping custom setting
        SLID_Broker_Mapping_Settings__c brokerMappingSettings = new SLID_Broker_Mapping_Settings__c();
        brokerMappingSettings.Batch_Job_Admin_Email__c = 'echeong@salesforce.com';
        brokerMappingSettings.SLID_CreateBrokerMappingClassName__c= 'SLID_CreateBrokerMapping';
        brokerMappingSettings.SLID_PurgeBrokerMappingClassName__c= 'SLID_PurgeBrokerMapping';
        brokerMappingSettings.SLID_PurgeBrokerStageClassName__c= 'SLID_PurgeBrokerStage';
        brokerMappingSettings.SLID_UpdateBrokerFieldsFromMIClassName__c = 'SLID_UpdateBrokerFieldsFromMI';
        insert brokerMappingSettings;
        
        SLID_ESB_BrokerStageService.ServiceResponse serviceResponse = SLID_ESB_BrokerStageService.executeBrokerStageBatchProcess();
        system.debug(serviceResponse);
        
        SLID_ESB_BrokerStageService.IS_TEST = true;
        
        SLID_ESB_BrokerStageService.ServiceResponse negServResp = SLID_ESB_BrokerStageService.executeBrokerStageBatchProcess();
        system.debug('--NEG RESP--' + negServResp);
        
        SLID_ESB_BrokerStageService.fetchBrokerMappingClassIds();
        
        test.stopTest();
        
    }
}