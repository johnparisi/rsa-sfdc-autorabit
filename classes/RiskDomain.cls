/**
* RiskDomain
* Author: James Lee
* Date: August 25 2016
* 
* The object domain is where the real work happens, this is where we will apply the actual business logic. 
* We have the main Object Domain which should handle all common processing. 
* Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
* Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
* just implement logic in the appopriate method. 
*/
public with sharing class RiskDomain
{
    private RiskService service = new RiskService();
    public static boolean suppressTrigger = false;
    
    public void beforeInsert(List<Risk__c> records)
    {
    }
    
    public void beforeUpdate(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
    }
    
    public void afterInsert(List<Risk__c> records)
    {
    }
    
    public void afterUpdate(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
    {
    }
    
    //Risk with Record Type of Open Risk or Close Risk
    public class SMEQDomain
    {
        private RiskService service = new RiskService();
        public void beforeInsert(List<Risk__c> records)
        {
            service.insertDefaultCoverages(records, null);
           
        }
        
        public void beforeUpdate(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
        {
            service.insertDefaultCoverages(records, oldRecords);
             system.debug('Before Insert');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        }
        
        public void afterInsert(List<Risk__c> records)
        {
            service.insertNewCoverages(records, null);
        }
        
        public void afterUpdate(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
        {
            service.removeUnselectedCoverages(records, oldRecords);
            system.debug('Before Insert');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
            service.insertNewCoverages(records, oldRecords);
            system.debug('Before Insert');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
            service.updateDefaultCoverageLimits(records, oldRecords);
            system.debug('Before Insert');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        }
    }
    //Risk with Record Type of Open Risk or Close Risk and are sitting on a Policy
    public class SMEPDomain
    {
        private RiskService service = new RiskService();
        
        public void afterUpdate(List<Risk__c> records, Map<Id, Risk__c> oldRecords)
        {
            service.removeUnselectedCoveragesFromPolicy(records, oldRecords);
            if (!suppressTrigger) { service.insertNewCoverages(records, oldRecords); }
            service.checkRiskReferralTrigger(records, oldRecords);
            service.updateDefaultCoverageLimits(records, oldRecords);
        }
    }
}