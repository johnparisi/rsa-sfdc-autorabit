public with sharing class CaseTriggerHandler {
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public static boolean firstRun = true;

    private List<Case> ciCases = new List<Case>();
    private List<Case> gslCases = new List<Case>();
    private List<Case> gslReferralCases = new List<Case>();
    private List<Case> smeqCases = new List<Case>();
    private List<Case> piCases = new List<Case>();
    

    public static boolean referralEmailFirstRun = true;

    public CaseTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void beforeInsert(List<Case> records){
        
        new CaseDomain().beforeInsert(records);

        initCases(records);

        if(ciCases.size() > 0){
            new CaseDomain.SMEDomain().beforeInsert(ciCases);
        }
        if(gslCases.size() > 0){
            new CaseDomain.GSLDomain().beforeInsert(gslCases);
        }
        if (smeqCases.size() > 0)
        {
            new CaseDomain.SMEQDomain().beforeInsert(smeqCases);
        }
        if(piCases.size() > 0){
            new CaseDomain.PIDomain().beforeInsert(piCases);
        }       
    }

    public void beforeUpdate(List<Case> records, Map<Id, Case> oldRecords){
        
        new CaseDomain().beforeUpdate(records, oldRecords);

        initCases(records);

        if(ciCases.size() > 0){
            new CaseDomain.SMEDomain().beforeUpdate(ciCases, oldRecords);
        }
        if(gslCases.size() > 0){
            new CaseDomain.GSLDomain().beforeUpdate(gslCases, oldRecords);
        }
        if (smeqCases.size() > 0)
        {
            new CaseDomain.SMEQDomain().beforeUpdate(smeqCases, oldRecords);
        }
    }

    //public void afterInsert(List<Case> records){
    //    new CaseDomain().afterInsert(records);

    //    initCases(records);

    //    if(ciCases.size() > 0){
    //        new CaseDomain.SMEDomain().afterInsert(ciCases);
    //    }
    //    if(gslCases.size() > 0){
    //        new CaseDomain.GSLDomain().afterInsert(gslCases);
    //    }
    //}
    public void afterUpdate(List<Case> records, Map<Id, Case> oldRecords)
    {
        //new CaseDomain().afterUpdate(records, oldRecords);

        initCases(records);
        
        if(ciCases.size() > 0){
            //new CaseDomain.SMEDomain().afterUpdate(ciCases, oldRecords);
        }
        if(gslCases.size() > 0){
            //new CaseDomain.GSLDomain().afterUpdate(gslCases, oldRecords);
        }

        if(gslReferralCases.size() > 0){
            new CaseDomain.GSLReferralDomain().afterUpdate(gslReferralCases, oldRecords);
        }
        if (smeqCases.size() > 0)
        {
            new CaseDomain.SMEQDomain().afterUpdate(smeqCases, oldRecords);
             System.debug('governnor Limit222:-');
             System.debug('AfterUpdate');  
System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        }
    }

    private void initCases(List<Case> records){
        String userPrefix = User.sObjectType.getDescribe().getKeyPrefix();
        Map<String,Id> caseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);
        Set<Id> ciRecordTypes = new Set<Id>();
        Set<Id> smeqRecordTypes = new Set<Id>();
        Set<Id> piRecordTypes = new Set<Id>();
        
        ciRecordTypes.add(caseTypes.get('CI_Loss_Control'));
        ciRecordTypes.add(caseTypes.get('CI_Mid_Term_Changes'));
        ciRecordTypes.add(caseTypes.get('CI_New_Business'));
        ciRecordTypes.add(caseTypes.get('CI_Portfolio'));
        ciRecordTypes.add(caseTypes.get('CI_Processing'));
        ciRecordTypes.add(caseTypes.get('CI_Retention'));
        ciRecordTypes.add(caseTypes.get('CI_Triage'));
       
        smeqRecordTypes.add(caseTypes.get('CI_Open_SPRNT'));
        
        piRecordTypes.add(caseTypes.get('PI_Underwriting'));
        piRecordTypes.add(caseTypes.get('PI_Pacific_Underwriting'));
        piRecordTypes.add(caseTypes.get('PI_FA_Underwriting'));

        for (Case c : records){
            if(ciRecordTypes.contains(c.RecordTypeId)){
                ciCases.add(c);
            }
            else if(c.RecordTypeId == caseTypes.get('bkrMidMarketGSL')){
                gslCases.add(c);
            }
            else if(c.RecordTypeId == caseTypes.get('Mid_Market_GSL_Referral')){
                gslReferralCases.add(c);
            }
            else if (smeqRecordTypes.contains(c.RecordTypeId))
            {
                smeqCases.add(c);
            }
            else if (piRecordTypes.contains(c.RecordTypeId))
            {
                piCases.add(c);
            }            
        }
    }

    public void afterInsert(List<Case> records){
        initCases(records);
        if(gslReferralCases.size() > 0){
        //myDMLLimit = Limits.getDMLStatements();
            new CaseDomain.GSLReferralDomain().afterInsert(gslReferralCases);
        
        System.debug('governnor AfterInsert-'); 
         
System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        }
    }
        
  //  public void OnBeforeInsert(Case[] newCases){
        //updateCaseFields(newCases, null);
  //  }
    
    /*public void OnAfterInsert(Case[] newCases){


    }
    
    @future public static void OnAfterInsertAsync(Set<ID> newCaseIDs){
        //Example usage
        if (firstRun) {
            firstRun = false;
        }        
        //CaseTriggerHandler handler = new CaseTriggerHandler(null, null);
        //handler.copyCaseToEmail(newCaseIDs);             
    }*/
    
  //  public void OnBeforeUpdate(Case[] oldCases, Case[] updatedCases, Map<ID, Case> CaseMap){
        //updateCaseFields(updatedCases, oldCases);      
  //  }
    
    public void OnAfterUpdate(Case[] oldCases, Case[] updatedCases, Map<ID, Case> CaseMap){
        
        if (firstRun) {
            firstRun = false;

            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            List<ID> lstCaseIds = new List<ID>();
            List<Case> lstCases = new List<Case>();
            
            for(Integer x = 0; x<updatedCases.size(); x++) {
                if(updatedCases[x].Status != oldCases[x].Status && updatedCases[x].Status == 'Bound'){
                    lstCaseIds.add(updatedCases[x].Id);
                }
            }
            for(Case rec : [Select Id from Case where Id in: lstCaseIds]){
                rec.setOptions(dmo);
                lstCases.add(rec);
            }                      
            if(lstCases.size() > 0)
            update lstCases;
       
        }
        else {
            System.debug('Already ran!');
            return;
        }       
        
    }
    
    /*@future public static void OnAfterUpdateAsync(Set<ID> updatedCaseIDs){
        //CaseTriggerHandler handler = new CaseTriggerHandler(null, null);
        //handler.copyCaseToEmail(updatedCaseIDs);
    }
    
    public void OnBeforeDelete(Case[] CasesToDelete, Map<ID, Case> CaseMap){
        
    }
    
    public void OnAfterDelete(Case[] deletedCases, Map<ID, Case> CaseMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedCaseIDs){
        
    }
    
    public void OnUndelete(Case[] restoredQuote_Versions){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }*/

    //public void updateCaseFields(Case[] newCases, Case[] oldCases){
        
    //    String userPrefix = User.sObjectType.getDescribe().getKeyPrefix();
    
    //    // ***
    //    // SFDC-19 - only apply the "In Progress" logic when OwnerId changes
    //    // ***          
    //    Case c; 
    
    //    for(Integer x = 0; x<newCases.size(); x++) {
    //        c = newCases[x];
            
    //        // copy Product so we can report on it using a custom field
    //        c.Product__c = c.ProductId;
            
    //        if(trigger.isupdate){
    //            String ownerId = String.valueOf(c.OwnerId);
                    
    //            if(ownerId != oldCases[x].OwnerId && ownerId.startsWithIgnoreCase(userPrefix)&&c.Status=='New') {
    //                c.Status='In Progress';
    //            }
    //            if(c.Status != oldCases[x].Status && !ownerId.startsWithIgnoreCase(userPrefix) && c.Status=='Closed') {
    //                c.OwnerId=userInfo.getUserId();
    //            }
    //        }
    //    }
    //}     
}