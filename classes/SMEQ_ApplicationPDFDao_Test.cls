@isTest
public class SMEQ_ApplicationPDFDao_Test
{
    /***
     * Method to test application valid quote is returned with valid reference number
     * Assertions
     * 1. Assert quote returned for PDF traversal is not null
     */ 
    @isTest
    static void testApplicationPDFDaoWithValidReferenceNumber()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            QuoteRequestWrapper testQuote = new QuoteRequestWrapper();
            SIC_Code_Detail_Version__c scdvs = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            Test.setCurrentPageReference(new PageReference('Page.SMEQ_ApplicationPDF'));
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(c);
            
            Map<String, Id> riskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
            
            List<Risk__c> listRisk = new List<Risk__c>();
            Risk__c riskLocation = new Risk__c(
                Quote__c = aQuote.Id,
                recordTypeId = riskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION),
                Province__c = 'Ontario',
                Coverage_Selected_Option_1__c = 'Building;Earthquake',
                Coverage_Selected_Option_2__c = 'Building;Equipment',
                Construction_Type__c = 'Fire Resistive');
            listRisk.add(riskLocation);
            insert listRisk;
            
            System.currentPageReference().getParameters().put('quoteId', aQuote.Id);
            
            SMEQ_ApplicationPDFDao applicationPDF = new SMEQ_ApplicationPDFDao();
            testQuote = applicationPDF.getDetailsForOtherMarketsPDF();
            
            System.assertNotEquals(null, testQuote);
            System.assertEquals(1, testQuote.locationRisks.size());
        }
    }
    
    /***
     * Method to test application valid quote is returned with invalid reference number
     * Assertions
     * 1. Assert quote returned for PDF traversal is null
     */ 
    @isTest
    static void testApplicationPDFDaoWithInValidReferenceNumber()
    {
        User u;
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            SMEQ_ApplicationPDFDao applicationPDF = new SMEQ_ApplicationPDFDao();
            QuoteRequestWrapper testQuote = new QuoteRequestWrapper();
            Test.setCurrentPageReference(new PageReference('Page.SMEQ_ApplicationPDF'));
            System.currentPageReference().getParameters().put('referencenumber', null);
            testQuote = applicationPDF.getDetailsForOtherMarketsPDF();
            System.assertEquals(null, testQuote); 
        }
    }
}