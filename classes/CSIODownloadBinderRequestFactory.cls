/**
 * CSIODownloadBinderRequestFactory
 * Author: Sophie Tran
 * Date: November 24, 2017
 * 
 * The purpose of this class is to generate the xml Request for the Download Binder service 
 */
public class CSIODownloadBinderRequestFactory extends CSIODownloadDocumentRequestFactory implements CSIORequestFactory {
   
    
    public CSIODownloadBinderRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds.requestingUser.FederationIdentifier, sds.quote.Business_Source_ID__c);
        this.soqlDataSet = sds; 
        this.ri = ri;
    }
    
    public String buildXMLRequest(){ 
        getHeaderXml();
        xml += CSIO420Header;
        if (this.soqlDataSet.quote.ePolicy__c == null) {
           throw new RSA_ESBException('Cannot download quote without COM#.');
        }
        xml += '   <InsuranceSvcRq>';
        xml += '    <RqUID>' + XMLHelper.encodeUID(this.soqlDataSet.quote.Id) + '</RqUID>';
        xml += '    <PolicySyncRq>';
        xml += '     <RqUID>' + XMLHelper.encodeUID(this.soqlDataSet.quote.Id) + '</RqUID>';
        xml += '     <TransactionRequestDt>' + caseCreateDateLong + '</TransactionRequestDt>';
        xml += '     <AsOfDt>' + todayDateShort + '</AsOfDt>';
        xml += '     <PolicyNumber>' + this.soqlDataSet.quote.ePolicy__c + '</PolicyNumber>';
        xml += '    </PolicySyncRq>';
        xml += '   </InsuranceSvcRq>';
        xml += CSIO420Footer;
        return xml;
    } 
}