global class M2VFileDownloadRemoteAction implements vlocity_ins.VlocityOpenInterface2{
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        System.debug('===M2VFileDownloadRemoteAction ' + methodName + ' input===' + input);
        if(methodName == 'getQuotePDF'){
            getQuotePDF(input,output);
        }else if(methodName == 'initQuoteDownloadPDFData'){
            initQuoteDownloadPDFData(input,output);
        }
        
        return true;
    }
    
    private static void initQuoteDownloadPDFData(Map<String, Object> input,Map<String, Object> output){
        System.debug('===initQuoteDownloadPDFData===running input===' + input);
        
        String quoteNumber = (String)input.get('quoteNumber');
        String quoteId = (String)input.get('quoteId');
        System.debug('===quoteNumber==' + quoteNumber);        
        
        String custPermId = (String)input.get('AgencyId');
        
        if(String.isEmpty(custPermId)){
            //if no contact user from case then get the current user FedId
            Map<String, Object> currentUser = (Map<String, Object>)input.get('user');
            custPermId = (String)currentUser.get('federationIdentifier');
        }
        
        String custLangPref = '';
        
        Map<String, Object> termAndConditionStep = (Map<String, Object>)input.get('TandCandLanguagestep');
        if(termAndConditionStep != null){
            custLangPref = (String)termAndConditionStep.get('selectPreferredLanguage');
        }
        if(String.isBlank(custLangPref)){
            custLangPref =UserService.getFormattedUserLanguage();
        }
        
        output.put('MsgRqUID', XMLHelper.encodeUID(quoteNumber));
        output.put('RqUID', XMLHelper.encodeUID(quoteId));
        output.put('OrgId',UserInfo.getOrganizationId());
        output.put('SPName','rsagroup.ca');
        output.put('CustLangPref',custLangPref);
        output.put('CustPermId',custPermId);
        
        System.debug('===initQuoteDownloadPDFData===output===' + output);
    }
    
    private static void getQuotePDF(Map<String, Object> input, Map<String, Object> output){
        String quoteId = (String)input.get('quoteId');    
        String body = (String)input.get('httpResponse');       
        
        String pdfString = body.substringAfter('</soap:Envelope>');
        List<String> pdfPieces = pdfString.split('\n');
        
        if (pdfPieces.size() == 1){
            //throw new RSA_ESBException('No PDF attached to response.');
        }else{
            String pdfBase64 = pdfPieces[7]; // Binary files are attached to SOAP in base64 encoding.
            String fileName = pdfPieces[5].substringAfter(': ');
            String contentType = pdfPieces[2].substringAfter(': ');
            
            String contentDocumentId  = M2VFileService.insertFile(fileName, quoteId, pdfBase64);
            if(String.isNotEmpty(contentDocumentId)){
                output.put('quotePDFDocumentId', contentDocumentId);
                String quotePDFDownloadURL = getCurrentBaseURL()
                    + '/sfc/servlet.shepherd/document/download/' + contentDocumentId + '?operationContext=S1';
                output.put('quotePDFDownloadURL',quotePDFDownloadURL);
            }
        }
    }
    
    private static String getCurrentBaseURL(){
        String result = '';
        String networkId = Network.getNetworkId();
        if(networkId == null){
            result= URL.getSalesforceBaseUrl().toExternalform();
        }else{
            Network net = [SELECT Id, Name,UrlPathPrefix FROM Network where id =: Network.getNetworkId() ];
            result= URL.getSalesforceBaseUrl().toExternalform() + '/' + net.UrlPathPrefix;
        }
        
        return  result;
    }
}