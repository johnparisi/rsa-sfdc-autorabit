public class CSIO_ItemIdInfo
{
    public String agencyId;
    
    public CSIO_ItemIdInfo(String agencyId)
    {
        this.agencyId = agencyId;
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<ItemIdInfo>';
        if (this.agencyId != null)
        	xml += ' <AgencyId>' + this.agencyId + '</AgencyId>';
        xml += '</ItemIdInfo>';
        
        return xml;
    }
}