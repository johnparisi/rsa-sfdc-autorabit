/**
* Implementation of a trigger handler for BigMachines__Quote__c objects.
* 
* The new Object Handler classes will follow this pattern.
* 
* It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
* Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
* 
* No need to update this unless adding a business unit or record types. 
*/

public with sharing class BigMachinesQuoteHandler {


    public void beforeInsert(List<BigMachines__Quote__c> records){
        new BigMachinesDomain().beforeInsert(records);
    }

    public void beforeUpdate(List<BigMachines__Quote__c> records, Map<Id, BigMachines__Quote__c> oldRecords){
         new BigMachinesDomain().beforeUpdate(records, oldRecords);
    }

    public void afterInsert(List<BigMachines__Quote__c> records){
        new BigMachinesDomain().afterInsert(records);
    }
    
    public void afterUpdate(List<BigMachines__Quote__c> records, Map<Id, BigMachines__Quote__c> oldRecords){
        new BigMachinesDomain().afterUpdate(records, oldRecords);
    }

}