public class CSIO_Location
{
    private Account insuredOrPrincipalAccount;
    private Risk__c risk;
    private List<Contact> contactList = new List<Contact>();
    private List<Account> accountList = new List<Account>();
    private List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = new List<Quote_Risk_Relationship__c>();
    
    public CSIO_ItemIdInfo itemIdInfo;
    public CSIO_Addr addr;
    public List<CSIO_AdditionalInterest> additionalInterestList;
    
    // constructor for creating a new location associated to a single principal or insured
    public CSIO_Location(Account acct, Risk__c risk)
    {
        this.insuredOrPrincipalAccount = acct;
        this.risk = risk;
        convert();
    }
    
    // constructor for adding multiple other parties to a location
    public CSIO_Location(Account insuredOrPrincipalAccount, Risk__c risk, List<Account> accountList, List<Contact> contactList, List<Quote_Risk_Relationship__c> quoteRiskRelationshipList) {
        this.insuredOrPrincipalAccount = insuredOrPrincipalAccount;
        this.risk = risk;
        this.accountList = accountList;
        this.contactList = contactList;
        this.quoteRiskRelationshipList = quoteRiskRelationshipList;
        // do a safety assertion. The relationship list ought to be the same size as the combination of accounts and contacts
        System.assert(accountList.size() + contactList.size() == quoteRiskRelationshipList.size(), 'There should be the same number of parties as relationships');
        // make sure that we've accounted for all the account and contact ids in the quoteRiskRelationships
        integer theCount = 0;
        for (Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationshipList) {
            for (Account account : accountList) {
                if (account.Id == quoteRiskRelationship.Account__r.Id) {
                    theCount ++;
                }
            }
        }
        for (Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationshipList) {
            for (Contact contact : contactList) {
                if (contact.Id == quoteRiskRelationship.Contact__r.Id) {
                    theCount ++;
                }
            }
        }
        System.assert(theCount == quoteRiskRelationshipList.size(), 'Could not find all the account / contacts in the qrr list, ' + quoteRiskRelationshipList.size() + ' vs ' + theCount );
        convert();
    }
    
   public void convert()
    {
        this.additionalInterestList = new List<CSIO_AdditionalInterest>();
        this.itemIdInfo = new CSIO_ItemIdInfo((String) insuredOrPrincipalAccount.Id); //Fix this
        this.addr = new CSIO_Addr(risk);
        boolean found = false;
        for (Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationshipList ) {
            System.assert(!(quoteRiskRelationship.Account__r.Id == null && quoteRiskRelationship.Contact__r.Id == null), 'Both contact and account should not be null');
            // find the related account or contact
            for (Account account : accountList) {
                if (quoteRiskRelationship.Account__r.Id == account.Id) {
                    System.assert(quoteRiskRelationship.Account__r != null, 'Account should not be null');
                    additionalInterestList.add(new CSIO_AdditionalInterest(account, quoteRiskRelationship));
                    found = true;
                    break;
                }
            }
            // if we've got this far then its a contact
            for (Contact contact : contactList) {
                if (quoteRiskRelationship.Contact__r.Id == contact.Id) {
                    System.assert(quoteRiskRelationship.Contact__r != null, 'Contact should not be null');
                    System.assert(contact.LastName != null, 'Last Name should not be null');
                    //System.assert(contact.MailingStreet != null, 'Mailing street should not be null');
                    additionalInterestList.add(new CSIO_AdditionalInterest(contact, quoteRiskRelationship));
                    found = true;
                }
            }
        }
    }
    
   public override String toString()
    {
        String xml = '';
        
        xml += '<Location id="' + this.risk.Id + '">';
        xml += this.itemIdInfo;
        xml += this.addr;
        if (additionalInterestList != null) {
            for (CSIO_AdditionalInterest additionalInterest : additionalInterestList) {
                xml += additionalInterest.toString();
            }
        }
        if (this.risk.Deleted__c == true){
            xml += '<rsa:Deleted>' + 1 + '</rsa:Deleted>';
        }
        else {
            xml+= '<rsa:Deleted>' + 0 + '</rsa:Deleted>';
        }
        xml += '</Location>';
        
        return xml;
    }
    
}