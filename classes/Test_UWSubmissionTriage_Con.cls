/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(Seealldata = true)
private class Test_UWSubmissionTriage_Con {

    static testMethod void testInvokableUpdateCaseDMOAssignmentRules() 
    {
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;
			// insert user as member of CI Triage
        	insert new GroupMember( GroupId = '00Go0000001qIMT', UserOrGroupId = u.Id );  
            
        	Case objCase;
            Case objCase1;

            System.runAs(u) {  
                // put both cases in SAME queue
                objCase = new Case(OwnerId = u.Id, bkrCase_Subm_Type__c = 'test', RecordTypeId='012o0000000C00J', Status = 'New', Priority = 'Medium', Origin = 'Phone', Subject = 'Test');
    
                insert objCase;        
                Test.startTest();

                	objCase.bkrCase_Subm_Type__c = null;
                	update objCase;
                    List<InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest> lstReq = new List<InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest>();
                    InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest req = new InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest();
                    req.caseId = String.valueOf(objCase.Id);
                    req.useAsyncCallout = false;
                    req.recordTypeLabel = 'CI Triage';
                    lstReq.add(req);                

                	InvokableUpdateCaseDMOAssignmentRules.InvokableUpdateCaseDMOAssignmentRules(lstReq);
          
                Test.stopTest(); 
                
            }
			system.assert([SELECT Id, OwnerId FROM Case where Id =: objCase.Id].OwnerId != u.Id);
            
    }

    static testMethod void testInvokableUpdateCaseDMOAssignmentRulesAsync() 
    {
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;
			// insert user as member of CI Triage
        	insert new GroupMember( GroupId = '00Go0000001qIMT', UserOrGroupId = u.Id );  
            
        	Case objCase;
            Case objCase1;

            System.runAs(u) {  
                // put both cases in SAME queue
                objCase = new Case(OwnerId = u.Id, bkrCase_Subm_Type__c = 'test', RecordTypeId='012o0000000C00J', Status = 'New', Priority = 'Medium', Origin = 'Phone', Subject = 'Test');
    
                insert objCase;        
                Test.startTest();

					system.assert([SELECT Id, OwnerId FROM Case where Id =: objCase.Id].OwnerId == u.Id);
                	objCase.bkrCase_Subm_Type__c = null;
                	update objCase;
                    List<InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest> lstReq = new List<InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest>();
                    InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest req = new InvokableUpdateCaseDMOAssignmentRules.UpdateCaseRequest();
                    req.caseId = String.valueOf(objCase.Id);
                    req.useAsyncCallout = true;
                    req.recordTypeLabel = 'CI Triage';
                    lstReq.add(req);                

                	InvokableUpdateCaseDMOAssignmentRules.InvokableUpdateCaseDMOAssignmentRules(lstReq);
          
                Test.stopTest(); 
                
            }
			system.assert([SELECT Id, OwnerId FROM Case where Id =: objCase.Id].OwnerId != u.Id);            
    }
    
    	static testMethod void mybkrCISME_MergeCase_Test() {
        // TO DO: implement unit test
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;
			        
            Case objCase;
            Case objCase1;

            System.runAs(u) {  
                // put both cases in SAME queue
                objCase = new Case(RecordTypeId='012o0000000xKh4', Status = 'New', Priority = 'Medium', Origin = 'Phone', Subject = 'Test');
    
                insert objCase;
                // assign a skill the user doesn't have
                objCase.bkrCase_Subm_Type__c = 'Cancellation';                
                update objCase;
                
                //assign this other case to OTHER queue
                objCase1 = new Case(RecordTypeId='012o0000000xKh4', Status = 'Waiting for Information', Priority = 'Medium', Origin = 'Phone', Subject = 'Test');
        
                insert objCase1;
                
                Test.startTest(); 
                Test.setCurrentPage(Page.CI_MergeCase);
                
                bkrCISME_MergeCase obj = new bkrCISME_MergeCase();        

                obj.sourceCaseId = '';
                obj.destinationCaseId = '';
                obj.validateCaseIds();
                
                system.assertequals(obj.sourceCase, new Case());
                system.assertequals(obj.destinationCase, new Case());
                
                obj.sourceCaseId = objCase.Id;
                obj.destinationCaseId = objCase1.Id;
                obj.sourceCaseTabId = 'TAB-1';
                obj.destinationCaseTabId = 'TAB-2';
                obj.validateCaseIds();
                
                system.assertequals(obj.sourceCase.ParentId, objCase.Id);
                system.assertequals(obj.destinationCase.ParentId, objCase1.Id);
                system.assertequals(obj.sourceCaseId, objCase.Id);
                system.assertequals(obj.destinationCaseId, objCase1.Id);
                system.assertequals(obj.sourceCaseTabId, 'TAB-1');
                system.assertequals(obj.destinationCaseTabId, 'TAB-2');

                obj.startFlow();
                Test.stopTest(); 
            }
        }

    	static testMethod void mybkrCISME_ReassignCase_Controller_Test() {
        // TO DO: implement unit test
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;
			        
        	
        	Group objGroup = new Group(DeveloperName = 'TEST', Name = 'Test', Type = 'Queue', Email = 'a@a.com');
        	insert objGroup;
        	
        	QueueSobject objQueueSobject = new QueueSobject(SobjectType = 'Case', QueueId = objGroup.Id);
        	insert objQueueSobject;

            GroupMember groupMember = new GroupMember(GroupId = objGroup.Id, UserOrGroupId=u.Id);               
            insert groupMember;
            
            System.runAs(u) {  
                Case objCase = new Case(bkrCase_Subm_Type__c = 'test', RecordTypeId='012o0000000C00J', Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');
        
                insert objCase;  
                
                Test.startTest(); 
                Test.setCurrentPage(Page.CI_ReassignCase);
                
                bkrCISME_ReassignCase_Controller obj = new bkrCISME_ReassignCase_Controller();        
                
                List<SelectOption> lstQueues = obj.getQueueSobjectList();
             
                obj.caseId = objCase.Id;
                obj.queueId = objGroup.Id;
                obj.validateCaseIds();
                system.assertequals(obj.c.Id, objCase.Id);
                obj.assignQueue();
                system.assertequals(obj.refreshPage, true);
                system.assertequals(obj.closePage, true);  
                Test.stopTest(); 
            }
        }
    
    static testMethod void myUWSubmissionTriage_ConTest() {
        // TO DO: implement unit test
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;

		System.runAs(u) {   	
	        Case objCase = new Case(Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');
	
			insert objCase;
			
			Product2 prd1 = new Product2 (Name = 'Test Product', IsActive = true,Family = 'Commercial Insurance');
        	insert prd1;
        						
			Test.startTest(); 
			Test.setCurrentPage(Page.UWSubmissionTriage);
        	
			ApexPages.StandardController controller = new ApexPages.StandardController(objCase);
			UWSubmissionTriage_Con ObjUWSubmissionTriage = new UWSubmissionTriage_Con(controller);
			
			ObjUWSubmissionTriage.Name = '';
			ObjUWSubmissionTriage.searchDnB();
			
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			
			
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
			ObjUWSubmissionTriage.Name = 'A Corp';
			
			
			ObjUWSubmissionTriage.Name = 'A Corp';
			ObjUWSubmissionTriage.CountryISOCode = 'CA';
			ObjUWSubmissionTriage.Address = '46 Fieldway Rd';
			ObjUWSubmissionTriage.City = 'Etobicoke';
			ObjUWSubmissionTriage.State = 'ON';
			ObjUWSubmissionTriage.PostalCode = 'M8Z 3L2';
			ObjUWSubmissionTriage.searchDnB();
			
			ObjUWSubmissionTriage.myDUNS = '255139792';
			ObjUWSubmissionTriage.selectRecordAction();
			ObjUWSubmissionTriage.selectRecordAction();
			ObjUWSubmissionTriage.getProductList();
			
			// Removed 03.01.2016
			/*ObjUWSubmissionTriage.getQueueSobjectList();

			
			try{
				ObjUWSubmissionTriage.queueId = objGroup.Id;
				ObjUWSubmissionTriage.assignQueue();
			
			}
			catch(exception e){
				
			}*/
						
			try{
				ObjUWSubmissionTriage.assignSubmType();
				
				objCase.OwnerId = 'Test';
			
			}
			catch(exception e){
				
			}
			
			ObjUWSubmissionTriage.updateRecordType();
			
			Test.stopTest();
		}
		
	    }
    
    static testMethod void testSelectSmeRecordAction()
    {
        // TO DO: implement unit test
        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
        insert u;
        
        System.runAs(u)
        {   	
            Case objCase = new Case(
                Status = 'New',
                Priority = 'Medium',
                Origin = 'Phone',
                bkrCase_Category__c = 'Reports',
                Subject = 'Test',
				Insured_Client_Name__c = 'A Corp',
				Insured_Client_Address__c = '46 Fieldway Rd',
				Insured_Client_City__c = 'Etobicoke',
				Insured_Client_State__c = 'ON',
				Insured_Client_Postal_Code__c = 'M8Z 3L2'
            );
            insert objCase;
            
            Product2 prd1 = new Product2(
                Name = 'Test Product',
                IsActive = true,
                Family = 'Commercial Insurance'
            );
            insert prd1;
            
            Test.startTest(); 
            Test.setCurrentPage(Page.UWSubmissionTriage);
            
            ApexPages.StandardController controller = new ApexPages.StandardController(objCase);
            UWSubmissionTriage_Con ObjUWSubmissionTriage = new UWSubmissionTriage_Con(controller);
            
            ObjUWSubmissionTriage.Name = '';
            ObjUWSubmissionTriage.searchSmeDnB();
            
            Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
            Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
            
            ObjUWSubmissionTriage.Name = 'A Corp';
            ObjUWSubmissionTriage.CountryISOCode = 'CA';
            ObjUWSubmissionTriage.Address = '46 Fieldway Rd';
            ObjUWSubmissionTriage.City = 'Etobicoke';
            ObjUWSubmissionTriage.State = 'ON';
            ObjUWSubmissionTriage.PostalCode = 'M8Z 3L2';
            ObjUWSubmissionTriage.searchSmeDnB();
            
            ObjUWSubmissionTriage.myDUNS = '255139792';
            ObjUWSubmissionTriage.selectSmeRecordAction();
            ObjUWSubmissionTriage.selectSmeRecordAction();
            ObjUWSubmissionTriage.getProductList();
            
            try
            {
                ObjUWSubmissionTriage.assignSubmType();
                objCase.OwnerId = 'Test';
            }
            catch(exception e)
            {
                
            }
            
            ObjUWSubmissionTriage.updateRecordType();
            
            Test.stopTest();
        }
        
    }
    
    /**
     * The purpose of the case is to ensure the user is eligible to manage cases within their referral level.
     */
    static testMethod void testIsEligible()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Case objCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Test.startTest(); 
            Test.setCurrentPage(Page.UWSubmissionTriage);
            
            ApexPages.StandardController controller = new ApexPages.StandardController(objCase);
            UWSubmissionTriage_Con ObjUWSubmissionTriage = new UWSubmissionTriage_Con(controller);
            
            ObjUWSubmissionTriage.Name = '';
            System.assertEquals(true, ObjUWSubmissionTriage.isEligible());
            
            Test.stopTest();
        }
    }
    
    /**
     * The purpose of the case is to ensure the user is ineligible to manage cases beyond their referral level.
     */
    static testMethod void testIsEligibleNegative()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_TUA, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Case objCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Test.startTest(); 
            Test.setCurrentPage(Page.UWSubmissionTriage);
            
            ApexPages.StandardController controller = new ApexPages.StandardController(objCase);
            UWSubmissionTriage_Con ObjUWSubmissionTriage = new UWSubmissionTriage_Con(controller);
            
            ObjUWSubmissionTriage.Name = '';
            System.assertEquals(false, ObjUWSubmissionTriage.isEligible());
            
            Test.stopTest();
        }
    }

	static testMethod void testGetRecordTypeList(){
		List<SelectOption> listOptions = new List<SelectOption>();
		User uw = UserService_Test.getUwUser(UserService_Test.UW_TUA, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
		System.runAs(uw)
			{
				Case objCase = SMEQ_TestDataGenerator.getOpenSprntCase();
				Test.startTest();
				Test.setCurrentPage(Page.UWSubmissionTriage);

				ApexPages.StandardController controller = new ApexPages.StandardController(objCase);
				UWSubmissionTriage_Con ObjUWSubmissionTriage = new UWSubmissionTriage_Con(controller);

				listOptions = ObjUWSubmissionTriage.getRecordTypeList();

				Test.stopTest();
			}
	}
}