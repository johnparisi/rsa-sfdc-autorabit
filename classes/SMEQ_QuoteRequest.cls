global with sharing class SMEQ_QuoteRequest
{
    public String referenceNumber {public get; public set;}
    public String language {public get; public set;}
    public String policyNumber {public get ; public set;}
    public String brokerID {public get; public set;}
    public String agentID {public get; public set;}
    public String caseId {public get; public set;}
    public String quoteID {public get; public set;}
    public String insuredClientID {public get; public set;}
    public Boolean liveChatRequired {public get; public set;}
    // FRZR-123 : This variable 'liveChatIndicator' is added to angular code to work properly since at angular end the changes cannot be handled.
    public Boolean liveChatIndicator {public get; public set;} 
    public Boolean kickOutRequired {public get; public set;}
    public String liveChatTriggerReasons {public get; public set;}
    public List<String> kickoutTriggerReasons {public get; public set;}
    public Boolean affirmativeConditionAnsweredYes {public get; public set;}
    public String finalizedDate {public get; public set;}
    public String quoteNumber {public get; public set;}
    public String selectedPackageID {public get; public set;}
    public Integer selectedAnnualPremium{public get; public set;}
    public Integer selectedAirMilesEarned{public get; public set;}
    public Boolean firstLocationAddrSameAsBusAddr {public get; public set;}
    public Boolean performedDandBSearch {public get; public set;}
    public Boolean hasDAndBMatch {public get; public set;}
    public List<SMEQ_CoverageDetailsModel> coverages {public get; public set;}
    public SMEQ_BusinessDetailsModel businessDetails {public get; public set;}
    public List<SMEQ_QuotePackage> packagesRequireRating {public get; public set;}
    public String nameInsured {public get; public set;}
    public String policyEffectiveDate {public get; public set;}
    public String policyPaymentplan {public get; public set;}
    public String policyBusinessDescription {public get; public set;}
    public Boolean isAdditionalInsured {public get; public set;}
    public Boolean isLossPayee {public get; public set;}
    public String policyNotes {public get; public set;}
    public String issuedDate {public get; public set;}
    public Boolean isBindLocationAddrSameAsMailAddr {public get; public set;}
    public String caseNumber {public get; public set;}
    public List<Liability> liability {public get; public set;}
    public String expiryDate {public get; public set;} 
    public String caseStatus {public get; public set;} 
    public Boolean isPolicyActive {public get; public set;} 
    public String ePolicyTransactionType {public get; public set;} 
    public String ePolicyTransactionStatus {public get; public set;}  
    public String newAnnualPremium {public get; public set;} 
    public String premium {public get; public set;} 
    public String newProratedPremium {public get; public set;} 
    public String isLocked {public get; public set;} 
    public String lastTransaction {public get; public set;} 
    public String amendmentEffectiveDate {public get; public set;} 
    public String reasonCodes {public get; public set;} 
    public List<String> policyHistories {public get; public set;} 
    public Boolean isCondo {public get; public set;}
    public List<String> additionalInsureds {public get; public set;} // Added to match angular data model
    public BillingInformation billingInformation {public get; public set;} // Added to match angular data model
    public BindMailingAddress bindMailingAddress {public get; public set;} // Added to match angular data model
    public Boolean isRenewalAmendmentFlow {public get; public set;} // Added to match angular data model
    public Boolean isHUB {public get; public set;} // Added to match angular data model
    
    private class BillingInformation
    {
        public String billingMethod;
        public Integer downPayment;
        public Integer monthlyWithdrawDownPayment;
        public Integer monthlyWithdrawPreferredDay;
        public String paymentFrequency;
        public String bankTransitID;
        public String bankAccountNumber;
        public String creditCardType;
        public String creditCardExpirationMonth;
        public String creditCardExpirationYear;
        public OtherClientBill otherClientBill;
        public List<OtherClientBill> deletedOtherClientBills;
        public String creditCardHolder;
        public String billingAddress;
    }
    
    public class Liability
    {
        public String id {public get; public set;}
        public List<String> liabilityAdditionalInsureds {public get; public set;}
        public String premiumFinancier {public get; public set;}
        public List<String> deletedPremiumFinanciers {public get; public set;} 
    }
    
     // Added to match angular data model
    private class OtherClientBill
    {
        public String otherClientAddress;
        public String otherClientBillParty;
    }
    
     // Added to match angular data model
    private class BindMailingAddress
    {
        public String province;
        public String postalCode;
        public String city;
        public String country;
        public String addressLine1;
        public String addressLine2;
    }
}