/**
 * RSA_ESBService
 * Author: James Lee
 * Date: October 6 2016
 * 
 * The purpose of this class is to provide the framework for sending and obtaining information 
 * from external services through the RSA ESB. 
 */

public class RSA_ESBService
{
    public static String RSA_ESB_CONTEXT_1_28 = 'RSA ESB 1.28';
    public static String RSA_ESB_CONTEXT_1_7_1 = 'RSA ESB 1.7.1';
    
    private RequestInfo ri;
    private Id sId;
    private String comNumber;
    private String context;
    private String offeringProject;
    private String federationId; 
    
    public RSA_ESBService(RequestInfo ri, Id sId)
    {
        this.ri = ri;
        this.sId = sId;
        this.context = getContext(ri);
    }
    
    //New constructor for conditions where case/quote will not exist until after the service Callout (GETPOLICY)
    public RSA_ESBService(RequestInfo ri, String comNumber, String offeringProject, String federationId)
    {
        this.ri = ri;
        this.comNumber = comNumber;
        this.offeringProject = offeringProject;
        this.federationId = federationId; 
        this.context = getContext(ri);
    }    
    
    private static void processResponse(RequestInfo.ServiceType svct, Id quoteId, String packageName, String responseBody)
    {
        RequestInfo ri = new RequestInfo(
            svct,
            packageName,
            null
        );
        
        CSIOXMLProcessor cxp = new CSIOXMLProcessor(responseBody, ri);
        cxp.process(quoteId);
    }
    /*
     * Retrieve the XML required 
     */
    public String getRequestXML()
    {
        //setting this condition as sId will not exist when this method is called during a GETPOLICY 
        if (sId != null)
        {

            SOQLDataSet sds = new SOQLDataSet(sId, ri);
            CSIODataSet cds = new CSIODataSet(sds, ri);
            return cds.getCSIOXMLFormat();
        }
        else{
            CSIODataSet cds = new CSIODataSet(comNumber, ri, offeringProject, federationId);
            return cds.getCSIOXMLFormat();
        }
    }
    
    public HttpRequest getHttpRequest(String requestBody)
    {
        String authorizationHeader = RSA_CalloutAuthentication.getAuthorizationHeader(context);
        String endpoint = RSA_CalloutAuthentication.getEndpoint(context);
        system.debug('!!@@@@@@@@@@ endpoint '+endpoint);
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        req.setHeader('Authorization', authorizationHeader);
        req.setBody(requestBody);

        return req;
    }
    
    public void parseResponseXML(String responseBody)
    {
        CSIOXMLProcessor cxp = new CSIOXMLProcessor(responseBody, this.ri);
        cxp.process(this.sId);
    }
    
    @testVisible
    private static String getContext(RequestInfo ri)
    {
        String context;
        if (ri.svcType == RequestInfo.ServiceType.GET_QUOTE || 
            ri.svcType == RequestInfo.ServiceType.GET_3_QUOTE || 
            ri.svcType == RequestInfo.ServiceType.UPLOAD_QUOTE || 
            ri.svcType == RequestInfo.ServiceType.FINALIZE_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.BIND_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.GET_POLICY ||
            ri.svcType == RequestInfo.ServiceType.LOAD_POLICY || 
            ri.svcType == RequestInfo.ServiceType.LOAD_POLICY_X ||
            ri.svcType == RequestInfo.ServiceType.ABORT_TRANSACTION ||
           	ri.svcType == RequestInfo.ServiceType.SAVE_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.RABIND_QUOTE ||
           	ri.svcType == RequestInfo.ServiceType.RAGET_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.RAFINALIZE_QUOTE)
        {
            context = RSA_ESB_CONTEXT_1_28;
        }
        else if (ri.svcType == RequestInfo.ServiceType.DOWNLOAD_QUOTE ||
                 ri.svcType == RequestInfo.ServiceType.DOWNLOAD_BINDER)
        {
            context = RSA_ESB_CONTEXT_1_7_1;
        }
        return context;
    }
    
    //Fix this - throwaway after integration
    @TestVisible
    @Future
    public static void insertNote(Id parent, String title, String body)
    {
        Note n = new Note();
        n.parentId = parent;
        n.title = title;
        n.isPrivate = false;
        
        Integer length = body.length();
        Integer head = 0;
        Integer current = Math.min(length, 32000);
        
        while (true)
        {
            n.Id = null;
            n.body = body.subString(head, current);
            insert n;
            
            if (current == length)
            {
                break;
            }
            
            head = current;
            current = Math.min(length, current + 32000);
        }
    }
}