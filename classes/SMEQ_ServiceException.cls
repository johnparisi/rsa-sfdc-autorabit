/**
 * ServiceException to flag any 'downstream' processing issues with service
 * requests.
 *
 */

public with sharing class SMEQ_ServiceException extends Exception {}