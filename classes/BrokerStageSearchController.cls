public without sharing class BrokerStageSearchController{

    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    // the actual account
    public Account myAccount;
    public Case myCase;
    public String accountName {get; set;}
    public List<Broker_Stage__c> brokerageList  {get; set;}
    public String brokerNumber{get; set;}
    public String selectedBrokerageNumber {get; set;}
    public BrokerStageSearchController(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        this.controller = controller;
        this.myCase = (Case)controller.getRecord();

    }
   public void retrieveBrokerageList() {
            Case thisCase =  [select accountid,account.bkrAccount_HuonBrokerNumber__c from case where case.id=:this.myCase.id];
            this.brokerageList= new List<Broker_Stage__c>();
            Account acc = [select name,bkrAccount_HuonBrokerNumber__c from account where account.id=:thisCase.accountid];
            this.accountName = acc.name;
            this.brokerNumber = acc.bkrAccount_HuonBrokerNumber__c;
            if(acc.bkrAccount_HuonBrokerNumber__c!=null)
            {

                 this.brokerageList = BrokerStageService.getStageBrokerageList(thisCase.account.bkrAccount_HuonBrokerNumber__c);
            }
   }

    public PageReference removeBrokerageSelection() {
        Case thisCase =  [select selected_brokerage_number__c, hasSubBrokerage__c from case where case.id=:this.myCase.id];
        if(thisCase.selected_brokerage_number__c!=null)
        {
            thisCase .selected_brokerage_number__c = null ;
           // system.debug('REMOVED selected_brokerage_number__c :'+selectedBrokerageNumber );
            update thisCase;
        }
        return null;
    }

    public PageReference updateCaseBrokerageNumber() {
        
        Case thisCase =  [select selected_brokerage_number__c, hasSubBrokerage__c from case where case.id=:this.myCase.id];
        if(selectedBrokerageNumber!=null)
        {
            thisCase .selected_brokerage_number__c = selectedBrokerageNumber ;
           // thisCase.hasSubBrokerage__c = !thisCase.hasSubBrokerage__c;
            //system.debug('selected_brokerage_number__c :'+selectedBrokerageNumber );
            update thisCase;
            //system.debug('selected_brokerage_number__c :'+thisCase .selected_brokerage_number__c );
        }
        return redirectToCase();
    }

    public PageReference redirectToCase() {
        PageReference casePage = new ApexPages.StandardController(this.myCase).view();
        casePage.setRedirect(true);
        return casePage ;
     }



  

}