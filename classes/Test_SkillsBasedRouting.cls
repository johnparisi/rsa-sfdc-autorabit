Global class Test_SkillsBasedRouting {
    public static void routeUsingSkills(List<LiveChatTranscript> cases) {
        //List<LiveChatTranscript> caseObjects = [SELECT Id, Name FROM LiveChatTranscript WHERE Id in :cases];
        system.debug('Testiknggggggggggggggggggg'+cases);
        for (LiveChatTranscript caseObj : cases) {
            try {
                // Create a PendingServiceRouting to initiate routing
                createPendingServiceRouting(caseObj);
            } catch(exception e) {
                System.debug('ERROR:' + e.getStackTraceString());
                throw e;
            }
        }
    }
    
    static void createPendingServiceRouting(LiveChatTranscript  caseObj) {
        // Create a new SkillsBased PendingServiceRouting
        PendingServiceRouting psrObj = new PendingServiceRouting(
            CapacityWeight = 1,
            IsReadyForRouting = FALSE,
            RoutingModel  = 'MostAvailable',
            RoutingPriority = 1,
            RoutingType = 'SkillsBased',
            ServiceChannelId = getChannelId(),
            WorkItemId = caseObj.Id,
            PushTimeout = 0
        );
        insert psrObj;
        psrObj = [select id, IsReadyForRouting from PendingServiceRouting where id = : psrObj.id];
        
        // Now add SkillRequirement(s) to the PendingServiceRouting
        SkillRequirement srObj = new SkillRequirement(
            RelatedRecordId = psrObj.id,
            SkillId = getSkillId(),
            SkillLevel = 5
        );
        insert srObj;
        
        // Update PendingServiceRouting as IsReadyForRouting
        psrObj.IsReadyForRouting = TRUE;
        update psrObj; 
    }
    
    static String getChannelId() {
        ServiceChannel channel = [Select Id From ServiceChannel Where RelatedEntity = 'LiveChatTranscript'];
        return channel.Id;
    }
    
    static String getSkillId() {
        String skillName = 'All_Underwriters_Atlantic_EN';
        Skill skill = [Select Id From Skill Where DeveloperName = :skillName];
        return skill.Id;
    }
}