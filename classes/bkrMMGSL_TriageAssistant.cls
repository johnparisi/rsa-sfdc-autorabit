public class bkrMMGSL_TriageAssistant{
    // Set All Variables 
    public Case c;
    public String Name {get;set;}
    public String Address {get;set;}
    public String City {get;set;}
    public String State {get;set;}
    public String PostalCode {get;set;}
    public String CountryISOCode {get;set;}
    public String myDUNS {get;set;}
    public Boolean refreshPage {get;set;}
    public Boolean closePage {get;set;}
    public List<DNBCallout.dnbEntry> results {get;set;}
    public DNBCallout.dnbEntry record {get;set;}
    // Activate the Controller 
    public bkrMMGSL_TriageAssistant(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()){  
            controller.addFields(new List<String>{'RecordTypeId','OwnerId'});
        }
        c = (Case)controller.getRecord(); 
        results = new List<DNBCallout.dnbEntry>();
        Map<String, Object> myMap = new Map<String, Object>();
    }
    // Activate the DNBCallout class and search D&B
    public void searchDnB(){
        results = new List<DNBCallout.dnbEntry>();
        if(Name == null || Name == '')
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'A company name is required to search D&B.'));
        else
            try{
                if (!Test.isRunningTest())  
                    results = DNBCallout.LookupByCompanyName(Name,CountryISOCode,Address,City,State,PostalCode,null);
                if(Test.isRunningTest())
                    Integer i = 10/0;
                     System.debug('Duns number from result'+results);
                    
            }           
        catch (exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }    
    // Lookup the Client Account, also known as the Policyholder, record type and get the record type's ID
    public void selectRecordAction(){
        refreshPage = false;
        List<RecordType> lstRecordType = new List<RecordType>([Select Id From RecordType Where SobjectType = 'Account' and RecordType.Name = 'Policyholder' LIMIT 1]); 
        Set<String> dunsNumbers = new Set<String>();
        Map<String, DNBCallout.dnbEntry> records = new Map<String, DNBCallout.dnbEntry>();
        Map<String, Account> clientAccountsByDUNS = new Map<String, Account>();
        for(DNBCallout.dnbEntry r : results){
            records.put(r.DUNSNumber, r);
        }
        List<Account> ClientAccounts = new List<Account>([Select Id, bkrAccount_DUNS__c From Account Where bkrAccount_DUNS__c in: records.keySet()]); 
        for(Account r : ClientAccounts){
            clientAccountsByDUNS.put(r.bkrAccount_DUNS__c, r);
        }
        for(DNBCallout.dnbEntry r : results){    
            // Lists existing Client Account Entries 
            // If a matching Client Account entry exists, use it rather than creating a new one
            if(clientAccountsByDUNS.containsKey(myDUNS)){
                c.bkrCase_Insured_Client__c = clientAccountsByDUNS.get(myDUNS).id;
                upsert c;
                refreshpage = true;
                return;
            }
            
            System.debug('Duns number from result'+r);
            // If no matching Client Account entries exist, create a new one using the search result  
            if(r.DUNSNumber == myDUNS){
                Account a = new Account();
                if(!lstRecordType.isEmpty())                   
                    a.RecordTypeId = lstRecordType[0].Id;
                a.Name = r.Company;
                a.bkrAccount_DUNS__c = r.DUNSNumber;
                a.BillingStreet = r.Address;
                a.BillingCity = r.City;
                a.BillingState = r.State;
                a.BillingPostalCode = r.ZipCode;
                a.Phone = r.Phone;
                try{
                    insert a;
                }catch (exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    return;
                }
                // Lastly, update the Client Account, also referred to as the Insured Client, field on the case and refresh the page

                c.bkrCase_Insured_Client__c = a.Id;
                upsert c;
                refreshPage = true;
                return;
            }
        }
    }
}