global class Vlocity_RatingRequestProcessor implements vlocity_ins.VlocityOpenInterface2 {
    
    global Object invokeMethod(String methodName, Map<String,Object> input, Map<String,Object> output, Map<String,Object> options) {
        Boolean result = true;
        try {
            if (methodName.equals('getRQRatingJSONRequest'))
                getRQRatingJSONRequest(input, output);
            else if (methodName.equals('getFQRatingJSONRequest'))
                getFQRatingJSONRequest(input, output);
            else 
                result = false;
        }catch (Exception e) {
            System.debug('exception: ' + e + ' line no' + e.getLineNumber());
            result = false;
        }
        return result;
    }
    
    public void getRQRatingJSONRequest(Map<String,Object> inputMap, Map<String,Object> outMap) {
        Vlocity_Model_RequestProcessor.cls_elementValueMap rq = retrieveData('getRQRatingJSONRequest',inputMap);
        String json = JSON.serialize(rq);
        system.debug('json -->'+System.JSON.serializePretty(rq));
        outMap.put('reqRQ',json);
    }
    
    public void getFQRatingJSONRequest(Map<String,Object> inputMap, Map<String,Object> outMap) {
        Vlocity_Model_RequestProcessor.cls_elementValueMap rq = retrieveData('getFQRatingJSONRequest',inputMap);
        String json = JSON.serialize(rq);
        system.debug('json -->'+System.JSON.serializePretty(rq));
        outMap.put('reqFQ',json);
    }
   
    public Vlocity_Model_RequestProcessor.cls_elementValueMap retrieveData(String methodName,Map<String,Object> inputMap){
        List<Object> listInput = new List<Object>();
        if(inputMap.containskey('input_1'))
            listInput = (List<Object>)inputMap.get('input_1'); // TBD : This key 'input_1' needs to be changed
        
        Map<String,Object> tempMap = new Map<String,Object>();
        if(!listInput.isEmpty())
            tempMap.put('ratingInfo',listInput.get(0)); 
        Map<String,Object> ratingResults = new Map<String,Object>();
        if(tempMap.containsKey('ratingInfo'))
            ratingResults = (Map<String,Object>)tempMap.get('ratingInfo');
        
        Map<String,Object> mapPropertyDetails = new Map<String,Object>();
        if(ratingResults.containsKey('SICQuestionnaire'))
            mapPropertyDetails.put('PropertyDetails',ratingResults.get('SICQuestionnaire'));
        /*
        List<Object> listLocations = (List<Object>)tempMap.get('PropertyDetails');

        List<Vlocity_Model_RequestProcessor.cls_LocationInfo> locWrapper = new List<Vlocity_Model_RequestProcessor.cls_LocationInfo>(); // TBD:  To be revisited... the key will be PropertyDetails and it will be an array
        for(Object jsnLoc : listLocations){
            Vlocity_Model_RequestProcessor.cls_LocationInfo l = new Vlocity_Model_RequestProcessor.cls_LocationInfo();
            
            locWrapper.add(l);
        }
        */
        String amountCAD = ''; // TBD : To be revisited..
        
        // <Producer> node preparation starts
        /* <ItemIdInfo> xml node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_ItemIdInfo producerItemidInfo = new Vlocity_Model_RequestProcessor.cls_ItemIdInfo();
        producerItemidInfo.AgencyId = 'XQATB01'; // = ratingResults.get('AgencyId');
         
        /* <CommlName> xml node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_CommlName producerCommlName = new Vlocity_Model_RequestProcessor.cls_CommlName();
        producerCommlName.CommercialName = 'BLUENOSE INSURANCE'; // = ratingResults.get('BrokerageName');
        
        /* <NameInfo> xml node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_NameInfo producerNameInfo = new Vlocity_Model_RequestProcessor.cls_NameInfo();
        producerNameInfo.CommlName = producerCommlName;
        
        /* <DetailAddr> xmnl node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_DetailAddr producerDetailAddr = new Vlocity_Model_RequestProcessor.cls_DetailAddr();
        producerDetailAddr.StreetNumber = '676'; // = ratingResults.get('BrokerageStreetNumber');
        producerDetailAddr.StreetName = 'GEORGE ST'; // = ratingResults.get('BrokerageStreetName');
        
        /* <Addr> xmnl node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_Addr producerAddr = new Vlocity_Model_RequestProcessor.cls_Addr();
        producerAddr.DetailAddr  = producerDetailAddr;
        producerAddr.StateProvCd = 'NS'; // = ratingResults.get('BrokerageState');
        producerAddr.CountryCd = 'CA'; // = ratingResults.get('BrokerageCountry');
        producerAddr.PostalCode = 'B1P 1K9'; // = ratingResults.get('BrokeragePostalCode');
        producerAddr.City = 'Sydney'; // = ratingResults.get('BrokerageCity');
        
        /* <GeneralpartyInfo> xml node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_GeneralPartyInfo producerGeneralpartyInfo = new Vlocity_Model_RequestProcessor.cls_GeneralPartyInfo();
        producerGeneralpartyInfo.Addr = producerAddr;
        producerGeneralpartyInfo.NameInfo = producerNameInfo;
        
        /* <ProducerInfo> xml node preperation for Producer */
        Vlocity_Model_RequestProcessor.cls_ProducerInfo producerInfo = new Vlocity_Model_RequestProcessor.cls_ProducerInfo();
        producerInfo.ProducerRoleCd = 'Broker'; // = ratingResults.get('ProducerRoleCd');
        producerInfo.ContractNumber = '52497052'; // = ratingResults.get('BrokerContractNumber');
        
        /* <Producer> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer producer = new Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer();
        producer.ItemIdInfo = producerItemidInfo;
        producer.GeneralPartyInfo = producerGeneralpartyInfo;
        producer.ProducerInfo = producerInfo;
        // <Producer> node preparation ends // Suman - Checking complete
        
        // <InsuredOrPrincipal> node preparation starts
        /* <ItemIdInfo> xml node preperation for InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_ItemIdInfo insuredItemidInfo = new Vlocity_Model_RequestProcessor.cls_ItemIdInfo();
        insuredItemidInfo.AgencyId = '001c000001tCoGIAA0'; // = ratingResults.get('AccountId');
        
        /* <CommlName> xml node preperation for InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_CommlName insuredCommlName = new Vlocity_Model_RequestProcessor.cls_CommlName();
        insuredCommlName.CommercialName = 'Test Insured CommercialName'; // TBD : Need to know the JSON node
        
        /* <NameInfo> xml node preperation InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_NameInfo insuredNameInfo = new Vlocity_Model_RequestProcessor.cls_NameInfo();
        insuredNameInfo.CommlName = insuredCommlName;
        
        /* <DetailAddr> xmnl node preperation for InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_DetailAddr insuredDetailAddr = new Vlocity_Model_RequestProcessor.cls_DetailAddr();
        //insuredDetailAddr.StreetNumber = '676'; // Not present in sample RQ XML
        insuredDetailAddr.StreetName = 'Test Insured StreetName'; // TBD : Need to know the JSON node
        
        /* <Addr> xmnl node preperation InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_Addr insuredAddr = new Vlocity_Model_RequestProcessor.cls_Addr();
        insuredAddr.DetailAddr  = insuredDetailAddr;
        insuredAddr.StateProvCd = 'NL'; // TBD : Need to know the JSON node
        insuredAddr.CountryCd = 'CA'; // TBD : Need to know the JSON node
        insuredAddr.PostalCode = 'A1A 1A1'; // TBD : Need to know the JSON node
        insuredAddr.City = 'Test Insured City'; // TBD : Need to know the JSON node
        
        /* <GeneralpartyInfo> xml node preperation InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_GeneralPartyInfo insuredGeneralpartyInfo = new Vlocity_Model_RequestProcessor.cls_GeneralPartyInfo();
        insuredGeneralpartyInfo.Addr = insuredAddr;
        insuredGeneralpartyInfo.NameInfo = insuredNameInfo;
        
        /* <BusinessInfo> xml node preperation for InsuredorPrincipal */
        Vlocity_Model_RequestProcessor.cls_Area area = new Vlocity_Model_RequestProcessor.cls_Area();
        area.UnitMeasurementCd = 'SqFt';
        Integer NumUnits = 0;
        if(mapPropertyDetails.containsKey('PropertyDetails')){
            for(Object o : (List<Object>)mapPropertyDetails.get('PropertyDetails')){
                Map<String,Object> tmpLocMap = (Map<String,Object>)o;
                NumUnits += Integer.ValueOf(tmpLocMap.get('TotalArea'));
            }
        }
        area.NumUnits = '1000'; // = String.valueOf(NumUnits);
        Vlocity_Model_RequestProcessor.cls_BusinessInfo businessInfo = new Vlocity_Model_RequestProcessor.cls_BusinessInfo();
        businessInfo.Area = area;

        Map<String, Object> mapGettingStarted = (Map<String, Object>)ratingResults.get('GettingStarted');
        Map<String, Object> mapTypeAheadSICBlock = (Map<String, Object>)ratingResults.get('TypeAheadSIC-Block');
        businessInfo.OperationsDesc = 'Lawyers [8111]'; // = mapTypeAheadSICBlock.get('TypeAheadSIC-Block');
        businessInfo.BusinessStartDt = '2000'; // TBD : Need to know the JSON node
        businessInfo.SICCd = '8111'; // = mapGettingStarted.get('SICcodeId');
        
        /* <InsuredOrPrincipalInfo> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_InsuredOrPrincipalInfo insuredOrPrincipalInfo = new Vlocity_Model_RequestProcessor.cls_InsuredOrPrincipalInfo();
        insuredOrPrincipalInfo.BusinessInfo = businessInfo;

        /* <InsuredOrPrincipal> xml node preparation */
        Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal insuredOrPrincipal = new Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal();
        insuredOrPrincipal.ItemIdInfo = insuredItemidInfo;
        insuredOrPrincipal.GeneralPartyInfo = insuredGeneralPartyInfo;
        insuredOrPrincipal.InsuredOrPrincipalInfo = insuredOrPrincipalInfo;
        // <InsuredOrPrincipal> node preparation ends
        
        // <CommlPolicy> node preperation starts
        /* <UnderwritingDecisionInfo> xml node preperation for InsuredOrPrincipal */
        Vlocity_Model_RequestProcessor.cls_UnderwritingDecisionInfo underwritingDecisionInfo = new Vlocity_Model_RequestProcessor.cls_UnderwritingDecisionInfo();
        underwritingDecisionInfo.UnderwriterId = 'XQATB01';
        
        /* <CommlPolicySupplement> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_AnnualSalesAmt annualsalesAmt = new Vlocity_Model_RequestProcessor.cls_AnnualSalesAmt();
        annualsalesAmt.Amt = '1000000' ; // = amountCAD;
        
        Vlocity_Model_RequestProcessor.cls_CreditOrSurcharge creditOrSurcharge;
        if(methodName=='getFQRatingJSONRequest'){ // TBD : Dynamic data to be populated
            Vlocity_Model_RequestProcessor.cls_NumericValue numericValue = new Vlocity_Model_RequestProcessor.cls_NumericValue();
            numericValue.FormatPct = '-20';
            
            creditOrSurcharge = new Vlocity_Model_RequestProcessor.cls_CreditOrSurcharge();
            creditOrSurcharge.NumericValue = numericValue;
            creditOrSurcharge.DeviateReason = 'brkmgmt';
        }
        
        Vlocity_Model_RequestProcessor.cls_CommlPolicySupplement commlpolicySupplement = new Vlocity_Model_RequestProcessor.cls_CommlPolicySupplement();
        commlpolicySupplement.AnnualSalesAmt = annualsalesAmt;
        if(methodName=='getFQRatingJSONRequest'){
            commlpolicySupplement.CreditOrSurcharge = creditOrSurcharge;
        }
        
        /* <CommlCoverage> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_ClaimsMadeInfo claimsmadeInfo = new Vlocity_Model_RequestProcessor.cls_ClaimsMadeInfo();
        claimsmadeInfo.ClaimsMadeInd = '0';
        Vlocity_Model_RequestProcessor.cls_CommlCoverageSupplement commlcoverageSupplement = new Vlocity_Model_RequestProcessor.cls_CommlCoverageSupplement();
        commlcoverageSupplement.ClaimsMadeInfo = claimsmadeInfo;
        Vlocity_Model_RequestProcessor.cls_CommlCoverage commlPolicyCommlCoverage = new Vlocity_Model_RequestProcessor.cls_CommlCoverage();
        commlPolicyCommlCoverage.CommlCoverageSupplement = commlcoverageSupplement;
        commlPolicyCommlCoverage.CoverageCd = 'csio:2';
        
        /* <CommlPolicy> xml mode preperation */
        Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy commlPolicy = new Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy();
        commlPolicy.id = 'a0gc000000IsjnIAAR'; // quote record id
        commlPolicy.LOBCd = 'csio:1';        
        commlPolicy.LanguageCd = 'en';
        commlPolicy.CompanyCd = 'ROY';
        commlPolicy.UnderwritingDecisionInfo = underwritingDecisionInfo;
        commlPolicy.CommlPolicySupplement = commlpolicySupplement;
        commlPolicy.CommlCoverage = commlPolicyCommlCoverage;
        commlPolicy.Segment = 'BPS';
        commlPolicy.Flavour = 'FL2';
        commlPolicy.SalesforceCaseNumber = '01389384'; 
        // <CommlPolicy> node preperation ends
        
        // <Location>, <CommlSubLocation> & <CommlPropertyLineBusiness> node preperation starts
        List<Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location> listLocations = new List<Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location>();
        
        List<Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation> listCommlSubLocation = new List<Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation>();
        
        List<Vlocity_Model_RequestProcessor.cls_CommlPropertyInfo> commlpropertyInfoList = new List<Vlocity_Model_RequestProcessor.cls_CommlPropertyInfo>();
        String[] strCoverage = new String[]{'Contents','Stock','Equipment','Building'};
        
        Integer locCounter = 1;
        for(Integer j=0; j<1; j++){ // Object o : (List<Object>)mapPropertyDetails.get('PropertyDetails')
            // Map<String,Object> tmpLocMap = (Map<String,Object>)o;
            
            /* <ItemIdInfo> xml node preperation for Location */
            Vlocity_Model_RequestProcessor.cls_ItemIdInfo locationItemidInfo = new Vlocity_Model_RequestProcessor.cls_ItemIdInfo();
            locationItemidInfo.AgencyId = '001c000001tCoGIAA0'; // = ratingResults.get('AccountId');
            
            /* <DetailAddr> xmnl node preperation for InsuredOrPrincipal */
            Vlocity_Model_RequestProcessor.cls_DetailAddr locationDetailAddr = new Vlocity_Model_RequestProcessor.cls_DetailAddr();
            locationDetailAddr.StreetName = '400'; // = tmpLocMap.get('UnitNumber');
            locationDetailAddr.StreetNumber = 'Grace St'; // = tmpLocMap.get('RegisteredBusinessAddress');
            
            /* <Addr> xmnl node preperation InsuredOrPrincipal */
            Vlocity_Model_RequestProcessor.cls_Addr locationAddr = new Vlocity_Model_RequestProcessor.cls_Addr();
            locationAddr.DetailAddr  = locationDetailAddr;
            locationAddr.StateProvCd = 'ON'; // = tmpLocMap.get('Province');
            locationAddr.CountryCd = 'CA'; // = tmpLocMap.get('CountryCd');
            locationAddr.PostalCode = 'M6G 3A9'; // = tmpLocMap.get('PostalCode');
            locationAddr.City = 'Toronto'; // = tmpLocMap.get('City');
            
            /* <Location> xmnl node preperation InsuredOrPrincipal */
            Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location location = new Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location();
            location.Id = 'Location'+locCounter; // STATIC : for example Location1
            location.ItemidInfo = locationItemidInfo;
            location.Addr = locationAddr;
            location.Deleted = '0'; // STATIC: Always Zero - Introduced for R&A
            listLocations.add(location);
            
            
            
            /* <Construction> xml node preperation */
            Vlocity_Model_RequestProcessor.cls_Construction construction = new Vlocity_Model_RequestProcessor.cls_Construction();
            construction.YearBuilt = '2000'; // = tmpLocMap.get('BuiltYear');
            construction.NumStories = '2'; // = tmpLocMap.get('NumberofStories'); 
            construction.BldgConstructionCd = '1'; // = tmpLocMap.get('BuildingType');
                
            /* <BldgProtection> xml node preperation */
            Vlocity_Model_RequestProcessor.cls_DistanceToFireStation distancetofireStation = new Vlocity_Model_RequestProcessor.cls_DistanceToFireStation();
            distancetofireStation.NumUnits = '5'; // tmpLocMap.get('FireHallDistance');
            distancetofireStation.UnitMeasurementCd = 'KMT'; // STATIC
            Vlocity_Model_RequestProcessor.cls_DistanceToHydrant distanceToHydrant = new Vlocity_Model_RequestProcessor.cls_DistanceToHydrant();
            distanceToHydrant.NumUnits = '300'; // = tmpLocMap.get('FireHydrantDistance');
            distanceToHydrant.UnitMeasurementCd = 'MTR'; // STATIC
            Vlocity_Model_RequestProcessor.cls_BldgProtection bldgProtection = new Vlocity_Model_RequestProcessor.cls_BldgProtection();
            bldgProtection.DistanceToFireStation = distancetofireStation ;
            bldgProtection.DistanceToHydrant = distanceToHydrant;
            bldgProtection.SprinkleredPct = '100';
            /*
            if(tmpLocMap.get('FireSprinkler')=='Yes') bldgProtection.SprinkleredPct = '100';
            else bldgProtection.SprinkleredPct = '0';
            */
            /* <BldgOccupancy> xml node preperation */
            Vlocity_Model_RequestProcessor.cls_AreaOccupied areaOccupied = new Vlocity_Model_RequestProcessor.cls_AreaOccupied();
            areaOccupied.NumUnits = '1000'; // = tmpLocMap.get('TotalArea');
            areaOccupied.UnitMeasurementCd = 'FTK'; // STATIC
            Vlocity_Model_RequestProcessor.cls_BldgOccupancy bldgOccupancy = new Vlocity_Model_RequestProcessor.cls_BldgOccupancy();
            bldgOccupancy.AreaOccupied = areaOccupied;
            
            /* <AlarmAndSecurity> xml node preperation */
            List<Vlocity_Model_RequestProcessor.cls_AlarmAndSecurity> alarmandSecurityList = new List<Vlocity_Model_RequestProcessor.cls_AlarmAndSecurity>();
            for(Integer i=1; i<=2; i++){ // Confirmed : The upper limit will be 2 and there will alwyas be only two <AlarmAndSecurity> nodes in the XML
                Vlocity_Model_RequestProcessor.cls_AlarmAndSecurity alarmandSecurity = new Vlocity_Model_RequestProcessor.cls_AlarmAndSecurity();
                alarmandSecurity.AlarmDescCd = 'csio:998';
                alarmandSecurity.AlarmTypeCd = 'csio:'+i;
                alarmandSecurityList.add(alarmandSecurity);
            }
            
            /* <CommlSubLocation> xml node preperation */
            Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation commlSubLocation = new Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation();
            commlSubLocation.LocationRef = 'Location'+locCounter;
            commlSubLocation.Construction = construction;
            commlSubLocation.BldgProtection = bldgProtection;
            commlSubLocation.BldgOccupancy = bldgOccupancy;
            commlSubLocation.AlarmAndSecurity = alarmandSecurityList;
            listCommlSubLocation.add(commlSubLocation);
            
            
            
            /* <CommlPropertyInfo> xml node prepartion */
            Vlocity_Model_RequestProcessor.cls_CommlPropertyInfo commlpropertyInfo = new Vlocity_Model_RequestProcessor.cls_CommlPropertyInfo();
            List<Vlocity_Model_RequestProcessor.cls_CommlCoverage> commlPropertyLineBusinessCommlCoverageList = new List<Vlocity_Model_RequestProcessor.cls_CommlCoverage>();
            for(String c : strCoverage){
                String amount = '';
                /*
                if(c == 'Stock')
                    amount = tmpLocMap.get('TotalValueOfStock').toString();
                if(c == 'Equipment')
                    amount = tmpLocMap.get('TotalValueOfEquipment').toString();
                if(c == 'Building')
                    amount = tmpLocMap.get('ReplacementValue').toString();
                if(c == 'Contents')
                    amount = String.valueOf(Integer.valueOf(tmpLocMap.get('TotalValueOfEquipment'))+Integer.valueOf(tmpLocMap.get('TotalValueOfStock')));
                */
                Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt limitFormatCurrencyAmt = new Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt();
                limitFormatCurrencyAmt.Amt = '100000'; 

                Vlocity_Model_RequestProcessor.cls_Limit limits = new Vlocity_Model_RequestProcessor.cls_Limit();
                limits.FormatCurrencyAmt = limitFormatCurrencyAmt;
                
                Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt deductibleFormatCurrencyAmt = new Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt();
                deductibleFormatCurrencyAmt.Amt = '1000'; // TBD : what is the value for deductible amount..need to review the JSON by Krish
                Vlocity_Model_RequestProcessor.cls_Deductible deductible = new Vlocity_Model_RequestProcessor.cls_Deductible();
                deductible.FormatCurrencyAmt = deductibleFormatCurrencyAmt;
                
                Vlocity_Model_RequestProcessor.cls_CommlCoverage commlPropertyLineBusinessCommlCoverage = new Vlocity_Model_RequestProcessor.cls_CommlCoverage();
                commlPropertyLineBusinessCommlCoverage.CoverageCd = 'csio:'+c;
                commlPropertyLineBusinessCommlCoverage.CoverageDesc = c;
                if(c == 'Building') 
                    commlPropertyLineBusinessCommlCoverage.Indicator = '1'; // TBD : to confirm if this is based on limit & deductible amounts..if the limit & deductible both are present then it will be 1 else 0
                else 
                    commlPropertyLineBusinessCommlCoverage.Indicator = '0';
                if(c == 'Building'){
                    commlPropertyLineBusinessCommlCoverage.Limits = limits;
                    commlPropertyLineBusinessCommlCoverage.Deductible = deductible;
                }
                commlPropertyLineBusinessCommlCoverageList.add(commlPropertyLineBusinessCommlCoverage);
            }
            commlpropertyInfo.CommlCoverage = commlPropertyLineBusinessCommlCoverageList;
            commlpropertyInfo.LocationRef = 'Location'+locCounter;
            commlpropertyInfoList.add(commlpropertyInfo);
            
            locCounter++;
        }
        // <Location>, <CommlSubLocation> node preperation ends
        

        /* <PropertyInfo> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_PropertyInfo propertyInfo = new Vlocity_Model_RequestProcessor.cls_PropertyInfo();
        propertyInfo.CommlPropertyInfo = commlpropertyInfoList;
        
        /* <CommlPropertyLineBusiness> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness commlpropertylineBusiness =  new Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness();
        commlpropertylineBusiness.LOBCd = 'csio:1';
        commlpropertylineBusiness.PropertyInfo = propertyInfo;
        // <CommlPropertyLineBusiness> node preperation ends 
        
        // <GeneralLiabilityLineBusiness> node preperation starts
        /* <LiabilityInfo> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt generalLiabilityLimitFormatCurrencyAmt = new Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt();
        generalLiabilityLimitFormatCurrencyAmt.Amt = '1000000'; // TBD : what will be the limit value..will be there in the sample JSON by krish
        
        Vlocity_Model_RequestProcessor.cls_Limit generalLiabilitylLimits = new Vlocity_Model_RequestProcessor.cls_Limit();
        generalLiabilitylLimits.FormatCurrencyAmt = generalLiabilityLimitFormatCurrencyAmt;
        
        Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt generalLiabilityDeductibleFormatCurrencyAmt = new Vlocity_Model_RequestProcessor.cls_FormatCurrencyAmt();
        generalLiabilityDeductibleFormatCurrencyAmt.Amt = '1000'; // TBD : what is the value for deductible amount..will be there in the sample JSON by krish
        
        Vlocity_Model_RequestProcessor.cls_Deductible generalLiabilityDeductible = new Vlocity_Model_RequestProcessor.cls_Deductible();
        generalLiabilityDeductible.FormatCurrencyAmt = generalLiabilityDeductibleFormatCurrencyAmt;
        
        Vlocity_Model_RequestProcessor.cls_CommlCoverage generalLiabilityLineBusinessCommlCoverage = new Vlocity_Model_RequestProcessor.cls_CommlCoverage();
        generalLiabilityLineBusinessCommlCoverage.CoverageCd = 'csio:BIPD'; // Static data
        generalLiabilityLineBusinessCommlCoverage.CoverageDesc = 'Commercial General Liability - Occurrence';
        generalLiabilityLineBusinessCommlCoverage.Indicator = '1'; // TBD : to confirm if this is based on limit & deductible amounts..confirmed
        generalLiabilityLineBusinessCommlCoverage.Limits = generalLiabilitylLimits;
        generalLiabilityLineBusinessCommlCoverage.Deductible = generalLiabilityDeductible;
        
        List<Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification> generalliabilityClassificationList = new List<Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification>();
        
        
        /*
        List<String> listTerritoryCds = new List<String>{'CA','US','ZZ'};
        for(String s: listTerritoryCds){ // total count of <GeneralLiabilityClassification> node fixed to count of 5.. 
            Vlocity_Model_RequestProcessor.cls_ExposureInfo exposureInfo = new Vlocity_Model_RequestProcessor.cls_ExposureInfo();
            exposureInfo.PremiumBasisCd = 'csio:6';
            
            if(s=='CA') // this logic should be modified..exposure info is annual canadian revenue for territory cd CA and annual us revenue for territorycd US and ZZ for annual foreign revenue..
                exposureInfo.Exposure = '1000000';
            else
                exposureInfo.Exposure = '0';

            Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification generalliabilityClassification = new Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification();
            generalliabilityClassification.TerritoryCd = s; 
            generalliabilityClassification.ExposureInfo = exposureInfo;
             
            generalliabilityClassificationList.add(generalliabilityClassification);
        }
        */
        
        Vlocity_Model_RequestProcessor.cls_ExposureInfo exposureInfoFood = new Vlocity_Model_RequestProcessor.cls_ExposureInfo();
        exposureInfoFood.PremiumBasisCd = 'rsa:food';
        exposureInfoFood.Exposure = '0';
        /*
        Vlocity_Model_RequestProcessor.cls_ExposureInfo exposureInfoCsio7 = new Vlocity_Model_RequestProcessor.cls_ExposureInfo();
        exposureInfoCsio7.PremiumBasisCd = 'csio:7';
        exposureInfoCsio7.Exposure = '0';
        */
        Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification generalliabilityClassificationFood = new Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification();
        generalliabilityClassificationFood.ExposureInfo = exposureInfoFood;
        /*
        Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification generalliabilityClassificationCsio7 = new Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification();
        generalliabilityClassificationCsio7.ExposureInfo = exposureInfoCsio7;
        */
        generalliabilityClassificationList.add(generalliabilityClassificationFood);
        /*
        generalliabilityClassificationList.add(generalliabilityClassificationCsio7);
        

        List<String> listPremiumBasisCd = new List<String>{'rsa:food','csio:7'};
        for(String s: listPremiumBasisCd){ 
            Vlocity_Model_RequestProcessor.cls_ExposureInfo exposureInfo = new Vlocity_Model_RequestProcessor.cls_ExposureInfo();
            exposureInfo.PremiumBasisCd = s;
            exposureInfo.Exposure = '0';
            
            Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification generalliabilityClassification = new Vlocity_Model_RequestProcessor.cls_GeneralLiabilityClassification();
            generalliabilityClassification.ExposureInfo = exposureInfo; // Exposure info is annual canadian revenue for territory cd CA and annual us revenue for territorycd US and ZZ for annual foreign revenue..
            generalliabilityClassificationList.add(generalliabilityClassification);
        }
        */
        Vlocity_Model_RequestProcessor.cls_LiabilityInfo liabilityInfo = new Vlocity_Model_RequestProcessor.cls_LiabilityInfo();
        liabilityInfo.CommlCoverage = generalLiabilityLineBusinessCommlCoverage; // TBD : this will not be a list. need confirmation..confirmed by onsite only 1 node for commenrical general liability
        liabilityInfo.GeneralLiabilityClassification = generalliabilityClassificationList;
        
        /* <GeneralLiabilityLineBusiness> xml node preperation */
        Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness generalliabilitylineBusiness = new   Vlocity_Model_RequestProcessor.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness();
        generalliabilitylineBusiness.LOBCd = 'csio:2';
        generalliabilitylineBusiness.LiabilityInfo = liabilityInfo;
        // <GeneralLiabilityLineBusiness> node preperation ends
        
        // ElementMap preperation
        Vlocity_Model_RequestProcessor.cls_ElementValueMap elementMap = new Vlocity_Model_RequestProcessor.cls_ElementValueMap();
        elementMap.RqUID = '36323461-3265-6537-3561-633930303662';
        elementMap.SignonRq_SignonTransport_CustId_SPName = 'rsagroup.ca';
        elementMap.SignonRq_SignonTransport_CustId_CustPermId = 'null';
        elementMap.SignonRq_ClientDt = Datetime.now().format('yyyy-mm-dd\'T\'HH:mm:ss');
        elementMap.SignonRq_CustLangPref = 'en';
        elementMap.SignonRq_ClientApp_Org = UserInfo.getOrganizationId();
        elementMap.SignonRq_ClientApp_Name = 'UDIG';
        elementMap.SignonRq_ClientApp_Version = 1;
        elementMap.SignonRq_rsa_QuoteSource = 'UDIG';
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CurCd = 'CAD';
        elementMap.InsuranceSvcRq_ServiceOperationType = 'RQ';
        elementMap.InsuranceSvcRq_Context = '3b95703a-2237-4d64-89d9-d040f69a5ad1'; 
        elementMap.PackageSelected = '01tc0000006ukxMAAQ'; //TBD : data model to be finalized
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer = producer;
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal = insuredOrPrincipal;
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy = commlPolicy;
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location = listLocations;
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation = listCommlSubLocation;
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness = commlPropertyLineBusiness;
        elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness = generalLiabilityLineBusiness;
        system.debug('RQ json -->'+System.JSON.serializePretty(elementMap));
        return elementMap;
    }
}