/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_ItemIdInfo.
 */
@isTest
public class CSIO_ItemIdInfo_Test
{
    static testMethod void testConvert()
    {
        Account a = SMEQ_TestDataGenerator.getInsuredAccount();
        
        CSIO_ItemIdInfo ciii = new CSIO_ItemIdInfo((String) a.Id);
        String output = '';
        output += ciii;
        
        System.assertEquals(
            output, 
            '<ItemIdInfo>' +
            ' <AgencyId>' + a.Id + '</AgencyId>' +
            '</ItemIdInfo>'
        );
    }
    
    static testMethod void testConvertWithoutAgencyId()
    {
        String nullString;
        
        CSIO_ItemIdInfo ciii = new CSIO_ItemIdInfo(nullString);
        String output = '';
        output += ciii;
        
        System.assertEquals(
            output, 
            '<ItemIdInfo>' +
            '</ItemIdInfo>'
        );
        
    }
}