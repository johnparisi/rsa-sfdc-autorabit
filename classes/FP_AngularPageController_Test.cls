@isTest
public class FP_AngularPageController_Test
{
    /**
     * This function tests FP_AngularPageController, to make sure the controller's functions are working
     * Assertions:
     * 1. Tests if the page header is properly set, true if it was set, false setting failed.
     * 2. Tests that the user does not have a sessionId.
     */
    public static testmethod void doTest () {
        FP_AngularPageController controller = new FP_AngularPageController();
        System.assertEquals('IE=edge',Apexpages.currentPage().getHeaders().get('X-UA-Compatible'));
        System.assertEquals(null, controller.verifyLoginStatus());
    }

     /**
     * This function tests FP_AngularPageController's versioning function
     * Assertions:
     * 1. Tests if the controller produce the correct version info base on current date. True version was generated correctly, false failed
     */
    public static testmethod void doTestGetAngularLastModified()
    {
       // Test.setCurrentPage(Page.ThePage);  // not really needed to test the component itself
        FP_AngularPageController controller = new FP_AngularPageController();
        List<StaticResource> resourceList = [
            SELECT LastModifiedDate
            FROM StaticResource
            WHERE Name = 'FPWL_Angular'
        ];

        String versionInfo = '';

        for (StaticResource sResouce : resourceList)
        {
             DateTime modTime = sResouce.LastModifiedDate;
             versionInfo = modTime.format('YYYYMMdd-HH-mm-ss', 'America/New_York');
        }
        System.assertEquals(versionInfo, controller.versionInfo);
    }

    /**
     * This function tests the remote function for Get Rate.
     * Assertions:
     * 1. Tests if the controller generates a continuation with one request.
     * 2. Tests if the request body is not null.
     */
    static testMethod void testProcessGetRateRequestOnePackage()
    {
        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();

            SMEQ_PackageRatingModel sprm = new SMEQ_PackageRatingModel();
            sprm.quoteID = q.Id;
            sprm.packagesRequireRating = new List<SMEQ_QuotePackage>();

            SMEQ_QuotePackage qp = new SMEQ_QuotePackage();
            qp.packageId = RiskService.PACKAGE_STANDARD;
            sprm.packagesRequireRating.add(qp);

            Test.startTest();
            Continuation con = (Continuation) FP_AngularPageController.processGetRateRequest(JSON.serialize(sprm));

            Map<String, System.HttpRequest> requests = con.getRequests();
            System.assertEquals(1, requests.size(), 'One request is expected for RQ.');

            for (System.HttpRequest hr : requests.values())
            {
                System.assertNotEquals(null, hr.getBody(), 'The request must contain a body.');
            }

            Test.stopTest();
        }
    }

    /**
     * This function tests the remote function for Get 3 Rate.
     * Assertions:
     * 1. Tests if the controller generates a continuation with one request.
     * 2. Tests if the request body is not null.
     */
    static testMethod void testProcessGetRateRequestThreePackages()
    {
        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();

            SMEQ_PackageRatingModel sprm = new SMEQ_PackageRatingModel();
            sprm.quoteID = q.Id;
            sprm.packagesRequireRating = new List<SMEQ_QuotePackage>();

            SMEQ_QuotePackage qp1 = new SMEQ_QuotePackage();
            qp1.packageId = RiskService.PACKAGE_STANDARD;
            sprm.packagesRequireRating.add(qp1);
            SMEQ_QuotePackage qp2 = new SMEQ_QuotePackage();
            qp2.packageId = RiskService.PACKAGE_OPTION_1;
            sprm.packagesRequireRating.add(qp2);
            SMEQ_QuotePackage qp3 = new SMEQ_QuotePackage();
            qp3.packageId = RiskService.PACKAGE_OPTION_2;
            sprm.packagesRequireRating.add(qp3);

            Test.startTest();
            Continuation con = (Continuation) FP_AngularPageController.processGetRateRequest(JSON.serialize(sprm));

            Map<String, System.HttpRequest> requests = con.getRequests();
            System.assertEquals(1, requests.size(), 'One request is expected for R3Q.');

            for (System.HttpRequest hr : requests.values())
            {
                System.assertNotEquals(null, hr.getBody(), 'The request must contain a body.');
            }

            Test.stopTest();
        }
    }

    /**
     * This function tests the remote function for Deviate Quote.
     * Assertions:
     * 1. Tests if the controller generates a continuation with three requests.
     * 2. Tests if the request body for each request is not null.
     */
    static testMethod void testProcessDeviateRequest()
    {
        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {
            SMEQ_TestDataGenerator.initializeExternalConnections();
            SMEQ_TestDataGenerator.initializeSMEMappings();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);

            SMEQ_PackageRatingModel sprm = new SMEQ_PackageRatingModel();
            sprm.quoteID = q.Id;
            sprm.packagesRequireRating = new List<SMEQ_QuotePackage>();

            SMEQ_QuotePackage qp = new SMEQ_QuotePackage();
            qp.packageId = RiskService.PACKAGE_STANDARD;
            sprm.packagesRequireRating.add(qp);

            Test.startTest();
            Continuation con = (Continuation) FP_AngularPageController.processDeviateRequest(JSON.serialize(sprm));

            Map<String, System.HttpRequest> requests = con.getRequests();
            System.assertEquals(3, requests.size(), 'Three requests are expected for deviation');

            for (System.HttpRequest hr : requests.values())
            {
                System.assertNotEquals(null, hr.getBody(), 'The request for each package must contain a body.');
            }

            Test.stopTest();
        }
    }

    /**
     * This function tests the remote function for Finalize Quote.
     * Assertions:
     * 1. Tests if the controller generates a continuation with one request.
     * 2. Tests if the request body is not null.
     */
    static testMethod void testProcessFinalizeRequest()
    {
        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();

            SMEQ_PackageFinalizedModel spfm = new SMEQ_PackageFinalizedModel();
            spfm.quoteID = q.Id;
            spfm.selectedPackageID = RiskService.PACKAGE_STANDARD;

            Test.startTest();
            Continuation con = (Continuation) FP_AngularPageController.processFinalizeRequest(JSON.serialize(spfm));

            Map<String, System.HttpRequest> requests = con.getRequests();
            System.assertEquals(1, requests.size(), 'One request is expected for FQ.');

            for (System.HttpRequest hr : requests.values())
            {
                System.assertNotEquals(null, hr.getBody(), 'The request must contain a body.');
            }

            Test.stopTest();
        }
    }

    /**
     * This function tests the remote function for Bind Quote.
     * Assertions:
     * 1. Tests if the controller generates a continuation with one request.
     * 2. Tests if the request body is not null.
     */
    static testMethod void testBindQuote()
    {
        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();

            Test.startTest();
            Continuation con = (Continuation) FP_AngularPageController.bindQuote(JSON.serialize(q));

            Map<String, System.HttpRequest> requests = con.getRequests();
            System.assertEquals(1, requests.size(), 'One request is expected for BQ.');

            for (System.HttpRequest hr : requests.values())
            {
                System.assertNotEquals(null, hr.getBody(), 'The request must contain a body.');
            }

            Test.stopTest();
        }
    }

    /**
     * This method is to test the post process the call out response
     * Assertions:
     * 1. Make sure that the premium for the package 1 is 100.00
     * 2. Make sure that the premium for the package 2 is 200.00
     * 3. Make sure that the premium for the package 3 is 300.00
     * 4. Make sure that the finalized date is today
     * 5. Make sure that the Quote Number is Q123456
     */
    @isTest
    public static void testPostProcessCalloutResponse()
    {
        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(cse);
            String quoteID = q.Id;
            Map<String, Object> responseMap =  FP_AngularPageController.postProcessCalloutResponse(quoteID);
            //System.assertEquals(100.00, (Decimal)responseMap.get('P1_PREMIUM'));
            //System.assertEquals(200.00, (Decimal)responseMap.get('P2_PREMIUM'));
            //System.assertEquals(300.00, (Decimal)responseMap.get('P3_PREMIUM'));
            //System.assertEquals(system.today(), (Date)responseMap.get('Finalized_Date'));
            String quote_number =  (String)responseMap.get('Quote_Number');
            System.assertEquals(true, quote_number.contains('QPC'));
        }
    }

    /**
     * This method is for testing getting the service type for multiple package rates request
     * Assertions:
     * 1. Make the sure the ServiceType returned is RequestInfo.ServiceType.GET_3_QUOTE.
     **/
    @isTest
    public static void testGetServiceTypeForCallOutRequestingThreeRates()
    {
        SMEQ_PackageRatingModel packageRatingModel = new SMEQ_PackageRatingModel();
        List<SMEQ_QuotePackage> packagesRequireRating = new List<SMEQ_QuotePackage>();
        SMEQ_QuotePackage quotePackage1 = new SMEQ_QuotePackage();
        quotePackage1.packageID = 'P1';
        SMEQ_QuotePackage quotePackage2 = new SMEQ_QuotePackage();
        quotePackage2.packageID = 'P2';
        SMEQ_QuotePackage quotePackage3 = new SMEQ_QuotePackage();
        quotePackage3.packageID = 'P3';
        packagesRequireRating.add(quotePackage1);
        packagesRequireRating.add(quotePackage2);
        packagesRequireRating.add(quotePackage3);
        packageRatingModel.packagesRequireRating = packagesRequireRating;
        RequestInfo.ServiceType serviceType = FP_AngularPageController.getServiceTypeForCallOut(packageRatingModel, null);
        System.assertEquals(RequestInfo.ServiceType.GET_3_QUOTE, serviceType);
    }

    /**
     * This method is for testing getting the service type for single package rate request
     * Assertions:
     * 1. Make the sure the ServiceType returned is RequestInfo.ServiceType.GET_QUOTE.
     **/
    @isTest
    public static  void testGetServiceTypeForCallOutRequestingSingleRate()
    {
        SMEQ_PackageRatingModel smeqPkgRatingModel = new SMEQ_PackageRatingModel();
        smeqPkgRatingModel.quoteID = 'testQuoteId';
        smeqPkgRatingModel.continuationReferenceID = '';
        SMEQ_QuotePackage qp = new SMEQ_QuotePackage();
        qp.packageID='testPkgId';
        qp.annualPremium=11;
        qp.airMilesEarned=22;
        qp.cglLimit=33;
        qp.propertyDeductible=44;
        SMEQ_PackageRatingModel packageRatingModel = new SMEQ_PackageRatingModel();
        List<SMEQ_QuotePackage> packagesRequireRating = new List<SMEQ_QuotePackage>();
        SMEQ_QuotePackage quotePackage1 = new SMEQ_QuotePackage();
        quotePackage1.packageID = 'P1';
        packagesRequireRating.add(quotePackage1);
        packageRatingModel.packagesRequireRating = packagesRequireRating;
        RequestInfo.ServiceType serviceType = FP_AngularPageController.getServiceTypeForCallOut(packageRatingModel, null);
        System.assertEquals(RequestInfo.ServiceType.GET_QUOTE, serviceType);
    }

    /**
     * This test method will ensure that the remoteAction function
     * correctly applies the deviation settings for a quote.
     *
     * Test scenarios:
     * 1. Apply a discount deviation for all three packages.
     * 2. Remove deviation from quote entirely.
     */
    public testMethod static void testApplyDeviationSetting()
    {
        Integer deviationPercent = 20;
        User u;
        Map<String, String> externalPackageTypeSelected;

        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            SMEQ_TestDataGenerator.initializeSMEMappings();
            externalPackageTypeSelected = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Package_Type_Selected__c');

            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();

            // Create a model which applies a discount for all three packages.
            // Test assertions:
            // 1. Deviation percentage is correct.
            // 2. Deviation type is correct.
            // 3. Standard package is selected.
            // 4. Option 1 package is selected.
            // 5. Option 2 package is selected.
            // 6. Deviation reason is set.
            SMEQ_PackageRatingModel smeqPkgRatingModel = new SMEQ_PackageRatingModel();
            smeqPkgRatingModel.quoteID = q.Id;
            smeqPkgRatingModel.deviatePercent = deviationPercent;
            smeqPkgRatingModel.deviateType = QuoteService.DEVIATION_TYPE_DISCOUNT;
            List<SMEQ_QuotePackage> packagesRequireRating = new List<SMEQ_QuotePackage>();
            SMEQ_QuotePackage quotePackage1 = new SMEQ_QuotePackage();
            quotePackage1.packageID = externalPackageTypeSelected.get(RiskService.PACKAGE_STANDARD);
            quotePackage1.deviate = true;
            packagesRequireRating.add(quotePackage1);
            SMEQ_QuotePackage quotePackage2 = new SMEQ_QuotePackage();
            quotePackage2.packageID = externalPackageTypeSelected.get(RiskService.PACKAGE_OPTION_1);
            quotePackage2.deviate = true;
            packagesRequireRating.add(quotePackage2);
            SMEQ_QuotePackage quotePackage3 = new SMEQ_QuotePackage();
            quotePackage3.packageID = externalPackageTypeSelected.get(RiskService.PACKAGE_OPTION_2);
            quotePackage3.deviate = true;
            packagesRequireRating.add(quotePackage3);
            smeqPkgRatingModel.packagesRequireRating = packagesRequireRating;

            System.assertEquals(
                FP_AngularPageController.RESPONSE_SUCCESS,
                FP_AngularPageController.applyDeviationSettings(JSON.serialize(smeqPkgRatingModel))
            );
            q = [
                SELECT id, Deviation_Percentage__c, Deviation_Type__c, Selected_Packages_to_Deviate__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(deviationPercent, q.Deviation_Percentage__c);
            System.assertEquals(QuoteService.DEVIATION_TYPE_DISCOUNT, q.Deviation_Type__c);
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_STANDARD));
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_OPTION_1));
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_OPTION_2));
            System.assertEquals(QuoteService.DEVIATION_BROKER_REASON, q.Deviation_Reason_s__c);

            // Create a model which removes deviation entirely.
            // Test assertions:
            // 1. Deviation percentage is null.
            // 2. Deviation type is null.
            // 3. There are no selected packages to deviate.
            smeqPkgRatingModel.deviatePercent = null;
            smeqPkgRatingModel.deviateType = null;
            for (SMEQ_QuotePackage qp : smeqPkgRatingModel.packagesRequireRating)
            {
                qp.deviate = false;
            }
            System.assertEquals(
                FP_AngularPageController.RESPONSE_SUCCESS,
                FP_AngularPageController.applyDeviationSettings(JSON.serialize(smeqPkgRatingModel))
            );
            q = [
                SELECT id, Deviation_Percentage__c, Deviation_Type__c, Selected_Packages_to_Deviate__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(null, q.Deviation_Percentage__c);
            System.assertEquals(null, q.Deviation_Type__c);
            System.assertEquals(null, q.Selected_Packages_to_Deviate__c);
        }
    }

    /**
     * This test method will ensure that the remoteAction function correctly resets a quote.
     *
     * Test scenarios:
     * 1. Prepare quote with one package.
     * 2. Prepare quote with three packages.
     */
    public testMethod static void testPrepareRating()
    {
        String alternativeSelectedPackage = RiskService.PACKAGE_OPTION_2;

        User u;
        Map<String, String> externalPackageTypeSelected;

        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            SMEQ_TestDataGenerator.initializeSMEMappings();
            externalPackageTypeSelected = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Package_Type_Selected__c');

            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {

            Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);

            // SMEQ_TestDataGenerator is not used here to cut down on the number of set up SOQL queries required.
            /*
            Case cse = new Case();
            insert cse;
            Quote__c q = new Quote__c();
            q.RecordTypeId = QuoteTypes.get('Open_Quote');
            q.Case__c = cse.Id;
            q.Canadian_Revenue__c = 1000;
            q.Package_Type_Selected__c = 'Standard';
            insert q;
*/
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(cse);
            // SMEQ_TestDataGenerator is not used here to cut down on the number of set up SOQL queries required.

            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            r.Coverage_Selected_Option_1__c = r.Coverage_Selected_Option_1__c + ';' +
                CoverageTriggerTest.COVERAGE_TYPE_EARTHQUAKE + ';' +
                CoverageTriggerTest.COVERAGE_TYPE_FLOOD + ';' +
                CoverageTriggerTest.COVERAGE_TYPE_SEWERBACKUP;
            r.Coverage_Selected_Option_2__c = r.Coverage_Selected_Option_1__c + ';' +
                CoverageTriggerTest.COVERAGE_TYPE_EARTHQUAKE + ';' +
                CoverageTriggerTest.COVERAGE_TYPE_FLOOD + ';' +
                CoverageTriggerTest.COVERAGE_TYPE_SEWERBACKUP;
            update r;

            List<Coverages__c> cs = [
                SELECT id, Limit_Value__c, Deductible_Value__c, Package_ID__c, Type__c, Selected__c, Risk__r.RecordTypeId
                FROM Coverages__c
                WHERE risk__r.quote__c = :q.Id
            ];
            for (Coverages__c c : cs)
            {
                // Assign dummy values.
                c.Limit_Value__c = CoverageTriggerTest.EARTHQUAKE_LIMIT_VALUE;
                c.Deductible_Value__c = CoverageTriggerTest.EARTHQUAKE_LIMIT_VALUE;
            }
            update cs;

            q.Package_Type_Selected__c = alternativeSelectedPackage;
            update q;

            Test.startTest();

            // Create a model which only has one package.
            // Test assertions:
            // 1. No changes made to quote.
            SMEQ_PackageRatingModel smeqPkgRatingModel = new SMEQ_PackageRatingModel();
            smeqPkgRatingModel.quoteID = q.Id;
            List<SMEQ_QuotePackage> packagesRequireRating = new List<SMEQ_QuotePackage>();
            SMEQ_QuotePackage quotePackage1 = new SMEQ_QuotePackage();
            quotePackage1.packageID = externalPackageTypeSelected.get(RiskService.PACKAGE_STANDARD);
            quotePackage1.deviate = true;
            packagesRequireRating.add(quotePackage1);
            smeqPkgRatingModel.packagesRequireRating = packagesRequireRating;

            System.assertEquals(
                FP_AngularPageController.RESPONSE_SUCCESS,
                FP_AngularPageController.prepareRating(JSON.serialize(smeqPkgRatingModel))
            );

            q = [
                SELECT id, Standard_Premium__c, Option_1_Premium__c, Option_2_Premium__c, Package_Type_Selected__c
                FROM Quote__c
                WHERE id = :q.id
            ];
            System.assertNotEquals(RiskService.PACKAGE_STANDARD, q.Package_Type_Selected__c, 'Package should not be reset.');

            r = [
                SELECT id, Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c
                FROM Risk__c
                WHERE id = :r.Id
            ];
            System.assert(r.Coverage_Selected_Option_1__c.contains(CoverageTriggerTest.COVERAGE_TYPE_EARTHQUAKE), 'Earthquake should not be removed.');
            System.assert(r.Coverage_Selected_Option_1__c.contains(CoverageTriggerTest.COVERAGE_TYPE_FLOOD), 'Flood should not be removed.');
            System.assert(r.Coverage_Selected_Option_1__c.contains(CoverageTriggerTest.COVERAGE_TYPE_SEWERBACKUP), 'Sewer backup should not be removed.');
            System.assert(r.Coverage_Selected_Option_2__c.contains(CoverageTriggerTest.COVERAGE_TYPE_EARTHQUAKE), 'Earthquake should not be removed.');
            System.assert(r.Coverage_Selected_Option_2__c.contains(CoverageTriggerTest.COVERAGE_TYPE_FLOOD), 'Flood should not be removed.');
            System.assert(r.Coverage_Selected_Option_2__c.contains(CoverageTriggerTest.COVERAGE_TYPE_SEWERBACKUP), 'Sewer backup should not be removed.');


            Map<String, Id> riskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
            for (Coverages__c c : [
                SELECT id, Limit_Value__c, Deductible_Value__c, Package_ID__c, Type__c, Selected__c, Risk__r.RecordTypeId
                FROM Coverages__c
                WHERE risk__r.quote__c = :q.Id
            ])
            {
                System.assertNotEquals(null, c.Deductible_Value__c, 'Deductible should not be reset.');
                System.assertEquals(c.Package_ID__c == alternativeSelectedPackage, c.Selected__c, 'Selected should not be reset.');

                if (c.Risk__r.RecordTypeId == riskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY) ||
                    (c.Type__c != RiskService.COVERAGE_BUILDING &&
                     c.Type__c != RiskService.COVERAGE_EQUIPMENT &&
                     c.Type__c != RiskService.COVERAGE_STOCK))
                {
                    System.assertNotEquals(null, c.Limit_Value__c, 'Limit should not be reset.');
                }
            }

            // Create a model which has three packages.
            // Test assertions:
            // 1. Quote package is reset.
            // 2. Risk coverages are reset.
            // 3. Coverage deductible is reset.
            // 4. Coverage selected is reset.
            // 5. Coverage limit is reset if default type.
            SMEQ_QuotePackage quotePackage2 = new SMEQ_QuotePackage();
            quotePackage2.packageID = externalPackageTypeSelected.get(RiskService.PACKAGE_OPTION_1);
            packagesRequireRating.add(quotePackage2);
            SMEQ_QuotePackage quotePackage3 = new SMEQ_QuotePackage();
            quotePackage3.packageID = externalPackageTypeSelected.get(RiskService.PACKAGE_OPTION_2);
            packagesRequireRating.add(quotePackage3);
            smeqPkgRatingModel.packagesRequireRating = packagesRequireRating;

            System.assertEquals(
                FP_AngularPageController.RESPONSE_SUCCESS,
                FP_AngularPageController.prepareRating(JSON.serialize(smeqPkgRatingModel))
            );

            q = [
                SELECT id, Standard_Premium__c, Option_1_Premium__c, Option_2_Premium__c, Package_Type_Selected__c
                FROM Quote__c
                WHERE id = :q.id
            ];
            System.assertEquals(RiskService.PACKAGE_STANDARD, q.Package_Type_Selected__c, 'Package should be reset.');

            r = [
                SELECT id, Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c
                FROM Risk__c
                WHERE id = :r.Id
            ];
            System.assert(!r.Coverage_Selected_Option_1__c.contains(CoverageTriggerTest.COVERAGE_TYPE_EARTHQUAKE), 'Earthquake should be removed.');
            System.assert(!r.Coverage_Selected_Option_1__c.contains(CoverageTriggerTest.COVERAGE_TYPE_FLOOD), 'Flood should be removed.');
            System.assert(!r.Coverage_Selected_Option_1__c.contains(CoverageTriggerTest.COVERAGE_TYPE_SEWERBACKUP), 'Sewer backup should be removed.');
            System.assert(!r.Coverage_Selected_Option_2__c.contains(CoverageTriggerTest.COVERAGE_TYPE_EARTHQUAKE), 'Earthquake should be removed.');
            System.assert(!r.Coverage_Selected_Option_2__c.contains(CoverageTriggerTest.COVERAGE_TYPE_FLOOD), 'Flood should be removed.');
            System.assert(!r.Coverage_Selected_Option_2__c.contains(CoverageTriggerTest.COVERAGE_TYPE_SEWERBACKUP), 'Sewer backup should be removed.');

            for (Coverages__c c : [
                SELECT id, Limit_Value__c, Deductible_Value__c, Package_ID__c, Type__c, Selected__c, Risk__r.RecordTypeId
                FROM Coverages__c
                WHERE risk__r.quote__c = :q.Id
            ])
            {
                System.assertEquals(null, c.Deductible_Value__c, 'Deductible should be reset.');
                System.assertEquals(c.Package_ID__c == RiskService.PACKAGE_STANDARD, c.Selected__c, 'Package should be reset.');

                if (c.Risk__r.RecordTypeId == riskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY) ||
                    (c.Type__c != RiskService.COVERAGE_BUILDING &&
                     c.Type__c != RiskService.COVERAGE_EQUIPMENT &&
                     c.Type__c != RiskService.COVERAGE_STOCK &&
                     c.Type__c != RiskService.COVERAGE_CONTENTS))
                {
                    System.assertEquals(null, c.Limit_Value__c, 'Limit should be reset.');
                }
            }

            Test.stopTest();
        }
    }

    /**
    * This method is for testing getting the service type for Finalize request
    * Assertions:
    * 1. Make the sure the ServiceType returned is RequestInfo.ServiceType.FINALIZE_QUOTE.
    **/
     @isTest
    public static  void testGetServiceTypeForCallOutRequestingFinalize() {
        SMEQ_PackageFinalizedModel packageFinalize = new SMEQ_PackageFinalizedModel();
        packageFinalize.selectedPackageID = 'testselectedPackageID';
        packageFinalize.quoteID = 'testquoteID';
        packageFinalize.quoteNumber = 'testquoteNumber';
        packageFinalize.continuationReferenceID = 'testcontinuationReferenceID';
        packageFinalize.finalizedDate = 'testquoteNumber';
        RequestInfo.ServiceType serviceType = FP_AngularPageController.getServiceTypeForCallOut(null, packageFinalize);
        System.assertEquals(RequestInfo.ServiceType.FINALIZE_QUOTE, serviceType);
    }

    /**
    * This method is to test initalizing the RSA_ESBService
    * Assertions:
    * 1. Make the sure the object returned is not null.
    **/
    @isTest
    public static  void testInitializeRsaEsbService()
    {


        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }

        System.runAs(testUser)
        {

            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(cse);
            String quoteID = q.Id;

            System.debug('QuoteID intialized' + quoteID);

            List<SME_Mapping__c> smeMappingCSList = new List<SME_Mapping__c>();
            SME_Mapping__c smeMapping1 = new SME_Mapping__c(
                Name = 'esbFlavor01',
                externalValue__c ='FL1',
                Field__c = 'esbFlavor',
                SFKey__c = 'Standard');
            SME_Mapping__c smeMapping2 = new SME_Mapping__c(
                Name = 'esbSegment01',
                externalValue__c ='CEI',
                Field__c = 'esbSegment',
                SFKey__c = 'Construction, Erection, Installation');
            smeMappingCSList.add(smeMapping1);
            smeMappingCSList.add(smeMapping2);
            try
            {
                Insert smeMappingCSList;
            }
            catch (DMLException dmle)
            {
                dmle.getMessage();
            }

            RSA_ESBService service = FP_AngularPageController.initializeRsaEsbService(quoteID, RequestInfo.ServiceType.GET_QUOTE);

            System.debug('Service:' + service);
            System.assertNotEquals(null, service);
        }

    }


    /**
    * This method is use to setup the test data for test cases to be executed in this class
    **/
    public static String setUpTestDataForCallOuts()
    {
        SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
        SIC_Code_Detail_Version__c sicCodeVersion = SMEQ_TestDataGenerator.getSicCodeDetailVersion();

        User testUser;

        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole())
        {
            UserService_Test.setupTestData();
            testUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }
        Case caseObject = SMEQ_TestDataGenerator.getOpenSprntCase();
        Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote(caseObject);
        return quote.id;
    }

    // Check if accept initial terms sets variables properly
    @isTest
    public static void testAcceptInitialTerms()
    {
        User u;

        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            SMEQ_TestDataGenerator.initializeSMEMappings();

            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            Contact c = [
                SELECT id, recordTypeId, Date_Accepted_Terms__c
                FROM Contact
                WHERE id = :u.ContactId
            ];

            FP_AngularPageController.acceptInitialTerms();

            c = [
            SELECT id, recordTypeId, Date_Accepted_Terms__c
            FROM Contact
            WHERE id = :u.ContactId
            ];
            System.assert(c.Date_Accepted_Terms__c != null);
        }
    }

    /**
     * This test method will test getQuoteByID() in FP_AngularPageController.
     */
    public static testMethod void testGetQuoteByID()
    {
        User u;

        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }

        System.runAs(u)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuotev2();

            Quote__c objQuote = SMEQ_TestDataGenerator.getOpenQuote();

            Risk__c locationRisk = [
                SELECT Id, Building_Value__c
                FROM Risk__c
                WHERE recordtype.developername = :RiskService.RISK_RECORDTYPE_LOCATION
            ];

            Account insuredClient = SMEQ_TestDataGenerator.getInsuredAccount();
            Contact cnt = SMEQ_TestDataGenerator.getProducerContact();

            Claim__c  claims = SMEQ_TestDataGenerator.getClaim(insuredClient, 1000, '01', 2015);

            Quote_Risk_Relationship__c quoteRiskRelation = new Quote_Risk_Relationship__c();
            quoteRiskRelation.Account__c = insuredClient.Id;
            quoteRiskRelation.Party_Number__c = 1;
            quoteRiskRelation.Party_Type__c = 'Loss Payee';
            quoteRiskRelation.Quote__c = objQuote.Id;
            quoteRiskRelation.Related_Risk__c = locationRisk.Id;
            quoteRiskRelation.Relationship_Type__c ='First Mortgagee';
            insert quoteRiskRelation;

            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            Test.startTest();
            QuoteRequestWrapper objQuoteReqWrapper = FP_AngularPageController.getQuoteById(objQuote.Id , cse.Id);

            System.assertNotEquals(null, objQuoteReqWrapper.quote, 'Quote can not be null.');
            System.assertNotEquals(null, objQuoteReqWrapper.cse, 'Case can not be null.');
            System.assertNotEquals(0, objQuoteReqWrapper.accList.size(), 'At least one account is expected.');
            if (quoteRiskRelation.Contact__c != null)
            {
                 System.assertNotEquals(0, objQuoteReqWrapper.contactList.size(), 'At least one contact is expected.');
            }
            System.assertNotEquals(0, objQuoteReqWrapper.liabilityRisks.size(), 'At least one liability risk is expected.');
            System.assertNotEquals(0, objQuoteReqWrapper.locationRisks.size(), 'At least one location risk is expected.');
            System.assertNotEquals(0, objQuoteReqWrapper.locationCoverage.size(), 'At least one location coverage is expected.');
            System.assertNotEquals(0, objQuoteReqWrapper.liabilityCoverage.size(), 'At least one liability  coverage is expected.');

            System.assertNotEquals(0, objQuoteReqWrapper.claims.size(), 'At least one claim is expected.');
            System.assertNotEquals(0, objQuoteReqWrapper.quoteRiskMapping.size(), 'At least one quote risk mapping is expected.');

            Test.stopTest();
        }
    }
}