public class CSIO_CommlSubLocation
{
    private Risk__c location;
    private Map<String, String> sas;
    
    public Construction construction;
    public BldgImprovements bldgImprovements;
    public BldgProtection bldgProtection;
    public BldgOccupancy bldgOccupancy;
    public List<Alarm> alarmAndSecurity;
    public Integer numberOfUnits;
    public List<ExposureInfo> exposureInfos;
    
    public CSIO_CommlSubLocation(Risk__c risk, Map<String, String> sas)
    {
        this.location = risk;
        this.sas = sas;
        convert();
    }
    
    public void convert()
    {
        this.construction = new Construction(location);
        String numberOfUnit = sas.get(SicQuestionService.CODE_NUMBER_OF_UNITS);
        if (numberOfUnit != null)
        {
            this.numberOfUnits = Integer.valueOf(numberOfUnit);
            this.construction.setNumberOfUnits(Integer.valueOf(numberOfUnit));
        }
        this.bldgImprovements = new BldgImprovements(location);
        this.bldgProtection = new BldgProtection(location);
        this.bldgOccupancy = new BldgOccupancy(location);
        this.alarmAndSecurity = new List<Alarm>();
        
        Alarm alarm1 = new Alarm();
        alarm1.alarmDescCd = 'csio:998';
        alarm1.alarmTypeCd = 'csio:2';
        this.alarmAndSecurity.add(alarm1);
        
        Alarm alarm2 = new Alarm();
        alarm2.alarmDescCd = 'csio:998';
        alarm2.alarmTypeCd = 'csio:1';
        this.alarmAndSecurity.add(alarm2);
        
        this.exposureInfos = new List<ExposureInfo>();
        String onsiteFacilities = sas.get(SicQuestionService.CODE_ONSITE_FACILITIES);
        if (onsiteFacilities != null)
        {
            for (String onF : onsiteFacilities.split(';'))
            {
                ExposureInfo ei = new ExposureInfo(onF);
                this.exposureInfos.add(ei);
            }
        }
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<CommlSubLocation LocationRef="' + location.Id + '">';
        xml += this.construction;
        xml += this.bldgImprovements;
        xml += this.bldgProtection;
        xml += this.bldgOccupancy;
        for (Alarm a : this.alarmAndSecurity)
        {
            xml += a;
        }
        
        for (ExposureInfo ei : this.exposureInfos)
        {
            xml += ei;
        }
        
        xml += '</CommlSubLocation>';
        return xml;
    }
    
    public class Construction
    {
        private Risk__c risk;
        
        private Integer yearBuilt;
        private Integer numStories;
        private Integer numUnits;
        private Integer csioBldgConstructionCd;
        
        public Construction(Risk__c risk)
        {
            this.risk = risk;
            convert();
        }
        
        private void convert()
        {
            Map<String, String> esbBldgConstructionCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbBldgConstructionCd');
            
            this.csioBldgConstructionCd = Integer.valueOf(esbBldgConstructionCd.get(this.risk.Construction_Type__c)); //Fixed?
            this.yearBuilt = Integer.valueOf(this.risk.Year_Built__c);
            this.numStories = (Integer) this.risk.Number_of_Stories__c;
        }
        
        public void setNumberOfUnits(Integer numUnits)
        {
            this.numUnits = numUnits;
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<Construction>';
            xml += '<YearBuilt>' + this.yearBuilt + '</YearBuilt>';
            xml += '<NumStories>' + this.numStories + '</NumStories>';
            if (this.numUnits != null)
            {
                xml += '<NumUnits>' + this.numUnits + '</NumUnits>';
            }
            xml += '<csio:BldgConstructionCd>' + this.csioBldgConstructionCd + '</csio:BldgConstructionCd>';
            xml += '</Construction>';
            
            return xml;
        }
    }
    
    public class BldgImprovements
    {
        private Risk__c risk;
        
        private Integer heatingImprovementYear;
        private Integer plumbingImprovementYear;
        private Integer roofingImprovementYear;
        private Integer wiringImprovementYear;
        
        public BldgImprovements(Risk__c risk)
        {
            this.risk = risk;
            convert();
        }
        
        private void convert()
        {
            this.heatingImprovementYear = (Integer) this.risk.Heating_Renovated_Year__c;
            this.plumbingImprovementYear = (Integer) this.risk.Plumbing_Renovated_Year__c;
            this.roofingImprovementYear = (Integer) this.risk.Roof_Renovated_Year__c;
            this.wiringImprovementYear = (Integer) this.risk.Electrical_Renovated_Year__c;
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<BldgImprovements>';
            if (this.heatingImprovementYear != null)
            {
                xml += ' <HeatingImprovementYear>' + this.heatingImprovementYear + '</HeatingImprovementYear>';
            }
            
            if (this.plumbingImprovementYear != null)
            {
                xml += ' <PlumbingImprovementYear>' + this.plumbingImprovementYear + '</PlumbingImprovementYear>';
            }
            
            if (this.roofingImprovementYear != null)
            {
                xml += ' <RoofingImprovementYear>' + this.roofingImprovementYear + '</RoofingImprovementYear>';
            }
            
            if (this.wiringImprovementYear != null)
            {
                xml += ' <WiringImprovementYear>' + this.wiringImprovementYear + '</WiringImprovementYear>';
            }
            xml += '</BldgImprovements>';
            
            return xml;
        }
    }
    
    public class BldgProtection
    {
        private Risk__c risk;
        
        public Measurement distanceToFireStation;
        public Measurement distanceToHydrant;
        public Integer sprinkleredPct;
        
        public BldgProtection(Risk__c risk)
        {
            this.risk = risk;
            convert();
        }
        
        private void convert()
        {
            this.distanceToFireStation = new Measurement('DistanceToFireStation', 'KMT');
            if (this.risk.Distance_To_Fire_Station__c == null)
            {
                Schema.DescribeFieldResult fireStationResult = Risk__c.Distance_To_Fire_Station__c.getDescribe();
                List<Schema.PicklistEntry> fireStationTypes = fireStationResult.getPicklistValues();
                this.risk.Distance_To_Fire_Station__c = fireStationTypes.get(fireStationTypes.size() - 1).getValue(); // Get the last value (worst case scenario / default value)
            }
            
            Map<String, String> esbDistanceToFireStation = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbDistanceToFireStation');
            this.distanceToFireStation.numUnits = Integer.valueOf(esbDistanceToFireStation.get(this.risk.Distance_To_Fire_Station__c));
            
            this.distanceToHydrant = new Measurement('DistanceToHydrant', 'MTR');
            if (this.risk.Distance_To_Fire_Hydrant__c == null)
            {
                Schema.DescribeFieldResult fireHydrantResult = Risk__c.Distance_To_Fire_Hydrant__c.getDescribe();
                List<Schema.PicklistEntry> fireHydrantTypes = fireHydrantResult.getPicklistValues();
                this.risk.Distance_To_Fire_Hydrant__c = fireHydrantTypes.get(fireHydrantTypes.size() - 1).getValue(); // Get the last value (worst case scenario / default value)
            }
            
            Map<String, String> esbDistanceToHydrant = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbDistanceToHydrant');
            this.distanceToHydrant.numUnits = Integer.valueOf(esbDistanceToHydrant.get(this.risk.Distance_To_Fire_Hydrant__c));
            
            this.sprinkleredPct = this.risk.Sprinkler_Coverage__c ? 100 : 0; // If the sprinkler coverage is true, then it is 100, 0 otherwise.
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<BldgProtection>';
            xml += this.distanceToFireStation;
            xml += this.distanceToHydrant;
            xml += ' <SprinkleredPct>' + this.sprinkleredPct + '</SprinkleredPct>';
            xml += '</BldgProtection>';
            
            return xml;
        }
    }
    
    public class BldgOccupancy
    {
        private Risk__c risk;
        
        private Measurement areaOccupied;
        
        public BldgOccupancy(Risk__c risk)
        {
            this.risk = risk;
            convert();
        }
        
        private void convert()
        {
            this.areaOccupied = new Measurement('AreaOccupied', 'FTK');
            this.areaOccupied.numUnits = (Integer) this.risk.Total_Occupied_Area__c;
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<BldgOccupancy>';
            xml += this.areaOccupied;
            xml += '</BldgOccupancy>';
            
            return xml;
        }
    }
    
    public class Measurement
    {
        public String tag;
        public Integer numUnits;
        public String unitMeasurementCd;
        
        public Measurement(String tag, String units)
        {
            this.tag = tag;
            this.unitMeasurementCd = units;
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<' + tag + '>';
            xml += ' <NumUnits>' + this.numUnits + '</NumUnits>';
            xml += ' <UnitMeasurementCd>' + this.unitMeasurementCd + '</UnitMeasurementCd>';
            xml += '</' + tag + '>';
            
            return xml;
        }
    }
    
    public class Alarm
    {
        public String alarmDescCd;
        public String alarmTypeCd;
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<AlarmAndSecurity>';
            xml += '<AlarmDescCd>' + this.alarmDescCd + '</AlarmDescCd>';
            xml += '<AlarmTypeCd>' + this.alarmTypeCd + '</AlarmTypeCd>';
            xml += '</AlarmAndSecurity>';
            
            return xml;
        }
    }
    
    public class ExposureInfo
    {
        public String onsiteFacility;
        
        private String exposureCd;
        
        public ExposureInfo(String onsiteFacility)
        {
            this.onsiteFacility = onsiteFacility;
            convert();
        }
        
        private void convert()
        {
            Map<String, String> dynamicQuestionOnsiteFacilities = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('dynamicQuestionOnsiteFacilities');
            this.exposurecd = dynamicQuestionOnsiteFacilities.get(this.onsiteFacility);
        }
        
        public override String toString()
        {
            String xml = '';
            
            if (this.exposureCd != null)
            {
                xml += '<ExposureInfo>';
                xml += ' <ExposureCd>' + this.exposureCd + '</ExposureCd>';
                xml += '</ExposureInfo>';
            }
            return xml;
        }
    }
    
}