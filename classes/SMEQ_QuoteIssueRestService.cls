@RestResource(urlMapping='/issueQuote')
global with sharing class SMEQ_QuoteIssueRestService {

    /**
     * Generic wrapper around response for returning Quote Response object
     */
    global class ResponseObjectWrapper extends SMEQ_RESTResponseModel {
        SMEQ_QuoteResponseModel payload = new SMEQ_QuoteResponseModel();
        public SMEQ_QuoteResponseModel getPayload() {
            return payload;
        }
    }

    @HttpPost
    global static ResponseObjectWrapper issueQuote(SMEQ_QuoteRequest quoteRequest) {
        ResponseObjectWrapper wrapper = new ResponseObjectWrapper();
        RestResponse res = RestContext.response;
        if (res != null) {
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Service  >>>>>>>' + quoteRequest);
        }
        SMEQ_QuoteResponseModel response = new SMEQ_QuoteResponseModel();
        SMEQ_QuoteDao quoteDao = new SMEQ_QuoteDao();
        try {

            System.debug('Calling  performIssueQuote >>>>>>>' + quoteRequest);
            quoteDao.performIssueQuote(quoteRequest);
            response.quoteRequest = quoteRequest;
            wrapper.payload = response;
            return wrapper;
        } catch (SMEQ_ServiceException se) {
            wrapper.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                    = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            wrapper.setErrors(errors);
            return wrapper;
        }
    }
}