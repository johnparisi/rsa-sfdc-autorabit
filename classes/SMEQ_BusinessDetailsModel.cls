global with sharing class SMEQ_BusinessDetailsModel
{
    public String id{public get; public set;}
    public String sicCode {public get; public set;}
    public String sicCodeDescription {public get; public set;}
    public String businessName {public get; public set;}
    public String DUNSNumber {public get; public set;}
    public Integer yearBusinessStarted{public get; public set;}
    public Double totalRevenue {public get; public set;}
    public Double canadianRevenue {public get; public set;}
    public Double usRevenue {public get; public set;}
    public Double foreignRevenue {public get; public set;}
    public Integer noOfClaims {public get; public set;}
    public Boolean isCurrentlyInsured {public get; public set;}
    public Boolean haveForeignRevenue {public get; public set;}
    public SMEQ_AddressModel mailingAddress {public get; public set;}
    public List<SMEQ_ClaimDetailsModel> claims {public get; public set;}
    public List<SMEQ_LocationDetailsModel> locationRisks {public get; public set;}
    public LiabilityRisk liabilityRisk {public get; public set;}
    public List<NonLiabilityRisk> nonLiabilityRisk {public get; public set;}
    public List<SMEQ_QuestionAnswerModel> questionAnswers {public get; public set;}
    public Integer noOfClaimFreeYears {public get; public set;}
    public List<String> packageSelectedForPrivacyBreach {public get; public set;}
    public List<String> packageExceededLLPScore {public get; public set;}
    public SMEQ_AddressModel amendmentMailingAddress {public get; public set;}
    
    /**
     * The purpose of this inner class is to map a non-location risk to the Salesforce data model.
     */
    public virtual class LiabilityRisk
    {
        public String id{get;set;}
        public List<SMEQ_CoverageDetailsModel> coverages{get;set;}
    }
    
    /**
     * The purpose of this subclass is to define the type of risk besides location or liability.
     */
    public class NonLiabilityRisk extends LiabilityRisk
    {
        
    }
}