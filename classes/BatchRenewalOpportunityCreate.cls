/**
* @File Name    :   BatchRenewalOpportunityCreate
* @Description  :   Batch Class for Creating Renewal Opportunity
* @Date Created :   01/28/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Batch
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       01/28/2017  Habiba Zaman  	Created the file/class
*/

global class BatchRenewalOpportunityCreate implements Database.Batchable<sObject> {
	
	String query;
	
	global BatchRenewalOpportunityCreate() {
		query = 'SELECT Id, Policy_Stage__c, Term_Expiry_Date__c FROM Policy__c WHERE'+
				' Policy_Stage__c =\'Waiting for Next Renewal Cycle\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		List<Policy__c> policyList = new List<Policy__c>();
   		if(scope.size() > 0){
			for(Policy__c p : (List<Policy__c>)scope){
				if(PolicyService.isExpiryDateWithinThreshold(p.Term_Expiry_Date__c)){
					p.Policy_Stage__c = 'Renewal Cycle Initialized';

					policyList.add(p);
				}
			}		

			update policyList;
		}
		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}