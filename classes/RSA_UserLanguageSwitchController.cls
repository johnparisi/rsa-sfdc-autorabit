global class RSA_UserLanguageSwitchController {
    @RemoteAction
    global static String updateUserLanguage(String userId, String language) {
        system.debug('=====updateUserLanguage======');  
        return 'success';
    }
}