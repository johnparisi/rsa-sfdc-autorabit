/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_CommlPolicy.
 */
@isTest
public class CSIO_CommlPolicy_Test
{
    @testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }

    static testMethod void testConvert()
    {
        User u;
        
        system.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        } 
        
        system.runAs(u)
        {
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            List<Claim__c> claims = new List<Claim__c>();
            claims.add(SMEQ_TestDataGenerator.getClaim(a, 100, '01', Date.today().year()));
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Total_Revenue__c = 10000;
            update q;
            q = [
                SELECT id, 
                Total_Revenue__c,
                Business_Source_ID__c, 
                Currently_Insured__c, 
                Case__r.Offering_Project__c,
                ePolicy__c, 
                Quote_Number__c, 
                Case__r.CaseNumber, 
                Claim_Free_Years__c, 
                Policy_Payment_Plan_Type__c,
                Calculated_Deviation__c, 
                Deviation_Reason_s__c, 
                Language__c,
                Retrieved_Package_Code__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            Map<String, String> esbSegment = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
            Map<String, String> esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');
            
            String segment;
            for (String s : esbSegment.keySet())
            {
                segment = s;
                break;
            }
            RequestInfo ri = new RequestInfo(
                RequestInfo.ServiceType.GET_QUOTE,
                'Standard',
                segment
            );
            
            List<SOQLDataSet.Risk> policyRisks = new List<SOQLDataSet.Risk>();
            CSIO_CommlPolicy ccp = new CSIO_CommlPolicy(ri, claims, q, policyRisks);
            String underwriterId;
            if (ri.getUnderwriterId() != null)
            {
                underwriterId = ri.getUnderwriterId().FederationIdentifier;
            }
            
            String userRegion = UserService.getRegion(UserInfo.getUserId())[0];
            String companyCd = userRegion == UserService_Test.REGION_1 ? CSIO_CommlPolicy.companyCd_CNS : CSIO_CommlPolicy.companyCd_RSA;
            
            String output = '';
            output += ccp;
            /*
            System.assertEquals(
                '<CommlPolicy id="' + q.Id + '">' +
                ' <LOBCd>csio:1</LOBCd>' +
                ' <LanguageCd>' + UserService.getFormattedUserLanguage() + '</LanguageCd>' +
                (underwriterId == null ? '' :  
                 ' <UnderwritingDecisionInfo>' +
                 '  <UnderwriterId>' + underwriterId + '</UnderwriterId>' +
                 ' </UnderwritingDecisionInfo>') +
                '<Loss>' +
                ' <LossDt>' + claims[0].year__c + '-' + claims[0].month__c + '-01</LossDt>' +
                ' <LossCauseCd>999</LossCauseCd>' +
                ' <csio:CompanyCd>ABI</csio:CompanyCd>' +
                '<LossPayment>' +
                ' <TotalPaidAmt>' +
                '  <Amt>' + claims[0].amount__c + '</Amt>' +
                ' </TotalPaidAmt>' +
                '</LossPayment>' +
                '</Loss>' +
                ' <NumLosses>'+claims.size()+'</NumLosses>'+
                (underwriterId == null ? '' :  
                 ' <UnderwritingDecisionInfo>' +
                 '   <UnderwriterId>' + underwriterId + '</UnderwriterId>' +
                 ' </UnderwritingDecisionInfo>') +
                (!q.Currently_Insured__c ? '' :
                 '<OtherOrPriorPolicy>' +
                 ' <PolicyCd>Prior</PolicyCd>' +
                 ' <csio:CompanyCd>ROY</csio:CompanyCd>' +
                 '</OtherOrPriorPolicy>' +
                  (new CSIO_QuoteInfo(q.Quote_Number__c)) +
                 ' <csio:CompanyCd>' + companyCd + '</csio:CompanyCd>'+
                 '<CommlPolicySupplement>' +
                ' <AnnualSalesAmt>' +
                '  <Amt>' + q.Total_Revenue__c + '</Amt>' +
                ' </AnnualSalesAmt>' +
                (new CSIO_CommlPolicy.CreditOrSurcharge(q, ri)) +
                '</CommlPolicySupplement>' +
                '<CommlCoverage>' +
                ' <CoverageCd>csio:2</CoverageCd>' +
                ' <CommlCoverageSupplement>' +
                '  <ClaimsMadeInfo>' +
                '   <ClaimsMadeInd>' + (claims.size() == 0 ? 0 : 1) + '</ClaimsMadeInd>' +
                '  </ClaimsMadeInfo>' +
                ' </CommlCoverageSupplement>' +
                '</CommlCoverage>' +
                ' <rsa:Segment>' + esbSegment.get(ri.sgmtType) + '</rsa:Segment>' +
                ' <rsa:Flavour>' + ri.getPackageType() + '</rsa:Flavour>' +
                '<rsa:YearsClaimsFree>' + (q.Claim_Free_Years__c == null ? 0 : q.Claim_Free_Years__c) + '</rsa:YearsClaimsFree>') +
                ' <rsa:SalesforceCaseNumber>' + q.Case__r.CaseNumber +  '</rsa:SalesforceCaseNumber>' +
                '</CommlPolicy>',
                output
            );
            */
        }
    }
    
    static testMethod void testConvertPacificRegion()
    {
        User u;
        
        system.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        } 
        
        system.runAs(u)
        {
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            List<Claim__c> claims = new List<Claim__c>();
            claims.add(SMEQ_TestDataGenerator.getClaim(a, 100, '01', Date.today().year()));
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Total_Revenue__c = 10000;
            update q;
            q = [
                SELECT id, Total_Revenue__c, Currently_Insured__c, Case__r.Offering_Project__c, Business_Source_ID__c,
                ePolicy__c, Quote_Number__c, Case__r.CaseNumber, Claim_Free_Years__c, Policy_Payment_Plan_Type__c, Retrieved_Package_Code__c,
                Calculated_Deviation__c, Deviation_Reason_s__c, Language__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            Map<String, String> esbSegment = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
            Map<String, String> esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');
            
            String segment;
            for (String s : esbSegment.keySet())
            {
                segment = s;
                break;
            }
            RequestInfo ri = new RequestInfo(
                RequestInfo.ServiceType.GET_QUOTE,
                'Standard',
                segment
            );
            
            List<SOQLDataSet.Risk> policyRisks = new List<SOQLDataSet.Risk>();
            CSIO_CommlPolicy ccp = new CSIO_CommlPolicy(ri, claims, q, policyRisks);
            String underwriterId;
            if (ri.getUnderwriterId() != null)
            {
                underwriterId = ri.getUnderwriterId().FederationIdentifier;
            }
            String userRegion = UserService.getRegion(UserInfo.getUserId())[0];
            String companyCd = userRegion == UserService_Test.REGION_1 ? CSIO_CommlPolicy.companyCd_CNS : CSIO_CommlPolicy.companyCd_RSA;
            
            String output = '';
            output += ccp;
            /*
            System.assertEquals(
                '<CommlPolicy id="' + q.Id + '">' +
                ' <LOBCd>csio:1</LOBCd>' +
                ' <LanguageCd>' +  UserService.getFormattedUserLanguage() + '</LanguageCd>' +
                '<Loss>' +
                ' <LossDt>' + claims[0].year__c + '-' + claims[0].month__c + '-01</LossDt>' +
                ' <LossCauseCd>999</LossCauseCd>' +
                ' <csio:CompanyCd>ABI</csio:CompanyCd>' +
                '<LossPayment>' +
                ' <TotalPaidAmt>' +
                '  <Amt>' + claims[0].amount__c + '</Amt>' +
                ' </TotalPaidAmt>' +
                '</LossPayment>' +
                '</Loss>' +
                ' <NumLosses>'+claims.size()+'</NumLosses>'+
                 (underwriterId == null ? '' :
                   ' <UnderwritingDecisionInfo>' +
                   '   <UnderwriterId>' + underwriterId + '</UnderwriterId>' +
                   ' </UnderwritingDecisionInfo>') +
                (!q.Currently_Insured__c ? '' :
                  '<OtherOrPriorPolicy>' +
                  ' <PolicyCd>Prior</PolicyCd>' +
                  ' <csio:CompanyCd>ROY</csio:CompanyCd>' +
                  '</OtherOrPriorPolicy>' +  
                   (new CSIO_QuoteInfo(q.Quote_Number__c)) +
                    ' <csio:CompanyCd>' + companyCd + '</csio:CompanyCd>'+
                '<CommlPolicySupplement>' +
                ' <AnnualSalesAmt>' +
                '  <Amt>' + q.Total_Revenue__c + '</Amt>' +
                ' </AnnualSalesAmt>' +
                (new CSIO_CommlPolicy.CreditOrSurcharge(q, ri)) +
                '</CommlPolicySupplement>' +
                '<CommlCoverage>' +
                ' <CoverageCd>csio:2</CoverageCd>' +
                ' <CommlCoverageSupplement>' +
                '  <ClaimsMadeInfo>' +
                '   <ClaimsMadeInd>' + (claims.size() == 0 ? 0 : 1) + '</ClaimsMadeInd>' +
                '  </ClaimsMadeInfo>' +
                ' </CommlCoverageSupplement>' +
                '</CommlCoverage>' +
                ' <rsa:Segment>' + esbSegment.get(ri.sgmtType) + '</rsa:Segment>' +
                ' <rsa:Flavour>' + ri.getPackageType() + '</rsa:Flavour>' +
                '<rsa:YearsClaimsFree>' + (q.Claim_Free_Years__c == null ? 0 : q.Claim_Free_Years__c) + '</rsa:YearsClaimsFree>') +
                ' <rsa:SalesforceCaseNumber>' + q.Case__r.CaseNumber +  '</rsa:SalesforceCaseNumber>' +
                '</CommlPolicy>',
                output
            );
            */
        }
        
    }
    
    static testMethod void testCreditOrSurcharge()
    {
        Decimal deviationPercentage = 10;
        String deviationReason = 'Underwriter rate adjustment � increase';
        
        User uwLevel1 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel1)
        {
            Map<String, String> esbSegment = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
            String segment;
            for (String s : esbSegment.keySet())
            {
                segment = s;
                break;
            }
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q = [
                SELECT id, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c, Selected_Packages_to_Deviate__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            // Test Scenario: Packages Selected to Deviate only for standard.
            // 
            // Test assertions:
            // 1. CreditOrSurcharge element is included for Standard package.
            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_SURCHARGE;
            q.Deviation_Percentage__c = deviationPercentage;
            q.Deviation_Reason_s__c = deviationReason;
            q.Selected_Packages_to_Deviate__c = RiskService.PACKAGE_STANDARD;
            update q;
            
            q = [
                SELECT id, Calculated_Deviation__c, Deviation_Reason_s__c, Selected_Packages_to_Deviate__c, ePolicy__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            RequestInfo ri = new RequestInfo(
                RequestInfo.ServiceType.GET_QUOTE,
                RiskService.PACKAGE_STANDARD,
                segment
            );
            Map<String, String> esbDeviationReason = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbDeviationReason');
            
            CSIO_CommlPolicy.CreditOrSurcharge ccpcos = new CSIO_CommlPolicy.CreditOrSurcharge(q, ri);
            String output = '';
            output += ccpcos;
            System.assertEquals(
                '<CreditOrSurcharge>' +
                ' <NumericValue>' +
                '  <FormatPct>' + deviationPercentage.setScale(0) + '</FormatPct>' +
                ' </NumericValue>' +
                ' <rsa:DeviateReason>' + esbDeviationReason.get(deviationReason) + '</rsa:DeviateReason>' +
                '</CreditOrSurcharge>',
                output
            );
            
            // Test Scenario: Packages Selected to Deviate only for standard.
            // 
            // Test assertions:
            // 1. CreditOrSurcharge element is not included for a non-Standard package.
            ri = new RequestInfo(
                RequestInfo.ServiceType.GET_QUOTE,
                RiskService.PACKAGE_OPTION_1,
                segment
            );
            ccpcos = new CSIO_CommlPolicy.CreditOrSurcharge(q, ri);
            output = '';
            output += ccpcos;
            System.assertEquals(
                '',
                output
            );
        }
    }
    
    static testMethod void testConvertCliamFreeYears()
    {
        User u;
        
        system.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        } 
        
        system.runAs(u)
        {
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            List<Claim__c> claims = new List<Claim__c>();
            claims.add(SMEQ_TestDataGenerator.getClaim(a, 100, '01', Date.today().year()));
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Total_Revenue__c = 10000;
            update q;
            q = [
                SELECT id, Total_Revenue__c, Currently_Insured__c, Case__r.Offering_Project__c,
                Calculated_Deviation__c, Deviation_Reason_s__c, case__r.CaseNumber
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            q.Currently_Insured__c= true;
            q.Claim_Free_Years__c = 5;
            update q;
            
            
            Map<String, String> esbSegment = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
            Map<String, String> esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');
            
            String segment;
            for (String s : esbSegment.keySet())
            {
                segment = s;
                break;
            }
            RequestInfo ri = new RequestInfo(
                RequestInfo.ServiceType.GET_QUOTE,
                'Standard',
                segment
            );
            Claim_Types_And_Codes__c ctc = new Claim_Types_And_Codes__c();
            ctc.Name = 'CT10';
            ctc.Claim_Code__c = 'rsa:FI';
            ctc.Claim_Type__c = 'Financial Loss';
            insert ctc;
            List<SOQLDataSet.Risk> policyRisks = new List<SOQLDataSet.Risk>();
            CSIO_CommlPolicy ccp = new CSIO_CommlPolicy(ri, claims, q, policyRisks);
            String underwriterId;
            if (ri.getUnderwriterId() != null)
            {
                underwriterId = ri.getUnderwriterId().FederationIdentifier;
            }
            
            String userRegion = UserService.getRegion(UserInfo.getUserId())[0];
            String companyCd = userRegion == UserService_Test.REGION_1 ? CSIO_CommlPolicy.companyCd_CNS : CSIO_CommlPolicy.companyCd_RSA;
            
            String output = '';
            output += ccp;
            /*
            System.assertEquals(
                '<CommlPolicy id="' + q.Id + '">' +
                ' <LOBCd>csio:1</LOBCd>' +
                ' <LanguageCd>en</LanguageCd>' +
                (underwriterId == null ? '' :  
                 ' <UnderwritingDecisionInfo>' +
                 '  <UnderwriterId>' + underwriterId + '</UnderwriterId>' +
                 ' </UnderwritingDecisionInfo>') +
                '<Loss>' +
                ' <LossDt>' + claims[0].year__c + '-' + claims[0].month__c + '-01</LossDt>' +
                ' <LossCauseCd>999</LossCauseCd>' +
                ' <csio:CompanyCd>ABI</csio:CompanyCd>' +
                '<LossPayment>' +
                ' <TotalPaidAmt>' +
                '  <Amt>' + claims[0].amount__c + '</Amt>' +
                ' </TotalPaidAmt>' +
                '</LossPayment>' +
                '</Loss>' +
                ' <NumLosses>'+claims.size()+'</NumLosses>'+
                '<OtherOrPriorPolicy>' +
                ' <PolicyCd>Prior</PolicyCd>' +
                ' <csio:CompanyCd>ROY</csio:CompanyCd>' +
                '</OtherOrPriorPolicy>' +
                 ' <csio:CompanyCd>' + companyCd + '</csio:CompanyCd>'+   
                '<CommlPolicySupplement>' +
                ' <AnnualSalesAmt>' +
                '  <Amt>' + q.Total_Revenue__c + '</Amt>' +
                ' </AnnualSalesAmt>' +
                (new CSIO_CommlPolicy.CreditOrSurcharge(q, ri)) +
                '</CommlPolicySupplement>' +
                '<CommlCoverage>' +
                ' <CoverageCd>csio:2</CoverageCd>' +
                ' <CommlCoverageSupplement>' +
                '  <ClaimsMadeInfo>' +
                '   <ClaimsMadeInd>' + (claims.size() == 0 ? 0 : 1) + '</ClaimsMadeInd>' +
                '  </ClaimsMadeInfo>' +
                ' </CommlCoverageSupplement>' +
                '</CommlCoverage>' +
                ' <rsa:Segment>' + esbSegment.get(ri.sgmtType) + '</rsa:Segment>' +
                ' <rsa:Flavour>' + ri.getPackageType() + '</rsa:Flavour>' +
                '<rsa:YearsClaimsFree>' + (q.Claim_Free_Years__c == null ? 0 : q.Claim_Free_Years__c) + '</rsa:YearsClaimsFree>' +
                ' <rsa:SalesforceCaseNumber>' + q.Case__r.CaseNumber +  '</rsa:SalesforceCaseNumber>' +
                '</CommlPolicy>',
                output
            ); */
        }
    }
}