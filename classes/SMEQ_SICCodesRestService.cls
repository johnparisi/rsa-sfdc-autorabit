/**
 * SIC code REST service returns all SME Broker SIC codes (cut down variant of SIC Code data objects)
 *
 * Returns an array of SMEQ_SICDataModel
 */
@RestResource(urlMapping='/sic/getSICCodes/*')
global with sharing class SMEQ_SICCodesRestService
{
    String DEFAULT_LANG = 'en';
    
    global class ResponseObjectWrapper extends SMEQ_RESTResponseModel
    {
        List<SMEQ_SICDataModel> payload = new List<SMEQ_SICDataModel>();
    }
    
    @HttpGet
    global static ResponseObjectWrapper getSICCodes()
    {
        ResponseObjectWrapper wrapper = new ResponseObjectWrapper();
        RestResponse res = RestContext.response;
        if (res != null)
        {
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Service  >>>>>>>');
        }
        
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        Map<String, String> params = RestContext.request.params;
        
        try
        {
            wrapper.payload = sicCodesDao.getAllSICCodes();
            System.debug('### ' + wrapper.payload);
            return wrapper;
        }
        catch (SMEQ_ServiceException se)
        {
            wrapper.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            wrapper.setErrors(errors);
            return wrapper;
        }
    }
}