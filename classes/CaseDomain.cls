/*
Case Object Domain
Author: Stephen Piercey
Date: May 31 2016

The object domain is where the real work happens, this is where we will apply the actual business logic. 
We have the main Object Domain which should handle all common processing. 
Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
just implement logic in the appopriate method. 
*/

public with sharing class CaseDomain {
	public void beforeInsert(List<Case> records){
	    if(!SLID_ESB_ServiceConstants.isSLID){
            CaseService.updateCaseFields(records, null);
            CaseService.setCaseRegion(records);
            CaseService.populateSICCodeDetailFields(records);
        }
        SLID_SetReferralCaseOwner.SetReferralCaseOwner(records);
        SLID_SetSourceSystemUser.SetSourceSystemUser(records);
	}

	public void beforeUpdate(List<Case> records, Map<Id, Case> oldRecords){
	    if(!SLID_ESB_ServiceConstants.isSLID){
            CaseService.updateCaseFields(records, oldRecords.values());
            CaseService.populateSICCodeDetailFields(records);
            //CaseService.setCaseRegion(records);
		}
        SLID_SetSourceSystemUser.SetSourceSystemUser(records);
	}

	//public void afterInsert(List<Case> records){

	//}

	//public void afterUpdate(List<Case> records, Map<Id, Case> oldRecords){

	//}

	//public void setRecordType(List<Case> records){
	//}
    public class PIDomain{
        public void beforeInsert(List<Case> records){
            CaseService.setPICategory(records);           
            CaseService.setCaseTransType(records);
			CaseService.setCaseSubPriRenewal(records);
            CaseService.setCasePolicy(records);
			CaseService.setCaseFAPriority(records);
        }
    }
	public class SMEDomain{
        public void beforeInsert(List<Case> records){
        	CaseService.setCaseBoost(records);
        }

        public void beforeUpdate(List<Case> records, Map<Id, Case> oldRecords){
            if(!SLID_ESB_ServiceConstants.isSLID){
                CaseService.setAcceptedPriorToQuoteFields(records, oldRecords);
                CaseService.setBoundFields(records, oldRecords);
                CaseService.setCaseBoost(records);
                CaseService.closeMilestones(records, oldRecords);
                CaseService.convertTriageCasesToSMEQCases(records, oldRecords);
            }
            //Success - CISME_Int : Create referral child cases
            SLID_CreateReferralCases.CreateReferralCases(records, oldRecords);
            //Success - CISME_Int : Update case status to Quoted
            SLID_CaseToQuoted.updateStatusToQuoted(records, oldRecords);
        }

        //public void afterInsert(List<Case> records){

        //}

        //public void afterUpdate(List<Case> records, Map<Id, Case> oldRecords){
        //}
    }

    public class SMEQDomain
    {
        public void beforeInsert(List<Case> records)
        {
            if(!SLID_ESB_ServiceConstants.isSLID){
                CaseService.setCaseBoost(records);
                //CaseService.insertUpdateBrokerPriority(records);
            }
        }

        public void beforeUpdate(List<Case> records, Map<Id, Case> oldRecords)
        {
            if(!SLID_ESB_ServiceConstants.isSLID){
                //CaseService.setCaseBoost(records);
                CaseService.setCaseHasSubBrokerage(records, oldRecords);
                CaseService.setCaseSelectedBrokerageNumber(records, oldRecords);
                //CaseService.calculatePriorityBrokerCase(records,oldRecords);
            }
        }

        //public void afterInsert(List<Case> records){

        //}

        public void afterUpdate(List<Case> records, Map<Id, Case> oldRecords)
        {
            CaseService.generateSMEQuoteForTriageCases(records, oldRecords);
        }
    }

	public class GSLDomain{
		public void beforeInsert(List<Case> records){

		}

		public void beforeUpdate(List<Case> records, Map<Id, Case> oldRecords){
		}

		//public void afterInsert(List<Case> records){

		//}

		//public void afterUpdate(List<Case> records, Map<Id, Case> oldRecords){

		//}
	}

    public class GSLReferralDomain{

        public void afterInsert(List<Case> records)
        {
            if(CaseTriggerHandler.referralEmailFirstRun) {
                CaseService.sendNewReferralCaseEmail(records);
                CaseTriggerHandler.referralEmailFirstRun = false;
            }
        }

        public void afterUpdate(List<Case> records, Map<Id, Case> oldRecords) {

            List<Case> changedRecords = Utils.getChangedObjects(records, oldRecords, new List<String>{'Status'});

            if(CaseTriggerHandler.referralEmailFirstRun) {
                CaseService.sendReferralCaseStatusUpdatedEmail(changedRecords);
                CaseTriggerHandler.referralEmailFirstRun = false;
            }
        }
	}
}