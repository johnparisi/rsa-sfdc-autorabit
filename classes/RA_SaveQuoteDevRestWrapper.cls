@RestResource(urlMapping='/saveQuote/*')

global with sharing class RA_SaveQuoteDevRestWrapper {
    
    @HttpPost
    global static Boolean saveQuote(string caseId) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type','application/json');
        }
           
        //SaveResponse response = FP_AngularPageController.saveQuoteTransaction(caseId);
        return true;
	}
}