public with sharing class SIC_Code_Detail_VersionHandler {
    public void beforeInsert(List<SIC_Code_Detail_Version__c> records){
        new SIC_Code_Detail_VersionDomain().beforeInsert(records);
    }

    public void beforeUpdate(List<SIC_Code_Detail_Version__c> records, Map<Id, SIC_Code_Detail_Version__c> oldRecords){        
        new SIC_Code_Detail_VersionDomain().beforeUpdate(records, oldRecords);
    }
}