global with sharing class SMEQ_PackageRatingModel
{
    public String quoteID {get; set;}
    public String continuationReferenceID {get; set;}
    public Integer deviatePercent {get; set;}
    public String deviateType {get; set;}
    public List<SMEQ_QuotePackage> packagesRequireRating {get; set;}
    public Boolean esbErrorFlag;
    public String esbErrorMessage;
}