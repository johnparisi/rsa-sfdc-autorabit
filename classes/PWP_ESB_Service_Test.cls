@isTest
private Class PWP_ESB_Service_Test {

    static testMethod void testLogging(){}

    static testMethod void testHandleESBPayload1(){
        try{
            Profile SysAdmin = PWP_TestDataFactory.getSystemAdminProfile();
            User adminUser = PWP_TestDataFactory.createAdminUser1(SysAdmin);

            System.runAs(adminUser){
                Test.startTest();
                PWP_ESB_Service.CallData pwpPayloadData = PWP_TestDataFactory.createPWPESBPayload();

                //Execute the webservice call
                PWP_ESB_Service.ServiceResponse serviceResponse = PWP_ESB_Service.handleESBPayload(pwpPayloadData);
                Test.stopTest();
            }
        }
        catch(Exception ex){
            system.debug('Exception ocurred -->'+ex);
        }
    }

    static testMethod void testHandleESBPayloadForException(){
        try{
            Profile SysAdmin = PWP_TestDataFactory.getSystemAdminProfile();
            User adminUser = PWP_TestDataFactory.createAdminUser1(SysAdmin);

            System.runAs(adminUser){
                Test.startTest();
                PWP_ESB_Service.CallData pwpPayloadData = PWP_TestDataFactory.createPWPPayloadDataForException();

                //Execute the webservice call
                PWP_ESB_Service.ServiceResponse serviceResponse = PWP_ESB_Service.handleESBPayload(pwpPayloadData);
                Test.stopTest();
            }
        }
        catch(Exception ex){
            system.debug('Exception ocurred -->'+ex);
        }
    }
}