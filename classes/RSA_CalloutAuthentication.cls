/*
 * How to add credentials to the customs settings:
 * 
 * Run this in anonymous apex:
String context = <replace with context>;
String username = <replace with username>;
String password = <replace with password>;
String endpoint = <replace with endpoint>;

RSA_CalloutAuthentication rca = new RSA_CalloutAuthentication(context);
rca.userName = username;
rca.password = password;
rca.endpoint = endpoint;
rca.encode();
 * 
 * 
 */
public class RSA_CalloutAuthentication
{
    private String callName;
    
	public String userName;
    public String password;
    public String endpoint;
    
    private External_Connections__c ec;
    
    public RSA_CalloutAuthentication(String callName)
    {
        this.callName = callName;
        
        this.ec = External_Connections__c.getValues(callName);
        if (ec == null)
        {
            this.ec = new External_Connections__c();
        }
        else
        {
            decode();
        }
    }
    
    public void decode()
    {
        if (ec.UserName__c != null && ec.Password__c != null && ec.endPoint__c != null)
        {
            this.username = EncodingUtil.base64Decode(ec.UserName__c).toString();
            this.password = EncodingUtil.base64Decode(ec.Password__c).toString();
            this.endpoint = EncodingUtil.base64Decode(ec.Endpoint__c).toString();
        }
    }
    
    public void encode()
    {
        ec.Name = this.callName;
        ec.UserName__c = EncodingUtil.base64Encode(Blob.valueOf(this.userName));
        ec.Password__c = EncodingUtil.base64Encode(Blob.valueOf(this.password));
        ec.Endpoint__c = EncodingUtil.base64Encode(Blob.valueOf(this.endpoint));
        upsert ec;
    }
    
    public static String getAuthorizationHeader(String context)
    {
        RSA_CalloutAuthentication rca = new RSA_CalloutAuthentication(context);
        Blob headerValue = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(rca.userName)) + ':' + EncodingUtil.base64Encode(Blob.valueOf(rca.password)));
        return 'Basic ' + EncodingUtil.base64Encode(headerValue);
    }
    
    public static String getEndpoint(String context)
    {
        RSA_CalloutAuthentication rca = new RSA_CalloutAuthentication(context);
        return rca.endpoint;
    }
}