public class CSIO_CommlPropertyInfo
{
	public Risk__c risk;
    public List<Coverages__c> coverages;
    public List<Coverages__c> removedCoverages;
    
    public List<CSIO_CommlCoverage> commlCoverages;
    public List<CSIO_CommlCoverage> deselectedCoverages;
    
    public Id riskId;
    
    public CSIO_CommlPropertyInfo(SOQLDataSet.Risk risk)
    {
        this.risk = risk.risk;
        this.coverages = risk.coverages;
        this.removedCoverages = risk.removedCoverages;
        convert();
    }
    
    public void convert()
    {
        this.commlCoverages = new List<CSIO_CommlCoverage>();
        for (Coverages__c c : coverages)
        {
            CSIO_CommlCoverage cc = new CSIO_CommlCoverage(c);
            this.commlCoverages.add(cc);
        }
        this.deselectedCoverages = new List<CSIO_CommlCoverage>();
        for (Coverages__c c : removedCoverages)
        {
            CSIO_CommlCoverage cc = new CSIO_CommlCoverage(c);
            this.deselectedCoverages.add(cc);
        }
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<CommlPropertyInfo LocationRef="' + this.risk.Id + '">';
        for (CSIO_CommlCoverage cc : this.commlCoverages)
        {
            xml += cc;
        }
        
        for (CSIO_CommlCoverage cc : this.deselectedCoverages)
        {
            xml += cc;
        }
        
        // Special coverage for scenarios where Building, Equipment, and Stock coverages are collected.
        // In the case of Condo package, Equipment and Stock are not collected, thus excluded.
        if (SOQLDataSet.scdv.Building_Required__c && SOQLDataSet.scdv.ePolicy_Package__c != 'Condo')
        {
            xml += '<CommlCoverage>';
            xml += ' <CoverageCd>rsa:POED</CoverageCd>';
            xml += ' <CoverageDesc>POED</CoverageDesc>';
            xml += ' <rsa:Indicator>1</rsa:Indicator>';
            xml += '</CommlCoverage>';
        }
        xml += '</CommlPropertyInfo>';
        
        return xml;
    }
}