@isTest (SeeAllData=False)
private class SLID_BrokerMappingBatchExecution_Test {

    static testMethod void testBrokerMappingBatch() {
        // Create an account 
        Account acct = new Account();
        acct.RecordTypeId='012o0000000AgDrAAK';
        acct.name = 'Test1';
        acct.accountNumber = '0119274543';
        acct.bkrAccount_Status__c='Active';
        insert acct;
        
        //Creating test data for the Stage Broker Object
        Broker_Stage__c brokerData = new Broker_Stage__c();
        brokerData.account__c = acct.id;
        brokerData.ADDRIND1__c = 'P';
        brokerData.ADDRNO1__c = '169';
        brokerData.AGENT_CITY__c = 'GUYSBOROUGH';
        brokerData.AGENT_POSTAL_CODE__c = 'B0H1N0';
        brokerData.AGENT_STATE__c = 'NS';
        brokerData.AGENT_STREET__c = '9996 HIGHWAY 16   PO BOX 169';
        brokerData.AGENT__c = 703830514;
        brokerData.BROKER_NUMBER__c = '150383074';
        brokerData.PARENT1_BROKER_NUMBER__c = '50205887';
        brokerData.PARENT2_BROKER_NUMBER__c = '';
        brokerData.PARENT3_BROKER_NUMBER__c ='';
        brokerData.NATLTERR__c = 'NAT1';
        brokerData.ACTIVTO__c = 99999999;
        brokerData.BRANCH__c = 8088;
        brokerData.TEAM__c = '7088';
        brokerData.SUBCOY__c = 4;
        brokerData.STREETNO__c = '12';
        brokerData.STREET__c = '34';
        brokerData.STREETSU__c = '56';
        brokerData.STREETDIR__c = '78';
        brokerData.ADDRIND1__c = 'A';
        brokerData.ADDRIND2__c = 'B';
        insert brokerData;
        
        createMAP_NATLTERRrecords();
        createMAP_Mrecords();
        create_MAP_CT01598Recs();
        
        //Populate the data for the custom setting
        SLID_Broker_Mapping_Settings__c brokerMappingSettings = new SLID_Broker_Mapping_Settings__c();
        brokerMappingSettings.Batch_Job_Admin_Email__c = 'echeong@salesforce.com';
        brokerMappingSettings.SLID_CreateBrokerMappingClassName__c= 'SLID_CreateBrokerMapping';
        brokerMappingSettings.SLID_PurgeBrokerMappingClassName__c= 'SLID_PurgeBrokerMapping';
        brokerMappingSettings.SLID_PurgeBrokerStageClassName__c= 'SLID_PurgeBrokerStage';
        brokerMappingSettings.SLID_UpdateBrokerFieldsFromMIClassName__c = 'SLID_UpdateBrokerFieldsFromMI';
        brokerMappingSettings.Hard_Delete_Records__c = true;
        insert brokerMappingSettings;
 
            
        //Invoke the VF page
        Test.StartTest();
        PageReference pageRef = Page.SLID_BrokerMappingBatchExecution;  
        Test.setCurrentPage(pageRef);
        
        SLID_BrokerMappingBatchExecutionCntrl controller = new SLID_BrokerMappingBatchExecutionCntrl();
        
        boolean renderAsyncjobs = controller.buttonsBlockRendered;
        Boolean renderBlock = controller.statusBlockRendered;
        
        if (renderAsyncjobs) {
          controller.executeDeleteBatchJob();
          controller.executeUpdateMappingDataJob(); 
          controller.executeUpdateMIdataOnBrokerStageRecs();
               
          List<AsyncApexJob> jobs = [Select Id , JobType, Status , CompletedDate , ApexClassId 
                                From AsyncApexJob 
                                Where ApexClassId IN :controller.classIds
                                And JobType = 'BatchApex'
                                ORDER BY CompletedDate DESC LIMIT 10];
          system.assertEquals(3, jobs.size());
        }
        
        renderAsyncjobs = controller.buttonsBlockRendered;
        
        //test exception
        SLID_BrokerMappingBatchExecutionCntrl controllerException = new SLID_BrokerMappingBatchExecutionCntrl();
        
        controllerException.classIds = null;
        controllerException.executeDeleteBatchJob();
        controllerException.executeUpdateMappingDataJob(); 
        controllerException.executeUpdateMIdataOnBrokerStageRecs();
        
        controllerException.classIdProcessNameMap = null;
        controllerException.mapBrokerMappingClassIds(controllerException.classIdProcessNameMap);
        Test.stopTest();        
    }
    
    private static void createMAP_NATLTERRrecords()
    {
    	List<MAP_NATLTERR__c> recsToInsert = new List<MAP_NATLTERR__c>();
    	
    	MAP_NATLTERR__c rec1 = new MAP_NATLTERR__c();
    	rec1.NATL_TERR_CD__c = 'NAT1';
    	rec1.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 1';
    	recsToInsert.add(rec1);
    	
    	MAP_NATLTERR__c rec2 = new MAP_NATLTERR__c();
    	rec2.NATL_TERR_CD__c = 'NAT2';
    	rec2.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 2';
    	recsToInsert.add(rec2);
    	
    	MAP_NATLTERR__c rec3 = new MAP_NATLTERR__c();
    	rec3.NATL_TERR_CD__c = 'NAT3';
    	rec3.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 3';
    	recsToInsert.add(rec3);
    	
    	MAP_NATLTERR__c rec4 = new MAP_NATLTERR__c();
    	rec4.NATL_TERR_CD__c = 'NAT4';
    	rec4.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 4';
    	recsToInsert.add(rec4);
    	
    	MAP_NATLTERR__c rec5 = new MAP_NATLTERR__c();
    	rec5.NATL_TERR_CD__c = 'NAT5';
    	rec5.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 5';
    	recsToInsert.add(rec5);
    	
    	MAP_NATLTERR__c rec6 = new MAP_NATLTERR__c();
    	rec6.NATL_TERR_CD__c = 'NAT6';
    	rec6.NATIONAL_BROKER_GROUP__c = 'NATIONAL BROKER GROUP 6';
    	recsToInsert.add(rec6);
    	
    	insert recsToInsert;
    }
    
    private static void createMAP_Mrecords()
    {
    	List<MAP_M__c> 	recsToInsert = new List<MAP_M__c>();
    	
    	MAP_M__c rec1 = new MAP_M__c();
    	rec1.SUBCOY__c = 4;
    	rec1.BRANCH__c = 6801;
    	rec1.REGION_GROUP__c = 'Ontario';
    	rec1.NEW_STRATEGIC_SEGMENT__c = 'PI';
    	rec1.NSS__c = 'PI';
    	rec1.BRAND__c = 'WA';
    	recsToInsert.add(rec1);
    	
    	MAP_M__c rec2 = new MAP_M__c();
    	rec2.SUBCOY__c = 4;
    	rec2.BRANCH__c = 6988;
    	rec2.REGION_GROUP__c = 'Ontario';
    	rec2.NEW_STRATEGIC_SEGMENT__c = 'PI';
    	rec2.NSS__c = 'PI';
    	rec2.BRAND__c = 'WA';
    	recsToInsert.add(rec2);
    	
    	MAP_M__c rec3 = new MAP_M__c();
    	rec3.SUBCOY__c = 5;
    	rec3.BRANCH__c = 3205;
    	rec3.REGION_GROUP__c = 'FA';
    	rec3.NEW_STRATEGIC_SEGMENT__c = 'FA';
    	rec3.NSS__c = 'FA';
    	rec3.BRAND__c = 'FA';
    	recsToInsert.add(rec3);
    	
    	MAP_M__c rec4 = new MAP_M__c();
    	rec4.SUBCOY__c = 6;
    	rec4.BRANCH__c = 188;
    	rec4.REGION_GROUP__c = 'Atlantic';
    	rec4.NEW_STRATEGIC_SEGMENT__c = 'MID MKT';
    	rec4.NSS__c = 'GSL';
    	rec4.BRAND__c = 'RSA';
    	recsToInsert.add(rec4);
    	
    	MAP_M__c rec5 = new MAP_M__c();
    	rec5.SUBCOY__c = 6;
    	rec5.BRANCH__c = 488;
    	rec5.REGION_GROUP__c = 'Atlantic';
    	rec5.NEW_STRATEGIC_SEGMENT__c = 'GSL';
    	rec5.NSS__c = 'GSL';
    	rec5.BRAND__c = 'RSA';
    	recsToInsert.add(rec5);
    	
    	MAP_M__c rec6 = new MAP_M__c();
    	rec6.SUBCOY__c = 6;
    	rec6.BRANCH__c = 1101;
    	rec6.REGION_GROUP__c = 'Quebec';
    	rec6.NEW_STRATEGIC_SEGMENT__c = 'PI';
    	rec6.NSS__c = 'PI';
    	rec6.BRAND__c = 'RSA';
    	recsToInsert.add(rec6);
    	
    	insert recsToInsert;
    	
    }
    
    private static void create_MAP_CT01598Recs()
    {
    	list<MAP_CT01598__c> recsToInsert = new List<MAP_CT01598__c>();
    	
    	MAP_CT01598__c rec1 = new MAP_CT01598__c();
    	rec1.CT01598_ARGUMENT__c = 'A';
    	rec1.CT01598_RESULT__c = 'Arg A';
    	recsToInsert.add(rec1);
    	
    	MAP_CT01598__c rec2 = new MAP_CT01598__c();
    	rec2.CT01598_ARGUMENT__c = 'B';
    	rec2.CT01598_RESULT__c = 'Arg B';
    	recsToInsert.add(rec2);
    	
    	insert recsToInsert;
    	
    }
}