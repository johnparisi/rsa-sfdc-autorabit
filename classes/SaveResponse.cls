global with sharing class SaveResponse {
	
	public boolean success;
	public boolean esbErrorFlag;
	public string esbErrorMessage;
}