public with sharing class SMEQ_DnBCallOutRequest {
	
	public CommonParms CommonParms {public get; public set;} 
	
	public RestRequest RestRequest {public get; public set;} 

	public class CommonParms{
		public String SourceSystem {public get; public set;}
		public Boolean ProductionIndicator {public get; public set;}
	}

	public class RestRequest{
		public String SubjectName {public get; public set;}
		public String StreetAddressLine1 {public get; public set;}
		public String PostalCode {public get; public set;}
		public String TerritoryName {public get; public set;}
		public String PrimaryTownName {public get; public set;}
		public String TelephoneNumber {public get; public set;}
		public String CountryISOAlpha2Code {public get; public set;}
		public String DUNSNumber {public get; public set;}
	}

}