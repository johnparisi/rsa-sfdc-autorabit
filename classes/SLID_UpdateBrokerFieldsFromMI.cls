global class SLID_UpdateBrokerFieldsFromMI implements Database.Batchable<sObject> 
{
	global String query {get; set;}
	
	global Map<String,Broker_Stage__c> brokerStageRecordsToUpdateMap {get; set;}
	
	@TestVisible
	private static Boolean IS_TEST = false;
	
	private static final String batchCompleteMessage = 'Batch Process to Update MI Data on Broker Stage records is completed';
    private static final String noErrorEncounteredMessage = 'No Errors encountered in the batch job';
	
    global SLID_UpdateBrokerFieldsFromMI()
    {
    	this.query = 'Select Id , AGENT_STREET__c , ACTIVTO__c , ACTIVE_TO__c , REGION_GROUP__c , NEW_STRATEGIC_SEGMENT__c , NSS__c , BRAND__c , NATIONAL_BROKER_GROUP__c ,'; 
    	this.query += 	     'NATLTERR__c , TEAM__c , BRANCH__c , SUBCOY__c, STREET__c , STREETNO__c , STREETSU__c , STREETDIR__c, ADDRIND1__c, ADDRNO1__c, ADDRIND2__c, ADDRNO2__c ';
    	this.query +='FROM Broker_Stage__c';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	return Database.getQueryLocator(this.query);
    }
    
    global void execute(Database.BatchableContext BC,List<sobject> scope)
    {
    	try
    	{
    		brokerStageRecordsToUpdateMap = new Map<String,Broker_Stage__c>();
    		
    		//Field to Set : NATIONAL_BROKER_GROUP__c
    		Set<String> uniqueNATLTERRvaluesFromBrokerStageRecs = new Set<String>();
    		
    		//Field to Set : REGION_GROUP__c
    		Set<String> brokerStageIdsWhereREGION_GROUPset = new Set<String>();
    		Set<Double> subcoyValuesForREGION_GROUP = new Set<Double>();
    		Set<Double> branchValuesForREGION_GROUP = new Set<Double>();
    		
    		//Field to Set : AGENT_STREET__c
    		Set<String> brokerStageADDRINDvalues = new Set<String>();
    		
	    	for(sobject sobj : scope)
	    	{
	    		Broker_Stage__c bStage = (Broker_Stage__c)sobj;
	    		
	    		//Field to Set : NATIONAL_BROKER_GROUP__c
	    		//Create a set of all the NATLTERR__c values on the broker stage records. 
	    		if(!isNullOrEmtptyString(bStage.NATLTERR__c))
	    			uniqueNATLTERRvaluesFromBrokerStageRecs.add(bStage.NATLTERR__c);
	    		
	    		//Field to Set : ACTIVE_TO__c
	    		setACTIVE_TOfieldOnBrokerStageRec(bStage);
	    		
	    		//Field to Set : REGION_GROUP__c
	    		//Field to Set : NEW_STRATEGIC_SEGMENT__c
			    //Field to Set : NSS__c
			    //Field to Set : BRAND__c
	    		setREGION_GROUPforPrairiesAndPacific(bStage,brokerStageIdsWhereREGION_GROUPset);
	    		subcoyValuesForREGION_GROUP.add(bStage.SUBCOY__c);
	    		branchValuesForREGION_GROUP.add(bStage.BRANCH__c);
	    		
	    		//Field to Set : AGENT_STREET__c
	    		if(!isNullOrEmtptyString(bStage.ADDRIND1__c))
	    			brokerStageADDRINDvalues.add(bStage.ADDRIND1__c);
	    		if(!isNullOrEmtptyString(bStage.ADDRIND2__c))
	    			brokerStageADDRINDvalues.add(bStage.ADDRIND2__c);
	    	}
	    	
	    	//Field to Set : NATIONAL_BROKER_GROUP__c
	    	setNATIONAL_BROKER_GROUPfieldOnBrokerStageRecs(scope,uniqueNATLTERRvaluesFromBrokerStageRecs);
	    	
	    	//Field to Set : REGION_GROUP__c
	    	//Field to Set : NEW_STRATEGIC_SEGMENT__c
			//Field to Set : NSS__c
			//Field to Set : BRAND__c
			system.debug('****'+ subcoyValuesForREGION_GROUP);
			system.debug('****'+ branchValuesForREGION_GROUP);
	    	if(!subcoyValuesForREGION_GROUP.isEmpty() || !branchValuesForREGION_GROUP.isEmpty())
	    		setBranchAndSubcoyBasedFieldsonBrokerStageRecs(scope,brokerStageIdsWhereREGION_GROUPset,subcoyValuesForREGION_GROUP,branchValuesForREGION_GROUP);
	    	
	    	 //Field to Set : AGENT_STREET__c
	    	 setAGENT_STREETonBrokerStageRecs(scope,brokerStageADDRINDvalues);
	    		
	    	
	    	if(!brokerStageRecordsToUpdateMap.isEmpty())
	    		update brokerStageRecordsToUpdateMap.values();
	    	
    	}
    	catch(Exception e)
    	{
    		system.debug('--execute--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','execute','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        //throw e;
    	}
    }
     
    global void finish(Database.BatchableContext BC)
    {
    	sendCompletionEmail(BC);
    }
    
    //Field to Set : AGENT_STREET__c
    private void setAGENT_STREETonBrokerStageRecs(List<sobject> scope , Set<String> brokerStageINDvalues)
    {
    	try
    	{
    		List<MAP_CT01598__c> addrRecsFromMAPobj = new List<MAP_CT01598__c>();
    		Map<String,MAP_CT01598__c> recArgumentToAddrRecMap = new Map<String,MAP_CT01598__c>();
    		
    		if(!brokerStageINDvalues.isEmpty())
    		{
		    	//query the MAP_CT01598__c object 
		    	addrRecsFromMAPobj = [Select CT01598_ARGUMENT__c , CT01598_RESULT__c , ZCT_TABLE_ID__c
		    											   From MAP_CT01598__c
		    											   Where CT01598_ARGUMENT__c IN: brokerStageINDvalues];
		    	
		    	for(MAP_CT01598__c mapRec : addrRecsFromMAPobj)
		    		recArgumentToAddrRecMap.put(mapRec.CT01598_ARGUMENT__c,mapRec);
    		}
	    	
	    	//loop through the broker stage recs and set the AGENT_STREET__c field
	    	//STREET__c , STREETNO__c , STREETSU__c , STREETDIR__c , ADDRNO1__c , ADDRNO2__c
	    	for(sobject sobj : scope)
	    	{
	    		String agentStreet = '';
	    		
	    		Broker_Stage__c bStage = (Broker_Stage__c)sobj;
	    		
	    		if(!isNullOrEmtptyString(bStage.STREETNO__c))
	    			agentStreet += bStage.STREETNO__c + ' ';
	    		if(!isNullOrEmtptyString(bStage.STREET__c))
	    			agentStreet += bStage.STREET__c + ' ';
	    		if(!isNullOrEmtptyString(bStage.STREETSU__c))
	    			agentStreet += bStage.STREETSU__c + ' ';
	    		if(!isNullOrEmtptyString(bStage.STREETDIR__c))
	    			agentStreet += bStage.STREETDIR__c + ' ';
	    		
	    		if(recArgumentToAddrRecMap.containsKey(bStage.ADDRIND1__c))
	    		{
	    			MAP_CT01598__c mRec = recArgumentToAddrRecMap.get(bStage.ADDRIND1__c);
	    			if(!isNullOrEmtptyString(mRec.CT01598_RESULT__c))
	    				agentStreet += mRec.CT01598_RESULT__c + ' ';
	    		}
	    		
	    		if(!isNullOrEmtptyString(bStage.ADDRNO1__c))
	    			agentStreet += bStage.ADDRNO1__c + ' ';
	    		
	    		if(recArgumentToAddrRecMap.containsKey(bStage.ADDRIND2__c))
	    		{
	    			MAP_CT01598__c mRec = recArgumentToAddrRecMap.get(bStage.ADDRIND2__c);
	    			if(!isNullOrEmtptyString(mRec.CT01598_RESULT__c))
	    				agentStreet += mRec.CT01598_RESULT__c + ' ';
	    		}
	    		
	    		if(!isNullOrEmtptyString(bStage.ADDRNO2__c))
	    			agentStreet += bStage.ADDRNO2__c + ' ';
	    			
	    		bStage.AGENT_STREET__c = agentStreet;
	    		
	    		addToMapForUpdate(bStage);
	    	}
    	}
    	catch(Exception e)
    	{
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','setAGENT_STREETonBrokerStageRecs','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        if(!Test.isRunningTest())
	        	throw e;	
    	}
    }
    
    //Field to Set : REGION_GROUP__c
    //Field to Set : NEW_STRATEGIC_SEGMENT__c
    //Field to Set : NSS__c
    //Field to Set : BRAND__c
    @TestVisible
    private void setBranchAndSubcoyBasedFieldsonBrokerStageRecs(List<sobject> scope , Set<String> brokerStageIdsWhereREGION_GROUPset , Set<Double> subcoyValuesForREGION_GROUP , Set<Double> branchValuesForREGION_GROUP)
    {
    	try
    	{
	    	//Set query string and query for the MAP_M__c records
	    	String queryForRegionGroup = 'Select Id, BRANCH__c , SUBCOY__c , REGION_GROUP__c , NEW_STRATEGIC_SEGMENT__c , NSS__c , BRAND__c From MAP_M__c ';
	    	
	    	system.debug('--subcoyValuesForREGION_GROUP' + subcoyValuesForREGION_GROUP);
	    	system.debug('--branchValuesForREGION_GROUP' + branchValuesForREGION_GROUP);
	    	
	    	if(!subcoyValuesForREGION_GROUP.isEmpty() && !branchValuesForREGION_GROUP.isEmpty())
	    		queryForRegionGroup += ' Where BRANCH__c IN: branchValuesForREGION_GROUP AND SUBCOY__c IN: subcoyValuesForREGION_GROUP';
	    	else if(!subcoyValuesForREGION_GROUP.isEmpty() && branchValuesForREGION_GROUP.isEmpty())
	    		queryForRegionGroup += ' Where SUBCOY__c IN: subcoyValuesForREGION_GROUP';
	    	else if(!branchValuesForREGION_GROUP.isEmpty() && subcoyValuesForREGION_GROUP.isEmpty())
	    		queryForRegionGroup += ' Where BRANCH__c IN: branchValuesForREGION_GROUP';
	    	
	    	List<MAP_M__c> MAP_Mrecs = Database.query(queryForRegionGroup);
	    	
	    	system.debug('---MAP_Mrecs' + MAP_Mrecs);
	    	
	    	//Set the map with the key as subcoy and values as the map with the key as branch to MAP_M__c records.
	    	//This way we can loop through the broker stage records and then get the Map of branch to MAP_M__c recs based on the  Subcoy and then eventually get the MAP_M__c record based ont he branch
	    	//and set the REGION_GROUP__c
	    	Map<Double , Map<Double,MAP_M__c>> mapSubcoyToMapBranchRec = new Map<Double,Map<Double,MAP_M__c>>();
	    	
	    	if(MAP_Mrecs != null && !MAP_Mrecs.isEmpty())
	    	{
		    	for(MAP_M__c mapRec : MAP_Mrecs)
		    	{
		    		if(mapRec.SUBCOY__c != null)
		    		{
		    			if(mapSubcoyToMapBranchRec.containsKey(mapRec.SUBCOY__c))
		    			{
		    				Map<Double,MAP_M__c> mapBranchToMAP_Mrec = mapSubcoyToMapBranchRec.get(mapRec.SUBCOY__c);
		    				if(mapRec.BRANCH__c != null)
		    					mapBranchToMAP_Mrec.put(mapRec.BRANCH__c,mapRec);
		    			}
		    			else
		    			{
		    				Map<Double,MAP_M__c> mapBranchToMAP_Mrec = new Map<Double,MAP_M__c>();
		    				if(mapRec.BRANCH__c != null)
		    					mapBranchToMAP_Mrec.put(mapRec.BRANCH__c,mapRec);
		    				mapSubcoyToMapBranchRec.put(mapRec.SUBCOY__c,mapBranchToMAP_Mrec);
		    			}
		    		}
		    	}
		    	
		    	system.debug('---mapSubcoyToMapBranchRec' + mapSubcoyToMapBranchRec);
		    	
		    	//Loop through the scope and set the region group.
		    	for(sobject sobj : scope)
		    	{
		    		Broker_Stage__c bStageRec = (Broker_Stage__c)sobj;
		    		
		    		//reset the values of all MI related fields to null
		    		if(!brokerStageIdsWhereREGION_GROUPset.contains(bStageRec.Id))
		    			bStageRec.REGION_GROUP__c = null;
		    		bStageRec.NEW_STRATEGIC_SEGMENT__c = null;
		    		bStageRec.NSS__c = null;
		    		bStageRec.BRAND__c = null;
		    		
		    		//filter out records where REGION_GROUP is already set
	    			if(bStageRec.SUBCOY__c != null)
	    			{
	    				system.debug('--bStageRec' + bStageRec);
	    				
	    				Map<Double,MAP_M__c> mapBranchToMAP_Mrec = mapSubcoyToMapBranchRec.get(bStageRec.SUBCOY__c);
	    				
	    				system.debug('----mapBranchToMAP_Mrec' + mapBranchToMAP_Mrec);
	    				
	    				if(bStageRec.BRANCH__c != null && mapBranchToMAP_Mrec != null)
	    				{
	    					MAP_M__c map_mRec = mapBranchToMAP_Mrec.get(bStageRec.BRANCH__c);
	    					if(map_mRec != null)
	    					{
	    						//Field to Set : REGION_GROUP__c
	    						if(!brokerStageIdsWhereREGION_GROUPset.contains(bStageRec.Id))
	    							bStageRec.REGION_GROUP__c = map_mRec.REGION_GROUP__c;
	    							
	    						//Field to Set : NEW_STRATEGIC_SEGMENT__c
	    						bStageRec.NEW_STRATEGIC_SEGMENT__c = map_mRec.NEW_STRATEGIC_SEGMENT__c;
	    						
	    						//Field to Set : NSS__c
	    						bStageRec.NSS__c = map_mRec.NSS__c;
	    						
	    						//Field to Set : BRAND__c
	    						bStageRec.BRAND__c = map_mRec.BRAND__c;
	    					}
	    				}
	    			}
	    			
	    			addToMapForUpdate(bStageRec);
		    	}
	    	}
    	}
    	catch(Exception e)
    	{
    		system.debug('--setBranchAndSubcoyBasedFieldsonBrokerStageRecs--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','setBranchAndSubcoyBasedFieldsonBrokerStageRecs','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        if(!Test.isRunningTest())
	        	throw e;
    	}
    }
    
    //Field to Set : REGION_GROUP__c
    //Set the REGION_GROUP__c on the broker stage rec based on the following 
    //If BRANCH__c == 8088 And TEAM__c == 7088 then REGION_GROUP__c = Prairies
    //If BRANCH__c == 7088 And TEAM__c == 8088 then REGION_GROUP__c = Pacific
    @TestVisible
    private void setREGION_GROUPforPrairiesAndPacific(Broker_Stage__c bStageRec , Set<String> brokerStageIdsWhereREGION_GROUPset)
    {
    	try
    	{
	    	if(bStageRec.BRANCH__c == 8088 && bStageRec.TEAM__c == '7088')
	    	{
	    		bStageRec.REGION_GROUP__c = 'Prairies';
	    		brokerStageIdsWhereREGION_GROUPset.add(bStageRec.Id);
	    		addToMapForUpdate(bStageRec);
	    	}
	    	if(bStageRec.BRANCH__c == 7088 && bStageRec.TEAM__c == '8088')
	    	{
	    		bStageRec.REGION_GROUP__c = 'Pacific';
	    		brokerStageIdsWhereREGION_GROUPset.add(bStageRec.Id);
	    		addToMapForUpdate(bStageRec);
	    	}
    	}
    	catch(Exception e)
    	{
    		system.debug('--setREGION_GROUPforPrairiesAndPacific--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','setREGION_GROUPforPrairiesAndPacific','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        if(!Test.isRunningTest())
	        	throw e;
    	}
    }
    
    //Field to Set : ACTIVE_TO__c
    //Check if the ACTIVTO__c field on the broker stage rec is populated and is exactly 8 characters.
    //The first 4 digits of that field would be the year , the next 2 month and the last two as day. 
    //Use the above logic to set the ACTIVE_TO__c field on the broker stage rec.
    @TestVisible
    private void setACTIVE_TOfieldOnBrokerStageRec(Broker_Stage__c bStage)
    {
    	try
    	{
    		//Check for length of 10 as the string returned from format() contains ',' i.e 99999999 will be 99,999,999
	    	if(bStage.ACTIVTO__c != null && bStage.ACTIVTO__c.format().length()== 10)
	    	{
		    	if(bStage.ACTIVTO__c == 99999999)
		    		bStage.ACTIVE_TO__c = Date.newInstance(3000,12,31);
		    	else
		    	{
		    		String activDateStringFormatted = bStage.ACTIVTO__c.format();
		    		
		    		//remove ',' from the string returned from format()
		    		List<String> activDateList = activDateStringFormatted.split(',');
		    		String activDateString = '';
		    		for(String s : activDateList)
		    			activDateString += s;
		    		
		    		String activYearString = activDateString.left(4);
		    		
		    		String activStringAfterYear = activDateString.substring(4);
		    		String activMonthString = activStringAfterYear.left(2);
		    		
		    		String activDayString = activStringAfterYear.substring(2);
		    		
		    		bStage.ACTIVE_TO__c = Date.newInstance(Integer.valueOf(activYearString),Integer.valueOf(activMonthString),Integer.valueOf(activDayString));
		    		
		    		addToMapForUpdate(bStage);
		    	}
		    		
	    	}
	    	else
	    	{
	    		bStage.ACTIVE_TO__c = null;
	    		addToMapForUpdate(bStage);
	    	}
    	}
    	catch(Exception e)
    	{
    		system.debug('--setACTIVE_TOfieldOnBrokerStageRec--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','setACTIVE_TOfieldOnBrokerStageRec','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        if(!Test.isRunningTest())
	        	throw e;
    	}
    }
    
    //Field to Set : NATIONAL_BROKER_GROUP__c
    private void setNATIONAL_BROKER_GROUPfieldOnBrokerStageRecs(List<sobject> scope , Set<String> uniqueNATLTERRvaluesFromBrokerStageRecs)
    {
    	try
    	{
    		Map<String,String> mapMAP_NATLTERR = queryMAP_NATLTERR(uniqueNATLTERRvaluesFromBrokerStageRecs);
			setNATIONAL_BROKER_GROUPfield(scope,mapMAP_NATLTERR);
    	}   
    	catch(Exception e)
    	{
    		system.debug('--setNATIONAL_BROKER_GROUPfieldOnBrokerStageRecs--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','setNATIONAL_BROKER_GROUPfieldOnBrokerStageRecs','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        throw e;
    	} 	
    }
    
    //Field to Set : NATIONAL_BROKER_GROUP__c
    //Query for all the records in the object MAP_NATLTERR__c where the field NATL_TERR_CD__c equals the value of NATLTERR__c from broker stage object thats passed in the set as arguments.
    //Hold the values in a map where NATL_TERR_CD__c is the key and NATIONAL_BROKER_GROUP__c is the value. 
    //Need to loop through the scope (broker stage objects) and set the NATIONAL_BROKER_GROUP__c field on the broker stage record after picking the value based on the key NATLTERR__c from the broker stage record.
    @TestVisible
    private Map<String,String> queryMAP_NATLTERR(Set<String> filterValues)
    {
    	Map<String,String> mapNATLTERR_BROKERGROUPbyNATL_TERR_CD = new Map<String,String>();
    	try
    	{
	    	if(filterValues != null && !filterValues.isEmpty())
	    	{
		    	List<MAP_NATLTERR__c> recs = [Select Id  , NATL_TERR_CD__c , NATIONAL_BROKER_GROUP__c
											  FROM MAP_NATLTERR__c
							      			  WHERE NATL_TERR_CD__c IN: filterValues];
	      		
	      		if(recs != null && !recs.isEmpty())
	      		{
	      			//negative testing for exception handling
	      			if(IS_TEST == true)
	      				recs = null;
	      				
		      		for(MAP_NATLTERR__c rec : recs)
		      		{
		      			if(!mapNATLTERR_BROKERGROUPbyNATL_TERR_CD.containsKey(rec.NATL_TERR_CD__c))
		      				mapNATLTERR_BROKERGROUPbyNATL_TERR_CD.put(rec.NATL_TERR_CD__c,rec.NATIONAL_BROKER_GROUP__c);
		      		}
	      		}
	    	}
    	}
    	catch(Exception e)
    	{
    		system.debug('--queryMAP_NATLTERR--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','queryMAP_NATLTERR','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        throw e;
    	}
    	
    	return mapNATLTERR_BROKERGROUPbyNATL_TERR_CD;
    }
    
    
     //Field to Set : NATIONAL_BROKER_GROUP__c
     //Loop through all the broker stage records coming in as a part of the scope. 
     //Look for the key in the map using the field NATLTERR__c on broker stage records. If the key exists then we set the NATIONAL_BROKER_GROUP__c field on the broker stage as the value for that key from the map.
     //If the key does not exist then we blank out th evalue on the NATIONAL_BROKER_GROUP__c field on the broker stage record.
     @TestVisible
    private void setNATIONAL_BROKER_GROUPfield(list<sobject> scope , Map<String,String> mapMAP_NATLTERR)
    {
    	try
    	{
	    	for(sobject sobj : scope)
	    	{
	    		Broker_Stage__c bStage = (Broker_Stage__c)sobj;
	    		
	    		if(mapMAP_NATLTERR.containsKey(bStage.NATLTERR__c))
	    			bStage.NATIONAL_BROKER_GROUP__c = mapMAP_NATLTERR.get(bStage.NATLTERR__c);
	    		else
	    			bStage.NATIONAL_BROKER_GROUP__c = '';
	    			
	    		addToMapForUpdate(bStage);
	    	}
    	}
    	catch(Exception e)
    	{
    		system.debug('--setNATIONAL_BROKER_GROUPfield--' + e);
    		//Create exception record
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','setNATIONAL_BROKER_GROUPfield','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        if(!Test.isRunningTest())
	        	throw e;
    	}
    }
    
   private void sendCompletionEmail(Database.BatchableContext bc)
   {
   		try
   		{
	      //Retrieve the email to send to
	      SLID_Broker_Mapping_Settings__c brokerMappingSettings = SLID_Broker_Mapping_Settings__c.getInstance();
	      
	      AsyncApexJob job = [Select Id , Status , NumberOfErrors 
	                          From AsyncApexJob 
	                          Where Id =: bc.getJobId()];
	                          
	      String emailBody = batchCompleteMessage + '<br/>';
	      if(job.NumberOfErrors == 0 || job.NumberOfErrors == null)
	        emailBody += noErrorEncounteredMessage;
	      else if(job.NumberOfErrors > 0)
	        emailBody += 'The batch job encountered ' + job.NumberOfErrors + ' errors.';
	      
	      
	
	      //Reserve a email capacity
	      Messaging.reserveSingleEmailCapacity(1);
	      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	      String[] toAddresses = new String[] {brokerMappingSettings.Batch_Job_Admin_Email__c}; 
	      mail.setToAddresses(toAddresses);
	      mail.setSenderDisplayName('Salesforce Batch Apex');
	      mail.setSubject('MI Data Update on Broker Stage Complete');
	      mail.setBccSender(false);
	      mail.setUseSignature(false);
	      mail.setHTMLBody(emailBody);
	      if(!Test.isRunningTest())
	      	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   		}
   		catch(Exception e)
   		{
	        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_UpdateBrokerFieldsFromMI','sendCompletionEmail','',UTIL_Logging.DEBUG_LEVEL_ERROR);
	        UTIL_Logging.logException(log); 
	        if(!Test.isRunningTest())
	        	throw e;
   		}
   } 
    
    private void addToMapForUpdate(Broker_Stage__c bStage)
    {
    	if(bStage != null)
    		brokerStageRecordsToUpdateMap.put(bStage.Id,bStage);
    }
    
    private Boolean isNullOrEmtptyString(String someText)
    {
    	if(someText == null || someText == '')
    		return true;
    	else
    		return false;
    	return null;
    }
}