global class Vlocity_RatingResponseProcessor implements vlocity_ins.VlocityOpenInterface2 {
    
    global Object invokeMethod(String methodName, Map<String,Object> input, Map<String,Object> output, Map<String,Object> options){
        if(methodName == 'getRQRatingJSONResponse'){
            getRQRatingJSONResponse(input, output, options);
        }
        else if(methodName == 'getRQRatingJSONResponse'){
            getFQRatingJSONResponse(input, output, options);
        }
        return NULL;
    }
    
    global Object getRQRatingJSONResponse(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        system.debug('inputs -->'+ inputs.keySet());
        system.debug('inputs -->'+ inputs.values());
        system.debug('output -->'+ output);
        system.debug('options -->'+ options);
        
        return null;
    }
    
    global Object getFQRatingJSONResponse(Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        system.debug('inputs -->'+ inputs.keySet());
        system.debug('inputs -->'+ inputs.values());
        system.debug('output -->'+ output);
        system.debug('options -->'+ options);
        
        return null;
    }
}