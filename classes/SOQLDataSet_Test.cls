/**
 * Author: James Lee
 * Created At: November 8, 2016
 * 
 * Unit tests for SOQLDataSet
 */
@isTest
public class SOQLDataSet_Test
{
    private static Quote__c getQuote()
    {
        return [
            SELECT id, Package_Type_Selected__c, Case__c, Case__r.Segment__c
            FROM Quote__c
            LIMIT 1
        ];
    }
    
    //public static testmethod void testGetValidBrokerNo() {}

    @isTest
    static void testGetQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c q = getQuote();
            RequestInfo ri = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_QUOTE, q);
            
            SOQLDataSet sds = new SOQLDataSet(q.Id, ri);
            System.assertEquals(q.Case__c, sds.smeCase.id);
        }
    }
    
    /*
     * Author: James Lee
     * Created At: November 8, 2016.
     * Test for Canadian postal code format.
     */
    static testMethod void testIsProperPostalCode()
    {
        String postalCode = null;
        System.assertEquals(false, SOQLDataSet.isProperPostalCode(postalCode));
        
        postalCode = '';
        System.assertEquals(false, SOQLDataSet.isProperPostalCode(postalCode));
        
        postalCode = 'M5O0P4';
        System.assertEquals(false, SOQLDataSet.isProperPostalCode(postalCode));
        
        postalCode = 'M3M3M3';
        System.assertEquals(true, SOQLDataSet.isProperPostalCode(postalCode));
        
        postalCode = 'M3M 3M3';
        System.assertEquals(true, SOQLDataSet.isProperPostalCode(postalCode));
    }   
}