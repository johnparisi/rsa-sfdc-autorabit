/**
  * @author        Saumil Bapat
  * @date          11/18/2016
  * @description   Test class for SLID_CreateReferralCases
*/
@isTest
private Class SLID_CreateReferralCases_Test {
    static testMethod void testCreateChildReferralCases()
    {
      //Set the source values on the record
      Case parentCase = SLID_TestDataFactory.createTestCase();
      parentCase.Status = 'In-Progress';
      update parentCase;

      parentCase.Status = 'Referred';
      parentCase.bkrCase_Subm_Stat_Rsn__c = 'Case referred';
      update parentCase;

      Case childReferralCase;

      try {
        childReferralCase = [Select id from Case where Parent_Case__c = :parentCase.Id];
      }
      catch (Exception e) {
        System.Debug('~~~e: ' + e);
      }
      System.Assert(childReferralCase != null, 'Child case not created');
    }
}