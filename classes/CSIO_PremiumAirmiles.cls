public class CSIO_PremiumAirmiles {
    public String flavour;
	public Integer premium;
    public Integer airMiles;
    String xml;
    public Map<String, Coverages__c> coverages;
    
    public CSIO_PremiumAirmiles(String xml) {
        String partialXML = xml.remove('rsa:');
        this.xml = partialXML.remove('csio:');
        extractValues();
    }
    
    void extractValues(){
        this.flavour = XMLHelper.retrieveValue(this.xml, CSIOXMLProcessor.PACKAGE_ID_PATH);
        this.premium = Integer.valueOf(XMLHelper.retrieveValue(this.xml, CSIOXMLProcessor.STANDARD_PREMIUM_PATH));
        this.airMiles = Integer.valueOf(XMLHelper.retrieveValue(this.xml, CSIOXMLProcessor.STANDARD_AIRMILES_PATH));
    }
}