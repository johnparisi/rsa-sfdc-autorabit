/**
This class decomposes the CSIO Policy XML insured portion, saving the insured's and additional insured's informtion 
*/
public class CSIOPolicyXMLInsuredProcessor {
       
        public Account insuredAccount;
    	public List <Quote__c> existingQuote;
        public Quote__c policy;
        public Account partyAccount; 
        public Boolean isAccount = false;
        public Quote_Risk_Relationship__c quoteRiskRelationship = null;
        public Map < Quote_Risk_Relationship__c, Account > accountQrrMap = null;
    
   
    public void process(Case newCase, Dom.XmlNode insuredNode) {
        Dom.XmlNode generalPartyNode = insuredNode.getChildElement('GeneralPartyInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); 
        Dom.XmlNode addrNode = generalPartyNode.getChildElement('Addr', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XmlNode insuredInfoNode = insuredNode.getChildElement('InsuredOrPrincipalInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); 
        Dom.XmlNode nameInfoNode = generalPartyNode.getChildElement('NameInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        
           this.existingQuote = [SELECT id, Case__c, Case__r.bkrCase_Insured_Client__c FROM Quote__c WHERE epolicy__c = :newCase.COM__c and Case__c != :newCase.Id];
           insuredAccount = new Account();
        if (!this.existingQuote.isEmpty()){
            
              insuredAccount.id = this.existingQuote[0].Case__r.bkrCase_Insured_Client__c;
        }

            //mapping the insured account's information 
            insuredAccount.Name = CSIOPolicyXMLProcessor.evaluatePath(nameInfoNode.getChildElement('CommlName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('CommercialName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
            //TEMP: condition placed until mapping is fully complete, city should always come back populated 
            if (addrNode.getChildElement('City', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null){
                insuredAccount.BillingCity  = addrNode.getChildElement('City', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
                // Country is not always mapped as we default to Canada in new business
                insuredAccount.BillingCountry = CSIOPolicyXMLProcessor.evaluatePath(addrNode.getChildElement('CountryCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                insuredAccount.BillingState = CSIOPolicyXMLProcessor.evaluatePath(addrNode.getChildElement('StateProvCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                insuredAccount.BillingPostalCode = CSIOPolicyXMLProcessor.evaluatePath(addrNode.getChildElement('PostalCode', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                //TEMP: condition placed until mapping is fully complete, streetname should always come back populated 
                //this is placed because insured address does not usually have a street number in ePolicy however, GETRATE expects that insured address line 1 is populated to abide by the validation rules in ESB
                insuredAccount.BillingStreet = '1 NA';
            }
           
        Integer sicCodes = 0;
        for (Dom.XmlNode businessInfoNode: insuredInfoNode.getChildElements()){
            if (businessInfoNode.getName() == 'BusinessInfo'){
                sicCodes ++; 
            }
        }
        if (sicCodes == 1){
        	Dom.XmlNode businessInfoNode = insuredInfoNode.getChildElement('BusinessInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        	insuredAccount.bkrAccount_Company_Start_Year__c = Integer.valueOf(businessInfoNode.getChildElement('BusinessStartDt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
            insuredAccount.RecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE Name ='Policyholder' and SobjectType ='Account'  limit 1].Id;
        	String sicCodeNumber = businessInfoNode.getChildElement('SICCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
       		Sic_Code__c sicCode = [SELECT id FROM Sic_Code__c WHERE Sic_Code__c = :sicCodeNumber];
        	newCase.bkrCase_SIC_Code__c = sicCode.Id;
        }
        else {
            throw new RSA_ExceedsToolLimitsException(Label.Exceeds_limit_of_tool);
        }

        //Additional insureds are mapped differently than Loss Payees, Liability additional insureds, Premier financier's. The QRRs are mapped within this class for Additional Named Insureds  
        accountQRRMap = new Map < Quote_Risk_Relationship__c, Account > ();
        
        boolean firstAddNamedInsured = true;
        integer index = 1;
        
        //Loop through as each additional named insured will be sitting in a supplementary name info 
        for (Dom.XmlNode childXmlNode: nameInfoNode.getChildElements()){

            if (childXmlNode.getName() == 'SupplementaryNameInfo') {
                if (!firstAddNamedInsured) {
                // create an account and populate it
                partyAccount = new Account();
                quoteRiskRelationship = new Quote_Risk_Relationship__c();
                Map<String, String> esbRelationshipType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Relationship_Type__c');
               
                //map additional insured
                Id accountOtherPartyRecordType = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true).get('Other_Party_Account'); 
                partyAccount.RecordTypeId = accountOtherPartyRecordType;
                partyAccount.Name = childXmlNode.getChildElement('SupplementaryName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
                // TBC: relationship between QRR, account and contact
                String natureInterestCD = childXmlNode.getChildElement('SupplementaryNameCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null? childXmlNode.getChildElement('SupplementaryNameCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() : null;
                quoteRiskRelationship.Party_Number__c = index;
                //TEMP until mapping is fully complete, additional named insured should always come back with a relationship type 
                if (natureInterestCD != null){
                    quoteRiskRelationship.Relationship_Type__c = esbRelationshipType.get(natureInterestCD);
                }
                //All QRRs mapped in insuredOrPrincipal will be additional named insureds based on current ePolicy structure 
                quoteRiskRelationship.Party_Type__c = SOQLDataSet.ADDITIONAL_INSURED;
                accountQrrMap.put(quoteRiskRelationship, partyAccount); 
                index++;
                } 
                firstAddNamedInsured = false;
            }
        }
    }
}