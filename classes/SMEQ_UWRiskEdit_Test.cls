/**
 * SMEQ_UWRiskEdit_Test
 * Author: Sophie Tran
 * Date: November 16 2017
 * 
 * This is the test class for SMEQ_UWRiskEdit
 */
@isTest
public class SMEQ_UWRiskEdit_Test
{
    static testMethod void invokeConstructorOnEdit()
    {      
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        System.runAs(uw)
        {
            Test.startTest();
             Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk(quote);
            
            //Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();
            
            // Calling the constructor for Open quote
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(risk);
            SMEQ_UWRiskEdit uwRiskEditOpen = new SMEQ_UWRiskEdit(sc1);
            
            risk.RecordTypeId = RiskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION);
            update risk;
            
            // Calling the constructor for Closed Risk
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(risk);
            SMEQ_UWRiskEdit uwRiskEditClose = new SMEQ_UWRiskEdit(sc2);
           uwRiskEditClose.getSICQuestionsWithoutSICCode(risk);
           
           try{
           list<risk__c> listRisk = new list<Risk__c>{risk};
           ApexPages.Standardsetcontroller sc2set = new ApexPages.Standardsetcontroller(listRisk);
            SMEQ_UWRiskEdit uwRiskEditCloseset = new SMEQ_UWRiskEdit(sc2set);
            }catch(exception ex){}
            
            Test.stopTest();
        }
    }
    
    static testMethod void testSave()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        
        System.runAs(uw)
        {
            Test.startTest();
            
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            SIC_Code_Detail_Version__c sicCodeVersion = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            List<SIC_Question__c> questions = new List<SIC_Question__c>(); 
            Map<SIC_Question__c, SIC_Answer__c> fields = new Map<SIC_Question__c, SIC_Answer__c>(); 
            questions = SicQuestionService.getAllApplicationQuestions(sicCodeVersion, SicQuestionService.CATEGORY_RISK);
            
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();
            
            List<Sic_Question__c> picklistQuestion = new List<SIC_Question__c>();    
            for (Sic_Question__c sq : questions)
            {
                if(sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    sq.AvailableValuesEn__c = 'Value1;Value2;Value3;Value4';
                    picklistQuestion.add(sq);
                } 
            }
            update picklistQuestion;
            
            // Calling the constructor
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(risk);
            SMEQ_UWRiskEdit uwRiskEditOpen = new SMEQ_UWRiskEdit(sc);
            system.debug('++++ uwRiskEditOpen.fields '+ uwRiskEditOpen.fields.keyset());
            
            picklistQuestion[0].Input_Type__c =SICQuestionService.INPUT_TYPE_MULTI_SELECT_DROPDOWN;
            update picklistQuestion;
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(risk);
            SMEQ_UWRiskEdit uwRiskEditOpe1n = new SMEQ_UWRiskEdit(sc1);
            
            
            for (Sic_Question__c sq : uwRiskEditOpen.fields.keySet())
            {
                Sic_Answer__c sa = uwRiskEditOpen.fields.get(sq);
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_TEXT)
                {
                    sa.Value__c = 'OneTwoThree';
                    system.debug('++++++' + sa.Value__c);
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_NUMBER)
                {
                    sa.Value__c = '12345';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_CURRENCY)
                {
                    sa.Value__c = '12345';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_CHECKBOX)
                {
                    sa.Value__c = 'true';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_LONG_TEXT)
                {
                    sa.Value__c = 'OneTwoThreeOneTwoThreeOneTwoThreeOneTwoThree';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    sa.Value__c = 'OneTwoThree';
                }
                system.debug('++++ here is an sq ' + sq.Input_Type__c);
                system.debug('++++ here is sa ' + uwRiskEditOpen.fields.get(sq));
            }
            System.assert(!uwRiskEditOpen.fields.isEmpty());
            uwRiskEditOpen.saveRisk();
            
            fields = SICQuestionService.getSicAnswers(risk.Id, questions);
            system.debug('++++ fields ' + fields);
            System.assert(!uwRiskEditOpen.fields.isEmpty());
            System.debug('### uwRiskEditOpen ' + uwRiskEditOpen.errors);
            Test.stopTest();
        }
    }
    
    static testMethod void testPicklistOptions()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        List<SIC_Question__c> questions = new List<SIC_Question__c>(); 
        Map<SIC_Question__c, SIC_Answer__c> fields = new Map<SIC_Question__c, SIC_Answer__c>(); 
        
        System.runAs(uw)
        {
            Test.startTest();
            
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            
            SIC_Code_Detail_Version__c sicCodeVersion = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            questions = SicQuestionService.getAllApplicationQuestions(sicCodeVersion, SicQuestionService.CATEGORY_RISK);
            
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();
            
            List<Sic_Question__c> picklistQuestion = new List<SIC_Question__c>();    
            for (Sic_Question__c sq : questions)
            {
                if(sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    sq.AvailableValuesEn__c = 'Value1;Value2;Value3;Value4';
                    picklistQuestion.add(sq);
                } 
            }
            update picklistQuestion;
            
            // Calling the constructor
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(risk);
            SMEQ_UWRiskEdit uwRiskEditOpen = new SMEQ_UWRiskEdit(sc);
            
            for (Sic_Question__c sq : UWRiskEditOpen.fields.keyset())
            {
                if(sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    system.assert(!uwRiskEditOpen.picklistOptions.get(sq).isEmpty());
                    system.assertEquals(5, uwRiskEditOpen.picklistOptions.get(sq).size());
                    
                } 
            }
            Test.stopTest();
        }
    }
}