/**
  * @author        Saumil Bapat
  * @date          12/6/2016
  * @description   Helper class to populate owner on referral cases
*/
public with sharing class SLID_CopyReferralParentInfo {
  //Name of the case referral record type
  private Static Final String REFERRAL_RECORD_TYPE_NAME = 'CI Referral';

  //Type of mapping
  private Static Final String MAPPING_TYPE = 'Owner';

  public static void CopyReferralParentInfo(List<Case> newRecords)
  {
    System.Debug('~~~SLID_CopyReferralParentInfo');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_CopyReferralParentInfo: ' + Limits.getQueries());
    //Query the fields in the case referral info field set
    List<Schema.FieldSetMember> fieldSetMembers = SObjectType.Case.FieldSets.Referral_Case_Info.getFields();
    
    //Get the 'CI Referral' RecordTypeId
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id referralRecordTypeId = caseRecordTypes.get(REFERRAL_RECORD_TYPE_NAME).getRecordTypeId();

    //List of cases with lanId populated
    List<Case> referralCases = new List<Case>();
    List<Id> referralParentCaseIds = new List<Id>();

    //Iterate over the list of cases
    for (Case newCase : newRecords)
    {
      if(newCase.RecordTypeId == referralRecordTypeId)
      {
        referralCases.add(newCase);
        referralParentCaseIds.add(newCase.Parent_Case__c);
      }
    }

    //Query the referral parent cases
    Map<Id, Case> referralParentCases = new Map<Id, Case>();
    try 
    {
      String caseQuery = 'Select ';
      for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
      {
        caseQuery += fieldSetMember.getFieldPath() + ',';
      }
      caseQuery = caseQuery.SubString(0, caseQuery.length() - 1);
      caseQuery += ' from Case where Id =:referralParentCaseIds';
      System.Debug('~~~' + caseQuery);
      List<SObject> queriedRecords = Database.Query(caseQuery);
      for (SObject queriedRecord : queriedRecords)
      {
        Case queriedCase = (Case) queriedRecord;
        referralParentCases.put(queriedCase.Id, queriedCase);
      }
      //referralParentCases = new Map<Id,Case>( [Select Id, AccountId, ContactId, ProductId, bkrCase_Region__c from Case where Id =:referralParentCaseIds ]);
    }
    catch (Exception e)
    {
      //Create a exception record
      UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_SetReferralCaseOwner','SetReferralCaseOwner','',UTIL_Logging.DEBUG_LEVEL_ERROR);
      UTIL_Logging.logException(log);
    }

    if (referralParentCases != null)
    for (Case newCase : referralCases)
    {
      //Fetch the parent case
      Case parentCase = referralParentCases.get(newCase.Parent_Case__c);

      //If the parent case is not null, copy over the information
      if (parentCase != null)
      {
        for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
        {
          String fieldPath = fieldSetMember.getFieldPath();
          newCase.put(fieldPath, parentCase.get(fieldPath));
        }
      }
    }
    System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_CopyReferralParentInfo: ' + Limits.getQueries());
  }
}