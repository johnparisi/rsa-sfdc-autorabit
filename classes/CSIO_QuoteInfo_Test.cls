/**
 * Author: James Lee
 * Created At: July 11, 2017
 * 
 * Unit tests for CSIO_QuoteInfo.
 */
@isTest
public class CSIO_QuoteInfo_Test
{
    @testSetup
    static void dataSetup()
    {
        
    }
    
    /*
     * Test assertions:
     * 1. The CSIO element for QuoteInfo is completely rendered when quoteNumber is non-null.
     */
    static testMethod void testConvertHasEpolicyNumber()
    {
        User u;
        
        system.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        } 
        
        system.runAs(u)
        {
            String quoteNumber = 'QPC000000TEST';
            
            CSIO_QuoteInfo cqi = new CSIO_QuoteInfo(quoteNumber);
            
            String output = '';
            output += cqi;
            System.assertEquals(
                '<QuoteInfo>' +
                ' <CompanysQuoteNumber>' + quoteNumber + '</CompanysQuoteNumber>' +
                '</QuoteInfo>',
                output
            );
        }
    }
    
    /*
     * Test assertions:
     * 1. The CSIO element for QuoteInfo is not rendered when quoteNumber is null.
     */
    static testMethod void testConvertHasNotEpolicyNumber()
    {
        User u;
        
        system.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        } 
        
        system.runAs(u)
        {
            CSIO_QuoteInfo cqi = new CSIO_QuoteInfo(null);
            
            String output = '';
            output += cqi;
            System.assertEquals(
                '',
                output
            );
        }
    }
}