/**
* @File Name    :   PolicyDomain
* @Description  :   Policy Domain Layer
* @Date Created :   01/14/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Domain
* @Modification Log:
**************************************************************************************
* Ver       Date        Author           Modification
* 1.0       01/14/2017  Habiba Zaman  Created the file/class
 * 
 * The object domain is where the real work happens, this is where we will apply the actual business logic. 
 * We have the main Object Domain which should handle all common processing. 
 * Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
 * Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
 * just implement logic in the appopriate method. 
 */
public with sharing class PolicyDomain {
    
    private PolicyService service = new PolicyService();
    
	public void beforeInsert(List<Policy__c> records){
	}

	public void beforeUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
	}

	public void afterInsert(List<Policy__c> records){
	}

	public void afterUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
	}

	//Policies for GSL Renewals
	public class GSLRenewalDomain{
    private PolicyService service = new PolicyService();
		public void beforeInsert(List<Policy__c> records){
			service.processPolicies(records, null);
			service.processPoliciesForOpportunity(records);

		}

		public void beforeUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
			service.processPoliciesBeforeUpdate(records, oldRecords);
			service.processPoliciesForOpportunity(records);
			service.processCancelledPolicies(records);

		}

		public void afterInsert(List<Policy__c> records){
			service.updateOpportunityPolicyNumber(records);
		}

		public void afterUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
			service.processPoliciesAfterUpdate(records, oldRecords);
			service.updateOpportunityPolicyNumber(records);

		}
	}
}