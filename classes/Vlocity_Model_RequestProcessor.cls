/**
* Vlocity_Model_RequestProcessor
* Author: Cognizant
* Date: May 20 2019
*
* The purpose of this apex class is to provide maodel classes for preparing the JSON that
* would be basically used by the Vlocity framework to generate the pre-requisites for the
* integration procedures to work for establishing the integration with other systems via MuleSOft
*/
public class Vlocity_Model_RequestProcessor{
    
    public cls_ElementValueMap elementValueMap;
    
    public class cls_ElementValueMap {
        // Details for xml node <SignonRq>
        public String SignonRq_SignonTransport_CustId_SPName;     //rsagroup_ca
        public String SignonRq_SignonTransport_CustId_CustPermId;    //XQATB01
        public String SignonRq_ClientDt;    //2019-04-15T12: 51: 59
        public String SignonRq_CustLangPref;    //en
        public String SignonRq_ClientApp_Org;    //00Dc0000003kucmEAA
        public String SignonRq_ClientApp_Name;    //BDIG
        public Integer SignonRq_ClientApp_Version;    //1
        public String SignonRq_rsa_QuoteSource;    //BDIG
        
        // Details for xml node <InsuranceSvcRq>
        public String RqUID;    //36666362-3430-3135-6163-626166323838
        public String InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CurCd;    //CAD
        public String InsuranceSvcRq_ServiceOperationType;  //RQ
        public String InsuranceSvcRq_Context;   //3b95703a-2237-4d64-89d9-d040f69a5ad1
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer;
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal;
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy;
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location[] InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location;
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation[] InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation;
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness;
        public cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness;
        public String PackageSelected;
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer {
        public cls_GeneralPartyInfo GeneralPartyInfo;
        public cls_ProducerInfo ProducerInfo;
        public cls_ItemIdInfo ItemIdInfo;
    }
    
    public class cls_GeneralPartyInfo {
        public cls_NameInfo NameInfo;
        public cls_Addr Addr;
    }
    
    public class cls_NameInfo {
        public cls_CommlName CommlName;
    }
    
    public class cls_CommlName {
        public String CommercialName;   //hd
    }
    
    public class cls_Addr {
        public cls_DetailAddr DetailAddr;
        public String City; //Pickering
        public String StateProvCd;  //ON
        public String PostalCode;   //L1V 6X8
        public String CountryCd;    //CA
    }
    
    public class cls_DetailAddr {
        public String StreetName;   //York
        public String StreetNumber; //18
    }
    
    public class cls_ProducerInfo {
        public String ProducerRoleCd;   //Broker
        public String ContractNumber;   //52497052
    }
    
    public class cls_ItemIdInfo {
        public String AgencyId; //0011700000ri5vMAAQ
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal {
        public cls_ItemIdInfo ItemIdInfo;
        public cls_GeneralPartyInfo GeneralPartyInfo;
        public cls_InsuredOrPrincipalInfo InsuredOrPrincipalInfo;
        public cls_CreditScoreInfo CreditScoreInfo;
    }
    
    public class cls_InsuredOrPrincipalInfo {
        public cls_BusinessInfo BusinessInfo;
    }
    
    public class cls_CreditScoreInfo {
        public String ReferenceNumber;
        public String CreditScore;
        public String CreditDate;
    }
    
    public class cls_BusinessInfo {
        public cls_Area Area;
        public String OperationsDesc;   //Television Repair Shops  [7691A]
        public String BusinessStartDt;  //2001
        public String SICCd;    //7691A
    }
    
    public class cls_Area {
        public String UnitMeasurementCd;    //SqFt
        public String NumUnits; //21212
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy {
        public String id;   //quote
        public String LOBCd;    //csio:1
        public String LanguageCd;   //en
        public String CompanyCd;    //ROY
        public cls_UnderwritingDecisionInfo UnderwritingDecisionInfo;
        public cls_CommlPolicySupplement CommlPolicySupplement;
        public cls_CommlCoverage CommlCoverage;
        public String Segment;  //BPS
        public String Flavour;    //FL2
        public String SalesforceCaseNumber; //01389384
    }
    
    public class cls_UnderwritingDecisionInfo {
        public String UnderwriterId; //XQAT302
    }
    
    public class cls_CommlPolicySupplement {
        public cls_AnnualSalesAmt AnnualSalesAmt;
        public cls_CreditOrSurcharge CreditOrSurcharge;
    }
    
    public class cls_CreditOrSurcharge{
        public cls_NumericValue NumericValue;
        public String DeviateReason;
    }
    
    public class cls_NumericValue{
        public String FormatPct;
    }
    
    public class cls_AnnualSalesAmt {
        public String Amt;  //100000
    }
    
    public class cls_CommlCoverage {
        public String CoverageCd;   //csio:Contents
        public String CoverageDesc; //Contents
        public cls_Limit Limits;
        public cls_Deductible Deductible;
        public String Indicator;    //1
        public cls_CommlCoverageSupplement  CommlCoverageSupplement;
    }
    
    public class cls_CommlCoverageSupplement {
        public cls_ClaimsMadeInfo ClaimsMadeInfo;
    }
    
    public class cls_ClaimsMadeInfo {
        public String ClaimsMadeInd;    //0
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location {
        public cls_ItemIdInfo ItemIdInfo;
        public cls_Addr Addr;
        public String Deleted;  //0
        public String id;   //location1
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation {
        public cls_Construction Construction;
        public cls_BldgImprovements BldgImprovements;
        public cls_BldgProtection BldgProtection;
        public cls_BldgOccupancy BldgOccupancy;
        public cls_AlarmAndSecurity[] AlarmAndSecurity;
        public String LocationRef;  //location1
    }
    
    public class cls_Construction {
        public String YearBuilt;    //2000
        public String NumStories;   //3
        public String BldgConstructionCd;   //1
    }
    
    public class cls_BldgImprovements {
        public String HeatingImprovementYear;   //2010
        public String PlumbingImprovementYear;  //2010
        public String RoofingImprovementYear;   //2010
        public String WiringImprovementYear;    //2010
    }
    
    public class cls_BldgProtection {
        public cls_DistanceToFireStation DistanceToFireStation;
        public cls_DistanceToHydrant DistanceToHydrant;
        public String SprinkleredPct;   //100
    }
    
    public class cls_DistanceToFireStation {
        public String NumUnits; //5
        public String UnitMeasurementCd;    //KMT
    }
    
    public class cls_DistanceToHydrant {
        public String NumUnits; //150
        public String UnitMeasurementCd;    //MTR
    }
    
    public class cls_BldgOccupancy {
        public cls_AreaOccupied AreaOccupied;
    }
    
    public class cls_AreaOccupied {
        public String NumUnits; //1000
        public String UnitMeasurementCd;    //FTK
    }
    
    public class cls_AlarmAndSecurity {
        public String AlarmDescCd;  //csio:998
        public String AlarmTypeCd;  //csio:2
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness {
        public String LOBCd;    //csio:1
        public cls_PropertyInfo PropertyInfo;
    }
    
    public class cls_PropertyInfo {
        public cls_CommlPropertyInfo[] CommlPropertyInfo;
    }
    
    public class cls_CommlPropertyInfo {
        public cls_CommlCoverage[] CommlCoverage;
        public String LocationRef;  //location1
    }
    
    public class cls_Limit {
        public cls_FormatCurrencyAmt FormatCurrencyAmt;
    }
    
    public class cls_Deductible {
        public cls_FormatCurrencyAmt FormatCurrencyAmt;
    }
    
    public class cls_FormatCurrencyAmt {
        public String Amt;  //25000
    }
    
    public class cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness {
        public String LOBCd;    //csio:2
        public cls_LiabilityInfo LiabilityInfo;
    }
    
    public class cls_LiabilityInfo {
        public cls_CommlCoverage CommlCoverage;
        public cls_GeneralLiabilityClassification[] GeneralLiabilityClassification;
    }
    
    public class cls_GeneralLiabilityClassification {
        public String TerritoryCd;  //CA
        public cls_ExposureInfo ExposureInfo;
    }
    
    public class cls_ExposureInfo {
        public String PremiumBasisCd;   //csio:6
        public String Exposure; //0
    }
    
    public class cls_LocationInfo{
        public String BuiltYear                 {get;set;}
        public String TotalArea                 {get;set;}
        public String NumberofStories           {get;set;}
        public String ReplacementValue          {get;set;}
        public String BuildingType              {get;set;}
        public String WallType                  {get;set;}
        public String RoofType                  {get;set;}
        public String HERPUpgrades              {get;set;}
        public String HERPeUpgrade              {get;set;}
        public String HERPhUpgrade              {get;set;}
        public String HERPrUpgrade              {get;set;}
        public String HERPpUpgrade              {get;set;}
        public String FireSprinkler             {get;set;}
        public String FireHydrantDistance       {get;set;}
        public String FireHallDistance          {get;set;}
        public String TotalValueOfStock         {get;set;}
        public String TotalValueOfEquipment     {get;set;}
        public String Province                  {get;set;}
        public String PostalCode                {get;set;}
        public String City                      {get;set;}
        public String RegisteredBusinessAddress {get;set;}
        public String RegisteredBusinessName    {get;set;} 
        public String locNum                    {get;set;}
    }
    
    /*public static requestProcessor2Json parse(String json){
        return (requestProcessor2Json) System.JSON.deserialize(json, requestProcessor2Json.class);
    }*/
    
    /*
    static testMethod void testParse() {
        String json=        '{'+
        '    "elementValueMap": {'+
        '        "RqUID": "36666362-3430-3135-6163-626166323838",'+
        '        "SignonRqSignonTransport_CustId_SPName": "rsagroup_ca",'+
        '        "SignonRq_SignonTransport_CustId_CustPermId": "XQATB01",'+
        '        "SignonRq_ClientDt": "2019-04-15T12: 51: 59",'+
        '        "SignonRq_CustLangPref": "en",'+
        '        "SignonRq_ClientApp_Org": "00Dc0000003kucmEAA",'+
        '        "SignonRq_ClientApp_Name": "BDIG",'+
        '        "SignonRq_ClientApp_Version": 1,'+
        '        "SignonRq_rsa:QuoteSource": "BDIG",'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CurCd": "CAD",'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer": {'+
        '            "GeneralPartyInfo": {'+
        '                "NameInfo": {'+
        '                    "CommlName": {'+
        '                        "CommercialName": "BLUENOSE INSURANCE"'+
        '                    }'+
        '                },'+
        '                "Addr": {'+
        '                    "DetailAddr": {'+
        '                        "StreetNumber": "676",'+
        '                        "StreetName": "GEORGE ST"'+
        '                    },'+
        '                    "StateProvCd": "NS",'+
        '                    "CountryCd": "CA",'+
        '                    "PostalCode": "B1P 1K9",'+
        '                    "City": "SYDNEY"'+
        '                }'+
        '            },'+
        '            "ProducerInfo": {'+
        '                "ProducerRoleCd": "Broker",'+
        '                "ContractNumber": "52497052"'+
        '            },'+
        '            "ItemIdInfo": {'+
        '                "AgencyId": "XQATB01"'+
        '            }'+
        '        },'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal": {'+
        '            "InsuredOrPrincipalInfo": {'+
        '                "BusinessInfo": {'+
        '                    "Area": {'+
        '                        "UnitMeasurementCd": "SqFt",'+
        '                        "NumUnits": "21212"'+
        '                    },'+
        '                    "OperationsDesc": "Television Repair Shops  [7691A]",'+
        '                    "BusinessStartDt": "2001",'+
        '                    "SICCd": "7691A"'+
        '                }'+
        '            },'+
        '            "GeneralPartyInfo": {'+
        '                "NameInfo": {'+
        '                    "CommlName": {'+
        '                        "CommercialName": "hd"'+
        '                    }'+
        '                },'+
        '                "Addr": {'+
        '                    "DetailAddr": {'+
        '                        "StreetNumber": "40",'+
        '                        "StreetName": "carabob court"'+
        '                    },'+
        '                    "CountryCd": "CA",'+
        '                    "PostalCode": "M1T 3N3",'+
        '                    "StateProvCd": "ON",'+
        '                    "City": "Scarborough"'+
        '                }'+
        '            },'+
        '            "ItemIdInfo": {'+
        '                "AgencyId": "001c000001tV0B8AAK"'+
        '            }'+
        '        },'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy": {'+
        '            "id": "quote",'+
        '            "CommlPolicySupplement": {'+
        '                "AnnualSalesAmt": {'+
        '                    "Amt": "100000"'+
        '                }'+
        '            },'+
        '            "SalesforceCaseNumber": "01389384",'+
        '            "Segment": "BPS",'+
        '            "CommlCoverage": {'+
        '                "CommlCoverageSupplement": {'+
        '                    "ClaimsMadeInfo": {'+
        '                        "ClaimsMadeInd": "0"'+
        '                    }'+
        '                },'+
        '                "CoverageCd": "csio:2"'+
        '            },'+
        '            "CompanyCd": "ROY",'+
        '            "LanguageCd": "en",'+
        '            "LOBCd": "csio:1"'+
        '        },'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location": ['+
        '            {'+
        '                "ItemIdInfo": {'+
        '                    "AgencyId": "0011700000ri5vMAAQ"'+
        '                },'+
        '                "Addr": {'+
        '                    "DetailAddr": {'+
        '                        "StreetName": "York",'+
        '                        "StreetNumber": "18"'+
        '                    },'+
        '                    "City": "Pickering",'+
        '                    "StateProvCd": "ON",'+
        '                    "PostalCode": "L1V 6X8",'+
        '                    "CountryCd": "CA"'+
        '                },'+
        '                "Deleted": "0",'+
        '                "id": "location1"'+
        '            }'+
        '        ],'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation": ['+
        '            {'+
        '                "Construction": {'+
        '                    "YearBuilt": "2000",'+
        '                    "NumStories": "3",'+
        '                    "BldgConstructionCd": "1"'+
        '                },'+
        '                "BldgImprovements": {'+
        '                    "HeatingImprovementYear": "2010",'+
        '                    "PlumbingImprovementYear": "2010",'+
        '                    "RoofingImprovementYear": "2010",'+
        '                    "WiringImprovementYear": "2010"'+
        '                },'+
        '                "BldgProtection": {'+
        '                    "DistanceToFireStation": {'+
        '                        "NumUnits": "5",'+
        '                        "UnitMeasurementCd": "KMT"'+
        '                    },'+
        '                    "DistanceToHydrant": {'+
        '                        "NumUnits": "150",'+
        '                        "UnitMeasurementCd": "MTR"'+
        '                    },'+
        '                    "SprinkleredPct": "100"'+
        '                },'+
        '                "BldgOccupancy": {'+
        '                    "AreaOccupied": {'+
        '                        "NumUnits": "1000",'+
        '                        "UnitMeasurementCd": "FTK"'+
        '                    }'+
        '                },'+
        '                "AlarmAndSecurity": ['+
        '                    {'+
        '                        "AlarmDescCd": "csio:998",'+
        '                        "AlarmTypeCd": "csio:2"'+
        '                    },'+
        '                    {'+
        '                        "AlarmDescCd": "csio:998",'+
        '                        "AlarmTypeCd": "csio:1"'+
        '                    }'+
        '                ],'+
        '                "LocationRef": "location1"'+
        '            }'+
        '        ],'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness": {'+
        '            "LOBCd": "csio:1",'+
        '            "PropertyInfo": {'+
        '                "CommlPropertyInfo": ['+
        '                    {'+
        '                        "CommlCoverage": ['+
        '                            {'+
        '                                "CoverageCd": "csio:Contents",'+
        '                                "CoverageDesc": "Contents",'+
        '                                "Limit": {'+
        '                                    "FormatCurrencyAmt": {'+
        '                                        "Amt": "25000"'+
        '                                    }'+
        '                                },'+
        '                                "Indicator": "1"'+
        '                            },'+
        '                            {'+
        '                                "CoverageCd": "csio:Stock",'+
        '                                "CoverageDesc": "Stock",'+
        '                                "Limit": {'+
        '                                    "FormatCurrencyAmt": {'+
        '                                        "Amt": "35000"'+
        '                                    }'+
        '                                },'+
        '                                "Indicator": "1"'+
        '                            },'+
        '                            {'+
        '                                "CoverageCd": "csio:Equipment",'+
        '                                "CoverageDesc": "Equipment",'+
        '                                "Limit": {'+
        '                                    "FormatCurrencyAmt": {'+
        '                                        "Amt": "40000"'+
        '                                    }'+
        '                                },'+
        '                                "Indicator": "1"'+
        '                            },'+
        '                            {'+
        '                                "CoverageCd": "csio:Building",'+
        '                                "CoverageDesc": "Building",'+
        '                                "Limit": {'+
        '                                    "FormatCurrencyAmt": {'+
        '                                        "Amt": "20000"'+
        '                                    }'+
        '                                },'+
        '                                "Indicator": "1"'+
        '                            }'+
        '                        ],'+
        '                        "LocationRef": "location1"'+
        '                    }'+
        '                ]'+
        '            }'+
        '        },'+
        '        "InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness": {'+
        '            "LOBCd": "csio:2",'+
        '            "LiabilityInfo": {'+
        '                "GeneralLiabilityClassification": ['+
        '                    {'+
        '                        "TerritoryCd": "CA",'+
        '                        "ExposureInfo": {'+
        '                            "PremiumBasisCd": "csio:6",'+
        '                            "Exposure": "0"'+
        '                        }'+
        '                    },'+
        '                    {'+
        '                        "TerritoryCd": "CA",'+
        '                        "ExposureInfo": {'+
        '                            "PremiumBasisCd": "csio:6",'+
        '                            "Exposure": "1000000"'+
        '                        }'+
        '                    },'+
        '                    {'+
        '                        "TerritoryCd": "CA",'+
        '                        "ExposureInfo": {'+
        '                            "PremiumBasisCd": "csio:6",'+
        '                            "Exposure": "0"'+
        '                        }'+
        '                    },'+
        '                    {'+
        '                        "TerritoryCd": "CA",'+
        '                        "ExposureInfo": {'+
        '                            "PremiumBasisCd": "rsa:food",'+
        '                            "Exposure": "0"'+
        '                        }'+
        '                    },'+
        '                    {'+
        '                        "TerritoryCd": "CA",'+
        '                        "ExposureInfo": {'+
        '                            "PremiumBasisCd": "csio:7",'+
        '                            "Exposure": "0"'+
        '                        }'+
        '                    }'+
        '                ]'+
        '            }'+
        '        },'+
        '        "InsuranceSvcRq_ServiceOperationType": "R3Q",'+
        '        "InsuranceSvcRq_Context": "3b95703a-2237-4d64-89d9-d040f69a5ad1",'+
        '        "PackageSelected": "01tc0000006ukxMAAQ"'+
        '    }'+
        '}';
        requestProcessor2Json obj = parse(json);
        System.assert(obj != null);
    }
    */
}