@RestResource(urlMapping='/saveUpdated/*')

global with sharing class RA_SaveUpdatedPolicyDevRestWrapper {
    
    @HttpPost
    global static Boolean saveUpdatedPolicy(String caseId, String newEffectiveDate, String isRenewal) {
        
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type','application/json');
        }
        
        SMEQ_SaveUpdatedLoadModel saveParams = new SMEQ_SaveUpdatedLoadModel();
        saveParams.isRenewal = isRenewal;
        saveParams.caseId = caseId;
        saveParams.newEffectiveDate = newEffectiveDate;
           
        Boolean policy = FP_AngularPageController.saveUpdatedQuoteTransaction(JSON.serialize(saveParams));
        
        return policy;
     
	}
}