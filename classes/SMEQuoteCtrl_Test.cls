/**
 * Author: James Lee
 * Created At: November 5, 2016
 * 
 * Unit tests for SMEQuoteCtrl
 */
@isTest
public without sharing class SMEQuoteCtrl_Test
{
    static void postUserDataSetup()
    {
        SMEQ_TestDataGenerator.createTestDataForBrokerStage();
        SMEQ_TestDataGenerator.generateFullTestQuotev2();
    }
    
    static Quote__c getQuote()
    {
        return [
            SELECT id,Package_Type_Selected__c,
            Case__r.Segment__c, Quote_Number__c
            FROM Quote__c
            LIMIT 1
        ];
    }
    
    static testMethod void testGetQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Quote__c quote = getQuote();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc); 
            PageReference p = smeQuoteController.processResponse();
            Test.startTest();
            Continuation con = (Continuation) smeQuoteController.getQuote();
            Test.stopTest();
            System.assertEquals(1, con.getRequests().size());
        }
    }
    
    private static void testGetRateWithOutOfSegmentCase()
    {
        Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
        c.Segment__c = 'Out of segment';
        update c;
        
        Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
        List<SIC_Answer__c> sas = SicQuestionService.getSicAnswers(quote.Id);
        for (SIC_Answer__c sa : sas)
        {
            sa.Value__c = null; // Reset all answers.
        }
        update sas;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
        SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc);
        Continuation con;
        try
        {
            con = (Continuation) smeQuoteController.getQuoteUW();
            System.assert(false, 'Monoline SIC code expected.');
        }
        catch (RSA_ESBException ree)
        {
            System.assertEquals(Label.UW_InvalidSMESICCode, ree.getMessage(), 'Unexpected message caught.');
        }
        catch(Exception e)
        {
            System.assert(false, 'RSA_ESBException expected.');
        }
    }
        
    static testMethod void testGetRateUsingOutOfSegmentCase()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            UserService_Test.setupTestData();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.initializeExternalConnections();
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            Test.startTest();
            testGetRateWithOutOfSegmentCase();
            Test.stopTest();
        }
    }
    
    static testMethod void testGetQuotes()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Test.startTest();
            Quote__c quote = getQuote();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc); 
            PageReference p = smeQuoteController.processResponse();
            Continuation con = (Continuation) smeQuoteController.getQuotes();
            Test.stopTest();
            System.assertEquals(3, con.getRequests().size());
            
        }
    }
    
    static testMethod void testFinalizeQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Quote__c quote = getQuote();
            quote.Quote_Number__c = null;
            update quote;
            Test.startTest();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc); 
            PageReference p = smeQuoteController.processResponse();
            Continuation con = (Continuation) smeQuoteController.finalizeQuote();
            Test.stopTest();
            System.assertEquals(1, con.getRequests().size());
        }
    }
    
    static testMethod void testUploadQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Quote__c quote = getQuote();
            Test.startTest();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc); 
            PageReference p = smeQuoteController.processResponse();
            Continuation con = (Continuation) smeQuoteController.uploadQuote();
            Test.stopTest();
            System.assertEquals(1, con.getRequests().size());
        }
    }
    
    static testMethod void testDownloadQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            Quote__c quote = getQuote();
            quote.Quote_Number__c = 'QPC0000000-01';
            update quote;
            Test.startTest();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc); 
            PageReference p = smeQuoteController.processResponse();
            Continuation con = (Continuation) smeQuoteController.downloadQuote();
            Test.stopTest();
            System.assertEquals(1, con.getRequests().size());
        }
    }
    
    
    private static void testGetQuoteUWWithForeignRevenuePopulated()
    {
        Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
        List<SIC_Answer__c> sas = SicQuestionService.getSicAnswers(quote.Id);
        for (SIC_Answer__c sa : sas)
        {
            if (SMEQuoteCtrl.isQuestion(sa, SicQuestionService.CODE_FOREIGN_REVENUE))
            {
                sa.value__c = '10000';
            }
            else
            {
                sa.Value__c = null; // Reset all other answers.
            }
        }
        update sas;
        
        system.assert(!sas.isEmpty());
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
        SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc);
        Continuation con;
        try
        {
            con = (Continuation) smeQuoteController.getQuoteUW();
            System.assert(false, 'Foreign Revenue expected.');
        }
        catch (RSA_ESBException ree)
        {
            System.assertEquals(Label.UW_InvalidSMESICCode, ree.getMessage(), 'Unexpected message caught.');
        }
        catch(Exception e)
        {
            System.assert(false, 'RSA_ESBException expected.');
        }
    }
    
    static testMethod void testgetQuoteUWwithForeignRevenue()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            UserService_Test.setupTestData();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            SMEQ_TestDataGenerator.initializeUWSicCodeSegments();
            
            Test.startTest();
            testGetQuoteUWWithForeignRevenuePopulated();
            Test.stopTest();
        }
    }
    
    private static void testGetQuoteUWWithUSRevenuePopulated()
    {
        Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
        List<SIC_Answer__c> sas = SicQuestionService.getSicAnswers(quote.Id);
        for (SIC_Answer__c sa : sas)
        {
            if (SMEQuoteCtrl.isQuestion(sa, SicQuestionService.CODE_US_REVENUE))
            {
                sa.value__c = '10000';
            }
            else
            {
                sa.Value__c = null; // Reset all other answers.
            }
        }
        update sas;
        
        system.assert(!sas.isEmpty());
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
        SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc);
        Continuation con;
        try
        {
            con = (Continuation) smeQuoteController.getQuoteUW();
            System.assert(false, 'US Revenue expected.');
        }
        catch (RSA_ESBException ree)
        {
            System.assertEquals(Label.UW_InvalidSMESICCode, ree.getMessage(), 'Unexpected message caught.');
        }
        catch(Exception e)
        {
            System.assert(false, 'RSA_ESBException expected.');
        }
    }
    
    static testMethod void testgetQuoteUWwithUSRevenue()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            UserService_Test.setupTestData();         
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            SMEQ_TestDataGenerator.initializeUWSicCodeSegments();

            Test.startTest();
            testGetQuoteUWWithUSRevenuePopulated();
            Test.stopTest();
        }
    }
    
    
    static testMethod void testGetRateWithMonolineSICCocdePopulated()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            UserService_Test.setupTestData();
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            SMEQ_TestDataGenerator.initializeUWSicCodeSegments();
            
            Test.startTest();  
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            c.Monoline__c = true;
            update c;
            
            Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote(c);
            List<SIC_Answer__c> sas = SicQuestionService.getSicAnswers(quote.Id);
            for (SIC_Answer__c sa : sas)
            {
                sa.Value__c = null; // Reset all answers.
            }
            update sas;
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(sc);
            Continuation con;
            try
            {
                con = (Continuation) smeQuoteController.getQuoteUW();
                System.assert(false, 'Monoline SIC code expected.');
            }
            catch (RSA_ESBException ree)
            {
                System.assertEquals(Label.UW_InvalidSMESICCode, ree.getMessage(), 'Unexpected message caught.');
            }
            catch(Exception e)
            {
                System.assert(false, 'RSA_ESBException expected.');
            }       
            Test.stopTest();
        }
    }
}