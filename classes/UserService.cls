/**
 * UserService
 * Author: James Lee
 * Date: December 20 2016
 * 
 * The user service class contains the service methods for the User object.
 */
public class UserService
{
    // This map stores the user information given their ID.
    private static Map<Id, User> userMap;
    
    @testVisible
    private static String OFFERING_REGION_UNAVAILABLE = 'Offering Project and Region unavailable.';
    @testVisible
    private static String OFFERING_UNAVAILABLE = 'Offering Project unavailable.';
    @testVisible
    private static String REGION_UNAVAILABLE = 'Region unavailable.';
    @testVisible
    private static String INVALID_USER_ID = 'Invalid user Id.';
    
    public static String USERTYPE_STANDARD = 'Standard';
    public static String USERTYPE_POWERPARTNER = 'PowerPartner';
    
    public static final String LANGUAGE_FRENCH = 'fr';
    public static final String LANGUAGE_ENGLISH = 'en';
    
    public static final String OFFERING_SPRNT = 'SPRNT';
    public static final String OFFERING_HUB = 'HUB';
    
    public static final String PROFILE_BROKER = 'Broker';
    
    /**
     * FP-558
     * Retrieve the information for a given user. 
     * Currently only applicable to brokers, to be augmented for UWs in a future story.
     * 
     * @param userId: The Id of a user to retrieve.
     * @exception UserServiceException: 
     * 1. Invalid UserID.
     * 2. Null region.
     * 3. Null offering for brokers.
     */
    @testVisible
    public static User getUser(Id userId)
    {
        if (Utils.getObjectType(userId) != 'User')
        {
            throw new UserServiceException(INVALID_USER_ID);
        }
        else if (userMap == null)
        {
            userMap = new Map<Id, User>();
        }
        else if (userMap.containsKey(userId))
        {
            return userMap.get(userId);
        }
        
        User u = [
            SELECT id, ContactId, Offering_Project_for_Brokers__c, Primary_Region__c, Secondary_Regions__c, Offering_Project__c,
            Profile.Name, FederationIdentifier
            FROM User
            WHERE id = :userId
            LIMIT 1
        ];
        
        userMap.put(u.Id, u);
        
        // Offering project for brokers only applicable if the user is a broker, i.e. has an associated contact.
        if ((u.Offering_Project_for_Brokers__c == null && u.ContactId != null) ||
            u.Primary_Region__c == null)
        {
            if ((u.Offering_Project_for_Brokers__c == null && u.ContactId != null) &&
                u.Primary_Region__c == null)
            {
                throw new UserServiceException(OFFERING_REGION_UNAVAILABLE);
            }
            else if (u.Offering_Project_for_Brokers__c == null && u.ContactId != null)
            {
                throw new UserServiceException(OFFERING_UNAVAILABLE);
            }
            else if (u.Primary_Region__c == null)
            {
                throw new UserServiceException(REGION_UNAVAILABLE);
            }
        }
        
        return u;
    }
    
    /**
     * FP-558
     * Retrieve a user's offering(s).
     * 
     * @param userId: The Id of a user to retrieve an offering for.
     * @return A list of all offerings enabled for the user.
     */
    public static List<String> getOffering(Id userId)
    {
        User u = getUser(userId);
        String offering;
        if (u.Offering_Project__c != null)
        {
            offering = u.Offering_Project__c;
        }
        else
        {
            offering = u.Offering_Project_for_Brokers__c;
        }
        return offering.split(';');
    }
    
    /**
     * FP-558
     * Retrieve a user's region(s).
     * 
     * @param userId: The Id of a user to retrieve an offering for.
     * @return A list of all offerings enabled for the user.
     */
    public static List<String> getRegion(Id userId)
    {
        User u = getUser(userId);
        
        String regions = u.Primary_Region__c;
        if (u.Secondary_Regions__c != null)
        {
            regions += ';' + u.Secondary_Regions__c;
        }
        
        return regions.split(';');
    }

    /*
     * Method to get custom user language
     * @return EN if EN_US or EN_UK; else the default value would be returned
     */
    public static String getFormattedUserLanguage()
    {
        String userLanguage = UserInfo.getLanguage();
        if (String.isNotBlank(userLanguage) && userLanguage.length() > 2)
        {
            return userLanguage.substring(0, 2);
        }
        else
        {
            return userLanguage;
        }
    }

    public class UserServiceException extends Exception
    {
        
    }
}