public class CSIO_InsuredOrPrincipal
{
    private Account insured;
    private Quote__c quote;
    private Risk__c risk;

    private List<Contact> contactList = new List<Contact>();
    private List<Account> accountList = new List<Account>();
    private List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = new List<Quote_Risk_Relationship__c>();
    
    public CSIO_ItemIdInfo itemIdInfo;
    public CSIO_GeneralPartyInfo generalPartyInfo;
    public InsuredOrPrincipalInfo insuredOrPrincipalInfo;
    public CreditScoreInfo creditScoreInfo;
    public static final String CREDIT_SCORE_DEFAULT_VALUE = '1';
    public static final String CREDIT_SCORE_DATE_FORMAT = 'yyyy-MM-dd';
    
    public CSIO_InsuredOrPrincipal(Account insured, Risk__c risk, Quote__c quote)
    {
        this.insured = insured;
        this.risk = risk;
        this.quote = quote;
        convert();
    }

    public CSIO_InsuredOrPrincipal(
        Account insured, Risk__c risk, Quote__c quote,
        List<Account> accountList, List<Contact> contactList, List<Quote_Risk_Relationship__c> quoteRiskRelationshipList)
    {
        this.insured = insured;
        this.risk = risk;
        this.quote = quote;
        this.accountList = accountList;
        this.contactList = contactList;
        this.quoteRiskRelationshipList = quoteRiskRelationshipList;
        convert();
    }
    
    private void convert()
    {
        this.itemIdInfo = new CSIO_ItemIdInfo(insured.Id); //Fixed?

        if (this.quote.ePolicy__c != null) {
            this.generalPartyInfo = new CSIO_GeneralPartyInfo(insured, this.risk);
        } else {
        	if(quoteRiskRelationshipList == null){
                this.generalPartyInfo = new CSIO_GeneralPartyInfo(insured, this.risk);
            }
            else {
            	this.generalPartyInfo = new CSIO_GeneralPartyInfo(insured, quoteRiskRelationshipList, accountList, contactList);
            }
        }
        
        this.insuredOrPrincipalInfo = new InsuredOrPrincipalInfo(this.quote);
        this.creditScoreInfo = new CreditScoreInfo(insured);

    }
    
    public override String toString()
    {
        String xml = '';
        xml += '<InsuredOrPrincipal>';
        xml += this.itemIdInfo;
        xml += this.generalPartyInfo;
        xml += this.insuredOrPrincipalInfo;
        xml += this.creditScoreInfo;
        xml += '</InsuredOrPrincipal>';
        return xml;
    }
    
    private class InsuredOrPrincipalInfo
    {
        private Quote__c q;
        
        public String SICCd;
        public Integer businessStartDate;
        public String operationsDesc;
        
        public Csio_CommlSubLocation.Measurement area;
        public Integer numberOfUnits;
        
        public InsuredOrPrincipalInfo(Quote__c quote)
        {
            this.q = quote;
            convert();
        }
        
        private void convert()
        {
            this.SICCd = this.q.Case__r.bkrCase_SIC_Code__r.SIC_Code__c;
            this.SICCd = '0'.repeat(4 - this.q.Case__r.bkrCase_SIC_Code__r.SIC_Code__c.length()) + this.SICCd;
            this.operationsDesc = this.q.Case__r.bkrCase_SIC_Code__r.Name;
            this.businessStartDate = Integer.valueOf(this.q.Year_Business_Started__c);
            
            this.area = new CSIO_CommlSubLocation.Measurement('rsa:Area', 'SqFt');
            this.area.numUnits = Integer.valueOf(q.Total_Occupied_Area__c);
            if (q.Total_Number_of_Units_on_Policy__c != null && q.Total_Number_of_Units_on_Policy__c != 0)
            {
                this.numberOfUnits = Integer.valueOf(q.Total_Number_of_Units_on_Policy__c);
            }
            else
            {
                this.numberOfUnits = Integer.valueOf(q.Total_Number_Of_Units__c);
            }
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<InsuredOrPrincipalInfo>';
            xml += ' <BusinessInfo>';
            xml += '  <SICCd>' + this.SICCd + '</SICCd>';
            xml += '  <BusinessStartDt>' + this.businessStartDate + '</BusinessStartDt>';
            xml += '  <OperationsDesc>' + XMLHelper.toCData(this.operationsDesc) + '</OperationsDesc>';
            if (this.numberOfUnits != 0)
            {
                xml += ' <rsa:NumberOfUnits>' + this.numberOfUnits + '</rsa:NumberOfUnits>';
            }
            else
            {
                xml += this.area;
            }
            xml += ' </BusinessInfo>';
            xml += '</InsuredOrPrincipalInfo>';
            
            return xml;
        }
    }
    
    public class CreditScoreInfo
    {
        private Account insured;
        
        public String dunsNumber;
        public String creditScore;
        public String creditScoreDt;
        
        public CreditScoreInfo(Account insured)
        {
            this.insured = insured;            
            convert();
        }
        
        private void convert()
        {
            this.dunsNumber = insured.bkrAccount_DUNS__c;
            this.creditScore = CREDIT_SCORE_DEFAULT_VALUE;
            this.creditScoreDt = Datetime.now().format(CREDIT_SCORE_DATE_FORMAT);
        }
        
        public override String toString()
        {
            String xml = '';
            
            if (dunsNumber != null)
            {
                xml += '<CreditScoreInfo>';
                xml += ' <ReferenceNumber>' + this.dunsNumber + '</ReferenceNumber>';
                xml += ' <CreditScore>' + this.creditScore + '</CreditScore>';
                xml += ' <CreditScoreDt>' + this.creditScoreDt + '</CreditScoreDt>';
                xml += '</CreditScoreInfo>';
            }
            
            return xml;
        }
    }
}