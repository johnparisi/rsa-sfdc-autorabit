@isTest
public class LiveChat_CustomSettingRestService_Test
{ 

     /**
     * Method to test LiveChat setting search rest service with variant and type and bundle parameters
     * 1. Assert response is not null
     * 2. Assert customsetting size matches number of records in database
     * 3. Assert response code is ok.
     * 
     */
    @isTest static void testSearchLiveChatSettingByTypeWithTypeAndVariantAndBundle() {

        User uw = UserService_Test.getUwUser('', UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        
        System.runAs(uw)
        {
            setupTestValues();    
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
    
            req.requestURI = '/services/apexrest/searchLiveChatSettings';  
            //send all parms
            req.addParameter('type', 'livechat');
            req.addParameter('variant', 'www');
            req.addParameter('bundle', 'RA');
    
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
    
            SMEQ_CustomSettingResponseModel cSettingResponse =  LiveChat_CustomSettingRestService.searchCustomSettingsByType();
            System.assert(cSettingResponse!=null); 
System.debug (cSettingResponse);
System.debug( cSettingResponse.customSettings.size() );           
 
            //response on test data should have 4 records
            System.assert(cSettingResponse.customSettings.size() ==4);
            
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, cSettingResponse.responseCode);
        }
    }


    /**
    * Add test values for LiveChat_Mapping
    */
    private static void setupTestValues() {

    //insert LiveChat_Mapping__c Custom Settings

    LiveChat_Mapping__c deploymentIDMapping   = new LiveChat_Mapping__c(Name = 'RA-livechat-01',
                                                                Field__c = 'livechat',
                                                                externalValue__c = '00D6300000093xH',
                                                                SFKey__c =  'SFDC_CHAT_DEPLOYMENT_ID',
                                                                variant__c = 'www',
                                                                bundle__c = 'RA'                                                                
                                                            );
    insert deploymentIDMapping;


    LiveChat_Mapping__c buttonIDMapping = new LiveChat_Mapping__c(Name='RA-livechat-02',
                                                           Field__c = 'livechat',
                                                           externalValue__c = '573630000008ORb',
                                                           SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                           variant__c = 'www',
                                                           bundle__c = 'RA'
                                                            );
    insert buttonIDMapping;


    LiveChat_Mapping__c buttonIDMappingFP = new LiveChat_Mapping__c(Name='RA-livechat-02',
                                                               Field__c = 'livechat',
                                                               externalValue__c = '573630000008ORb',
                                                               SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                               variant__c = 'www',
                                                               bundle__c = 'RA'
                                                                );
    insert buttonIDMappingFP;



    LiveChat_Mapping__c orgIDMapping = new LiveChat_Mapping__c(Name='RA-livechat-03',
                                                           Field__c = 'livechat',
                                                           externalValue__c = 'SFDC_ORG_ID',
                                                           SFKey__c = '00D6300000093xH',
                                                           variant__c = 'www',
                                                           bundle__c = 'RA'
                                                            );
    insert orgIDMapping;


    }
    }