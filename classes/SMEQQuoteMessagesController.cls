/**
 * SMEQQuoteMessagesController
 * Author: Suman Haldar
 * Date: June 26 2018
 *
 * The purpose of this controller is to provide appropriate data for the VF page SMEQ_Quote_Warnings
 * which shows warning messages to the underwriter based on the flood zones of the locations risks
 * and other applicable combinations of prior claim and underwriter level.
 * The above VF page also shows the referral/ineligible/warning messages coming from ePolicy to the UW users.
 */
public with sharing class SMEQQuoteMessagesController{

    public Quote__c currentRecord {get; set;} // variable to store the quote record
    public Boolean isFrench {get; set;} // Stores the logged in user's language preference
    public User userRecord; // Stores the current logged in user details
    public List<Claim__c> listClaims; // Stores the claims that are associated with the insured client
    public Boolean isWarning {get; set;} // Denotes whether warning message needs to be displayed or not
    public Boolean isEPolicyMessage {get; set;} // Denotes whether decline message needs to be displayed or not // FRZR-123
    public String warningMessage4FloodZone {get; set;} // Stores the warning message that needs to be displayed for location risk with Flood Zone 4
    public String warningMessage5FloodZone {get; set;} // Stores the warning message that needs to be displayed for location risk with Flood Zone 5
    public Boolean isFloodZone4Present = false; // Denotes whether any risk location with Flood Zone as 4 is present for the current in-scope quote
    public Boolean showFloodZone4Div {get; set;} // Stores indicator to show the div for flood zone 4 section
    public Boolean isFloodZone5Present = false; // Denotes whether any risk location with Flood Zone as 5 is present for the current in-scope quote
    public Boolean showFloodZone5Div {get; set;} // Stores indicator to show the div for flood zone 5section
    public String floodZone4RiskLocations = ''; // Stores the risk numbers for all the locations risks that has Flood Zone as 4
    public String floodZone5RiskLocations = ''; // Stores the risk numbers for all the locations risks that has Flood Zone as 5
    public String ePolicyMessageEnglish {get; set;} // Stores the english messages from ePolicy // FRZR-123
    public String ePolicyMessageFrench {get; set;} // Stores the french messages from ePolicy // FRZR-123
    // private Map<String, String> flavourEpolicyMsgEnMap; // Store the english message from epolicy and its corresponding flavour
    //private Map<String, String> flavourEpolicyMsgFrMap; // Store the french message from epolicy and its corresponding flavour

    public Map<String, List<String>> msgFlavoursEnMap {get; set;} // FRZR-166
    public Map<String, List<String>> msgFlavoursFrMap {get; set;} // FRZR-166
    private List<String> flavoursList; // FRZR-166

    /*
    public Set<String> msgTypeEnSet {get; set;}
    public Set<String> msgTypeFrSet {get; set;}
    public Map<String, Map<String, Set<String>>> flavourTypeEpolicyMsgEnMap {get; set;} // FRZR-166: Store the different type english messages from epolicy and its corresponding flavour
    private Map<String, Set<String>> flavourTypeEpolicyMsgInnerMap; // FRZR-166
    private Set<String> flavourTypeEpolicyMsgSet; // FRZR-166
    public Map<String, Map<String, Set<String>>> flavourTypeEpolicyMsgFrMap {get; set;} // FRZR-166: Store the differemt type french messages from epolicy and its corresponding flavour
    */
    public Map<String,String> replacementMap {get; set;}

    public SMEQQuoteMessagesController(ApexPages.StandardController controller) {
        userRecord = [SELECT LanguageLocaleKey, uwUser_Level__c
                      FROM User
                      WHERE Id=: UserInfo.getUserId()
                      LIMIT 1];

        isFrench = (userRecord.LanguageLocaleKey == 'fr') ? true : false;
        isWarning = false;
        isEPolicyMessage = false; // FRZR-123
        showFloodZone4Div = false;
        showFloodZone5Div = false;

        // The fields Case__c, ePolicy_Message_English__c & ePolicy_Message_French__c are added for story FRZR-123
        this.currentRecord = [SELECT Id, Case__r.bkrCase_Insured_Client__c, Case__c,
                              ePolicy_Message_English__c, ePolicy_Message_French__c,
                                         (SELECT Id, Flood_Zone__c, Name
                                          FROM Risks__r
                                          WHERE RecordType.DeveloperName = 'Location'
                                          AND Flood_Zone__c >= 4)
                              FROM Quote__c
                              WHERE Id =: controller.getRecord().Id];

        // Querying the prior claims for the current insured client which is of Flood type
        listClaims = new List<Claim__c>([SELECT Type_Of_Claim__c
                                         FROM Claim__c
                                         WHERE Account__c =: currentRecord.Case__r.bkrCase_Insured_Client__c
                                         AND Type_Of_Claim__c = 'Flood']);

        // Logic to set variables like : isFloodZone4Present, isFloodZone5Present, floodZone4RiskLocations & floodZone5RiskLocations
        if(this.currentRecord.Risks__r.size() > 0){
            for(Risk__c r: this.currentRecord.Risks__r){
                if(r.Flood_Zone__c == 4){
                    isFloodZone4Present = true;
                    if(floodZone4RiskLocations == '')
                        floodZone4RiskLocations = r.Name;
                    else
                        floodZone4RiskLocations += ','+r.Name;
                }

                if(r.Flood_Zone__c == 5){
                    isFloodZone5Present = true;
                    if(floodZone5RiskLocations == '')
                        floodZone5RiskLocations = r.Name;
                    else
                        floodZone5RiskLocations += ','+r.Name;
                }
            }
        }

        if(isFloodZone4Present && listClaims.size() > 0){ // Section for FZ-4 && Flood claim record present
            showFloodZone4Div = true;
            if(isFrench)
                warningMessage4FloodZone = Label.Flood_Zone_Warning_Message_1+' '+floodZone4RiskLocations+' '+Label.Flood_Zone_4_Warning_Message_2;
            else
                warningMessage4FloodZone = Label.Flood_Zone_Warning_Message_1+' '+floodZone4RiskLocations+' '+Label.Flood_Zone_4_Warning_Message_2;
            isWarning = true;
        }

        if(isFloodZone5Present && userRecord.uwUser_Level__c == 'Level 3'){ // Section for FZ-5 && UW level is 3
            showFloodZone5Div = true;
            if(isFrench)
                warningMessage5FloodZone = Label.Flood_Zone_Warning_Message_1+' '+floodZone5RiskLocations+' '+Label.Flood_Zone_5_Warning_Message_2;
            else
                warningMessage5FloodZone = Label.Flood_Zone_Warning_Message_1+' '+floodZone5RiskLocations+' '+Label.Flood_Zone_5_Warning_Message_2;
            isWarning = true;
        }

        // FRZR-123 : Starts
        replacementMap = new Map<String,String>();
        replacementMap.put('FL1',CSIOXMLProcessor.PACKAGE_STANDARD);
        replacementMap.put('FL2',CSIOXMLProcessor.PACKAGE_OPTION1);
        replacementMap.put('FL3',CSIOXMLProcessor.PACKAGE_OPTION2);
        /*
        replacementMap.put('Refer:','Refer Message');
        replacementMap.put('Référer:','Référer Message');
        replacementMap.put('Ineligible:','Ineligible Message');
        replacementMap.put('Inéligible:','Inéligible Message');
        replacementMap.put('Warning:','Warning Message');

        msgTypeEnSet = new Set<String>();
        msgTypeEnSet.add('Refer');
        msgTypeEnSet.add('Ineligible');
        msgTypeEnSet.add('Warning');

        msgTypeFrSet = new Set<String>();
        msgTypeFrSet.add('Référer');
        msgTypeFrSet.add('Inéligible');
        msgTypeFrSet.add('Warning');
        */

        if(!isFrench){
            if(this.currentRecord.ePolicy_Message_English__c != NULL){
                //flavourEpolicyMsgEnMap = new Map<String, String>();
                //flavourTypeEpolicyMsgEnMap = new Map<String, Map<String, Set<String>>>();
                msgFlavoursEnMap = new Map<String, List<String>>();
                flavoursList = new List<String>();

                for(String commPkgNode: XMLHelper.extractBlocks(this.currentRecord.ePolicy_Message_English__c, 'CommlPkgPolicyQuoteInqRs')){
                    String flavourValue = XMLHelper.extractBlock(commPkgNode,'rsa:Flavour').substringBetween('<rsa:Flavour>','</rsa:Flavour>');

                    if(commPkgNode.contains('<rsa:LiveChatMessage_English>') && commPkgNode.contains('</rsa:LiveChatMessage_English>')){

                        for(String s: XMLHelper.extractBlocks(commPkgNode, 'rsa:LiveChatMessage_English')){
                            if(msgFlavoursEnMap.containsKey(s.substringBetween('<rsa:LiveChatMessage_English>','</rsa:LiveChatMessage_English>'))){
                                flavoursList = msgFlavoursEnMap.get(s.substringBetween('<rsa:LiveChatMessage_English>','</rsa:LiveChatMessage_English>'));
                            }
                            else{
                                flavoursList = new List<String>();
                            }
                            flavoursList.add(flavourValue);
                            msgFlavoursEnMap.put(s.substringBetween('<rsa:LiveChatMessage_English>','</rsa:LiveChatMessage_English>'),flavoursList);
                            /*
                            for(String st: msgTypeEnSet){
                                if(s.contains(st)){
                                    if(flavourTypeEpolicyMsgEnMap.containsKey(flavourValue))
                                        flavourTypeEpolicyMsgInnerMap = flavourTypeEpolicyMsgEnMap.get(flavourValue);
                                    else
                                        flavourTypeEpolicyMsgInnerMap = new Map<String, Set<String>>();
                                    if(flavourTypeEpolicyMsgInnerMap.containsKey(st))
                                        flavourTypeEpolicyMsgSet = flavourTypeEpolicyMsgInnerMap.get(st);
                                    else
                                        flavourTypeEpolicyMsgSet = new Set<String>();

                                    flavourTypeEpolicyMsgSet.add(s.substringBetween('<rsa:LiveChatMessage_English>','</rsa:LiveChatMessage_English>').replace(st+':','* '));
                                    flavourTypeEpolicyMsgInnerMap.put(st,flavourTypeEpolicyMsgSet);
                                    flavourTypeEpolicyMsgEnMap.put(flavourValue,flavourTypeEpolicyMsgInnerMap);
                                }
                            }
                            */
                        }
                        /*
                        String msg = commPkgNode.substringBetween('<rsa:LiveChatMessage_English>','</rsa:LiveChatMessage_English>');
                        if(flavourEpolicyMsgEnMap.containsKey(msg)){
                            String tmpFlavour = flavourEpolicyMsgEnMap.get(msg);
                            tmpFlavour = tmpFlavour + ', ' + flavourValue;
                            flavourEpolicyMsgEnMap.put(msg,tmpFlavour);
                        }
                        else
                            flavourEpolicyMsgEnMap.put(msg,flavourValue);
                        */
                    }
                }
                system.debug('msgFlavoursEnMap -->'+msgFlavoursEnMap);
                /*
                if(!flavourEpolicyMsgEnMap.isEmpty()){
                    for(String s: flavourEpolicyMsgEnMap.keySet()){
                        if(flavourEpolicyMsgEnMap.keyset().size() == 1){
                            ePolicyMessageEnglish = s + '<br/>' + '<br/><b>Package: </b>' + flavourEpolicyMsgEnMap.get(s);
                        }
                        else{
                            if(ePolicyMessageEnglish == NULL)
                                ePolicyMessageEnglish = s + '<br/>' + '<br/><b>Package: </b>' + flavourEpolicyMsgEnMap.get(s);
                            else
                                ePolicyMessageEnglish += '<br/><br/>' + s + '<br/>' + '<br/><b>Package: </b>' + flavourEpolicyMsgEnMap.get(s);
                        }
                    }
                }
                */
                isEPolicyMessage = true;
            }
            /*
            for(String f: replacementMap.keySet()){
                if(ePolicyMessageEnglish != NULL){
                    if(ePolicyMessageEnglish.contains(f))
                        ePolicyMessageEnglish = ePolicyMessageEnglish.replace(f,replacementMap.get(f));
                }
            }
            */
        }
        else{
            if(this.currentRecord.ePolicy_Message_French__c != NULL){
                //flavourEpolicyMsgFrMap = new Map<String, String>();
                //flavourTypeEpolicyMsgFrMap = new Map<String, Map<String, Set<String>>>();
                msgFlavoursFrMap = new Map<String,List<String>>();

                for(String commPkgNode: XMLHelper.extractBlocks(this.currentRecord.ePolicy_Message_French__c, 'CommlPkgPolicyQuoteInqRs')){
                    String flavourValue = XMLHelper.extractBlock(commPkgNode,'rsa:Flavour').substringBetween('<rsa:Flavour>','</rsa:Flavour>');

                    if(commPkgNode.contains('<rsa:LiveChatMessage_French>') && commPkgNode.contains('</rsa:LiveChatMessage_French>')){

                        for(String s: XMLHelper.extractBlocks(commPkgNode, 'rsa:LiveChatMessage_French')){
                            if(msgFlavoursFrMap.containsKey(s.substringBetween('<rsa:LiveChatMessage_French>','</rsa:LiveChatMessage_French>'))){
                                flavoursList = msgFlavoursFrMap.get(s.substringBetween('<rsa:LiveChatMessage_French>','</rsa:LiveChatMessage_French>'));
                            }
                            else{
                                flavoursList = new List<String>();
                            }
                            flavoursList.add(flavourValue);
                            msgFlavoursFrMap.put(s.substringBetween('<rsa:LiveChatMessage_French>','</rsa:LiveChatMessage_French>'),flavoursList);
                            /*
                            for(String st: msgTypeFrSet){
                                if(s.contains(st)){
                                    if(flavourTypeEpolicyMsgFrMap.containsKey(flavourValue))
                                        flavourTypeEpolicyMsgInnerMap = flavourTypeEpolicyMsgFrMap.get(flavourValue);
                                    else
                                        flavourTypeEpolicyMsgInnerMap = new Map<String, Set<String>>();
                                    if(flavourTypeEpolicyMsgInnerMap.containsKey(st))
                                        flavourTypeEpolicyMsgSet = flavourTypeEpolicyMsgInnerMap.get(st);
                                    else
                                        flavourTypeEpolicyMsgSet = new Set<String>();

                                    flavourTypeEpolicyMsgSet.add(s.substringBetween('<rsa:LiveChatMessage_French>','</rsa:LiveChatMessage_French>').replace(st+':','* '));
                                    flavourTypeEpolicyMsgInnerMap.put(st,flavourTypeEpolicyMsgSet);
                                    flavourTypeEpolicyMsgFrMap.put(flavourValue,flavourTypeEpolicyMsgInnerMap);
                                }
                            }
                            */
                        }

                        /*
                        String msg = commPkgNode.substringBetween('<rsa:LiveChatMessage_French>','</rsa:LiveChatMessage_French>');
                        if(flavourEpolicyMsgFrMap.containsKey(msg)){
                            String tmpFlavour = flavourEpolicyMsgFrMap.get(msg);
                            tmpFlavour = tmpFlavour + ', ' + flavourValue;
                            flavourEpolicyMsgFrMap.put(msg,tmpFlavour);
                        }
                        else
                            flavourEpolicyMsgFrMap.put(msg,flavourValue);
                        */
                    }
                }
                system.debug('msgFlavoursFrMap -->'+msgFlavoursFrMap);
                /*
                if(!flavourEpolicyMsgFrMap.isEmpty()){
                    for(String s: flavourEpolicyMsgFrMap.keySet()){
                        if(flavourEpolicyMsgFrMap.keyset().size() == 1){
                            ePolicyMessageFrench = s + '<br/>' + '<br/><b>Multirisque: </b>' + flavourEpolicyMsgFrMap.get(s);
                        }
                        else{
                            if(ePolicyMessageFrench == NULL)
                                ePolicyMessageFrench = s + '<br/>' + '<br/><b>Multirisque: </b>' + flavourEpolicyMsgFrMap.get(s);
                            else
                                ePolicyMessageFrench += '<br/><br/>' + s + '<br/>' + '<br/><b>Multirisque: </b>' + flavourEpolicyMsgFrMap.get(s);
                        }
                    }
                }
                system.debug('ePolicyMessageFrench -->'+ePolicyMessageFrench);
                */
                isEPolicyMessage = true;
            }
            /*
            for(String f: replacementMap.keySet()){
                if(ePolicyMessageFrench != NULL){
                    if(ePolicyMessageFrench.contains(f)){
                        ePolicyMessageFrench = ePolicyMessageFrench.replace(f,replacementMap.get(f));
                    }
                }
            }
            */
        }
        // FRZR-123 : Ends
    }
}