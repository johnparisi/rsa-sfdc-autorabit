/**
* @File Name    :   PolicyTriggerHandler
* @Description  :   Policy Object Handler class
* @Date Created :   01/14/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Trigger Handler
* @Modification Log:
**************************************************************************************
* Ver       Date        Author           Modification
* 1.0       01/14/2017  Habiba Zaman  Created the file/class
* 
* The new Object Handler classes will follow this pattern.
* 
* It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
* Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
* 
* No need to update this unless adding a business unit or record types. 
*/

public with sharing class PolicyTriggerHandler {

	private List<Policy__c> policies = new List<Policy__c>();


	public void beforeInsert(List<Policy__c> records){
		new PolicyDomain().beforeInsert(records);

        initPolicies(records);

        if(policies.size() > 0){
            new PolicyDomain.GSLRenewalDomain().beforeInsert(policies);
        }
    }

    public void beforeUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
 		
 		new PolicyDomain().beforeUpdate(records, oldRecords);

        initPolicies(records);

        if(policies.size() > 0){
            new PolicyDomain.GSLRenewalDomain().beforeUpdate(policies, oldRecords);
        }
    }

    public void afterInsert(List<Policy__c> records){
    	new PolicyDomain().afterInsert(records);

        initPolicies(records);

        if(policies.size() > 0){
            new PolicyDomain.GSLRenewalDomain().afterInsert(policies);
        }

    }
    public void afterUpdate(List<Policy__c> records, Map<Id, Policy__c> oldRecords){
    	new PolicyDomain().afterUpdate(records, oldRecords);

        initPolicies(records);

        if(policies.size() > 0){
            new PolicyDomain.GSLRenewalDomain().afterUpdate(policies, oldRecords);
        }
    
    }

    private void initPolicies(List<Policy__c> records){
        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();

        policyRecordTypes.add(PolicyTypes.get('bkrMMGSL_Policy_NewBusiness'));
        // only load policy those don't have new business record type.
        for (Policy__c p : records){
            if(!policyRecordTypes.contains(p.RecordTypeId)){
                policies.add(p);
            }
        }
    }
}