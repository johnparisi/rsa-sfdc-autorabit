public with sharing class SMEQ_DnBCallOut {
    
    public Map<String, String> dnbSettings {public get; public set;}

    public SMEQ_DnBCallOut(){
        dnbSettings = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('DnBConfiguration');
    }

    /*
    Author: Stephen Piercey
    Date: March 23, 2017

    This calls the ESB match service and returns the ESB model of the dnb result.
    /v1.0/match
    */
    public DNBMatch getCompanyMatch(String name, String streetAddress, String city, String state, String postalCode
        , String phoneNumber, String countryCode){
        SMEQ_DnBRequestModel dnb = new SMEQ_DnBRequestModel();
        dnb.name = name;
        dnb.streetAddress = streetAddress;
        dnb.city = city;
        dnb.state = state;
        dnb.postalCode = postalCode;
        dnb.phoneNumber = phoneNumber;
        dnb.countryCode = countryCode;
        DNBMatch result = getCompanyMatch(dnb);
        System.debug('sep: SMEQ_DnBCallOut.getCompanyMatch result: ' + result);
        return result;
    }

    /*
    Author: Stephen Piercey
    Date: March 23, 2017

    This calls the ESB match service and returns the ESB model of the dnb result.
    /v1.0/match
    */
    public DNBMatch getCompanyMatch(SMEQ_DnBRequestModel requestModel){
        //return processDnBServiceCallOutRequest(requestModel);
        SMEQ_DnBCallOutRequest dnbCallOutRequest =  buildCallOutReqestFromDnBRequestModel(requestModel);
        String responseData = buildHttpRequestAndPerformDnBCallOut(dnbCallOutRequest);
        System.debug('sep: SMEQ_DnBCallOut.getCompanyMatch responseData: ' + responseData);
        DNBMatch obj = DNBMatch.parse(responseData);
        System.debug('sep: SMEQ_DnBCallOut.getCompanyMatch dnbAppend: ' + obj);
        return obj;
    }

    /*
    Author: Stephen Piercey
    Date: March 15, 2017

    This is a D&B (Dun & Bradstreet) "Firmographic Product Service" which will return a message with company reporting details based on the "Detailed Company Profile - Enhanced" reporting.
    The D&B number must be supplied, and additionally an "aged maximum number of days" to identify if an existing cached value can be used based on the age of the message or if a new call needs to be made to retrieve the latest D&B reporting details. A default aged number of days can be defined in the ESB config.properties file.
    /v1.0/append
    */
    public DNBAppend getCompanyAppend(String dunsNumber){
        String responseData = getDNBResponseString(dunsNumber, '/v1.0/append');
        System.debug('sep: SMEQ_DnBCallOut.getCompanyScore responseData: ' + responseData);
        DNBAppend obj = DNBAppend.parse(responseData);
        System.debug('sep: SMEQ_DnBCallOut.getCompanyAppend dnbAppend: ' + obj);
        return obj;
        //Add any errors from the reponse to the responseModel
    }

    /*
    Author: Stephen Piercey
    Date: March 15, 2017
    This is a D&B (Dun & Bradstreet) company score service which will return a message with company scoring based on the "Predictive Bankruptcy & Payment Risk - Enhanced" reporting.
    The D&B number must be supplied, and additionally an "aged maximum number of days" to identify if an existing cached value can be used based on the age of the message or if a new call needs to be made to retrieve the latest D&B reporting details. A default aged number of days can be defined in the ESB config.properties file.
    
    Not fully implemented. Will need to add the ESB model for score and convert the responseData 

    /v1.0/score
    */
    public void getCompanyScore(String dunsNumber){
        String responseData = getDNBResponseString(dunsNumber, '/v1.0/score');
    }


    /*
    Author: Stephen Piercey
    Date: March 15, 2017
    
    This will take a dunsNumber and operation to call the existing SMEQ implementation of communication to the ESB but allow for additional service calls
    */
    private String getDNBResponseString(String dunsNumber, String op){
        //Build the existing SMEQ Request Model
        SMEQ_DnBRequestModel requestModel = new SMEQ_DnBRequestModel();
        requestModel.dunsnumber = dunsNumber;

        //Setup callout model and make the request 
        SMEQ_DnBCallOutRequest dnbCallOutRequest =  buildCallOutReqestFromDnBRequestModel(requestModel);
        String dnbResponse = buildHttpRequestAndPerformDnBCallOut(dnbCallOutRequest,op);

        return dnbResponse;
    }

    private Map<String, Object> getDNBResponseMap(String dunsNumber, String op){        
        //Map the response
        return (Map<String, Object>)JSON.deserializeUntyped(getDNBResponseString(dunsNumber,op));
    }

    /**
    * This method processes the DnBRequest model and returns a response from DnB Service callout
    * @param SMEQ_DnBRequestModel requestModel
    * @return SMEQ_DnBResponseModel
    **/
    @TestVisible 
    public SMEQ_DnBResponseModel processDnBServiceCallOutRequest(SMEQ_DnBRequestModel requestModel){
        SMEQ_DnBResponseModel responseModel = new SMEQ_DnBResponseModel();
        //Build the call out request based on DnBRequestModel
        SMEQ_DnBCallOutRequest dnbCallOutRequest =  buildCallOutReqestFromDnBRequestModel(requestModel);
        //Build the http request and perform dnb esb service based on DnBCalloutRequest
        String dnbResponse = buildHttpRequestAndPerformDnBCallOut(dnbCallOutRequest);
        //Desialize the dnbresponse String
        Map<String, Object> responseDataMap = (Map<String, Object>)JSON.deserializeUntyped(dnbResponse);
        //Check if there are any errors in the response
        List<Object> errorObjectList = (List<Object>) responseDataMap.get('errors');
        if(errorObjectList!=null && errorObjectList.size()>0){
            Boolean noSearchRecordsFoundError = false;
            List<SMEQ_RESTResponseModel.ResponseError> errorList = new List<SMEQ_RESTResponseModel.ResponseError>();
            for(Object errorObject:errorObjectList){
                SMEQ_RESTResponseModel.ResponseError responseError = new SMEQ_RESTResponseModel.ResponseError();
                Map<String, Object> errorObjectMap = (Map<String, Object>)errorObject;
                if(errorObjectMap.get('errorCode')!=null){
                    //If the error code is 800 that means no records are found based on the request
                    responseError.setErrorCode((String)errorObjectMap.get('errorCode'));
                    if(responseError.getErrorCode().equals('800')){
                        noSearchRecordsFoundError = true;
                        break;
                    }
                }
                //If any other error other than 800 then prepare the response with errors
                if(errorObjectMap.get('errorMessage')!=null){
                    responseError.setErrorMessage((String)errorObjectMap.get('errorMessage'));
                }

                if(errorObjectMap.get('detailErrorMessage')!=null){
                    responseError.setdetailErrorMessage((String)errorObjectMap.get('detailErrorMessage'));
                }

                errorList.add(responseError);
                
            }
            //If the error is not 800 then set the response code sent to client as ERROR
            if(!noSearchRecordsFoundError){
                responseModel.setErrors(errorList);
                responseModel.responseCode = SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR;
            }
            
        }
        else{
            processRawResponseToBuildDnBResponseModel(responseDataMap, responseModel);
        }

        return responseModel;
    }

    private void processErrorsFromResponse(SMEQ_DnBResponseModel responseModel, Map<String, Object> responseDataMap){
        List<Object> errorObjectList = (List<Object>) responseDataMap.get('errors');
        if(errorObjectList!=null && errorObjectList.size()>0){
            Boolean noSearchRecordsFoundError = false;
            List<SMEQ_RESTResponseModel.ResponseError> errorList = new List<SMEQ_RESTResponseModel.ResponseError>();
            for(Object errorObject:errorObjectList){
                SMEQ_RESTResponseModel.ResponseError responseError = new SMEQ_RESTResponseModel.ResponseError();
                Map<String, Object> errorObjectMap = (Map<String, Object>)errorObject;
                if(errorObjectMap.get('errorCode')!=null){
                    //If the error code is 800 that means no records are found based on the request
                    responseError.setErrorCode((String)errorObjectMap.get('errorCode'));
                    if(responseError.getErrorCode().equals('800')){
                        noSearchRecordsFoundError = true;
                        break;
                    }
                }
                //If any other error other than 800 then prepare the response with errors
                if(errorObjectMap.get('errorMessage')!=null){
                    responseError.setErrorMessage((String)errorObjectMap.get('errorMessage'));
                }

                if(errorObjectMap.get('detailErrorMessage')!=null){
                    responseError.setdetailErrorMessage((String)errorObjectMap.get('detailErrorMessage'));
                }

                errorList.add(responseError);
                
            }
            //If the error is not 800 then set the response code sent to client as ERROR
            if(!noSearchRecordsFoundError){
                responseModel.setErrors(errorList);
                responseModel.responseCode = SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR;
            }
        }
    }




    
    /**
    * This method processes the raw response and builds the DnBResponse model
    * @SMEQ_DnBResponseModel responseModel
    *
    */
    @TestVisible 
    private void processRawResponseToBuildDnBResponseModel(Map<String, Object> responseDataMap, SMEQ_DnBResponseModel responseModel){
        List<SMEQ_DnBResponseModel.DnBEntry> dbEntryList = new List<SMEQ_DnBResponseModel.DnBEntry>();  
        if(responseDataMap!=null){
            //Extract the DNBresponse
            Map<String, Object> dnBResponseObject =  (Map<String, Object>) responseDataMap.get('DNBResponse');
            if(dnBResponseObject!=null){
                //Extract the GetCleanseMatchResponse
                Map<String, Object> getCleanseMatchResponseObject = (Map<String, Object>)dnBResponseObject.get('GetCleanseMatchResponse');
                if(getCleanseMatchResponseObject!=null){
                    //Extract the GetCleanseMatchResponseDetail
                    Map<String, Object> getCleanseMatchResponseDetailObject = (Map<String, Object>)getCleanseMatchResponseObject.get('GetCleanseMatchResponseDetail');
                
                    if(getCleanseMatchResponseDetailObject!=null){
                        System.debug('getCleanseMatchResponseObject:' + getCleanseMatchResponseObject);
                        //Extract the MatchResponseDetail
                        Map<String, Object> matchResponseDetailObject = (Map<String, Object>)getCleanseMatchResponseDetailObject.get('MatchResponseDetail');
                        if(matchResponseDetailObject!=null){
                            System.debug('matchResponseDetailObject:' + matchResponseDetailObject);
                            //Extract the MatchCandidate
                            List<Object> matchCandidateListObject = (List<Object>)matchResponseDetailObject.get('MatchCandidate');
                            if(matchCandidateListObject!=null && matchCandidateListObject.size()>0){
                                for(Object matchCandidate:matchCandidateListObject){
                                    Map<String, Object> matchCandidateMap = (Map<String, Object>)matchCandidate;
                                    SMEQ_DnBResponseModel.DnBEntry dnBEntry = new SMEQ_DnBResponseModel.DnBEntry();
                                    //Extract the DUNSNumber
                                    if(matchCandidateMap.get('DUNSNumber')!=null){
                                        dnBEntry.DUNSNumber = (String)matchCandidateMap.get('DUNSNumber');
                                    }
                                    //Extract the Organiztion primary name
                                    if(matchCandidateMap.get('OrganizationPrimaryName')!=null){
                                        Map<String, Object> organizationPrimaryNameObject = (Map<String, Object>)matchCandidateMap.get('OrganizationPrimaryName');
                                        //Extract the organiztion name
                                        if(organizationPrimaryNameObject.get('OrganizationName')!=null){
                                            Map<String, Object> organizationNameObject = (Map<String, Object>)organizationPrimaryNameObject.get('OrganizationName');
                                            if(organizationNameObject.get('$')!=null){
                                                dnBEntry.organizationName = (String)organizationNameObject.get('$');
                                            }
                                        }
                                    }

                                    Map<String, Object> primaryAddressObject = (Map<String, Object>)matchCandidateMap.get('PrimaryAddress');    
                                    //Extract the Primary town name
                                    if(primaryAddressObject.get('PrimaryTownName')!=null){
                                        dnBEntry.city = (String) primaryAddressObject.get('PrimaryTownName');
                                    }
                                    //Exctract the postal code
                                    if(primaryAddressObject.get('PostalCode')!=null){
                                        dnBEntry.postalCode = (String) primaryAddressObject.get('PostalCode');
                                    }
                                    //Extract the territory name
                                    if(primaryAddressObject.get('TerritoryAbbreviatedName')!=null){
                                        dnBEntry.province = (String) primaryAddressObject.get('TerritoryAbbreviatedName');
                                    }

                                    //Extract the Street address line
                                    List<Object> streetAddressObjectList = (List<Object>)primaryAddressObject.get('StreetAddressLine');
                                    if(streetAddressObjectList!=null && streetAddressObjectList.size()>0){
                                        for(Object streetAddressObject:streetAddressObjectList){
                                            Map<String, Object> streetAddressMap = (Map<String, Object>)streetAddressObject;
                                            if(streetAddressMap.get('LineText')!=null){
                                                dnBEntry.streetAddress = (String)streetAddressMap.get('LineText');
                                            }
                                            
                                            break;
                                        }
                                    }
                                    
                                    dbEntryList.add(dnBEntry);
                                }
                            }
                        }
                    }
                }
            }
        }
        responseModel.payload = dbEntryList;
    }

    /**
    * This method is build the http request based on the DnB configurations in the custom settings
    * @param SMEQ_DnBCallOutRequest dnbCallOutRequest
    * @return String
    */
    @TestVisible 
    private String  buildHttpRequestAndPerformDnBCallOut(SMEQ_DnBCallOutRequest dnbCallOutRequest){
        
        Http http = new Http();
        //Get the endpoint based on the configuration
        String endpoint = dnbSettings.get('DnBDomain') + dnbSettings.get('DnBMatchOperationEndpoint');
        system.debug('$$'+endpoint);
        //Get the basic authentication from the custom settings
        String authorizationHeader = dnbSettings.get('DnBBasicAuthentication');
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        req.setHeader('Authorization', authorizationHeader);
        system.debug('$$authorizationHeader'+authorizationHeader);
        req.setEndpoint(endpoint);
        //Serialize the object to the JSON string
        system.debug('dnbCallOutRequest::'+dnbCallOutRequest);
        String requestBody =JSON.serialize(dnbCallOutRequest, true);
        system.debug('requestBody::'+requestBody);
        //Replace the StreetAddressLine1 with StreetAddressLine-1
        requestBody.replace('StreetAddressLine1', 'StreetAddressLine-1');
        req.setBody(requestBody);
        System.debug(requestBody);
        HttpResponse res = http.send(req);
        System.debug('ResponseBody::'+res.getBody());
        return res.getBody();
    }

    /*
    Author: Stephen Piercey
    Date: March 15, 2017
    
    This is a more generic implementation of the SMEQ method developed above. It take in an operation string to allow for additional services from ESB
    */
    private String  buildHttpRequestAndPerformDnBCallOut(SMEQ_DnBCallOutRequest dnbCallOutRequest, String op){
        Http http = new Http();
        //Get the endpoint based on the configuration
        String settingsOp = '';
        if(dnbSettings.containsKey(op)){
            settingsOp = dnbSettings.get(op);
        }
        else{
            settingsOp = op;
        }
        String endpoint = dnbSettings.get('DnBDomain') + settingsOp;
        //Get the basic authentication from the custom settings
        String authorizationHeader = dnbSettings.get('DnBBasicAuthentication');
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        req.setHeader('Authorization', authorizationHeader);
        req.setEndpoint(endpoint);
        //Serialize the object to the JSON string
        String requestBody =JSON.serialize(dnbCallOutRequest, true);
        //Replace the StreetAddressLine1 with StreetAddressLine-1
        requestBody.replace('StreetAddressLine1', 'StreetAddressLine-1');
        req.setBody(requestBody);
        HttpResponse res = http.send(req);
        return res.getBody();
    }


    /**
    * This method transforms the SMEQ_DnBRequestModel to SMEQ_DnBCallOutRequest
    * @param SMEQ_DnBRequestModel requestModel
    * @return SMEQ_DnBCallOutRequest
    */
    @TestVisible 
    private SMEQ_DnBCallOutRequest buildCallOutReqestFromDnBRequestModel(SMEQ_DnBRequestModel requestModel){
        SMEQ_DnBCallOutRequest callOutRequest = new SMEQ_DnBCallOutRequest();
        SMEQ_DnBCallOutRequest.CommonParms commonsParams = new SMEQ_DnBCallOutRequest.CommonParms();
        SMEQ_DnBCallOutRequest.RestRequest restRequest = new SMEQ_DnBCallOutRequest.RestRequest();
        //updating the common params based on the configuration in the custom settings
        commonsParams.SourceSystem = dnbSettings.get('DnBSourceSystem');
        commonsParams.ProductionIndicator = Boolean.valueOf(dnbSettings.get('DnBProductionIndicator'));
        //Update the request object based on the incoming request model
        restRequest.SubjectName = requestModel.name;
        restRequest.StreetAddressLine1 = requestModel.streetAddress;
        restRequest.PostalCode = requestModel.postalCode;
        restRequest.TerritoryName = requestModel.state;
        restRequest.PrimaryTownName = requestModel.city;
        restRequest.TelephoneNumber = requestModel.phoneNumber;
        if(!String.isBlank(requestModel.dunsnumber)){
            restRequest.DUNSNumber = requestModel.dunsnumber;
        }
        //Defaulting the country code to Canada based on the configuration from custom settings otherwise; the 
        //country code coming in the request
        //restRequest.CountryISOAlpha2Code = dnbSettings.get('DnBDefaultCountry');
        restRequest.CountryISOAlpha2Code = 'CA';
        if(!String.isBlank(requestModel.countryCode)){
            //restRequest.CountryISOAlpha2Code = requestModel.countryCode;
        }
        callOutRequest.RestRequest = restRequest;
        callOutRequest.CommonParms = commonsParams;
        return callOutRequest;
    }
}