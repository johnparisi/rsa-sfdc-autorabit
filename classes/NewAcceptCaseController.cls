public class NewAcceptCaseController {

    public NewAcceptCaseController(ApexPages.StandardController controller) {

    }

    public Id recordId {get; set;}
     public String conUrl {get; set;}
    public string filterId {get; set;}
    public Pagereference pageUrl {get; set;}
	 public NewAcceptCaseController( ApexPages.StandardSetController controller ) {
         filterId=controller.getFilterId();
     }
    public void getUpdatedRecord(){
        system.debug('url '+apexpages.currentpage());
       // PageReference ref = new PageReference(/{objectprefix}?fcf=00BO0000000LhtN);
         system.debug('setid '+filterId);
          
        string prefix = case.sobjecttype.getDescribe().getKeyPrefix();
		conUrl = ApexPages.currentPage().getHeaders().get('referer')+'console?tsid='+filterId;
        system.debug('conUrl '+conUrl);
         User userDetails = [select id, profile.name from user where id =: userinfo.getUserId()];
         recordId=AcceptNextCase.acceptCase(null,userDetails.profile.name,filterId);
        pageUrl = new PageReference('/'+prefix+'?fcf='+filterId);
        pageUrl.setRedirect(true);
         system.debug('recordId '+recordId);
    }

}