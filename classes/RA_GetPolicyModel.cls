/**
Model to represent data going to and from the ESB classes for getPolicy service.
*/

public with sharing class RA_GetPolicyModel {
    
    public String policyNumber {get; set;}
  	public String continuationReferenceID {get; set;}
  	public String xmlRequest {get; set;}
    public RequestInfo.ServiceType serviceType {get; set;}
    public Case parentCase {get; set;}
    public String quoteId {get; set;}
    public String reasons {get; set;}
    public Date effectiveDate {get; set;}
    public Date transactionEffectiveDate {get; set;}
    public String ePolicyTransactionStatus {get; set;}
}