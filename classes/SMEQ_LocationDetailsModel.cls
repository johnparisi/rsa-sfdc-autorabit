global with sharing class SMEQ_LocationDetailsModel {
    
    public SMEQ_AddressModel address {public get; public set;}
    public Integer yearBuilt {public get; public set;}
    public Double buildingValue {public get; public set;}
    public Double equipmentValue {public get; public set;}
    public Double stockValue {public get; public set;}
    public String constructionType {public get; public set;}
    public Boolean hasSprinklerCoverage {public get; public set;}
    public Integer distanceToFireHydrant {public get; public set;}
    public Integer distanceToFireStation {public get; public set;}
    public Integer totalOccupiedArea {public get; public set;}
    public Integer numberOfStories {public get; public set;}
    public Boolean isBuildingRenovated {public get; public set;}
    public Integer electricalRenovatedYear{public get; public set;}
    public Integer heatingRenovatedYear {public get; public set;}
    public Integer roofingRenovatedYear {public get; public set;}
    public Integer plumbingRenovatedYear {public get; public set;}
    public String typeOfWall{public get; public set;}
    public String typeOfRoof{public get; public set;}
    public String id{public get; public set;}
    public Integer sequenceNumber{public get; public set;}
    public List<SMEQ_CoverageDetailsModel> coverages{get;set;}
    public List<SMEQ_QuestionAnswerModel> questionAnswers {public get; public set;}
    public List<String> LossPayees {public get; public set;}
    public Double rentalRevenue {public get; public set;}
    public Boolean rentalRevenueSelected {public get; public set;}
    public Boolean deleted {public get; public set;} 
    public Integer floodZone {public get; public set;}

}