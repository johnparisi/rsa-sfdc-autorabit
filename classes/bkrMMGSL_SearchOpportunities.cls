public with sharing class bkrMMGSL_SearchOpportunities {

  // the soql without the order and limit
  private String soqlSearchOpportunities {get;set;}
  // the collection of contacts to display
  public List<Opportunity> opportunities {get;set;}

  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'CloseDate'; } return sortField;  }
    set;
  }

  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soqlSearchOpportunities + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }

  // init the controller and display some sample data when the page loads
  public bkrMMGSL_SearchOpportunities() {
    soqlSearchOpportunities = 'select Name, Account.Name, bkrClientAccount__r.Name, bkrClientAccount__r.bkrAccount_CommonName__c, bkrProduct__r.Name, bkrProductLineEffectiveDate__c, Owner.Name, StageName, bkrOpportunity_ReasonWonorLost__c from Opportunity WHERE RecordType.Name = \'Mid-Market & GSL\'';
    runQuery();
  }

  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }

  // runs the actual query
  public void runQuery() {

    try {
      opportunities = Database.query(soqlSearchOpportunities + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, an error has occurred while processing your request.'));
    }

  }

  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {

    String opportunityName = Apexpages.currentPage().getParameters().get('opportunityName');
    String brokerName = Apexpages.currentPage().getParameters().get('brokerName');
    String clientAccountName = Apexpages.currentPage().getParameters().get('clientAccountName');
    String clientAccountOtherCommonName = Apexpages.currentPage().getParameters().get('clientAccountOtherCommonName');

      soqlSearchOpportunities = 'select Name, Account.Name, bkrClientAccount__r.Name, bkrClientAccount__r.bkrAccount_CommonName__c, bkrProduct__r.Name, bkrProductLineEffectiveDate__c, Owner.Name, StageName, bkrOpportunity_ReasonWonorLost__c from Opportunity WHERE RecordType.Name = \'Mid-Market & GSL\'';
      if (!opportunityName.equals(''))
          soqlSearchOpportunities += ' and Name LIKE \''+String.escapeSingleQuotes(opportunityName)+'%\'';
      if (!brokerName.equals(''))
          soqlSearchOpportunities += ' and Account.Name LIKE \''+String.escapeSingleQuotes(brokerName)+'%\'';
      if (!clientAccountName.equals(''))
          soqlSearchOpportunities += ' and bkrClientAccount__r.name LIKE \''+String.escapeSingleQuotes(clientAccountName)+'%\'';
      if (!clientAccountOtherCommonName.equals(''))
          soqlSearchOpportunities += ' and bkrClientAccount__r.bkrAccount_CommonName__c LIKE \''+String.escapeSingleQuotes(clientAccountOtherCommonName)+'%\''; 


    // run the query again
    runQuery();

    return null;
  }
}