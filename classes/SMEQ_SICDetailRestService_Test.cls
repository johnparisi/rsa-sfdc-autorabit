@isTest
private class SMEQ_SICDetailRestService_Test
{
	/**
    * This method provides test assertions to get resource bundles for all variants and Bundle 
    */
    @isTest static void testGetDetail()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/sic/getSICDetail?sicCode=1755A';
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            req.addParameter('sicCode', '1755A');
            SMEQ_SICDetailRestService.ResponseObjectWrapper responseWrapper = SMEQ_SICDetailRestService.getDetail();
            System.debug('Response Objcet:' + responseWrapper);
            System.assert(responseWrapper!=null);
            System.assert(responseWrapper.getResponseCode()!=null);
            System.assertEquals('OK', responseWrapper.getResponseCode());
        }
    }
    
    /**
    * This method provides test assertion when the Sic code is empty 
    */
    @isTest static void testGetEmpty()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/sic/getSICDetail?sicCode=1755A';
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            req.addParameter('sicCode', '');
            SMEQ_SICDetailRestService.ResponseObjectWrapper responseWrapper = SMEQ_SICDetailRestService.getDetail();
            System.debug('Response Objcet:' + responseWrapper);
            System.assert(responseWrapper!=null);
            System.assert(responseWrapper.getResponseCode()!=null);
            System.assertEquals('ERROR', responseWrapper.getResponseCode());
        }
    }
    
    /**
    * This method creates an exception in SMEQ_SICDetailRestService to increase coverage 
    */
    @isTest static void testGetException()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/sic/getSICDetail?sicCode=1755A';
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            req.addParameter('sicCode', null);
            SMEQ_SICDetailRestService.ResponseObjectWrapper responseWrapper = SMEQ_SICDetailRestService.getDetail();
        }
    }    
    /*
    * This method provide test coverage for SMEQ_SICDataModel class
    */
    @isTest static void testSMEQ_SICDataModel()
    {
    	SMEQ_SICDataModel testSICDataModel = new SMEQ_SICDataModel();
    	SMEQ_SICDataModel.Question testQuestion = new SMEQ_SICDataModel.Question();
    	testQuestion.questionId = 1;
    }
	
}