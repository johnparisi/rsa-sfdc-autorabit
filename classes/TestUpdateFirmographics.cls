@isTest
private class TestUpdateFirmographics{
    private static testmethod void test() {

        
        List<Account> accounts = new List<Account>{
            new Account(Name = 'Test'),
            new Account(Name = 'Test', BillingCountry = 'USA')
        };
        insert accounts;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        UpdateFirmographics controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[0]));
        controller.checkLastUpdate();
        controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[1]));
        controller.checkLastUpdate();
        Test.stopTest();
    }
}