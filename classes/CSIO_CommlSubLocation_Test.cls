/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_CommlSubLocation.
 */
@isTest
public class CSIO_CommlSubLocation_Test
{
	@testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    static testMethod void testConvert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c location = SMEQ_TestDataGenerator.getLocationRisk();
            
            Map<String, String> sas = new Map<String, String>();
            sas.put(SicQuestionService.CODE_NUMBER_OF_UNITS, '7');
            sas.put(SicQuestionService.CODE_ONSITE_FACILITIES, 'Bike Rentals');
            
            CSIO_CommlSubLocation ccsl = new CSIO_CommlSubLocation(location, sas);
            String output = '';
            output += ccsl;
            
            Map<String, String> esbDistanceToFireStation = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbDistanceToFireStation');
            Map<String, String> esbDistanceToHydrant = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbDistanceToHydrant');
            
            System.assertEquals(
                '<CommlSubLocation LocationRef="' + location.Id + '">' +
                '<Construction>' +
                '<YearBuilt>' + location.Year_Built__c + '</YearBuilt>' +
                '<NumStories>' + location.Number_of_Stories__c + '</NumStories>' +
                (sas.containsKey(SicQuestionService.CODE_NUMBER_OF_UNITS) &&
                 sas.get(SicQuestionService.CODE_NUMBER_OF_UNITS) != null ?
                 '<NumUnits>' + sas.get(SicQuestionService.CODE_NUMBER_OF_UNITS) + '</NumUnits>' : ''
                ) +
                '<csio:BldgConstructionCd>1</csio:BldgConstructionCd>' +
                '</Construction>' +
                '<BldgImprovements>' +
                ' <HeatingImprovementYear>' + location.Heating_Renovated_Year__c + '</HeatingImprovementYear>' +
                ' <PlumbingImprovementYear>' + location.Plumbing_Renovated_Year__c + '</PlumbingImprovementYear>' +
                ' <RoofingImprovementYear>' + location.Roof_Renovated_Year__c + '</RoofingImprovementYear>' +
                ' <WiringImprovementYear>' + location.Electrical_Renovated_Year__c + '</WiringImprovementYear>' +
                '</BldgImprovements>' +
                '<BldgProtection>' +
                '<DistanceToFireStation>' +
                ' <NumUnits>' + esbDistanceToFireStation.get(location.Distance_To_Fire_Station__c) + '</NumUnits>' +
                ' <UnitMeasurementCd>KMT</UnitMeasurementCd>' +
                '</DistanceToFireStation>' +
                '<DistanceToHydrant>' +
                ' <NumUnits>' + esbDistanceToHydrant.get(location.Distance_To_Fire_Hydrant__c) + '</NumUnits>' +
                ' <UnitMeasurementCd>MTR</UnitMeasurementCd>' +
                '</DistanceToHydrant>' +
                ' <SprinkleredPct>100</SprinkleredPct>' +
                '</BldgProtection>' +
                '<BldgOccupancy>' +
                '<AreaOccupied>' +
                ' <NumUnits>' + location.Total_Occupied_Area__c + '</NumUnits>' +
                ' <UnitMeasurementCd>FTK</UnitMeasurementCd>' +
                '</AreaOccupied>' +
                '</BldgOccupancy>' +
                '<AlarmAndSecurity><AlarmDescCd>csio:998</AlarmDescCd><AlarmTypeCd>csio:2</AlarmTypeCd></AlarmAndSecurity><AlarmAndSecurity><AlarmDescCd>csio:998</AlarmDescCd><AlarmTypeCd>csio:1</AlarmTypeCd></AlarmAndSecurity>' +
                (sas.containsKey(SicQuestionService.CODE_ONSITE_FACILITIES) && 
                 sas.get(SicQuestionService.CODE_ONSITE_FACILITIES) != null ?
                 (new CSIO_CommlSubLocation.ExposureInfo(sas.get(SicQuestionService.CODE_ONSITE_FACILITIES))).toString() : ''
                ) +
                '</CommlSubLocation>',
                output
            );
        }
    }
    
    static testMethod void testExposureInfo() // TODO: Fix in merge
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            String exposureInfo = '';
            
            CSIO_CommlSubLocation.ExposureInfo ccslei = new CSIO_CommlSubLocation.ExposureInfo(exposureInfo);
        }
    }
}