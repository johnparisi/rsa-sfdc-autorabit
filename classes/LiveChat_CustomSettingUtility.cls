public class LiveChat_CustomSettingUtility
{

    private static final String DEFAULT_VARIANT = 'www';
    private static final String LIVECHAT_SFKEY_CHAT_BUTTON = 'SFDC_CHAT_BUTTON_ID';
    private static final String LIVE_CHAT_FIELD_TYPE = 'livechat';
    
    public SMEQ_CustomSettingResponseModel getCustomSettingsForLiveChat(String fieldType, String variant, String bundle) {
        String userRegion = UserService.getRegion(UserInfo.getUserId())[0];
        String userLanguage = UserService.getFormattedUserLanguage();
        if (hasAllParamsPresent(fieldType, userRegion, userLanguage, variant, bundle)) {
            return processResponse(getChatSettings(fieldType,userRegion,userLanguage, variant, bundle));
        }
        return null;
    }    

    @TestVisible private List<LiveChat_Mapping__c> getChatSettings(String fieldType, String region, String language, String variant, String bundle) {
system.debug('getChatSettings');
    
        List<LiveChat_Mapping__c> lcMappingsList = new List<LiveChat_Mapping__c>();
        Set<String> processedIds = new Set<String>();
        lcMappingsList.addAll(getLiveChatMappings(variant, region, language, fieldType, processedIds, false, bundle));
        lcMappingsList.addAll(getLiveChatMappings(variant, null, null, fieldType, processedIds, true, bundle));
system.debug(lcMappingsList);
        return lcMappingsList;
    }    

    private List<LiveChat_Mapping__c> getLiveChatMappings(String variant, String userRegion, String userLanguage, String fieldType,
                                               Set<String> processedIds, Boolean ignoreProcessed, String bundle) {
        List<LiveChat_Mapping__c> lcMappingsList = new List<LiveChat_Mapping__c>();
        if (ignoreProcessed) {
            lcMappingsList = [SELECT externalValue__c, SFKey__c, variant__c, bundle__c FROM LiveChat_Mapping__c
                    WHERE region__c=:userRegion and language__c=:userLanguage and field__c=:fieldType
            and variant__c=:variant and bundle__c=:bundle and SFKey__c not in :processedIds];
        } else {
            lcMappingsList = [SELECT externalValue__c, SFKey__c, variant__c, bundle__c FROM LiveChat_Mapping__c
                    WHERE region__c=:userRegion and language__c=:userLanguage and field__c=:fieldType
                    and variant__c=:variant and bundle__c=:bundle];
        }
        if(!lcMappingsList.isEmpty()) {
            for (LiveChat_Mapping__c lcMapping : lcMappingsList) {
                processedIds.add(lcMapping.SFKey__c);
            }
        }
        lcMappingsList.addAll([SELECT externalValue__c, SFKey__c, variant__c, bundle__c FROM LiveChat_Mapping__c
                WHERE region__c=:userRegion and language__c=:userLanguage and field__c=:fieldType
                and variant__c=:DEFAULT_VARIANT  and bundle__c=:bundle and SFKey__c not in :processedIds]);

        return lcMappingsList;
    }

    private Boolean hasAllParamsPresent(String fieldType, String userRegion, String userLanguage, String variant, String bundle) {
        if (String.isNotBlank(fieldType) && String.isNotBlank(userRegion) && String.isNotBlank(userLanguage) &&
                String.isNotBlank(variant)) {
            return true;
        } else {
            return false;
        }
    }  

    private SMEQ_CustomSettingResponseModel processResponse(List<LiveChat_Mapping__c> settingList) {
        SMEQ_CustomSettingResponseModel smeCustomSettingResponseModel = new SMEQ_CustomSettingResponseModel();
        List<SMEQ_KeyValueModel> smeKeyValueList = new List<SMEQ_KeyValueModel>();
        if (!settingList.isEmpty()) {
            for (LiveChat_Mapping__c setting:settingList) {
                SMEQ_KeyValueModel smeKeyValue = new SMEQ_KeyValueModel();
                smeKeyValue.key = setting.SFKey__c;
                smeKeyValue.value = setting.externalValue__c;
                smeKeyValueList.add(smeKeyValue);
            }
            smeCustomSettingResponseModel.customSettings = smeKeyValueList;
        }
        return smeCustomSettingResponseModel;
    }
    
}