/**

This fetches the current logged in session users account details 

This is Broker user specific, it expects and checks that this broker user 
has an associated Contact record and is associated with a Account record (brokerage).
*/

public without sharing class  SMEQ_UserProfileDao {
  
  /**
     * Fetches the user details for the currently logged in user session
     * @return List of SMEQ_UserProfileDataModel populated with user details
     */

    public SMEQ_UserProfileDataModel getSessionUserProfile() {
        try {
            Id userId = userinfo.getUserId();
            List<User> users = [select ContactId,FederationIdentifier from User where Id = :userId];
            if (users.isEmpty()) {
                throw new SMEQ_ServiceException(Label.User_details_not_found_for_userId + userId);
            }

            User user = users[0];
            Id contactId = user.ContactId;
            if (contactId == null) {
                throw new SMEQ_ServiceException(Label.Invalid_User_details_User_userId + userId + ':' + userinfo.getFirstName() + ' ' + userinfo.getLastName() + Label.does_not_have_an_associated_Contact_record);
            }

            List<Contact> contacts = [select Id, Account.id, Account.Name, lastName, firstName, phone, email, Date_Accepted_Terms__c, EP_Reference_Number__c from Contact where Id = :contactId];
            if (contacts.isEmpty()) {
                throw new SMEQ_ServiceException(Label.Invalid_User_details_User_userId + userId + ':' + userinfo.getFirstName() + ' ' + userinfo.getLastName() + Label.cannot_find_associated_Contact_record_contactId + contactId);
            }

            Contact contact = contacts[0];
            User u = [select Id, Phone, extension, email from User where Id = :UserInfo.getUserId()][0];

            SMEQ_UserProfileDataModel.BrokerAccount ownAccount = new SMEQ_UserProfileDataModel.BrokerAccount();
            ownAccount.accountName = contact.Account.Name;
            ownAccount.accountId = contact.Account.id;

            SMEQ_UserProfileDataModel model = new SMEQ_UserProfileDataModel();
            model.ownBrokerage = ownAccount;
            //public BrokerAccount brokerageHierarchy;
            model.contactId = contact.Id;
            model.phone = u.phone;
            model.phoneExtn = u.extension;
            model.email = u.email;
            model.firstName = contact.firstName;
            model.lastName = contact.lastName;
            //UserService userService = new UserService();
            model.language = UserService.getFormattedUserLanguage();
            model.epReferenceNumber = contact.EP_Reference_Number__c;
            if (user.FederationIdentifier!=null)
            {
                model.contactLoginId = user.FederationIdentifier;
            }
            
            if(contact.Date_Accepted_Terms__c!=null)
            {
                model.acceptedTerms = true;
            }
            else { model.acceptedTerms = false; }

            model.region = UserService.getRegion(UserInfo.getUserId())[0];
            

            // Create Hierarchy
            List<Account> accounts = [select Id, Name, Parent.Id, Parent.Name, Parent.Parent.Id, Parent.Parent.Name from Account where Id = :contact.Account.id];
            Account account = accounts[0];
            SMEQ_UserProfileDataModel.BrokerAccount hierarchy = new SMEQ_UserProfileDataModel.BrokerAccount();

            hierarchy = ownAccount;

            SMEQ_UserProfileDataModel.BrokerAccount parent;
            System.debug('account parent id: ' + account.parent.id + ' account: ' + account);
            If (account.Parent.Id != null) {
                parent = new SMEQ_UserProfileDataModel.BrokerAccount();
                parent.accountId = account.Parent.Id;
                parent.accountName = account.Parent.Name;
                parent.childBrokerAccount=ownAccount ;
                hierarchy = parent;
            }

            if (account.Parent.Parent != null) {
                SMEQ_UserProfileDataModel.BrokerAccount grandParent = new SMEQ_UserProfileDataModel.BrokerAccount();
                grandParent.accountId = account.Parent.Parent.Id;
                grandParent.accountName = account.Parent.Parent.Name;
                grandParent.childBrokerAccount= parent;
                hierarchy = grandParent;
            }

            model.brokerageHierarchy= hierarchy;

            // check also that user has a n Account Id
            return model;
        } catch (Exception se) {
            System.debug(LoggingLevel.ERROR,
                    'Unexpected processing exception invoking getSessionUserProfile(): '
                            + se.getMessage() + ','
                            + 'line:' + se.getLineNumber()
                            + se.getStackTraceString());
            throw new SMEQ_ServiceException(se.getMessage(), se);
        }
    }

}