public class SOQLDataSet
{
    private static final Integer CLAIMS_YEAR_LIMIT = 5;
    
    private static final String LIABILITY_ADDITIONAL_INSURED = 'Liability Additional Insured';
    public static final String ADDITIONAL_INSURED =  'Additional Insured';
    private static final String LOSS_PAYEE = 'Loss Payee';
    private static final String PREMIER_FINANCIER = 'Premier Financier';

    private Id quoteId;
    public String brokerNo;
    
    public Account brokerage;
    public Contact producer;
    public User requestingUser;

    public String sessionId;
    
    public List<Claim__c> claims;
    public Account insured;
    public String caseId;
    public Case smeCase;
    public Quote__c quote;
    public List<Risk__c> locationRisks;
    public Risk__c liabilityRisk;
    public List<Risk__c> policyRisks;
    public List<Coverages__c> liabilityRiskCoverages;
    public Map<String, List<Risk>> locationRisksByPackage;
    public Map<String, Risk> liabilityRiskByPackage;
    public Map<String, List<Risk>> policyRisksByPackage;
    
    public Set<id> renewalAndAmendmentRisk;
    public static Sic_Code_Detail_Version__c scdv;
    
    // additional attributes for bind
    public List<Quote_Risk_Relationship__c> quoteRiskRelationships;
    public Map<Id, Account> accountBindParties;
    public Map<Id, Contact> contactBindParties;
    
    public RequestInfo ri;
    
    public Map<Id, Map<String, String>> questionAnswerMap;
    private List<SIC_Question__c> questions;
    
    public SOQLDataSet(Id sId, RequestInfo ri)
    {
        this.ri = ri;
        
        String objectType = Utils.getObjectType(sId);
        if (objectType == 'case')
        {
            this.caseId = sId;
            getCase(this.caseId);
        }
        else if (objectType == 'quote__c')
        {
            this.quoteId = sId;
            getQuote(this.quoteId);
        }
        
        generateDataset();
    }
    
    private void generateDataset()
    {
        if (smeCase == null)
        {
            getCase(quote.case__c); // Assumes quote was provided
        }
        
        if (quote == null)
        {
            getQuote(smeCase.Id); // Assumes case was provided
        }
        
        getBrokerage();
        getProducer();
        getRequestingUser();
        getSicCodeDetailVersion();
        System.assert(brokerage != null, 'Brokerage should not be null before brokerstage lookup');
        BrokerStageService.getStageBrokerNumber(brokerage);

        sessionId = RA_SessionManagementUtil.getSession(smeCase.id);

        if (ri.svcType == RequestInfo.ServiceType.GET_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.GET_3_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.UPLOAD_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.FINALIZE_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.RAGET_QUOTE ||
            ri.svcType == RequestInfo.ServiceType.RAFINALIZE_QUOTE)
        {
            getInsured();
            getClaims();
            getRisks();
            getCoverages();
        }
        else if (ri.svcType == RequestInfo.ServiceType.BIND_QUOTE ||
                ri.svcType == RequestInfo.ServiceType.SAVE_QUOTE ||
                ri.svcType == RequestInfo.ServiceType.RABIND_QUOTE)
        {
            getInsured();
            getClaims();
            getRisks();
            getCoverages();
            getBindParties();
        }
        
        getSICQuestions();
        
        List<Id> dynamicQuestionParentIds = new List<ID>();
        dynamicQuestionParentIds.add(this.quote.Id);
        if (locationRisks != null)
        {
            for (Risk__c r : locationRisks)
            {
                dynamicQuestionParentIds.add(r.Id);
            }
        }
        questionAnswerMap = getSICAnswers(dynamicQuestionParentIds, questions);
    }
    
    private void getQuote(Id sId)
    {
        //in the situation of Abort, a caseId is passed through, a quote needs to be retrieved
        if (smeCase != null){
            Quote__c childQuote = [SELECT id FROM Quote__c WHERE Case__c = :sId];
            sId = childQuote.id;
        }
        List<Quote__c> q  = [
            SELECT id, Name, Case__c, Case__r.CaseNumber, Case_Status__c,Mailing_Address_Is_The_Same__c,
            Canadian_Revenue__c, US_Revenue__c, Foreign_Revenue__c, Total_Revenue__c,
            Total_Occupied_Area__c, Total_Number_Of_Units__c, Total_Number_of_Units_on_Policy__c,
            Year_Business_Started__c, Currently_Insured__c, case__r.contactId,
            Quote_Date__c, Quote_Number__c, ePolicy__c, createdById,
            Standard_Premium__c, Option_1_Premium__c, Option_2_Premium__c,
            Business_Source_ID__c,Transaction_Effective_Date__c, Retrieved_Package_Code__c,
            Case__r.bkrCase_SIC_Code__r.SIC_Code__c, Case__r.bkrCase_SIC_Code__r.Name, 
            Case__r.Offering_Project__c, Claim_Free_Years__c, Selected_Packages_with_Privacy_Breach__c,Renewal_Reason_s__c, 
            Selected_Packages_to_Deviate__c, Calculated_Deviation__c, Deviation_Reason_s__c,
            Terms_And_Conditions__c, Scheduled_Items__c, Effective_Date__c, Policy_Payment_Plan_Type__c,
            Downpayment_Amount__c, Monthly_Withdrawal_Preferred_Day__c, Payment_Frequency__c,
            Bank_Transit_Id__c, Bank_Account_Number__c, Credit_Card_Type__c, Status__c,
            Credit_Card_Expiration_Month__c, Credit_Card_Expiration_Year__c, Credit_Card_Name__c,Language__c, Reasons__c,
            Address_Line_1__c, Address_Line_2__c, City__c, State_Province__c, Postal_Code__c, Country__c
            FROM Quote__c
            WHERE id = :sId
            LIMIT 1
        ];
        
        if (!q.isEmpty())
        {
            quote = q[0];
            if (caseId == null)
            {
                this.caseId = quote.Case__c;
            }
            
            if (quote.Year_Business_Started__c == null)
            {
                throw new RSA_ESBException(Label.Insured_Client_Year_Validation);
            }
        }
        // make sure that quote isn't null
        System.assert(quote != null, 'Quote is null after lookup on the SOQLDataSet constructor, bad ID passed');
    }
    
    public User getAgencyId()
    {
        List<User> agencyId = new List<User>();
        if(quote != null){
            if ( quote.case__r.contactId != NULL ) {
                agencyId = [SELECT Id, FederationIdentifier
                            FROM User 
                            WHERE ContactId = : quote.case__r.contactId ];
            }
        }
        //adding logic for GETPOLICY when the quote has still not been created, using the case directly to get user information 
        if (ri.svcType == RequestInfo.ServiceType.GET_POLICY){
             agencyId = [SELECT Id, FederationIdentifier
                            FROM User 
                            WHERE ContactId = : smeCase.contactId ];
        }

        if (!agencyId.isEmpty())
            return agencyId.get(0);
        else
            return null;
    }
    
    @TestVisible
    private void getCase(Id caseId)
    {
    	System.assert(caseId != null, 'caseID should not be null on getCase on SOQLDataSet');
        List<Case> c;
        try
        {
            c = [
                SELECT id, Account.Id, Contact.Id, bkrCase_Insured_Client__c, CreatedDate, OwnerId, Offering_Project__c, CaseNumber, Com__c,
                Business_Source_ID__c, bkrCase_Subm_Type__c, New_Business_Source__c 
                FROM Case
                WHERE id = :caseId
                LIMIT 1
            ];
        }
        catch (NullPointerException npe)
        {
            //Fix this
        }
        catch (Exception e)
        {
            //Fix this
        }
        
        if (!c.isEmpty())
        {
            smeCase = c[0];
        }
        // make sure that case isn't null
        System.assert(smeCase.Id != null, 'Case is null after lookup on the SOQLDataSet constructor, bad ID passed');
        System.assert(smeCase.AccountId != null, 'smeCase.AccountId should not be null at getBrokerage on SOQLDataSet');
    }
    
    private void getSicCodeDetailVersion()
    {
        scdv = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(smeCase.Id)[0];
    }
    
    /*
     * Retrieve the brokerage account from the case.
     */
    private void getBrokerage()
    {
        List<Account> a;
        System.assert(smeCase != null, 'smeCase should not be null at getBrokerage on SOQLDataSet');
        try
        {
            a = [
                SELECT id, Name,
                BillingStreet, Billing_Address_Line2__c, BillingCity, BillingPostalCode, BillingState, BillingCountry,
                bkrAccount_HuonBrokerNumber__c, RecordTypeId
                FROM Account
                WHERE id = :smeCase.AccountId
                LIMIT 1
            ];
        }
        catch (NullPointerException npe)
        {
            //Fix this
        }
        catch (Exception e)
        {
            //Fix this
        }
        
        if (!a.isEmpty())
        {
            brokerage = a[0];
        }
        System.assert(brokerage != null, 'Brokerage should not be null after lookup on SOQLDataSet');
    }
    
    private void getProducer()
    {
        List<Contact> c;
        try
        {
            c = [
                SELECT id, FirstName, LastName
                FROM Contact
                WHERE id = :smeCase.Contact.Id
                LIMIT 1
            ];
        }
        catch (NullPointerException npe)
        {
            //Fix this
        }
        catch (Exception e)
        {
            //Fix this
        }
        
        if (!c.isEmpty())
        {
            producer = c[0];
        }
    }
    
    public User getUnderwriterId(){
        List<User> contextUser = [ 	SELECT id, FederationIdentifier 
                                    FROM User 
                                    WHERE id = :UserInfo.getUserId() ];
        if ( !contextUser.isEmpty() )
            return contextUser.get(0);
        else
            return null;
    }    
    
    private void getRequestingUser()
    {
        List<User> u = [
        	SELECT id, FederationIdentifier 
         	FROM User 
         	WHERE id = :smeCase.OwnerId
     	];
        
        if (!u.isEmpty())
        {
            requestingUser = u[0];
        }
        
        if (!Test.isRunningTest() && requestingUser != null && requestingUser.FederationIdentifier == null)
        {
            throw new RSA_ESBException(Label.Case_owner + smeCase.OwnerId + Label.DoesNotFedID);            
        }
    }
    
    
    private void getInsured()
    {
        List<Account> a;
        try
        {
            a = [
                SELECT id, Name, 
                BillingStreet, Billing_Address_Line2__c, BillingCity, BillingPostalCode, BillingState, BillingCountry, 
                bkrAccount_DUNS__c, RecordTypeId
                FROM Account
                WHERE id = :smeCase.bkrCase_Insured_Client__c
                LIMIT 1
            ];
        }
        catch (NullPointerException npe)
        {
            throw new RSA_ESBException(Label.InsuredClientAccountIsNotAssigned + quote.Case__c + Label.for_this_Quote + quote.Id + Label.ParenthDot);
        }
        
        if (!a.isEmpty())
        {
            insured = a[0];
        }
        else
        {
            throw new RSA_ESBException(Label.InsuredClientAccountIsNotAssigned + quote.Case__c + Label.for_this_Quote + quote.Id + Label.ParenthDot);
        }
        
        if (!isProperPostalCode(insured.BillingPostalCode))
        {
            throw new RSA_ESBException(Label.Insured_client_s_postal_code + insured.BillingPostalCode + Label.of_client_is_invalid);
        }
        
    }
    
    private void getClaims()
    {
        this.claims = new List<Claim__c>();
        List<Claim__c> cls;
        try
        {
            cls = [
                SELECT Id, Account__c, Amount__c, Month__c, Year__c, HUON_Claim__C, Type_of_Claim__c
                FROM Claim__c
                WHERE Account__c = :insured.Id
                AND Year__c >= :Date.today().year() - CLAIMS_YEAR_LIMIT // All claims within limit
            ];
        }
        catch (NullPointerException npe)
        {
            //Fix this
        }
        catch (Exception e)
        {
            //Fix this
        }
        
        // Only use the quote date
        Date quoteDate = quote.Quote_Date__c != null ? quote.Quote_Date__c : Date.today();
        
        System.debug('### claims ' + cls);
        
        if (cls != null && cls.size() > 0) //Fix this
        {
            for (Claim__c cl : cls)
            {
                // Skip claims that are from 5 years and the month is earlier than the month of the quote date.
                if (cl.year__c == Date.today().year() - CLAIMS_YEAR_LIMIT && Integer.valueOf(cl.Month__c) < quoteDate.month())
                {
                    continue;
                }
                
                claims.add(cl);
            }
        }
    }
    
    private void getRisks()
    {
        Map<String,Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        
        List<Risk__c> rs = [
            SELECT id, RecordTypeId, Quote__c, 
            Registered_Business_Name__c, 
            Address_Line_1__c, Address_Line_2__c, City__c, Province__c, Postal_Code__c, Country__c,
            Distance_To_Fire_Hydrant__c, Distance_To_Fire_Station__c, 
            Electrical_Renovated_Year__c, Heating_Renovated_Year__c, Plumbing_Renovated_Year__c, Roof_Renovated_Year__c,
            Number_of_Stories__c, Sprinkler_Coverage__c, Construction_Type__c,
            Total_Occupied_Area__c, Year_Built__c,
            Building_Value__c, Equipment_Value__c, Stock_Value__c, 
            Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c,
            Mailing_City__c , Mailing_Country__c, Mailing_Address_Postal_Code__c, 
			Mailing_Province__c , Mailing_Address_Street__c , Business_Description__c, Deleted__c,
            Quote__r.Address_Line_1__c, Quote__r.Address_Line_2__c, Quote__r.City__c, related_com__c,
            Quote__r.Country__c, Quote__r.State_Province__c, Quote__r.Postal_Code__c, Quote__r.ePolicy__c,
            Quote__r.Status__c, Quote__r.Mailing_Address_Is_The_Same__c
            FROM Risk__c
            WHERE Quote__c = :quote.Id
        ];
        
        this.locationRisks = new List<Risk__c>();
        this.policyRisks = new List<Risk__c>();
        
        for (Risk__c r : rs)
        {
            if (RiskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION) == r.RecordTypeId)
            {
                if (!isProperPostalCode(r.Postal_Code__c))
                {
                    throw new RSA_ESBException(Label.RiskParen + r.Id + Label.s_postal_code + r.Postal_Code__c + Label.is_invalid);
                }else if(r.Total_Occupied_Area__c == null || r.Number_of_Stories__c == null || r.Address_Line_1__c == null
                		|| r.Distance_To_Fire_Hydrant__c == null || r.Distance_To_Fire_Station__c == null|| r.City__c == null
                		|| r.Year_Built__c == null || r.Province__c == null || r.Country__c == null || r.Construction_Type__c == null){
                			
                	throw new RSA_ESBException(Label.Please_fill_in_all_required_fields_for_Risk + r.Id + ')');
                }
                
                this.locationRisks.add(r);
            }
            else if (RiskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY) == r.RecordTypeId)
            {
                this.liabilityRisk = r;
            }
            else // All other risks will be related to policy.
            {
                this.policyRisks.add(r);
            }
        }
        
        if (this.locationRisks.size() == 0)
        {
            throw new RSA_ESBException(Label.At_least_one_location_risk_must_be_included_for_the_quote + quote.Id + ')');
        }
        
        if (this.liabilityRisk == null)
        {
            throw new RSA_ESBException(Label.No_liability_risk_found_for_quote + quote.Id + ')');
        }
        
        renewalAndAmendmentRisk = RiskService.isRenewalAndAmendment(locationRisks);
    }
    
    private void getCoverages()
    {
        this.locationRisksByPackage = new Map<String, List<Risk>>();
        this.liabilityRiskByPackage = new Map<String, Risk>();
        this.policyRisksByPackage = new Map<String, List<Risk>>();
        
        List<Coverages__c> locationCoverages = [
            SELECT id, Deductible_Value__c, Limit_Value__c, Package_ID__c, Selected__c, Type__c, Risk__c,
            Risk__r.Related_Com__c, TransactionType__c
            FROM Coverages__c
            WHERE Risk__c IN :this.locationRisks
            ORDER BY Package_ID__c
        ];
		system.debug('location coverages before you touch them' + locationCoverages);        

        // Iterate over location coverages and add any in coverages_selected__c which aren't already in location coverages.
        
        // Liability section.
        List<Coverages__c> liabilityCoverages = [
            SELECT id, Deductible_Value__c, Limit_Value__c, Package_ID__c, Selected__c, Type__c, Risk__c, TransactionType__c
            FROM Coverages__c
            WHERE Risk__c = :liabilityRisk.Id
            ORDER BY Package_ID__c
        ];
        
        for (Coverages__c c : liabilityCoverages)
        {
            if (c.Limit_Value__c != null && Math.mod(Integer.valueOf(c.Limit_Value__c), 1000000) != 0)
            {
                throw new RSA_ESBException(Label.CGL_limit_value_must_be_multiples_of_millions_Current_value + c.Limit_Value__c + Label.for_the + c.Package_ID__c + Label.packagePeriod);
            }
        }
    
        // Other policy-level coverage section.
        List<Coverages__c> policyCoverages = new List<Coverages__c>();
        if (!this.policyRisks.isEmpty())
        {
            policyCoverages = [
                SELECT id, Deductible_Value__c, Limit_Value__c, Package_ID__c, Selected__c, Type__c, Risk__c, TransactionType__c
                FROM Coverages__c
                WHERE Risk__c IN :this.policyRisks
                ORDER BY Package_ID__c
            ];
        }
        
        for (Coverages__c c : policyCoverages)
        {
            if(renewalAndAmendmentRisk.contains(c.Risk__c) && (c.Limit_Value__c == null || c.Deductible_Value__c == null) && c.selected__c == true){
                throw new RSA_ESBException(Label.Missing_Coverage_Data + c.Type__c);
            }
        }
        liabilityRiskCoverages = liabilityCoverages;

        
        Schema.DescribeFieldResult packageIDResult = Coverages__c.Package_ID__c.getDescribe();
        List<Schema.PicklistEntry> packageTypes = packageIDResult.getPicklistValues();
        
        // Iterate over the package IDs listed in the Coverages object.
        for (Schema.PicklistEntry pkg : packageTypes)
        {
            String packageId = pkg.getValue();  
            
            List<Risk> locationRiskPackages = new List<Risk>();
            Risk liabilityRiskPackages = new Risk();
            List<Risk> policyRiskPackages = new List<Risk>();
            
            for (Risk__c r : locationRisks)
            {
                Risk rsk = getRiskCoverages(r, packageId, locationCoverages);
                locationRiskPackages.add(rsk);
            }
            
            liabilityRiskPackages = getRiskCoverages(this.liabilityRisk, packageId, liabilityCoverages);
            
            for (Risk__c r : this.policyRisks)
            {
                Risk rsk = getRiskCoverages(r, packageId, policyCoverages);
                policyRiskPackages.add(rsk);
            }
            
            this.locationRisksByPackage.put(packageId, locationRiskPackages);
            this.liabilityRiskByPackage.put(packageId, liabilityRiskPackages);
            this.policyRisksByPackage.put(packageId, policyRiskPackages);
        }
    }
    
    private void getBindParties()
    {
        // grab all the accounts and contacts that match
        this.quoteRiskRelationships = [
            SELECT Id, Party_Type__c, Contact__r.Id, Account__r.Id, Related_Risk__r.Id, Relationship_Type__c, Deleted__c 
            FROM Quote_Risk_Relationship__c 
            WHERE (Party_Type__c = :LIABILITY_ADDITIONAL_INSURED
                   OR Party_Type__c = :ADDITIONAL_INSURED
                   OR Party_Type__c = :LOSS_PAYEE
                   OR Party_Type__c = :PREMIER_FINANCIER) 
            AND Quote__r.Id = :quoteId
        ];
        
        Set<Id> contactIDSet = new Set<Id>();
        Set<Id> accountIDSet = new Set<Id>();
        
        Map<String, Integer> partyTypeValidationCount = new Map<String, Integer>();
        
        for (Quote_Risk_Relationship__c qrr : this.quoteRiskRelationships)
        {
            if (qrr.account__c != null)
            {
                accountIDSet.add(qrr.Account__c);
            }
            else if (qrr.Contact__c != null)
            {
                contactIDSet.add(qrr.Contact__c);
            }
            
            Integer count = partyTypeValidationCount.get(qrr.Party_Type__c); // Get count for this party type.
            if (count == null)
            {
                count = 0;
            }
            partyTypeValidationCount.put(qrr.Party_Type__c, ++count); // Increment and put back into map.
        }
        
        Integer pfCount = partyTypeValidationCount.get('Premium Financier');
        Integer ocbCount = partyTypeValidationCount.get('Other Client Bill');
        
        if (pfCount != null)
            System.assert(pfCount <= 1, 'Only one premium financier expected.');
        if (ocbCount != null)
            System.assert(ocbCount <= 1, 'Only one other client bill expected.');
        
        this.contactBindParties = new Map<Id, Contact>([
            SELECT Id, FirstName, LastName, Attention__c, Contract_Number__c,  MailingStreet, MailingCity, MailingCountry, MailingState, MailingPostalCode, Email, Phone
            FROM Contact
            WHERE Id in :contactIDSet
        ]);
        
        this.accountBindParties = new Map<Id, Account> ([
            SELECT Id, Name, Phone, BillingStreet, Billing_Address_Line2__c,  BillingState, BillingPostalCode, BillingCity, BillingCountry, Contract_Number__c
            FROM Account
            WHERE Id in :accountIDSet
        ]);
    }
    
    /**
     * This method compiles a list of coverages into a package with its parent risk.
     * This method also includes a list of add-on coverages which are not included for the parent risk.
     * 
     * @param r: The parent risk.
     * @param packageId: The package type of the coverages. i.e. Standard, Option 1, Option 2
     * @param coverages: The list of coverages to add to the parent risk.
     * @return: A Risk object.
     */
    private Risk getRiskCoverages(Risk__c r, String packageId, List<Coverages__c> coverages)
    {
        Map<String, Id> riskTypes = Utils.getRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        
        Risk rsk = new Risk();
        rsk.packageId = packageId;
        rsk.risk = r;
        
        rsk.coverages = new List<Coverages__c>();
        for (Coverages__c coverage : coverages)
        {
            if (coverage.Package_ID__c == packageId && coverage.Risk__c == r.Id)
            {
                // FRZR-181
                if (! (coverage.Type__c == 'Building' && coverage.Limit_Value__c == null)) {
                    rsk.coverages.add(coverage);
                }
            }
        }
        
        // Find the add-on coverages which are not selected in the package.
        rsk.removedCoverages = new List<Coverages__c>();
        if (r.RecordTypeId == riskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION))
        {
            if (ri.svcType == RequestInfo.ServiceType.UPLOAD_QUOTE){ // FP-6460
                CSIO_CommlCoverage.sendToEpolicy = true;
            }
            for (String s : scdv.Coverage_Add_ons__c.split(';'))
            {
                Boolean packageHasCoverage = false;
                for (Coverages__c c : coverages)
                {
                    if (c.Type__c == s && 
                        c.Package_Id__c == packageId)
                    {
                        if (c.limit_value__c == null && // Add-on coverages without limits will default to false.
                            ri.svcType != RequestInfo.ServiceType.GET_3_QUOTE) // R3Q is exempt, as add-on coverages cannot be passed.
                        {
                            break;
                        }
                        
                        packageHasCoverage = true;
                        break;
                    }
                }
                
                if (!packageHasCoverage)
                {
                    Coverages__c c = new Coverages__c();
                    c.Type__c = s;
                    c.Risk__c = r.Id;
                    c.Package_ID__c = packageId;
                    c.Limit_Value__c = -1; // This negative value will determine whether or not in denote as removed.
                    rsk.removedCoverages.add(c);
                }
            }
        }
        
        return rsk;
    }
    
    /*
     * This inner class organizes a list of coverages under a location risk in 
     * a way which facilitates the conversion into a CSIO XML request format.
     */
    public class Risk
    {
        public String packageId;
        public Risk__c risk;
        public List<Coverages__c> coverages;
        public List<Coverages__c> removedCoverages;
    }
    
    /*
     * This function will test the postal code string to ensure it follows the accepted format for Canadian postal codes.
     * 
     * @param postalCode The string to be tested.
     * @return true if the string is a valid Canadian postal code, false otherwise.
     */
    @testVisible
    private static boolean isProperPostalCode(String postalCode)
    {
        if (postalCode == null)
        {
            return false;
        }
        
        Pattern myPattern = pattern.compile('^(?!.*[DFIOQUdfioqu])[A-VXYa-vxy][0-9][A-Za-z][0-9][A-Za-z][0-9]$');
        Matcher myMatcher = myPattern.matcher(postalCode.remove(' '));
        return myMatcher.matches();
    }
    
    /*
     * Checks and returns a ; delimited list of all location coverage types.
     */
    private static String getAllStandardCoverageTypes()
    {
        String coveragesSelected = '';
        
        Schema.DescribeFieldResult typeResult = Coverages__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> coverageTypes = typeResult.getPicklistValues();
        
        for (Schema.PicklistEntry typ : coverageTypes)
        {
            if (typ.getValue() == RiskService.COVERAGE_CGL ||
                typ.getValue() == RiskService.COVERAGE_AGGREGATE ||
                typ.getValue() == RiskService.COVERAGE_CYBER) // Excludes CGL
            {
                continue;
            }
            
            if (coveragesSelected != '')
            {
                coveragesSelected += ';';
            }
            
            coveragesSelected += typ.getValue();
        }
        
        return coveragesSelected;
    }
    
    /*
     * FP-760
     * 
     * This method will retrieve a list of SIC Questions related to a case's SIC Code Detail Version.
     */
    private void getSICQuestions()
    {
        this.questions = new List<SIC_Question__c>();
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(smeCase.Id);
        if (!scdvs.isEmpty())
        {
            this.questions = SicQuestionService.getSicQuestions(scdvs[0]);
        }
    }
    
    /*
     * FP-760
     * 
     * This method will return a list of SIC Questions.
     * 
     * @param QuoteOrRiskIds: A list of quote or risk IDs.
     * @param sqs: A list of SIC questions to retrieve answers for.
     * @return: A map of maps. The first map will map from the sObject id to another map. The second map will map from a SIC Question's questionID to its answer.
     */
    private static Map<Id, Map<String, String>> getSICAnswers(List<Id> QuoteOrRiskIds, List<SIC_Question__c> sqs)
    {
        // The map from sObject Id to another map.
        // The internal map maps from the SIC Question question code to the SIC Answer.
        Map<Id, Map<String, String>> idsqsamap = new Map<Id, Map<String, String>>();
        
        // Retrieve all SIC answers for a list of sObjects and SIC Questions.
    	Map<Id, Map<SIC_Question__c, SIC_Answer__c>> mapSQSA = SicQuestionService.getSicAnswers(QuoteOrRiskIds, sqs);
        
        // Convert the map to use the questionID to lookup the SIC answer.
        for (Id sid : mapSQSA.keySet())
        {
            Map<SIC_Question__c, SIC_Answer__c> sqsa = mapSQSA.get(sid);
            Map<String, String> sqam = new Map<String, String>();
            
            for (SIC_Question__c sq	: sqsa.keySet())
            {
                SIC_Answer__c sa = sqsa.get(sq);
                sqam.put(String.valueof(sq.questionId__c), sa.value__c != null ? sa.value__c : sa.Long_Value__c);
            }
            
            idsqsamap.put(sid, sqam);
        }
        
    	return idsqsamap;
    }
}