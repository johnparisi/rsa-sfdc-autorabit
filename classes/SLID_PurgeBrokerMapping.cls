/**
  * @author        Saumil Bapat
  * @date          11/3/2016
  * @description   Batch class to delete existing broker mapping
*/
global class SLID_PurgeBrokerMapping implements Database.Batchable<sObject>
{
	private final Boolean isManuallyTriggered;

   //Query of logs to delete
   global String query;

   //Type__c value for Broker records in the object SLID_Mapping_KeyValue__c
   //This 'type' would map the AGENT__c (scrambled) to the account Id
   public Static Final String brokerTypeValue = 'Broker';
   
   //Type__c value for Broker records in the object SLID_Mapping_KeyValue__c
   //This 'type' would map the BROKER_NUMBER__c field (unscrambled) to the account Id
   public Static Final String brokerTypeUnscrambled = 'Broker_US';
   
   public List<String> brokerTypes;

   //Retrieve the broker mapping setting 
   public static SLID_Broker_Mapping_Settings__c brokerMappingSettings = SLID_Broker_Mapping_Settings__c.getInstance();
	
   global SLID_PurgeBrokerMapping()
   {
       new SLID_PurgeBrokerMapping(true);
   }

   //Constructor to instantiate the query
   global SLID_PurgeBrokerMapping(Boolean manuallyTriggered)
   {
   	  this.isManuallyTriggered = manuallyTriggered;
   	  
   	  brokerTypes = new List<String>();
   	  brokerTypes.add(brokerTypeValue);
   	  brokerTypes.add(brokerTypeUnscrambled);
      //Exception_Logging__c exceptionParams = Exception_Logging__c.getInstance();
      this.query = 'Select Id from SLID_Mapping_KeyValue__c where Type__c IN :brokerTypes';

   }

   //Start method for the batch
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   //Execute method for the batch
   global void execute(Database.BatchableContext BC,List<SObject> scope)
   {
      try
      {
        //Throw a exception if testing (to test catch statements)
        SLID_ESB_ServiceConstants.throwTestingException('execute');
          
        if(!scope.isEmpty())
        {
          delete scope;

          if(SLID_PurgeBrokerMapping.brokerMappingSettings.Hard_Delete_Records__c)
          {
            Database.EmptyRecycleBin(scope);
          }
        }   
      }
      catch (Exception e)
      {
        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_PurgeBrokerMapping','execute','',UTIL_Logging.DEBUG_LEVEL_ERROR);
        UTIL_Logging.logException(log);
        throw e;
      }
   }

   //Finish method for the batch
   global void finish(Database.BatchableContext BC)
   {
      AsyncApexJob job = [Select Id , Status , NumberOfErrors
                          From AsyncApexJob
                          Where Id =: bc.getJobId()];

      if(job != null && (job.NumberOfErrors == 0 || job.NumberOfErrors == null))
      {
          //Initiate a create broker mapping batch job
          System.Debug('~~~Broker Mapping Purge Complete');
          SLID_CreateBrokerMapping createBrokerMappingBatch = new SLID_CreateBrokerMapping(isManuallyTriggered);
          Id createBrokerMappingBatchId = Database.ExecuteBatch(createBrokerMappingBatch);
          system.debug('~~~createBrokerMappingBatchId: ' + createBrokerMappingBatchId);
      }
      else
      {
        //Check if the admin email has been defined
        String adminEmail = SLID_PurgeBrokerMapping.brokerMappingSettings.Batch_Job_Admin_Email__c;
        if (adminEmail != null && adminEmail != '')
        {
          NotificationEmailForErrors(job.NumberOfErrors);
        }
      }
   }

   public void NotificationEmailForErrors(Integer noOfErrors)
   {
      String emailBody = 'Batch Process to Update Broker Mapping Data is completed' + '<br/>';
      emailBody += 'The batch job encountered ' + noOfErrors + ' errors.';

        //Reserve a email capacity
      Messaging.reserveSingleEmailCapacity(1);
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {SLID_PurgeBrokerMapping.brokerMappingSettings.Batch_Job_Admin_Email__c};
      mail.setToAddresses(toAddresses);
      mail.setSenderDisplayName('Salesforce Batch Apex');
      mail.setSubject('Broker Mapping Data Complete');
      mail.setBccSender(false);
      mail.setUseSignature(false);
      mail.setHTMLBody(emailBody);
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }
}