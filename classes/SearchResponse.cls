global with sharing class SearchResponse {
  	public QuoteRequestWrapper quoteRequestWrapper;
    public Boolean notFound;
    public Boolean inaccessibleInEPolicy;
    public Boolean locked;
    public Boolean sessioninValid;
    public Boolean esbErrorFlag;
    public String esbErrorMessage;
}