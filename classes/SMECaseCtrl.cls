/**
 * SMECaseCtrl
 * Author: Sara Al Mouaswas
 * Date: October 6 2016
 * 
 * The purpose of this controller is to capture button press actions on a visualforce page to send 
 * a getPolicy request to ePolicy via the ESB asynchronously using the Continuation Pattern: 
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_continuation_overview.htm
 */
global class SMECaseCtrl
{
    
    public static final Integer CONTINUATION_TIMEOUT = 120;
    public static final Integer CONTINUATION_PARALLEL_CALLOUT_LIMIT = 3;
    
    private static final String SME_RECORD_TYPE = 'CI Open SPRNT';
    private static final String SME_INDUSTRY_SEGMENT = 'SME-EP';    
    private ApexPages.StandardController controller;
    
    // Tracks the request label returned by the continuation server with the request body string.
    private Map<String, String> requestMap;
    private Map<String, RequestInfo> ris;
    
    public Id caseId { get; set; }
    public transient Case newCase {get;set;}
    public User agencyId;
    
    private RSA_ESBService service;
    
    // Continuation object to facilitate asynchronous callout to ePolicy.
    // Marked as transient as it does not need to be serialized and used as part of the response parsing.
    private transient Continuation con;
    
    public SMECaseCtrl(ApexPages.StandardController controller)
    {
        this.controller = controller;
        
        newCase = (Case) controller.getRecord();
        caseId = newCase.Id;
        
        newCase = [
            SELECT id, COM__c, Monoline__c, Restriction_limit__c, bkrCase_Subm_Type__c, Offering_Project__c
            FROM Case
            WHERE id = :caseId
            LIMIT 1
        ][0];
        
        agencyId = [SELECT Id, FederationIdentifier, Language__c
                                FROM User 
                                WHERE Id = : UserInfo.getUserId()][0];
        this.requestMap = new Map<String, String>();
        this.ris = new Map<String, RequestInfo>();
        
        // Continuation object to facilitate asynchronous callout to ePolicy.
        this.con = new Continuation(CONTINUATION_TIMEOUT);
        this.con.continuationMethod = 'processResponse';
    }
    
    //calling continuation method to call ESB service 
    public Object getPolicy(){

        Case thisCase = [SELECT id, COM__c, bkrCase_Subm_Type__c, recordType.name, recordType.DeveloperName FROM Case WHERE id = :this.caseId LIMIT 1][0];
        if ( thisCase.recordType.name == SME_RECORD_TYPE    // ?--> CONTAINS(RecordType.DeveloperName, 'SPRNT')
                && (thisCase.bkrCase_Subm_Type__c == 'Simple Amendment' || thisCase.bkrCase_Subm_Type__c == 'Complex Amendment' || thisCase.bkrCase_Subm_Type__c == 'Renewal' || thisCase.bkrCase_Subm_Type__c == 'Renewal Re-visit' )){
            if( thisCase.COM__c == null ){
                throw new RSA_ESBException(Label.COM_Required);
            }
        }
    
        RequestInfo ri =  new RequestInfo(
	            RequestInfo.ServiceType.GET_POLICY
	        );
        addContinuationRequest(ri);
        return con;
    }
    
    /*
     * Processes all the RA responses to the requests made to the continuation server.
     */
    public PageReference processResponse()
    {
        System.debug('!!! ' + requestMap.keySet());
        for (String requestLabel : requestMap.keySet())
        {
            parseRAResponse(requestLabel);
        }
        
        PageReference pageRef = new PageReference('/' + caseId);
        pageRef.setRedirect(true);
        return pageRef;//Returns to the quote page
    }
    
        
    /*
     * Takes the RequestInfo object to produce the request and adds it to the continuation server.
     */
    private void addContinuationRequest(RequestInfo ri)
    {
        this.service = new RSA_ESBService(ri, newCase.COM__c, newCase.Offering_Project__c, agencyId.FederationIdentifier);
        String request = service.getRequestXML();
        //system.assert(false, ri);
        HttpRequest req = service.getHttpRequest(request);
        String requestLabel = this.con.addHttpRequest(req);
        requestMap.put(requestLabel, request);
        ris.put(requestLabel, ri);
    }
    
    /*
     * This helper method extracts the response body and sends it to be processed for RA transactions which use the CSIOPolicyXMLProcessor.
     */
    private void parseRAResponse(String requestLabel)
    {
        HttpResponse response = Continuation.getResponse(requestLabel);
        //delete old session ids for users 
        String currentSession = RA_SessionManagementUtil.getSession(newCase.Id);
        if (currentSession != null){    
             RA_SessionManagementUtil.removeSession(newCase.Id);
        }
        
        system.debug('response' + response.getBody());
        system.debug('request' + requestMap.get(requestLabel));
        new CSIOPolicyErrorHandling(response.getBody()).process();
        new CSIOPolicyXMLProcessor(newCase, response.getBody()).process();
        
        //The policy record will be deleted and a new record will be generated and populated, response and request need to be stored in the new record 
        List <Quote__c> policyList = [SELECT Id, Case__r.Locked_in_External_Policy_System__c, Last_Federation_Id__c, Case__r.ePolicy_Transaction_Status__c
                                      FROM Quote__c where Case__c = :caseId];
        
        if (policyList.size() != 0){
            RSA_ESBService.insertNote(policyList[0].Id, 'Generated GETPOLICY Response', response.getBody());
            RSA_ESBService.insertNote(policyList[0].Id, 'Generated GETPOLICY Request', requestMap.get(requestLabel));
            //User should not be able to modify the policy if it is locked in ePolicy - meaning someone else is currently working on the policy 
            if (policyList[0].case__r.Locked_in_External_Policy_System__c == true){
                newCase.external_Policy_System_Error_Message__c = 'Policy Locked in ePolicy - FR'; 
                //TBD stop UW from starting transaction 
            }
            if (policyList[0].case__r.ePolicy_Transaction_Status__c  == 'Pending'){
                newCase.Resume_Transaction__c = true;
            }
            newCase.external_Policy_System_Error_Message__c = '';
        }
        else {
            newCase.external_policy_system_error_message__c = 'Policy Not Found - FR';
        } 
        update newCase;
    }
}