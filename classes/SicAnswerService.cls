/*
* Sic_Answer__c Service Layer
* Contains processing service methods related to the Sic_Answer__c Object
*/
public class SicAnswerService
{
    /**
     * setCanadianValueFromAccountRevenue
     * @param records: SIC_Answer__c records that were inserted
     * 
     * Retrieve the insured client's total revenue value and set as value if this is the Canadian Revenue SIC Answer.
     */
    public void setCanadianValueFromAccountRevenue(List<Sic_Answer__c> records)
    {
        Map<Sic_Answer__c, Id> sas2qid = new Map<Sic_Answer__c, Id>();
        
        for (Sic_Answer__c sa : records)
        {
            if (sa.QuestionId__c == Decimal.valueOf(SicQuestionService.CODE_CANADIAN_REVENUE) &&
                sa.Quote__c != null &&
                sa.Value__c == null) // This value should not be overwritten if provided by the user.
            {
                sas2qid.put(sa, sa.Quote__c);
            }
        }
        
        if (!sas2qid.isEmpty())
        {
            // Get all insured account ids related to all SIC Answer's quote's case.
            Map<Id, Id> qid2aid = new Map<Id, Id>();
            for (Quote__c q : [
                SELECT id, case__r.bkrCase_Insured_Client__c
                FROM Quote__c
                WHERE id IN :sas2qid.values()
                AND case__r.bkrCase_Insured_Client__c != null
            ])
            {
                qid2aid.put(q.Id, q.case__r.bkrCase_Insured_Client__c);
            }
            
            Map<Id, Account> accts = new Map<Id, Account>([
                SELECT id, annualRevenue
                FROM Account
                WHERE id IN :qid2aid.values()
            ]);
            
            // Map the SIC Answer to its related insured account record.
            Map<Id, Account> sa2acct = new Map<Id, Account>();
            for (Id qid : qid2aid.keySet())
            {
                sa2acct.put(qid, accts.get(qid2aid.get(qid)));
            }
            
            for (Sic_Answer__c sa : records)
            {
                // Update the value if the insured account has an annual revenue.
                if (sa.QuestionId__c == Decimal.valueOf(SicQuestionService.CODE_CANADIAN_REVENUE) &&
                    sa2acct.get(sa.Quote__c) != null && 
                    sa2acct.get(sa.Quote__c).annualRevenue != null)
                {
                    sa.Value__c = String.valueOf(Integer.valueOf(sa2acct.get(sa.Quote__c).annualRevenue)); // Ignore the decimal place of the annual revenue.
                }
            }
        }
    }
    
    /**
     * updateDefaultCoverageLimits
     * @param   records     Sic_Answer__c that were inserted/updated
     * @param   oldRecords  Previous state of the Sic Answer that were inserted/updated mapped by id
     * 
     * Update default coverage limits based on parent risk value.
     */
    public void updateDefaultCoverageLimits(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
    {
        // List of all SIC Answers related to risks.
        List<Id> raIds = new List<Id>();
        
        Map<Id, List<Sic_Answer__c>> risk2SA = new Map<Id, List<Sic_Answer__c>>();
        
        // Filter for the Sic Answers related to risks.
        for (Sic_Answer__c sa : records)
        {
            if (sa.Risk__c != null)
            {
                raIds.add(sa.Id);
                risk2SA.put(sa.Risk__c, new List<Sic_Answer__c>());
            }
        }
        
        // There is an assumption that SIC Answers for stock and equipment have been inserted or updated in pairs.
        List<Sic_Answer__c> updatedSas = [
            SELECT id, Risk__c, Value__c, Question_Code__r.QuestionId__c
            FROM Sic_Answer__c
            WHERE ID IN :raIds
        ];
        
        // If the sa has a risk parent, map the sic answer under the risk.
        for (Sic_Answer__c sa : updatedSas)
        {
            if (sa.Risk__c != null)
            {
                List<Sic_Answer__c> sas = risk2SA.get(sa.Risk__c);
                sas.add(sa);
                risk2SA.put(sa.Risk__c, sas);
            }
        }
        
        if (!risk2SA.isEmpty())
        {
            // Retrieve all coverages for this risk.
            List<Coverages__c> cs = [
                SELECT id, Limit_Value__c, type__c, Risk__c
                FROM Coverages__c
                WHERE risk__c IN :risk2SA.keySet()
            ];
            
            Map<String, String> dynamicQuestionCoverageType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionCoverage');
            
            // If the SIC answer matches the coverage, then update the coverage limit.
            for (Coverages__c c : cs)
            {
                for (Sic_Answer__c sa : risk2SA.get(c.Risk__c))
                {
                    if (c.Type__c == dynamicQuestionCoverageType.get(String.valueOf(sa.Question_Code__r.QuestionId__c)))
                    {
                        c.Limit_Value__c = getIntValue(sa);
                    }
                }
            }
            
            update cs;
        }
    }
    
    /**
     * updateTotalRevenue
     * @param   records     Sic_Answer__c that were inserted/updated
     * @param   oldRecords  Previous state of the Sic Answer that were inserted/updated mapped by id
     * 
     * Update total revenue based on dynamic quote currency fields.
     */
    public void updateTotalRevenue(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
    {
        List<Sic_Answer__c> sas = new List<Sic_Answer__c>();
        
        // Filter for the Sic Answers related to quotes.
        for (Sic_Answer__c sa : records)
        {
            if (sa.Quote__c != null)
            {
                sas.add(sa);
            }
        }
        
        Map<Id, Sic_Answer__c> updatedValue = new Map<Id, Sic_Answer__c>();
        
        // Filter for the Sic Answers that have an update value.
        if (oldRecords == null)
        {
            for (Sic_Answer__c sa : sas)
            {
                updatedValue.put(sa.Quote__c, sa);
            }
        }
        else
        {
            for (Sic_Answer__c sa : (List<Sic_Answer__c>) Utils.getChangedObjects(sas, oldRecords, new List<String> {'Value__c'}))
            {
                updatedValue.put(sa.Quote__c, sa);
            }
        }
        
        List<Sic_Answer__c> updatedSas = [
            SELECT id, Quote__c, Value__c, Question_Code__r.QuestionId__c
            FROM Sic_Answer__c
            WHERE Quote__c IN :updatedValue.keySet()
        ];
        
        // A set of all quotes that were updated.
        Set<Id> quotes = new Set<Id>();
        // Store the quote to total revenue value.
        Map<Id, Integer> totalRevenue = new Map<Id, Integer>();
        Map<String, String> dynamicQuestionRevenueType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionRevenue');
        
        // Iterate over the list of SIC answers, sum up the values if they are revenue questions.
        for (Sic_Answer__c sa : updatedSas)
        {
            if (dynamicQuestionRevenueType.containsKey(String.valueOf(sa.Question_Code__r.QuestionId__c)))
            {
                Integer total = totalRevenue.get(sa.Quote__c);
                if (total == null)
                {
                    total = 0;
                }
                
                total += getIntValue(sa);
                totalRevenue.put(sa.Quote__c, total);
                quotes.add(sa.Quote__c);
            }
        }
        
        if (!quotes.isEmpty())
        {
            // Retrieve all quotes for this risk.
            List<Quote__c> qs = [
                SELECT id, Total_Revenue__c
                FROM Quote__c
                WHERE id IN :quotes
            ];
            
            // Update the total revenue for the quote.
            for (Quote__c q : qs)
            {
                q.Total_Revenue__c = totalRevenue.get(q.Id);
            }
            
            update qs;
        }
    }
    
    /**
     * updateNumberOfUnits
     * @param   records     Sic_Answer__c that were inserted/updated
     * @param   oldRecords  Previous state of the Sic Answer that were inserted/updated mapped by id
     * 
     * Update number of units based on dynamic answer value.
     */
    public void updateNumberOfUnits(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
    {
        List<Sic_Answer__c> sas = new List<Sic_Answer__c>();
        
        // Filter for the Sic Answers related to risks.
        for (Sic_Answer__c sa : records)
        {
            if (sa.Risk__c != null)
            {
                sas.add(sa);
            }
        }
        
        Map<Id, Sic_Answer__c> updatedValue = new Map<Id, Sic_Answer__c>();
        
        // Filter for the Sic Answers that have an update value.
        if (oldRecords == null)
        {
            for (Sic_Answer__c sa : sas)
            {
                updatedValue.put(sa.Risk__c, sa);
            }
        }
        else
        {
            for (Sic_Answer__c sa : (List<Sic_Answer__c>) Utils.getChangedObjects(sas, oldRecords, new List<String> {'Value__c'}))
            {
                updatedValue.put(sa.Risk__c, sa);
            }
        }
        
        List<Sic_Answer__c> updatedSas = [
            SELECT id, Risk__c, Value__c, Question_Code__r.QuestionId__c
            FROM Sic_Answer__c
            WHERE Risk__c IN :updatedValue.keySet()
        ];
        
        // A set of all risks that were updated.
        Set<Id> risks = new Set<Id>();
        // Store the risk to number of units value.
        Map<Id, Integer> numUnits = new Map<Id, Integer>();
        Map<String, String> dynamicQuestionRevenueType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionRevenue');
        
        // Iterate over the list of SIC answers, sum up the values if they are revenue questions.
        for (Sic_Answer__c sa : updatedSas)
        {
            if (String.valueOf(sa.Question_Code__r.QuestionId__c) == SicQuestionService.CODE_NUMBER_OF_UNITS)
            {
                numUnits.put(sa.Risk__c, getIntValue(sa));
                risks.add(sa.Risk__c);
            }
        }
        
        if (!risks.isEmpty())
        {
            // Retrieve all risks for the updated units.
            List<Risk__c> rs = [
                SELECT id, Number_Of_Units__c
                FROM Risk__c
                WHERE id IN :risks
            ];
            
            // Update the number of units for the risk.
            for (Risk__c r : rs)
            {
                r.Number_Of_Units__c = numUnits.get(r.Id);
            }
            
            update rs;
        }
    }
    
    
    /**
     * Helper to retrieve Sic Answer record's numeric value.
     * 
     * @param sa: A Sic Answer record.
     * @return: The integer value of the Sic Answer record.
     */
    public static Integer getIntValue(Sic_Answer__c sa)
    {
        if (sa == null || sa.Value__c == null)
        {
            return 0; // Use 0 as default value.
        }
        
        return Integer.valueOf(sa.Value__c);
    }
    
    /**
     * Helper to retrieve Sic Answer record's string value.
     * 
     * @param sa: A Sic Answer record.
     * @return: The string value of the Sic Answer record.
     */
    public static String getStringValue(Sic_Answer__c sa)
    {
        if (sa == null || sa.Value__c == null)
        {
            return ''; // Use empty string as default value.
        }
        
        return String.valueOf(sa.Value__c);
    }
}