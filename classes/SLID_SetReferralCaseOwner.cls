/**
  * @author        Saumil Bapat
  * @date          12/6/2016
  * @description   Helper class to populate owner on referral cases
*/
public with sharing class SLID_SetReferralCaseOwner {
  //Name of the case referral record type
  private Static Final String REFERRAL_RECORD_TYPE_NAME = 'CI Referral';

  //Type of mapping
  private Static Final String MAPPING_TYPE = 'Owner';

  public static void SetReferralCaseOwner(List<Case> newRecords)
  {
    System.Debug('~~~SLID_SetReferralCaseOwner');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_SetReferralCaseOwner: ' + Limits.getQueries());
    //If the integration log id is not set, it is not running in a webservice context
    if (Util_Logging.integrationLogId == null)
    {
        System.Debug('~~~Integration Id is null');
        System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_SetReferralCaseOwner: ' + Limits.getQueries());
        return;
    }
    //Get the 'CI Referral' RecordTypeId
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id referralRecordTypeId = caseRecordTypes.get(REFERRAL_RECORD_TYPE_NAME).getRecordTypeId();

    //List of cases with lanId populated
    List<Case> casesWithLanIds = new List<Case>();
    List<String> LanIds = new List<String>();

    //List of cases without lanId populated
    List<Case> casesWithoutLanIds = new List<Case>();
    List<String> keyValueFilters = new List<String>();

    for (Case newCase : newRecords)
    {
      System.Debug('~~~newCase: ' + newCase);
      
      if(newCase.RecordTypeId == referralRecordTypeId)
      {
        //Check if the case has a lan Id Populated
        if(newCase.LanId__c != null)
        {
          casesWithLanIds.add(newCase);
          LanIds.add(newCase.LanId__c);
        }
        else
        {
          casesWithoutLanIds.add(newCase);
          String shortRecordTypeId;
          String shortProductId;
          if(newCase.RecordTypeId != null) 
          {
              shortRecordTypeId = getShortId(newCase.RecordTypeId);
          }
          if(newCase.productId != null) 
          {
              shortProductId = getShortId(newCase.ProductId);
          }
          System.debug('~~~KeyValueFilters: ' + newCase.bkrCase_Region__c + ',' + shortRecordTypeId + ',' + shortProductId + ',' + newCase.Level__c  +'_' + MAPPING_TYPE);
          keyValueFilters.add(newCase.bkrCase_Region__c + ',' + shortRecordTypeId + ',' + shortProductId + ',' + newCase.Level__c  +'_' + MAPPING_TYPE);
        }
      }
    }

    //Instantiate the user map based on LanId
    Map<String,User> userAliasMap = new Map<String,User>();
    try
    {
      for (User u : [select alias, id from user where alias IN :LanIds])
      {
        userAliasMap.put(u.alias, u);
      }
    }
    catch (Exception e)
    {
      //Create a exception record
      UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_SetReferralCaseOwner','SetReferralCaseOwner','',UTIL_Logging.DEBUG_LEVEL_ERROR);
      UTIL_Logging.logException(log);
    }

    //Populate the ownerIDs based on LanId
    for (Case newCase : casesWithLanIds)
    {
      User u = userAliasMap.get(newCase.LanId__c);
      if (u != null)
      {
        newCase.OwnerId = u.Id;
      }
    }

    //Instantiate the mapping for user based on case region / product
    SLID_ESB_MappingUtils.instantiateWithFilterValues(keyValueFilters);

    //Populate the OwnerIds based on region / product
    for (Case newCase : casesWithoutLanIds)
    { 
      System.Debug('~~~newCaseWithoutLanId: ' + newCase);
      System.Debug('~~~Mapping Key: ' + newCase.bkrCase_Region__c + ',' + getShortId(newCase.RecordTypeId) + ',' + getShortId(newCase.ProductId) + ',' + newCase.Level__c);
      Id userId = SLID_ESB_MappingUtils.getMappedValue(newCase.bkrCase_Region__c + ',' + getShortId(newCase.RecordTypeId) + ',' + getShortId(newCase.ProductId) + ',' + newCase.Level__c, MAPPING_TYPE);
      if (userId != null)
      {
        newCase.OwnerId = userId;
      }
    }
    System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_SetReferralCaseOwner: ' + Limits.getQueries());
  }
  
  private static String getShortId (String IdVal)
  {
      System.Debug('~~~getShortId IdVal: ' + IdVal);
      if(IdVal == '' || IdVal == null)
      {
          return '';
      }
      
      String recordId = String.ValueOf(IdVal);
      
      if (recordId.length() <= 15)
      {
          return recordId;
      }
      else
      {
          String shortRecordId = recordId.SubString(0,14);
          return shortRecordId;
      }
  }
}