@RestResource(urlMapping='/searchCustomSettings/*')
global with sharing class SMEQ_CustomSettingSearchRestService
{
    private static final String LIVE_CHAT_FIELD_TYPE = 'livechat';
    
    @HttpGet
    global static SMEQ_CustomSettingResponseModel searchCustomSettingsByType() {        
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        SMEQ_CustomSettingResponseModel cSettingResponseModel = new SMEQ_CustomSettingResponseModel();
        SMEQ_CustomSettingDao cSettingDao = new SMEQ_CustomSettingDao();
        String settingFieldType = '';  
        String settingVariant = '';      
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');        
        if(req.params.get('type') != null){ 
            settingFieldType = EncodingUtil.urlDecode(req.params.get('type'),'UTF-8'); 
        }

        if(req.params.get('variant')!=null){
            settingVariant= EncodingUtil.urlDecode(req.params.get('variant'),'UTF-8');
        }

        //search by both variant and and field type
        if(settingFieldType!='' && settingVariant!='')
        {
            try{
                

                cSettingResponseModel = cSettingDao.getCustomSettingsByTypeAndVariant(settingFieldType,settingVariant);
            }
            catch(Exception se){
                cSettingResponseModel.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR); 
                List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
                SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');errors.add(error);cSettingResponseModel.setErrors(errors);
    		}
        }

        //search by field type only
        else if(settingFieldType!='' && settingVariant=='')
        {
            try{
                if(settingFieldType == LIVE_CHAT_FIELD_TYPE){
                     cSettingResponseModel = cSettingDao.getCustomSettingsByTypeAndVariant(settingFieldType,settingVariant);
                }
                else{
                    cSettingResponseModel = cSettingDao.getCustomSettingsByType(settingFieldType);
                }
            }
            catch(Exception se){
                cSettingResponseModel.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR); 
                List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
                SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');errors.add(error);cSettingResponseModel.setErrors(errors);
            }
        }
        
        //field type or variant parameters have not been specified
        else
        {
             cSettingResponseModel.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR); 
             List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
             SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(
                                                            SMEQ_RESTResponseModel.ERROR_CODE_699, 
                                                            'Undefined type parameter: Unexpected System Exception');errors.add(error);
                                                            cSettingResponseModel.setErrors(errors);
        }
		return cSettingResponseModel;
    }

}