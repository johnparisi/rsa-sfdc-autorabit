/**
 * SMEQ_UWRiskEdit
 * Author: James Lee
 * Date: December 30 2016
 * Feature: FP-748
 * 
 * The purpose of this extension class is to provide support for displaying and saving values for fields in a quote record.
 */
public with sharing class SMEQ_UWRiskEdit
{
    public Quote__c quote {get;set;}
    public Risk__c risk {get;set;}
    
    public List<SIC_Question__c> questions {get;set;}
    public Map<SIC_Question__c, SIC_Answer__c> fields {get;set;}
    public Map<SIC_Question__c, List<SelectOption>> picklistOptions {get;set;}
    public Map<SIC_Question__c, Integer> picklistSize {get;set;}
    public Map<SIC_Question__c, List<String>> multipicklistValues {get;set;}
    public String errors {get;set;}
    
    public RecordType rt {get;set;}
    
    public Boolean hideEdit {get;set;}
    public Boolean showRAFields {get;set;}
    public Boolean showRentalRevenue {get;set;}
    public Boolean buildingRequired {get;set;}
    
    /**
     * Constructor for risk edit.
     */
    public SMEQ_UWRiskEdit(apexPages.standardController con)
    {
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        
    	this.risk = (Risk__c) con.getRecord();
        
        this.quote = [
            SELECT id, name,
            case__r.Offering_Project__c, case__r.bkrCase_Region__c, RecordTypeId, case__r.bkrCase_SIC_Code__r.Sic_Code__c
            FROM Quote__c
            WHERE id = :this.risk.Quote__c
        ];
        
        this.rt = [
            SELECT id, Name
            FROM RecordType
            WHERE Id = :this.quote.RecordTypeId
        ];
        if (this.rt.Id == QuoteTypes.get(QuoteHandler.RECORDTYPE_POLICY) || this.rt.Id == QuoteTypes.get(QuoteHandler.RECORDTYPE_POLICY_ClOSED))
        {
            this.showRAFields = true;
        }
        
        if (this.risk.RecordTypeId != RiskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION))
        {
            this.hideEdit = true;
        }
        initialize();
    }
    
    /**
     * Constructor for risk insert.
     */
    public SMEQ_UWRiskEdit(ApexPages.StandardSetController setCon)
    {
        Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        
        this.risk = new Risk__c();
        this.risk.Quote__c = ApexPages.currentPage().getParameters().get('id');
        this.risk.RecordTypeId = RiskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION);
        
        this.quote = [
            SELECT id, name,
            case__r.Offering_Project__c, case__r.bkrCase_Region__c, case__r.bkrCase_SIC_Code__r.Sic_Code__c
            FROM Quote__c
            WHERE id = :this.risk.Quote__c
        ];
        
        this.rt = [
            SELECT id, Name
            FROM RecordType
            WHERE Id = :this.risk.RecordTypeId
        ];
        
        initialize();
    }
    
    private void initialize()
    {
        SIC_Code_Detail_Version__c scdv = retrieveQuoteSICCodeDetailVersion(this.quote);
        if(scdv != NULL){
            this.questions = SicQuestionService.getAllApplicationQuestions(scdv, SicQuestionService.CATEGORY_RISK);
        }
        else{
            // The else part is added when there is no associated SIC Code to the case. This is part of story PWP-398
            this.questions = getSICQuestionsWithoutSICCode(risk);
        }
        this.fields = initializeAnswerMapping(questions);
        this.picklistOptions = generatePicklistOptions(questions);

        if(scdv != NULL){
            // The if check is added to avoid code exception when there is no associated SIC Code to the case.
            // This is part of story PWP-398
            this.showRentalRevenue = scdv.Coverage_Defaults__c.contains(RiskService.COVERAGE_RENTAL);
            this.buildingRequired = scdv.Building_Required__c;
        }
    }
    
    /**
     * FP-748
     * 
     * Retrieve a risk's quote's case's SIC Code Detail Version using the SMEQ_SicCodeService.
     * 
     * @param q: The quote to retrieve it's case's SIC Code Detail Version record.
     * @return: The referenced SIC Code Detail Version record.
     */
    private SIC_Code_Detail_Version__c retrieveQuoteSICCodeDetailVersion(Quote__c q)
    {
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicDetailsBySicProjectRegion(
            q.case__r.Offering_Project__c, 
            q.case__r.bkrCase_Region__c, 
            q.case__r.bkrCase_SIC_Code__r.Sic_Code__c
        );
        
        if (scdvs.size() > 0)
        {
        	return scdvs[0];
        }
        else
        {
            return null;
        }
    }
    
    /**
     * FP-748
     * 
     * Initialize missing answers for a given list of questions.
     * Questions mapped to null answers will be remapped to a default answer.
     * 
     * @param sqs: The list of SIC Code questions.
     * @return: The map from SIC Questions to SIC Answers
     */
    private Map<SIC_Question__c, SIC_Answer__c> initializeAnswerMapping(List<SIC_Question__c> sqs)
    {
        if (sqs == null || sqs.isEmpty())
        {
            return new Map<SIC_Question__c, SIC_Answer__c>();
        }
        
        Map<SIC_Question__c, SIC_Answer__c> fields = SICQuestionService.getSicAnswers(risk.Id, sqs);
        this.multipicklistValues = new Map<SIC_Question__c, List<String>>();
        
        for (SIC_Question__c sq : fields.keySet())
        {
            // Fix this - multiselect issue.
            if (sq.Input_Type__c == SICQuestionService.INPUT_TYPE_MULTI_SELECT_DROPDOWN)
            {
                SIC_Answer__c sa = fields.get(sq);
                
                if (sa.Long_Value__c != null && 
                    sa.Long_Value__c != '' && 
                    sa.Long_Value__c != SICQuestionService.EMPTY_MULTIPICKLIST_VALUE)
                {
                    this.multipicklistValues.put(sq, sa.Long_Value__c.split(';'));
                }
            }
        }
        
        return fields;
    }
    
    /**
     * FP-748
     * 
     * Generates a map for a list of questions for their available picklist options.
     * 
     * @param sqs: The list of SIC Code questions.
     * @return: The map from SIC Questions to lists of picklist options.
     */
    private Map<SIC_Question__c, List<SelectOption>> generatePicklistOptions(List<SIC_Question__c> sqs)
    {
        Map<SIC_Question__c, List<SelectOption>> picklistOptions = new Map<SIC_Question__c, List<SelectOption>>();
        Map<SIC_Question__c, Integer> picklistSize = new Map<SIC_Question__c, Integer>();
        
        for (SIC_Question__c sq : sqs)
        {
            if (sq.AvailableValuesEn__c != null)
            {
                List<SelectOption> sos = new List<SelectOption>();
                if (!sq.IsRequired__c) // Required value questions will not have a blank value as a choice.
                {
                    sos.add(new SelectOption('', '---')); // The initial base value.
                }
                
                for (String s : sq.AvailableValuesEn__c.split(';'))
                {
                    SelectOption so = new SelectOption(s, s);
                    sos.add(so);
                }
                
                picklistOptions.put(sq, sos);
                picklistSize.put(sq, sos.size());
            }
        }
        
        return picklistOptions;
    }
    
    /**
     * This helper function converts the default saved values from VFP 
     * and stores it in the more standard semi-comma delimited string.
     */
    private void updateMultiPicklistValues()
    {
        for (SIC_Question__c sq : fields.keySet())
        {
            if (sq.Input_Type__c == 'Multi Select Dropdown')
            {
                SIC_Answer__c sa = fields.get(sq);
                sa.Long_Value__c = String.join(sa.Long_Value__c.substringBetween('[',']').split(', '), ';');
            }
        }
    }
    
    /**
     * FP-748
     * 
     * Validates and saves the quotes values and answers for dynamic questions.
     */
    public PageReference saveRisk()
    {
        updateMultiPicklistValues();
        ResultSet rs = SICQuestionService.validateSicAnswers(fields.values());
        
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        
        this.errors = '';
        
        if (rs.getErrorCount() > 0 &&
            (risk.Quote__r.recordTypeId != QuoteTypes.get(QuoteHandler.RECORDTYPE_POLICY) && 
             risk.Quote__r.recordTypeId != QuoteTypes.get(QuoteHandler.RECORDTYPE_POLICY_ClOSED)))
        {
            Map<Id, String> errMap = new Map<Id, String>();
            
            Map<SIC_Answer__c, SIC_Question__c> sas2sqs = new Map<SIC_Answer__c, SIC_Question__c>();
            for (SIC_Question__c sq : fields.keySet())
            {
                sas2sqs.put(fields.get(sq), sq);
            }
            
            for (ProcessingResult pr : rs.getResults())
            {
                String key = pr.getKey();
                for (SIC_Answer__c sa : fields.values())
                {
                    if (key == SicQuestionService.getAnswerRecordKey(sa))
                    {
                        String errMsg = '';
                        for (ProcessingError pe : pr.getErrors())
                        {
                            if (errMsg != '')
                            {
                                errMsg += ', ';
                            }
                            errMsg += pe.getError();
                        }
                        errMap.put(sas2sqs.get(sa).Id, errMsg);
                    }
                }
            }
            this.errors = (String)JSON.serialize(errMap);
            
            return null;
        }
        
        upsert risk;
        System.debug(risk);
        
        for (SIC_Answer__c sa : fields.values())
        {
            if (sa.Risk__c == null)
            {
            	sa.Risk__c = risk.Id;
            }
        }
        
        SicQuestionService.saveSicAnswers(fields.values());
        
        PageReference redirectSuccess = new ApexPages.StandardController(risk).view();
        return redirectSuccess;
    }

    /*
    *   Name        : getSICQuestionsWithoutSICCode
    *   @description: This method would return the list of questions associated with the Risk when the case has
    *                 No SIC Code associated with it. This method is implemented only for Policy Works implementation
    *                 since cases coming from Policy Works has max probability of of not having any SIC Code associated
    *   @param      : Risk__c objRisk - The associated Risk Object
    *   @return     : List<SIC_Question__c> - The list of SIC Question records associated with the Quote
    */
    public List<SIC_Question__c> getSICQuestionsWithoutSICCode(Risk__c objRisk){
        List<SIC_Question__c> listSicQuestions = new List<SIC_Question__c>();
        Set<Id> setSicQuestionId = new Set<Id>();
        List<SIC_Answer__c> listSicAnswers = new List<SIC_Answer__c>([SELECT Question_Code__c FROM SIC_Answer__c WHERE Risk__c =: objRisk.Id]);

        for(SIC_Answer__c tmp: listSicAnswers)
            setSicQuestionId.add(tmp.Question_Code__c);

        listSicQuestions = [SELECT Id, QuestionId__c, AvailableValuesEn__c, AvailableValuesFr__c, Category__c, Sub_Category__c, Input_Type__c, IsRequired__c, QuestionText_en__c,
                            QuestionText_fr__c, Question_Text__c,Xpath__c, Resource_Bundle_Label__r.string_id__c, Resource_Bundle_Place_Holder_Key__r.string_id__c, RecordtypeId,
                            RecordType.DeveloperName, Question_Code__c, FrontEndVisibility__c,Order__c, Minimum_Validation_Amount__c, Minimum_Validation_Required__c
                            FROM SIC_Question__c
                            WHERE Id IN: setSicQuestionId];

        return listSicQuestions;
    }
}