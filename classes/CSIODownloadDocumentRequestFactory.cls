/**
 * CSIODownloadDocumentRequestFactory
 * Author: Sophie Tran
 * Date: November 24, 2017
 * 
 * The purpose of this class is to store all common header and footer XML to be used in the service 
 * specific factory classes for the services that use CSIO version 4_20
 */

public abstract class CSIODownloadDocumentRequestFactory extends CSIORequestAbstractFactory {
    protected String CSIO420Footer = ''; 
    protected String CSIO420Header = '';
    protected String language;
    protected String businessSource;
    protected String federationId;
    protected Map<String, String> transactionType;
    protected CSIO_Producer producer;
    protected CSIODownloadDocumentRequestFactory(String federationId, String businessSource){
        language = UserService.getFormattedUserLanguage();
        //generating a uniqueId to be sent to ESB for each  request
        this.uniqueId = EncodingUtil.convertToHex(Crypto.GenerateAESKey(128));
        this.federationId = federationId;
        this.businessSource = businessSource;
        transactionType = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('bkrCase_Subm_Type__c');
    }
    
    
    protected Void getHeaderXML(){
        //generate the Header 
            CSIO420Header += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.rsagroup.ca/1.0/policy/commercial">';
            CSIO420Header += ' <soapenv:Header/>';
            CSIO420Header += ' <soapenv:Body>';
            CSIO420Header += '  <ACORD xmlns="http://www.ACORD.org/standards/PC_Surety/ACORD1.7.0/xml/" xmlns:csio="http://www.CSIO.org/standards/PC_Surety/CSIO4.2.0/xml/">';
            CSIO420Header += '   <SignonRq>';
            CSIO420Header += '    <SignonTransport>';
            CSIO420Header += '     <CustId>';
            CSIO420Header += '      <SPName>' + SERVICE_PROVIDER_NAME + '</SPName>';
            CSIO420Header += '      <CustPermId>' + this.federationId + '</CustPermId>';
            CSIO420Header += '     </CustId>';
            CSIO420Header += '    </SignonTransport>';
            CSIO420Header += '    <ClientDt>' + todayDateLong + '</ClientDt>'; //Fix this - 2007-09-10T15:12:50-07:00         
            CSIO420Header += '    <CustLangPref>' + language + '</CustLangPref>';         
            CSIO420Header += '    <ClientApp>';
            CSIO420Header += '     <Org>' + UserInfo.getOrganizationId() + '</Org>'; //Fix this
            CSIO420Header += '     <Name>' + this.businessSource + '</Name>';
            CSIO420Header += '     <Version>1</Version>';
            CSIO420Header += '    </ClientApp>';
            CSIO420Header += '   </SignonRq>';
        
          // generate the footer
          
            CSIO420Footer += '  </ACORD>';
            CSIO420Footer += ' </soapenv:Body>';
            CSIO420Footer += '</soapenv:Envelope>';
      
    }
}