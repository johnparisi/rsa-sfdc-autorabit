@isTest
private class SMEQ_SICService_Test {
	@testSetup
	static void dataSetup(){		
		SIC_Code__c sicCode = new SIC_Code__c(name = 'test sic code');
		insert sicCode;
		
		SIC_Code_Detail__c sicCodeDetail = new SIC_Code_Detail__c(name = 'test sic code detail', SIC_Code__c = sicCode.Id,
		Short_Description_En__c = 'test en', Short_Description_Fr__c = 'test fr', Application_Name__c = 'SME');
		insert sicCodeDetail;
	}

    static testMethod void myUnitTest() {
    	SMEQ_SICService sicService = new SMEQ_SICService();
    	List<SMEQ_SICDataModel> sicDataModels = sicService.getAllSICCodes('en');
        
        SMEQ_SICDataModel sicDataModel = sicService.getSICDetail('1234','en');
    }
}