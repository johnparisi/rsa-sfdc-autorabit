public class CSIO_Producer
{
    private Account brokerage;
    private Contact producer;
    private User requestingUser;
    private String caseId;
    public CSIO_ItemIdInfo itemIdInfo;
    public CSIO_GeneralPartyInfo generalPartyInfo;
    public ProducerInfo producerInfo;
    public RequestInfo ri; 
    
    public CSIO_Producer(Account brokerage, Contact producer, User requestingUser,String caseId)
    {
        this.brokerage = brokerage;
        this.producer = producer;
        this.requestingUser = requestingUser;
        this.caseId = caseId;
        convert();
    }
    
    public CSIO_Producer(Account brokerage, Contact producer, User requestingUser, String caseId, RequestInfo ri)
    {
        this.brokerage = brokerage;
        this.producer = producer;
        this.requestingUser = requestingUser;
        this.caseId = caseId;
        this.ri = ri;
        convert();
    }
    private void convert()
    {
        // Based on SES-177, might have to check for the broker's federationID 
        // rather than use the current user's federationID, since it might also be an UW.
        String federationId;
        if (requestingUser != NULL)
            federationId = requestingUser.FederationIdentifier;
        this.itemIdInfo = new CSIO_ItemIdInfo(federationId); //Fixed?
        this.producerInfo = new ProducerInfo(this.caseId);
        this.generalPartyInfo = new CSIO_GeneralPartyInfo(this.brokerage);
    }
    
    public override String toString()
    {
        String xml = '';
        xml += '<Producer>';
        xml += this.itemIdInfo;
        if (ri == null || ri.svcType != RequestInfo.Servicetype.GET_POLICY){
            xml += this.generalPartyInfo;
        }
        xml += this.producerInfo;
        xml += '</Producer>';
        
        return xml;
    }
    
    public class ProducerInfo
    {
        private String caseId;
        
        public String producerRoleCd;
        public String contractNumber;
        
        public ProducerInfo(String caseId)
        {
            this.caseId = caseId;
            convert();
        }
        
        private void convert()
        {
            this.producerRoleCd = 'Broker';
            String brokerStageNumber = BrokerStageService.getStageBrokerNumberByCase(this.caseId);
            this.contractNumber = brokerStageNumber != null ? brokerStageNumber.replaceFirst('^0+', '') : '';
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<ProducerInfo>';
            xml += ' <ContractNumber>' + this.contractNumber + '</ContractNumber>';
            xml += ' <ProducerRoleCd>' + this.producerRoleCd + '</ProducerRoleCd>';
            xml += '</ProducerInfo>';
            
            return xml;
        }
    }
}