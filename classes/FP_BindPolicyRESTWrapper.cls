/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/bind-policy/*')
global with sharing class FP_BindPolicyRESTWrapper {
    
	@HttpPost
    global static QuoteRequestWrapper bindTransaction(String caseId) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        QuoteRequestWrapper bindResponse = null;
		try {
			bindResponse = FP_AngularPageController.bindPolicyREST(Id.valueOf(caseId));
		 	bindResponse.success = true;
		}
		catch (Exception e) {
            bindResponse = new QuoteRequestWrapper();
			bindResponse.success = false;
			bindResponse.errorMessages.add(e.getMessage());
			bindResponse.errorMessages.add(e.getStackTraceString());
		}
    	return bindResponse;
    }
}