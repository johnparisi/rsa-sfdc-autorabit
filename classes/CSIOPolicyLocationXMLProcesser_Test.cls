/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CSIOPolicyLocationXMLProcesser_Test {
    
    /**
    Test the entire first XML document
    */
    @testSetup static void setup(){
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    static testMethod void testXmlOneCSIOPolicyLocationXMLProcessor() {
        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(RA_AbstractTransactionTest.xmlPolicy); //originally xmlTestOne
        CSIOPolicyLocationXMLProcesser processor = new CSIOPolicyLocationXMLProcesser();
        Dom.XMLNode soapNode = xmlDocument.getRootElement().getChildElement('Body', CSIOPolicyXMLProcessor.SOAP_NAMESPACE);
        Dom.XMLNode acordNode = soapNode.getChildElement('ACORD', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode insuranceSvcRsNode = acordNode.getChildElement('InsuranceSvcRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode policySyncRsNode = insuranceSvcRsNode.getChildElement('PolicySyncRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlPkgPolicyQuoteInqRs = policySyncRsNode.getChildElement('CommlPkgPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode locationNode = commlPkgPolicyQuoteInqRs.getChildElement('Location', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlSublocationNode = commlPkgPolicyQuoteInqRs.getChildElement('CommlSubLocation', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        processor.process(locationNode, commlSublocationNode);
        Risk__c location = processor.location;
        
        //Additional interests on the location 
        Integer foundCount = 0;
        for (Quote_Risk_Relationship__C quoteRiskRelationship : processor.accountQrrMap.keySet()) {
                system.debug('+++ Relationship Type: ' + quoteRiskRelationship.Relationship_Type__c);
            if(foundCount == 0){
                System.assert(quoteRiskRelationship.Relationship_Type__c == 'First Mortgagee', 'The relationshiptype is first mortgagee' + quoteRiskRelationship.Relationship_Type__c);
            }
            foundCount = foundCount + 1;                
        }
        
        System.assert(foundCount == 2, 'Should have found two QRRs' + foundCount);
        foundCount = 0;
        system.debug('##### ACCOUNTS'+ processor.accountQrrMap.values());
        system.debug('##### CONTACTS' + processor.contactQrrMap.values());
        for (Account account: processor.accountQrrMap.values()) {
            system.debug('account Name on CSIOPolicyLocationXMLProcessor_Test:' + account.Name);
            if (account.Name == 'Toronto Dominion Bank') {
                System.assertEquals('Toronto Dominion Bank', account.Name, 'Loss payee should be Toronto Dominion Bank');
                System.assertEquals('777  Bay', account.BillingStreet, 'loss payee address should be "777 Bay"');
                System.assertEquals('M5G 2C8', account.BillingPostalCode, 'Other client bill postal code should be "M5G 2C8"');
                
            }
            foundCount = foundCount + 1;

        }
        system.debug(foundCount);
        System.assert(foundCount == 2, 'Should have found two accounts' + foundCount);
           
        foundcount = 0; 
        for (Contact contact: processor.contactQrrMap.values()) {
            foundCount = foundCount + 1;
        }
        
        System.assertEquals(0, foundCount, 'There should be no contacts');
        
        //CommlSublocation elements, starting with the construction elements 
        System.assertEquals('2010', location.Year_Built__c, 'The year it was built was 2010');
        System.assertEquals(2, location.Number_of_Stories__c, 'This location has two stories');
        System.assertEquals('Fire Resistive', location.Construction_Type__c, 'This is a fire resisitive construction type');
        
        //BldgProtection elements 
        System.assertEquals('Within 150 Meters', location.Distance_To_Fire_Hydrant__c, 'The distance is Within 150 Meters' + location.Distance_To_Fire_Hydrant__c);
        System.assertEquals('Within 5 KM', location.Distance_To_Fire_Station__c, 'The distance is Within 5 KM' +  location.Distance_To_Fire_Station__c );
        System.assert(location.Sprinkler_Coverage__c, 'The sprinkler coverage is checked');//when the sprinkler checkbox is checked, the value is set to 100 otherwise it is 0: based on CSIO_CommlSublocation
        System.assertEquals(25000, location.Total_Occupied_Area__c, 'The area occupied is 1200 FKT');
        //Alarm and security are values defaulted in as they were required by ESB when request was created 
    }
 }