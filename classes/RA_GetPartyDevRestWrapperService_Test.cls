@isTest
Public class RA_GetPartyDevRestWrapperService_Test{

    static testmethod void myTest(){
        Account ac = new Account();
        ac.name = 'test Acc';
        insert ac;
        contact con = new contact();
        con.accountid = ac.id;
        con.lastname = 'lastname';

        insert con;
        string userInput = con.lastname;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response= res;

        PartySearchResponse response= RA_GetPartyDevRestWrapperService.getParty(userInput,true);
    }

}