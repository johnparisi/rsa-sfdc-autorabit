public class SMEQ_ResourceBundlesDao {
    private static final String DEFAULT_VARIANT = 'www';
    private static final String DEFAULT_REGION = null;
    public String logoString = 'quote.header.logo.uri';



  /**
    * This method is to return resource bundles filtered by variant and bundle
    * @param String variant
    * @param String bundle
    * @return SMEQ_ResourceBundlesModel
    **/
    @TestVisible
    public SMEQ_ResourceBundlesModel getResourceBundles(String variant, String bundle){
        SMEQ_ResourceBundlesModel smeResourceBundles = new SMEQ_ResourceBundlesModel();
        List<SMEQ_KeyValueModel> resourceBundlesEnglish = new List<SMEQ_KeyValueModel>();
        List<SMEQ_KeyValueModel> resourceBundlesFrench = new List<SMEQ_KeyValueModel>();
        Map<String, String> resourceBundlesVariantMap=null;
        //Get the resource bundles by varaint and bundle that will be replacing the global variant 'www'
        if(variant!=null && variant.length()>0){
            resourceBundlesVariantMap = getResourceBundlesMapForVariant(variant,bundle);
        }
        //Get the resource bundles by global variant www
        List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesListWithOutVariant = queryResourceBundles('www', bundle);
        //Iterate through the resource bundles
        for(SMEQ_RESOURCE_BUNDLES__c resourceBundle: resourceBundlesListWithOutVariant){
            //Check if the map has already resource bundle for the key with variant if yes then overwrite the value
            if(resourceBundlesVariantMap!=null && resourceBundlesVariantMap.get(resourceBundle.string_id__c + '_' + resourceBundle.language_code__c)!=null){
                resourceBundle.value__c = resourceBundlesVariantMap.get(resourceBundle.string_id__c + '_' + resourceBundle.language_code__c);
            }
            //Check the language of the resource bundle and add to the appropriate english or french map
            if(resourceBundle.language_code__c.toUpperCase() == 'EN'){
                resourceBundlesEnglish.add(buildSMEKeyValueModel(resourceBundle));
            }
            else{
                resourceBundlesFrench.add(buildSMEKeyValueModel(resourceBundle));
            }
        }
        smeResourceBundles.resourceBundleEnglish = resourceBundlesEnglish;
        smeResourceBundles.resourceBundleFrench = resourceBundlesFrench;
        return  smeResourceBundles;
    }
    /**
    * This method is returns resource bundles filtered by variant, bundle and region
    * @param String variant
    * @param String bundle
    * @return SMEQ_ResourceBundlesModel
    **/
    public SMEQ_ResourceBundlesModel getResourceBundles(String variant, String bundle, String region){
        SMEQ_ResourceBundlesModel smeResourceBundles = new SMEQ_ResourceBundlesModel();
        List<SMEQ_KeyValueModel> resourceBundlesEnglish = new List<SMEQ_KeyValueModel>();
        List<SMEQ_KeyValueModel> resourceBundlesFrench = new List<SMEQ_KeyValueModel>();
        
        //Get the resource bundles that match parameters
        List<SMEQ_RESOURCE_BUNDLES__c> completeResourceBundlesList = queryResourceBundles(variant, bundle, region);

        if(!completeResourceBundlesList.isEmpty())
        {
            //Iterate through the resource bundles
            for(SMEQ_RESOURCE_BUNDLES__c resourceBundle : completeResourceBundlesList)
            {  
                //Check the language of the resource bundle and add to the appropriate english or french map
                if(resourceBundle.language_code__c.toUpperCase() == 'EN'){
                    resourceBundlesEnglish.add(buildSMEKeyValueModel(resourceBundle));
                }
                else{
                    resourceBundlesFrench.add(buildSMEKeyValueModel(resourceBundle));
                }
            }
        }
        
        smeResourceBundles.resourceBundleEnglish = resourceBundlesEnglish;
        smeResourceBundles.resourceBundleFrench = resourceBundlesFrench;
        return  smeResourceBundles;
    }
    
    /**
    * This method is to return resource bundles filtered by variant and region
    * @param String variant
    * @param String region
    * @return SMEQ_ResourceBundlesModel
    **/
    public SMEQ_ResourceBundlesModel getResourceBundlesByVariantAndRegion(String variant, String region){
        SMEQ_ResourceBundlesModel resourceBundles = new SMEQ_ResourceBundlesModel();
        
        List<SMEQ_KeyValueModel> resourceBundlesEnglish = new List<SMEQ_KeyValueModel>();
        List<SMEQ_KeyValueModel> resourceBundlesFrench = new List<SMEQ_KeyValueModel>();
        
        //Get the resource bundles that match parameters
        List<SMEQ_RESOURCE_BUNDLES__c> completeResourceBundlesList = queryResourceBundlesByVariantAndRegion(variant, region);
        if(!completeResourceBundlesList.isEmpty())
        {
            //Iterate through the resource bundles
            for(SMEQ_RESOURCE_BUNDLES__c resourceBundle : completeResourceBundlesList)
            {  
                //Check the language of the resource bundle and add to the appropriate english or french map
                if(resourceBundle.language_code__c.toUpperCase() == 'EN'){
                    resourceBundlesEnglish.add(buildSMEKeyValueModel(resourceBundle));
                }
                else{
                    resourceBundlesFrench.add(buildSMEKeyValueModel(resourceBundle));
                }
            }
        }
        
        resourceBundles.resourceBundleEnglish = resourceBundlesEnglish;
        resourceBundles.resourceBundleFrench = resourceBundlesFrench;
        return resourceBundles;
    }
    
    
    /**
    * This method is to return resource bundles filtered by variant, region and return default variant and region if not exists
    * @param String variant
    * @param String region
    * @return List<SMEQ_RESOURCE_BUNDLES__c>
    **/
    public List<SMEQ_RESOURCE_BUNDLES__c> queryResourceBundlesByVariantAndRegion(String variant, String region) {
        List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        Set<String> processedIDs = new Set<String>(); 
        
        if(variant == DEFAULT_VARIANT && region != null) {
            addResourceBundlesByVariantAndRegion(resourceBundlesList, processedIDs, DEFAULT_VARIANT, region, false);  
            addDefaultResourceBundles(resourceBundlesList, processedIDs, DEFAULT_REGION, true);
        } else if(variant == DEFAULT_VARIANT && (region==null || region=='')){
            addDefaultResourceBundles(resourceBundlesList, processedIDs, null, false);
        } else if(variant != null && variant != DEFAULT_VARIANT && region != null){
            addResourceBundlesByVariantAndRegion(resourceBundlesList, processedIDs, variant, region, false);   
            addResourceBundlesByVariantAndRegion(resourceBundlesList, processedIDs, variant, DEFAULT_REGION, true);  
            addResourceBundlesByVariantAndRegion(resourceBundlesList, processedIDs, DEFAULT_VARIANT, region, true);  
            addDefaultResourceBundles(resourceBundlesList, processedIDs, DEFAULT_REGION, true);
        } else if(variant != null && variant != DEFAULT_VARIANT && (region==null || region=='')){
            addResourceBundlesByVariantAndRegion(resourceBundlesList, processedIDs, variant, DEFAULT_REGION, false); 
            addDefaultResourceBundles(resourceBundlesList, processedIDs, DEFAULT_REGION, true);
        }    
        
        return resourceBundlesList;
    }
    
    
    /**
    * This method is to add resource bundles filtered by variant and region.  isIdCheckRequired indicator is used to
    * ignore the already processed string ids.
    * @param List<SMEQ_RESOURCE_BUNDLES__c> resourceBundles
    * @param Set<String> processedIDs
    * @param String variant
    * @param String region
    * @param boolean isIdCheckRequired
    * @return void
    **/
    private void addResourceBundlesByVariantAndRegion(List<SMEQ_RESOURCE_BUNDLES__c> resourceBundles, Set<String> processedIDs, String variant, String region, boolean isIdCheckRequired) {
        if(isIdCheckRequired) {
            for(SMEQ_RESOURCE_BUNDLES__c r : [SELECT String_id__c, value__c, variant__c, bundle__C, 
                                              language_code__C, region__c  from SMEQ_RESOURCE_BUNDLES__c where variant__c= :variant 
                                              and region__c = :region and String_id__c not in :processedIDs]) {
                processedIDs.add(r.String_id__c);
                resourceBundles.add(r);
            } 
        } else {
            for(SMEQ_RESOURCE_BUNDLES__c r : [SELECT String_id__c, value__c, variant__c, bundle__C, language_code__C, 
                                              region__c  from SMEQ_RESOURCE_BUNDLES__c where variant__c= :variant and 
                                              region__c = :region]) {
                processedIDs.add(r.String_id__c);
                resourceBundles.add(r);
            } 
        } 
	}

    /**
    * This method is to add resource bundles filtered by variant and region
    * @param List<SMEQ_RESOURCE_BUNDLES__c> resourceBundles
    * @param Set<String> processedIDs
    * @param String variant
    * @param String region
    * @param boolean isIdCheckRequired
    * @return void
    **/
    private void addDefaultResourceBundles(List<SMEQ_RESOURCE_BUNDLES__c> resourceBundles, Set<String> processedIDs, String region, boolean isIdCheckRequired) {
        if(isIdCheckRequired) {
            resourceBundles.addAll([SELECT String_id__c, value__c, variant__c, bundle__C, language_code__C, region__c  
                                    from SMEQ_RESOURCE_BUNDLES__c where variant__c= :DEFAULT_VARIANT and
                                    region__c = :region and String_id__c not in :processedIDs]);
        } else {
            resourceBundles.addAll([SELECT String_id__c, value__c, variant__c, bundle__C, language_code__C, region__c  
                                    from SMEQ_RESOURCE_BUNDLES__c where variant__c= :DEFAULT_VARIANT and region__c = :region]);
        }
    }

    
    /**
     * This method is build the key valaue model based on SMEQ_RESOURCE_BUNDLES__c object
     * @param SMEQ_RESOURCE_BUNDLES__c smeResourceBundle
     * @return SMEQ_KeyValueModel
     */
    @TestVisible
    private SMEQ_KeyValueModel buildSMEKeyValueModel(SMEQ_RESOURCE_BUNDLES__c smeResourceBundle){
        SMEQ_KeyValueModel smeKeyValueModel = new SMEQ_KeyValueModel();
        smeKeyValueModel.key = smeResourceBundle.string_id__c;
        smeKeyValueModel.value = smeResourceBundle.value__c;
        return smeKeyValueModel;
    }
    
    /**
     * This method queries the Resource Bundle Object to return a list that match the parameters
     * @param String variant
     * @param String bunlde
     * @return List<SMEQ_RESOURCE_BUNDLES__c>
     */
    @TestVisible
    private List<SMEQ_RESOURCE_BUNDLES__c> queryResourceBundles(String variant, String bundle, String region){
        
        List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesListForRegion = new List<SMEQ_RESOURCE_BUNDLES__c>();
        List<SMEQ_RESOURCE_BUNDLES__c> combinedList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        
        //get default variant records only if no variant parameter was passed in
        if(variant==DEFAULT_VARIANT && (region==null || region==''))
        {
            resourceBundlesList= [SELECT String_id__c, value__c, variant__c, 
                                  bundle__C, language_code__C, Region__c
                                  FROM SMEQ_RESOURCE_BUNDLES__c
                                  WHERE variant__c =:DEFAULT_VARIANT];
        }

        //get all records that have default variant or variant passed in as argument
        else if(variant!=DEFAULT_VARIANT && (region==null || region==''))
        {
             resourceBundlesList = [SELECT String_id__c, value__c, variant__c, 
                                    bundle__C, language_code__C, Region__c 
                                    FROM SMEQ_RESOURCE_BUNDLES__c
                                    WHERE (variant__c =:variant OR variant__c =:DEFAULT_VARIANT)
                                    ];
            resourceBundlesList = removeDuplicates(variant, resourceBundlesList);
        }
        else if(variant!=DEFAULT_VARIANT && region!=null)
        {
             resourceBundlesList = [SELECT String_id__c, value__c, variant__c, 
                                    bundle__C, language_code__C, Region__c 
                                    FROM SMEQ_RESOURCE_BUNDLES__c
                                    WHERE (variant__c =:variant OR variant__c =:DEFAULT_VARIANT)
                                    AND Region__c = null];
             
             resourceBundlesList = removeDuplicates(variant, resourceBundlesList);

             resourceBundlesListForRegion = [SELECT String_id__c, value__c, variant__c, 
                                            bundle__C, language_code__C, Region__c  
                                            FROM SMEQ_RESOURCE_BUNDLES__c
                                            WHERE variant__c =:variant
                                            AND Region__c =:region];

            resourceBundlesList = removeRegionDuplicates(resourceBundlesListForRegion, resourceBundlesList);

        }

        return resourceBundlesList;
    }
    
    /**
     * This method removes duplicate key entries for given variant in a resource bundle list
     * unquie key is a combination of the string id and language
     * @param String
     * @param  List<SMEQ_RESOURCE_BUNDLES__c>
     * @return List<SMEQ_RESOURCE_BUNDLES__c>
     */
    @TestVisible
    private List<SMEQ_RESOURCE_BUNDLES__c> removeDuplicates(String variant,  List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesList)
    {
        Set<String> variantIDList = new Set<String>();
        List<SMEQ_RESOURCE_BUNDLES__c> variantMatchList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        List<SMEQ_RESOURCE_BUNDLES__c> nonVariantList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        List<SMEQ_RESOURCE_BUNDLES__c> completeList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        
        for(SMEQ_RESOURCE_BUNDLES__c rb: resourceBundlesList)
        {
            if(rb.variant__c == variant)
            {
                variantIDList.add(rb.string_id__c + '_' + rb.language_code__c);
                variantMatchList.add(rb);
            }
        }
        
        for(SMEQ_RESOURCE_BUNDLES__c rb: resourceBundlesList)
        {
            //hasnt been previously added
            if(!variantIDList.contains(rb.string_id__c + '_' + rb.language_code__c))
            {
                nonVariantList.add(rb);
            }
        }
        
        completeList.addAll(variantMatchList);
        completeList.addAll(nonVariantList);
        return completeList;
    }
    
    /**
    * This method removes duplicate key entries for given region in a resource bundle list
    * The unquie key is a combination of the string id and language
    * @param String
    * @param  List<SMEQ_RESOURCE_BUNDLES__c>, List<SMEQ_RESOURCE_BUNDLES__c>
    * @return List<SMEQ_RESOURCE_BUNDLES__c>
    **/
    @TestVisible
    private List<SMEQ_RESOURCE_BUNDLES__c> removeRegionDuplicates(List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesRegionList, List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesList)
    {
        Set<String> regionIdList = new Set<String>();
        List<SMEQ_RESOURCE_BUNDLES__c> regionMatchList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        List<SMEQ_RESOURCE_BUNDLES__c> nonRegionList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        List<SMEQ_RESOURCE_BUNDLES__c> completeList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        
        for(SMEQ_RESOURCE_BUNDLES__c rg: resourceBundlesRegionList)
        {
           System.debug(rg.string_id__c);
           regionIdList.add(rg.string_id__c + '_' + rg.language_code__c);
           regionMatchList.add(rg);
            
        }
        
        for(SMEQ_RESOURCE_BUNDLES__c rb: resourceBundlesList)
        {
            //hasnt been previously added
            if(!regionIdList.contains(rb.string_id__c + '_' + rb.language_code__c))
            {
                nonRegionList.add(rb);
            }
        }
        
        completeList.addAll(regionMatchList);
        completeList.addAll(nonRegionList);
        return completeList;
    }
    
    /**
     * This method is to build the query to retrive resource bundles from the sales force
     * @param String variant
     * @param String bundle
     * @return String
     */
    @TestVisible
    private String buildResourceBundlesSearchString(String variant, String bundle)
    {
        String resourceBundleSearchQueryString = 'SELECT String_id__c, value__c, variant__c, bundle__C, language_code__C from SMEQ_RESOURCE_BUNDLES__c';
        if(variant!=null && variant.length()>0){
            resourceBundleSearchQueryString = resourceBundleSearchQueryString + ' where variant__C= :variant';
        }
        
        if(bundle!=null && bundle.length()>0){
            if(!resourceBundleSearchQueryString.contains('where')){
                resourceBundleSearchQueryString = resourceBundleSearchQueryString + ' where ';
            }
            else{
                resourceBundleSearchQueryString = resourceBundleSearchQueryString + ' AND ';
            }
            resourceBundleSearchQueryString = resourceBundleSearchQueryString + ' bundle__C = :bundle';
        }
        System.debug('The query String:' + resourceBundleSearchQueryString + ':Plus variables variant:' + variant + ':bundle:' + bundle);
        return resourceBundleSearchQueryString;
    }
    
    /**
     * This method is return the key value pairs Map for varaints and bundle
     * @param String variant
     * @param String bundle
     * @return Map<String, String>
     */
    @TestVisible
    private Map<String, String> getResourceBundlesMapForVariant(String variant, String bundle){
        Map<String, String> resourceBundleMap = new Map<String,String>();
        List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesListVariant = queryResourceBundles(variant, bundle);
        for(SMEQ_RESOURCE_BUNDLES__c resourceBundle: resourceBundlesListVariant){
            resourceBundleMap.put(resourceBundle.string_id__c + '_' + resourceBundle.language_code__c, resourceBundle.value__C);
        }
        return resourceBundleMap;
    }
    
    /**
     * This method is query the sales force objects for resource bundles return the list of found resource bundles
     * @param String variant
     * @param String bunlde
     * @return List<SMEQ_RESOURCE_BUNDLES__c>
     */
    @TestVisible
    private List<SMEQ_RESOURCE_BUNDLES__c> queryResourceBundles(String variant, String bundle){
        String queryGenerated = buildResourceBundlesSearchString(variant, bundle);
        System.debug('The Query generated:' +queryGenerated);
        List<SMEQ_RESOURCE_BUNDLES__c> resourceBundlesList = Database.query(queryGenerated);
        System.debug('The number of record returned by the query:' + resourceBundlesList.size());
        return resourceBundlesList;
    }
}