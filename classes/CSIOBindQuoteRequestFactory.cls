/**
 * CSIOBindQuoteRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to generate the xml Request for the BIND service 
 */
public class CSIOBindQuoteRequestFactory extends CSIOBindSaveQuoteRequestFactory {
    
    public CSIOBindQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds,ri);
    }
}