public with sharing class SMEQ_PackageFinalizedModel {
	public String selectedPackageID {public get; public set;}
  	public String quoteID {public get; public set;}
  	public String quoteNumber {public get; public set;}
  	public String continuationReferenceID {get; set;}
  	public String finalizedDate {get; set;}
  	public Map<String, String> logWrappers {get; set;}
    public String caseId {public get; public set;}
    public String xmlRequest {get; set;}  
    public Boolean esbErrorFlag;
    public String esbErrorMessage;
}