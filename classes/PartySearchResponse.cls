global with sharing class PartySearchResponse {
        public boolean success;
        public List<String> errorMessages = new List<String>();
        public List<sObject> accountOrContactList = new List<sObject>();
}