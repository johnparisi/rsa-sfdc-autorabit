/**
 * CSIOAbortRequestFactory
 * Author: Tim H
 * Date: September 14, 2017
 * 
 * This class generates the CSIO XML for the abort transaction
 */
	
public with sharing class CSIOAbortRequestFactory extends CSIORequestDocumentFactory implements CSIORequestFactory  {
		
	public CSIOAbortRequestFactory(SoqlDataSet sds, RequestInfo ri) {
        super(sds.requestingUser.FederationIdentifier, sds.smeCase.New_Business_Source__c, sds.quote.Business_Source_ID__c, ri);
        this.soqlDataSet = sds;
        rsaContextId = esbContext.get(sds.smeCase.Offering_Project__c);
	}
    
	public String buildXMLRequest(){
        getHeaderXml();
        xml += CSIO128Header;
        xml += '<RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
        xml += ' <rsa:ServiceOperationType>AT</rsa:ServiceOperationType>';
        xml += ' <rsa:Context>' + rsaContextId + '</rsa:Context>';
        xml += ' <rsa:SessionId>' + this.soqlDataSet.sessionId + '</rsa:SessionId>';
        xml += CSIO128Footer;
        return xml;
    }
    
}