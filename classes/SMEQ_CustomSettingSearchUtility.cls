public class SMEQ_CustomSettingSearchUtility
{
    
     /*
     * This method generates a map for a field to allow conversion of external values to 
     * internal values recoginzed by salesforce.
     * 
     * @param fieldType the field which will have values mapped to the external value.
     * @return a map of values from external system values to Salesforce values.
     */
    @TestVisible 
    public static Map<String, String> getCustomSettingsByFieldType(String fieldType)
    {
        Map<String, String> settingsMap = new Map<String, String>();
        for (SME_Mapping__c sme : SME_Mapping__c.getAll().values())
        {
            // FP-6535
            if (sme.Field__c == fieldType && sme.language__c != 'Fr')
            {
            	settingsMap.put(sme.externalValue__c, sme.SFKey__c);
            }
        }
        
        return settingsMap;
    }
    
    @TestVisible 
    public static Map<String, String> getCustomSettingsByFieldTypeLanguage(String fieldType, String language)
    {
        Map<String, String> settingsMap = new Map<String, String>();
        for (SME_Mapping__c sme : SME_Mapping__c.getAll().values())
        {
            if (sme.Field__c == fieldType && sme.language__c == language)
            {
            	settingsMap.put(sme.externalValue__c, sme.SFKey__c);
            }
        }
        
        return settingsMap;
    }
    
    public static Map<String, String> getCustomSettingsByFieldTypeVariant(String fieldType, String variant)
    {
        Map<String, String> settingsMap = new Map<String, String>();
        for (SME_Mapping__c sme : SME_Mapping__c.getAll().values())
        {
            if (sme.Field__c == fieldType && sme.variant__c == variant)
            {
            	settingsMap.put(sme.externalValue__c, sme.SFKey__c);
            }
        }
        
        return settingsMap;
    }
    
    public static Map<String, String> reverseCustomSettingsMap(Map<String, String> customSettingMap)
    {
        Map<String, String> reversedSettingsMap = new Map<String, String>();
        for (String key : customSettingMap.keySet())
        {
            reversedSettingsMap.put(customSettingMap.get(key), key);
        }
        
        return reversedSettingsMap;
    }
    
    /*
     * This method generates a map for a field to allow conversion of internal Salesforce values to 
     * values agreed and recognized by the external system.
     * 
     * @param fieldType the field which will have values mapped to the external value.
     * @return a map of values from Salesforce to values recognized by external systems.
     */
    public static Map<String, String> getLookupValuesForExternalServices(String fieldType)
    {
        Map<String, String> settingsMap = new Map<String, String>();
        for (SME_Mapping__c sme : SME_Mapping__c.getAll().values())
        {
            if (sme.Field__c == fieldType && sme.variant__c == null)
            {
            	settingsMap.put(sme.SFKey__c, sme.externalValue__c);
            }
        }
        
        return settingsMap;
    }
    
    /*
     * This method generates a map for a field to allow conversion of internal Salesforce values to 
     * values agreed and recognized by the external system.
     * 
     * @param fieldType the field which will have values mapped to the external value.
     * @return a map of values from Salesforce to values recognized by external systems.
     */
    public static Map<String, String> getLookupValuesForExternalServicesByVariant(String fieldType, String variant)
    {
        Map<String, String> settingsMap = new Map<String, String>();
        for (SME_Mapping__c sme : SME_Mapping__c.getAll().values())
        {
            if (sme.Field__c == fieldType && sme.variant__c == variant)
            {
            	settingsMap.put(sme.SFKey__c, sme.externalValue__c);
            }
        }
        
        return settingsMap;
    }
}