@isTest
private class RA_LoadPolicyTest extends RA_AbstractTransactionTest {

	@testSetup static void testSetup() {
		RA_AbstractTransactionTest.setup();
	}
    
    /**
    Test the entire first XML document
    */
    static testMethod void testLoadPolicy() {
		// find the case (shouldn't need to do this !)  
        Case parentCase = [
            SELECT Id, Com__c, CaseNumber
            FROM Case
            LIMIT 1
        ];
        
        // process the XML policy document
        CSIOPolicyXMLProcessor csioXMLProcessor = new CSIOPolicyXMLProcessor(parentCase, RA_AbstractTransactionTest.xmlPolicy);
        csioXMLProcessor.process();
        
        // test that we can find the document in the database
        Quote__c policy = [
            SELECT Id, ePolicy__c, Air_Miles_Loyalty_Points_Standard__c, Total_Revenue__c, Standard_Premium__c, Claim_Free_Years__c, Case__c
            FROM Quote__c
            WHERE Case__c = :parentCase.id
            LIMIT 1
        ];
        
        parentCase.COM__c = policy.ePolicy__c;
        policy.Transaction_Effective_Date__c = date.today();
        /*
        update parentCase;
        update policy;
        System.assertNotEquals(null, policy, 'Policy not found');
        System.assertEquals(parentCase.Id, policy.Case__c, 'Case ID on policy should be the same as the case ID on the parent case');
        System.assertNotEquals(null, policy.ePolicy__c, 'COM number not set on policy');
        System.assertEquals(parentCase.COM__c, policy.ePolicy__c, 'Policy and Case policy numbers do not match');
        
        FP_AngularPageController.loadPolicyTransaction(parentCase.CaseNumber);
        */
    }
}