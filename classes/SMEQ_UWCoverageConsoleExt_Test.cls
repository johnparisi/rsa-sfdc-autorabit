/**
 * Author: Aakash Dang
 * Created At: November 3, 2016
 * 
 * Unit tests for SMEQ_UWCoverageConsoleExt
 */
@isTest
public class SMEQ_UWCoverageConsoleExt_Test
{
    private static Map<String, String> mpString = new Map<String, String>();
    private static String keyVal = 'Earthquake';
    
    static testMethod void invokeConstructor()
    {
        
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();

            // Calling the constructor
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(risk);
            SMEQ_UWCoverageConsoleExt coverageConsole = new SMEQ_UWCoverageConsoleExt(sc);
        }
    }
    
    static testMethod void testPicklistValuesOp1()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();

            String currentID = risk.ID;
            mpString = SMEQ_UWCoverageConsoleExt.getPickListValuesForOption1(currentID);
            system.assertEquals(mpString.get(keyVal), '');
            
            Set<String> setKeyVals = new Set<String>();
            setKeyVals = mpString.keyset();
            system.assertEquals(setKeyVals.size(), 3);
        }
    }
    
    static testMethod void testPicklistValuesOp2()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();

            String currentID = risk.ID;
            mpString = SMEQ_UWCoverageConsoleExt.getPickListValuesForOption2(currentID);
            system.assertEquals(mpString.values().size(), 3);
        }
    }
    
    static testMethod void testGetPicklistValueForLabel()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            List<Schema.PicklistEntry> ple = new List<Schema.PicklistEntry>();
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk();
            
            // Calling the constructor
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(risk);
            SMEQ_UWCoverageConsoleExt coverageConsole = new SMEQ_UWCoverageConsoleExt(sc);
            
            coverageConsole.coveragesSelectedForOptionOne = '';
            coverageConsole.coveragesSelectedForOptionTwo = '';
            coverageConsole.updateCoverages();
            
            system.assert(risk.Coverage_Selected_Option_1__c != null);
            system.assert(risk.Coverage_Selected_Option_2__c != null);
        }
    }
    
    static testMethod void testPicklistValuesPolicy()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_2, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRiskForPolicy();

            String currentID = risk.ID;
            mpString = SMEQ_UWCoverageConsoleExt.getPickListValuesForPolicy(currentID);
            //should get building, equipment, stock which are in the Coverages_Selected_For_Policy__c field, and Flood, Earthquake
            //sewer backup which are the coverage add-ons for the SCDV in the test data 
            system.assert(mpString.containsKey('Flood'));
            system.assertEquals(6, mpString.values().size());
        }
    }
    
}