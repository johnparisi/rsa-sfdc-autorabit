/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RA_GetRateRequestUnitTest extends RA_AbstractTransactionTest {

    @testSetup static void testSetup() {
        RA_AbstractTransactionTest.setup();
    }
    
    /**
    Test the entire first XML document
    */
    static testMethod void testGetRate() {
        // find the case (shouldn't need to do this !)  
        Case parentCase = [
            SELECT Id
            FROM Case
            LIMIT 1
        ];
        
        SMEQ_PackageRatingModel packageRatingModel = new SMEQ_PackageRatingModel(); 
        //string rateRequest = JSON.serialize(SMEQ_PackageRatingModel);
        
        // process the XML policy document
        CSIOPolicyXMLProcessor csioXMLProcessor = new CSIOPolicyXMLProcessor(parentCase, RA_AbstractTransactionTest.xmlPolicy);
        csioXMLProcessor.process();
        
        // test that we can find the document in the database
        Quote__c policy = [
            SELECT Id, Air_Miles_Loyalty_Points_Standard__c, Total_Revenue__c, Standard_Premium__c, Claim_Free_Years__c, Case__c
            FROM Quote__c
            WHERE ePolicy__c = 'COM810101133'
            LIMIT 1
        ];
        
        System.assertNotEquals(null, policy, 'Policy not found');
        System.assertEquals(parentCase.Id, policy.Case__c, 'Case ID on policy should be the same as the case ID on the parent case');
        
        packageRatingModel.quoteID = policy.id;
        FP_AngularPageController.processGetRateRequest(JSON.serialize(packageRatingModel));
    }
}