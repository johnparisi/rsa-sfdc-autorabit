/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CI_UW_MilestoneTimeCalculator {

    static testMethod void myUnitTest() {
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;
			
			// Select an existing milestone type to test with
	        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType where Name = 'Issue' LIMIT 1];      
	        if(mtLst.size() == 0) { return; }
	        MilestoneType mt = mtLst[0];
	        
	        SlaProcess[] slaLst = [Select Id from SlaProcess where Name like 'CI%' LIMIT 1];
	        if(slaLst.size() == 0) { return; }
	        SlaProcess sla = slaLst[0];

        System.runAs(u) {   	              
			Account a = new Account(Name='My Test Account');
			insert a;
			
		 	BusinessHours[] bhLst = [Select Id from BusinessHours LIMIT 1];
		 	if(bhLst.size() == 0) { return; }
	        BusinessHours bh = bhLst[0];
	        	                   
			Entitlement ent = new Entitlement(Name='Test',AccountId=a.Id,SlaProcessId=sla.Id,BusinessHoursId=bh.Id);
			insert ent;
		 	
     
				Test.startTest(); 
					Case objCase = new Case(AccountId=a.Id,EntitlementId=ent.Id,  Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Subm_Type__c = 'Amendment - Complex', Subject = 'Test');

					insert objCase;		
							
					CI_UW_MilestoneTimeCalculator calc = new CI_UW_MilestoneTimeCalculator(); 
					Integer actualTriggerTime = calc.calculateMilestoneTriggerTime(objCase.Id, mt.Id);
				    // varies by time, unfortunately  system.assert(actualTriggerTime == 10*8*60);
				    
				    MilestoneType[] mtLst1 = [SELECT Id, Name FROM MilestoneType where Name = 'Triage' LIMIT 1];      
			        if(mtLst1.size() == 0) { return; }
			        MilestoneType mt1 = mtLst1[0];
					
					calc = new CI_UW_MilestoneTimeCalculator(); 
					actualTriggerTime = calc.calculateMilestoneTriggerTime(objCase.Id, mt1.Id);
					// varies by time, unfortunately  system.assert(actualTriggerTime == 30);
												
					objCase.bkrCase_Subm_Type__c = 'New Business';
					update objCase;

				    MilestoneType[] mtLst2 = [SELECT Id, Name FROM MilestoneType where Name = 'Quote Issue' LIMIT 1];      
			        if(mtLst2.size() == 0) { return; }
			        MilestoneType mt2 = mtLst2[0];
					
					calc = new CI_UW_MilestoneTimeCalculator(); 
					actualTriggerTime = calc.calculateMilestoneTriggerTime(objCase.Id, mt2.Id);
					
				Test.stopTest();
			}

    }

    static testMethod void myUnitTest2() {
        	User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y');
			insert u;
			
			// Select an existing milestone type to test with
	        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType where Name = 'Triage' LIMIT 1];      
	        if(mtLst.size() == 0) { return; }
	        MilestoneType mt = mtLst[0];
	        
	        SlaProcess[] slaLst = [Select Id from SlaProcess where Name like 'CI%' LIMIT 1];
	        if(slaLst.size() == 0) { return; }
	        SlaProcess sla = slaLst[0];
		System.runAs(u) {  	              
			Account a = new Account(Name='My Test Account');
			insert a;
			
		 	BusinessHours[] bhLst = [Select Id from BusinessHours LIMIT 1];
		 	if(bhLst.size() == 0) { return; }
	        BusinessHours bh = bhLst[0];
	        	                   
			Entitlement ent = new Entitlement(Name='Test',AccountId=a.Id,SlaProcessId=sla.Id,BusinessHoursId=bh.Id);
			insert ent;
		 	
      
				Test.startTest(); 
					Case objCase = new Case(AccountId=a.Id,EntitlementId=ent.Id,  Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');
		
					insert objCase;		
							
					CI_UW_MilestoneTimeCalculator calc = new CI_UW_MilestoneTimeCalculator(); 
					Integer actualTriggerTime = calc.calculateMilestoneTriggerTime(objCase.Id, mt.Id);
				
				Test.stopTest();
			}

    }    
	      
}