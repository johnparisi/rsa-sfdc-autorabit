global with sharing class InvokableUpdateCaseDMOAssignmentRules {

    @InvocableMethod(label='Update a Case using Assignment Rules' description='Updates a Case an executes Assignment Rules.')
    global static void InvokableUpdateCaseDMOAssignmentRules( List<UpdateCaseRequest> lstRequests )
    {  
		Database.DMLOptions dmo = new Database.DMLOptions();
		dmo.assignmentRuleHeader.useDefaultRule = true;       
		
		Boolean isUseAsyncCallout = false;
		List<Case> lstCases = new List<Case>();
		List<ID> lstCaseIds = new List<ID>();
		
		// determine if we should use async
       	if(lstRequests[0].useAsyncCallout==true){			
	        if(lstRequests[0].recordTypeLabel != null){
		        for(UpdateCaseRequest request : lstRequests){        	
					lstCases.add(new Case(Id=request.caseId,RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(request.recordTypeLabel).getRecordTypeId()));
		        }
		        update lstCases;
	        }
	        // now call the assignment rules async
	        for(UpdateCaseRequest request : lstRequests){        	
				lstCaseIds.add(request.caseId);
	        }
	        updateAsynchronously(lstCaseIds);
       	}else{
       		Case c;
	        for(UpdateCaseRequest request : lstRequests){        	
	            c = new Case(Id=request.caseId);
	            if(request.recordTypeLabel != null)
	            	c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(request.recordTypeLabel).getRecordTypeId();
	            c.setOptions(dmo);				
				lstCases.add(c);
	        }
	        update lstCases;
       	}	    
    }

	  global class UpdateCaseRequest {
	    @InvocableVariable(required=true)
	    public ID caseId;

	    @InvocableVariable(required=true)
	    public boolean useAsyncCallout; 

	    @InvocableVariable(required=false)
	    public string recordTypeLabel;
	  }    
	      
    @future
    public static void updateAsynchronously(List<ID> lstCaseIds)
    {   	
    	Database.DMLOptions dmo = new Database.DMLOptions();
		dmo.assignmentRuleHeader.useDefaultRule = true; 
		Case c;
		List<Case> lstCases = new List<Case>();
		
		for(ID caseId : lstCaseIds){
	        c = new Case(Id=caseId);
	        c.setOptions(dmo);
	        lstCases.add(c);
		}
        update lstCases;
    }    
}