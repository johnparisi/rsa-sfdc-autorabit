global with sharing class SMEQ_CustomSettingResponseModel extends SMEQ_RestResponseModel
{
    
    public List<SMEQ_KeyValueModel> customSettings;
}