/**
  * @author        Anmol Bakshi
  * @date          10/18/2016
  * @description   Main class to receive requests from ESB for SLID processes
*/
global class SLID_ESB_Service {

    global class RecordResponse
    {
        webservice String recordNumber;
        webservice String recordId;
        webservice String recordType;
        webservice String recOperation;
        webservice Boolean isSuccess;
        webservice String failureMessage;
    }

    global class ItemResponse
    {
        //webservice boolean isSucces {get; set;}
        webservice boolean isSuccess;
        webservice String failureMessage;
        webservice List<RecordResponse> recResponse;
    }

    global class ServiceResponse
    {
        webservice Boolean isSuccess;
        webservice Boolean allOrNone;
        webservice String failureMessage;
        webservice String failureType;
        webservice List<ItemResponse> itemResponses;
    }

    global Class KeyValPair
    {
        webservice String key;
        webservice String value;
        global KeyValPair() {} //Adding in blank constructor to bypass bug
        global KeyValPair(String Key, String Value)
        {
            this.key = Key;
            this.value = Value;
        }
    }

    global class DataRecord
    {
        webservice List<KeyValPair> params;

        public String get(String nameValue)
        {
            for (KeyValPair param : params)
            {
                if (param.key == nameValue)
                {
                    return param.value;
                    break;
                }
            }
            return '';
        }
    }

    global Class CallData
    {
        webservice String source;
        webservice String processName;
        webservice String transactionId;
        webservice String ESBTimeStamp;
        webservice List<DataRecord> records;
    }

    webservice static ServiceResponse handleESBPayload(CallData dataFromESB)
    {
        // Variable setting for FP-6578
        SLID_ESB_ServiceConstants.isSLID = true;

        //Instantiate Service Response placeholder
        ServiceResponse response;

        //Create Integration Log
        Util_Logging.integrationLogId = UTIL_Logging.createIntegrationLog(
                                                            SLID_ESB_ServiceUtils.inboundRecordTypeId,
                                                            String.valueOf(dataFromESB.records),
                                                            dataFromESB.processName,
                                                            dataFromESB.source,
                                                            dataFromESB.transactionId + '_' + dataFromESB.ESBTimeStamp
                                                            );
        try
        {

            /*
            1. Query for the SLID_Mapping_ServiceProcess__c record that aligns with the process for which the webservice is invoked and the source system from which the service is invoked
            2. Initiatize the object handler
            3. Loop through the batches sent from the ESB. For every target object of this process, create a new record of that type
            4. Upsert the set of records which have been initialized
            5. Set and return the webservice response
            */


            //1. Query the service Process by processName and source. Throws a error if the service process does not exist or is not valid.
            SLID_Mapping_ServiceProcess__c serviceProcess = SLID_ESB_ServiceUtils.getServiceProcessByNameAndSource(dataFromESB.processName, dataFromESB.source);

            //2. Initialize the object handler
            SLID_ESB_ServiceUtils.ObjectHandler objHandler = new SLID_ESB_ServiceUtils.ObjectHandler(serviceProcess, dataFromESB);

            //3. Loop through the data records send from the ESB
            objHandler.instantiateRecords();

            //4. Upsert the set of records that have been initialized for this request
            objHandler.upsertObjects();

            //5. Get the service response
            response = objHandler.getServiceResponse();
        }
        catch (Exception e)
        {
            //Set request isSuccess to false
            response = new SLID_ESB_Service.ServiceResponse();
            response.isSuccess = false;
            response.failureMessage = e.getMessage();
            response.failureType = e.getTypeName();

            //Create a exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_Service','handleESBPayload','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
        }

        //return the response
        return response;
    }


}