/**
  * @author        Suman Haldar
  * @date          10/18/2016
  * @description   Main class to receive requests from ESB for PolicyWorks
*/
global class PWP_ESB_Service {

    /**
    *  @description   Inner class to store the response data that would be sent back to ESB as part of inner class 'ServiceResponse'
    */

    global class DataResponse{
        // This variable stores the key of the record which is of the patter ObjectAPIName.FieldAPIName. Sample key could be 'Case.Status'
        webservice String key;
        // This variable stores the value of the record. Sample value could be 'In-progress'
        webservice String value;
    }


    /**
    *  @description   Inner class to store the response that would be sent back to ESB
    */
    global class ServiceResponse{
        /**
        *  This variable stores the boolean that says whether the integration is a success or failure
        */
        webservice Boolean isSuccess;

        webservice Boolean allOrNone;

        /**
        *  This variable stores the failure message from the exception record
        */
        webservice String failureMessage;

        /**
        *  This variable stores the failure type from the exception record
        */
        webservice String failureType;

        /**
        *  This variable stores list of 'DataResponse' records
        */
        webservice List<DataResponse> listDataResponses;
    }

    /**
    *  @description   Inner class to store the request that ESB would be sent to salesforce
    */
    global Class KeyValPair{
        webservice String key;
        webservice String value;
        //webservice String type;

        //global KeyValPair() {} //Adding in blank constructor to bypass bug

        global KeyValPair(String Key, String Value){
            this.key = Key;
            this.value = Value;
            //this.type = Type;
        }
    }

    /**
    *  @description   Inner class to store the request that ESB would be sent to salesforce
    */
    global class DataRecord{
        webservice List<KeyValPair> params;

        /*
        public String get(String nameValue){
            for (KeyValPair param : params){
                if (param.key == nameValue){
                    return param.value;
                    break;
                }
            }
            return '';
        }
        */
    }

    /**
    *  @description   Inner class to store attachments from the request that ESB would be sent to salesforce
    */
    global class AttachmentRecords{
        webservice List<KeyValPair> params;

        /*
        public String get(String nameValue){
            for (KeyValPair param : params){
                if (param.key == nameValue){
                    return param.value;
                    break;
                }
            }
            return '';
        }
        */
    }

    /**
    *  @description   Inner class to store the risks from the request that ESB would be sent to salesforce
    */
    global class RiskRecord{
        webservice List<KeyValPair> params;

        /*
        public String get(String nameValue){
            for (KeyValPair param : params){
                if (param.key == nameValue){
                    return param.value;
                    break;
                }
            }
            return '';
        }
        */
    }

    /**
    *  @description   Inner class to store the risks from the request that ESB would be sent to salesforce
    */
    global class CoverageRecord{
        webservice List<KeyValPair> params;

        /*
        public String get(String nameValue){
            for (KeyValPair param : params){
                if (param.key == nameValue){
                    return param.value;
                    break;
                }
            }
            return '';
        }
        */
    }

    /**
    *  @description   Inner class to store the risks from the request that ESB would be sent to salesforce 
    */
    global class RiskList{
        webservice List<RiskDetail> riskDetail;
    }

    global class RiskDetail{
        webservice List<RiskRecord> riskRecord;
        webservice List<CoverageList> coverageList;
    }

    global class CoverageList{
        webservice List<CoverageRecord> coverageRecord;
    }

    /**
    *  @description   Inner class to store the request that ESB would be sent to salesforce 
    */
    global Class CallData{
        webservice String source;
        webservice String processName;
        webservice String transactionId;
        webservice String ESBTimeStamp;
        webservice List<DataRecord> records;
        webservice List<AttachmentRecords> attRecords;
        webservice List<RiskList> locationRiskList;
        webservice List<RiskList> liabilityRiskList;
    }


    /**
    *  @description This method would be hit by ESB as part of the Policy Works integration 
    *
    *  @param       PWP_ESB_Service.CallData 
    *  @return      PWP_ESB_Service.ServiceResponse
    */
    webservice static ServiceResponse handleESBPayload(CallData dataFromESB){
        system.debug('dataFromESB -->'+dataFromESB);

        //Instantiate Service Response placeholder
        ServiceResponse response;

        //Create Integration Log
        Util_Logging.integrationLogId = UTIL_Logging.createIntegrationLog(
                Schema.SObjectType.Integration_Log__c.getRecordTypeInfosByName().get('INBOUND').getRecordTypeId(),
                String.valueOf(dataFromESB.records),
                'Policy Works Integration',
                'PolicyWorks',
                dataFromESB.transactionId + '_' + dataFromESB.ESBTimeStamp
        );
        //system.debug('Util_Logging.integrationLogId -->'+Util_Logging.integrationLogId);
        try{
            PWP_ESB_ServiceUtils objServiceUtils = new PWP_ESB_ServiceUtils(dataFromESB);

            // To iterate over the ESB data set and insert the records into Salesforce Object structure
            Map<String,SObject> mapObjNameRecord = objServiceUtils.instantiateRecords(dataFromESB);

            // To set the response value after doing Salesforce operations
            response = objServiceUtils.createServiceResponse(mapObjNameRecord);
        }
        catch (Exception e){
            //Set request isSuccess to false
            response = new PWP_ESB_Service.ServiceResponse();
            response.isSuccess = false;
            response.failureMessage = e.getMessage();
            response.failureType = e.getTypeName();

            //Create a exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'PWP_ESB_Service','handleESBPayload','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
        }
        return response;
    }
}