/**
This class decomposes the CSIO Policy XML into a set of Quote Risk Relationship and an account or contact
*/

public with sharing class CSIOPolicyAdditionalPartyProcessor {
    
    public Quote_Risk_Relationship__c quoteRiskRelationship = null;
    public Account partyAccount = null;
    public Contact partyContact = null;
    public Boolean isAccount = false;
   
    /**
    Processes an additional party node into a quote risk relationship, account or contact
    @param xmlNode The additional party XML node
    */
    
    public void process(Dom.XMLNode xmlNode) {
        quoteRiskRelationship = new Quote_Risk_Relationship__c();
        // address, if it is present
        CSIOPolicyAddrXMLProcessor addressProcessor = processAddress(xmlNode);
        // figure out if it is an account or a contact
        Dom.XMLNode nameInfo = xmlNode.getChildElement('GeneralPartyInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('NameInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        //Dom.XmlNode supplementaryNode = nameInfo.getChildElement('SupplementaryNameInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        // will either be a commercial name or a family name
        if (nameInfo.getChildElement('CommlName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null) {
            // create an account and populate it
            partyAccount = new Account();
            isAccount = true;
            Id accountOtherPartyRecordType = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true).get('Other_Party_Account'); 
            partyAccount.RecordTypeId = accountOtherPartyRecordType;
            partyAccount.Name = nameInfo.getChildElement('CommlName', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('CommercialName',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
            // add address attributes if they are present
            if (addressProcessor !=null) {
                partyAccount.BillingStreet = addressProcessor.streetAddress;
                partyAccount.BillingCity = addressProcessor.city;
                partyAccount.BillingState = addressProcessor.province;
                partyAccount.BillingCountry = addressProcessor.country;
                partyAccount.BillingPostalCode = addressProcessor.postalCode;
            }
            
            if (CSIOPolicyXMLProcessor.evaluatePath(xmlNode.getChildElement('AdditionalInterestInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('AccountNumberId', CSIOPolicyXMLProcessor.ACORD_NAMESPACE)) != null) {
                partyAccount.Contract_Number__c = xmlNode.getChildElement('AdditionalInterestInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('AccountNumberId', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
            }
        }
        else {
            partyContact = new Contact();
            isAccount = false;
            Id contactOtherPartyRecordType = Utils.GetRecordTypeIdsByDeveloperName(Contact.SObjectType, true).get('Other_Party_Contact');
            partyContact.RecordTypeId = contactOtherPartyRecordType;
            partyContact.FirstName = nameInfo.getChildElement('FamilyName',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('FamilyNames',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
            partyContact.LastName = nameInfo.getChildElement('FamilyName',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Surname',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
            Dom.XMLNode addrNode = xmlNode.getChildElement('GeneralPartyInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Addr',CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
            // add the address attributes
            if (addressProcessor !=null) {
                partyContact.MailingStreet = addressProcessor.streetAddress;
                partyContact.MailingCity = addressProcessor.city;
                partyContact.MailingState = addressProcessor.province;
                partyContact.MailingCountry = addressProcessor.country;
                partyContact.MailingPostalCode = addressProcessor.postalCode;
            }
        }
        // now the Quote Risk Relationship

        //System.debug('#### party_type ' + quoteRiskRelationship.Party_Type__c + ' natureInterestCD ' + natureInterestCD + ' esbNatureInterestCd ' + esbNatureInterestCd.get(natureInterestCD));
    }
    
    // private method to return an address processor, if it is present
    
    private CSIOPolicyAddrXMLProcessor processAddress(Dom.XMLNode xmlNode) {
        CSIOPolicyAddrXMLProcessor csioPolicyAddrXMLProcessor = null;
        boolean addressPresent = false;
        Dom.XMLNode addrNode = xmlNode.getChildElement('GeneralPartyInfo',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Addr',CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
           if (addrNode != null) {
               addressPresent = true;
            csioPolicyAddrXMLProcessor = new CSIOPolicyAddrXMLProcessor();
            csioPolicyAddrXMLProcessor.process(addrNode);
        }
        return CSIOPolicyAddrXMLProcessor;
    }
    
}