/**
 * SMEQ_UWQuoteEdit_Test
 * Author: Sophie Tran
 * Date: November 16 2017
 * 
 * This is the test class for SMEQ_UWQuoteEdit
 */
@isTest
public class SMEQ_UWQuoteEdit_Test {
    
     static testMethod void invokeConstructorOnEdit()
     {      
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        System.runAs(uw)
        {
            Test.startTest();
            Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c risk = SMEQ_TestDataGenerator.getLocationRisk(quote);
            risk.Primary_Location__c = true;
            update risk;
            // Calling the constructor for Open quote
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(quote);
            SMEQ_UWQuoteEdit uwQuoteEditOpen = new SMEQ_UWQuoteEdit(sc1);
            
            quote.RecordTypeId = QuoteTypes.get(QuoteHandler.RECORDTYPE_CLOSED_QUOTE);
            update quote;
            
            // Calling the constructor for Closed Quote
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(quote);
            SMEQ_UWQuoteEdit uwQuoteEditClose = new SMEQ_UWQuoteEdit(sc2);
            
            quote.Mailing_Address_Is_The_Same__c = true;
            update quote;
            uwQuoteEditClose.updateMailingAddress();
            uwQuoteEditClose.getSICQuestionsWithoutSICCode(quote);
            
            try{
            list<quote__c> listQuote = new list<Quote__c>{quote};
            ApexPages.Standardsetcontroller sc2set = new ApexPages.Standardsetcontroller(listQuote);
            SMEQ_UWQuoteEdit uwQuoteEditCloseset = new SMEQ_UWQuoteEdit(sc2set);
            }catch(exception ex){}
            Test.stopTest();
        }
     }
    
    /*static testMethod void invokeConstructorOnInsert()
     {
        
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        List <Quote__c> lstQuote = new List<Quote__c>();
        System.runAs(uw)
        {
            Test.startTest();
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c newQuote1 = new Quote__c();
            newQuote1.RecordTypeId = QuoteTypes.get(QuoteHandler.RECORDTYPE_OPEN_QUOTE);
            newQuote1.Case__c = cse.Id;
            lstQuote.add(newQuote1);
            insert lstQuote;

            // Calling the constructor for new quote
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstQuote);
            //system.debug('++++++ sc' + sc);
            //system.debug('++++++ lstQuote' + lstQuote);
            sc.setSelected(lstQuote);
            //SMEQ_UWQuoteEdit uwQuoteEditNew = new SMEQ_UWQuoteEdit(sc);
            Test.stopTest();
        }
     }*/
     static testMethod void testSave()
     {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);

        System.runAs(uw)
        {
            Test.startTest();
            
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
            SIC_Code_Detail_Version__c sicCodeVersion = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            List<SIC_Question__c> questions = new List<SIC_Question__c>(); 
            Map<SIC_Question__c, SIC_Answer__c> fields = new Map<SIC_Question__c, SIC_Answer__c>(); 
            questions = SicQuestionService.getAllApplicationQuestions(sicCodeVersion, SicQuestionService.CATEGORY_QUOTE);
            
            List<Sic_Question__c> picklistQuestion = new List<SIC_Question__c>();    
            for (Sic_Question__c sq : questions)
            {
                if(sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    sq.AvailableValuesEn__c = 'Value1;Value2;Value3;Value4';
                    picklistQuestion.add(sq);
                } 
            }
            update picklistQuestion;
            
            // Calling the constructor
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQ_UWQuoteEdit uwQuoteEditOpen = new SMEQ_UWQuoteEdit(sc);
            system.debug('++++ uwQuoteEditOpen.fields '+ uwQuoteEditOpen.fields.keyset());
            
            for (Sic_Question__c sq : uwQuoteEditOpen.fields.keySet())
            {
                Sic_Answer__c sa = uwQuoteEditOpen.fields.get(sq);
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_TEXT)
                {
                    sa.Value__c = 'OneTwoThree';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_NUMBER)
                {
                    sa.Value__c = '12345';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_CURRENCY)
                {
                    sa.Value__c = '12345';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_CHECKBOX)
                {
                    sa.Value__c = 'true';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_LONG_TEXT)
                {
                    sa.Value__c = 'OneTwoThreeOneTwoThreeOneTwoThreeOneTwoThree';
                }
                if (sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    sa.Value__c = 'OneTwoThree';
                }    
            }
            System.assert(!uwQuoteEditOpen.fields.isEmpty());
            system.debug('+++++ ' + uwQuoteEditOpen.fields);
            uwQuoteEditOpen.saveQuote();
            fields = SICQuestionService.getSicAnswers(quote.Id, questions);
            System.assert(!uwQuoteEditOpen.fields.isEmpty());  
            Test.stopTest();
        }
     }
     static testMethod void testPicklistOptions()
     {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        Map<String, Id> QuoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        List<SIC_Question__c> questions = new List<SIC_Question__c>(); 
        Map<SIC_Question__c, SIC_Answer__c> fields = new Map<SIC_Question__c, SIC_Answer__c>(); 

        System.runAs(uw)
        {
            Test.startTest();
            
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote();
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
            SIC_Code_Detail_Version__c sicCodeVersion = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            questions = SicQuestionService.getAllApplicationQuestions(sicCodeVersion, SicQuestionService.CATEGORY_QUOTE);
            List<Sic_Question__c> picklistQuestion = new List<SIC_Question__c>();    
            for (Sic_Question__c sq : questions)
            {
                if(sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    sq.AvailableValuesEn__c = 'Value1;Value2;Value3;Value4';
                    picklistQuestion.add(sq);
                } 
            }
            update picklistQuestion;

            // Calling the constructor
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(quote);
            SMEQ_UWQuoteEdit uwQuoteEditOpen = new SMEQ_UWQuoteEdit(sc);
            
            picklistQuestion[0].Input_Type__c = SICQuestionService.INPUT_TYPE_MULTI_SELECT_DROPDOWN;
            update picklistQuestion;
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(quote);
            SMEQ_UWQuoteEdit uwQuoteEditOpen1 = new SMEQ_UWQuoteEdit(sc1);
            
            
            for (Sic_Question__c sq : UWQuoteEditOpen.fields.keyset())
            {
                if(sq.Input_Type__c == SicQuestionService.INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
                {
                    system.assert(!uwQuoteEditOpen.picklistOptions.get(sq).isEmpty());
                    system.assertEquals(5, uwQuoteEditOpen.picklistOptions.get(sq).size());
                    
                } 
            }
            Test.stopTest();
        }
     }
 }