/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_CommlPropertyInfo.
 */
@isTest
public class CSIO_CommlPropertyInfo_Test
{
    @testSetup
    static void dataSetup()
    {
        //SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
	static testMethod void testConvert()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            
            SMEQ_TestDataGenerator.initializeSMEMappings();
            String packageId = RiskService.PACKAGE_OPTION_1;
            
            Risk__c location = SMEQ_TestDataGenerator.getLocationRisk();
            location = [
                SELECT id, Building_Value__c, Equipment_Value__c, Stock_Value__c
                FROM Risk__c
                WHERE id = :location.Id
            ];
            location.Building_Value__c = 5000;
            location.Equipment_Value__c = 500;
            location.Stock_Value__c = 500;
            update location;
            
            Coverages__c cBuilding = SMEQ_TestDataGenerator.getCoverage(location, packageId, 'Building');
            Coverages__c cEquipment = SMEQ_TestDataGenerator.getCoverage(location, packageId, 'Equipment');
            Coverages__c cStock = SMEQ_TestDataGenerator.getCoverage(location, packageId, 'Stock');
            
            Coverages__c cFlood = SMEQ_TestDataGenerator.getExtraCoverage(location, packageId, 'Flood');
            cFlood.Limit_Value__c = 250000;
            cFlood.Deductible_Value__c = 1000;
            Coverages__c cEarthquake = SMEQ_TestDataGenerator.getExtraCoverage(location, packageId, 'Earthquake');
            cEarthquake.Limit_Value__c = -1;
            Coverages__c cSewerBackup = SMEQ_TestDataGenerator.getExtraCoverage(location, packageId, 'Sewer Backup');
            cSewerBackup.Limit_Value__c = -1;
            
            List<Coverages__c> coverages = new List<Coverages__c>();
            coverages.add(cBuilding);
            coverages.add(cEquipment);
            coverages.add(cStock);
            
            List<Coverages__c> removedCoverages = new List<Coverages__c>();
            removedCoverages.add(cFlood);
            removedCoverages.add(cEarthquake);
            removedCoverages.add(cSewerBackup);
            
            SOQLDataSet.Risk r = new SOQLDataSet.Risk();
            r.packageId = packageId;
            r.risk = location;
            r.coverages = coverages;
            r.removedCoverages = removedCoverages;
            
            CSIO_CommlPropertyInfo ccpi = new CSIO_CommlPropertyInfo(r);
            String output = '';
            output += ccpi;
            
            System.assertEquals(
                '<CommlPropertyInfo LocationRef="' + location.Id + '">' + 
                new CSIO_CommlCoverage(cBuilding) + 
                new CSIO_CommlCoverage(cEquipment) + 
                new CSIO_CommlCoverage(cStock) + 
                new CSIO_CommlCoverage(cFlood) + 
                new CSIO_CommlCoverage(cEarthquake) + 
                new CSIO_CommlCoverage(cSewerBackup) + 
                '</CommlPropertyInfo>',
                output
            );
        }
    }
    
    public static SOQLDataSet.Risk getRisk(Risk__c location, String packageId)
    {
        Coverages__c cBuilding = SMEQ_TestDataGenerator.getCoverage(location, packageId, 'Building');
        Coverages__c cEquipment = SMEQ_TestDataGenerator.getCoverage(location, packageId, 'Equipment');
        Coverages__c cStock = SMEQ_TestDataGenerator.getCoverage(location, packageId, 'Stock');
        Coverages__c cFlood = SMEQ_TestDataGenerator.getExtraCoverage(location, packageId, 'Flood');
        
        List<Coverages__c> coverages = new List<Coverages__c>();
        coverages.add(cBuilding);
        coverages.add(cEquipment);
        coverages.add(cStock);
        
        List<Coverages__c> removedCoverages = new List<Coverages__c>();
        removedCoverages.add(cFlood);
        
        SOQLDataSet.Risk r = new SOQLDataSet.Risk();
        r.packageId = packageId;
        r.risk = location;
        r.coverages = coverages;
        r.removedCoverages = removedCoverages;
        
        return r;
    }
}