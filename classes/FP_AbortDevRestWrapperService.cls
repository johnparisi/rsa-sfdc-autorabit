/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/abort/*')
global with sharing class FP_AbortDevRestWrapperService {
    
    @HttpPost
    global static AbortResponse abortTransaction(String caseId) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        AbortResponse abortResponse = null;
		try {
			abortResponse = FP_AngularPageController.abortTransactionREST(Id.valueOf(caseId));
		 	abortResponse.success = true;
		}
		catch (Exception e) {
            abortResponse = new AbortResponse();
			abortResponse.success = false;
			abortResponse.errorMessages.add(e.getMessage());
			abortResponse.errorMessages.add(e.getStackTraceString());
		}
    	return abortResponse;
    }
    

}