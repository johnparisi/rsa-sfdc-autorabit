/**
* SMEQ_CustomSettingDao
* Author: Yasmin Shash
* Date: November 29 2016
*
* The purpose of this Dao class is query and return relevant records in the
* SME_Mapping_c object
*/

public class SMEQ_CustomSettingDao {

	private static final String DEFAULT_VARIANT = 'www';
	private static final String LIVECHAT_SFKEY_CHAT_BUTTON = 'SFDC_CHAT_BUTTON_ID';
	private static final String LIVE_CHAT_FIELD_TYPE = 'livechat';

/***
* Get list of key value pairs associated with  the field type in custom settings
* @param String settingType
* @return SMEQ_CustomSettingResponsModel
*/
	public SMEQ_CustomSettingResponseModel getCustomSettingsByType(String fieldType)
		{
			SMEQ_CustomSettingResponseModel cSettingResponseModel = new SMEQ_CustomSettingResponseModel();
			List<SMEQ_KeyValueModel> settingsMap = new List<SMEQ_KeyValueModel>();
			List<SME_Mapping__c> settingList = [SELECT externalValue__c,
					SFKey__c FROM SME_Mapping__c
					WHERE Field__c =:fieldType];

			if (!settingList.isEmpty())
			{
				for (SME_Mapping__c cSetting:settingList)
					{
						SMEQ_KeyValueModel aSetting = new SMEQ_KeyValueModel();
						aSetting.key = cSetting.SFKey__c;
						aSetting.value = cSetting.externalValue__c;
						settingsMap.add(aSetting);
					}
				cSettingResponseModel.customSettings = settingsMap;

			}
			return cSettingResponseModel;
		}

	/***
	* Get list of key value pairs associated with type and variant in SME_Mapping__c custom settings
	* @param String fieldType, String variant
	* @return SMEQ_CustomSettingResponsModel
	*/
	public SMEQ_CustomSettingResponseModel getCustomSettingsByTypeAndVariant(String fieldType, String variant) {
		if (String.isNotBlank(fieldType) && fieldType == LIVE_CHAT_FIELD_TYPE) {
			return processResponse(getCustomSettingsForLiveChat(fieldType, variant));
		} else {
			return processResponse(getCustomerSettingsForAll(fieldType, variant));
		}
	}

	/**
	*Method to fetch customer setting for all flows
	*
	*/
	private List<SME_Mapping__c> getCustomerSettingsForAll(String fieldType, String variant) {
		List<SME_Mapping__c> settingList;

		//filter by field type and variant if both are specified
		if (variant != DEFAULT_VARIANT && variant != null && variant != '' && fieldType != null && fieldType != '')
		{
			settingList = [ SELECT externalValue__c, SFKey__c , variant__c
					FROM SME_Mapping__c WHERE
					Field__c =:fieldType OR variant__c =:variant];

			//remove duplicates
			settingList = removeDuplicates(settingList, variant);
		}

		//if variant is not specfied filter by field type only and use default variant
		else if (fieldType != null && fieldType != '')
		{
			settingList = [ SELECT externalValue__c, SFKey__c, variant__c
					FROM SME_Mapping__c WHERE
					Field__c =:fieldType AND variant__c =:DEFAULT_VARIANT];
		}

		return settingList;
	}
	/**
	* This method removes duplicate objects that have the same key but different variants
	* in the SME_Mapping__c Object
	* @param customSettingsList
	* @param String variant
	* @return List<SME_Mapping__c>
	**/
	@TestVisible
	private List<SME_Mapping__c> removeDuplicates(List<SME_Mapping__c> customSettingsList,
												  String variant) {
		for (Integer i = 0; i < customSettingsList.size(); i++) {
			/* live chat configuration will only contain duplicates for SFDC_CHAT_BUTTON_ID
			   If two entries for this key are found entries that dont match the variant
			   should be removed
			*/
			if (customSettingsList[i].SFKey__c == LIVECHAT_SFKEY_CHAT_BUTTON &&
							customSettingsList[i].variant__c != variant) {
				customSettingsList.remove(i);
			}
		}
		return customSettingsList;
	}
	/***
	* Get list of key value pairs associated with type, variant, region, userLanguage in SME_Mapping__c custom settings
	* @param String fieldType      Value can be  livechat
	* @param String variant        Values can be FP or WWW (i.e focal point or www or others)
	* @return List<SME_Mapping__c> list of chat settings
	*/
	private List<SME_Mapping__c> getCustomSettingsForLiveChat(String fieldType, String variant) {
		String userRegion = UserService.getRegion(UserInfo.getUserId())[0];
		String userLanguage = UserService.getFormattedUserLanguage();
		if (hasAllParamsPresent(fieldType, userRegion, userLanguage, variant)) {
			return getChatSettings(fieldType,userRegion,userLanguage, variant);
		}
		return null;
	}
	/*
	*Method to retrieve values chat settings from SME Mapping
	 @param String region         Denotes user province
	*@param String userLanguage   Denotes whether the user language is English or French
	*@param String variant        Values can be FP or WWW (i.e focal point or www or others)  or etc
	*@param String fieldType      Value can be  livechat or etc
	*@return List<SME_Mapping__c> Returns list of user settings
	**/
	private List<SME_Mapping__c> getChatSettings(String fieldType, String region, String language, String variant) {
		List<SME_Mapping__c> smeMappingsList = new List<SME_Mapping__c>();
		Set<String> processedIds = new Set<String>();
		//This is to retrieve the settings based on variant(FP or liveChat or etc), fieldType etc
		smeMappingsList.addAll(getSmeMappings(variant, region, language, fieldType, processedIds, false));
		/*This is to retrieve settings if values like region or language is null/blank for fields like org id etc
		* which is not bounded to any region or language
		*/
		smeMappingsList.addAll(getSmeMappings(variant, null, null, fieldType, processedIds, true));
		return smeMappingsList;
	}
	/*
	* Method to retrive values from SMEMapping table
	*@param String variant          Values can be FP or WWW (i.e focal point or www or others)
	*@param String userRegion       Denotes user province
	*@param String userLanguage     Denotes whether the user language is English or French
	*@param String fieldType        Value can be  livechat
	*@param Set<?> processIds       Values of previously fetched ids
	*@param Boolean ignoreProcessed Default False; and True - only if previously a list has been processed
    *									from SMEMapping table
	*@return List<SME_Mapping__c> Returns list of chat settings
	*/
	private List<SME_Mapping__c> getSmeMappings(String variant, String userRegion, String userLanguage, String fieldType,
											   Set<String> processedIds, Boolean ignoreProcessed) {
		List<SME_Mapping__c> smeMappingsList = new List<SME_Mapping__c>();
		if (ignoreProcessed) {
			smeMappingsList = [SELECT externalValue__c, SFKey__c, variant__c FROM SME_Mapping__c
					WHERE region__c=:userRegion and language__c=:userLanguage and field__c=:fieldType
			and variant__c=:variant and SFKey__c not in :processedIds];
		} else {
			smeMappingsList = [SELECT externalValue__c, SFKey__c, variant__c FROM SME_Mapping__c
					WHERE region__c=:userRegion and language__c=:userLanguage and field__c=:fieldType
					and variant__c=:variant];
		}
		if(!smeMappingsList.isEmpty()) {
			for (SME_Mapping__c smeMapping : smeMappingsList) {
				processedIds.add(smeMapping.SFKey__c);
			}
		}
		smeMappingsList.addAll([SELECT externalValue__c, SFKey__c, variant__c FROM SME_Mapping__c
				WHERE region__c=:userRegion and language__c=:userLanguage and field__c=:fieldType
				and variant__c=:DEFAULT_VARIANT	and SFKey__c not in :processedIds]);

		return smeMappingsList;
	}
	/*
	*Method to process chat settings and return the response
	*@param List<SME_Mapping__c>            Consumes the list of chat setting
	*@return SMEQ_CustomSettingResponsModel Returns SMEQ_CustomSettingResponseModel as response
	*/
	private SMEQ_CustomSettingResponseModel processResponse(List<SME_Mapping__c> settingList) {
		SMEQ_CustomSettingResponseModel smeCustomSettingResponseModel = new SMEQ_CustomSettingResponseModel();
		List<SMEQ_KeyValueModel> smeKeyValueList = new List<SMEQ_KeyValueModel>();
		if (!settingList.isEmpty()) {
			for (SME_Mapping__c setting:settingList) {
				SMEQ_KeyValueModel smeKeyValue = new SMEQ_KeyValueModel();
				smeKeyValue.key = setting.SFKey__c;
				smeKeyValue.value = setting.externalValue__c;
				smeKeyValueList.add(smeKeyValue);
			}
			smeCustomSettingResponseModel.customSettings = smeKeyValueList;
		}
		return smeCustomSettingResponseModel;
	}
	/*
	*Method to do null check for Input parameters
	*@param String fieldType     Value can be  livechat or etc
	*@param String userRegion    Denotes user province
	*@param String userLanguage  Denotes whether the user language is English or French
	*@return Boolean			 Returns true or false after performing null check
	*@param String variant       Values can be FP or WWW (i.e focal point or www or others) or etc
	*/
	private Boolean hasAllParamsPresent(String fieldType, String userRegion, String userLanguage, String variant) {
		if (String.isNotBlank(fieldType) && String.isNotBlank(userRegion) && String.isNotBlank(userLanguage) &&
				String.isNotBlank(variant)) {
			return true;
		} else {
			return false;
		}
	}
}