public class ApplicationLogWrapper {

    public string source {get;set;}
    public string sourceFunction {get;set;}
    public string referenceId {get;set;}
    public string referenceInfo{get;set;}
    public string logMessage {get;set;}
    public string payload {get;set;}
    public string ex {get;set;}
    public string debugLevel {get;set;}
    public DateTime startTime {get;set;}
    public DateTime endTime {get;set;}
    public long timer {get;set;}
    
}