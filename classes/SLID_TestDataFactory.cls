/**
  * @author        Saumil Bapat
  * @date          10/25/2016
  * @description   Data Factory for SLID_ESB_Service_Test
*/
@isTest
public class SLID_TestDataFactory {

    public static boolean throwException = false;
    public static string breakPoint = '';
    public Static void throwTestingException(String methodName)
    {
        if (throwException == true && methodName == breakPoint)
        {
            Exception ex = UTIL_Logging.createMappingException('Test Exception');
            throw ex;
        }
    }


    public Static Final String adminEmail = 'sbapat@salesforce.com';
    //Create Admin User for testing runAs
    public static user adminUser {
        get
        {
            if (SLID_TestDataFactory.adminUser == null)
            {
                adminUser = SLID_TestDataFactory.createTestAdminUser(); 
            }
            return SLID_TestDataFactory.adminUser;
        }
    }
    
    public static user adminUser2 {
        get
        {
            if (SLID_TestDataFactory.adminUser2 == null)
            {
                adminUser2 = SLID_TestDataFactory.createTestAdminUser(); 
            }
            return SLID_TestDataFactory.adminUser2;
        }
    }

    //Create Service Process for test classes
    public static SLID_Mapping_ServiceProcess__c referralProcess {
        get
        {
            if (SLID_TestDataFactory.referralProcess == null)
            {
                SLID_TestDataFactory.createReferralProcess(); 
            }
            return SLID_TestDataFactory.referralProcess;
        }
    }


    public static List<SLID_Mapping_TargetObject__c> referralProcessTargetObjects;

    public static List<SLID_Mapping_TargetField__c> referralProcessTargetFields;

    public static SLID_ESB_Service.CallData referralProcessData {
        get
        {
            if (SLID_TestDataFactory.referralProcessData == null)
            {
                SLID_TestDataFactory.createReferralProcessData();
            }
            return SLID_TestDataFactory.referralProcessData;
        }
    }
    public static Exception_Logging__c exceptionLoggingSettings {
        get
        {
            if(SLID_TestDataFactory.exceptionLoggingSettings == null)
            {
                SLID_TestDataFactory.createExceptionLoggingSettings();
            }
            return SLID_TestDataFactory.exceptionLoggingSettings;
        }
    }
    public static User createTestAdminUser()
    {
        //Create Test User
        Profile SysAdmin = [Select id from Profile where Name = 'System Administrator' limit 1];
        User testAdminUser = new User(  
            firstname = 'testFirstName',
            lastName = 'testLastName',
            email = 'test@test.org',
            Username = 'test@test.org',
            EmailEncodingKey = 'ISO-8859-1',
            Alias ='tuser',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            ProfileId = SysAdmin.Id
        );
        return testAdminUser;
    }

    public static void createReferralProcess()
    {
        //Initialize a referral task service process
        SLID_Mapping_ServiceProcess__c testReferralProcess = new SLID_Mapping_ServiceProcess__c(
                                                                    Name = 'Referral',
                                                                    Source__c = 'ePolicy',
                                                                    Active__c = true);

        insert testReferralProcess; //Insert the test process

        //Update the reference of the static variable
        SLID_TestDataFactory.referralProcess = testReferralProcess;
        
        List<SLID_Mapping_TargetObject__c> testTargetObjects = new List<SLID_Mapping_TargetObject__c>();
        //Initialize Target Object record for the parent case
        SLID_Mapping_TargetObject__c parentCase = new SLID_Mapping_TargetObject__c(
                                                    Name = 'Referral Parent Case',
                                                    Target_Object_API_Name__c = 'Case',
                                                    External_Id__c = 'Case_Number_Ext_Id__c',
                                                    ServiceProcess__c = testReferralProcess.Id);
        testTargetObjects.add(parentCase);

        //Initialize Target Object record for the child case
        SLID_Mapping_TargetObject__c childCase = new SLID_Mapping_TargetObject__c(
                                                    Name = 'Referral Child Case',
                                                    Target_Object_API_Name__c = 'Case',
                                                    External_Id__c = null,
                                                    ServiceProcess__c = testReferralProcess.Id);                                
        testTargetObjects.add(childCase);

        SLID_Mapping_TargetObject__c testOpportunity = new SLID_Mapping_TargetObject__c(
                                                    Name = 'Test Opportunity',
                                                    Target_Object_API_Name__c = 'Opportunity',
                                                    External_Id__c = null,
                                                    ServiceProcess__c = testReferralProcess.Id);                                
        testTargetObjects.add(testOpportunity);

        SLID_Mapping_TargetObject__c testAcccount = new SLID_Mapping_TargetObject__c(
                                                    Name = 'Test Account',
                                                    Target_Object_API_Name__c = 'Account',
                                                    External_Id__c = null,
                                                    ServiceProcess__c = testReferralProcess.Id);                                
        testTargetObjects.add(testAcccount);

        insert testTargetObjects;   //Insert the target objects

        //Update the reference of the static targetObjects variable
        SLID_TestDataFactory.referralProcessTargetObjects = testTargetObjects;


        //Fetch the recordType ids for the target fields
        Map <String,Schema.RecordTypeInfo> targetFieldRecordTypes = SLID_Mapping_TargetField__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id defaultFieldRT = targetFieldRecordTypes.get('Default Value').getRecordTypeId();
        Id mappedFieldRT = targetFieldRecordTypes.get('Mapped Value From Source').getRecordTypeId();

        //List of target fields to create in the test
        List<SLID_Mapping_TargetField__c> testTargetFields = new List<SLID_Mapping_TargetField__c>();

        //bkrCase_Policy__c field on parent case
        SLID_Mapping_TargetField__c parentCasePolicyNumber = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'bkrCase_Policy__c',
                                                                Target_Object__c = parentCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'PolicyNumber',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        parentCase.SLID_Mapping_TargetField__r.add(parentCasePolicyNumber);
        testTargetFields.add(parentCasePolicyNumber);

        //Status field on parent case
        SLID_Mapping_TargetField__c parentCaseStatus = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Status',
                                                                Target_Object__c = parentCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'Status',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        parentCase.SLID_Mapping_TargetField__r.add(parentCaseStatus);
        testTargetFields.add(parentCaseStatus);

        //bkrCase_Subm_Stat_Rsn__c field on parent case
        SLID_Mapping_TargetField__c parentCaseSubmStat = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'bkrCase_Subm_Stat_Rsn__c',
                                                                Target_Object__c = parentCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'StatusComments',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        parentCase.SLID_Mapping_TargetField__r.add(parentCaseSubmStat);
        testTargetFields.add(parentCaseSubmStat);

        //Case_Number_Ext_Id__c field on parent case
        SLID_Mapping_TargetField__c parentCaseExtID = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Case_Number_Ext_Id__c',
                                                                Target_Object__c = parentCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'CaseNumber',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        parentCase.SLID_Mapping_TargetField__r.add(parentCaseExtID);
        testTargetFields.add(parentCaseExtID);

        //Status field on child case
        SLID_Mapping_TargetField__c childCaseStatus = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Status',
                                                                Target_Object__c = childCase.Id,
                                                                recordTypeId = defaultFieldRT,
                                                                Source_fields__c = null,
                                                                Default_Value__c = 'In-Progress',
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        childCase.SLID_Mapping_TargetField__r.add(childCaseStatus);
        testTargetFields.add(childCaseStatus);

        //bkrCase_Subm_Stat_Rsn__c field on child case
        SLID_Mapping_TargetField__c childCaseSubmStat = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'bkrCase_Subm_Stat_Rsn__c',
                                                                Target_Object__c = childCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'StatusComments',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        childCase.SLID_Mapping_TargetField__r.add(childCaseSubmStat);
        testTargetFields.add(childCaseSubmStat);

        //OwnerID field on child case
        SLID_Mapping_TargetField__c childCaseOwner = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'OwnerId',
                                                                Target_Object__c = childCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'BranchCode,ProductCode',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = 'Owner');
        childCase.SLID_Mapping_TargetField__r.add(childCaseOwner);
        testTargetFields.add(childCaseOwner);

        //ParentId field on child case
        SLID_Mapping_TargetField__c childCaseParent = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'ParentId',
                                                                Target_Object__c = childCase.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'CaseNumber',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = 'Case_Number_Ext_Id__c',
                                                                Mapping_Type__c = null);
        childCase.SLID_Mapping_TargetField__r.add(childCaseParent);
        testTargetFields.add(childCaseParent);

        //Name field on Account
        SLID_Mapping_TargetField__c accountName = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Name',
                                                                Target_Object__c = testAcccount.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'AccountName',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(accountName);
        testTargetFields.add(accountName);

        //AccountEmployees field on Account
        SLID_Mapping_TargetField__c accountEmployees = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'NumberOfEmployees',
                                                                Target_Object__c = testAcccount.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'AccountEmployees',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(accountEmployees);
        testTargetFields.add(accountEmployees);

        //AccountRevenue field on Account
        SLID_Mapping_TargetField__c accountRevenue = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'AnnualRevenue',
                                                                Target_Object__c = testAcccount.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'AccountRevenue',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(accountRevenue);
        testTargetFields.add(accountRevenue);

        //AccountPhone field on Account
        SLID_Mapping_TargetField__c accountPhone = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Phone',
                                                                Target_Object__c = testAcccount.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'AccountPhone',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(accountPhone);
        testTargetFields.add(accountPhone);

        //Name field on Opportunity
        SLID_Mapping_TargetField__c opportunityName = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Name',
                                                                Target_Object__c = testOpportunity.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'OpportunityName',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(opportunityName);
        testTargetFields.add(opportunityName);

        //StageName field on Opportunity
        SLID_Mapping_TargetField__c opportunityStage = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'StageName',
                                                                Target_Object__c = testOpportunity.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'OpportunityStage',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(opportunityStage);
        testTargetFields.add(opportunityStage);

        //Amount field on Opportunity
        SLID_Mapping_TargetField__c opportunityAmount = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'Amount',
                                                                Target_Object__c = testOpportunity.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'OpportunityAmount',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(opportunityAmount);
        testTargetFields.add(opportunityAmount);

        //CloseDate field on Opportunity
        SLID_Mapping_TargetField__c opportunityCloseDate = new SLID_Mapping_TargetField__c(
                                                                Target_Field_API_Name__c = 'CloseDate',
                                                                Target_Object__c = testOpportunity.Id,
                                                                recordTypeId = mappedFieldRT,
                                                                Source_fields__c = 'OpportunityCloseDate',
                                                                Default_Value__c = null,
                                                                External_Id_for_Target_Field__c = null,
                                                                Mapping_Type__c = null);
        testAcccount.SLID_Mapping_TargetField__r.add(opportunityCloseDate);
        testTargetFields.add(opportunityCloseDate);

        for (SLID_Mapping_TargetField__c targetField : testTargetFields)
        {
            system.debug('~~~targetField.Target_Object__c: ' + targetField.Target_Object__c);
        }
        insert testTargetFields;    //Insert the target fields

        //Update the reference of the static targetFields variable
        SLID_TestDataFactory.referralProcessTargetFields = testTargetFields;
    }

    //Create the ESB_Service_Request data for the Exception
    public static void createReferralProcessData()
    {
        //Initiate a new webservice request object
        SLID_ESB_Service.CallData testReferralProcessData = new SLID_ESB_Service.CallData();
        testReferralProcessData.source = 'ePolicy';
        testReferralProcessData.processName = 'Referral';
        testReferralProcessData.transactionId = 'TRANS1';

        //Set the source values on the record
        Case testCase1 = SLID_TestDataFactory.createTestCase();
        Case testCase2 = SLID_TestDataFactory.createTestCase();

        //Create a new source record
        SLID_ESB_Service.DataRecord record1 = new SLID_ESB_Service.DataRecord();
        record1.params = new List<SLID_ESB_Service.KeyValPair>();
        record1.params.add(new SLID_ESB_Service.KeyValPair('PolicyNumber', 'COM80001234'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('Status', 'Referred'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('StatusComments', 'Test Commnets for TRANS 1'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('CaseNumber', testCase1.caseNumber));
        record1.params.add(new SLID_ESB_Service.KeyValPair('BranchCode', '1988'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('ProductCode', 'IRC'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('AccountName', 'Test Account Name'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('AccountEmployees', '100'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('AccountRevenue', '1000000'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('AccountPhone', '4163234324'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('OpportunityName', 'Test Opportunity Name'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('OpportunityStage', 'Quoting'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('OpportunityAmount', '10000'));
        record1.params.add(new SLID_ESB_Service.KeyValPair('OpportunityCloseDate', '2016-10-27'));
        
        SLID_ESB_Service.DataRecord record2 = new SLID_ESB_Service.DataRecord();
        record2.params = new List<SLID_ESB_Service.KeyValPair>();
        record2.params.add(new SLID_ESB_Service.KeyValPair('PolicyNumber', 'COM80001235'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('Status', ''));
        record2.params.add(new SLID_ESB_Service.KeyValPair('StatusComments', 'Test Commnets for TRANS 2'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('CaseNumber', testCase2.caseNumber));
        record2.params.add(new SLID_ESB_Service.KeyValPair('BranchCode', '1988'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('ProductCode', 'IRC'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('AccountName', 'Test Account Name 2'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('AccountEmployees', '101'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('AccountRevenue', '1000001'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('AccountPhone', '4163234324'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('OpportunityName', 'Test Opportunity Name'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('OpportunityStage', 'Quoting'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('OpportunityAmount', '10000'));
        record2.params.add(new SLID_ESB_Service.KeyValPair('OpportunityCloseDate', '1999-10-28'));
        
        //Add the record to the request data
        testReferralProcessData.records = new List<SLID_ESB_Service.DataRecord>();
        testReferralProcessData.records.add(record1);
        testReferralProcessData.records.add(record2);

        SLID_TestDataFactory.referralProcessData = testReferralProcessData;
    }

    //Return a test product
    public Static Product2 returnTestProduct()
    {
      Product2 testProduct = new Product2(
                                  IsActive = true,
                                  ProductCode = 'Test Product Code',
                                  Description = 'Test Product Description',
                                  Family = 'Commercial Insurance',
                                  Name = 'Test Product',
                                  CanUseRevenueSchedule = true
                                );
      return testProduct;
    }

    //Return test Account with RT = Prospect
    public Static Account returnTestAccount()
    {
      //Fetch the recordType ids for the Account
      Map <String,Schema.RecordTypeInfo> accountRecordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id prospectRT = accountRecordTypes.get('Prospect').getRecordTypeId();
     
      Account testAccount = new Account(
                                    Name = 'Test Account',
                                    recordTypeId = prospectRT
                                  );
      return testAccount;
    }

    //Return a test Contact with RT = Broker
    public Static Contact returnTestContact()
    {
      //Fetch the recordType ids for the Account
      Map <String,Schema.RecordTypeInfo> contactRecordTypes = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id brokerRT = contactRecordTypes.get('Broker').getRecordTypeId();
     
      Contact testContact = new Contact(
                                    LastName = 'Test Contact',
                                    recordTypeId = brokerRT
                                  );
      return testContact;
    }

    //Create a test case with RT = CI New Business
    public static Case createTestCase()
    {
         //Fetch the recordType ids for the Case
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id newBusinessRT = caseRecordTypes.get('CI New Business').getRecordTypeId();
        
        Case testCase = new Case (
                                    Subject='Test Case Subject',
                                    Description = 'Test Case Description',
                                    Status='New',
                                    RecordTypeId = newBusinessRT
                                    );
        insert testCase;
        testCase = [select id, description, subject, caseNumber from case where id = :testCase.Id];
        System.Debug('~~~New Created testCase: ' + testCase);
        return testCase;
    }

    //Return a test case with RT = CI New Business
    public static Case returnTestNewBusinessCase()
    {
         //Fetch the recordType ids for the Case
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id newBusinessRT = caseRecordTypes.get('CI New Business').getRecordTypeId();
        
        Case testCase = new Case (
                                    Subject='Test Case Subject',
                                    Description = 'Test Case Description',
                                    Status='New',
                                    RecordTypeId = newBusinessRT
                                    );
        return testCase;
    }

    //Return a test case with RT = CI Referral
    public static Case returnTestReferralCase()
    {
        //Fetch the recordType ids for the Case
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id referralRT = caseRecordTypes.get('CI Referral').getRecordTypeId();
        
        Case testCase = new Case (
                                    Subject='Test Case Subject',
                                    Description = 'Test Case Description',
                                    Status='New',
                                    RecordTypeId = referralRT
                                    );
        System.Debug('~~~New Created testCase: ' + testCase);
        return testCase;
    }

    //Create and insert a quote version and it's parent case
    public static Quote_Version__c createQuoteVersion()
    {
      Case parentCase = SLID_TestDataFactory.createTestCase();
      Quote_Version__c quoteVersion = new Quote_Version__c();
      quoteVersion.Quote_Number__c = '12345';
      quoteVersion.Premium__c = 12345.00;
      quoteVersion.Primary__c = true;
      quoteVersion.Quote_Date__c = Date.Today();
      quoteVersion.Status__c = 'New';
      quoteVersion.Case__c = parentCase.Id;
      insert quoteVersion;
      return quoteVersion;
    }

    //Return a test quote version without inserting it
    public static Quote_Version__c returnQuoteVersion()
    {
      Quote_Version__c quoteVersion = new Quote_Version__c();
      quoteVersion.Quote_Number__c = '12345';
      quoteVersion.Premium__c = 12345.00;
      quoteVersion.Primary__c = true;
      quoteVersion.Quote_Date__c = Date.Today();
      quoteVersion.Status__c = 'New';
      return quoteVersion;
    }

    public static Exception_Logging__c createExceptionLoggingSettings()
    {
        Exception_Logging__c testExceptionLoggingSettings;
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs( thisUser ){
            //Initialize exception logging custom setting
            testExceptionLoggingSettings = new Exception_Logging__c(
                                                    Exception_Description_Max__c = 255,
                                                    Exception_Details_Max__c = 255,
                                                    Exception_Logs_Purge_Days__c = 30,
                                                    Logging_Enabled__c = true,
                                                    Info__c = true,
                                                    Warning__c = true,
                                                    Debug__c = true,
                                                    Error__c = true
                                                );
            //Insert exception logging setting
            insert testExceptionLoggingSettings;
        }
        //Return custom exceptino setting
        return testExceptionLoggingSettings;
    }
    
    public static Integration_Logging__c createIntegrationLoggingSettings()
    {
      //Create integration log custom setting record
      Integration_Logging__c testIntegrationLoggingSettings = new Integration_Logging__c(
                                                                    Integration_Logs_Purge_Days__c = 30,
                                                                    Logging_Enabled__c = true,
                                                                    Pay_Load_Max__c = 100000
                                                                  );

      //insert Integration_Logging__c
      insert testIntegrationLoggingSettings;

      //Return custom exceptino setting
      return testIntegrationLoggingSettings;
    }
    public static void initializeIntegrationLogId()
    {
      SLID_TestDataFactory.createIntegrationLoggingSettings();
      SLID_TestDataFactory.createExceptionLoggingSettings();
      //Set Integration Log Id
      Map <String,Schema.RecordTypeInfo> integrationLogRecordTypes = Integration_Log__c.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id inboundRT = integrationLogRecordTypes.get('INBOUND').getRecordTypeId();
      UTIL_Logging.integrationLogId = UTIL_Logging.createIntegrationLog(
                                            inboundRT, 
                                            'Test Payload', 
                                            'Test Process Name', 
                                            'Test Source Name', 
                                            'Test Transaction Id'
                                        );
    }
}