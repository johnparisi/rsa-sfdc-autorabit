/**
* @File Name    :   ActuarialDetailTriggerHandler
* @Description  :   Actuarial Detail Object Handler class
* @Date Created :   12/12/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Trigger Handler
* @Modification Log:
**************************************************************************************
* Ver       Date        Author           Modification
* 1.0       12/12/2017  Habiba Zaman  Created the file/class
* 
* The new Object Handler classes will follow this pattern.
* 
* It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
* Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
* 
* No need to update this unless adding a business unit or record types. 
*/

public with sharing class ActuarialDetailTriggerHandler {

	private List<Actuarial_Detail__c> actuarialDetails = new List<Actuarial_Detail__c>();


	public void beforeInsert(List<Actuarial_Detail__c> records){
		new ActuarialDetailDomain().beforeInsert(records);
    }

    public void beforeUpdate(List<Actuarial_Detail__c> records, Map<Id, Actuarial_Detail__c> oldRecords){
 		new ActuarialDetailDomain().beforeUpdate(records, oldRecords);
    }

    public void afterInsert(List<Actuarial_Detail__c> records){
    	new ActuarialDetailDomain().afterInsert(records);
    }
    
    public void afterUpdate(List<Actuarial_Detail__c> records, Map<Id, Actuarial_Detail__c> oldRecords){
    	new ActuarialDetailDomain().afterUpdate(records, oldRecords);    
    }
}