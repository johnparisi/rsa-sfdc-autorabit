@isTest
public class SMEQ_ResourceBundlesDao_Test {
	
    /**
    * This method is to test to get all resource bundles when default variant and bundle are not provided
    * 
    * @Assertions;
    * 1. Based on the data setup there should be 5 records
    **/
    static testMethod void testQueryResourceBundlesWithoutVariantAndBundle(){
        SMEQ_ResourceBundlesDao resourceBundlesDao = new SMEQ_ResourceBundlesDao();
        SMEQ_ResourceBundlesModel smeResourceBundles = new SMEQ_ResourceBundlesModel();
        generateResourceBundlesTestData();
        smeResourceBundles = resourceBundlesDao.getResourceBundles('www',null);
        //List<SMEQ_RESOURCE_BUNDLES__c> bundleList = resourceBundlesDao.queryResourceBundles(null,null);
        System.assertEquals(5, smeResourceBundles.resourceBundleEnglish.size());
    }


     /**
    * This method is to test to get all resource bundles when default variant and bundle are not provided
    * 
    * @Assertions;
    * 1. Based on the data setup there should be 4 records
    **/
    static testMethod void testQueryResourceBundlesWithVariantAndNoRegion(){
        SMEQ_ResourceBundlesDao resourceBundlesDao = new SMEQ_ResourceBundlesDao();
        SMEQ_ResourceBundlesModel smeResourceBundles = new SMEQ_ResourceBundlesModel();
        generateResourceBundlesTestData();
        smeResourceBundles = resourceBundlesDao.getResourceBundlesByVariantAndRegion('variant1',null);
        //List<SMEQ_RESOURCE_BUNDLES__c> bundleList = resourceBundlesDao.queryResourceBundles(null,null);
        System.assertEquals(4, smeResourceBundles.resourceBundleEnglish.size());
    }


    /**
    * This method is to test to get all resource bundles when default variant and bundle are not provided
    * 
    * @Assertions;
    * 1. Based on the data setup there should be 4 records
    **/
    static testMethod void testQueryResourceBundlesWithVariantAndRegion(){
        SMEQ_ResourceBundlesDao resourceBundlesDao = new SMEQ_ResourceBundlesDao();
        SMEQ_ResourceBundlesModel smeResourceBundles = new SMEQ_ResourceBundlesModel();
        generateResourceBundlesTestData();
        smeResourceBundles = resourceBundlesDao.getResourceBundles('variant1',null, 'BC');
        //List<SMEQ_RESOURCE_BUNDLES__c> bundleList = resourceBundlesDao.queryResourceBundles(null,null);
        System.assertEquals(4, smeResourceBundles.resourceBundleEnglish.size());
    }
    
    /**
    * This method is called to setup the resource bundles test data.
    *
    **/
    static public void generateResourceBundlesTestData(){
        List<SMEQ_RESOURCE_BUNDLES__c> resBundlesList = new List<SMEQ_RESOURCE_BUNDLES__c>();
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST for bundle 1', 'www', 'test.label',null));
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST 1 for bundle 1', 'www', 'test.label1',null));
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST 2 for bundle 2', 'www', 'test.label2',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST for bundle 2', 'www', 'test.label',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST for bundle 2', 'www', 'test.label1',null));
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'EN', 'TEST Variant for bundle 1', 'variant1', 'test.label2',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST Variant for bundle 2', 'variant1', 'test.label',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'EN', 'TEST Variant for bundle 2', 'variant1', 'test.label','BC'));
        
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST for bundle 1_FR', 'www', 'test.label',null));
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST 1 for bundle 1_FR', 'www', 'test.label1',null));
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST 2 for bundle 2_FR', 'www', 'test.label2',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'FR', 'TEST for bundle 2_FR', 'www', 'test.label',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'FR', 'TEST for bundle 2_FR', 'www', 'test.label1',null));
        resBundlesList.add(insertResourceBundles('Bundle1', 'CA', 'FR', 'TEST Variant for bundle 1_FR', 'variant1', 'test.label2',null));
        resBundlesList.add(insertResourceBundles('Bundle2', 'CA', 'FR', 'TEST Variant for bundle 2_FR', 'variant1', 'test.label',null));
        insert resBundlesList;
    }
    
    /**
    * This is a helper method to create SMEQ_Resource_Bundles objects based on the arguments.
    */
    static SMEQ_RESOURCE_BUNDLES__c insertResourceBundles (string bundleId, string countryCode, string languageCode, string value, string variant, string stringId, String region){
        SMEQ_RESOURCE_BUNDLES__c smeResourceBundles = new SMEQ_RESOURCE_BUNDLES__c();
        smeResourceBundles.bundle__c = bundleId;
        smeResourceBundles.country_code__c = countryCode;
        smeResourceBundles.language_code__c = languageCode;
        smeResourceBundles.value__c = value;
        smeResourceBundles.variant__c = variant;
        smeResourceBundles.string_id__c = stringId;
        smeResourceBundles.Region__c = region;
        return smeResourceBundles;
    }

    
}