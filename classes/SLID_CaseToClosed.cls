/**
  * @author        Saumil Bapat
  * @date          11/16/2016
  * @description   Helper class to close the parent case on quote
  * @Process       Invoked on before update
*/
public with sharing class SLID_CaseToClosed {
  //create a static variable for declined comments mapping Type
  public Static Final String DECLINED_COMMENT_MAPPING_TYPE = 'Reason';
  
  public static void updateStatusToClosed(List<Quote_Version__c> oldQuotes, List<Quote_Version__c> newQuotes)
  {
    System.Debug('~~~updateStatusToClosed');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_CaseToClosed: ' + Limits.getQueries());
    Map<Id, Quote_Version__c> oldQuoteMap = new Map<Id,Quote_Version__c>();
    if(oldQuotes != null) {
      for (Quote_Version__c oldQuote : oldQuotes)
        {
          oldQuoteMap.put(oldQuote.Id, oldQuote);
        }
    }

    //Loop through the parent quotes and get the parent case IDs & Close Comments
    Set<Id> parentCaseIDs = new Set<Id>();
    List<String> declinedComments = new List<String>();
    for (Quote_Version__c quoteVersion : newQuotes)
    {
      //add the id of the parent case
      parentCaseIds.add(quoteVersion.Case__c);

      //Add the mapping filter value to initialize MappingUtils
      declinedComments.add(
        quoteVersion.Declined_Closed_Reason__c + '_' + DECLINED_COMMENT_MAPPING_TYPE
      );
    }

    //Instantiate the mapping for declined Comments
    SLID_ESB_MappingUtils.instantiateWithFilterValues(declinedComments);
    
    //Query the parent cases with child quoteVersions
    Map<Id,Case> parentCaseMap = new Map<Id,Case>();
    parentCaseMap = new Map<Id,Case>([Select Id, Status, (Select Id from Policy_Quotes__r where Active__c = true) from Case where Id IN :parentCaseIDs]);

    //Initialize of list of cases which will need to be updated 
    Map<Id,Case> casesToBeUpdated = new Map<Id,Case>();
    
    //Loop through the queries cases
    for (Quote_Version__c newQuote : newQuotes)
    {
      Quote_Version__c oldQuote;
      if(!oldQuoteMap.isEmpty()){
        oldQuote = oldQuoteMap.get(newQuote.Id);
      }
      if ((oldQuote != null && oldQuote.Active__c == true && newQuote.Active__c == false) || newQuote.Active__c == false)
      {
        System.Debug('~~~newQuote: ' + newQuote);
        Case parentCase = parentCaseMap.get(newQuote.Case__c);
        System.Debug('~~~parentCase.Policy_Quotes__r.size(): ' + parentCase.Policy_Quotes__r.size());
        if(parentCase.Policy_Quotes__r.size() == 0)
        {
          System.Debug('~~~Setting status to closed. Case: ' + parentCase);
          //Set the case status to closed
          parentCase.Status = 'Closed';
            
          //Set the case close reason
          String caseCloseReason = SLID_ESB_MappingUtils.getMappedValue(
            newQuote.Declined_Closed_Reason__c , DECLINED_COMMENT_MAPPING_TYPE 
          );
          parentCase.bkrCase_Close_Reason__c = caseCloseReason;

          //Add parent case to the list to be updated
          casesToBeUpdated.put(parentCase.Id,parentCase);
        }
      }
    }

    if (casesToBeUpdated.values().size() != 0)
    {
      System.Debug('~~~updating parent Cases: ' + casesToBeUpdated);
      update casesToBeUpdated.values();
    }
    System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_CaseToClosed: ' + Limits.getQueries());
  }
}