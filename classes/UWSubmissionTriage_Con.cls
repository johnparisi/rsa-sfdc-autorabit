public with sharing class UWSubmissionTriage_Con {

    private static final String CASE_RECORDTYPE_OPEN_SPRNT = 'CI_Open_SPRNT';
    
    public Case c;
    
    public String Name { get; set; }
    public String Address { get; set; }
    public String City { get; set; }
    public String State { get; set; }
    public String PostalCode { get; set; }
    public String CountryISOCode { get; set; }
    public Boolean refreshPage {get; set; }
    public Boolean closePage {get; set; }
    public List<DNBCallout.dnbEntry> results { get; set; }
    public DNBCallout.dnbEntry record { get; set; }
    
    public String myDUNS { get; set; }
    public String queueId { get; set; } 
    
    public boolean smeDNB { get; set; }
    
    public Flow.Interview.CI_SME_Triage_Flow myflow { get; set; }
    public class UWSubmissionTriage_ConException extends Exception {}

    // PWP-401 starts
    // String variable to make an encoded URL since parameters can have values with special characters
    public String strCreateInsuredCLientEncodedURL {get; set;}
    // PWP-401 ends
    
    public boolean getvarCompleteTriageProcess() {
        if (myflow == null) { 
            return null; 
        }
        else {
            return (boolean)myflow.varCompleteTriageProcess && !isEligible();
        }
    }
    
    /*
     * This function determines whether or not an UW is eligible to handle a triaged case. 
     * If the UW is eligible, the case tab should not be closed and the UW returned back to the case list view screen.
     */
    @testVisible
    private boolean isEligible()
    {
        Case c = [
            SELECT Id, UW_Referral_Level__c, SIC_Code_Referral_Level__c, SME_Region_Enabled__c, recordTypeId
            FROM Case
            WHERE Id = :c.Id
        ];
        Integer caseLevel = c.SIC_Code_Referral_Level__c == null ? 0 : Integer.valueOf(c.SIC_Code_Referral_Level__c.right(1));
        Integer uwLevel = c.UW_Referral_Level__c == null ? 0 : Integer.valueOf(c.UW_Referral_Level__c.right(1));
        Map<String,Id> caseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);
        
        if (c.recordtypeId == caseTypes.get('CI_Open_SPRNT') &&
            c.SME_Region_Enabled__c &&
            uwLevel >= caseLevel)
        {
            return true;
        }
        
        return false;
    }
    
    public UWSubmissionTriage_Con(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {  
            controller.addFields(new List<String>{'RecordTypeId','bkrCase_SIC_Code__c','bkrCase_Insured_Client__r.Sic','bkrCase_Insured_Client__r.SicDesc','OwnerId','Insured_Client_Name__c','Insured_Client_Address__c','Insured_Client_State__c','Insured_Client_City__c','Insured_Client_Postal_Code__c'});
        }
        c = (Case)controller.getRecord(); 
        /*c = [
            SELECT id, RecordTypeId, bkrCase_SIC_Code__c, bkrCase_Insured_Client__r.Sic, bkrCase_Insured_Client__r.SicDesc, OwnerId
            FROM Case
            WHERE id = :controller.getRecord().id
        ];*/
        results = new List<DNBCallout.dnbEntry>();
        Map<String, Object> myMap = new Map<String, Object>();
        myflow = new Flow.Interview.CI_SME_Triage_Flow(myMap); 
        
        Map<String, Id> CaseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);
        if (CaseTypes.get(CASE_RECORDTYPE_OPEN_SPRNT) == c.RecordTypeId)
        {
            smeDNB = true;
        }

        // PWP-209 - Starts
        if(c.Insured_Client_Name__c != NULL)
            Name = c.Insured_Client_Name__c;
        if(c.Insured_Client_Address__c != NULL)
            Address = c.Insured_Client_Address__c;
        if(c.Insured_Client_City__c != NULL)
            City = c.Insured_Client_City__c;
        if(c.Insured_Client_State__c != NULL)
            State = c.Insured_Client_State__c;
        if(c.Insured_Client_Postal_Code__c != NULL)
            PostalCode =c.Insured_Client_Postal_Code__c;
        // PWP-209 - Ends

        // PWP-401 - Starts
        // Encoding of the URL is introduced to handle special characters coming as part of the data present in URL parameters
        // This code is implemented for Policy Works implementation only
        Map<String, Id> AccountRecordTypes = Utils.getRecordTypeIdsByDeveloperName(Account.SObjectType, true);
        String policyHolderrecordTypeId = AccountRecordTypes.get('Policyholder');
        //String policyHolderrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Policyholder').getRecordTypeId();

        String encodedName; String encodedAddress; String encodedCity; String encodedState; String encodedPostalCode;
        if(Name != NULL)
            encodedName = EncodingUtil.urlEncode(Name,'UTF-8');
        if(Address != NULL)
            encodedAddress = EncodingUtil.urlEncode(Address,'UTF-8');
        if(City != NULL)
            encodedCity = EncodingUtil.urlEncode(City,'UTF-8');
        if(State != NULL)
            encodedState = EncodingUtil.urlEncode(State,'UTF-8');
        if(PostalCode != NULL)
            encodedPostalCode = EncodingUtil.urlEncode(PostalCode,'UTF-8');
        //strCreateInsuredCLientEncodedURL = '/001/e?RecordType='+policyHolderrecordTypeId+'&acc2='+encodedName+'&acc17street='+encodedAddress+'&acc17city='+encodedCity+'&acc17state='+encodedState+'&acc17zip='+encodedPostalCode+'&ent=Account&saveURL=%2Fapex%2FCI_NewAccountSaveRedirect%3Ffield%3DbkrCase_Insured_Client__c';
        strCreateInsuredCLientEncodedURL = '/001/e?RecordType='+policyHolderrecordTypeId;
        if(encodedName != null && encodedName != ''){
            strCreateInsuredCLientEncodedURL += '&acc2=';
            strCreateInsuredCLientEncodedURL += encodedName;
        }
        if(encodedAddress != null && encodedAddress != ''){
            strCreateInsuredCLientEncodedURL += '&acc17street=';
            strCreateInsuredCLientEncodedURL += encodedAddress;
        }
        if(encodedCity != null && encodedCity != ''){
            strCreateInsuredCLientEncodedURL += '&acc17city=';
            strCreateInsuredCLientEncodedURL += encodedCity;
        }
        if(encodedState != null && encodedState != ''){
            strCreateInsuredCLientEncodedURL += '&acc17state=';
            strCreateInsuredCLientEncodedURL += encodedState;
        }
        if(encodedPostalCode != null && encodedPostalCode != ''){
            strCreateInsuredCLientEncodedURL += '&acc17zip=';
            strCreateInsuredCLientEncodedURL += encodedPostalCode;
        }
        strCreateInsuredCLientEncodedURL += '&ent=Account&saveURL=%2Fapex%2FCI_NewAccountSaveRedirect%3Ffield%3DbkrCase_Insured_Client__c';
        // PWP-401 - Ends
    }

    public void searchDnB(){
        results = new List<DNBCallout.dnbEntry>();
        // ***
        // TODO: add logic to return error in pagemessage if:
        //      Name == null or ''
        //      Return custom label:  "Company Name is required for DnB search"
        // *** 
        /* START : RSA-174 : Updated By Trekbin 
           Checking if Name is null then display error message on page else call the method*/ 
        if(Name == null || Name == '')
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Company Name is required for DnB search'));
        else
            try{
                results = DNBCallout.LookupByCompanyName(Name,CountryISOCode,Address,City,State,PostalCode,null);
                 if(Test.isRunningTest())
                    Integer i = 10/0;
            }
            
                catch (exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                }
            
        /* END : RSA-174 : Updated By Trekbin */
    }    
    
    public void searchSmeDnB()
    {
        results = new List<DNBCallout.dnbEntry>();
        
        if(Name == null || Name == '')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Company Name is required for DnB search'));
        }
        else
        {
            try{
                SMEQ_DnBRequestModel smereq = new SMEQ_DnBRequestModel();
                smereq.name = Name;
                smereq.streetAddress = Address;
                smereq.city = City;
                smereq.state = State;
                smereq.countryCode = CountryISOCode;
                smereq.postalCode = PostalCode;
                
                SMEQ_DnBCallOut sdbc = new SMEQ_DnBCallOut();
                SMEQ_DnBResponseModel smeres = sdbc.processDnBServiceCallOutRequest(smereq);
                List<SMEQ_DnBResponseModel.DnBEntry> resultList = smeres.payload;
                
                for (SMEQ_DnBResponseModel.DnBEntry smede : resultList)
                {
                    DNBCallout.dnbEntry de = new DNBCallout.dnbEntry();
                    de.Address = smede.streetAddress;
                    de.City = smede.city;
                    de.State = smede.province;
                    de.Zipcode = smede.postalCode;
                    de.DUNSNumber = smede.DUNSNumber;
                    de.Company = smede.organizationName;
                    
                    results.add(de);
                }
            }
            catch (exception e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
    }
    
    public void selectRecordAction(){
        
        refreshPage = false;
        
        /* RSA-174 : Updated By Trekbin 
           Query record type 'Policyholder' for mapping */
        List<RecordType> lstRecordType = new List<RecordType>([Select Id From RecordType  Where SobjectType = 'Account' and RecordType.Name = 'Policyholder' LIMIT 1]); 
        
        for(DNBCallout.dnbEntry r : results){
            if(r.DUNSNumber  == myDUNS){
                // ***
                // FUTURE PHASE: can include Firmographics lookup to get additional information from DnB
                // ***                  
                //record = DNBCallout.lookupCompanyFirmographics(r.DUNSNumber);
                
                Account a = new Account();
                // ***
                // TODO: need to set the RecordTypeId from a SOQL query
                // use Account.RecordType.Name = 'Policyholder'
                // *** 
                /* RSA-174 : Updated By Trekbin 
                   If recordtype list is not null then assign the id */ 
                if(!lstRecordType.isEmpty())                   
                    a.RecordTypeId = lstRecordType[0].Id; //'012o0000000x3vq';
                //
                a.Name = r.Company;
                a.bkrAccount_DUNS__c = r.DUNSNumber;
                a.BillingStreet = r.Address;
                a.BillingCity = r.City;
                a.BillingState = r.State;
                a.BillingPostalCode = r.ZipCode;
                a.Phone = r.Phone;
                // ***
                // FUTURE PHASE: need to map the fields from DnB results to Account fields (create new) per Jay's spreadsheet
                // ***                
                //record.LocationType = 'Headquarters'; 
                /*try{
                    a.AnnualRevenue = decimal.valueOf(record.AnnualSalesUSDollars);
                } catch(exception e) {
                    System.debug('Conversion error: '+ e);
                }  
                try{
                    a.NumberOfEmployees = integer.valueOf(record.EmployeesTotal);
                } catch(exception e) {
                    System.debug('Conversion error: '+ e);
                }                    
                //EmployeesHere                
                //CompanyStartYear
                //SingleLocation
                a.SicDesc = record.LineOfBusiness;
                a.Sic = record.IndustryCode1;
                // ***
                // FUTURE PHASE TODO: need to query the correct SIC Code and assign to Case if found
                // ***  
                */
                try{
                    insert a;
                }catch (exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    return;
                }  
                // assign the added Account to the Insured Client field
                c.bkrCase_Insured_Client__c = a.Id;
                update c;
                
                // ***
                // FUTURE PHASE: can immediately query Policy history
                // ***                  
                //queryPolicyResults(r.Company);
                refreshPage = true;
                return;
            }
        }
        //return null; 
    }
    
    public void selectSmeRecordAction()
    {
        refreshPage = false;
        
        List<RecordType> lstRecordType = new List<RecordType>(
            [
                SELECT Id
                FROM RecordType
                WHERE SobjectType = 'Account'
                AND RecordType.Name = 'Policyholder'
                LIMIT 1
            ]
        ); 
        
        for (DNBCallout.dnbEntry r : results)
        {
            if (r.DUNSNumber == myDUNS)
            {
                List<Account> accts = [
                    SELECT id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, bkrAccount_DUNS__c
                    FROM Account
                    WHERE bkrAccount_DUNS__c = :r.DUNSNumber
                    LIMIT 1
                ];
                
                Account a;
                if (accts.size() == 0) // No such DUNS Number in record
                {
                    a = new Account();
                
                    if (!lstRecordType.isEmpty())
                    {
                        a.RecordTypeId = lstRecordType[0].Id;
                    }
                    a.Name = r.Company;
                    a.bkrAccount_DUNS__c = r.DUNSNumber;
                    a.BillingStreet = r.Address;
                    a.BillingCity = r.City;
                    a.BillingState = r.State;
                    a.BillingPostalCode = r.ZipCode;
                    
                    try
                    {
                        insert a;
                    }
                    catch (exception e)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        return;
                    }
                }
                else
                {
                    a = accts[0];
                    a.Name = r.Company;
                    a.BillingStreet = r.Address;
                    a.BillingCity = r.City;
                    a.BillingState = r.State;
                    a.BillingPostalCode = r.ZipCode;
                    
                    try
                    {
                        upsert a;
                    }
                    catch (exception e)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        return;
                    }
                }
                
                // assign the added Account to the Insured Client field
                c.bkrCase_Insured_Client__c = a.Id;

                // PWP-209 - Starts
                c.Insured_Client_Name__c = NULL;
                c.Insured_Client_Address__c = NULL;
                c.Insured_Client_City__c = NULL;
                c.Insured_Client_State__c = NULL;
                c.Insured_Client_Postal_Code__c = NULL;
                // PWP-209 - Ends

                update c;
                
                refreshPage = true;
                return;
            }
        }
    }   
    
    public List<SelectOption> getProductList(){
        List<SelectOption> listOptions = new List<SelectOption>();
        for(Product2 q : [SELECT Id,Name,ProductCode 
                                FROM Product2 
                                WHERE Family =  'Commercial Insurance' 
                         ]){
            listOptions.add(new SelectOption(q.Id,q.Name));
        }
        return listOptions;
    }
    
    // Removed 03.01.2016
    /*public List<SelectOption> getQueueSobjectList(){
        List<SelectOption> listOptions = new List<SelectOption>();
        for(QueueSobject q : [SELECT QueueId,Queue.DeveloperName,Queue.Name 
                                FROM QueueSobject 
                                WHERE (Queue.DeveloperName like 'Ontario_CI_%' 
                                OR Queue.DeveloperName like 'Ontario_IRC_%'
                                OR Queue.DeveloperName like 'Ontario_MM_%')
                                and SobjectType = 'Case'
                                ORDER BY Queue.DeveloperName]){
            listOptions.add(new SelectOption(q.QueueId,q.Queue.Name));
        }
        return listOptions;
    }*/
    
    public List<SelectOption> getRecordTypeList(){
        List<SelectOption> listOptions = new List<SelectOption>();
        for(RecordType r : [SELECT Id,Name
                                FROM RecordType 
                                WHERE (Description like 'Commercial Insurance -%' 
                                AND (NOT Name like 'CI_Tri%'))
                                and SobjectType = 'Case'
                                ORDER BY Name]){
            listOptions.add(new SelectOption(r.Id,r.Name));
        }
        return listOptions;
    }    
    /*public void assignQueue(){
        ID currentOwner = c.OwnerId;
        refreshPage = false;
        closePage = false;      
        if(queueId!=null && queueId!=''){
            c.OwnerId = queueId;
            try{
                update c;
                Case c1 = [Select Id, OwnerId from Case where Id =: c.Id];
                if(c1.OwnerId != currentOwner)
                    closePage = true;           
                refreshPage = true;         
            }catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your email was not forwarded. Please forward the original email manually'));
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }       
        }
    } */   
    public void assignSubmType(){
        refreshPage = false;
        closePage = false;
        ID currentOwner = c.OwnerId;
        // use the built-in Case Assignment rules
        try{    
            Database.DMLOptions dmo = new Database.DMLOptions();
            /* START : RSA-174 : Updated By Trekbin 
               Query AssignmentRule and check the list length, if its not null then assign the id of 'CI Triage Assignment Rules' '*/
            // Removed 03.01.2016
            //List<AssignmentRule> lstAssingmentRule = new List<AssignmentRule>([select id from AssignmentRule where SobjectType = 'Case' and Name = 'CI Triage Assignment Rules' limit 1]);
            //if(!lstAssingmentRule.isEmpty())
            //    dmo.assignmentRuleHeader.assignmentRuleId = lstAssingmentRule[0].Id; //'01Q170000000Pa4';
            //else
                dmo.assignmentRuleHeader.useDefaultRule = true; 
            /* END : RSA-174 : Updated By Trekbin */
            c.setOptions(dmo);
            update c;
            Case c1 = [Select Id, OwnerId from Case where Id =: c.Id];
            if (c1.OwnerId != currentOwner)
            {
                closePage = !isEligible();
            }
            refreshPage = true;
            
            if(Test.isRunningTest())
                    Integer i = 10/0;
                    
        }catch(exception e){
            // ***
            // SFDC-11 - make error messages generic since we aren't forwarding emails anymore
            // ***      
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your email was not forwarded. Please forward the original email manually'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            
            // Removed 03.01.2016
            //Database.DMLOptions dmo = new Database.DMLOptions();
            //c.setOptions(dmo);
            //try{
                //update c;
                ////Case c1 = [Select Id, OwnerId from Case where Id =: c.Id];
                ////if(c1.OwnerId != currentOwner)
                ////  closePage = true;           
                ////refreshPage = true;
            //}catch (exception ex){
            //        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            //        return;
            //}  
        }
        
    }        
    public void updateRecordType()
    {
        // RecordTypeId set by picklist in page
        system.debug(c.RecordTypeId);
        try{
            if(c.RecordTypeId!=null)
                update c;
        }catch (exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }            
    }    
    
    
    // ***
    // FUTURE PHASE: query Policy history
    // ***         
    /*public void queryPolicyAction(){
        queryPolicyResults(policySearchValue);
        if(c.bkrCase_Policies_Search_Performed__c != true){
            c.bkrCase_Policies_Search_Performed__c = true;
            update c;
        }
    }
    public void queryPolicyResults(String policySearchValue){
        String searchquery='FIND \'' + String.escapeSingleQuotes(policySearchValue) +'\' IN ALL FIELDS RETURNING Policy__c(Address__c,Broker__c, Broker__r.Name,Client_Name__c,Client_Account__c, Client_Account__r.Name, Effective_Date__c,Line_of_Business__c,Name,Policy_Status__c,Type__c),Contact, Lead'; 
        List<List<SObject>> results =search.query(searchquery);        
        policyList = new List<PolicyResult>();
        PolicyResult pr = new PolicyResult();
        for(SObject r : results[0]){
            pr.obj = r;
            policyList.add(pr);
        }
        system.debug(policyList);        
    }
    public void assignPolicyAction(){
        List<Policy__c> updatePolicyList = new List<Policy__c>();
        for(PolicyResult r : policyList){
            if(r.selected){  
                r.obj.put('Client_Account__c',c.bkrCase_Insured_Client__c); 
                updatePolicyList.add((Policy__c)r.obj);
            }
        }    
        update updatePolicyList;
    }
    public class policyResult{ 
        public Boolean selected{ get; set; }
        public SObject obj{ get; set; }
    }*/ 
}