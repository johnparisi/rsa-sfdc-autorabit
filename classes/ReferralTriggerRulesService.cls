/**
 * ReferralTriggerRulesService
 * Author: James Lee
 * Date: January 20 2017
 * Feature: FP-518
 * 
 * The purpose of this service class is to manage referrals related to the SME quoting tool.
 */
public class ReferralTriggerRulesService
{
    
    /**
     * @return: a list of ReferralTriggerRules classes that are defined for a given quoteRequest.
     */
    public static List<ReferralTriggerRules__c> getReferralTriggerClasses(SMEQ_QuoteRequest quoteRequest, Case caseToUpsert)
    {
        List<String> offerings = new List<String>{caseToUpsert.Offering_Project__c};
        Map<String, List<ReferralTriggerRules__c>> offering2rtrMap;
        List<ReferralTriggerRules__c> staticQuestionRTRs;
        if (offerings != null)
        {
            offering2rtrMap = getReferralTriggerClassesForOffering(offerings, false);
            staticQuestionRTRs = offering2rtrMap.get(caseToUpsert.Offering_Project__c);
        }
        
        // Get referral trigger rules for dynamic questions.
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicDetailsBySicProjectRegion(caseToUpsert.Offering_Project__c, caseToUpsert.bkrCase_Region__c, caseToUpsert.bkrCase_SIC_Code__c);
        if (scdvs.isEmpty())
        {
            throw new ReferralTriggerRulesServiceException();
        }
        
        SIC_Code_Detail_Version__c scdv = scdvs[0];
        List<SIC_Question__c> sqs = SicQuestionService.getSicQuestions(scdv);
        
        List<ReferralTriggerRules__c> dynamicQuestionRTRs = getReferralTriggerClassesForQuestion(sqs, false);
        
        return getUniqueReferralTriggerRules(staticQuestionRTRs, dynamicQuestionRTRs);
    }
    
    //Added for Renewals and Amendments 
    
    public static Map<Id, List<ReferralTriggerRules__c>> getReferralTriggerClasses(List<Case> caseToUpsert)
    {
     Map<Id, List<ReferralTriggerRules__c>> rMap = new Map<Id, List<ReferralTriggerRules__c>>(); 
      
      //loop through the cases
     for (Case c : caseToUpsert)
     {
       List<String> offerings = new List<String>{c.Offering_Project__c}; // the case can have multiple offerings
       Map<String, List<ReferralTriggerRules__c>> offering2rtrMap; //map the the specific offering to a list of trigger rules 
       List<ReferralTriggerRules__c> referralTriggerReasonsByOffering; 
    
     	if (offerings != null)
        {
            offering2rtrMap = getReferralTriggerClassesForOffering(offerings, false); //this returns a map with the offering and the the list of rules associated with the offering 
            referralTriggerReasonsByOffering = offering2rtrMap.get(c.Offering_Project__c); 
        }
        rMap.put(c.Id, referralTriggerReasonsByOffering); //getUniqueReferralTriggerRules returns the values 
     }
       return rMap; //this returns case with the offerings 
   }
    
    /**
     * Find the union of two lists of Referral Trigger Rules.
     * 
     * @param aList: the first list of Referral Trigger Rules.
     * @param bList: the second list of Referral Trigger Rules.
     * @return: a list of Referral Trigger Rules in both lists.
     */
    private static List<ReferralTriggerRules__c> getUniqueReferralTriggerRules(List<ReferralTriggerRules__c> aList, List<ReferralTriggerRules__c> bList)
    {
        Map<Id, ReferralTriggerRules__c> unionMap = new Map<Id, ReferralTriggerRules__c>();
        if (aList != null)
        {
            for (ReferralTriggerRules__c rtr : aList)
            {
                unionMap.put(rtr.Id, rtr);
            }
        }
        if (bList != null)
        {
            for (ReferralTriggerRules__c rtr : bList)
            {
                unionMap.put(rtr.Id, rtr);
            }
        }
        
        return unionMap.values();
    }
    
    /**
     * 
     * @return: a list of ReferralTriggerRules classes that are defined for a given offering.
     */
    public static Map<String, List<ReferralTriggerRules__c>> getReferralTriggerClassesForOffering(List<String> offerings, boolean sourceESB)
    {
        Map<String, List<ReferralTriggerRules__c>> offering2rtrMap = new Map<String, List<ReferralTriggerRules__c>>();
        
        Map<String, List<Id>> offering2rtrIdMap = new Map<String, List<Id>>();
        for (Referral_Triggers_Association__c rta : [
            SELECT Id, Offering__c, ReferralTriggerRules__c, ReferralTriggerRules__r.source_ESB__c
            FROM Referral_Triggers_Association__c
            WHERE Offering__c INCLUDES (:Utils.commaDelimit(offerings)) And ReferralTriggerRules__r.source_ESB__c = :sourceESB
        ])
        {
            for (String offering : rta.Offering__c.split(';'))
            {
                List<Id> rtrIds = offering2rtrIdMap.get(offering);
                if (rtrIds == null)
                {
                    rtrIds = new List<Id>();
                }
                rtrIds.add(rta.ReferralTriggerRules__c);
                
                offering2rtrIdMap.put(offering, rtrIds);
            }
        }
        
        List<Id> allRTR = new List<Id>();
        if (offerings != null)
        {
            for (String offering : offerings)
            {
                if(offering2rtrIdMap.get(offering) != null)
                {
                    allRTR.addAll(offering2rtrIdMap.get(offering));
                }
            }
        }
            
        for (ReferralTriggerRules__c rtr : [
            SELECT Id, Referral_Trigger_Reasons__c, Rule_Class_Name__c
            FROM ReferralTriggerRules__c
            WHERE Id IN :allRTR
        ])
        {
            for (String offering : offerings)
            {
                for (Id rtrId : offering2rtrIdMap.get(offering))
                {
                    if (rtr.Id == rtrId)
                    {
                        // Find rtr list in map.
                        List<ReferralTriggerRules__c> rtrs = offering2rtrMap.get(offering);
                        if (rtrs == null)
                        {
                            rtrs = new List<ReferralTriggerRules__c>();
                        }
                        
                        // Add the rtr to the list, and update the map.
                        rtrs.add(rtr);
                        offering2rtrMap.put(offering, rtrs);
                    }
                }
            }
        }
        
        return offering2rtrMap;
    }

    /**
     * 
     * 
     * @return: a list of ReferralTriggerRules classes that are associated with a dynamic question
     */    
    public static List<ReferralTriggerRules__c> getReferralTriggerClassesForQuestion(List<SIC_Question__c> sqs, boolean sourceESB)
    {
        // SIC Code Detail Version to SIC Questions map to be returned.
        Map<SIC_Question__c, List<ReferralTriggerRules__c>> sqs2rtr = new Map<SIC_Question__c, List<ReferralTriggerRules__c>>();
        
        // Convert List to Id mapped List elements.
        Map<Id, SIC_Question__c> questionMap = new Map<Id, SIC_Question__c>(sqs);
        
        // Initialize SIC Questions to Referral Trigger Rules map.
        for (SIC_Question__c sq : questionMap.values())
        {
            sqs2rtr.put(sq, new List<ReferralTriggerRules__c>());
        }
        
        // Query for all referral trigger associations.
        Map<Id, Id> sqvm = new Map<Id, Id>();
        List<Id> rtrIds = new List<Id>();
        for (Referral_Triggers_Association__c sqrta : [
            SELECT id, SIC_Question__c, ReferralTriggerRules__c, ReferralTriggerRules__r.source_ESB__c
            FROM Referral_Triggers_Association__c
            WHERE SIC_Question__c IN :sqs And ReferralTriggerRules__r.source_ESB__c = :sourceESB
        ])
        {
            sqvm.put(sqrta.ReferralTriggerRules__c, sqrta.SIC_Question__c);
            rtrIds.add(sqrta.ReferralTriggerRules__c);
        }
        
        // Create the list of ReferralTriggerRules for each SIC Question.
        for (ReferralTriggerRules__c rtr : [
            SELECT id, Referral_Trigger_Reasons__c, Rule_Class_Name__c
            FROM ReferralTriggerRules__c
            WHERE id IN :rtrIds
        ])
        {
            SIC_Question__c sq = questionMap.get(sqvm.get(rtr.id));
            
            List<ReferralTriggerRules__c> rtrs = sqs2rtr.get(sq);
            rtrs.add(rtr);
            sqs2rtr.put(sq, rtrs);
        }
        
        List<ReferralTriggerRules__c> rtrs = new List<ReferralTriggerRules__c>();
        for (SIC_Question__c sq : sqs2rtr.keySet())
        {
            List<ReferralTriggerRules__c> rtr2 = sqs2rtr.get(sq);
            rtrs = getUniqueReferralTriggerRules(rtrs, rtr2);
        }
        
        return rtrs;
    }
    public static List<ReferralTriggerRules__c> getReferralTriggerClassesFromESB(String xml, Case caseToUpdate)
    {
        List<String> offerings = new List<String>{caseToUpdate.Offering_Project__c};
        Map<String, List<ReferralTriggerRules__c>> offering2rtrMap;
        List<ReferralTriggerRules__c> staticQuestionRTRs;
        if (offerings != null)
        {
            offering2rtrMap = getReferralTriggerClassesForOffering(offerings, true);
            staticQuestionRTRs = offering2rtrMap.get(caseToUpdate.Offering_Project__c);
        }
        
        // Get referral trigger rules for dynamic questions.
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicDetailsBySicProjectRegion(caseToUpdate.Offering_Project__c, caseToUpdate.bkrCase_Region__c, caseToUpdate.bkrCase_SIC_Code__c);
        if (scdvs.isEmpty())
        {
            throw new ReferralTriggerRulesServiceException();
        }
        
        SIC_Code_Detail_Version__c scdv = scdvs[0];
        List<SIC_Question__c> sqs = SicQuestionService.getSicQuestions(scdv);
        
        List<ReferralTriggerRules__c> dynamicQuestionRTRs = getReferralTriggerClassesForQuestion(sqs, true);
        
        return getUniqueReferralTriggerRules(staticQuestionRTRs, dynamicQuestionRTRs);
    }    
    
    public class ReferralTriggerRulesServiceException extends Exception
    {
        
    }
}