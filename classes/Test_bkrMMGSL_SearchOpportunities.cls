@isTest (SeeAllData=true)
private class Test_bkrMMGSL_SearchOpportunities {
    
    public static testMethod void testMyController() {
        
        // Create a Brokerage
        Account testAcct = new Account (Name = 'My Test Account');
        insert testAcct;
        
        // Create a Broker 
        Contact cntc= new Contact();
        cntc.Lastname = 'test';
        cntc.Accountid=testAcct.id;
        insert cntc; 
        
        // Create a Client Account
        Account testClientAcct = new Account (Name = 'My Test Client Account', bkrAccount_CommonName__c = 'Test Common Name Inc', RecordTypeId = '012o0000000x3vq');
        insert testClientAcct; 
        
        // Create an Opportunity
        Opportunity oppt = new Opportunity(Name ='Opportunity',
                                           AccountID = testAcct.ID,
                                           bkrClientAccount__c = testClientAcct.Id,
                                           bkrOpportunity_Producer__c = cntc.Id,
                                           StageName = 'Closed Won',
                                           Amount = 3000,
                                           Type='New Business',
                                           bkrProductLineEffectiveDate__c = System.today(),
        								   CloseDate = System.today());
        insert oppt;
        
        PageReference pageRef = Page.bkrMMGSL_SearchOpportunities;
        Test.setCurrentPage(pageRef);
        bkrMMGSL_SearchOpportunities controller = new bkrMMGSL_SearchOpportunities();
        ApexPages.currentPage().getParameters().put('opportunityName','null12');
        ApexPages.currentPage().getParameters().put('brokerName','null12');
        ApexPages.currentPage().getParameters().put('clientAccountName','null12');
        ApexPages.currentPage().getParameters().put('clientAccountOtherCommonName','null12');
        
        controller.runSearch();
        controller.toggleSort();        
        
    }
}