global class SME_SICCodeDetails implements vlocity_ins.VlocityOpenInterface2 {
    
    global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        Boolean result = true;
        try {
            if (methodName.equals('getSICDetails')) {
                getSICDetails(inputs, output);
            }
            else {
                result = false;
            }
        }catch (Exception e) {
            System.debug('exception: ' + e + ' line no' + e.getLineNumber());
            result = false;
        }
        return result;
    }
     public void getSICDetails(Map<String,Object> inputMap, Map<String,Object> outMap) {
        //  String sicCode= '1532';
         system.debug('inputMap >>> ' + inputMap);
         Map<String, Object> gettingStartedNode = (Map<String, Object>)inputMap.get('GettingStarted');
         Map<String, Object> userNode = (Map<String, Object>)inputMap.get('user');
         
         system.debug('>>>>>>>>>>>> '+ gettingStartedNode.get('SICcodeId'));
         String SICCode = (String) gettingStartedNode.get('SICcodeId');
         String Offering = (String) gettingStartedNode.get('Offerings');
         String Region = (String) userNode.get('region');
         System.debug('Offering$$'+Offering);
         System.debug('Region$$'+region);
         
         SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
         sicDataModel = getSICDetailVersion(SICCode, offering, region);
         System.debug('SICCodeDetails$$$'+sicDataModel);
         
         outMap.put('SICDetailWrapper',sicDataModel);
     }
     
     /**
	 * Fetch full SIC data structure for a single SIC code and for specific Region and Offering
	 * @param sicCode
	 * @return SMEQ_SICDataModel if found or null if no records found
	 */
    public SMEQ_SICDataModel getSICDetailVersion(String sicCode, String offering, String region)
    {
        SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
        System.debug('SIC Code Passed From the Service:' + sicCode);
        try
        {
            List<SMEQ_SICDataModel> sicDataModelList;
            List<SIC_Code_Detail_Version__c> sicCodeDetailVersionList;
            ID sicCodeID;
            if (sicCode != null)
            {
                List<SIC_Code__c> sicCodeList = [
                    SELECT ID, About_the_Industry__c, About_the_Industry_Fr__c 
                    FROM SIC_Code__c
                    WHERE SIC_Code__c = :sicCode
                    LIMIT 1
                ];
                
                if (!sicCodeList.isEmpty())
                {
                    sicCodeID = sicCodeList[0].id;
                }
            }
            try
            {
                sicCodeDetailVersionList = declineForApplicationDisabled(getSicDetailsBySicProjectRegion(
                    offering,
                    region,
                    sicCodeID
                ));
            }
            catch (UserService.UserServiceException us)
            {
                System.debug('### exception thrown ' + us.getMessage());
            }
            
            sicDataModelList = convertSicCodeDetailListToSICDataModel(sicCodeDetailVersionList);
            
            if (sicDataModelList != null && sicDataModelList.size() > 0)
            {
                System.debug('Found SIC code Detail:' + sicDataModelList.get(0));
                sicDataModel = sicDataModelList.get(0);
                List<SIC_Code_Question_Association__c> sicCodeQuestionList = getQuestionsAssociatedWithSICCode(sicCode, offering, region, sicCodeDetailVersionList);
                Map<String,SMEQ_SICDataModel.Question> questionModelMap = new Map<String, SMEQ_SICDataModel.Question>();
                List<SMEQ_SICDataModel.Question> questionsList = convertSicCodeQuestionToQuestionModel(sicCodeQuestionList, questionModelMap);
                
                //Get the Affirmative Conditions for the Questions for the SIC Code
                //Build the list of questionId that we need to get affermative conditions
                List<String> questionIDList = new List<String>();
                for (SIC_Code_Question_Association__c sicCodeAssociation : sicCodeQuestionList)
                {
                    questionIDList.add(sicCodeAssociation.questionId__c);
                }
                
                List<SIC_Code_Question_Affirmative_Condition__c> affirmativeConditionsList = getAffirmativeConditionsForAllQuestionsAssiatedWithSicCode(questionIDList);
                convertSICCodeQuestionAffirmativeConditionToAffirmativeConditionDomainAndAddToQuestionDomain(affirmativeConditionsList, questionModelMap);
                sicDataModel.questions = questionsList;
                
                List<SIC_Terms_And_Condition_Association__c> sicCodeTermsAndConditionsList = getTermsAndConditionsAssociatedWithSICCode(sicCode, offering, region, sicCodeDetailVersionList);
                List<SMEQ_SICDataModel.TermsAndConditions> termConditionsList = convertSicCodeTermsAndConditionToTermConditionModel(sicCodeTermsAndConditionsList);
                
                sicDataModel.termsAndConditions = termConditionsList;
                updateReferralStateFlagsForSicDataModel(sicDataModel);
                System.debug('SIC Code Details:' + sicDataModelList.get(0));
            }
            
        }
        catch (Exception se)
        {
            System.debug(LoggingLevel.ERROR,
                         'Unexpected processing exception invoking : getSICDetailVersion '
                         + se.getMessage() + ','
                         + se.getCause() + ','
                         + 'line:' + se.getLineNumber()
                         + se.getStackTraceString());
            throw new SMEQ_ServiceException(se.getMessage(), se);
        }
        return sicDataModel;
    }
    
    /*
     * This returns a list of SIC Code Details based on the region and or offering
     * 
     * @param String Project, String Region, ID sicCode.
     * @return List<SIC_Code_Detail__c>.
     */
    public static List<SIC_Code_Detail_Version__c> getSicDetailsBySicProjectRegion(String offering, String region, ID sicCode)
    {
        system.debug('offering : ' + offering);
        system.debug('region : ' + region);
        system.debug('sicCode : ' + sicCode);
        //  Removed Region__c filter
        List<SIC_Code_Detail_Version__c> sicCodesList = [
                SELECT Id, Name, Action__c,
                Application_Enabled__c, Building_Required__c, ePolicy_Package__c,
                Canadian_Liability__c, USA_Liability__c,
                Crime_Hazard__c, Fid_Hazard__c, Property_Hazard__c,
                E_O_Liability__c, Poll_Liability__c,
                Coverage_Add_ons__c, Coverage_Defaults__c,
                RAG__c, Referral_Level__c,
                Segment__c, Subsegment__c, 
                SME__c, Non_RSA__c, Mid_Market__c,
                SIC_Code__r.SIC_Code__c,
                Short_Description_En__c, Short_Description_Fr__c, // Broker tool fields
                Application_Name__c,
                Description_Operations_En__c, Description_Operations_Fr__c, // Broker tool fields
                Category__c, Subcategory__c, Monoline__c,
                Region__c, Offering__c, Disabled_Policy_Actions__c
                FROM SIC_Code_Detail_Version__c
                WHERE Offering__c INCLUDES (:offering) AND
                      SIC_Code__c =: sicCode 
                ORDER BY Category__c ASC
            ];
        return sicCodesList;
    }
    
    /**
     * FP-720
     * Sets SIC Codes to Declined status when Application Enabled is false.
     * 
     * @param scdvs: A list of SIC Code Detail Version records.
     * @return: A list of SIC Code Detail Versions where the Referral_Level__c field set to Declined 
     *          when the Application_Enabled__c field is false.
     */
    @TestVisible
    private List<SIC_Code_Detail_Version__c> declineForApplicationDisabled(List<SIC_Code_Detail_Version__c> scdvs)
    {
        for (SIC_Code_Detail_Version__c scdv : scdvs)
        {
            if (!scdv.Application_Enabled__c || scdv.Monoline__c)
            {
                scdv.Referral_Level__c = 'DECLINED';
            }
        }
        
        return scdvs;
    }
    
    /**
	 * Converts the SMEQ_SICDataModel to SMEQ_SICDataModel populateing the short decription in bothe langunages and sic code.
	 * @return List of SMEQ_SICDataModel populated with short description in both langunages and sic code
	 */
    @TestVisible
    private List<SMEQ_SICDataModel> convertSicCodeDetailListToSICDataModel(List<SIC_Code_Detail_Version__c> sicCodeDetailVersionList)
    {
        Map<String, String> frenchCategory = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('frenchCategory');
        Map<String, String> frenchSubcategory = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('frenchSubcategory');
        
        List<SMEQ_SICDataModel> sicDataModelList = new List<SMEQ_SICDataModel>();
        if (sicCodeDetailVersionList != null)
        {
            for (SIC_Code_Detail_Version__c sicCodeDetailVersion : sicCodeDetailVersionList)
            {
                SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
                sicDataModel.shortDescriptionEN = sicCodeDetailVersion.Short_Description_En__c;
                sicDataModel.shortDescriptionFR = sicCodeDetailVersion.Short_Description_Fr__c;
                sicDataModel.descriptionOperationsEN = sicCodeDetailVersion.Description_Operations_En__c;
                sicDataModel.descriptionOperationsFR = sicCodeDetailVersion.Description_Operations_Fr__c;
                sicDataModel.sicCode = sicCodeDetailVersion.SIC_Code__r.SIC_Code__c;
                sicDataModel.buildingRequired = sicCodeDetailVersion.Building_Required__c;
                sicDataModel.referral = sicCodeDetailVersion.Referral_Level__c;
                sicDataModel.category = sicCodeDetailVersion.Category__c;
                sicDataModel.categoryFR = frenchCategory.get(sicCodeDetailVersion.Category__c);
                sicDataModel.subCategory = sicCodeDetailVersion.Subcategory__c;
                sicDataModel.subCategoryFR = frenchSubcategory.get(sicCodeDetailVersion.Subcategory__c);
                sicDataModel.coverageDefaults = sicCodeDetailVersion.Coverage_Defaults__c;
                sicDataModel.coverageAddOns = sicCodeDetailVersion.Coverage_Add_ons__c;
                sicDataModel.disabledPolicyActions = sicCodeDetailVersion.Disabled_Policy_Actions__c;
                sicDataModelList.add(sicDataModel);
            }
        }
        return sicDataModelList;
    }
    
    /**
     * Query the Data to get all the eligbility questions associated with the SIC Code
     * @param sicCode String
     * @return List<SIC_Code_Question_Association__c>
     */
    @TestVisible
    private List<SIC_Code_Question_Association__c> getQuestionsAssociatedWithSICCode(String sicCode, String offering, String region, List<SIC_Code_Detail_Version__c> sicCodeDetailVersionList)
    {
        //Get the recordtypes for questions
        List<SIC_Code_Question_Association__c> eligibilityQuestions = new List<SIC_Code_Question_Association__c>();
        Map<String, Id> SICQuestionTypes = Utils.GetRecordTypeIdsByDeveloperName(SIC_Question__c.SObjectType, true);

        //Get the Questions related to SIC Code
        List<SIC_Code_Question_Association__c> allSicQuestions = [
            SELECT questionId__c,  questionId__r.RecordTypeId, questionId__r.liveChatRequired__c, questionId__r.questionText_en__c, questionId__r.questionText_fr__c, SIC_Code_Detail_Version__r.SIC_Code__r.SIC_Code__c
            FROM SIC_Code_Question_Association__c
            WHERE SIC_Code_Detail_Version__c IN : sicCodeDetailVersionList 
            ORDER BY order__c
        ];

        for(SIC_Code_Question_Association__c que:allSicQuestions)
        {
            if (que.questionId__r.RecordTypeId == SICQuestionTypes.get(SicQuestionService.RECORDTYPE_ELIGIBILITY))
            {
                eligibilityQuestions.add(que);
            }
        }
        return eligibilityQuestions;

    }
    
    /**
	 * Coverts the SIC_Code_Question_Association__c to SMEQ_SICDataModel. Question populating the following properties
	 * 1) live chat required 
	 * 2) QuetionText_En, 
	 * 3) QuestionText_Fr
	 * Also populates the Map with Question Domain model with questionID as the key
	 * @return List of SMEQ_SICDataModel.Question populated with the propertied mentioned above
	 */
    @TestVisible
    private List<SMEQ_SICDataModel.Question> convertSicCodeQuestionToQuestionModel(List<SIC_Code_Question_Association__c> sicCodeQuestionList, Map<String, SMEQ_SICDataModel.Question> questionModelMap)
    {
        List<SMEQ_SICDataModel.Question> questionList = new List<SMEQ_SICDataModel.Question>();
        if (sicCodeQuestionList != null && sicCodeQuestionList.size() > 0)
        {
            for (SIC_Code_Question_Association__c sicCodeQuestion : sicCodeQuestionList)
            {
                SMEQ_SICDataModel.Question questionModel = new SMEQ_SICDataModel.Question();
                questionModel.liveChatRequired = sicCodeQuestion.questionId__r.liveChatRequired__c;
                questionModel.questionTextEN = sicCodeQuestion.questionId__r.questionText_en__c;
                questionModel.questionTextFR = sicCodeQuestion.questionId__r.questionText_fr__c;
                questionList.add(questionModel);
                questionModelMap.put(sicCodeQuestion.questionId__c, questionModel);
            }
        }
        return questionList;
    }
    
    /**
	 * Query the Data to get all affirmative conditions associated with all the questions for the SIC Code
	 * @param questionIDList List<String>
	 * @return List<SIC_Code_Question_Affirmative_Condition__c>
	 */
    @TestVisible
    private List<SIC_Code_Question_Affirmative_Condition__c> getAffirmativeConditionsForAllQuestionsAssiatedWithSicCode(List<String> questionIDList)
    {
        List<SIC_Code_Question_Affirmative_Condition__c> affirmationConditionsList = [
            SELECT questionId__c, affConditionId__r.affConditionText_en__c, affConditionId__r.affConditionText_fr__c
            FROM SIC_Code_Question_Affirmative_Condition__c
            WHERE questionId__c IN :questionIDList
            ORDER BY order__c
        ];
        
        return affirmationConditionsList;
    }
    
    /**
	 * Convert SIC_Code_Question_Affirmative_Condition__c  to AfirmativeConditiond domain model and associate them to question model
	 * @param affirmationConditionListList <SIC_Code_Question_Affirmative_Condition__c> 
	 * @param questionModelMap Map<String,SMEQ_SICDataModel.Question>
	*/
    @TestVisible
    private void convertSICCodeQuestionAffirmativeConditionToAffirmativeConditionDomainAndAddToQuestionDomain(List<SIC_Code_Question_Affirmative_Condition__c> affirmationConditionList, Map<String, SMEQ_SICDataModel.Question> questionModelMap)
    {
        for (SIC_Code_Question_Affirmative_Condition__c affirmativeCondition : affirmationConditionList)
        {
            SMEQ_SICDataModel.Question question = questionModelMap.get(affirmativeCondition.questionId__c);
            
            if (question.affirmativeConditions == null)
            {
                question.affirmativeConditions = new List<SMEQ_SICDataModel.AffirmativeCondition>();
            }
            SMEQ_SICDataModel.AffirmativeCondition affirmativeConditionModel = new SMEQ_SICDataModel.AffirmativeCondition();
            affirmativeConditionModel.affirmativeTextEN = affirmativeCondition.affConditionId__r.affConditionText_en__c;
            affirmativeConditionModel.affirmativeTextFR = affirmativeCondition.affConditionId__r.affConditionText_fr__c;
            question.affirmativeConditions.add(affirmativeConditionModel);
        }
        
    }
    
    /**
	 * Query the Data to get all questions associated with the SIC Code
	 * @param sicCode String
	 * @return List<SIC_Terms_And_Condition_Association__c>
	 */
    @TestVisible
    private List<SIC_Terms_And_Condition_Association__c> getTermsAndConditionsAssociatedWithSICCode(String sicCode, String offering, String region, List<SIC_Code_Detail_Version__c> sicCodeDetailVersionList)
    {
        system.debug(offering);
        system.debug(region);
        //Get the Questions related to SIC Code
        List<SIC_Terms_And_Condition_Association__c> sicTermsAndConditionsList = [
            SELECT tcId__r.conditionText_fr__c, tcId__r.conditionText_en__c
            FROM SIC_Terms_And_Condition_Association__c
            WHERE SIC_Code_Detail_Version__c IN : sicCodeDetailVersionList
            ORDER BY order__c
        ];
        System.debug('### ' + sicTermsAndConditionsList);
        return sicTermsAndConditionsList;
    }
    
    /**
	 * Coverts the SIC_Terms_And_Condition_Association__c to SMEQ_SICDataModel.TermsAndConditions populating the following properties
	 * 1) conditionTextEN 
	 * 2) conditionTextFR
	 * @return List of SMEQ_SICDataModel.TermsAndConditions populated with the propertied mentioned above
	 */
    @TestVisible
    private List<SMEQ_SICDataModel.TermsAndConditions> convertSicCodeTermsAndConditionToTermConditionModel(List<SIC_Terms_And_Condition_Association__c> sicCodeTermAndConditionList)
    {
        List<SMEQ_SICDataModel.TermsAndConditions> termConditionsList = new List<SMEQ_SICDataModel.TermsAndConditions>();
        if (sicCodeTermAndConditionList != null && sicCodeTermAndConditionList.size() > 0)
        {
            for (SIC_Terms_And_Condition_Association__c sicCodeTermCondition : sicCodeTermAndConditionList)
            {
                SMEQ_SICDataModel.TermsAndConditions termsAndConditionsModel = new SMEQ_SICDataModel.TermsAndConditions();
                termsAndConditionsModel.conditionTextEN = sicCodeTermCondition.tcId__r.conditionText_en__c;
                termsAndConditionsModel.conditionTextFR = sicCodeTermCondition.tcId__r.conditionText_fr__c;
                termConditionsList.add(termsAndConditionsModel);
            }
        }
        return termConditionsList;
    }
    
    @TestVisible
    private void updateReferralStateFlagsForSicDataModel(SMEQ_SICDataModel sicDataModel)
    {
        sicDataModel.hasAppetite = true;
        sicDataModel.isComplexSIC = false;
        sicDataModel.isNormalSIC = false;
        
        if (sicDataModel != null)
        {
            if (sicDataModel.referral != null && sicDataModel.referral.trim().length() > 0)
            {
                if (sicDataModel.referral.equals('LEVEL 1'))
                {
                    sicDataModel.isNormalSIC = true;
                }
                else if(sicDataModel.referral.equals('LEVEL 2') || sicDataModel.referral.equals('LEVEL 3'))
                {
                    sicDataModel.isComplexSIC = true;
                }
                else
                {
                    sicDataModel.hasAppetite = false;
                }
            }
            else
            {
                sicDataModel.isNormalSIC = true;
            }
        }
    }
}