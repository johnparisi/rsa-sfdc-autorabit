/**
This class digests CommlCoverage element in the get policy response and emits coverage SObjects.
*/

public with sharing class CSIOPolicyXMLCoverageProcessor {
    
    public List<Coverages__c> coverageList;
    private List<String> coverageListType; 
    private Map<String, Sic_Answer__c> riskAnswerMap = new Map<String, Sic_Answer__c>();
    private Set<String> enabledCoverages;
    /**
    Convert an arry of CommlCoverage into a list of Coverage__c
    @param xmlCommlCoverageElementArray the array of CommlCoverage element
    */
    
    public void process(Dom.XMLNode[] xmlCommlCoverageElementArray, Risk__c risk, Map<Id, Map<SIC_Question__c, SIC_Answer__c>>sicAnswers, Sic_Code_Detail_Version__c scdv, Set<String> enabledCoverages) {
        coverageList = new List<Coverages__c>();
        coverageListType = new List<String>();
        this.enabledCoverages = enabledCoverages;
        Map<String, String> customSettingsCoverages = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('dynamicQuestionCoverage');
        Map<String, String> customSettingscoverageCdMapping = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageCd');
        customSettingscoverageCdMapping.putAll(SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServicesByVariant('esbCoverageCd', scdv.ePolicy_Package__c));
        Map<String, String> customSettingscoverageCdReverseMapping = SMEQ_CustomSettingSearchUtility.reverseCustomSettingsMap(customSettingscoverageCdMapping);
        if(sicAnswers.get(risk.Id) != null){
            for (Sic_Question__c question: sicAnswers.get(risk.Id).keySet()){
                riskAnswerMap.put(risk.Id + '' + question.questionId__c, sicAnswers.get(risk.Id).get(question));
            }
        }
        for (Dom.XMLNode xmlNode : xmlCommlCoverageElementArray) {
            String indicatorNode = CSIOPolicyXMLProcessor.evaluatePath(xmlNode.getChildElement('Indicator', CSIOPolicyXMLProcessor.RSA_NAMESPACE));
            Boolean keepCoverage = false;
            if (indicatorNode == '1' || indicatorNode == null){
                keepCoverage = true;
            }
            if(xmlNode.getName() == 'CommlCoverage'){
                String Limit_Value = CSIOPolicyXMLProcessor.evaluatePath(xmlNode.getChildElement('Limit', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));
                // declare a new coverage object and populate
                Coverages__c coverage = new Coverages__c();
                String coverageTypeCsio = xmlNode.getChildElement('CoverageCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
                
                coverage.Type__c = customSettingscoverageCdReverseMapping.get(coverageTypeCsio);
                if (coverage.Type__c == null)
                {
                    continue;
                }

                //based on Coverage type, populate the appropriate fields, the building equipment and stock have ADDITIONAL fields that need to be populated 
                for (String coverageType : customSettingsCoverages.keyset()){
                    if (coverage.Type__c == coverageType  && scdv.coverage_defaults__c.contains(coverageType)){
                    	//To cover the situation that a coverage i.e. Content comes back but content value is not a SCDV question for this SIC Code
                    	if(keepcoverage){
                    		if (riskAnswerMap.get(risk.id + '' + customSettingsCoverages.get(coverageType)) != null) {
                    			Dom.XmlNode LimitNode = xmlNode.getChildElement('Limit', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                    			String limitAmountNode = LimitNode.getChildElement('FormatCurrencyAmt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
                    			riskAnswerMap.get(risk.id + '' + customSettingsCoverages.get(coverageType)).Value__c = limitAmountNode;
                    		}
                    		coverage = setCoverageFields(xmlNode, coverage);
                    	}
                        else{
                            if (riskAnswerMap.get(risk.id + '' + customSettingsCoverages.get(coverageType)) != null) {
                                riskAnswerMap.get(risk.id + '' + customSettingsCoverages.get(coverageType)).Value__c = '0';
                            }
                        }
                    }
                }
                if(keepCoverage){
                    if (coverage.Type__c == RiskService.COVERAGE_BUILDING){
                    risk.Building_Value__c = Decimal.valueOf(xmlNode.getChildElement('Limit', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('FormatCurrencyAmt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText()); 
                    coverage = setCoverageFields(xmlNode, coverage);
                    } 
                    else if (coverage.Type__c == RiskService.COVERAGE_CGL || 
                               coverage.Type__c == RiskService.COVERAGE_AGGREGATE || 
                               coverage.Type__c == RiskService.COVERAGE_CYBER) {
                        coverage = setCoverageFields(xmlNode, coverage);
                    }
                    if (scdv.Coverage_Add_ons__c.contains(coverage.Type__c)){
                        coverage = setCoverageFields(xmlNode, coverage);
                    }
                    coverage.Risk__c = risk.Id;
                }
            }
        }
        coverageList = filterDisabledCoverages(coverageList, risk);
        //Checks for the 3 default coverages, their absence would result in an error when upserting the risk
        //One scenario for this is named perils, we only allow broad coverages in the tool
        Set<String> coverageSet = new Set<String>(coverageListType);
        if(!coverageSet.contains(RiskService.COVERAGE_BUILDING) && 
           !coverageSet.contains(RiskService.COVERAGE_EQUIPMENT) &&
           !coverageSet.contains(RiskService.COVERAGE_STOCK) &&
           !coverageSet.contains(RiskService.COVERAGE_CGL)){
            throw new RSA_ExceedsToolLimitsException(Label.Exceeds_limit_of_tool);
        }
        system.debug('### coverageList at the end of CSIOCoverageProcessor' + coverageList);
        risk.Coverage_Selected_Policy__c = Utils.commaDelimit(coverageListType);
    }
    
    
    // private method to pull amounts
    private Double getAmount(Dom.XMLNode xmlNode) {
        return Double.valueOf(xmlNode.getChildElement('FormatCurrencyAmt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
    }
    
    private Coverages__c setCoverageFields(Dom.XmlNode xmlNode, Coverages__c coverage){
        if (xmlNode.getChildElement('Deductible', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null) {
            coverage.Deductible_Value__c = getAmount(xmlNode.getChildElement('Deductible', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));    
        }
        if(xmlNode.getChildElement('Limit', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null){
            coverage.Limit_Value__c = getAmount(xmlNode.getChildElement('Limit', CSIOPolicyXMLProcessor.ACORD_NAMESPACE));    
        }
        coverage.Package_ID__c = RiskService.PACKAGE_STANDARD;
        coverage.Selected__c = true;
        coverageList.add(coverage);
        coverageListType.add(coverage.Type__c);
        return coverage;
    }

    
   /*
     * Checks the list of coverages and strips away coverages which are not enabled for this sic code.
     * 
     * @param lc: A list of coverages from the risks.
     * @param caseId: The caseId for checking coverages.
     * @return The list of coverages with non-enabled coverages stripped out.
     */
    private List<Coverages__c> filterDisabledCoverages(List<Coverages__c> lc, Risk__c r)
    {
        List<Coverages__c> keep = new List<Coverages__c>();     
        Set<String> enabledCoverages = CoverageService.getEnabledCoverages(r.Quote__r.Case__c);
        
        for (Coverages__c c : lc)
        {
            if (this.enabledCoverages.contains(c.Type__c))
            {
                keep.add(c);
            }
        }
        return keep;
    }
    
}