global with sharing class SMEQ_QuoteResponseModel extends SMEQ_RESTResponseModel{
	
	public String caseId {public get; public set;}

	public SMEQ_QuoteRequest quoteRequest {public get; public set;}

	public String[] referralReasons {public get; public set;}
}