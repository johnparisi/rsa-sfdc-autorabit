/**
 * The purpose of this class is to encapsulate the permissions custom setting.
 */
public class RSA_ESBUWPermissions
{
    /**
     * Retrieve the permissions for the segment in the offering.
     * 
     * @param offering: The offering of the quote.
     * @param segment: The segment of the quote.
     * @return: Matched custom setting permissions if found, blank record otherwise.
     */
    public static UW_SICCodeSegment__c getPermission(String offering, String segment)
    {
        // Permissions will be false by default.
        UW_SICCodeSegment__c usc = new UW_SICCodeSegment__c();
        
        for (UW_SICCodeSegment__c us : UW_SICCodeSegment__c.getAll().values())
        {
            if (us.Offering__c == offering && us.Segment__c == segment)
            {
                usc = us;
                break;
            }
        }
        return usc;
    }
}