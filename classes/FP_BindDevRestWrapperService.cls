/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/bind/*')
global with sharing class FP_BindDevRestWrapperService {
    @HttpPut
    global static FP_AngularPageController.OutcomeResponse saveBindState(Quote__c quote, List<Risk__c> risks, List<BindQuoteRiskContactAccountWrapper> bindQuoteRiskContactAccountWrappers) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        FP_AngularPageController.OutcomeResponse outcomeResponse = null;
		try {
			outcomeResponse = FP_AngularPageController.saveQuoteState(JSON.serialize(quote), risks, bindQuoteRiskContactAccountWrappers);
		 	outcomeResponse.success = true;
		}
		catch (Exception e) {
			outcomeResponse = new FP_AngularPageController.OutcomeResponse();
			outcomeResponse.success = false;
			outcomeResponse.errorMessages.add(e.getMessage());
			outcomeResponse.errorMessages.add(e.getStackTraceString());
		}
    	return outcomeResponse;
    }
    
    @HttpPost
    global static FP_AngularPageController.OutcomeResponse bindQuote(Quote__c quote, List<Risk__c> risks, List<BindQuoteRiskContactAccountWrapper> bindQuoteRiskContactAccountWrappers) {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        FP_AngularPageController.OutcomeResponse outcomeResponse = null;
		try {
			outcomeResponse = FP_AngularPageController.bindQuoteRest(JSON.serialize(quote));
		 	outcomeResponse.success = true;
		}
		catch (Exception e) {
			outcomeResponse = new FP_AngularPageController.OutcomeResponse();
			outcomeResponse.success = false;
			outcomeResponse.errorMessages.add(e.getMessage());
			outcomeResponse.errorMessages.add(e.getStackTraceString());
		}
        return outcomeResponse;
    }	
    
}