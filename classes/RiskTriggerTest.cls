/**
 * Author: James Lee
 * Created At: August 29, 2016
 * 
 * Unit tests for RiskTrigger.
 */
@isTest
public class RiskTriggerTest
{
    private static final Integer COVERAGE_LIMIT_BUILDING = 10000;
    
    static testmethod void testLiabilityRisks()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c r = SMEQ_TestDataGenerator.getLiabilityRisk();
            r = [
                SELECT Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c
                FROM Risk__c
                WHERE id = :r.Id
                LIMIT 1
            ];
            
            System.assert(r.Coverage_Selected_Option_1__c == RiskService.COVERAGE_CGL);
            System.assert(r.Coverage_Selected_Option_2__c == RiskService.COVERAGE_CGL);
        }
    }
    
    static testmethod void testInsertAndUpdateRisks()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            SIC_Code_Detail_Version__c scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();

            Map<String, Id> riskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);

            Risk__c r = [
                SELECT Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c
                FROM Risk__c
                WHERE recordTypeId = :riskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION)
                LIMIT 1
            ];
            
            r.Coverage_Selected_Option_1__c += ';Flood';
            r.Coverage_Selected_Option_2__c += ';Flood';
            update r;
            
            System.assert(r.Coverage_Selected_Option_1__c.contains('Building'));
            System.assert(r.Coverage_Selected_Option_1__c.contains('Equipment'));
            System.assert(r.Coverage_Selected_Option_1__c.contains('Stock'));
            System.assert(r.Coverage_Selected_Option_1__c.contains('Flood'));
            System.assert(r.Coverage_Selected_Option_2__c.contains('Building'));
            System.assert(r.Coverage_Selected_Option_2__c.contains('Equipment'));
            System.assert(r.Coverage_Selected_Option_2__c.contains('Stock'));
            System.assert(r.Coverage_Selected_Option_2__c.contains('Flood'));
            
            assertCoverageExists(r.Coverage_Selected_Option_1__c, r, 'Option 1', scdv.Coverage_Defaults__c);
        }
    }
    
    /**
     * Helper method for testInsertAndUpdateRisks.
     */
    private static void assertCoverageExists(String coverages, Risk__c r, String option, String defaultCoverages)
    {
        String unselectedCoverages = '';
        for (String s : RiskService.getAllLocationCoverageTypes(defaultCoverages).split(';'))
        {
            if (!coverages.contains(s))
            {
                if (unselectedCoverages.length() > 0)
                {
                    unselectedCoverages += ';';
                }
                
                unselectedCoverages += s;
            }
        }
        
        for (Coverages__c c : [
            SELECT Id, Type__c, Selected__c
            FROM Coverages__c
            WHERE Risk__c = :r.Id
            AND Package_ID__c = :option
        ])
        {
            System.assert(coverages.contains(c.Type__c));
            System.assert(!unselectedCoverages.contains(c.Type__c));
        }
    }
    
    static testMethod void testUpdateDefaultCoverageLimits()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c r = SMEQ_TestDataGenerator.getEmptyLocationRisk();
            r = [
                SELECT id, Building_Value__c, Equipment_Value__c, Stock_Value__c
                FROM Risk__c
                WHERE id = :r.Id
                LIMIT 1
            ];
            
            String packageId = RiskService.PACKAGE_STANDARD;
            
            Coverages__c cBuilding = SMEQ_TestDataGenerator.getCoverage(r, packageId, RiskService.COVERAGE_BUILDING);
            Coverages__c cEquipment = SMEQ_TestDataGenerator.getCoverage(r, packageId, RiskService.COVERAGE_EQUIPMENT);
            Coverages__c cStock = SMEQ_TestDataGenerator.getCoverage(r, packageId, RiskService.COVERAGE_STOCK);
            
            System.assertEquals(null, cBuilding.Limit_Value__c);
            System.assertEquals(null, cEquipment.Limit_Value__c);
            System.assertEquals(null, cStock.Limit_Value__c);
            
            r.Building_Value__c = COVERAGE_LIMIT_BUILDING;
            update r;
            
            List<Coverages__c> cs = [
                SELECT id, Limit_Value__c, Type__c
                FROM Coverages__c
                WHERE risk__c = :r.Id
                AND Package_Id__c = :packageId
            ];
            
            for (Coverages__c c : cs)
            {
                if (c.Type__c == RiskService.COVERAGE_BUILDING)
                {
                    System.assertEquals(COVERAGE_LIMIT_BUILDING, Integer.valueOf(c.Limit_Value__c));
                }
            }
        }
    }
    
}