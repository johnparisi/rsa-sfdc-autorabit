public class CSIO_GeneralLiabilityClassification
{
    private Integer exposure;
    
    private Map<String, String> sas;
    private String sa;
    
	public String territoryCd;
    public ExposureInfo exposureInfo;
    
    public CSIO_GeneralLiabilityClassification(Map<String, String> sas, String sa)
    {
        this.sas = sas;
        this.sa = sa;
        convert();
    }
    
    public void convert()
    {
        Map<String, String> esbTerritoryCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbTerritoryCd');
        
        this.territoryCd = esbTerritoryCd.get(this.sa);
        this.exposureInfo = new ExposureInfo(this.sas, this.sa);
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<GeneralLiabilityClassification>';
        if (this.territoryCd != null)
        {
            xml += ' <TerritoryCd>' + this.territoryCd + '</TerritoryCd>';
        }
        xml += this.exposureInfo;
        xml += '</GeneralLiabilityClassification>';
        
        return xml;
    }
    
    public class ExposureInfo
    {
        private Map<String, String> sas;
        private String sa;
        
        public String premiumBasisCd;
        public Integer exposure;
        
        public ExposureInfo(Map<String, String> sas, String sa)
        {
            this.sas = sas;
            this.sa = sa;
            convert();
        }
        
        private void convert()
        {
            Map<String, String> esbPremiumBasisCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbPremiumBasisCd');
            
            this.premiumBasisCd = esbPremiumBasisCd.get(this.sa);
            this.exposure = this.sas.get(this.sa) != null ? Integer.valueOf(this.sas.get(this.sa)) : 0;
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<ExposureInfo>';
            xml += ' <PremiumBasisCd>' + this.premiumBasisCd + '</PremiumBasisCd>';
            xml += ' <Exposure>' + this.exposure + '</Exposure>';
            xml += '</ExposureInfo>';
            
            return xml;
        }
    }
}