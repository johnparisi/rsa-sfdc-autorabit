public class CSIO_AdditionalInterest {
    
    private Account account;
    private Contact contact;
    private Quote_Risk_Relationship__c quoteRiskRelationship;
    private String partyType;
    private String contractNumber;
    
    public CSIO_GeneralPartyInfo generalPartyInfo;
    
    public CSIO_AdditionalInterest(Account account,  Quote_Risk_Relationship__c quoteRiskRelationship) {
    	this.account = account;
    	this.quoteRiskRelationship = quoteRiskRelationship;
    	convert();
    }

    public CSIO_AdditionalInterest(Contact contact, Quote_Risk_Relationship__c quoteRiskRelationship) {
    	this.contact = contact;
    	this.quoteRiskRelationship = quoteRiskRelationship;
    	//System.assert(contact.LastName != null, 'Last Name should not be null');
    	//System.assert(contact.MailingStreet !=null, 'Mailing street should not be null');
    	convert();
    }
    
    private void convert() {
    	if (account != null) {
    		this.generalPartyInfo = new CSIO_GeneralPartyInfo(account, quoteRiskRelationship);
            if(quoteRiskRelationship.Party_Type__c == RelationshipControllerExtension.PREMIER_FINANCIER){
                this.contractNumber = account.Contract_Number__c;
            }
    	}
    	else if (contact !=null) {
    		this.generalPartyInfo = new CSIO_GeneralPartyInfo(contact, quoteRiskRelationship);
            if(quoteRiskRelationship.Party_Type__c == RelationshipControllerExtension.PREMIER_FINANCIER){
                this.contractNumber = contact.Contract_Number__c;
            }
    	}
        
        Map<String, String> esbNatureInterestCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbParty_Type__c');
    	
        // set the type appropriately
    	this.partyType = esbNatureInterestCd.get(quoteRiskRelationship.Party_Type__c);
        if (this.partyType == null)
        {
            // No party types matched, check relationship types.
            Map<String, String> esbRelationshipType = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Relationship_Type__c');
			this.partyType = esbRelationshipType.get(quoteRiskRelationship.Relationship_Type__c);
        }
    }
    
    public override String toString()
    {
        String xml = '';
        
        xml += '<AdditionalInterest>';
        xml += this.generalPartyInfo;
        xml += ' <AdditionalInterestInfo>';
        xml += '  <NatureInterestCd>' + this.partyType + '</NatureInterestCd>';
        if(this.contractNumber != null){
            xml += '  <AccountNumberId>' + this.contractNumber + '</AccountNumberId>';
        }
        xml += ' </AdditionalInterestInfo>';
        if (this.quoteRiskRelationship.Deleted__c == true){
            xml += '<rsa:Deleted>' + 1 + '</rsa:Deleted>';
        }
        else {
            xml+= '<rsa:Deleted>' + 0 + '</rsa:Deleted>';
        }
        xml += '</AdditionalInterest>';
        
        return xml;
    }
}