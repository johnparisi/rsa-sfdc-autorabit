global class CertificateAttachment {
    public class CustomException extends Exception{}
    //global Id recipientId { get; set; }
    //global Id relatedToId { get; set; }
    global Contact recipient { get; set; }
    global Campaign relatedTo { get; set; }
    
    public Campaign campRec { 
        get {
            if (relatedTo != null && ((String)relatedTo.Id).startsWith(Campaign.SObjectType.getDescribe().getKeyPrefix())) {
                //return [SELECT Id, Name, bkrCampaign_CourseDate__c, bkrCampaign_CourseCategory__c, bkrCampaign_CourseHours__c FROM Campaign WHERE Id =:relatedToId LIMIT 1];
                return relatedTo;
            } else {
                throw new CustomException('A Campaign is required.');
                return null;
            }
        } 
        set; 
    }
    public Contact contactRec { 
        get {
            if (recipient != null && ((String)recipient.Id).startsWith(Contact.SObjectType.getDescribe().getKeyPrefix())) {
                //return [SELECT Id, FirstName, LastName, Account.Name FROM Contact WHERE Id =:recipientId LIMIT 1];
                return recipient;
            } else {
                throw new CustomException('A Contact is required.');
                return null;
            }
        }
        set;
    }
}