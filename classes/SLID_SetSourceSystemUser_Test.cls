/**
  * @author        Saumil Bapat
  * @date          11/18/2016
  * @description   Test class for SLID_SetReferralCaseOwner
*/
@isTest
private Class SLID_SetSourceSystemUser_Test {
    static testMethod void testSetReferralCaseOwner()
    {
        //Create User with LanId
        User testUser = SLID_TestDataFactory.adminUser;
        testUser.userName = 'TestAdmin5@Testing.com';
        testUser.alias = 'testUser';
        testUser.Ext_User__c = 'testUser';
        insert testUser;
        System.Debug('~~~testUser: ' + testUser);
        
        
        System.runAs(testUser)
        {
          //Set Integration Log Id
          SLID_TestDataFactory.initializeIntegrationLogId();
        
          //Create a test Account
          Account testAccount = SLID_TestDataFactory.returnTestAccount();
          insert testAccount;
          System.Debug('~~~testAccount: ' + testAccount);

          //Create a test Contact
          Contact testContact = SLID_TestDataFactory.returnTestContact();
          testContact.AccountId = testAccount.Id;
          insert testContact;
          System.Debug('~~~testContact: ' + testContact);

          //Create a test case
          Case testCase = SLID_TestDataFactory.returnTestNewBusinessCase();
          testCase.AccountId = testAccount.Id;
          testCase.ContactId = testContact.Id;
          testCase.Source_System_User_Id__c = 'testUser';
          insert testCase;
          System.Debug('~~~testCase: ' + testCase);

          //Create a test quote version
          Quote_Version__c testQuote = SLID_TestDataFactory.returnQuoteVersion();
          testQuote.Case__c = testCase.Id;
          testQuote.Source_System_User_Id__c = 'testUser';
          insert testQuote;
          System.Debug('~~~testQuote: ' + testQuote);

          testQuote = [Select id, Source_System_User_Id__c, Source_System_User__c from Quote_Version__c where id = :testQuote.Id];
          testCase = [Select id, Source_System_User_Id__c, Source_System_User__c from Case where id = :testCase.Id];
          testUser = [Select Id, Ext_User__c from User where id = :testUser.Id];

          System.Debug('~~~testQuote: ' + testQuote);
          System.Debug('~~~testCase: ' + testCase);
          System.Debug('~~~testUser: ' + testUser);
          System.Assert(testCase.Source_System_User__c == testUser.Id, '~~~testCase Source_System_User__c not set correctly: ' + testCase.Source_System_User__c);
          System.Assert(testQuote.Source_System_User__c == testUser.Id, '~~~testQuote Source_System_User__c not set correctly: ' + testQuote.Source_System_User__c);

        }
      }
}