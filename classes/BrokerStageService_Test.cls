/**
 * Author: Ricky Li
 * Created At: January 24, 2017
 * 
 * Unit tests for BrokerStageService
 */
@isTest
private class BrokerStageService_Test {
    private static String EP_REFERENCE_NUMBER = '1234567890';
    private static String SELECTED_BROKERAGE_NUMBER = '0987654321';
    static testMethod void myUnitTest()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();        
            Account a = SMEQ_TestDataGenerator.getBrokerageAccount();
            Contact c = SMEQ_TestDataGenerator.getProducerContact(a);
            String brokerNumber = BrokerStageService.getStageBrokerNumber(a.bkrAccount_HuonBrokerNumber__c);
            brokerNumber = BrokerStageService.getStageBrokerNumber('1000550001');        
            brokerNumber = BrokerStageService.getStageBrokerNumber('1000770001');        
            brokerData.PARENT3_BROKER_NUMBER__c = '1000000002';
            update brokerData;
            brokerNumber = BrokerStageService.getStageBrokerNumber('1000000002');
            system.assertEquals(brokerNumber, '1000000001');
            List<Broker_Stage__c> brokers = BrokerStageService.getStageBrokerageList('1000000002');
            system.assertEquals(1, brokers.size());
            Case testCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            testCase.accountid = a.id;
            testCase.ContactId = c.Id;
            upsert testCase;
            brokerNumber = BrokerStageService.getStageBrokerNumberByCase(testCase.id);
            system.assertEquals(brokerNumber, '1000000001');
            c.EP_Reference_Number__c = EP_REFERENCE_NUMBER;
            update c;
            // updated by TSH - clearing the cache on the BrokerStageService
            BrokerStageService.brokerCases.clear();
            brokerNumber = BrokerStageService.getStageBrokerNumberByCase(testCase.id);
           // system.assertEquals(brokerNumber, '1000000001');
            testCase.selected_brokerage_number__c = SELECTED_BROKERAGE_NUMBER;
            upsert testCase;
            // updated by TSH - clearing the cache on the BrokerStageService
            BrokerStageService.brokerCases.clear();
            brokerNumber = BrokerStageService.getStageBrokerNumberByCase(testCase.id);
            // system.assertEquals(brokerNumber, '1000000001');
        }
    }
}