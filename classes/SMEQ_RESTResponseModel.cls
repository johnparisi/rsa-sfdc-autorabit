/**
 * Created by t903018 on 6/29/2016.
 */

public virtual  class SMEQ_RESTResponseModel {

    public static String RESPONSE_CODE_OK = 'OK';
    public static String RESPONSE_CODE_ERROR = 'ERROR';
    public static String ERROR_CODE_600='600';
    public static String ERROR_CODE_699='699';
    public String responseCode;
    List<ResponseError> errors;

    /**
     * Default Response NO Errors
     */
    public  SMEQ_RESTResponseModel(){
        responseCode = RESPONSE_CODE_OK;
    }

    public void setResponseCode( String  code) {
        responseCode = code;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setErrors(List<ResponseError> theErrors)
    {
        errors = theErrors;
    }

    public List<ResponseError> getErrors()
    {
        return errors;
    }

    public class ResponseError {
        String errorCode;
        String field;
        String errorMessage;
        String detailErrorMessage;

        public ResponseError(){}
        public ResponseError(String code,String errorMsg){
            errorCode = code;
            errorMessage = errorMsg;
        }
        public void setErrorCode(String code)
        {
            this.errorCode=code;
        }

        public String getErrorCode()
        {
            return errorCode;
        }

        public void setField(String value)
        {
            this.field=value;
        }
        public void setErrorMessage(String msg)
        {
            this.errorMessage=msg;
        }
        public void setdetailErrorMessage(String detailMessage)
        {
            this.detailErrorMessage=detailMessage;
        }
    }
}