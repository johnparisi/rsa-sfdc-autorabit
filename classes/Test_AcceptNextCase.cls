/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_AcceptNextCase {


    public class MockHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
            //System.assertEquals('GET', req.getMethod());
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"query":"SELECT Id FROM Case"}');
            res.setStatusCode(200);
            return res;
        }
    }

    static testMethod void testNotHasSkillAssignSecondCase() {

        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y'); //CISME
        // assign a skill the Case doesn't have; NONE means all!
        u.bkrUser_CI_Transaction_Type_Skills__c = 'BOR';
        insert u;

        BusinessHours bh = [Select Id from BusinessHours LIMIT 1][0];
        DateTime nextStart = BusinessHours.nextStartDate(bh.id, System.Now());

        Group objGroup1 = new Group(DeveloperName = 'Ontario_CI', Name = 'Test',Type='Queue');
        insert objGroup1;

        QueueSobject objQueueSobject1 = new QueueSobject(SobjectType = 'Case', QueueId = objGroup1.Id);
        insert objQueueSobject1;

        //only add user to CI queue
        GroupMember groupMember = new GroupMember(GroupId = objGroup1.Id, UserOrGroupId=u.Id);
        insert groupMember;

        Case objCase;
        Case objCase1;

        System.runAs([Select Id from User where IsActive=true][0]){

            //assign Case to CI queue
            // put both cases in SAME queue
            // using Broker Connect-Outbound RecordType(not in routing rules)
            objCase = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            // assign a skill the user doesn't have
            objCase.bkrCase_Subm_Type__c = 'Cancellation';
            objCase.OwnerId = objGroup1.Id;

            insert objCase;

            //assign this other case to SAME queue
            objCase1 = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, OwnerId = objGroup1.Id, Status = 'Waiting for Information', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');
            insert objCase1;

        }

        System.runAs(u) {
            Test.setCreatedDate(objCase.Id, nextStart);
            Test.setCreatedDate(objCase1.Id, nextStart);
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            AcceptNextCase.acceptCase(u.Id, '', null);

            Test.stopTest();
        }
        system.assertequals(objGroup1.Id, [Select Id, OwnerId from Case where Id =: objCase.Id].OwnerId);
        system.assertequals(u.Id, [Select Id, OwnerId from Case where Id =: objCase1.Id].OwnerId);
    }

    static testMethod void testNotHasSkillNotSameProductStillAssignCase() {

        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y'); //CISME
        // assign a skill the Case doesn't have; NONE means all!
        u.bkrUser_CI_Transaction_Type_Skills__c = 'BOR';
        insert u;

        BusinessHours bh = [Select Id from BusinessHours LIMIT 1][0];
        DateTime nextStart = BusinessHours.nextStartDate(bh.id, System.Now());

        Group objGroup = new Group(DeveloperName = 'Ontario_CI', Name = 'Test',Type='Queue');
        insert objGroup;

        QueueSobject objQueueSobject = new QueueSobject(SobjectType = 'Case', QueueId = objGroup.Id);
        insert objQueueSobject;

        Group objGroup1 = new Group(DeveloperName = 'Ontario_CI_IRC', Name = 'Test',Type='Queue');
        insert objGroup1;

        QueueSobject objQueueSobject1 = new QueueSobject(SobjectType = 'Case', QueueId = objGroup1.Id);
        insert objQueueSobject1;

        //only add user to IRC queue
        GroupMember groupMember = new GroupMember(GroupId = objGroup1.Id, UserOrGroupId=u.Id);
        insert groupMember;

        Case objCase;
        Case objCase1;

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        System.runAs([Select Id from User where IsActive=true][0]){

            Account acctParent = new Account(RecordTypeId='012o0000000Ag6G',Name='TA'); //MM Broker
            insert acctParent;

            Account acct = new Account(RecordTypeId='012o0000000AgDr',Name='TA',ParentId=acctParent.Id); //ML Broker
            insert acct;

            AccountTeamMember atm = new AccountTeamMember(AccountId=acctParent.Id,TeamMemberRole='CI - Property & Casualty',UserId=u.id);
            insert atm;
            //assign Case to IRC queue
            // ***
            // Set to CI Triage record type to prevent Case Assignment
            // ***
            // // using Broker Connect-Outbound RecordType(not in routing rules)
            objCase = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, AccountId=acct.Id, Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');
            objCase.User_Assigned_With_Next_Case_Button__c = [Select Id from User where IsActive=true][0].Id;
            insert objCase;
            system.debug(objCase);
            // assign a skill the user doesn't have
            objCase.bkrCase_Subm_Type__c = 'Cancellation';
            objCase.OwnerId = objGroup1.Id;
            update objCase;

            //assign this other case to OTHER queue
            objCase1 = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, OwnerId = objGroup.Id, Status = 'Waiting for Information', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase1;

        }
        System.runAs(u) {
            Test.setCreatedDate(objCase.Id, nextStart);
            Test.setCreatedDate(objCase1.Id, nextStart);
            Test.startTest();

            system.debug([Select Id, OwnerId from Case where Id =: objCase.Id]);
            AcceptNextCase.acceptCase(u.Id, '', null);

            Test.stopTest();
        }
        system.assertequals(u.Id, [Select Id, OwnerId from Case where Id =: objCase.Id].OwnerId);
        system.assertequals(u.Id, [Select Id, User_Assigned_With_Next_Case_Button__c from Case where Id =: objCase.Id].User_Assigned_With_Next_Case_Button__c);
        system.assertequals(objGroup.Id, [Select Id, OwnerId from Case where Id =: objCase1.Id].OwnerId);
    }

    static testMethod void testHasSkillAndOnAccountTeam() {

        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y'); //CISME
        u.bkrUser_CI_Transaction_Type_Skills__c = 'Cancellation';
        insert u;

        BusinessHours bh = [Select Id from BusinessHours LIMIT 1][0];
        DateTime nextStart = BusinessHours.nextStartDate(bh.id, System.Now());

        Group objGroup = new Group(DeveloperName = 'Ontario_CI', Name = 'Test',Type='Queue');
        insert objGroup;

        QueueSobject objQueueSobject = new QueueSobject(SobjectType = 'Case', QueueId = objGroup.Id);
        insert objQueueSobject;

        Group objGroup1 = new Group(DeveloperName = 'Ontario_CI_IRC', Name = 'Test',Type='Queue');
        insert objGroup1;

        QueueSobject objQueueSobject1 = new QueueSobject(SobjectType = 'Case', QueueId = objGroup1.Id);
        insert objQueueSobject1;

        //only add user to IRC queue
        GroupMember groupMember = new GroupMember(GroupId = objGroup1.Id, UserOrGroupId=u.Id);
        insert groupMember;

        Case objCase;
        Case objCase1;

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        System.runAs([Select Id from User where IsActive=true][0]){

            Account acctParent = new Account(RecordTypeId='012o0000000Ag6G',Name='TA'); //MM Broker
            insert acctParent;

            Account acct = new Account(RecordTypeId='012o0000000AgDr',Name='TA',ParentId=acctParent.Id); //ML Broker
            insert acct;

            AccountTeamMember atm = new AccountTeamMember(AccountId=acctParent.Id,TeamMemberRole='CI - Property & Casualty',UserId=u.id);
            insert atm;
            //assign Case to IRC queue
            // ***
            // Set to CI Triage record type to prevent Case Assignment
            // ***
            objCase = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, AccountId=acct.Id, Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase;

            Product2 prod = new Product2(Name = 'Epolicy', ProductCode = 'CI_SME_EPOLICY');
            insert prod;
            Id portfolioRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CI Portfolio').getRecordTypeId();
            objCase.RecordTypeId = portfolioRecordTypeId;
            objCase.bkrCase_Subm_Type__c = 'Cancellation';
            objCase.ProductId = prod.Id;
            objCase.OwnerId = objGroup1.Id;
            system.debug(objCase);
            update objCase;

            //assign this other case to OTHER queue
            objCase1 = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, OwnerId = objGroup.Id, Status = 'Waiting for Information', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase1;

        }
        System.runAs(u) {
            Test.setCreatedDate(objCase.Id, nextStart);
            Test.setCreatedDate(objCase1.Id, nextStart);

            Test.startTest();
            system.debug([Select Id, OwnerId from Case where Id =: objCase.Id]);
            AcceptNextCase.acceptCase(u.Id, '', null);

            Test.stopTest();
        }
        system.assertequals(u.Id, [Select Id, OwnerId from Case where Id =: objCase.Id].OwnerId);
        system.assertequals(objGroup.Id, [Select Id, OwnerId from Case where Id =: objCase1.Id].OwnerId);
    }

    static testMethod void testNotHasSkillAndOnAccountTeamAssignSecond() {

        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000KU1y'); //CISME
        u.bkrUser_CI_Transaction_Type_Skills__c = 'BOR';
        insert u;

        BusinessHours bh = [Select Id from BusinessHours LIMIT 1][0];
        DateTime nextStart = BusinessHours.nextStartDate(bh.id, System.Now());

        Group objGroup = new Group(DeveloperName = 'Ontario_CI', Name = 'Test',Type='Queue');
        insert objGroup;

        QueueSobject objQueueSobject = new QueueSobject(SobjectType = 'Case', QueueId = objGroup.Id);
        insert objQueueSobject;

        Group objGroup1 = new Group(DeveloperName = 'Ontario_CI_IRC', Name = 'Test',Type='Queue');
        insert objGroup1;

        QueueSobject objQueueSobject1 = new QueueSobject(SobjectType = 'Case', QueueId = objGroup1.Id);
        insert objQueueSobject1;

        // add user to IRC queue
        GroupMember groupMember = new GroupMember(GroupId = objGroup1.Id, UserOrGroupId=u.Id);
        insert groupMember;

        Case objCase;
        Case objCase1;

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        System.runAs([Select Id from User where IsActive=true][0]){

            Account acctParent = new Account(RecordTypeId='012o0000000Ag6G',Name='TA'); //MM Broker
            insert acctParent;

            Account acct = new Account(RecordTypeId='012o0000000AgDr',Name='TA',ParentId=acctParent.Id); //ML Broker
            insert acct;

            AccountTeamMember atm = new AccountTeamMember(AccountId=acctParent.Id,TeamMemberRole='CI - Property & Casualty',UserId=u.id);
            insert atm;
            //assign Case to IRC queue
            // ***
            // Set to CI Triage record type to prevent Case Assignment
            // ***
            objCase = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, AccountId=acct.Id, Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase;

            Product2 prod = new Product2(Name = 'Epolicy', ProductCode = 'CI_SME_EPOLICY');
            insert prod;
            Id portfolioRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CI Portfolio').getRecordTypeId();
            objCase.RecordTypeId = portfolioRecordTypeId;
            objCase.bkrCase_Subm_Type__c = 'Cancellation';
            objCase.ProductId = prod.Id;
            objCase.OwnerId = objGroup1.Id;
            system.debug(objCase);
            update objCase;

            //assign this other case to OTHER queue
            objCase1 = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, bkrCase_Subm_Type__c = 'BOR', OwnerId = objGroup1.Id, Status = 'Waiting for Information', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase1;

        }
        System.runAs(u) {
            Test.setCreatedDate(objCase.Id, nextStart);
            Test.setCreatedDate(objCase1.Id, nextStart);

            Test.startTest();
            system.debug([Select Id, OwnerId from Case where Id =: objCase.Id]);
            AcceptNextCase.acceptCase(u.Id, '', null);

            Test.stopTest();
        }
        system.assertequals(objGroup1.Id, [Select Id, OwnerId from Case where Id =: objCase.Id].OwnerId);
        system.assertequals(u.Id, [Select Id, OwnerId from Case where Id =: objCase1.Id].OwnerId);
    }

    static testMethod void myMMGSLUnitTest() {

        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000pteH'); //MMGSL
        insert u;

        BusinessHours bh = [Select Id from BusinessHours LIMIT 1][0];
        DateTime nextStart = BusinessHours.nextStartDate(bh.id, System.Now());

        Group objGroup = new Group(DeveloperName = 'Ontario_CI', Name = 'Test',Type='Queue');
        insert objGroup;

        QueueSobject objQueueSobject = new QueueSobject(SobjectType = 'Case', QueueId = objGroup.Id);
        insert objQueueSobject;

        Group objGroup1 = new Group(DeveloperName = 'Ontario_CI_IRC', Name = 'Test',Type='Queue');
        insert objGroup1;

        QueueSobject objQueueSobject1 = new QueueSobject(SobjectType = 'Case', QueueId = objGroup1.Id);
        insert objQueueSobject1;

        //only add user to IRC queue
        GroupMember groupMember = new GroupMember(GroupId = objGroup1.Id, UserOrGroupId=u.Id);
        insert groupMember;

        Case objCase;
        Case objCase1;

        System.runAs([Select Id from User where IsActive=true][0]){

            insert new RSA_Custom_Settings__c(MMGSL_Queue_IDs__c = objGroup1.Id);
            //assign Case to IRC queue
            // ***
            // Set to CI Triage record type to prevent Case Assignment
            // ***
            objCase = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, OwnerId = objGroup1.Id, Status = 'New', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase;

            //assign this other case to OTHER queue
            objCase1 = new Case(RecordTypeId='012o0000000C00J', BusinessHoursId = bh.Id, OwnerId = objGroup.Id, Status = 'Waiting for Information', Priority = 'Medium', Origin = 'Phone', bkrCase_Category__c = 'Reports', Subject = 'Test');

            insert objCase1;

        }
        System.runAs(u) {
            Test.setCreatedDate(objCase.Id, nextStart);
            Test.setCreatedDate(objCase1.Id, nextStart);
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            system.debug([Select Id, OwnerId from Case where Id =: objCase.Id]);
            AcceptNextCase.acceptCase(u.Id, 'Regional Mid-Market & GSL', null);

            Test.stopTest();
        }
        system.assertequals(u.Id, [Select Id, OwnerId from Case where Id =: objCase.Id].OwnerId);
        system.assertequals(objGroup.Id, [Select Id, OwnerId from Case where Id =: objCase1.Id].OwnerId);
    }

    static testMethod void testSMEQuotesAcceptNextCase()
	{
		User u = Test_HelperMethods.CreateUser('T','U','00eo0000000pteH');
		u.uwUser_Level__c = 'Level 3';
		insert u;

		final String SME_QUOTES_LISTVIEW = 'SME_Broker_UW';
		ListView lv = [
				SELECT id
				FROM listview
				WHERE DeveloperName = :SME_QUOTES_LISTVIEW
		];

		Map<String, Id> CaseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);

		Case c1;
		Case c2;

		System.runAs([Select Id from User where IsActive=true][0])
			{
				SIC_Code_Detail__c scd = new SIC_Code_Detail__c();
				scd.Referral_Level__c = 'Level 3';
				insert scd;

				SIC_Code__c sc = new SIC_Code__c();
				sc.SIC_Code_Detail__c = scd.Id;
				insert sc;

				c1 = new Case();
				c1.RecordTypeId = CaseTypes.get('CI_Open_SPRNT');
				c1.bkrCase_Subm_Type__c = 'New Business';
				c1.bkrCase_SIC_Code__c = sc.Id;
				c1.Origin = 'Broker Tool';
				c1.Status = 'New';
				c1.Offering_Project__c = 'SPRNT v1';
				c1.bkrCase_Region__c = 'Pacific';

				insert c1;

				c2 = c1.clone(false);
				insert c2;
			}
		Id caseId = null;
		System.runAs(u)
			{
				Test.setCreatedDate(c2.Id, Date.today() - 5);
				Test.startTest();
				{
					SMEQ_SingleRequestMock mockResponse = new SMEQ_SingleRequestMock(200,
							'Complete',
									'{"query": "SELECT CaseNumber, toLabel(Status), bkrCase_SIC_Code__r.Name, bkrCase_Referral_Level__c, bkrCase_Insured_Client__r.Name, Subject, toLabel(Priority), CreatedDate, Owner.Alias, SME_Sort_Order__c, SME_Underwriter_Visible__c, Id, RecordTypeId, LastModifiedDate, SystemModstamp FROM Case WHERE RecordTypeId = \'' + CaseTypes.get('CI_Open_SPRNT') + '\' AND SME_Underwriter_Visible__c = true AND Status = \'New\' ORDER BY SME_Sort_Order__c DESC NULLS LAST, Id ASC NULLS FIRST"}',
							null
					);

					Test.setMock(HttpCalloutMock.class, mockResponse);
					caseId = AcceptNextCase.acceptCase('', '', lv.Id);
				}
				Test.stopTest();
			}

		System.assertEquals(caseId, c2.Id);
	}

    static testMethod void testSMEQuotesAcceptNextCaserenewal()
    {
        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000pteH');
        u.uwUser_Level__c = 'Level 3';
        insert u;

        final String RENEWALS_QUOTES_LISTVIEW = 'Quebec_IRC_Renouvellement_Case';
        ListView lv = [
                SELECT id
                FROM listview
                WHERE DeveloperName = :RENEWALS_QUOTES_LISTVIEW
        ];

        Map<String, Id> CaseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);

        Case c1;
        Case c2;

        System.runAs([Select Id from User where IsActive=true][0])
            {
                SIC_Code_Detail__c scd = new SIC_Code_Detail__c();
                scd.Referral_Level__c = 'Level 3';
                insert scd;

                SIC_Code__c sc = new SIC_Code__c();
                sc.SIC_Code_Detail__c = scd.Id;
                insert sc;

                c1 = new Case();
                c1.RecordTypeId = CaseTypes.get('CI_Open_SPRNT');
                c1.bkrCase_Subm_Type__c = 'Renewal Re-visit';
                //c1.bkrCase_SIC_Code__c = sc.Id;
                c1.Origin = 'Broker Tool';
                c1.Status = 'New';
                c1.Offering_Project__c = 'SPRNT v1';
                c1.bkrCase_Region__c = 'Pacific';
                insert c1;

                c2 = c1.clone(false);
                insert c2;
            }
        Id caseId = null;
        System.runAs(u)
            {
                Test.setCreatedDate(c2.Id, Date.today() - 5);
                Test.startTest();
                {
                    SMEQ_SingleRequestMock mockResponse = new SMEQ_SingleRequestMock(200,
                            'Complete',
                                    '{"query": "SELECT CaseNumber, toLabel(Status), bkrCase_SIC_Code__r.Name, bkrCase_Referral_Level__c, bkrCase_Insured_Client__r.Name, Subject, toLabel(Priority), CreatedDate, Owner.Alias, SME_Sort_Order__c, SME_Underwriter_Visible__c, Id, RecordTypeId, LastModifiedDate, SystemModstamp FROM Case WHERE RecordTypeId = \'' + CaseTypes.get('CI_Open_SPRNT') + '\' AND SME_Underwriter_Visible__c = true AND Status = \'New\' ORDER BY SME_Sort_Order__c DESC NULLS LAST, Id ASC NULLS FIRST"}',
                            null
                    );

                    Test.setMock(HttpCalloutMock.class, mockResponse);
                    caseId = AcceptNextCase.acceptCase('', '', lv.Id);
                }
                Test.stopTest();
            }

        System.assertEquals(caseId, c1.Id);
    }

    static testMethod void testSMEQuotesAcceptNextCaseretention()
    {
        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000pteH');
        u.uwUser_Level__c = 'Level 3';
        insert u;

        final String RETENTION_QUOTES_LISTVIEW = 'Quebec_Retention_Case';
        ListView lv = [
                SELECT id
                FROM listview
                WHERE DeveloperName = :RETENTION_QUOTES_LISTVIEW
        ];

        Map<String, Id> CaseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);

        Case c1;
        Case c2;

        System.runAs([Select Id from User where IsActive=true][0])
            {
                SIC_Code_Detail__c scd = new SIC_Code_Detail__c();
                scd.Referral_Level__c = 'Level 3';
                insert scd;

                SIC_Code__c sc = new SIC_Code__c();
                sc.SIC_Code_Detail__c = scd.Id;
                insert sc;

                c1 = new Case();
                c1.RecordTypeId = CaseTypes.get('CI_Open_SPRNT');
                c1.bkrCase_Subm_Type__c = 'Renewal Re-visit';
                //c1.bkrCase_SIC_Code__c = sc.Id;
                c1.Origin = 'Broker Tool';
                c1.Status = 'New';
                c1.Offering_Project__c = 'SPRNT v1';
                c1.bkrCase_Region__c = 'Pacific';
                insert c1;

                c2 = c1.clone(false);
                insert c2;
            }
        Id caseId = null;
        System.runAs(u)
            {
                Test.setCreatedDate(c2.Id, Date.today() - 5);
                Test.startTest();
                {
                    SMEQ_SingleRequestMock mockResponse = new SMEQ_SingleRequestMock(200,
                            'Complete',
                                    '{"query": "SELECT CaseNumber, toLabel(Status), bkrCase_SIC_Code__r.Name, bkrCase_Referral_Level__c, bkrCase_Insured_Client__r.Name, Subject, toLabel(Priority), CreatedDate, Owner.Alias, SME_Sort_Order__c, SME_Underwriter_Visible__c, Id, RecordTypeId, LastModifiedDate, SystemModstamp FROM Case WHERE RecordTypeId = \'' + CaseTypes.get('CI_Open_SPRNT') + '\' AND SME_Underwriter_Visible__c = true AND Status = \'New\' ORDER BY SME_Sort_Order__c DESC NULLS LAST, Id ASC NULLS FIRST"}',
                            null
                    );

                    Test.setMock(HttpCalloutMock.class, mockResponse);
                    caseId = AcceptNextCase.acceptCase('', '', lv.Id);
                }
                Test.stopTest();
            }

        System.assertEquals(caseId, c2.Id);
    }

    static testMethod void testSMEQuotesAcceptNextCaseprorenewal()
    {
        User u = Test_HelperMethods.CreateUser('T','U','00eo0000000pteH');
        u.uwUser_Level__c = 'Level 3';
        insert u;

        final String PRORENEWALS_QUOTES_LISTVIEW = 'SME_Renewals_Case';
        ListView lv = [
                SELECT id
                FROM listview
                WHERE DeveloperName = :PRORENEWALS_QUOTES_LISTVIEW
        ];

        Map<String, Id> CaseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);

        Case c1;
        Case c2;

        System.runAs([Select Id from User where IsActive=true][0])
            {
                SIC_Code_Detail__c scd = new SIC_Code_Detail__c();
                scd.Referral_Level__c = 'Level 3';
                insert scd;

                SIC_Code__c sc = new SIC_Code__c();
                sc.SIC_Code_Detail__c = scd.Id;
                insert sc;

                c1 = new Case();
                c1.RecordTypeId = CaseTypes.get('CI_Open_SPRNT');
                c1.bkrCase_Subm_Type__c = 'Renewal Re-visit';
                //c1.bkrCase_SIC_Code__c = sc.Id;
                c1.Origin = 'Broker Tool';
                c1.Status = 'New';
                c1.Offering_Project__c = 'SPRNT v1';
                c1.bkrCase_Region__c = 'Pacific';
                insert c1;

                c2 = c1.clone(false);
                insert c2;
            }
        Id caseId = null;
        System.runAs(u)
            {
                Test.setCreatedDate(c2.Id, Date.today() - 5);
                Test.startTest();
                {
                    SMEQ_SingleRequestMock mockResponse = new SMEQ_SingleRequestMock(200,
                            'Complete',
                                    '{"query": "SELECT CaseNumber, toLabel(Status), bkrCase_SIC_Code__r.Name, bkrCase_Referral_Level__c, bkrCase_Insured_Client__r.Name, Subject, toLabel(Priority), CreatedDate, Owner.Alias, SME_Sort_Order__c, SME_Underwriter_Visible__c, Id, RecordTypeId, LastModifiedDate, SystemModstamp FROM Case WHERE RecordTypeId = \'' + CaseTypes.get('CI_Open_SPRNT') + '\' AND SME_Underwriter_Visible__c = true AND Status = \'New\' ORDER BY SME_Sort_Order__c DESC NULLS LAST, Id ASC NULLS FIRST"}',
                            null
                    );

                    Test.setMock(HttpCalloutMock.class, mockResponse);
                    caseId = AcceptNextCase.acceptCase('', '', lv.Id);
                }
                Test.stopTest();
            }

        System.assertEquals(caseId, c1.Id);
    }
}