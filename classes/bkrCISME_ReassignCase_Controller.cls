public with sharing class bkrCISME_ReassignCase_Controller {

    public Case c { get; set; } 
    
    public Boolean refreshPage {get; set; }
    public Boolean closePage {get; set; }
    public String queueId { get; set; } 
    public String caseId { get; set; } 
	private List<SelectOption> queueListOptions = new List<SelectOption>();
    
    public bkrCISME_ReassignCase_Controller(){ //constructor
        for(QueueSobject q : [SELECT QueueId,Queue.DeveloperName,Queue.Name 
                                FROM QueueSobject
                                WHERE Queue.Email = 'a@a.com' // using the email field as the unique identifier for CI-SME cases
                              								  // also ensures that notificaiton emails don't get sent to users of queue, ever
                                and SobjectType = 'Case'
                                ORDER BY Queue.DeveloperName]){
            queueListOptions.add(new SelectOption(q.QueueId,q.Queue.Name));
        }        
    }
    public List<SelectOption> getQueueSobjectList(){
        return queueListOptions;
    }

    public void validateCaseIds(){
        c = null;
        for(Case item : [Select Id, OwnerId from Case where Id =: caseId]){
            c = item;
        }    
        return;
    }    
    
    public void assignQueue(){
        c = new Case();
        for(Case item : [Select Id, OwnerId from Case where Id =: caseId]){
            c = item;
        }
        if(c == null ) return;
        
    	ID currentOwner = c.OwnerId;
    	refreshPage = false;
    	closePage = false;    	
    	if(queueId!=null && queueId!=''){
        	c.OwnerId = queueId;
        	try{
	        	update c;
                system.debug(c);
	    		Case c1 = [Select Id, OwnerId from Case where Id =: c.Id];
                if(c1.OwnerId != currentOwner){
					closePage = true;    
                }
	    		refreshPage = true;        	
	    	}catch(exception e){
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    		} 		
    	}
    }    

}