@RestResource(urlMapping='/saveBrokerMessage/*')
global with sharing class SMEQ_BrokerMessageRestService {
    @HttpPost
    global static SMEQ_BrokerMessageResponseModel saveBrokerMessage(SMEQ_BrokerMessageRequestModel brokerMessageRequest){
        RestResponse res = RestContext.response;
        String msgResponse = '';
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Service  >>>>>>>' + brokerMessageRequest);
        }
        SMEQ_BrokerMessageResponseModel response = new  SMEQ_BrokerMessageResponseModel();
        SMEQ_BrokerMessageDao brokerMessage = new SMEQ_BrokerMessageDao();
        try {
        	response = brokerMessage.saveBrokerMessageToCase(brokerMessageRequest);
        }
        catch(SMEQ_ServiceException se){
            response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            response.setErrors(errors);
        }
        return response;
    }
}