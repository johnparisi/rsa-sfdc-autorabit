@isTest
public class SMEQ_CustomSettingDao_Test
{

	 /***
     * Method to test getCustomSettingsByType returns valid list with a valid type parameter
     * Assertions
     * 1. Assert returned custom setting maps size is equals 4 after setUpTest values have been inserted
     */ 
    @isTest static void testGetCustomSettingsByTypeWithValidType()
    {
       
        SMEQ_CustomSettingDao cSettingSearch = new SMEQ_CustomSettingDao();
        SMEQ_CustomSettingResponseModel csResponseModel = new SMEQ_CustomSettingResponseModel();
        List<SMEQ_KeyValueModel> kvModelList = new  List<SMEQ_KeyValueModel>();
        csResponseModel = cSettingSearch.getCustomSettingsByType('livechat');
        
        //test data has four records
        System.assert(csResponseModel.customSettings.size() == 4);
    }


    /**
     * Method to test getCustomSettingsByType returns an empty list with an invalid type parameter
     * Assertions
     * 1. Assert returned custom setting maps size is > 1 after setUpTest values have been inserted
     */ 
    @isTest static void testGetCustomSettingsByTypeWithInvalidValidType()
    {
        
        SMEQ_CustomSettingDao cSettingSearch = new SMEQ_CustomSettingDao();
        SMEQ_CustomSettingResponseModel csResponseModel = new SMEQ_CustomSettingResponseModel();
        List<SMEQ_KeyValueModel> kvModelList = new  List<SMEQ_KeyValueModel>();
        csResponseModel = cSettingSearch.getCustomSettingsByType('test');
        System.assert(csResponseModel.customSettings == null);
    }

     
    /**
     * Method to test getCustomSettingsByTypeAndVariant returns a valid list 
     * when valid field type and variant parameters are specified
     * Assertions
     * 1. Assert returned custom setting maps size is > 1 after setUpTest values have been inserted
     */ 
    @isTest static void testGetCustomSettingsByTypeVariant()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_CustomSettingDao cSettingSearch = new SMEQ_CustomSettingDao();
            SMEQ_CustomSettingResponseModel csResponseModel = new SMEQ_CustomSettingResponseModel();
            List<SMEQ_KeyValueModel> kvModelList = new  List<SMEQ_KeyValueModel>();
            csResponseModel = cSettingSearch.getCustomSettingsByTypeAndVariant('livechat', 'www');
            
             //test data only has three records which have the default variant
            System.assert(csResponseModel.customSettings.size() == 3);
        }
    }
    
    /**
     * Method to test getCustomSettingsByTypeAndVariant returns a valid list 
     * when valid field type and default variant parameters are specified
     * Assertions
     * 1. Assert the three records with default variant www are returned
     */ 
    @isTest static void testGetCustomSettingsByTypeVariantForDefaultVariant()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_CustomSettingDao cSettingSearch = new SMEQ_CustomSettingDao();
            SMEQ_CustomSettingResponseModel csResponseModel = new SMEQ_CustomSettingResponseModel();
            List<SMEQ_KeyValueModel> kvModelList = new  List<SMEQ_KeyValueModel>();
            
            
            //if no empty variant parameter is specified default variant should be used to filter
            csResponseModel = cSettingSearch.getCustomSettingsByTypeAndVariant('livechat','FP');
            //test data only has three records for default variant www
            System.assert(csResponseModel.customSettings.size() == 3);
            
            //if no empty variant parameter is specified default variant should be used to filter
            csResponseModel = cSettingSearch.getCustomSettingsByTypeAndVariant('livechat','www');
            //test data only has three records for default variant www
            System.assert(csResponseModel.customSettings.size() == 3);
        }
    }
    
    /**
     * Method to test getCustomSettingsByTypeAndVariant returns a valid list 
     * when valid field type variant parameters are specified
     * Assertions
     * 1. Assert the three records with default variant www are returned
     */ 
    @isTest static void testGetCustomSettingsByTypeVariantForNonDefaultVariant()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_CustomSettingDao cSettingSearch = new SMEQ_CustomSettingDao();
            SMEQ_CustomSettingResponseModel csResponseModel = new SMEQ_CustomSettingResponseModel();
            List<SMEQ_KeyValueModel> kvModelList = new  List<SMEQ_KeyValueModel>();
            
            //variant parameter is specified
            csResponseModel = cSettingSearch.getCustomSettingsByTypeAndVariant('livechat','FP');
            
            //test data only has duplicate entries for SFDC_CHAT_BUTTON_ID
            //duplicate entry should be removed and only three records should be returned
            System.assert(csResponseModel.customSettings.size() == 3);
        }
    }
    
    /**
     * Method to test getCustomSettingsByTypeAndVariant returns a valid list 
     * when valid field type variant parameters are specified
     * Assertions
     * 1. Assert the three records with default variant www are returned
     */ 
    @isTest static void testRemoveDuplicates()
    {
        
        SMEQ_CustomSettingDao cSettingSearch = new SMEQ_CustomSettingDao();
        SMEQ_CustomSettingResponseModel csResponseModel = new SMEQ_CustomSettingResponseModel();
        List<SMEQ_KeyValueModel> kvModelList = new  List<SMEQ_KeyValueModel>();
        List<SME_Mapping__c> smemap = [SELECT externalValue__c, 
                                       SFKey__c, variant__c FROM SME_Mapping__c ];
        
        //variant parameter is specified
        smemap = cSettingSearch.removeDuplicates(smemap,'FP');
        
        //test data has duplicate entries for SFDC_CHAT_BUTTON_ID
        //duplicate entry should be removed and only three records should be returned
        System.assert(smemap.size() == 3);

    }

     
     /**
     * Add test values for SME_Mapping
     */
     @testSetup
     private static void setupTestValues() {
	    
	    //insert SME_Mapping__c Custom Settings

        SME_Mapping__c deploymentIDMapping   = new SME_Mapping__c(Name = 'Broker-livechat-01',
                                                                    Field__c = 'livechat',
                                                                    externalValue__c = '00D6300000093xH',
                                                                    SFKey__c =  'SFDC_CHAT_DEPLOYMENT_ID',
                                                                    variant__c = 'www'
                                                                );
        insert deploymentIDMapping;


        SME_Mapping__c buttonIDMapping = new SME_Mapping__c(Name='Broker-livechat-02',
                                                               Field__c = 'livechat',
                                                               externalValue__c = '573630000008ORb',
                                                               SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                               variant__c = 'www'
                                                                );
        insert buttonIDMapping;


        SME_Mapping__c buttonIDMappingFP = new SME_Mapping__c(Name='Broker-livechat-02',
                                                               Field__c = 'livechat',
                                                               externalValue__c = '573630000008ORb',
                                                               SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                               variant__c = 'FP'
                                                                );
        insert buttonIDMappingFP;

        SME_Mapping__c orgIDMapping = new SME_Mapping__c(Name='Broker-livechat-03',
                                                               Field__c = 'livechat',
                                                               externalValue__c = 'SFDC_ORG_ID',
                                                               SFKey__c = '00D6300000093xH',
                                                               variant__c = 'www'
                                                                );
        insert orgIDMapping;


     }
}