@isTest
public class SMEQ_BrokerMessageDao_Test
{
    /**
     * Method to test message sent from broker is saved to case comments
     * Assertions
     * 1. Assert related case has comment attached to it
     * 2. Assert the comment attached to the case is the comment specified in the request
     * 
     */
    @isTest static void testSaveBrokerMessageToCaseWithCaseIDAndMessageSpecified()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            Case caseToComment = new Case();
            String commentToSave = '';
            String savedComment = '';
            SMEQ_BrokerMessageRequestModel  brokerMsgRequest = new  SMEQ_BrokerMessageRequestModel();
            SMEQ_BrokerMessageResponseModel brokerMsgResponse = new SMEQ_BrokerMessageResponseModel();
            SMEQ_BrokerMessageDao brokerMessage = new SMEQ_BrokerMessageDao();
            caseToComment.bkrCase_Region__c = UserService_Test.REGION_1;
            insert caseToComment;
            
            caseToComment = [
                SELECT id, caseNumber
                FROM Case
                WHERE id = :caseToComment.id
            ];
            
            brokerMsgRequest  = buildBrokerMessageRequest(caseToComment.caseNumber, 'NB');
            brokerMsgResponse = brokerMessage.saveBrokerMessageToCase(brokerMsgRequest);
            commentToSave     = brokerMessage.buildCaseComment(brokerMsgRequest.email, brokerMsgRequest.phone, brokerMsgRequest.extension, brokerMsgRequest.comment);
            
            CaseComment caseWithComment = [
                SELECT Id, CommentBody
                FROM CaseComment
                WHERE ParentId = :caseToComment.Id
                LIMIT 1
            ];
            savedComment = caseWithComment.CommentBody;
            
            System.assert(!String.isEmpty(savedComment));
            System.assertEquals(commentToSave, savedComment);
        }
    }

    /**
     * Method to test message sent from broker is saved to case comments
     * Assertions
     * 1. Assert related case has comment attached to it
     * 2. Assert the comment attached to the case is the comment specified in the request
     * 
     */
    @isTest static void testSaveBrokerMessageToCaseWithNoCaseIDAndMessage()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            Case caseToComment = new Case();
            String commentToSave = '';
            String savedComment = '';
            SMEQ_BrokerMessageRequestModel brokerMsgRequest = new SMEQ_BrokerMessageRequestModel();
            SMEQ_BrokerMessageResponseModel brokerMsgResponse = new SMEQ_BrokerMessageResponseModel();
            SMEQ_BrokerMessageDao brokerMessage = new SMEQ_BrokerMessageDao();
            
            brokerMsgRequest = buildBrokerMessageRequest(null, 'AM');
            commentToSave = brokerMessage.buildCaseComment(brokerMsgRequest.email, brokerMsgRequest.phone, brokerMsgRequest.extension, brokerMsgRequest.comment);
            brokerMsgResponse = brokerMessage.saveBrokerMessageToCase(brokerMsgRequest);
            
            List<CaseComment> caseWithComment = [
                SELECT Id, CommentBody
                FROM CaseComment
                WHERE CommentBody = :commentToSave
            ];
            savedComment = caseWithComment[0].CommentBody;
            
            System.assertEquals(1, caseWithComment.size());
            System.assert(!String.isEmpty(savedComment));
            System.assertEquals(commentToSave, savedComment);
        }
    }

    /**
     * Method to test message sent from broker is not saved to case comment when case id is null
     * Assertions
     * 1. Assert success message is not set on response after comment has not been saved
     * 
     */
    @isTest static void testSaveBrokerMessageToCaseWithInvalidParameter()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            Case caseToComment = new Case();
            String commentToSave = '';
            String savedComment = '';
            SMEQ_BrokerMessageRequestModel  brokerMsgRequest = new  SMEQ_BrokerMessageRequestModel();
            SMEQ_BrokerMessageResponseModel brokerMsgResponse = new SMEQ_BrokerMessageResponseModel();
            SMEQ_BrokerMessageDao brokerMessage = new SMEQ_BrokerMessageDao();
            caseToComment.bkrCase_Region__c = UserService_Test.REGION_1;
            insert caseToComment;
            
            brokerMsgRequest  = buildBrokerMessageRequest(null, 'RE');
            brokerMsgRequest.comment = null;
            brokerMsgResponse = brokerMessage.saveBrokerMessageToCase(brokerMsgRequest);
            commentToSave     = brokerMsgRequest.comment;        
            
            System.assertEquals('ERROR', brokerMsgResponse.successMessage);
        }
    }


    /**
     * Method to test case comment is includes email, phone, ext, message when the parameters are specified.
     * Assertions
     * 1. Assert comment is built correctly when all parameters are present
     * 2. Assert comment is built correctly when email parameters is not present
     * 3. Assert comment is built correctly when phone parameter is not present
     * 4. Assert comment is built correctly when extension parameter is not present
     * 5. Assert comment is built correctly when message parameter is not present.
     * 
     */
    @isTest
    static void testBuildCaseComment()
    {
        String completeComment = '';
        String testComment = '';
        SMEQ_BrokerMessageDao brokerMessage = new SMEQ_BrokerMessageDao();
        
        testComment = 'Email: test@test.com' + '\n' + 'Phone: 604-400-4000' + '\n' + 'Ext: 123' + '\n' + 'Message: test comment';
        completeComment = brokerMessage.buildCaseComment('test@test.com', '604-400-4000', '123', 'test comment');
        System.debug('complete is' + completeComment + 'test com' + testComment);
        System.assert(testComment == completeComment);

        testComment = 'Phone: 604-400-4000' + '\n' + 'Ext: 123' + '\n' + 'Message: test comment';
        completeComment = brokerMessage.buildCaseComment(null, '604-400-4000', '123', 'test comment');
        System.assert(testComment == completeComment);

        testComment = 'Email: test@test.com' + '\n' + 'Ext: 123' + '\n' + 'Message: test comment';
        completeComment = brokerMessage.buildCaseComment('test@test.com', null, '123', 'test comment');
        System.assert(testComment == completeComment);

        testComment = 'Email: test@test.com' + '\n' + 'Phone: 604-400-4000' + '\n' +  'Message: test comment';
        completeComment = brokerMessage.buildCaseComment('test@test.com', '604-400-4000', null, 'test comment');
        System.assert(testComment == completeComment);

        testComment = 'Email: test@test.com' + '\n' + 'Phone: 604-400-4000' + '\n' + 'Ext: 123' + '\n';
        completeComment = brokerMessage.buildCaseComment('test@test.com', '604-400-4000', '123', null);
        System.assert(testComment == completeComment);
    }

    public static SMEQ_BrokerMessageRequestModel buildBrokerMessageRequest(String caseId, String bundle)
    {
        SMEQ_BrokerMessageRequestModel brokerMessageReq = new SMEQ_BrokerMessageRequestModel();
        brokerMessageReq.email = 'test@testmail.com';
        brokerMessageReq.phone = '604-345-1489';
        brokerMessageReq.extension = '123';
        brokerMessageReq.brokerID = '008998989';
        brokerMessageReq.caseNumber = caseId;
        brokerMessageReq.comment = 'test message to UnderWriter';
        brokerMessageReq.bundle = bundle;
        return brokerMessageReq;
    }
}