/**
 * Author: Aakash Dang
 * Created At: November 4, 2016
 * 
 * Unit tests for InvokableGetSICCodeDetails
 */
@isTest
public class InvokableGetSICCodeDetails_Test
{
    static testMethod void withoutProjectOffering()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<InvokableGetSICCodeDetails.UpdateSICDetails> lstSicDetailsWrapper = new List<InvokableGetSICCodeDetails.UpdateSICDetails>();
            InvokableGetSICCodeDetails.UpdateSICDetails obj = new InvokableGetSICCodeDetails.UpdateSICDetails();
            
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            cse.bkrCase_Region__c = null;
            cse.Offering_Project__c = null;
            update cse;
            
            obj.caseId = cse.Id;
            lstSicDetailsWrapper.add(obj);
            List<SIC_Code_Detail__c> lstSicCodes = InvokableGetSICCodeDetails.InvokableGetSICCodeDetailsForCase(lstSicDetailsWrapper);
        }
    }
    static testMethod void withoutProjectOffering2()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<InvokableGetSICCodeDetails.UpdateSICDetails> lstSicDetailsWrapper = new List<InvokableGetSICCodeDetails.UpdateSICDetails>();
            InvokableGetSICCodeDetails.UpdateSICDetails obj = new InvokableGetSICCodeDetails.UpdateSICDetails();
            SIC_Code_Detail__c scd = new SIC_Code_Detail__c();
            scd.Name = 'TEST SIC CODE DETAIL';
            scd.Referral_Level__c = 'Level 3';
            scd.Segment__c = 'Construction, Erection, Installation';
            scd.SIC_Code__c = '1234';
            insert scd;
            SIC_Code__c sc = new SIC_Code__c();
            sc.Name = 'TEST SIC CODE';
            sc.SIC_Code__c = scd.SIC_Code__c;
            sc.SIC_Code_Detail__c = scd.id;
            insert sc;
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            cse.bkrCase_Region__c = null;
            cse.Offering_Project__c = null;
            cse.bkrCase_SIC_Code__c=sc.id;
            update cse;
            
            obj.caseId =cse.id;
            lstSicDetailsWrapper.add(obj);
            List<SIC_Code_Detail__c> lstSicCodes = InvokableGetSICCodeDetails.InvokableGetSICCodeDetailsForCase(lstSicDetailsWrapper);
        }
    }
    
    
    static testMethod void withProjectOffering()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<InvokableGetSICCodeDetails.UpdateSICDetails> lstSicDetailsWrapper2 = new List<InvokableGetSICCodeDetails.UpdateSICDetails>();
            InvokableGetSICCodeDetails.UpdateSICDetails obj2 = new InvokableGetSICCodeDetails.UpdateSICDetails();
            
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase();
            
            obj2.caseId = cse.Id;
            lstSicDetailsWrapper2.add(obj2);
            
            List<SIC_Code_Detail__c> lstSicCodes2 = InvokableGetSICCodeDetails.InvokableGetSICCodeDetailsForCase(lstSicDetailsWrapper2);
        }
    }

}