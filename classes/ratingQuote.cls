public class ratingQuote {

	public List<Output> output;

	public class AggregationResults {
	}

	public class Output {
		public List<CalculationResults> calculationResults = new List<CalculationResults>();
		public AggregationResults aggregationResults  = new AggregationResults();
	}
     
	public class CalculationResults {
		public String ID ='0';
		public Double premiumBODO1=34.8;
		public Double premiumBLDB1=4.5;
		public Integer premiumEQPB1=23;
		public Double premiumCTSB1=45.3;
		public Integer premiumSTKB1=44;
		public Integer premiumSBUE1=33;
		public Integer premiumFLDE1=44;
		public Integer premiumEQKE1=43;
		public String productKey='01tc0000006ucZIAAY';
		public String parentProdKey='01tc0000006ucZIAAY';
	}

	
	public static ratingQuote parse(String json) {
		return (ratingQuote) System.JSON.deserialize(json, ratingQuote.class);
	}
}