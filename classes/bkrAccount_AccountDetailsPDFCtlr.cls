/**
 *      @author       : Merisha Shim (Deloitte)
 *      @date         : 04/22/2016
 *      @description  : Apex class to generate the Broker Profile Account PDF
 *						Controller class for bkrAccount_AccountDetailsPDF page
 *      Modification Log:
 *      ------------------------------------------------------------------------------------------
 *           Developer               User story            Date                Description
 *      ------------------------------------------------------------------------------------------
 *          Merisha Shim (Deloitte)                        04/22/2016          Original Version
 */
public with sharing class bkrAccount_AccountDetailsPDFCtlr {
	public List<Contact> keyContactsList { get; set; }
	public List<bkrCompVol__c> competitorVolumesPIList { get; set; }
	public List<bkrCompVol__c> competitorVolumesCIList { get; set; }

	private final Account acct;

	public bkrAccount_AccountDetailsPDFCtlr(ApexPages.StandardController stdController) {
		this.acct = (Account)stdController.getRecord();

		keyContactsList = [SELECT Id, Email, Title, Name 
							FROM Contact 
							WHERE AccountId =: this.acct.Id 
							AND Key_contact__c = TRUE];

		List<bkrCompVol__c> competitorVolumesList = [SELECT Name, CI_Brokerage_Volume__c, PI_Brokerage_Volume__c,
														bkrCompVol_PersonalVolume__c, bkrCompVol_CommercialVolume__c,
														bkrCompVol_CompetitorName__r.Name
														FROM bkrCompVol__c 
														WHERE bkrCompVol_BrokerageName__c =: this.acct.Id];

		seperateCompetitorVolumeLists(competitorVolumesList);

	}

	private void seperateCompetitorVolumeLists(List<bkrCompVol__c> competitorVolumesList) {
		competitorVolumesCIList = new List<bkrCompVol__c>();
		competitorVolumesPIList = new List<bkrCompVol__c>();
		for(bkrCompVol__c cn : competitorVolumesList) {
			
			if(cn.bkrCompVol_CommercialVolume__c != null) {
				competitorVolumesCIList.add(cn);
				
			} 
			if(cn.bkrCompVol_PersonalVolume__c != null) {
				competitorVolumesPIList.add(cn);
			}
		}
	}
}