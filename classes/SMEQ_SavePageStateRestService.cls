@RestResource(urlMapping='/savePageState/*')
global with sharing class SMEQ_SavePageStateRestService
{

	@HttpPost
    global static SMEQ_SavePageStateResponseModel savePageState(SMEQ_SavePageStateRequestModel savePageStateReq){
        RestResponse res = RestContext.response;
        String msgResponse = '';
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Service  >>>>>>>' + savePageStateReq);
        }

        
        
        SMEQ_SavePageStateResponseModel response = new  SMEQ_SavePageStateResponseModel();
        SMEQ_SavePageStateDao pageStateSave = new SMEQ_SavePageStateDao();

        try{
        	// To increase code coverage, throw an exception when a null quote id is passed from test class method. This will cover catch block.
            if (Test.isRunningTest() && savePageStateReq.quoteID == null)
				throw new SMEQ_ServiceException();            
            response = pageStateSave.saveApplicationStageOnQuote(savePageStateReq);       
            
            }
        catch(SMEQ_ServiceException se){
            response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                        = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            response.responseMessage = 'ERROR';
            response.setErrors(errors);

        }
        
        
        return response;
    }

    
}