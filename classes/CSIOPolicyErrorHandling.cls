public with sharing class CSIOPolicyErrorHandling {
    
    private String xml; // this is the XML policy document that is being processed
    
    private static String STATUS_SUCCESS = 'Success';
    private static STring STATUS_ERROR = 'Error';
    
    private static String ERROR_RESPONSE_CODE_PATH =             '/ca.rsagroup.domain.Response/responseCode';
    private static String ERROR_CODE_PATH =                     '/ca.rsagroup.domain.Response/errors/ca.rsagroup.domain.Error/errorCode';
    private static String ERROR_MESSAGE_FAILURE_PATH =             '/ca.rsagroup.domain.Response/errors/ca.rsagroup.domain.Error/errorMessage';
    
    public CSIOPolicyErrorHandling(String xml){
        this.xml = xml;
    }
    
    public void process() {
        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(xml);
        Dom.XMLNode soapNode = xmlDocument.getRootElement().getChildElement('Body', CSIOPolicyXMLProcessor.SOAP_NAMESPACE);
        Dom.XMLNode acordNode = soapNode.getChildElement('ACORD', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XmlNode faultNode = soapNode.getChildElement('Fault', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        
        Dom.XmlNode faultStringNode;
        if (faultNode != null){
            faultStringNode =  soapNode.getChildElement('Fault', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('faultstring', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        }
        
        Dom.XmlNode insuranceNode;
        Dom.XmlNode commlPkgPolicyQuoteNode = null; 
        Dom.XmlNode policySyncRsNode = null; 
        Dom.XmlNode msgStatusNode = null; 
        Dom.XmlNode msgStatusCdNode = null;
        Dom.XmlNode msgStatusDescNode = null;
        Dom.XmlNode descMessagesNode = null; 
    	Dom.XmlNode descMessageNode = null; 

        if (acordNode != null){
            insuranceNode = acordNode.getChildElement('InsuranceSvcRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
            if (insuranceNode != null){
                policySyncRsNode = insuranceNode.getChildElement('PolicySyncRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); //getPolicy                
                commlPkgPolicyQuoteNode = insuranceNode.getChildElement('CommlPkgPolicyQuoteInqRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); //getRate             
                if (commlPkgPolicyQuoteNode != null){   
                    msgStatusNode =  commlPkgPolicyQuoteNode.getChildElement('MsgStatus', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                    if (msgStatusNode != null){
                        msgStatusCdNode =  msgStatusNode.getChildElement('MsgStatusCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                        msgStatusDescNode = msgStatusNode.getChildElement('MsgStatusDesc', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); 
                        if (msgStatusDescNode != null) {
                            descMessagesNode  = msgStatusDescNode.getChildElement('messages', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); 
                            if (descMessagesNode != null){
                                descMessageNode = descMessagesNode.getChildElement('message', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                            }
                        }
                    } 
                }
                else if (policySyncRsNode != null){
                    msgStatusNode =  policySyncRsNode.getChildElement('MsgStatus', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                    if (msgStatusNode != null){
                        msgStatusCdNode =  msgStatusNode.getChildElement('MsgStatusCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                        msgStatusDescNode = msgStatusNode.getChildElement('MsgStatusDesc', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); 
                        if (msgStatusDescNode != null) {
                            descMessagesNode  = msgStatusDescNode.getChildElement('messages', CSIOPolicyXMLProcessor.ACORD_NAMESPACE); 
                            if (descMessagesNode != null){
                                descMessageNode = descMessagesNode.getChildElement('message', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                            }
                        }
                    }
                }
            }
        }

        Dom.XmlNode serviceNode;
        if (insuranceNode != null){
            serviceNode = insuranceNode.getChildElement('ServiceOperationType',CSIOPolicyXMLProcessor.RSA_NAMESPACE); 
        }
        
        String responsePath = XMLHelper.retrieveValue(this.xml, '/ca.rsagroup.domain.Error/responseCode');
        if (responsePath != null && responsePath != STATUS_SUCCESS){
            String responseErrorMessage = XMLHelper.retrieveValue(this.xml, 'ca.rsagroup.domain.Error/errors/errorMessage');
            String responseErrorCode = XMLHelper.retrieveValue(this.xml, 'ca.rsagroup.domain.Error/errors/errorCode');
            throw new RSA_ESBException(responseErrorCode + '-' + responseErrorMessage);
        }
        
        if (this.xml == '') {
            throw new RSA_noPolicyReturnedException('No response received from the ESB.');
        }
        
        
      	String msgStatusCdNodeText = msgStatusCdNode != null ? msgStatusCdNode.getText() : null; 
        String msgStatusDescNodeText = msgStatusDescNode != null ? msgStatusDescNode.getText() : null; 
        String descMessageNodeText = descMessageNode != null ? descMessageNode.getText() : null; 
        String errorMessageToBeReturned = null;
        
        if (descMessageNode != null){  
            //Scenario where MsgStatusCd node reads 'Success' but MsgStatusDesc node has children nodes with errors
            errorMessageToBeReturned = descMessageNodeText;
        } else if (msgStatusCdNodeText != null && msgStatusCdNodeText != STATUS_SUCCESS && (msgStatusDescNodeText.contains('3000') || msgStatusDescNodeText.contains('3001'))){ 
            //Scenario where MsgStatusCd node reads 'Error' and the MsgStatusDesc contains a 3000 'operation(s) must be reviewed by an underwriter' error, or 3001 Policy not Found
            errorMessageToBeReturned = msgStatusDescNodeText;
        }
        system.debug('%%% errorMessageToBeReturned: ' + errorMessageToBeReturned);
        if ((msgStatusCdNodeText != null && msgStatusCdNodeText != STATUS_SUCCESS && msgStatusDescNodeText.contains('3000')) || descMessageNodeText != null) { 
            throw new RSA_esbErrorFlagException(errorMessageToBeReturned); 
        } 
        
        
        if (msgStatusCdNodeText != null && msgStatusCdNodeText != STATUS_SUCCESS) { 
            //Scenario where MsgStatusCd node reads 'Error', generic ePolicy error - sets 'inaccessibleInEPolicy' or 'notFound' flag to true
            if((errorMessageToBeReturned + '').contains('3001')){
                throw new RSA_noPolicyReturnedException (errorMessageToBeReturned); 
            } else {
                throw new RSA_ErrorMessageReturnedException(msgStatusCdNodeText + ' - ' + msgStatusDescNodeText + ' ### ' + xml);
            }
        }
        
        
        String faultString = faultStringNode != null? faultStringNode.getText(): null;
        if (faultString != null) {
            throw new RSA_ErrorMessageReturnedException(faultString);
        }
        
        
        Map<String, String> esbService = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbService');
    }
}