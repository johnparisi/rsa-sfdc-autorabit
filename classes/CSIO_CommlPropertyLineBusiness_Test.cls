/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_CommlPropertyLineBusiness.
 */
@isTest
public class CSIO_CommlPropertyLineBusiness_Test
{
	@testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
	static testMethod void testConvert()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SOQLDataSet.scdv = SMEQ_TestDataGenerator.getSicCodeDetailVersion();
            
            String packageId = RiskService.PACKAGE_STANDARD;
            
            List<SOQLDataSet.Risk> rs = getRisks(packageId);
            
            CSIO_CommlPropertyLineBusiness cplb = new CSIO_CommlPropertyLineBusiness(rs);
            String output = '';
            output += cplb;
            
            System.assertEquals(
                output, 
                '<CommlPropertyLineBusiness>' +
                '<LOBCd>csio:1</LOBCd>' +
                new CSIO_CommlPropertyLineBusiness.PropertyInfo(rs) +
                '</CommlPropertyLineBusiness>'
            );
        }
    }
    
    public static List<SOQLDataSet.Risk> getRisks(String packageId)
    {
        List<SOQLDataSet.Risk> rs = new List<SOQLDataSet.Risk>();
        
        Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
        Risk__c location1 = SMEQ_TestDataGenerator.getLocationRisk(q);
        location1 = [
            SELECT id, Building_Value__c, Equipment_Value__c, Stock_Value__c, Quote__c
            FROM Risk__c
            WHERE id = :location1.Id
        ];
        
        SOQLDataSet.Risk r1 = CSIO_CommlPropertyInfo_Test.getRisk(location1, packageId);
        rs.add(r1);
        
        Risk__c location2 = SMEQ_TestDataGenerator.getLocationRisk(q);
        location2 = [
            SELECT id, Building_Value__c, Equipment_Value__c, Stock_Value__c, Quote__c
            FROM Risk__c
            WHERE id = :location2.Id
        ];
        
        SOQLDataSet.Risk r2 = CSIO_CommlPropertyInfo_Test.getRisk(location2, packageId);
        rs.add(r2);
        
        return rs;
    }
}