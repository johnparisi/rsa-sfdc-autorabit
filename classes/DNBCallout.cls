public class DNBCallout {
    // ***
    // TODO: need to move to a custom setting: Azure D&B LookupByCompanyName "Number of Records to be Returned"
    // ***      
    private static integer CONSTANT_RECORDS_TO_BE_RETURNED = 1;
    public class DNBCalloutException extends Exception {}
    
    /*
    Author: Stephen Piercey
    Date: March 23, 2017

    This calls the ESB match service and returns the ESB model of the dnb result.
    */
    public static List<dnbEntry> lookupByCompanyName(String name, String countryCode, String streetAddress, String city, String state, String postalCode, String phoneNumber) {
        List<dnbEntry> results = new List<dnbEntry>();
        SMEQ_DnBCallOut dnb = new SMEQ_DnBCallOut();
        DNBMatch matchRes = dnb.getCompanyMatch(name,streetAddress,city,state,postalCode,phoneNumber,countryCode);
        results = parseLookupByCompanyName(matchRes);
        System.debug('sep lookupByCompanyName results: ' + results);
        return results;
    }

    /*
    Author: Stephen Piercey
    Date: March 23, 2017

    This will convert the backend ESB model (DNBMatch) into the local dnbEntry class for page rendering. 
    */
    private static List<dnbEntry> parseLookupByCompanyName(DNBMatch matchRes) {
        List<dnbEntry> records = new List<dnbEntry>();
        for(DNBMatch.MatchCandidate record : matchRes.DNBResponse.GetCleanseMatchResponse.GetCleanseMatchResponseDetail.MatchResponseDetail.MatchCandidate){
          dnbEntry entry = new dnbEntry();
          System.debug('sep parseLookupByCompanyName record: ' + record);
          if(record.PrimaryAddress.StreetAddressLine != null){
            entry.Address = record.PrimaryAddress.StreetAddressLine[0].LineText;
          }
          entry.City = record.PrimaryAddress.PrimaryTownName;
          entry.State = record.PrimaryAddress.TerritoryAbbreviatedName;
          entry.ZipCode = record.PrimaryAddress.PostalCode;
          entry.Country = record.PrimaryAddress.CountryISOAlpha2Code;
          entry.DUNSNumber = record.DUNSNumber;
          entry.ConfidenceCode = '' + record.MatchQualityInformation.ConfidenceCodeValue;
          entry.Company = record.OrganizationPrimaryName.OrganizationName.d;
          records.add(entry);
        }
        return records;
    }


    /*
    Author: Stephen Piercey
    Date: March 23, 2017

    This calls the ESB append service and returns the ESB model of the dnb result.
    */
    public static dnbEntry lookupCompanyFirmographics(String dunsNumber) {
        //List<dnbEntry> results = new List<dnbEntry>();
        dnbEntry myRecord;
        SMEQ_DnBCallOut dnb = new SMEQ_DnBCallOut();
        DNBAppend matchRes = dnb.getCompanyAppend(dunsNumber);
        myRecord = parselookupCompanyFirmographics(matchRes);
        System.debug('sep lookupCompanyFirmographics myRecord: ' + myRecord);
        return myRecord;
    }  


    /*
    Author: Stephen Piercey
    Date: March 23, 2017

    This will take a DNBAppend object from the ESB services and convert it into the local class dnbEntry 
    (which is used on the triage screen and during update filmographics)

    It would make sense to just remove the dnbEntry class altogether and have the pages and update class access the ESB derived classes directly. 
    Given urgency and time constraints I have only replace the backend and am converting to the existing front end model. 

    It's a bit unoptimal to do it this way since the ESB model (DNBAppend) has many lists that can be null, that is why we need to check every
    value before assigning
    */
    private static dnbEntry parselookupCompanyFirmographics(DNBAppend dnb){
      dnbEntry record = new dnbEntry();
      System.debug('sep DNBCallout parselookupCompanyFirmographics dnb: ' + dnb);
      if(dnb.errors == null){
      
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.InquiryDetail.DUNSNumber != null){
          System.debug('sep DNBCallout parselookupCompanyFirmographics DUNSNumber ');
          record.DUNSNumber = dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.InquiryDetail.DUNSNumber;
        }
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationName.OrganizationPrimaryName != null){
          System.debug('sep DNBCallout parselookupCompanyFirmographics Company ');
          record.Company= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationName.OrganizationPrimaryName[0].OrganizationName.d;
        }
        if (dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress != null){
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress.size() == 1){
            record.SingleLocation = 'true';
          }
          else{
            record.SingleLocation = 'false';
          }
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].StreetAddressLine != null){
            System.debug('sep DNBCallout parselookupCompanyFirmographics Address ');
            record.Address= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].StreetAddressLine[0].LineText;
          }
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].PrimaryTownName != null){
            System.debug('sep DNBCallout parselookupCompanyFirmographics City ');
            record.City= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].PrimaryTownName;
          }
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].TerritoryAbbreviatedName != null){
            System.debug('sep DNBCallout parselookupCompanyFirmographics State ');
            record.State= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].TerritoryAbbreviatedName;
          }
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].PostalCode != null){
            System.debug('sep DNBCallout parselookupCompanyFirmographics ZipCode ');
            record.ZipCode= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].PostalCode;
          }
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].CountryISOAlpha2Code != null){
            System.debug('sep DNBCallout parselookupCompanyFirmographics CountryISOCode ');
            record.CountryISOCode= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].CountryISOAlpha2Code;
          }
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].CountryISOAlpha2Code != null){
            System.debug('sep DNBCallout parselookupCompanyFirmographics Country ');
            record.Country= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Location.PrimaryAddress[0].CountryISOAlpha2Code; 
          }  
        }

        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Telecommunication.FacsimileNumber != null){                           
          System.debug('sep DNBCallout parselookupCompanyFirmographics Phone ');
          record.Phone= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Telecommunication.FacsimileNumber[0].TelecommunicationNumber;
        }                              
          record.Fax= '';
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.SubjectHeader.OrganizationSummaryText != null){
          System.debug('sep DNBCallout parselookupCompanyFirmographics LocationType ');
          record.LocationType= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.SubjectHeader.OrganizationSummaryText; 
        }
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial != null){
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial.KeyFinancialFiguresOverview != null){
            if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial.KeyFinancialFiguresOverview[0].SalesRevenueAmount != null){
                System.debug('sep DNBCallout parselookupCompanyFirmographics AnnualSalesUSDollars ');
                record.AnnualSalesUSDollars= ''+ dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial.KeyFinancialFiguresOverview[0].SalesRevenueAmount[0].d;
            }
          }
        }
        
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.IndividualEntityEmployeeDetails != null){
          System.debug('sep DNBCallout parselookupCompanyFirmographics EmployeesHere ');
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.IndividualEntityEmployeeDetails.TotalEmployeeQuantity != null){
            record.EmployeesHere= ''+ dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.IndividualEntityEmployeeDetails.TotalEmployeeQuantity;
          }
        }  

        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.ConsolidatedEmployeeDetails != null){ 
          System.debug('sep DNBCallout parselookupCompanyFirmographics EmployeesTotal ');
          if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.ConsolidatedEmployeeDetails.TotalEmployeeQuantity != null){
            record.EmployeesTotal= ''+ dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.ConsolidatedEmployeeDetails.TotalEmployeeQuantity; 
          }
        }  

        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationDetail.OperatingStatusText.d != null){
          System.debug('sep DNBCallout parselookupCompanyFirmographics LegalStatus ');
          record.LegalStatus= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationDetail.OperatingStatusText.d;   
        }
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationDetail.OrganizationStartYear != null){
          System.debug('sep DNBCallout parselookupCompanyFirmographics CompanyStartYear ');
          record.CompanyStartYear= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationDetail.OrganizationStartYear;   
        }
        
        record.OutOfBusinessInd= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationDetail.OperatingStatusText.d;
        
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.ActivitiesAndOperations.LineOfBusinessDetails[0].LineOfBusinessDescription.d != null){     
          System.debug('sep DNBCallout parselookupCompanyFirmographics LineOfBusiness ');
          record.LineOfBusiness= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.ActivitiesAndOperations.LineOfBusinessDetails[0].LineOfBusinessDescription.d; 
        }     
        
        List<String> industryCodes = new List<String>();
        List<String> businessNames = new List<String>();

        System.debug('sep DNBCallout parselookupCompanyFirmographics IndustryCode ');
        for(DNBAppend.IndustryCode code : dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.IndustryCode.IndustryCode){
          System.debug('sep DNBCallout parselookupCompanyFirmographics code:  ' + code);
          industryCodes.add(code.IndustryCode.d);
        }

        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationName.TradeStyleName != null){
          for(DNBAppend.OrganizationPrimaryName tradeName: dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationName.TradeStyleName){
            if(tradeName.OrganizationName != null && tradeName.OrganizationName.d != null){
              System.debug('sep DNBCallout parselookupCompanyFirmographics tradeName.OrganizationName:  ' + tradeName.OrganizationName);
              businessNames.add(tradeName.OrganizationName.d);
            }
          }
        }

        if(industryCodes.size() >= 1){
          record.IndustryCode1= industryCodes[0];
        }
        if(industryCodes.size() >= 2){
          record.IndustryCode2= industryCodes[1]; 
        }
        if(industryCodes.size() >= 3){         
          record.IndustryCode3= industryCodes[2];  
        }
        if(industryCodes.size() >= 4){        
          record.IndustryCode4= industryCodes[3];
        }
        if(industryCodes.size() >= 5){         
          record.IndustryCode5= industryCodes[4];
        }         
        record.FormerCompanyName='';// dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization;          
        if(dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.SubjectHeader.TransferDUNSNumberRegistration != null){     
          System.debug('sep DNBCallout parselookupCompanyFirmographics PreviousDUNSNumber ');
          record.PreviousDUNSNumber= dnb.DNBResponse.OrderProductResponse.OrderProductResponseDetail.Product.Organization.SubjectHeader.TransferDUNSNumberRegistration[0].TransferredFromDUNSNumber;          
        }
        if(businessNames.size() >= 1){
          record.DoingBusinessAs1= businessNames[0];
        }
        if(businessNames.size() >= 2){
          record.DoingBusinessAs2= businessNames[1]; 
        }
        if(businessNames.size() >= 3){         
          record.DoingBusinessAs3= businessNames[2];  
        }
        if(businessNames.size() >= 4){        
          record.DoingBusinessAs4= businessNames[3];
        }
        if(businessNames.size() >= 5){         
          record.DoingBusinessAs5= businessNames[4];
        }
        record.ResponseBody = json.serialize(dnb);
      }

      return record;
    }
    
    /*
    Author: Jay
    Date: 

    SP: A local class that is used for rendering DnB results to the triage page and used in UpdateFirmofraphics.cls

    It would make more sense to eventually refactor this so the front end uses the ESB models (DNBMatch and DNBAppend) 
    */
    public class dnbEntry{ 
        public String DUNSNumber{ get; set; }
        public String Company{ get; set; }
        public String Address{ get; set; }
        public String City{ get; set; }
        public String State{ get; set; }
        public String ZipCode{ get; set; }
        public String CountryISOCode{ get; set; }
        public String Country{ get; set; }
        public String Phone{ get; set; }
        public String FAx{ get; set; }
        public String LocationType{ get; set; }
        public String ConfidenceCode{ get; set; }
        public String AnnualSalesUSDollars{ get; set; }
        public String EmployeesHere{ get; set; }
        public String EmployeesTotal{ get; set; }
        public String LegalStatus{ get; set; }
        public String CompanyStartYear{ get; set; }
        public String SingleLocation{ get; set; }
        public String OutOfBusinessInd{ get; set; }
        public String LineOfBusiness{ get; set; }
        public String IndustryCode1{ get; set; }
        public String IndustryCode2{ get; set; }
        public String IndustryCode3{ get; set; }
        public String IndustryCode4{ get; set; }
        public String IndustryCode5{ get; set; }

        public List<String> industryCodes {get;set;}

        public String FormerCompanyName{ get; set; }
        public String PreviousDUNSNumber{ get; set; }
        public String DoingBusinessAs1{ get; set; }
        public String DoingBusinessAs2{ get; set; }
        public String DoingBusinessAs3{ get; set; }
        public String DoingBusinessAs4{ get; set; }
        public String DoingBusinessAs5{ get; set; }
        public Id ExistingAccountId{ get; set; }
        
        public String ResponseBody{ get; set; }  
    }
}