/**
 * Author: James Lee
 * Created At: November 6, 2016
 * 
 * Unit tests for CSIOXMLProcessor.
 */
@isTest
public class CSIOXMLProcessor_Test
{
    static void postUserDataSetup()
    {
        SMEQ_TestDataGenerator.generateFullTestQuoteV2();
    }
    
    static Quote__c getQuote()
    {
        return [
            SELECT id, Package_Type_Selected__c, Case__r.Segment__c
            FROM Quote__c
            LIMIT 1
        ];
    }
    
	static testMethod void testProcessGetQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            
            Test.startTest();
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_QUOTE, q);
            CSIOXMLProcessor cxp = new CSIOXMLProcessor(
                CSIODataSet_Test.generateGetRateMockResponse(
                    riGetQuote.getServiceType(), 
                    riGetQuote.getPackageType(), 
                    new SOQLDataSet(q.Id, riGetQuote)
                ), 
                riGetQuote);
            
            cxp.process(q.Id);
            Test.stopTest();
        }
    }
    
    static testMethod void testProcessGet3Quote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            
            Test.startTest();
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.GET_3_QUOTE, q);
            CSIOXMLProcessor cxp = new CSIOXMLProcessor(
                CSIODataSet_Test.generateGet3RateMockResponse(
                    new SOQLDataSet(q.Id, riGetQuote)
                ), 
                riGetQuote);
            
            cxp.process(q.Id);
            Test.stopTest();
        }
    }
    
    static testMethod void testProcessBindQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            
            Test.startTest();
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.BIND_QUOTE, q);
            String strXML = CSIODataSet_Test.generateGet3RateMockResponse(new SOQLDataSet(q.Id, riGetQuote));
            strXML = strXML.replace('<rsa:ServiceOperationType>R3Q</rsa:ServiceOperationType>','<rsa:ServiceOperationType>BQ</rsa:ServiceOperationType>');
            CSIOXMLProcessor cxp = new CSIOXMLProcessor(strXML,riGetQuote);
            
            cxp.process(q.Id);
            Test.stopTest();
        }
    }
    
    static testMethod void testProcessDownloadQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            
            Test.startTest();
            RequestInfo riGetQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, q);
            CSIOXMLProcessor cxp = new CSIOXMLProcessor(
                CSIODataSet_Test.generateDownloadMockResponse(), 
                riGetQuote);
            
            cxp.process(q.Id);
            Test.stopTest();
        }
    }
    
    static testMethod void testProcessEmptyResponse()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            postUserDataSetup();
            Quote__c q = getQuote();
            
            Test.startTest();
            CSIOXMLProcessor cxp = new CSIOXMLProcessor('', null);
            
            try
            {
                cxp.process(q.Id);
                System.assert(false, 'Exception expected');
            }
            catch(RSA_ESBException ree)
            {
                
            }
            Test.stopTest();
        }
    }
}