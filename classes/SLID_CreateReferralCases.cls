/**
  * @author        Saumil Bapat
  * @date          11/16/2016
  * @description   Helper class to create renewal cases
*/
public with sharing class SLID_CreateReferralCases {
  //Name of the case referral record type
  private Static Final String REFERRAL_RECORD_TYPE_NAME = 'CI Referral';
  public static void CreateReferralCases(List<Case> newRecords, Map<Id, Case> oldRecords)
  {
  
    System.Debug('~~~ SLID_CreateReferralCases');
    System.Debug('~~~SOQL_QUERIES_ISSUED_BEGIN:SLID_CreateReferralCases: ' + Limits.getQueries());
    //If the integration log id is not set, it is not running in a webservice context
    if (Util_Logging.integrationLogId != null)
    {   
        System.Debug('~~~Integration Id is not null');
        System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_CreateReferralCases: ' + Limits.getQueries());
        return;
    }

    //Initialize a list of new cases to be created
    List<Case> childReferralCases = new List<Case>();

    //Iterate over the old cases to check the ones that moved into referred status
    for (Case newCase : newRecords)
    {
      System.Debug('~~~Referral newCase: ' + newCase);
      Case oldCase = oldRecords.get(newCase.Id);
      if (oldCase.Status != 'Referred' && newCase.Status == 'Referred')
      {
        Case childReferralCase = createChildCase(newCase);
        childReferralCases.add(childReferralCase);
      }
    }

    try
    {
      System.Debug('~~~Inserting child Referral Cases: ' + childReferralCases);
      if(childReferralCases.size() > 0)
      insert childReferralCases;
    }
    catch (Exception e)
    {
      UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_CreateReferralCases','CreateReferralCases','','Error');
      UTIL_Logging.logException(log);
      throw e;
    }
    //Debug SOQL Queries Issued
    System.Debug('~~~SOQL_QUERIES_ISSUED_END:SLID_CreateReferralCases: ' + Limits.getQueries());
  }

  private static Case createChildCase(Case parentCase)
  {
    Case childReferralCase = new Case();

    //Set Referral Record Type
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id referralRecordTypeId = caseRecordTypes.get(REFERRAL_RECORD_TYPE_NAME).getRecordTypeId();
    childReferralCase.RecordTypeId = referralRecordTypeId;

    //Set Account Id
    childReferralCase.AccountId = parentCase.AccountId;

    //Set EntitlementId
    Entitlement referralEntitlement;
    
    //Set parent case
    childReferralCase.Parent_Case__c = parentCase.Id;
    
    try 
    {
        referralEntitlement = [Select Id from Entitlement limit 1];
        childReferralCase.EntitlementId = referralEntitlement.Id;
    }
    catch (Exception e)
    {
      UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_CreateReferralCases','createChildCase','','Error');
      UTIL_Logging.logException(log);
      //throw e;
      System.Debug('~~~Entitlement not found');
    }
    
    //Set Status
    childReferralCase.Status = 'In-Progress';

    //Set Parent Case
    childReferralCase.parentId = parentCase.Id;
    
    //Return child case
    return childReferralCase;
  }
}