/**
 * ResultSet
 * Author: James Lee
 * Date: January 10 2017
 * Feature: FP-748
 * 
 * The purpose of this class is to serve as the response of a validation check.
 */
public class ResultSet
{
	private List<ProcessingResult> results;
    
    private Integer errorCount;
    
    public ResultSet()
    {
        this.results = new List<ProcessingResult>();
        this.errorCount = 0;
    }
    
    public void addResult(ProcessingResult pr)
    {
        this.results.add(pr);
        errorCount += pr.getErrors().size();
    }
    
    public List<ProcessingResult> getResults()
    {
        return this.results;
    }
    
    public Integer getErrorCount()
    {
        return this.errorCount;
    }
}