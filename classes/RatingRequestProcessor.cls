global class RatingRequestProcessor implements vlocity_ins.VlocityOpenInterface2 {
    
    global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options) {
        Boolean result = true;
        try {
            if (methodName.equals('getRatingRequest')) {
                getRatingRequest(inputs, output);
            }
            else {
                result = false;
            }
        }catch (Exception e) {
            System.debug('exception: ' + e + ' line no' + e.getLineNumber());
            result = false;
        }
        return result;
    }
    
    public void getRatingRequest(Map<String,Object> inputMap, Map<String,Object> outMap) {
        system.debug('inputMap::'+JSON.serializePretty(inputMap));
        
        List<Object> getinput_1 = (List<Object>)inputMap.get('input_1');
        system.debug('getinput_1::'+getinput_1);
        
        Object parseInput = getinput_1.get(0);
        system.debug('parseInput::'+parseInput);
        
        Map<String,Object> pinputMap = new Map<String,Object>();
        pinputMap.put('ratingInfo',parseInput);
        
        Map<String,Object> ratingResults = (Map<String,Object>)pinputMap.get('ratingInfo');
        
        String requestType = String.valueOf(ratingResults.get('requestType'));
        system.debug('requestType::'+requestType);
        
        String amountCAD = String.valueOf(ratingResults.get('SmallBusiness.numberStories'));
        system.debug('amountCAD::'+amountCAD);
        
        String businessstartYr = String.valueOf(ratingResults.get('SmallBusiness.yearBuilt'));
        system.debug('businessstartYr::'+businessstartYr);
        
        List<locationWrapper> locwrapList = new List<locationWrapper>();
        
        Map<String,Object> customNode1 = (Map<String,Object>)ratingResults.get('custom_node_1');
        //Map<String,Object> customNode2 = (Map<String,Object>)ratingResults.get('custom_node_2');
        
        locationWrapper locWrap = new locationWrapper ();
        
        locWrap.HERPUpgrades = String.valueOf(customNode1.get('HERPUpgrades'));
        
        if(locWrap.HERPUpgrades == 'Yes'){       
            Map<String,Object> HERFResults = (Map<String,Object>) customNode1.get('AdditionalHERPQuestions');
            locWrap.HERPeUpgrade = String.valueOf(HERFResults.get('LastElectricalUpgrade'));
            locWrap.HERPhUpgrade = String.valueOf(HERFResults.get('LastHeatingUpgrade'));
            locWrap.HERPpUpgrade = String.valueOf(HERFResults.get('LastPlumbingUpgrade'));
            locWrap.HERPrUpgrade = String.valueOf(HERFResults.get('LastRoofUpgrade'));
        }
        
        locWrap.BuiltYear = String.valueOf(customNode1.get('BuiltYear'));
        locWrap.TotalArea = String.valueOf(customNode1.get('TotalArea'));
        locWrap.NumberofStories = String.valueOf(customNode1.get('NumberofStories'));
        locWrap.ReplacementValue = String.valueOf(customNode1.get('ReplacementValue'));
        locWrap.BuildingType = String.valueOf(customNode1.get('BuildingType'));
        locWrap.WallType = String.valueOf(customNode1.get('WallType'));
        locWrap.RoofType = String.valueOf(customNode1.get('RoofType'));
          
        locWrap.FireSprinkler = String.valueOf(customNode1.get('FireSprinkler'));
          
        locWrap.FireHydrantDistance = String.valueOf(customNode1.get('FireHydrantDistance'));
        locWrap.FireHallDistance = String.valueOf(customNode1.get('FireHallDistance')); 
          
        locWrap.TotalValueOfStock = String.valueOf(customNode1.get('TotalValueOfStock'));
        locWrap.TotalValueOfEquipment = String.valueOf(customNode1.get('TotalValueOfEquipment'));

        locWrap.Province = String.valueOf(customNode1.get('Province0'));
        locWrap.PostalCode = String.valueOf(customNode1.get('PostalCode0'));
        locWrap.City = String.valueOf(customNode1.get('City0'));
        locWrap.RegisteredBusinessAddress = String.valueOf(customNode1.get('RegisteredBusinessAddress0'));
        locWrap.RegisteredBusinessName = String.valueOf(customNode1.get('RegisteredBusinessName0'));
        
        locWrap.locNum = 'Location1';
        
        locwrapList.add(locWrap);
        
        Map<String,Object> customNode2 = (Map<String,Object>)ratingResults.get('custom_node_2');
        
        if(customNode2 != null){
            
            locationWrapper locWrap1 = new locationWrapper();
            
            locWrap1.HERPUpgrades = String.valueOf(customNode2.get('HERPUpgrades1'));
            if(locWrap1.HERPUpgrades == 'Yes'){    
                Map<String,Object> HERFResults1 = (Map<String,Object>) customNode2.get('AdditionalHERPQuestions1');
                locWrap1.HERPeUpgrade = String.valueOf(HERFResults1.get('LastElectricalUpgrade1'));
                locWrap1.HERPhUpgrade = String.valueOf(HERFResults1.get('LastHeatingUpgrade1'));
                locWrap1.HERPpUpgrade = String.valueOf(HERFResults1.get('LastPlumbingUpgrade1'));
                locWrap1.HERPrUpgrade = String.valueOf(HERFResults1.get('LastRoofUpgrade1'));
            }
            
            locWrap1.BuiltYear = String.valueOf(customNode2.get('BuiltYear1'));
            locWrap1.TotalArea = String.valueOf(customNode2.get('TotalArea1'));
            locWrap1.NumberofStories = String.valueOf(customNode2.get('NumberofStories1'));
            locWrap1.ReplacementValue = String.valueOf(customNode2.get('ReplacementValue1'));
            locWrap1.BuildingType = String.valueOf(customNode2.get('BuildingType1'));
            locWrap1.WallType = String.valueOf(customNode2.get('WallType1'));
            locWrap1.RoofType = String.valueOf(customNode2.get('RoofType1'));
              
            
              
            locWrap1.FireSprinkler = String.valueOf(customNode2.get('FireSprinkler1'));
              
            locWrap1.FireHydrantDistance = String.valueOf(customNode2.get('FireHydrantDistance1'));
            locWrap1.FireHallDistance = String.valueOf(customNode2.get('FireHallDistance1')); 
              
            locWrap1.TotalValueOfStock = String.valueOf(customNode2.get('TotalValueOfStock1'));
            locWrap1.TotalValueOfEquipment = String.valueOf(customNode2.get('TotalValueOfEquipment1'));
    
            locWrap1.Province = String.valueOf(customNode2.get('Province1'));
            locWrap1.PostalCode = String.valueOf(customNode2.get('PostalCode1'));
            locWrap1.City = String.valueOf(customNode2.get('City1'));
            locWrap1.RegisteredBusinessAddress = String.valueOf(customNode2.get('RegisteredBusinessAddress1'));
            locWrap1.RegisteredBusinessName = String.valueOf(customNode2.get('RegisteredBusinessName1'));
            
            locWrap1.locNum = 'Location2';
            
            locwrapList.add(locWrap1);
        }
        //List<Object> locationVal = (List<Object>)(customNode.get('VLOCITY-FORMULA-LIST'));
        system.debug('locationVal::'+locwrapList);
        //system.debug('locationVal2::'+customNode2);
        
        
        requestProcessor2Json.cls_elementValueMap r3q = retrieveData(locwrapList,amountCAD,businessstartYr,requestType);
        String json = JSON.serialize(r3q);
        system.debug('json::'+System.JSON.serializePretty(r3q));
        outMap.put('reqR3Q',json);
        
    }
    
    public requestProcessor2Json.cls_elementValueMap retrieveData(List<locationWrapper> locWrapper,String amountCAD,String businessstartYr,String requestType){
          requestProcessor2Json r3q = new requestProcessor2Json();
          requestProcessor2Json.cls_elementValueMap elementMap = new requestProcessor2Json.cls_elementValueMap();
          if(requestType == 'R3Q')
              elementMap.RqUID = '36666362-3430-3135-6163-626166323838';
          else
              elementMap.RqUID = '34306439-3935-3733-3861-643062336131';
          elementMap.SignonRq_SignonTransport_CustId_SPName = 'rsagroup.ca';
          elementMap.SignonRq_SignonTransport_CustId_CustPermId = 'XQATB01';
          //elementMap.SignonRq_ClientDt = Datetime.now().format('yyyy-mm-dd\'T\'HH:mm:ss');
          elementMap.SignonRq_ClientDt = Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss');
          elementMap.SignonRq_CustLangPref = 'en';
          elementMap.SignonRq_ClientApp_Org = UserInfo.getOrganizationId();
          elementMap.SignonRq_ClientApp_Name = 'BDIG';
          elementMap.SignonRq_ClientApp_Version = 1;
          elementMap.SignonRq_rsa_QuoteSource = 'BDIG';
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CurCd = 'CAD';
          if(requestType == 'R3Q'){
              elementMap.InsuranceSvcRq_ServiceOperationType = 'R3Q';
          }
          else{
              elementMap.InsuranceSvcRq_ServiceOperationType = 'RQ';
          }
          elementMap.InsuranceSvcRq_Context = '3b95703a-2237-4d64-89d9-d040f69a5ad1'; 
          elementMap.PackageSelected = '01tc0000006ukxMAAQ'; //TBU
          
          /* Producer */
          
          requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer producer = 
              new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer();
          
              requestProcessor2Json.cls_GeneralPartyInfo generalpartyInfo = new requestProcessor2Json.cls_GeneralPartyInfo();
              
              /* add nameInfo */
              
              requestProcessor2Json.cls_NameInfo nameInfo = new requestProcessor2Json.cls_NameInfo();
              
              /* add commlName */
              
              requestProcessor2Json.cls_CommlName commlName = new requestProcessor2Json.cls_CommlName();
              commlName.CommercialName = 'BLUENOSE INSURANCE';
              
              nameInfo.CommlName = commlName;
              GeneralPartyInfo.NameInfo = nameInfo;
              
              /* add Addr */
              
              requestProcessor2Json.cls_Addr Addr = new requestProcessor2Json.cls_Addr();
              
              requestProcessor2Json.cls_DetailAddr detailAddr = new requestProcessor2Json.cls_DetailAddr();
              detailAddr.StreetName = 'Lawrence Ave E';
              detailAddr.StreetNumber = '1441';
              
              Addr.DetailAddr  = detailAddr;
              Addr.StateProvCd = 'ON';
              Addr.CountryCd = 'CA';
              Addr.PostalCode = 'M4A 1W3';
              Addr.City = 'North York';
              
              GeneralPartyInfo.Addr = Addr;
              
              /* add producerInfo */
              
              requestProcessor2Json.cls_ProducerInfo producerInfo = new requestProcessor2Json.cls_ProducerInfo();
              producerInfo.ProducerRoleCd = 'Broker';
              producerInfo.ContractNumber = '52497052';
              
              /* add itemidInfo */
              
              requestProcessor2Json.cls_ItemIdInfo itemidInfo = new requestProcessor2Json.cls_ItemIdInfo();
              itemidInfo.AgencyId = 'XQATB01';
              
              producer.GeneralPartyInfo = generalpartyInfo;
              producer.ProducerInfo = producerInfo;
              producer.ItemIdInfo = itemidInfo;
              
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Producer = producer;
          
          /* InsuredOrPrincipal */
          
          requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal insuredOrprincipal = 
              new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal();
              
              /* add credit score */
              requestProcessor2Json.cls_CreditScoreInfo creditscoreInfo = 
                  new requestProcessor2Json.cls_CreditScoreInfo();
              
              creditscoreInfo.ReferenceNumber = '202075561';
              creditscoreInfo.CreditScore = '1';
              creditscoreInfo.CreditScoreDt = '2019-05-27';
              
              insuredOrprincipal.CreditScoreInfo = creditscoreInfo;
              
              /* add insured or principal Info */
              
              requestProcessor2Json.cls_InsuredOrPrincipalInfo insuredorprincipalInfo = 
                  new requestProcessor2Json.cls_InsuredOrPrincipalInfo();
              
              /* add business Info */
              
              requestProcessor2Json.cls_BusinessInfo businessInfo = 
                  new requestProcessor2Json.cls_BusinessInfo();
                  
              requestProcessor2Json.cls_Area area = new requestProcessor2Json.cls_Area();
              area.UnitMeasurementCd = 'SqFt';
              Integer NumUnits = 0;
              for(locationWrapper lw : locWrapper){
                  NumUnits += Integer.ValueOf(lw.TotalArea);
              }
              system.debug('NumUnits::'+NumUnits);
              area.NumUnits = String.valueOf(NumUnits);
              
              businessInfo.Area = area;
              businessInfo.OperationsDesc = 'Lawyers [8111]';
              //businessInfo.BusinessStartDt = '2000';
              businessInfo.BusinessStartDt = businessstartYr;
              businessInfo.SICCd = '8111';
              
              insuredorprincipalInfo.BusinessInfo = businessInfo;
              
              insuredOrprincipal.InsuredOrPrincipalInfo = insuredorprincipalInfo;
              
              requestProcessor2Json.cls_GeneralPartyInfo generalpartyInfo1 = new requestProcessor2Json.cls_GeneralPartyInfo();
              
              /* add nameInfo */
              
              requestProcessor2Json.cls_NameInfo nameInfo1 = new requestProcessor2Json.cls_NameInfo();
              
              /* add commlName */
              
              requestProcessor2Json.cls_CommlName commlName1 = new requestProcessor2Json.cls_CommlName();
              commlName1.CommercialName = 'BLUENOSE INSURANCE';
              
              nameInfo1.CommlName = commlName1;
              GeneralPartyInfo1.NameInfo = nameInfo1;
              
              /* add Addr */
              
              requestProcessor2Json.cls_Addr Addr1 = new requestProcessor2Json.cls_Addr();
              
              requestProcessor2Json.cls_DetailAddr detailAddr1 = new requestProcessor2Json.cls_DetailAddr();
              detailAddr1.StreetName = 'Lawrence Ave E';
              detailAddr1.StreetNumber = '1441';
              
              Addr1.DetailAddr  = detailAddr1;
              Addr1.StateProvCd = 'ON';
              Addr1.CountryCd = 'CA';
              Addr1.PostalCode = 'M4A 1W3';
              Addr1.City = 'North York';
              
              GeneralPartyInfo1.Addr = Addr1;
              
              insuredOrprincipal.GeneralPartyInfo = GeneralPartyInfo1;
    
              /* add itemidInfo */
              
              requestProcessor2Json.cls_ItemIdInfo itemidInfo1 = new requestProcessor2Json.cls_ItemIdInfo();
              itemidInfo1.AgencyId = '0011700000ri5vMAAQ';
              
              insuredOrprincipal.ItemIdInfo = itemidInfo1;
          
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_InsuredOrPrincipal = insuredOrprincipal;
          
          /* comml Policy */
          
          requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy commlPolicy = 
              new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy();
          
              commlPolicy.id = 'quote';
              commlPolicy.SalesforceCaseNumber = '01389384';
              commlPolicy.Segment = 'BPS';
              
              /* added for RQ */
              if(requestType == 'RQ'){
                  commlPolicy.Flavour = 'FL2';
              }
              
              commlPolicy.CompanyCd = 'ROY';
              commlPolicy.LanguageCd = 'en';
              commlPolicy.LOBCd = 'csio:1';
              
              requestProcessor2Json.cls_AnnualSalesAmt annualsalesAmt = new requestProcessor2Json.cls_AnnualSalesAmt();
              annualsalesAmt.Amt = amountCAD;
              
              requestProcessor2Json.cls_CommlPolicySupplement commlpolicySupplement = new requestProcessor2Json.cls_CommlPolicySupplement();
              commlpolicySupplement.AnnualSalesAmt = annualsalesAmt;
              
              requestProcessor2Json.cls_Limit Limits = new requestProcessor2Json.cls_Limit();
              
              requestProcessor2Json.cls_FormatCurrencyAmt formatCurrencyAmt = new requestProcessor2Json.cls_FormatCurrencyAmt();
              formatCurrencyAmt.Amt = '25000';
              
              Limits.FormatCurrencyAmt = formatCurrencyAmt;
              
              requestProcessor2Json.cls_CommlCoverage commlCoverage = new requestProcessor2Json.cls_CommlCoverage();
              /*commlCoverage.CoverageCd = 'csio:Contents';
              commlCoverage.CoverageDesc = 'Contents';
              commlCoverage.Indicator = '1';
              commlCoverage.Limits = Limits;*/
              
              requestProcessor2Json.cls_CommlCoverageSupplement commlcoverageSupplement = 
                  new requestProcessor2Json.cls_CommlCoverageSupplement();
              
              requestProcessor2Json.cls_ClaimsMadeInfo claimsmadeInfo = new requestProcessor2Json.cls_ClaimsMadeInfo();
              
              claimsmadeInfo.ClaimsMadeInd = '0';
              
              commlcoverageSupplement.ClaimsMadeInfo = claimsmadeInfo;
              commlCoverage.CommlCoverageSupplement = commlcoverageSupplement;
              
              commlPolicy.CommlPolicySupplement = commlpolicySupplement;
              commlPolicy.CommlCoverage = commlCoverage;
          
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPolicy = commlPolicy;
          
          /* Location */
          
          List<requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location> locationList = 
              new List<requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location>();
          
          for(locationWrapper l: locWrapper){
          
              requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location location = 
                  new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location();
              
                  requestProcessor2Json.cls_ItemIdInfo itemidInfo2 = new requestProcessor2Json.cls_ItemIdInfo();
                  itemidInfo2.AgencyId = '0011700000ri5vMAAQ';
                  
                  requestProcessor2Json.cls_Addr Addr2 = new requestProcessor2Json.cls_Addr();
                  
                  requestProcessor2Json.cls_DetailAddr detailAddr2 = new requestProcessor2Json.cls_DetailAddr();
                  detailAddr2.StreetName = l.RegisteredBusinessAddress;
                  detailAddr2.StreetNumber = '1441';
                  
                  Addr2.StateProvCd = l.Province;
                  Addr2.CountryCd = 'CA';
                  Addr2.PostalCode = l.PostalCode;
                  Addr2.City = l.city;
                  Addr2.detailAddr = detailAddr2;
                  
                  location.ItemIdInfo = itemidInfo2;
                  location.Addr = Addr2;
                  location.Deleted = '0';
                  location.id = l.locNum;
              
              locationList.add(location);
          } 
          
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_Location = locationList;
          
          /* CommlSubLocation */
          
          List<requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation> commlsublocationList = 
              new List<requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation>();
          
          for(locationWrapper sl: locWrapper){
              
              requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation commlsublocation = 
                  new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation();
    
              requestProcessor2Json.cls_Construction construction = new requestProcessor2Json.cls_Construction();
              
              construction.YearBuilt = sl.BuiltYear;
              construction.NumStories = sl.NumberofStories;
              construction.BldgConstructionCd = '1';
              
              requestProcessor2Json.cls_BldgImprovements bldgImprovement = new requestProcessor2Json.cls_BldgImprovements();
              
              bldgImprovement.HeatingImprovementYear = sl.HERPhUpgrade;
              bldgImprovement.PlumbingImprovementYear = sl.HERPpUpgrade;
              bldgImprovement.RoofingImprovementYear = sl.HERPrUpgrade;
              bldgImprovement.WiringImprovementYear = sl.HERPeUpgrade;
                  
              requestProcessor2Json.cls_BldgProtection bldgProtection = new requestProcessor2Json.cls_BldgProtection();
              
              requestProcessor2Json.cls_DistanceToFireStation distancetofireStation = 
                  new requestProcessor2Json.cls_DistanceToFireStation();
                  
              distancetofireStation.NumUnits = sl.FireHallDistance;
              distancetofireStation.UnitMeasurementCd = 'KMT';
              
              requestProcessor2Json.cls_DistanceToHydrant distanceToHydrant = new requestProcessor2Json.cls_DistanceToHydrant();
              distanceToHydrant.NumUnits = sl.FireHydrantDistance;
              distanceToHydrant.UnitMeasurementCd = 'MTR';
              
              bldgProtection.DistanceToFireStation = distancetofireStation ;
              bldgProtection.DistanceToHydrant = distanceToHydrant;
              String spklr = '';
              if(sl.FireSprinkler == 'Yes'){
                  bldgProtection.SprinkleredPct = '100';
              }
              else{
                  bldgProtection.SprinkleredPct = '0';
              }
              
              requestProcessor2Json.cls_BldgOccupancy bldgOccupancy = new requestProcessor2Json.cls_BldgOccupancy();
              
              requestProcessor2Json.cls_AreaOccupied areaOccupied = new requestProcessor2Json.cls_AreaOccupied();
              areaOccupied.NumUnits = sl.TotalArea;
              areaOccupied.UnitMeasurementCd = 'FTK';
              
              bldgOccupancy.AreaOccupied = areaOccupied;
              
              List<requestProcessor2Json.cls_AlarmAndSecurity> alarmandSecurityList = new List<requestProcessor2Json.cls_AlarmAndSecurity>();
    
              for(Integer i=1;i<3;i++){
                  requestProcessor2Json.cls_AlarmAndSecurity alarmandSecurity = new requestProcessor2Json.cls_AlarmAndSecurity();
                  alarmandSecurity.AlarmDescCd = 'csio:998';
                  alarmandSecurity.AlarmTypeCd = 'csio:'+i;
                  alarmandSecurityList.add(alarmandSecurity);
              }
              
                
              
              /*alarmandSecurity.AlarmDescCd = 'csio:998';
              alarmandSecurity.AlarmTypeCd = 'csio:2';    
              
              alarmandSecurityList.add(alarmandSecurity);  */   
              
              commlsublocation.Construction = construction;
              commlsublocation.BldgImprovements = bldgImprovement;
              commlsublocation.BldgProtection = bldgProtection;
              commlsublocation.BldgOccupancy = bldgOccupancy;
              commlsublocation.AlarmAndSecurity = alarmandSecurityList;
              commlsublocation.LocationRef = sl.locNum;
              
              commlsublocationList.add(commlsublocation);
          }
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlSubLocation = commlsublocationList;
          
          /* CommlPropertyLineBusiness */
          
          requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness commlpropertylineBusiness =  
              new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness();
          
          requestProcessor2Json.cls_PropertyInfo propertyInfo = new requestProcessor2Json.cls_PropertyInfo();
          
          List<requestProcessor2Json.cls_CommlPropertyInfo> commlpropertyInfoList = new List<requestProcessor2Json.cls_CommlPropertyInfo>();
          
          for(locationWrapper pl: locWrapper){

              requestProcessor2Json.cls_CommlPropertyInfo commlpropertyInfo = new requestProcessor2Json.cls_CommlPropertyInfo();
              
              /* requestProcessor2Json.cls_Limit Limits1 = new requestProcessor2Json.cls_Limit();
              
              requestProcessor2Json.cls_FormatCurrencyAmt formatCurrencyAmt1 = new requestProcessor2Json.cls_FormatCurrencyAmt();
              formatCurrencyAmt1.Amt = '25000';
              
              Limits1.FormatCurrencyAmt = formatCurrencyAmt1;*/
              String [] strCoverage = new String[]{};
              
              if(requestType == 'R3Q'){
                  strCoverage = new String[]{'Contents','Stock','Equipment','Building'};
              }
              else{
                  strCoverage = new String[]{'Contents','Stock','Equipment','Building','Earthquake','Flood','SewerBackup'};
              }
              
              List<requestProcessor2Json.cls_CommlCoverage> commlCoverageList = new List<requestProcessor2Json.cls_CommlCoverage>();
              
              /*String stockAmt = pl.TotalValueOfStock;
              String equipmentAmt = pl.TotalValueOfEquipment;
              String contentAmt = String.valueOf(Integer.valueOf(pl.TotalValueOfEquipment)+Integer.valueOf(pl.TotalValueOfStock));
              String buildingAmt = pl.ReplacementValue;*/
              
              for(String c : strCoverage){
              
                  String amount = '';
                  if(c == 'Stock'){
                      amount = pl.TotalValueOfStock;
                  }
                  if(c == 'Equipment'){
                      amount = pl.TotalValueOfEquipment;
                  }
                  if(c == 'Building'){
                      amount = pl.ReplacementValue;
                  }
                  if(c == 'Contents'){
                      amount = String.valueOf(Integer.valueOf(pl.TotalValueOfEquipment)+Integer.valueOf(pl.TotalValueOfStock));
                  }
                  
                  requestProcessor2Json.cls_Limit Limits1 = new requestProcessor2Json.cls_Limit();
                  
                  requestProcessor2Json.cls_Deductible Deductible = new requestProcessor2Json.cls_Deductible(); // Added to RQ
                  
                  if(c != 'Earthquake' && c != 'Flood' && c != 'SewerBackup'){
                  
                      requestProcessor2Json.cls_FormatCurrencyAmt formatCurrencyAmt1 = new requestProcessor2Json.cls_FormatCurrencyAmt();
                      formatCurrencyAmt1.Amt = amount;
                      
                      Limits1.FormatCurrencyAmt = formatCurrencyAmt1;
                      
                      /* Added for RQ */

                      if(requestType == 'RQ'){
    
                          requestProcessor2Json.cls_FormatCurrencyAmt formatCurrencyAmt2 = new requestProcessor2Json.cls_FormatCurrencyAmt();
                          formatCurrencyAmt2.Amt = '2500';
                          
                          Deductible.FormatCurrencyAmt = formatCurrencyAmt2;
                      
                      }
                  
                  }
                  
                  requestProcessor2Json.cls_CommlCoverage commlCoverage1 = new requestProcessor2Json.cls_CommlCoverage();

                  commlCoverage1.CoverageCd = 'csio:'+c;

                  if(c == 'SewerBackup'){
                      commlCoverage1.CoverageDesc = 'Sewer Backup';
                  }
                  else{
                      commlCoverage1.CoverageDesc = c;
                  }
                  if(c != 'Earthquake' && c != 'Flood' && c != 'SewerBackup'){
                      commlCoverage1.Indicator = '1';
                  }
                  else{
                      commlCoverage1.Indicator = '0';
                  }
                  
                  if(c != 'Earthquake' && c != 'Flood' && c != 'SewerBackup'){
                      commlCoverage1.Limits = Limits1;
                  }
                  
                  if(requestType == 'RQ'){
                      if(c != 'Earthquake' && c != 'Flood' && c != 'SewerBackup'){
                          commlCoverage1.Deductible = Deductible;
                      }
                  }
                  
                  commlCoverageList.add(commlCoverage1);
              }
              commlpropertyInfo.CommlCoverage = commlCoverageList;
              commlpropertyInfo.LocationRef = pl.locNum;
              
              commlpropertyInfoList.add(commlpropertyInfo);
              }
              propertyInfo.CommlPropertyInfo = commlpropertyInfoList;
              commlpropertylineBusiness.PropertyInfo = propertyInfo;
              //commlpropertylineBusiness.LOBCd = 'csio:1';
          
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_CommlPropertyLineBusiness = commlpropertylineBusiness;
          
          /* GeneralLiabilityLineBusiness */
          
          requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness generalliabilitylineBusiness = 
              new requestProcessor2Json.cls_InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness();
              
          requestProcessor2Json.cls_LiabilityInfo liabilityInfo = new requestProcessor2Json.cls_LiabilityInfo();
          
          /* Added for RQ */
          //commlPackage
          if(requestType == 'RQ'){
              requestProcessor2Json.cls_CommlCoverage commlCoverageGL = new requestProcessor2Json.cls_CommlCoverage();
              
              requestProcessor2Json.cls_Limit LimitsGL = new requestProcessor2Json.cls_Limit();
                      
              requestProcessor2Json.cls_Deductible DeductibleGL = new requestProcessor2Json.cls_Deductible(); // Added to RQ
    
              
              requestProcessor2Json.cls_FormatCurrencyAmt formatCurrencyAmtGL = new requestProcessor2Json.cls_FormatCurrencyAmt();
              formatCurrencyAmtGL.Amt = '1000000';
              
              LimitsGL.FormatCurrencyAmt = formatCurrencyAmtGL;
    
              requestProcessor2Json.cls_FormatCurrencyAmt formatCurrencyAmtGL1 = new requestProcessor2Json.cls_FormatCurrencyAmt();
              formatCurrencyAmtGL1.Amt = '1000';
              
              DeductibleGL.FormatCurrencyAmt = formatCurrencyAmtGL1;
              
              commlCoverageGL.Deductible = DeductibleGL;
              commlCoverageGL.Limits = LimitsGL;
              commlCoverageGL.CoverageCd = 'csio:BIPD';
              commlCoverageGL.CoverageDesc = 'Commercial General Liability - Occurrence';
              commlCoverageGL.Indicator = '1';
              liabilityInfo.commlCoverage = commlCoverageGL;
          }
          
          List<requestProcessor2Json.cls_GeneralLiabilityClassification> generalliabilityClassificationList = 
              new List<requestProcessor2Json.cls_GeneralLiabilityClassification>();
          
          List<String> PremiumBasisCdList = new List<String>{'csio:6-CA','csio:6-ZZ','csio:6-US','csio:7','rsa:food'};
          
          requestProcessor2Json.cls_GeneralLiabilityClassification generalliabilityClassification = 
                  new requestProcessor2Json.cls_GeneralLiabilityClassification();
          
          //for(String s: PremiumBasisCdList){

              requestProcessor2Json.cls_ExposureInfo exposureInfo = new requestProcessor2Json.cls_ExposureInfo();
              
              /*if(s == 'csio:6-CA'){
                  exposureInfo.PremiumBasisCd = 'csio:6';
                  exposureInfo.Exposure = amountCAD;
                  generalliabilityClassification.TerritoryCd = 'CA';
              }
              if(s == 'csio:6-ZZ'){
                  exposureInfo.PremiumBasisCd = 'csio:6';
                  exposureInfo.Exposure = '0';
                  generalliabilityClassification.TerritoryCd = 'ZZ';
              }
              if(s == 'csio:6-US'){
                  exposureInfo.PremiumBasisCd = 'csio:6';
                  exposureInfo.Exposure = '0';
                  generalliabilityClassification.TerritoryCd = 'US';
              }
              if(s == 'csio:7'){
                  exposureInfo.PremiumBasisCd = 'csio:7';
                  exposureInfo.Exposure = '0';
                  generalliabilityClassification.TerritoryCd = '';
              }
              if(s == 'rsa:food'){
                  exposureInfo.PremiumBasisCd = 'rsa:food';
                  exposureInfo.Exposure = '0';
                  generalliabilityClassification.TerritoryCd = '';
              }*/
              
              exposureInfo.PremiumBasisCd = 'csio:6';
              exposureInfo.Exposure = amountCAD;
              
              generalliabilityClassification.TerritoryCd = 'CA';
              
              generalliabilityClassification.ExposureInfo = exposureInfo;
              
              generalliabilityClassificationList.add(generalliabilityClassification);
          //}
              
              /* ZZ */
              
              generalliabilityClassification = 
                  new requestProcessor2Json.cls_GeneralLiabilityClassification();

              exposureInfo = new requestProcessor2Json.cls_ExposureInfo();
              
              exposureInfo.PremiumBasisCd = 'csio:6';
              exposureInfo.Exposure = '0';
              
              generalliabilityClassification.TerritoryCd = 'ZZ';
              
              generalliabilityClassification.ExposureInfo = exposureInfo;
              
              generalliabilityClassificationList.add(generalliabilityClassification);
          
              liabilityInfo.GeneralLiabilityClassification = generalliabilityClassificationList;
              
              /* US */
              
              generalliabilityClassification = 
                  new requestProcessor2Json.cls_GeneralLiabilityClassification();

              exposureInfo = new requestProcessor2Json.cls_ExposureInfo();
              
              exposureInfo.PremiumBasisCd = 'csio:6';
              exposureInfo.Exposure = '0';
              
              generalliabilityClassification.TerritoryCd = 'US';
              
              generalliabilityClassification.ExposureInfo = exposureInfo;
              
              generalliabilityClassificationList.add(generalliabilityClassification);
          
              liabilityInfo.GeneralLiabilityClassification = generalliabilityClassificationList;
              
              /* rsa:food */
              
              generalliabilityClassification = 
                  new requestProcessor2Json.cls_GeneralLiabilityClassification();

              exposureInfo = new requestProcessor2Json.cls_ExposureInfo();
              
              exposureInfo.PremiumBasisCd = 'rsa:food';
              exposureInfo.Exposure = '0';
              
              generalliabilityClassification.TerritoryCd = 'FF';
              
              generalliabilityClassification.ExposureInfo = exposureInfo;
              
              generalliabilityClassificationList.add(generalliabilityClassification);
          
              liabilityInfo.GeneralLiabilityClassification = generalliabilityClassificationList;
              
              /* csio:7 */
              
              generalliabilityClassification = 
                  new requestProcessor2Json.cls_GeneralLiabilityClassification();

              exposureInfo = new requestProcessor2Json.cls_ExposureInfo();
              
              exposureInfo.PremiumBasisCd = 'csio:7';
              exposureInfo.Exposure = '0';
              
              generalliabilityClassification.TerritoryCd = 'CS';
              
              generalliabilityClassification.ExposureInfo = exposureInfo;
              
              generalliabilityClassificationList.add(generalliabilityClassification);
          
              liabilityInfo.GeneralLiabilityClassification = generalliabilityClassificationList;
          
          generalliabilitylineBusiness.LOBCd = 'csio:2';
          generalliabilitylineBusiness.LiabilityInfo = liabilityInfo;
          
          elementMap.InsuranceSvcRq_CommlPkgPolicyQuoteInqRq_GeneralLiabilityLineBusiness = generalliabilitylineBusiness;
          
          return elementMap;
           
      }
      
      public class locationWrapper{
          public String BuiltYear             {get;set;}
          public String TotalArea             {get;set;}
          public String NumberofStories       {get;set;}
          public String ReplacementValue      {get;set;}
          public String BuildingType          {get;set;}
          public String WallType              {get;set;}
          public String RoofType              {get;set;}
          
          public String HERPUpgrades          {get;set;}
          public String HERPeUpgrade         {get;set;}
          public String HERPhUpgrade         {get;set;}
          public String HERPrUpgrade         {get;set;}
          public String HERPpUpgrade         {get;set;}
          
          public String FireSprinkler         {get;set;}
          
          public String FireHydrantDistance   {get;set;}
          public String FireHallDistance      {get;set;}
          
          public String TotalValueOfStock     {get;set;}
          public String TotalValueOfEquipment {get;set;}

          public String Province              {get;set;}
          public String PostalCode            {get;set;}
          public String City                  {get;set;}
          public String RegisteredBusinessAddress {get;set;}
          public String RegisteredBusinessName {get;set;} 
          public String locNum                 {get;set;}
      }
    
}