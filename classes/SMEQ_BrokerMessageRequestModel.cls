global with sharing class SMEQ_BrokerMessageRequestModel
{
    public String email {public get; public set;}
    public String phone {public get; public set;}
    public String extension {public get; public set;}
    public String brokerID {public get; public set;}
    public String caseId {public get; public set;}
    public String caseNumber {public get; public set;}
    public String comment {public get; public set;}
    public String sicCode {public get; public set;}
    public String bundle {public get; public set;}
}