global class SMEQ_FinalizeQuoteResponseModel extends SMEQ_RESTResponseModel
{
    public String finalizeDate {public get; public set;}
    public String ePolicyNumber {public get; public set;}
}