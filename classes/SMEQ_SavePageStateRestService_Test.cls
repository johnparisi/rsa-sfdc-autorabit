@isTest
public class SMEQ_SavePageStateRestService_Test
{
    @testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    /**
     * Method to test save page state with valid parameters
     * 1. Assert response is not null
     * 2. Assert success message is set on response
     * 3. Assert response code is ok.
     * 
     */
    @isTest
    static void testSearchCustomSettingByTypeWithValidParam()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            //create mock case and quote
            Case aCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(aCase);
            
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/savePageState';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_SavePageStateRequestModel savePageStateReq = buildMockSavePageStateRequest(aQuote.Id);
            
            SMEQ_SavePageStateResponseModel response =  SMEQ_SavePageStateRestService.savePageState(savePageStateReq);
            Quote__c updatedQuote = [SELECT Id, Broker_Application_Stage__c, Status__c FROM Quote__c WHERE ID=:aQuote.ID LIMIT 1];
            System.assert(response!=null);
            System.debug('response is *****' + response);
            System.debug('request was *****' + savePageStateReq);
            System.debug('updated quote ****' + updatedQuote);
            System.assert(response.responseMessage == 'SUCCESS');
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, response.responseCode);
        }
    }
        
    /**
     * Method to test save page state with invalid parameters
     * 1. Assert response is not null
     * 2. Assert success message is set to ERROR on response
     * 3. Assert response code is ERROR.
     * 
     */
    @isTest
    static void testSavePageStateRestServiceWithInValidParameters()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/savePageState';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_SavePageStateRequestModel savePageStateReq = new SMEQ_SavePageStateRequestModel();
            savePageStateReq.quoteID = null;
            savePageStateReq.lastVisitedPageNum = null;
            
            SMEQ_SavePageStateResponseModel response =  SMEQ_SavePageStateRestService.savePageState(savePageStateReq);
            System.debug('Response message being returned is *****' + response.responseMessage);
            System.assert(response.responseMessage == 'ERROR');
            System.assert(response!=null);
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, response.responseCode);
        }
    }

    /**
     * Method to test save page state with valid quote id but no page number parameters
     * 1. Assert response is not null
     * 2. Assert success message is set to ERROR on response
     * 3. Assert response code is ERROR.
     * 
     */
    @isTest
    static void testSavePageStateRestServiceWithValidQuoteIDParameters()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            //create mock case and quote
            Case aCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(aCase);
            
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/savePageState';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_SavePageStateRequestModel savePageStateReq = new SMEQ_SavePageStateRequestModel();
            savePageStateReq.quoteID = aQuote.Id;
            savePageStateReq.lastVisitedPageNum = null;
            
            SMEQ_SavePageStateResponseModel response =  SMEQ_SavePageStateRestService.savePageState(savePageStateReq);
            System.assert(response.responseMessage == 'ERROR');
            System.assert(response!=null);
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, response.responseCode);
        }
    }
    
    /**
     * Method to test save page state with invalid quote id but no page number parameters
     * 1. Assert response is not null
     * 2. Assert success message is set to ERROR on response
     * 3. Assert response code is ERROR.
     * 
     */
    @isTest
    static void testSavePageStateRestServiceWithValidPageNumber()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            //create mock case and quote
            Case aCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_TestDataGenerator.getOpenQuote(aCase);
            
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/savePageState';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_SavePageStateRequestModel savePageStateReq = new SMEQ_SavePageStateRequestModel();
            savePageStateReq.quoteID = null;
            savePageStateReq.lastVisitedPageNum = 5;
            
            SMEQ_SavePageStateResponseModel response =  SMEQ_SavePageStateRestService.savePageState(savePageStateReq);
            System.assert(response.responseMessage == 'ERROR');
            System.assert(response!=null);
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, response.responseCode);
        }
    }

    //use to populate mock PageStateSaveRequest
    public static SMEQ_SavePageStateRequestModel buildMockSavePageStateRequest(String quoteID)
    {
        SMEQ_SavePageStateRequestModel mockRequest = new  SMEQ_SavePageStateRequestModel();
        mockRequest.quoteID = quoteID;
        mockRequest.lastVisitedPageNum = 3;
        return mockRequest;
    }
}