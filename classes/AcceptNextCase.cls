global with sharing class AcceptNextCase
{
// JCP test Aug. 11
    webService static Id acceptCase(String userId, String profileName, String listId)
    {
        // userId not set in button        
        User user = [
            SELECT Id, bkrUser_CI_Transaction_Type_Skills__c, Primary_Region__c, Secondary_Regions__c
            FROM User
            WHERE Id = :UserInfo.getUserId()
        ];
        userId = user.Id;
        // ***
        // RSA SME UW
        // ***
        //final String SME_QUOTES_LISTVIEW1 = 'SME_Broker_UW';
        final String PRORENEWALS_QUOTES_LISTVIEW1 = 'SME_Renewals_Case';
        /*
        final List<String> SME_QUOTES_LISTVIEW = new List<String>{'SME_Broker_UW', 
                                                    'Quebec_IRC_New_Business_Case',
                                                    'Quebec_IRC_Portfolio_Case',
                                                    'Quebec_IRC_Processing_Case',
                                                    'Quebec_Portfolio_Case',
                                                    'Quebec_Processing_Case',
                                                    'Quebec_Triage_Case'};
       */
        final List<String> SME_QUOTES_LISTVIEW = new List<String>{'SME_Broker_UW'};
        final List<String> RENEWALS_QUOTES_LISTVIEW = new List<String>{'Quebec_IRC_Renouvellement_Case',
            'Central_IRC_Renewal_Case'};
        final List<String> RETENTION_QUOTES_LISTVIEW = new List<String>{'Quebec_Retention_Case','Quebec_IRC_Retention_Case',
            'Central_Retention_Case','Central_IRC_Retention_Case','Pacific_Retention_Case'};            
        final List<String> PRORENEWALS_QUOTES_LISTVIEW = new List<String>{PRORENEWALS_QUOTES_LISTVIEW1};                                           
                                                      
        final String SPRNT_RECORDTYPE = 'CI_Open_SPRNT';
        final String REFERRAL_TRANSACTION_TYPE = 'Referral';
        final String RENREVISIT_TRANSACTION_TYPE = 'Renewal Re-visit';
        final String CIREFERRAL_RECORD_TYPE = 'CI_Referral';
        final String CIRETENTION_RECORD_TYPE = 'CI_Retention';
        
         Map<ID, ListView> lvMap = new Map<ID, ListView>([
            SELECT id, DeveloperName
            FROM listview
            WHERE DeveloperName IN :SME_QUOTES_LISTVIEW
        ]);
         Map<ID, ListView> lvMapRenewals = new Map<ID, ListView>([
            SELECT id, DeveloperName
            FROM listview
            WHERE DeveloperName IN :RENEWALS_QUOTES_LISTVIEW
        ]);
         Map<ID, ListView> lvMapProRenewals = new Map<ID, ListView>([
            SELECT id, DeveloperName
            FROM listview
            WHERE DeveloperName IN :PRORENEWALS_QUOTES_LISTVIEW
        ]);
         Map<ID, ListView> lvMapRetentions = new Map<ID, ListView>([
            SELECT id, DeveloperName
            FROM listview
            WHERE DeveloperName IN :RETENTION_QUOTES_LISTVIEW
        ]);
        
        List<Case> highPriorityCase = new List<Case>();
        String lvName;
        if(lvMap.containsKey(listId) || lvMapRenewals.containskey(listId) || lvMapProRenewals.containsKey(listId) || lvMapRetentions.containsKey(listId) ) {
            Set<Id> caseIds = getCaseIDs(listId);
            if (lvMap.containsKey(listId))
            {
                lvName = lvMap.get(listId).DeveloperName;
                
                
                highPriorityCase = [
                    SELECT id
                    FROM Case
                    WHERE Id IN :caseIds
                    AND SME_Underwriter_Visible__c = TRUE
                    ORDER BY SME_Sort_Order__c DESC
                    LIMIT 1
                ];
            }
        
            else if (lvMapRenewals.containsKey(listId))     {
                lvName = lvMapRenewals.get(listId).DeveloperName;
                highPriorityCase = [
                    SELECT id
                    FROM Case
                    WHERE Id IN :caseIds
                    AND SME_Underwriter_Visible__c = TRUE
                    ORDER BY IRC_Renewal_Sort_Order__c Desc, Transaction_Effective_Date__c Asc
                    LIMIT 1
                ];
                if (highPriorityCase.isEmpty()) {
                    highPriorityCase = [
                        SELECT id
                        FROM Case
                        WHERE Id IN :caseIds
                        AND SME_Underwriter_Visible__c = TRUE
                        ORDER BY createdDate
                        LIMIT 1
                    ];
                }                   
                
            }
            else if (lvMapProRenewals.containsKey(listId)) {
                lvName = lvMapProRenewals.get(listId).DeveloperName;
                highPriorityCase = [
                    SELECT id
                    FROM Case
                    WHERE Id IN :caseIds
                    ORDER BY Pro_Renewal_Sort_Order__c Desc, Transaction_Effective_Date__c
                    LIMIT 1
                ];
                if (highPriorityCase.isEmpty()) {
                    highPriorityCase = [
                        SELECT id
                        FROM Case
                        WHERE Id IN :caseIds
                        ORDER BY createdDate
                        LIMIT 1
                    ];
                }                                   
            }
            else if (lvMapRetentions.containsKey(listId)) {
                lvName = lvMapRetentions.get(listId).DeveloperName;
                highPriorityCase = [
                    SELECT id
                    FROM Case
                    WHERE Id IN :caseIds
                    ORDER BY Retention_Sort_Order__c Desc
                    LIMIT 1
                ];
                if (highPriorityCase.isEmpty()) {
                    highPriorityCase = [
                        SELECT id
                        FROM Case
                        WHERE Id IN :caseIds
                        ORDER BY createdDate
                        LIMIT 1
                    ];
                }                                   
            }            
            if (!highPriorityCase.isEmpty())
            {
                Case c = [
                    SELECT Id, OwnerId, RecordType.DeveloperName, bkrCase_Subm_Type__c, Assigned_With_Next_Case_Button__c
                    FROM Case
                    WHERE Id = :highPriorityCase.get(0).Id
                    AND IsClosed = false
                    FOR UPDATE];
                
                c.OwnerId = userid;
                c.User_Assigned_With_Next_Case_Button__c = userId;
                Id caseRecordTypeId = Utils.GetRecordTypeIdsByDeveloperName(Case.SObjectType,true).get(SPRNT_RECORDTYPE);
                for (String lvInList : PRORENEWALS_QUOTES_LISTVIEW) {
                    if (lvName == lvInList) {
                        if (c.RecordType.DeveloperName == CIRETENTION_RECORD_TYPE && c.bkrCase_Subm_Type__c == RENREVISIT_TRANSACTION_TYPE) {
                            c.RecordTypeId = caseRecordTypeId;
                            break;
                        }
                    }  
                }
                update c;
                return c.id;
            }
            return null;
        }
        
        
        final string CONSTANT_PORTFOLIO_RECORD_TYPE = 'CI_Portfolio';
        final Map<String,String> mapProductCodeTeam = new Map<String,String>{'CI_SME_EPOLICY' => 'CI - Property & Casualty', 'CI_SME_IRC' => 'CI - IRC Auto'};
        List<ID> myGroups = new List<ID>();
        Set<Id> caseIds = new Set<Id>();
        String orderClause;
        Map<ID,List<AccountTeamMember>> mapCaseAccountTeamMembers = new Map<ID,List<AccountTeamMember>>();
        Map<ID,ID> mapAccountIdCaseId = new Map<ID,ID>();

        //if(profileName.contains('Regional Mid-Market & GSL')) {
            caseIds = getCaseIDs(listId);
        // ***
        // SFDC-14 - reuse MM/GSL logic to pull from specific List View for ALL users, now.
        // ***                        
        if(profileName.contains('Regional Mid-Market & GSL')) {
            if(caseIds.isEmpty()){
                return null;
            }
            for(String s : RSA_Custom_Settings__c.getOrgDefaults().MMGSL_Queue_IDs__c.split('\n')) {
                myGroups.add(s.trim());
            }
            orderClause = 'ORDER BY CreatedDate';
        }
        else {      
            for(GroupMember gm : [SELECT GroupId FROM GroupMember WHERE UserOrGroupId =: UserInfo.getUserId() AND Group.Type = 'Queue']){
                myGroups.add(gm.GroupId);
            }
            // ***
            // SFDC-14 - add secondary sort by CreatedDate ASC  
            // ***                      
            orderClause = 'ORDER BY bkrCase_Sort_Order__c DESC, CreatedDate ASC NULLS LAST';
            // end SFDC-14 //
        }
        // end SFDC-14 //
        String query = 'Select Id, bkrCase_Region__c, OwnerId, bkrCase_Subm_Type__c, BusinessHoursId, AccountId, Account.ParentId, Account.Parent.ParentId, RecordType.DeveloperName, Product.ProductCode FROM Case WHERE ';
        
        


        if(!caseIds.isEmpty()) {
            query += 'Id IN :caseIds AND ';
        }
        query += 'OwnerId in: myGroups ';
        // ***
        // 03.08.2016 -per Emma Watts, remove the logic to pull from "my cases", just queues.  
        // ***          
        // query += 'OR (OwnerId = \'' + UserInfo.getUserId() + '\' AND Status IN (\'New\',\'In Progress\'))) ';
        // 
        query += 'AND IsClosed = false ';

        //Add region based criteria, should only return Cases that match the Primary or
        //Secondary Region of the user. 
        if(!String.isBlank(user.Primary_Region__c)){
            query += 'AND ((bkrCase_Region__c =\'' + user.Primary_Region__c +'\') ';
        }
        if(!String.isBlank(user.Secondary_Regions__c)){
            List<String> secondaryRegions = user.Secondary_Regions__c.split(';');

            //No Primary
            if(String.isBlank(user.Primary_Region__c)){
                //start with AND
                query += 'AND (';
                for(Integer i = 0; i < secondaryRegions.size(); i++){
                    //first, and maybe final
                    if(i == 0){
                        query += '(bkrCase_Region__c =\''+ secondaryRegions[i]+'\') ';
                    }
                    //not the final one
                    else{
                        query += 'OR (bkrCase_Region__c =\''+ secondaryRegions[i]+'\') ';
                    }
                }
            }

            //Primary and Secondary
            else{
                for(Integer i = 0; i < secondaryRegions.size(); i++){
                    //first, and maybe final
                    if(i == 0){
                        query += ' OR (bkrCase_Region__c =\''+ secondaryRegions[i]+'\') ';
                    }
                    //not the final one
                    else{
                        query += 'OR (bkrCase_Region__c =\''+ secondaryRegions[i]+'\') ';
                    }
                }
            }

            query += ')';
        }
        if( (!String.isBlank(user.Primary_Region__c)) && (String.isBlank(user.Secondary_Regions__c)) ) {
            query += ')';
        }


        query += orderClause;
        system.debug(query);
        system.debug('myGroups '+myGroups);
        // ***
        // SFDC-14 - build map of broker teams  
        // ***      
        if(!profileName.contains('Regional Mid-Market & GSL')) { // don't perform secondary filters        
            for(Case c : Database.query(query)) {
                if(c.AccountId != null)
                    mapAccountIdCaseId.put(c.AccountId,c.Id);
                if(c.Account.ParentId != null)
                    mapAccountIdCaseId.put(c.Account.ParentId,c.Id);    
                if(c.Account.Parent.ParentId != null)
                    mapAccountIdCaseId.put(c.Account.Parent.ParentId,c.Id);                             
            }
            for(AccountTeamMember atm : [SELECT AccountId,TeamMemberRole,UserId 
                                            FROM AccountTeamMember 
                                            where AccountId in: mapAccountIdCaseId.keyset()
                                            AND TeamMemberRole in: mapProductCodeTeam.values()]) {

                if (mapCaseAccountTeamMembers.containsKey( mapAccountIdCaseId.get(atm.AccountId) ))
                {
                    mapCaseAccountTeamMembers.get(mapAccountIdCaseId.get(atm.AccountId)).add(atm);
                }else{
                    mapCaseAccountTeamMembers.put(mapAccountIdCaseId.get(atm.AccountId), new List<AccountTeamMember>{atm});
                }   
            }
        }

        List<Case> primaryCases = new List<Case>();
        List<Case> secondaryCases = new List<Case>();
        Boolean isPrimary;
        Boolean isSME;

        for(Case c : Database.query(query)) {
            isPrimary = false;
            isSME = false;
            // ***
            // SFDC-14 - check business hours, skills, broker team  
            // ***      
            if(c.OwnerId != userid && !profileName.contains('Regional Mid-Market & GSL')) { // don't perform secondary filters
                //Is it a primary or secondary?
                isSME = true;
                if(c.bkrCase_Region__c == user.Primary_Region__c){
                    isPrimary = true;
                }

                if(c.BusinessHoursId != null && !BusinessHours.isWithin(c.BusinessHoursId, Test.isRunningTest() ? BusinessHours.nextStartDate(c.BusinessHoursId, System.Now()) : System.now()))
                    continue; // skip cases outside business hours of that timezone
                system.debug('before');
                if(user.bkrUser_CI_Transaction_Type_Skills__c != null && c.bkrCase_Subm_Type__c != null){ // is a limited skill user
                    Boolean foundSkill = false;
                    system.debug('foundskill');
                    for(String s : user.bkrUser_CI_Transaction_Type_Skills__c.split(';')){
                        system.debug(s + ' ' + c.bkrCase_Subm_Type__c);
                        if(s == c.bkrCase_Subm_Type__c){
                            foundSkill = true;
                            continue;
                        }
                    }
                    if(!foundSkill) // skip cases not explicitly in a limited skill user
                        continue;
                }
                if(mapCaseAccountTeamMembers.get(c.Id) != null &&  c.RecordType.DeveloperName == CONSTANT_PORTFOLIO_RECORD_TYPE){ // Account Team check required on Portfolio record types
                    Boolean foundProduct = false;

                    for(AccountTeamMember atm : mapCaseAccountTeamMembers.get(c.Id)){
                        if(mapProductCodeTeam.get(c.Product.ProductCode) == atm.TeamMemberRole){
                            foundProduct = true;
                            continue;
                        }
                    }
                    if(!foundProduct) // skip cases when matching Product not listed on Team
                        continue;   
                                            
                    Boolean foundOnTeam = false;    
                                
                    for(AccountTeamMember atm : mapCaseAccountTeamMembers.get(c.Id)){
                        if(mapProductCodeTeam.get(c.Product.ProductCode) == atm.TeamMemberRole && atm.UserId == userid){
                            foundOnTeam = true;
                            continue;
                        }
                    }
                    if(!foundOnTeam) // skip cases when user not on Account Team
                        continue;                       
                }

                if(isPrimary){
                    primaryCases.add(c);
                }
                else{
                    secondaryCases.add(c);
                }
            }  // end SFDC-14 //

            //SME Cases are being tracked and will be sent outside of the loop
            if(!isSME){
                Id returnId = AcceptNextCase.takeOwnership(c, userid);
                if(returnId != null){
                    return returnId;
                }
            }
        }

        //check for primary or secondary and accept that case. 
        if(primaryCases.size() > 0){
            for(Case c : primaryCases){
                Id returnId = AcceptNextCase.takeOwnership(c, userid);
                if(returnId != null){
                    return returnId;
                }
            }
        }

        if(secondaryCases.size() > 0){
            for(Case c : secondaryCases){
                Id returnId = AcceptNextCase.takeOwnership(c, userid);
                if(returnId != null){
                    return returnId;
                }
            }
        }


        // ***
        // SFDC-14 - loop again without secondary-pass filters (no cases matched)
        // ***          
        for(Case c : Database.query(query)) {
            for(Case cUpdate : [Select Id, OwnerId, Assigned_With_Next_Case_Button__c
                                    FROM Case
                                    // ***
                                    // SFDC-14 - double-check criteria on lock of record
                                    // ***                  
                                    WHERE (Id =: c.Id AND IsClosed = false) //AND
                                    //(OwnerId in: myGroups
                                    // ***
                                    // 03.08.2016 -per Emma Watts, remove the logic to pull from "my cases", just queues.  
                                    // ***                                      
                                    //OR (OwnerId =: UserInfo.getUserId() AND Status in ('New','In Progress')) //)
                                    //
                                    // end SFDC-14 //                                                                       
                                    FOR UPDATE]){                                       
                /*if(cUpdate.Assigned_With_Next_Case_Button__c == true){ // cycle the Assigned with next case button - to show in field history
                    cUpdate.OwnerId = userid;
                    cUpdate.Assigned_With_Next_Case_Button__c = false;
                    update cUpdate;                 
                }*/
                cUpdate.OwnerId = userid;
                cUpdate.User_Assigned_With_Next_Case_Button__c = userId;
                update cUpdate;
                return cUpdate.Id;
            }           
        }     
        return null;
    }
   
   public static Id takeOwnership(Case c, String userid){
    for(Case cUpdate : [Select Id, OwnerId, Assigned_With_Next_Case_Button__c
                                    FROM Case
                                    // ***
                                    // SFDC-14 - double-check criteria on lock of record
                                    // ***                  
                                    WHERE Id =: c.Id AND OwnerId =: c.OwnerId
                                                                                                        
                                    FOR UPDATE]){    
                // added to check that ownership didn't change during lock
                if(cUpdate.OwnerId != c.OwnerId)
                    continue;                                        
                /*if(cUpdate.Assigned_With_Next_Case_Button__c == true){ // cycle the Assigned with next case button - to show in field history
                    cUpdate.OwnerId = userid;
                    cUpdate.Assigned_With_Next_Case_Button__c = false;
                    // added to check that ownership didn't change during lock
                    try{
                        update cUpdate;                 
                    }catch(exception e){
                        continue;
                    }
                    // end SFDC-14 //   
                }*/
                cUpdate.OwnerId = userid;
                cUpdate.User_Assigned_With_Next_Case_Button__c = userId;

                    try{
                        update cUpdate;                 
                    }catch(exception e){
                        continue;
                    }
                    // end SFDC-14 //   
                return cUpdate.Id;
            }
        return null;
   }
   
    private static Set<Id> getCaseIDs(String listId)
    {
        Set<Id> caseIds = new Set<Id>();
        
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type', 'application/json');
        String domainUrl = URL.getSalesforceBaseUrl().toExternalForm();
        String endpointUrl = domainUrl + '/services/data/v32.0/sobjects/Case/listviews/' + listid + '/describe';
        req.setEndpoint(endpointUrl);
        req.setMethod('GET');      
        Http h = new Http();
        HttpResponse res = h.send(req);        
        Map<String,Object> root  = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        System.debug(root.get('query'));
        
        for (Case c : Database.query((String)root.get('query')))
        {
            caseIds.add(c.Id);
        }
        
        return caseIds;
    }
}