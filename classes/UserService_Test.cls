@isTest
public class UserService_Test
{

    public static final String OFFERING_1 = 'SPRNT v1';

    public static final String OFFERING_2 = 'HUB';

    public static final String REGION_1 = 'Pacific';

    public static final String REGION_2 = 'Ontario';
 
    public static String UW_LEVEL1 = 'Level 1';

    public static String UW_LEVEL2 = 'Level 2';

    public static String UW_LEVEL3 = 'Level 3';
    
    public static String UW_TUA = 'Level 0';
    
    
    
	/**
     * Testing the methods which retrieve the regions and offerings of the running user.
     * Assertions:
     * 1. Offering is not null.
     * 2. Offering equals the offering defined.
     * 3. Region is not null.
     * 4. Region equals the regions defined
     */
    public testMethod static void testRetrieveOfferingAndOneRegion()
    {
        User u;
        
        System.runAs(getNonPortalUserWithRole())
        {
            setupTestData();
        	u = getPortalUser(OFFERING_1, REGION_1);
        }
        
        User us = [
            SELECT id, Offering_Project_for_Brokers__c, Primary_Region__c, Secondary_Regions__c
            FROM User
            WHERE id = :u.Id
        ];
        
        String offering = us.Offering_Project_for_Brokers__c;
        String region = us.Primary_Region__c + (us.Secondary_Regions__c != null ? ';' + us.Secondary_Regions__c : '');
        
        System.runAs(u)
        {
            System.Test.startTest();
            System.assertEquals(offering.split(';'), UserService.getOffering(u.Id));
            System.assertEquals(region.split(';'), UserService.getRegion(u.Id));
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing the methods which retrieve the regions and offerings of the running user.
     * Assertions:
     * 1. Offering is not null.
     * 2. Offering equals the offering defined.
     * 3. Region is not null.
     * 4. Region equals the regions defined
     */
    public testMethod static void testRetrieveOfferingAndTwoRegions()
    {
        User u;
        
        System.runAs(getNonPortalUserWithRole())
        {
            setupTestData();
        	u = getPortalUser(OFFERING_1, REGION_1 + ';' + REGION_2);
        }
        
        User us = [
            SELECT id, Offering_Project_for_Brokers__c, Primary_Region__c, Secondary_Regions__c
            FROM User
            WHERE id = :u.Id
        ];
        
        String offering = us.Offering_Project_for_Brokers__c;
        String region = us.Primary_Region__c + (us.Secondary_Regions__c != null ? ';' + us.Secondary_Regions__c : '');
        
        System.runAs(u)
        {
            System.Test.startTest();
            System.assertEquals(offering.split(';'), UserService.getOffering(u.Id));
            System.assertEquals(region.split(';'), UserService.getRegion(u.Id));
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing the method which retrieves the user.
     * Assertions:
     * 1. Exception message expected for null region.
     */
    public testMethod static void testRetrieveOfferingAndNullRegions()
    {
        User u;
        
        System.runAs(getNonPortalUserWithRole())
        {
            setupTestData();
        	u = getPortalUser(OFFERING_1, '');
        }
        
        System.runAs(u)
        {
            System.Test.startTest();
            try
            {
                UserService.getUser(u.Id);
            }
            catch (UserService.UserServiceException use)
            {
                System.assertEquals(UserService.REGION_UNAVAILABLE, use.getMessage());
            }
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing the method which retrieves the user.
     * Assertions:
     * 1. Exception message expected for null offering.
     */
    public testMethod static void testRetrieveNullOfferingAndOneRegions()
    {
        User u;
        
        System.runAs(getNonPortalUserWithRole())
        {
            setupTestData();
        	u = getPortalUser('', REGION_1);
        }
        
        System.runAs(u)
        {
            System.Test.startTest();
            try
            {
                UserService.getUser(u.Id);
            }
            catch (UserService.UserServiceException use)
            {
                System.assertEquals(UserService.OFFERING_UNAVAILABLE, use.getMessage());
            }
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing the method which retrieves the user.
     * Assertions:
     * 1. Exception message for null offering and region expected.
     */
    public testMethod static void testRetrieveNullOfferingAndNullRegions()
    {
        User u;
        
        System.runAs(getNonPortalUserWithRole())
        {
            setupTestData();
        	u = getPortalUser('', '');
        }
        
        System.runAs(u)
        {
            System.Test.startTest();
            try
            {
                UserService.getUser(u.Id);
            }
            catch (UserService.UserServiceException use)
            {
                System.assertEquals(UserService.OFFERING_REGION_UNAVAILABLE, use.getMessage());
            }
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing the method which retrieves the user.
     * Assertions:
     * 1. Exception message expected for invalid Id.
     */
    public testMethod static void testInvalidUserIdGetUser()
    {
        User u;
        
        System.runAs(getNonPortalUserWithRole())
        {
            setupTestData();
        	u = getPortalUser(OFFERING_1, '');
        }
        
        System.runAs(u)
        {
            System.Test.startTest();
            try
            {
                Account a = [
                    SELECT id
                    FROM Account
                    LIMIT 1
                ];
                
                UserService.getUser(a.Id);
            }
            catch (UserService.UserServiceException use)
            {
                System.assertEquals(UserService.INVALID_USER_ID, use.getMessage());
            }
            System.Test.stopTest();
        }
    }
	
    /**
     * This method sets up a contact for a test portal user.
     */
	public static void setupTestData()
    {
        Map<String,Id> AccountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);
        Map<String,Id> ContactTypes = Utils.GetRecordTypeIdsByDeveloperName(Contact.SObjectType, true);
        
        Account a = SMEQ_TestDataGenerator.getBrokerageAccount();
        a.isPartner = true;
        update a;
        
        Contact c = SMEQ_TestDataGenerator.getProducerContact(a);
    }
    
    /**
     * This method sets up the admin user which will be creating the test portal user.
     */
    public static User getNonPortalUserWithRole()
    {
        UserRole nonPortalRole = [
            Select Id
            FROM UserRole
            Where PortalType = 'None'
            Limit 1
        ];
        System.debug('UserRole is ' + nonPortalRole);
        
        Profile adminProfile = [
            SELECT Id
            FROM Profile
            WHERE name = 'System Administrator'
        ];
        User portalAccountOwner = new User(
            UserRoleId = nonPortalRole.Id,
            ProfileId = adminProfile.Id,
            Username = System.now().millisecond() + 'admin@rsatest.com',
            Alias = 'admin',
            Email='admin@rsatest.com',
            EmailEncodingKey = 'UTF-8',
            Firstname = 'Bruce',
            Lastname = 'Wayne',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            uwUser_Level__c = UserService_Test.UW_LEVEL3,
            Primary_Region__c = UserService_Test.REGION_1,
            Offering_Project__c = UserService_Test.OFFERING_1.substringBefore(' '),
            TimeZoneSidKey = 'America/Chicago'
        );
        insert portalAccountOwner;
        
        return portalAccountOwner;
    }
    
    /**
     * This method sets up the underwriter user.
     * 
     * @param uwLevel: The UW level set for the user.
     */
    public static User getUwUser(String uwLevel, String regions, String offerings)
    {
        String underwriterProfile = 'RSA Standard User [CI, Policy-Level, No Case Queue]';
        String quotePermissionSet = 'X01_Base_Enable_Quote_Case';
        
        UserRole nonPortalRole = [
            Select Id
            FROM UserRole
            Where PortalType = 'None'
            Limit 1
        ];
        System.debug('UserRole is ' + nonPortalRole);
        
        Profile uwProfile = [
            SELECT Id
            FROM Profile
            WHERE name = :underwriterProfile
        ];
        
        PermissionSet ps = [
            SELECT id
            FROM PermissionSet
            WHERE Name = :quotePermissionSet
        ];
        
        User uwUser = new User(
            UserRoleId = nonPortalRole.Id,
            ProfileId = uwProfile.Id,
            Username = Math.random() + 'uw@rsatest.com',
            Alias = 'uw',
            Email = System.now().millisecond() + 'uw@rsatest.com',
            EmailEncodingKey = 'UTF-8',
            Firstname = 'uw',
            Lastname = 'test',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Chicago',
            Offering_Project__c = offerings,
            Primary_Region__c = regions.substringBefore(';'),
            Secondary_Regions__c = regions.substringAfter(';'),
            uwUser_Level__c = uwLevel
        );
        insert uwUser;
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.PermissionSetId = ps.Id;
        psa.AssigneeId = uwUser.Id;
        insert psa;
        
        return uwUser;
    }
    
    /*
     * This method sets up the test portal user.
     */
    public static User getPortalUser(String offering, String regions)
    {
        Profile p = [
            SELECT Id
            FROM Profile
            WHERE Name = 'SME Broker'
            LIMIT 1
        ];
        
        UserRole ur = [
            SELECT id
            FROM UserRole
            WHERE Name LIKE '%Partner User'
            LIMIT 1
        ];
        
        Contact c = SMEQ_TestDataGenerator.getProducerContact();
        
        User u = Test_HelperMethods.CreateUser('T','U', (String) p.Id);
        u.ContactId = c.Id;
        u.userRole = ur;
        u.Offering_Project_for_Brokers__c = offering;
        u.Primary_Region__c = regions.substringBefore(';');
        u.Secondary_Regions__c = regions.substringAfter(';');
        insert u;
        
        c.OwnerId = u.Id;
        update c;
        
        return u;
    }

    /**
     * This method sets up the test portal user with different languages.
     */
    public static User getPortalUser(String offering, String regions, String language)
    {
        Profile p = [
            SELECT Id
            FROM Profile
            WHERE Name = 'SME Broker'
            LIMIT 1
        ];
        
        UserRole ur = [
            SELECT id
            FROM UserRole
            WHERE Name LIKE '%Partner User'
            LIMIT 1
        ];
        
        Contact c = SMEQ_TestDataGenerator.getProducerContact();
        
        User u = Test_HelperMethods.CreateUser('T','U', (String) p.Id, language);
        u.ContactId = c.Id;
        u.userRole = ur;
        u.Offering_Project_for_Brokers__c = offering;
        u.Primary_Region__c = regions.substringBefore(';');
        u.Secondary_Regions__c = regions.substringAfter(';');
        insert u;
        
        c.OwnerId = u.Id;
        update c;
        
        return u;
    }
}