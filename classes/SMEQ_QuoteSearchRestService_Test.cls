@isTest
private class SMEQ_QuoteSearchRestService_Test {
  /**
    * This method provides test assertions to perform a quote search base on incoming search terms
            * 1. Assert response is not null
         * 2. Assert response's payload is not null
         * 3. Assert response code is ok.
    */
    @isTest static void testSearchQuote(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/quoteSearch';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        SMEQ_QuoteSearchRequestModel request = new SMEQ_QuoteSearchRequestModel();
        request.searchText = 'test';
        request.brokerID = '123';
        SMEQ_QuoteSearchResponseModel response = SMEQ_QuoteSearchRestService.searchQuote(request );

        System.assert(response!=null);
        //System.assert(response.errors== null);
        System.assert(response.payload!= null);
        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, response.responseCode);
    }
   /**
    * This method provides test assertions to perform a quote search base on incoming search terms
    * 1. Assert response is not null from mock method
    */
    @isTest static void testMockResponse(){
    
        SMEQ_QuoteSearchResponseModel response = SMEQ_QuoteSearchRestService.getMockResponse();

        System.assert(response !=null);
    }   
  
     /**
    * This to test exception scenario
    * 1. Assert response contains error
    */
      @isTest static void testSearchQuoteException(){
       
        SMEQ_QuoteSearchResponseModel response;
        try{
            response = SMEQ_QuoteSearchRestService.searchQuote(null);
        }catch(Exception e){
        
            System.assert(response.getErrors().size()>0 );
   
        }
        

    }  

}