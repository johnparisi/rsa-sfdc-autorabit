@isTest
private class Test_bkrMMGSL_OpportunityAssistant_2 {
    
    static testmethod void OpportunityAssistantTest() {
        
        // Create a Sample Client Account
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        //brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        insert brokerageAcc;
        
        // Create a Broker to Work at the Brokerage Account
        Contact brokerCntc = new Contact();
        brokerCntc.FirstName= 'John';
        brokerCntc.LastName='Smith';
        brokerCntc.AccountId=brokerageAcc.Id;
        insert brokerCntc;
        
        // Create a Case from the Broker Contact
        Opportunity o = new Opportunity();
        o.Name ='x';
        o.AccountId = brokerageAcc.Id;
        o.bkrOpportunity_Producer__c = brokerCntc.Id;
        o.bkrClientAccount__c=clientAcc.id;
        o.StageName='Submission Received';
        o.Probability=1;
        o.CloseDate=Date.newInstance(2025, 07, 15);
        o.bkrProductLineEffectiveDate__c =Date.newInstance(2025, 07, 15);
        o.bkrProduct__c='01to0000001531cAAA';
        o.Type='New Business';
        insert o;
        
        Test.setCurrentPage(Page.bkrMMGSL_OpportunityAssistant);
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(o);
        bkrMMGSL_OpportunityAssistant vfp = new bkrMMGSL_OpportunityAssistant(sc);
        
        vfp.Name='test';
        vfp.Address ='';
        vfp.City ='';
        vfp.State ='ON';
        vfp.PostalCode='';
        vfp.CountryISOCode ='CA';
        vfp.searchDnB();
        
        DNBCallout.dnbEntry fnt= new  DNBCallout.dnbEntry();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293813';
        fnt.Company='test'; // Added by me becz the this variable is the use to give Account Name
        vfp.results.add(fnt);
        vfp.myDUNS='255293813';
        vfp.selectRecordAction();
        
        vfp.Name='test';
        vfp.Address ='';
        vfp.City ='';
        vfp.State ='ON';
        vfp.PostalCode='';
        vfp.CountryISOCode ='CA';
        vfp.searchDnB();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293814';
        vfp.results.add(fnt);
        vfp.myDUNS='255293814';
        vfp.selectRecordAction();
        
        // ------------------
        
        vfp.Name='';
        vfp.Address ='';
        vfp.City ='';
        vfp.State ='ON';
        vfp.PostalCode='';
        vfp.CountryISOCode ='CA';
        vfp.searchDnB();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        fnt.DUNSNumber='255293814';
        vfp.results.add(fnt);
        vfp.myDUNS='255293814';
        vfp.record=fnt;
        vfp.selectRecordAction();
        vfp.closePage=true;
        vfp.selectRecordAction();
        
    }
    
}