/**
 * XMLHelper
 * Author: James Lee
 * Date: October 6 2016
 * 
 * The purpose of this class is to assist in XML parsing.
 */
public class XMLHelper
{
    private static final String CDATA_OPENING_TAG = '<![CDATA[';
    private static final String CDATA_CLOSING_TAG = ']]>';
    
    public static String toCData(String value)
    {
        return CDATA_OPENING_TAG + value + CDATA_CLOSING_TAG;
    }
    
    /*
     * Encodes a string into the RqUID format: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
     * 
     * @param the string to encode.
     * @return a string encoded into the RqUID format.
     */
	public static String encodeUID(String value)
    {
        String base = EncodingUtil.convertToHex(Blob.valueOf(value));
        String pbase = '0'.repeat(32 - base.length()) + base;
        String encodedValue = pbase.subString(0, 8) + '-' + pbase.subString(8, 12) + '-' + pbase.substring(12, 16) + '-' + pbase.substring(16, 20) + '-' + pbase.substring(20, 32);
        return encodedValue;
    }
    
    /*
     * Decodes a string from the RqUID format: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
     * 
     * @param the string to decode.
     * @return a string decoded from the RqUID format.
     */
    public static String decodeUID(String value)
    {
        List<String> pieces = value.split('-');
        String whole = '';
        for (String piece : pieces)
        {
            whole += piece;
        }
        
        whole = whole.replaceFirst('^0+', ''); // Strip leading zeros
        String decodedValue = EncodingUtil.convertFromHex(whole).toString();
        return decodedValue;
    }
    
	/*
     * Searches for values inside the XML at the designated xpath.
     * Invokes helper method deepDive(XmlStreamReader, String, String) to retrieve these values.
     * 
     * @param xml The XML document to be parsed.
     * @param target The target path of the values to retrieve.
     * @return A list of values at the target path.
     */
    public static List<String> retrieveValues(String xml, String target)
    {
        XmlStreamReader reader = new XmlStreamReader(xml);
        reader.next();
        String tag = reader.getLocalName();
        if (target.contains(tag))
        {
            target = '/' + tag + target.subStringAfter(tag);
        	return deepDive(reader, '', target);
        }
        
        return new List<String>();
    }
    
    /*
     * Searches for values inside the XML at the designated xpath.
     * Invokes helper method deepDive(XmlStreamReader, String, String) to retrieve these values.
     * 
     * @param xml The XML document to be parsed.
     * @param target The target path of the values to retrieve.
     * @return A list of values at the target path.
     */
    public static String retrieveValue(String xml, String target)
    {
        List<String> values = retrieveValues(xml, target);
        
        if (values.size() == 0)
        {
            return null;
        }
        return values[0];
    }
    
    /*
     * A recursive method which calls itself whenever it finds the next tag which matches the target xpath.
     * When path matches target, the text content is added to the list of values to be returned.
     * 
     * @param reader The XMLStreamReader parsing through the XML document.
     * @param path The current path being traversed by the reader.
     * @param target The target path of values to retrieve.
     * @return A list of values at the target path.
     */
    private static List<String> deepDive(XmlStreamReader reader, String path, String target)
    {
        List<String> li = new List<String>();
        
        while (true)
        {
            if (path.split('/').size() > target.split('/').size())
            {
                break;
            }
            else if (reader.getEventType() == XMLTag.END_ELEMENT)
            {
                return li;
            }
            else if (reader.getEventType() == XMLTag.START_ELEMENT)
            {
                path += '/' + reader.getLocalName();
            }
            
            if (path == target) // Target found.
            {
                reader.next(); // Move to the contents of the tag.
                
                if (reader.getEventType() == XMLTag.CHARACTERS ||
                    reader.getEventType() == XMLTag.CDATA)
                {
                    li.add(reader.getText());
                }
            }
            else if (reader.getEventType() == XMLTag.START_ELEMENT)
            {
                if (target.contains(path))
                {
                    reader.next();
                    li.addAll(deepDive(reader, path, target));
                }
                else
                {
                    String tag = reader.getLocalName(); // The tag that doesn't match the target path.
                    path = path.substringBeforeLast('/'); // Remove the appended tag.
                    
                    while (reader.hasNext()) // Skip past this tag.
                    {
                        // Check for the closing element for the tag.
                        if (reader.getEventType() == XMLTag.END_ELEMENT && tag == reader.getLocalName())
                        {
                            break;
                        }
                        
                        reader.next();
                    }
                }
            }
            
            // Move to the next element.
            if (reader.hasNext())
            {
                reader.next();
            }
            else
            {
                break;
            }
        }
        
        return li;
    }
    
    /*
     * Searches for a the first instance of the tag and returns the contents of this tag.
     * 
     * @param xml the XML to extract from.
     * @param tag the tag to search for.
     * @return the XML block between the tag.
     */ 
    public static String extractBlock(String xml, String tag)
    {
        String openingTag = '<' + tag; // Closing > excluded to account for attributes.
        String closingTag = '</' + tag + '>';
        
        String body = xml.substringBetween(openingTag, closingTag);
        
        return openingTag + body + closingTag;
    }
    
    /*
     * Splits the xml by the closing tag and returns the list of elements.
     * 
     * @param xml the XML to extract the list from.
     * @param tag the tag to get the list of.
     * @return a list of XML strings for the specified tag.
     */
    public static List<String> extractBlocks(String xml, String tag)
    {
        String openingTag = '<' + tag; // Closing > excluded to account for attributes.
        String closingTag = '</' + tag + '>';
        
        List<String> blocks = new List<String>();
        for (String s : xml.split(closingTag))
        {
            if (!s.contains(openingTag))
            {
                break; // Skip the residue.
            }
            
            String block = extractBlock(s + closingTag, tag);
            blocks.add(block);
        }
        
        return blocks;
    }
    
    /*
     * Retrieves the attributes for a tag found in the xml.
     * 
     * @param xml the XML to extract the attributes from.
     * @param tag the tag to retrieve the attribute.
     * @param attributeName the name of the attribute to retrieve.
     * @return a list of attribute values for a tag.
     */
    public static List<String> getAttributes(String xml, String tag, String attributeName)
    {
        List<String> attributes = new List<String>();
        
        for (String tags : extractBlocks(xml, tag))
        {
            XmlStreamReader reader = new XmlStreamReader(tags);
            reader.next(); // Move cursor into the document start tag.
            attributes.add(reader.getAttributeValue(null, attributeName));
        }
        
        return attributes;
    }
    
    /*
     * Removes all tags in the xml with the tag as the element.
     * 
     * @param xml the XML to remove the elements for.
     * @param tag the tag to have removed from the XML.
     * @return the XML without the tag elements.
     */
    public static String removeTags(String xml, String tag)
    {
        String openingTag = '<' + tag; // Closing > excluded to account for attributes.
        String closingTag = '</' + tag + '>';
        
        for (String block : extractBlocks(xml, tag))
        {
            xml = xml.remove(block);
        }
        
        return xml;
    }
    
    /*
     * Strip the namespace out of the xml.
     * 
     * @param xml: the XML to remove the namespace for.
     * @param namespace: the namespace to search and remove from XML.
     * @return: the xml without the namespace.
     */
    public static String stripNamespace(String xml, String namespace)
    {
        String openingNamespace = '<' + namespace + ':';
        String closingNamespace = '</' + namespace + ':';
        
        return xml.replace(openingNamespace, '<').replace(closingNamespace, '</');
    }
}