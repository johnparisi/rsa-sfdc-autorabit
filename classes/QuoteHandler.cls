/**
 * QuoteHandler
 * Author: James Lee
 * Date: August 25 2016
 * 
 * The new Object Handler classes will follow this pattern.
 * 
 * It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
 * Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
 * 
 * You should never need to update this unless you are adding a business unit or record types. 
 */
public with sharing class QuoteHandler
{
    public static final String RECORDTYPE_OPEN_QUOTE = 'Open_Quote';
    public static final String RECORDTYPE_CLOSED_QUOTE = 'Closed_Quote';
    public static final String RECORDTYPE_POLICY = 'Policy';
    public static final String RECORDTYPE_POLICY_ClOSED = 'Closed_Policy';
    
    private List<Quote__c> smeQuotes = new List<Quote__c>();

    public void beforeInsert(List<Quote__c> records)
    {
        new QuoteDomain().beforeInsert(records);
        system.debug('Before Insert'); 

        initQuotes(records);

        if (smeQuotes.size() > 0)
        {
        //before Insert
            
            new QuoteDomain.SMEQDomain().beforeInsert(smeQuotes);
            system.debug('Before Insert');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        }
    }

    public void beforeUpdate(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        new QuoteDomain().beforeUpdate(records, oldRecords);
        system.debug('Before Update');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        initQuotes(records);
        if (smeQuotes.size() > 0)
        {
            new QuoteDomain.SMEQDomain().beforeUpdate(smeQuotes, oldRecords);
        }
    }

    public void afterInsert(List<Quote__c> records)
    {
        new QuoteDomain().afterInsert(records);
        system.debug('After Insert');
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
            System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
            System.debug('Limits.getCallouts - '+ Limits.getCallouts());
            System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
            System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
            System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
            System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
            System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
            System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
            System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
            System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
            System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
            System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
            System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
            System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
            System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
            System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
            System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
            System.debug('Limits.getQueries - '+ Limits.getQueries());
            System.debug('Limits.getQueries - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
            System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
            System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
            System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
            System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
            System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
            System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
            System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
            System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        initQuotes(records);

        if (smeQuotes.size() > 0)
        {
            new QuoteDomain.SMEQDomain().afterInsert(smeQuotes);
        }
    }
    
    public void afterUpdate(List<Quote__c> records, Map<Id, Quote__c> oldRecords)
    {
        new QuoteDomain().afterUpdate(records, oldRecords);

        initQuotes(records);
        
        if (smeQuotes.size() > 0)
        {
            new QuoteDomain.SMEQDomain().afterUpdate(smeQuotes, oldRecords);
            system.debug('After Update');
          
//System.debug('Limits.getLimitAggregateQueries - '+ Limits.getAggregateQuerie());
            
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getQueries - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
        }
    }

    private void initQuotes(List<Quote__c> records)
    {
        Map<String,Id> QuoteTypes = Utils.getRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        Set<Id> smeqRecordTypes = new Set<Id>();
        
        smeqRecordTypes.add(QuoteTypes.get(RECORDTYPE_OPEN_QUOTE));
        smeqRecordTypes.add(QuoteTypes.get(RECORDTYPE_CLOSED_QUOTE));
        
        for (Quote__c a : records)
        {
            if (smeqRecordTypes.contains(a.RecordTypeId))
            {
                smeQuotes.add(a);
            }
        }
    }
}