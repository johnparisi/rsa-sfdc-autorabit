/**
 * Author: John Zhang
 * Created At: Feb 7, 2017
 * 
 * Unit tests for BrokerStageSearchController
 */
@isTest
private class BrokerStageSearchController_Test
{
    static void postUserDataSetup()
    {
        SMEQ_TestDataGenerator.createTestDataForBrokerStage();
        SMEQ_TestDataGenerator.generateFullTestQuoteV2();
    }
    
    static Case getTestCase()
    {
        Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();
        Account a = SMEQ_TestDataGenerator.getBrokerageAccount();
        String brokerNumber = BrokerStageService.getStageBrokerNumber(a.bkrAccount_HuonBrokerNumber__c);
        brokerNumber = BrokerStageService.getStageBrokerNumber('1000550001');
        brokerNumber = BrokerStageService.getStageBrokerNumber('1000770001');
        brokerData.PARENT3_BROKER_NUMBER__c = '1000000002';
        update brokerData;
        brokerNumber = BrokerStageService.getStageBrokerNumber('1000000002');
        List<Broker_Stage__c> brokers = BrokerStageService.getStageBrokerageList('1000000002');
        Case testCase = SMEQ_TestDataGenerator.getOpenSprntCase();
        testCase.accountid = a.id;
        update testCase;
        return testCase;
    }
    
    static testMethod void test_retrieveBrokerageList()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            
            Case testCase = getTestCase();
            ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
            BrokerStageSearchController searchController = new BrokerStageSearchController(sc);
            searchController.retrieveBrokerageList();
            //system.assertEquals(2, searchController.brokerageList.size());
        }
    }
    
    static testMethod void test_removeBrokerageSelection()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            
            Case testCase = getTestCase();
            System.test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
            testCase.selected_brokerage_number__c = '123456';
            update testCase;
            BrokerStageSearchController searchController = new BrokerStageSearchController(sc);
            searchController.removeBrokerageSelection();
            testCase = [select selected_brokerage_number__c from case where id = :testCase.id];
            system.assertEquals(null, testCase.selected_brokerage_number__c);
            System.test.stopTest();
        }
    }

    static testMethod void test_updateCaseBrokerageNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            
            Case testCase = getTestCase();
            ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
            BrokerStageSearchController searchController = new BrokerStageSearchController(sc);
            searchController.selectedBrokerageNumber = '654321';
            searchController.updateCaseBrokerageNumber();
            testCase = [select selected_brokerage_number__c from case where id = :testCase.id];
            system.assertEquals('654321', testCase.selected_brokerage_number__c);
        }
    }
    
    static testMethod void test_redirectToCase()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            postUserDataSetup();
            
            Case testCase = getTestCase();
            ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
            BrokerStageSearchController searchController = new BrokerStageSearchController(sc);
            system.assertNotEquals(null, searchController.redirectToCase());
        }
    }
}