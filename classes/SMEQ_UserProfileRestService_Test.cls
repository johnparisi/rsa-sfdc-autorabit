@isTest
public with sharing class SMEQ_UserProfileRestService_Test {

	/**
     * HELPER METHOD ONLY : Setup test data
     */
    @testSetup
    static void contactAccountSetup() {
        User currentUser = [Select UserRoleId, Id from User where Id=:UserInfo.getUserId()];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        currentUser.UserRoleId = portalRole.Id;
        System.runAs(new User(Id = Userinfo.getUserId())){
            update currentUser;
        }
    }

   /**
    * Test fetching user profile: without an associated Contact
    * Run User Profile Rest Service as an invalid user - should detect error accordingly
    */
    @isTest static void testUserProfileInvalid() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/user/getUserProfile';
        req.httpMethod = 'GET';
        RestContext.response = res;
        SMEQ_UserProfileRestService.ResponseObjectWrapper responseWrapper = SMEQ_UserProfileRestService.getUserProfile();
        System.assert(responseWrapper != null);
        System.assert(responseWrapper.getResponseCode() != null);
        System.assertEquals('ERROR', responseWrapper.getResponseCode());
        List<SMEQ_RESTResponseModel.ResponseError> errors = responseWrapper.getErrors();
        System.assertEquals(SMEQ_RESTResponseModel.ERROR_CODE_699, errors[0].getErrorCode());
    }

    /**
     * Run User Profile Rest Service as a valid SME broker profile user - should pass
     * Exercise DAO so it correctly populates the UserProfile for return
     */
    @isTest
    static void testUserProfileValid()
    {
        Id tContactId = null;
        Id tBrokerProfileId = null;
        Id tUserId = null;
        Id tAccountId = null;
        List<Account> accounts = [Select Id from Account where name = :SMEQ_UserProfileDao_Test.TEST_ACCOUNT_NAME];
        tAccountId = accounts[0].Id;
        List<Profile> profiles = [select id from profile where name = 'SME Broker'];
        tBrokerProfileId = profiles[0].Id;
        List<Contact> contacts = [Select Id from Contact where LastName = :SMEQ_UserProfileDao_Test.TEST_LASTNAME];
        tContactId = contacts[0].Id;

        Contact theContact = contacts[0];
        User tUser = SMEQ_UserProfileDao_Test.setupUser(tBrokerProfileId, tContactId);
        System.runAs(tUser) {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/user/getUserProfile';
            req.httpMethod = 'GET';
            RestContext.response = res;
            SMEQ_UserProfileRestService.ResponseObjectWrapper responseWrapper = SMEQ_UserProfileRestService.getUserProfile();
            System.assert(responseWrapper.getResponseCode() != null);
            System.assertEquals('OK', responseWrapper.getResponseCode());
            SMEQ_UserProfileDataModel model = responseWrapper.getPayload();
            System.assert(model.contactId == tContactId);
            System.assert(model.ownBrokerage.accountName == SMEQ_UserProfileDao_Test.TEST_ACCOUNT_NAME);
            System.assert(model.ownBrokerage.accountId == tAccountId);
            System.assert(model.email == SMEQ_UserProfileDao_Test.TEST_EMAIL);
            System.assert(model.phone == SMEQ_UserProfileDao_Test.TEST_PHONE);
            System.assert(model.firstName == SMEQ_UserProfileDao_Test.TEST_FIRSTNAME);
            System.assert(model.lastName == SMEQ_UserProfileDao_Test.TEST_LASTNAME);
        }
    }

    @testSetup
    static void adminUserSetup() {
        Account testChildAccount = new Account(name = SMEQ_UserProfileDao_Test.TEST_ACCOUNT_NAME);
        insert testChildAccount;
        Contact testContact = new Contact(LastName = SMEQ_UserProfileDao_Test.TEST_LASTNAME,
                FirstName = SMEQ_UserProfileDao_Test.TEST_FIRSTNAME,
                Phone = SMEQ_UserProfileDao_Test.TEST_PHONE,
                email = SMEQ_UserProfileDao_Test.TEST_EMAIL,
                AccountId = testChildAccount.Id);
        insert testContact;
    }
}