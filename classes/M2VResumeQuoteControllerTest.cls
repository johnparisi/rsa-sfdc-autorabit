/**
* @author Gopinath Perumal CloudSynApps.Inc
* @date 08-Dec-2019
*
* @group Quote
* @group-content ../../ApexDocContent/Quote.htm
*
* @description Created as part of JIRA User Story # M2V-14 Save Create Quote at application step
*/
@isTest
public class M2VResumeQuoteControllerTest {
    private static testMethod void testgetResumeUrl()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
          insert r;
        
       	  User user1 = new User();
          user1.ProfileID = [Select Id From Profile Where Name='System Administrator'].id;
          user1.EmailEncodingKey = 'ISO-8859-1';
          user1.LanguageLocaleKey = 'en_US';
          user1.TimeZoneSidKey = 'America/New_York';
          user1.LocaleSidKey = 'en_US';
          user1.FirstName = 'first';
          user1.LastName = 'last';
          user1.Username = 'test123@domain123.com123';   
          user1.Alias = 't1';
          user1.Email = 'no@email.com';
          user1.IsActive = true;
          user1.UserRoleId = r.Id;
        
  		  System.RunAs(user1) {
          Account a = new Account(Name='Test Account Name');
          insert a;
        
          Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
          insert c;
        
          User user = new User();
          user.ProfileID = [Select Id From Profile Where Name='SME Broker'].id;
          user.EmailEncodingKey = 'ISO-8859-1';
          user.LanguageLocaleKey = 'en_US';
          user.TimeZoneSidKey = 'America/New_York';
          user.LocaleSidKey = 'en_US';
          user.FirstName = 'first';
          user.LastName = 'last';
          user.Username = 'test321@domain321.com321';   
          user.CommunityNickname = 'testUser123';
          user.Alias = 't1';
          user.Email = 'no@email.com';
          user.IsActive = true;
          user.ContactId = c.Id;
      	  Insert user;		
      
    		NetworkMember membership = [SELECT network.name, networkId, memberID 
                                              FROM NetworkMember 
                                              WHERE memberID = :user.Id LIMIT 1];
      
           vlocity_ins__OmniScriptInstance__c savedInstance = New vlocity_ins__OmniScriptInstance__c();
      	   savedInstance.vlocity_ins__ObjectId__c = c.Id;
      	   savedInstance.vlocity_ins__RelativeResumeLink__c = 'www.test.com';
      	   savedInstance.vlocity_ins__Status__c = 'In Progress';
      	   Insert savedInstance;
      
            Test.startTest();
                M2VResumeQuoteController resume = New M2VResumeQuoteController();
                PageReference resumeQuotePage = Page.M2VResumeQuote;
                Test.setCurrentPage(resumeQuotePage);
                ApexPages.currentPage().getParameters().put('id',savedInstance.Id);
                resume.currentNetworkId = membership.networkId;
                resume.getResumeUrl();
      		Test.stopTest();
  		}
    }

}