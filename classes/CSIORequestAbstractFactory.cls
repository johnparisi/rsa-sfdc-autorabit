/**
 * CSIORequestAbstractFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to define common variables needed in all the service 
 * specific factory classes used to generate service requests
 */

public abstract class CSIORequestAbstractFactory {
    protected String xml = '';
    protected SOQLDataSet soqlDataSet;
    protected final String SERVICE_PROVIDER_NAME = 'rsagroup.ca'; 
    protected String dateFormatShort = 'YYYY-MM-dd';
    protected String dateFormatLong = 'yyyy-MM-dd\'T\'hh:mm:ss\'Z\'';
    protected String caseCreateDateLong;
    protected String caseCreateDateShort;
    protected String todayDateShort = DateTime.now().format(dateFormatShort);
    protected String todayDateLong = DateTime.now().format(dateFormatLong);
    protected RequestInfo ri;
    protected Map<String, String> esbContext;
    protected String rsaOperation;
    protected String rsaContextId;
    protected String uniqueId;
    protected String comNumber;
    
    public CSIORequestAbstractFactory(){
        esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');    
    }
}