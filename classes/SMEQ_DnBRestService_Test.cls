/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SMEQ_DnBRestService_Test {
    static testMethod void myUnitTest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();   
        req.requestURI = '/services/apexrest/getDnBAddress';  
        req.addParameter('type', 'livechat');       

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        SMEQ_DnBResponseModel dnbResponse =  SMEQ_DnBRestService.getDnBAddress(buildSMEQ_DnBRequestModel());
        System.assert(dnbResponse!=null);     
    }
    
    public static SMEQ_DnBRequestModel buildSMEQ_DnBRequestModel() {
    	SMEQ_DnBRequestModel dnbReqModel = new SMEQ_DnBRequestModel();
    	dnbReqModel.name = 'TestDnBReq';
    	dnbReqModel.streetAddress = '22 Epic Dr';
    	dnbReqModel.city = 'Toronto';
    	dnbReqModel.state = 'Ontario';
    	dnbReqModel.postalCode = 'M4H1S4';
    	dnbReqModel.phoneNumber = '6547658789';
    	dnbReqModel.countryCode = '1';
    	return dnbReqModel;
    }
}