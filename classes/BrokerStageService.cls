public with sharing class BrokerStageService {

	//A Map of Broker Numbers from the Broker Stage Object keyed by the HUON Broker Number(from Account object) 
	private static Map<String, String> brokerNumbers;
    //A Map of Cases for Broker Stage lookup.
    // TSH - Updated to public - need to clear the cache for the unit test to work. Therefore needs to be public.
    public static Map<String, Case> brokerCases;

	// Stores the list of Broker Stages for a HUON #.
    private static Map<String, List<Broker_Stage__c>> storedBrokerStages;
    

	static{
		brokerNumbers = new Map<String, String>();
		storedBrokerStages = new Map<String, List<Broker_Stage__c>>();
	}

	/*
	@Author: Stephen Piercey
	@Date: Dec. 1 2016
	@Description: Use this to get the Broker Number from the new Broker Stage object
	*/
	public static String getStageBrokerNumber(Account a){
		return BrokerStageService.getStageBrokerNumber(a.bkrAccount_HuonBrokerNumber__c);
	}

	 /*
    @Author: Stephen Piercey
    @Date: Dec. 1 2016
    @Description: Use this to get the Broker Number from the new Broker Stage object
    */
    public static String getStageBrokerNumberByCase(String caseId)
    {
        if (brokerCases == null)
        {
            brokerCases = new Map<String, Case>();
        }
        
        Case myCase = brokerCases.get(caseId);
        
        if (myCase == null)
        {
            myCase = [
                SELECT account.bkrAccount_HuonBrokerNumber__c,selected_brokerage_number__c,contact.EP_Reference_Number__c
                FROM case
                WHERE id = :caseId
            ];
            brokerCases.put(caseId, myCase);
        }
        
        if (myCase.selected_brokerage_number__c != null)
        {
            return myCase.selected_brokerage_number__c;
        }
        else if (myCase.contact.EP_Reference_Number__c != null)
        {
            return myCase.contact.EP_Reference_Number__c;
        }
        else
        {
            return BrokerStageService.getStageBrokerNumber(myCase.account.bkrAccount_HuonBrokerNumber__c);
        }
    }

	/*
    @Author: Stephen Piercey
    @Date: Dec. 1 2016
    @Description: Use this to get the Broker Number from the new Broker Stage object
    */
    public static String getStageBrokerNumber(String brokerNumber){
        if(brokerNumber == null){
            throw new RSA_ESBException('The selected Account does not have a valid HUON Broker Number');
            return null;
        }
        else if (brokerNumbers.containsKey(brokerNumber)){
            return brokerNumbers.get(brokerNumber);
        }
        else{
            // BSD-2850: MVP Broker Data. @Author: Pratik Surti
            List<Broker_Stage__c> stagedBroker = [SELECT Id,SEQAGTNO__c, BROKER_NUMBER__c, PARENT1_BROKER_NUMBER__c, PARENT2_BROKER_NUMBER__c, PARENT3_BROKER_NUMBER__c,
                    WORKFLOW_EP__c, CLASSPROF__c, AGTSECID__c
            FROM Broker_Stage__c
            WHERE ( BROKER_NUMBER__c = : brokerNumber
            OR PARENT1_BROKER_NUMBER__c = : brokerNumber
            OR PARENT2_BROKER_NUMBER__c = : brokerNumber
            OR PARENT3_BROKER_NUMBER__c = : brokerNumber )
            AND ( WORKFLOW_EP__c = 'NB' )
            AND ( CLASSPROF__c = 'C001' OR CLASSPROF__c = 'C002' )
                    //LIMIT 1
            ];
            if (stagedBroker != null && !stagedBroker.isEmpty() ){
                //return staged broker with BROKER_NUMBER__c = brokerNumber first
                for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                    if(brokerNumber.equals(stagedBrokerSelected.BROKER_NUMBER__c)){
                        brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                        return String.valueOf(stagedBrokerSelected.SEQAGTNO__c);
                    }
                }
                //if no staged broker with BROKER_NUMBER__c match brokerNumber then look for PARENT1_BROKER_NUMBER__c
                for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                    if(brokerNumber.equals(stagedBrokerSelected.PARENT1_BROKER_NUMBER__c)){
                        brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                        return  String.valueOf(stagedBrokerSelected.SEQAGTNO__c);
                    }
                }
                //if no staged broker with PARENT1_BROKER_NUMBER__c match brokerNumber then look for PARENT2_BROKER_NUMBER__c
                for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                    if(brokerNumber.equals(stagedBrokerSelected.PARENT2_BROKER_NUMBER__c)){
                        brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                        return  String.valueOf(stagedBrokerSelected.SEQAGTNO__c);
                    }
                }
                //if no staged broker with PARENT2_BROKER_NUMBER__c match brokerNumber then look for PARENT3_BROKER_NUMBER__c
                for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                    if(brokerNumber.equals(stagedBrokerSelected.PARENT3_BROKER_NUMBER__c)){
                        brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                        return  String.valueOf(stagedBrokerSelected.SEQAGTNO__c);
                    }
                }
                return null;//place holder to trick the compiler
            }
            else{
                throw new RSA_ESBException('No valid match found for HUON Number under Broker Stage data');
                return null;
            }
        }
    }

    /*
    @Author: John Zhang
    @Date: Feb. 1 2017
    @Description: Returns a List containing all associated brokerages base on the account selected
    */
    public static List<Broker_Stage__c> getStageBrokerageList(String bkrAccount_HuonBrokerNumber)
    {
        List<Broker_Stage__c> brokerageList = storedBrokerStages.get(bkrAccount_HuonBrokerNumber);
        
        if (brokerageList != null)
        {
            return brokerageList;
        }
        else if (bkrAccount_HuonBrokerNumber != null)
        {
            brokerageList = [
                    SELECT NAME, BROKER_NUMBER__C, AGTSECID_DESC__C,SEQAGTNO__C,AGENT_STREET__C,AGENT_CITY__C, AGENT_STATE__C, AGENT_POSTAL_CODE__C,BRAND__C
                            //REGION_GROUP__C, STREET__C, STREETNO__C, AGTSECID_DESC__C, , STREETSU__C, STREETDIR__C, ,
                    FROM Broker_Stage__c
                    WHERE (BROKER_NUMBER__c = :bkrAccount_HuonBrokerNumber
                    OR PARENT1_BROKER_NUMBER__c = :bkrAccount_HuonBrokerNumber
                    OR PARENT2_BROKER_NUMBER__c = :bkrAccount_HuonBrokerNumber
                    OR PARENT3_BROKER_NUMBER__c = :bkrAccount_HuonBrokerNumber )
                    AND (WORKFLOW_EP__c = 'NB' )
                    AND (CLASSPROF__c = 'C001' OR CLASSPROF__c = 'C002' )
                    //LIMIT 1
            ];
            
            storedBrokerStages.put(bkrAccount_HuonBrokerNumber, brokerageList);
        }
        else
        {
            brokerageList = new List<Broker_Stage__c>();
        }
        
        return brokerageList;
    }


}