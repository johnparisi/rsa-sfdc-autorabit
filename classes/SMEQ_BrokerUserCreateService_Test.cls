/**
* @File Name    :   SMEQ_BrokerUserCreateService_Test
* @Description  :   Batch Broker User Creation Service test class
* 					
* @Date Created :   10/08/2017
* @Author       :   Habiba Zaman @ Deloitte [hzaman@deloitte.ca]
* @group        :   Test
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       10/08/2017  Habiba Zaman  	Created the file/class
* 1.1		08/09/2017 	Habiba Zaman	Changes contact creation method. 
*										Refactored code to used already declared variables.
*/
@isTest
private class SMEQ_BrokerUserCreateService_Test {
	
	public static final String BROKER_ACCOUNT_RECORD_TYPE = 'bkrAccount_RecordTypeBkrMGML';
    public static final String CONTACT_RECORD_TYPE = 'bkrContact_BrokerRecordType';
    
	/* Test Scenario: Testing if an account is partner enabled */
	@isTest static void Account_Partner_Enabled() {
		
		User portalAccountOwner1 = UserService_Test.getNonPortalUserWithRole();
		System.runAs(portalAccountOwner1){
			Map<String,Id> accountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);
            
			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '005634322',
				OwnerId = portalAccountOwner1.Id
			);
			Database.insert(portalAccount1);
			portalAccount1.IsPartner = true;
			Database.update(portalAccount1);

			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='testusername@testin.com',
				Federation_ID__c = 'XPUWEY1',
				bkrAccount_HuonBrokerNumber__c ='005634322',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);
			SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			test.startTest();
		
			smq.init(usList);

			test.stopTest();
			User_Staging__c usResult = new User_Staging__c();
			usResult = smq.userStagingRecords.get(usList.get(0).Id);
			System.assertEquals(usResult.Broker_Account__c , portalAccount1.Id);

		}
	}

	/* Test Scenario: Testing if an account is not partner enabled and updating ERR1 code */
	
	@isTest static void Account_Partner_Not_Enabled() {
		// Implement test code

		User portalAccountOwner1 = UserService_Test.getNonPortalUserWithRole();

		System.runAs(portalAccountOwner1){
			Map<String,Id> accountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);

			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '005634322'
			);
			Database.insert(portalAccount1);
            portalAccount1.IsPartner = false;
			Database.update(portalAccount1);

			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
                EP_Reference_Number__c = '12345678',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='testusername@testin.com',
				Federation_ID__c = 'XPUWEY1',
				bkrAccount_HuonBrokerNumber__c='005634322',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);
			SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			test.startTest();
		
			smq.init(usList);

			test.stopTest();
			User_Staging__c usResult = new User_Staging__c();
            List<User_Staging__c> result = [Select Error_Code__c from User_Staging__c 
                                            where bkrAccount_HuonBrokerNumber__c='005634322'];
            if(!result.isEmpty() && result.size() > 0)
            {
                usResult = result.get(0);
            }
            System.assertEquals(portalAccount1.IsPartner, false);
			System.assertEquals(usResult.Error_Code__c , 'ERR1');

		}
    	
	}

	/* Test Scenario: Testing if an account has contact */

	@isTest static void Contact_For_Account_Exists() {
		// Implement test code
		User portalAccountOwner1 = UserService_Test.getNonPortalUserWithRole();
        

		System.runAs(portalAccountOwner1){
           	SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = smq.accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '1000770001',
				OwnerId = portalAccountOwner1.Id
			);
			Database.insert(portalAccount1);
			portalAccount1.IsPartner = true;
			Database.update(portalAccount1);

			//Create Contact

			Contact theContact = new Contact(
    			FirstName = 'Test',
			    Lastname = 'Contact',
			    AccountId = portalAccount1.Id,
                RecordTypeId = smq.contactTypes.get(CONTACT_RECORD_TYPE),
			    Email = 'test@testingclass.com',
			    EP_Reference_Number__c = '1000000001'
			);

			Insert(theContact);

			//Create Broker Stage data
			Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();

			//Create User Stage object
			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='testusername@testin.com',
				Federation_ID__c = 'XPUWEY1',
				Broker_Account__c = portalAccount1.Id,
				bkrAccount_HuonBrokerNumber__c='1000770001',
               	EP_Reference_Number__c = '1000000001',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);

			test.startTest();
		
			smq.init(usList);

			test.stopTest();

			User_Staging__c usResult = new User_Staging__c();
			usResult = smq.userStagingRecords.get(usList.get(0).Id);

			System.assertEquals(usResult.Broker_Contact__c , theContact.Id);

		}
    	
	}

	/* Test Scenario: Testing if an account has no broker contact and generating error code */

	@isTest static void New_Contact_For_Account() {
		// Implement test code
		User portalAccountOwner1 = UserService_Test.getNonPortalUserWithRole();
		System.runAs(portalAccountOwner1){
            SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = smq.accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '1000770001',
				OwnerId = portalAccountOwner1.Id
			);
			Database.insert(portalAccount1);
			portalAccount1.IsPartner = true;
			Database.update(portalAccount1);

			

			//Create Broker Stage data
			Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();

			//Create User Stage object
			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='testusername@testin.com',
				Federation_ID__c = 'XPUWEY1',
				Broker_Account__c = portalAccount1.Id,
				bkrAccount_HuonBrokerNumber__c='1000770001',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);

			test.startTest();
		
			smq.init(usList);

			test.stopTest();
            Contact c = new Contact();
            List<Contact> contactList = [Select Id, Email from Contact 
                                        where AccountId = :portalAccount1.Id];
            if(!contactList.isEmpty() && contactList.size()>0){
                c = contactList.get(0);
            }

			System.assertEquals(c.Email , us.Email_Address__c);

		}
	}

	/* Test Scenario: Testing if an user is created */

	@isTest static void User_Created() {
		// Implement test code

		User portalAccountOwner1 = UserService_Test.getNonPortalUserWithRole();
		System.runAs(portalAccountOwner1){
            SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = smq.accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '1000770001',
				OwnerId = portalAccountOwner1.Id
			);
			Database.insert(portalAccount1);
			portalAccount1.IsPartner = true;
			Database.update(portalAccount1);

			//Create Contact
			Contact theContact = new Contact(
    			FirstName = 'Test',
			    Lastname = 'Contact',
			    AccountId = portalAccount1.Id,
                RecordTypeId = smq.contactTypes.get(CONTACT_RECORD_TYPE),
			    Email = 'test@testingclass.com',
			    EP_Reference_Number__c = '1000000001'
			);

			Insert(theContact);

			//Create Broker Stage data
			Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();

			//Create User Stage object
			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='testusername@testin.com',
				Federation_ID__c = 'XPUWEY1',
                EP_Reference_Number__c = '1000000001',
				Broker_Account__c = portalAccount1.Id,
				bkrAccount_HuonBrokerNumber__c='1000770001',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);

			test.startTest();
		
			smq.init(usList);

			test.stopTest();

			User_Staging__c usResult = new User_Staging__c();
			usResult = smq.userStagingRecords.get(usList.get(0).Id);

			List<User> createdUser = [SELECT Username, Contact.Id 
										FROM User 
										WHERE Contact.Id =: theContact.Id];
			User u = new User();
			if(createdUser.size() > 0){
				u = createdUser.get(0);
			}

			System.assertEquals(usResult.Broker_Username__c  , u.Username);

		}
	}

	/* Test Scenario: Testing if an user is not created based on duplicate email address */

	@isTest static void User_NOT_Created() {
		// Implement test code

				// Implement test code

		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = 'test2forUserNotCreate@test.com',
		   	Alias = 'batman',
			Email='bruce.wayne@wayneenterprises.com',
			EmailEncodingKey='UTF-8',
			Firstname='Bruce',
			Lastname='Wayne',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		Database.insert(portalAccountOwner1);

		System.runAs(portalAccountOwner1){
			Map<String,Id> accountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);

			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				RecordTypeId = accountTypes.get(BROKER_ACCOUNT_RECORD_TYPE),
				bkrAccount_HuonBrokerNumber__c = '1000770001',
				OwnerId = portalAccountOwner1.Id
			);
			Database.insert(portalAccount1);
			portalAccount1.IsPartner = true;
			Database.update(portalAccount1);

			//Create Contact
			Contact theContact = new Contact(
    			FirstName = 'Test',
			    Lastname = 'Contact',
			    AccountId = portalAccount1.Id,
			    Email = 'test@test.com',
			    EP_Reference_Number__c = '1000000001'
			);

			Insert(theContact);

			//Create Broker Stage data
			Broker_Stage__c brokerData = SMEQ_TestDataGenerator.createTestDataForBrokerStage();

			//Create User Stage object
			User_Staging__c us = new User_Staging__c(
				Email_Address__c = 'test@testingclass.com',
				First_Name__c = 'Test123',
				Last_Name__c = 'TestLastName',
				Broker_Username__c ='test2forUserNotCreate@test.com',
				Federation_ID__c = 'XPUWEY1',
				Broker_Account__c = portalAccount1.Id,
				bkrAccount_HuonBrokerNumber__c='1000770001',
                Language__c = 'en_US',
                Locale__c = 'en_CA',
                Timezone__c = 'America/New_York'
			);
			List<User_Staging__c> usList = new List<User_Staging__c>();
			usList.add(us);

			Database.insert(usList);
			SMEQ_BrokerUserCreateService smq = new SMEQ_BrokerUserCreateService();

			test.startTest();
		
			smq.init(usList);

			test.stopTest();

			User_Staging__c usResult = new User_Staging__c();
			usResult = smq.userStagingRecords.get(usList.get(0).Id);

			List<User> createdUser = [SELECT Username, Contact.Id 
										FROM User 
										WHERE Contact.Id =: theContact.Id];
			User u = new User();
			if(createdUser.size() > 0){
				u = createdUser.get(0);
			}
			//test to fail as there is already a user exists in system.
			String err = usResult.Error_Code__c;
			System.assertEquals(err.contains('ERR3')  , true );
		}
	}
	
}