/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
public class  FP_BindServiceTest {
    
    private static Account brokerAccount;
    private static Account clientAccount;
    private static Case testCase;
    private static Quote__c quote;
    private static Risk__c risk1;
    private static Risk__c risk2;
    private static Broker_Stage__c brokerStage;
    
    
    @isTest
    private static void parties() {
        // setup the data that we'd expect to be in place at finalization
        
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            setupBaseTestData();
            // add additional bind-time attributes into the graph
            
            // quote    
            quote.Terms_And_Conditions__c = 'anything';
            quote.Scheduled_Items__c = false;
            quote.Effective_Date__c = Date.today();
            quote.Policy_Payment_Plan_Type__c = 'Credit Card';
            quote.Downpayment_Amount__c = 0;
            quote.Monthly_Withdrawal_Preferred_Day__c = null;
            quote.Credit_Card_Payment_Ref__c = '12345678';
            quote.Credit_Card_Type__c = 'Visa';
            quote.Credit_Card_Expiration_Month__c = '10';
            quote.Credit_Card_Expiration_Year__c = '18';
            /* Remove
            quote.Monthly_Withdrawal_Payment_Amount__c = 0;
            quote.Billing_Address__c = '123 Nowhere Street';
            quote.Billing_City__c = 'Toronto';
            quote.Billing_Country__c = 'Canada';
            quote.Billing_Province__c = 'Ontario';
            quote.Billing_Postal_Code__c = 'M3S4S1';
            */
            quote.Package_Type_Selected__c = 'Standard';
            
            // additional location risk fields
            risk1.Mailing_City__c = 'Toronto';
            risk1.Mailing_Country__c = 'Canada';
            risk1.Mailing_Address_Postal_Code__c = 'M4S2P2';
            risk1.Mailing_Province__c = 'Ontario';
            risk1.Mailing_Address_Street__c = '123 Nowhere Street';
            risk1.Business_Description__c = 'Heres a business';
            risk1.Total_Occupied_Area__c = 2000;
            risk1.Number_of_Stories__c = 2;
            risk1.Distance_To_Fire_Hydrant__c = 'Over 300 Meters';
            risk1.Distance_To_Fire_Station__c = 'Within 5 KM';
            risk1.Year_Built__c = '2000';
            risk1.Construction_Type__c = 'Frame';
            risk1.Address_Line_1__c = '123 Nowhere Street';
            risk1.City__c = 'Null City';
            risk1.Province__c = 'Ontario';
            risk1.Country__c = 'Canada';
            
            risk2.Mailing_City__c = 'Toronto';
            risk2.Mailing_Country__c = 'Canada';
            risk2.Mailing_Address_Postal_Code__c = 'M4S2P2';
            risk2.Mailing_Province__c = 'Ontario';
            risk2.Mailing_Address_Street__c = '123 Nowhere Street';
            risk2.Business_Description__c = 'Heres a business';
            risk2.Total_Occupied_Area__c = 2000;
            risk2.Number_of_Stories__c = 2;
            risk2.Distance_To_Fire_Hydrant__c = 'Over 300 Meters';
            risk2.Distance_To_Fire_Station__c = 'Within 5 KM';
            risk2.Year_Built__c = '2000';
            risk2.Construction_Type__c = 'Frame';
            risk2.Address_Line_1__c = '123 Nowhere Street';
            risk2.City__c = 'Null City';
            risk2.Province__c = 'Ontario';
            risk2.Country__c = 'Canada';
            
            // --- loss payees
            
            // a loss payee contact
            Contact lossPayeeContact = new Contact();
            lossPayeeContact.FirstName = 'Loss';
            lossPayeeContact.LastName = 'Payeecontact';
            lossPayeeContact.Attention__c = null;
            lossPayeeContact.Contract_Number__c = null;
            lossPayeeContact.MailingStreet = '123 Nowhere Street';
            lossPayeeContact.MailingCity = 'Toronto';
            lossPayeeContact.MailingCountry = 'Canada';
            lossPayeeContact.MailingState = 'Ontario';
            lossPayeeContact.MailingPostalCode = 'M4S2P3';
            lossPayeeContact.Email = 'emaila@fakeemail.ca';
            lossPayeeContact.Phone = '5122122311';
            lossPayeeContact.AccountId = clientAccount.Id;
            
            insert lossPayeeContact;
            
            // a loss payee account
            Account lossPayeeAccount = new Account();
            lossPayeeAccount.Name = 'A loss payee account';
            lossPayeeAccount.bkrAccount_Regions__c = 'West';
            lossPayeeAccount.Phone = '9046853755';
            lossPayeeAccount.BillingStreet = '126 Nowhere Street';
            lossPayeeAccount.BillingState = 'ON';
            lossPayeeAccount.BillingPostalCode = 'M4S1P2';
            lossPayeeAccount.BillingCity = 'Toronto';
            lossPayeeAccount.BillingCountry = 'Canada';
            lossPayeeAccount.Email_Address__c = 'emailb@fakeemail.ca';
            
            insert lossPayeeAccount;
            
            // additional insured account
            
            Account additionalInsuredAccount = new Account();
            additionalInsuredAccount.Name = 'Policy level additional insured account';
            additionalInsuredAccount.bkrAccount_Regions__c = 'West';
            additionalInsuredAccount.Phone = '9046853755';
            additionalInsuredAccount.Email_Address__c = 'emailc@fakeemail.ca';
            
            insert additionalInsuredAccount;
            
            Contact additionalInsuredContact = new Contact();
            additionalInsuredContact.FirstName = 'Additional';
            additionalInsuredContact.LastName = 'Insuredcontact';
            additionalInsuredContact.Attention__c = null;
            additionalInsuredContact.Email = 'emaild@fakeemail.ca';
            additionalInsuredContact.Phone = '5122922311';
            additionalInsuredContact.AccountId = clientAccount.Id;
            insert additionalInsuredContact;
            
            // premier financier - account
            Account premierFinancierAccount = new Account();
            premierFinancierAccount.Name = 'Premier Financier account';
            premierFinancierAccount.bkrAccount_Regions__c = 'West';
            premierFinancierAccount.Phone = '9046853755';
            premierFinancierAccount.BillingStreet = '125 Nowhere Street';
            premierFinancierAccount.BillingState = 'ON';
            premierFinancierAccount.BillingPostalCode = 'M4S1P2';
            premierFinancierAccount.BillingCity = 'Toronto';
            premierFinancierAccount.BillingCountry = 'Canada';
            premierFinancierAccount.Email_Address__c = 'emaile@fakeemail.ca';
            insert premierFinancierAccount;
            
            // other client bill - contact
            Contact otherClientBillContact = new Contact();
            otherClientBillContact.FirstName = 'Other';
            otherClientBillContact.LastName = 'Clientbill';
            otherClientBillContact.Attention__c = null;
            otherClientBillContact.Contract_Number__c = null;
            otherClientBillContact.MailingStreet = '120 Nowhere Street';
            otherClientBillContact.MailingCity = 'Toronto';
            otherClientBillContact.MailingCountry = 'Canada';
            otherClientBillContact.MailingState = 'Ontario';
            otherClientBillContact.MailingPostalCode = 'M4S2P3';
            otherClientBillContact.Email = 'emailf@fakeemail.ca';
            otherClientBillContact.Phone = '5122922311';
            otherClientBillContact.AccountId = clientAccount.Id;
            insert otherClientBillContact;
            
            // additional liability insured - contact
            Contact liabilityAdditionalInsuredContact = new Contact();
            liabilityAdditionalInsuredContact.FirstName = 'Second Other';
            liabilityAdditionalInsuredContact.LastName = 'Client Bill';
            liabilityAdditionalInsuredContact.Attention__c = null;
            liabilityAdditionalInsuredContact.Contract_Number__c = null;
            liabilityAdditionalInsuredContact.MailingStreet = '120 Nowhere Street';
            liabilityAdditionalInsuredContact.MailingCity = 'Toronto';
            liabilityAdditionalInsuredContact.MailingCountry = 'Canada';
            liabilityAdditionalInsuredContact.MailingState = 'Ontario';
            liabilityAdditionalInsuredContact.MailingPostalCode = 'M4S2P3';
            liabilityAdditionalInsuredContact.Email = 'emailg@fakeemail.ca';
            liabilityAdditionalInsuredContact.Phone = '5122922311';
            liabilityAdditionalInsuredContact.AccountId = clientAccount.Id;
            insert liabilityAdditionalInsuredContact;
            
            
            // Quote risk relationships
            Quote_Risk_Relationship__c additionalInsuredAccountRelationship = new Quote_Risk_Relationship__c();
            additionalInsuredAccountRelationship.Account__c = additionalInsuredAccount.Id;
            additionalInsuredAccountRelationship.Party_Number__c = 1;
            additionalInsuredAccountRelationship.Party_Type__c = 'Additional Insured';
            additionalInsuredAccountRelationship.Relationship_Type__c = 'Private Load';
            additionalInsuredAccountRelationship.Quote__c = quote.Id;
            System.assert(additionalInsuredAccountRelationship.Account__c != null, 'Related account should not be null');
            
            Quote_Risk_Relationship__c additionalInsuredContactRelationship = new Quote_Risk_Relationship__c();
            additionalInsuredContactRelationship.Contact__c =  additionalInsuredContact.Id;
            additionalInsuredContactRelationship.Party_Number__c = 2;
            additionalInsuredContactRelationship.Party_Type__c = 'Additional Insured';
            additionalInsuredContactRelationship.Relationship_Type__c = 'Private Load';
            additionalInsuredContactRelationship.Quote__c = quote.Id;
            
            Quote_Risk_Relationship__c lossPayeeRiskContactRelationship = new Quote_Risk_Relationship__c();
            lossPayeeRiskContactRelationship.Contact__c = lossPayeeContact.Id;
            lossPayeeRiskContactRelationship.Party_Number__c = 1;
            lossPayeeRiskContactRelationship.Party_Type__c = 'Loss Payee';
            lossPayeeRiskContactRelationship.Related_Risk__c = risk2.Id;
            lossPayeeRiskContactRelationship.Relationship_Type__c = 'Lessor';
            lossPayeeRiskContactRelationship.Quote__c = quote.Id;
            
            Quote_Risk_Relationship__c lossPayeeRiskAccountRelationship = new Quote_Risk_Relationship__c();
            lossPayeeRiskAccountRelationship.Account__c = lossPayeeAccount.Id;
            lossPayeeRiskAccountRelationship.Party_Number__c = 2;
            lossPayeeRiskAccountRelationship.Party_Type__c = 'Loss Payee';
            lossPayeeRiskAccountRelationship.Related_Risk__c = risk2.Id;
            lossPayeeRiskAccountRelationship.Relationship_Type__c = 'Lessor';
            lossPayeeRiskAccountRelationship.Quote__c = quote.Id;
            
            Quote_Risk_Relationship__c premierFinancierRelationship = new Quote_Risk_Relationship__c();
            premierFinancierRelationship.Account__c = premierFinancierAccount.Id;
            premierFinancierRelationship.Party_Number__c = 1;
            premierFinancierRelationship.Party_Type__c = 'Premier Financier';
            premierFinancierRelationship.Quote__c = quote.Id;
            
            Quote_Risk_Relationship__c otherClientBillRelationship = new Quote_Risk_Relationship__c();
            otherClientBillRelationship.Contact__c = otherClientBillContact.Id;
            otherClientBillRelationship.Party_Number__c = 1;
            otherClientBillRelationship.Party_Type__c = 'Other Client Bill';
            otherClientBillRelationship.Quote__c = quote.Id;
            
            Quote_Risk_Relationship__c liabilityAdditionalInsuredRelationship = new Quote_Risk_Relationship__c();
            liabilityAdditionalInsuredRelationship.Contact__c = liabilityAdditionalInsuredContact.Id;
            liabilityAdditionalInsuredRelationship.Party_Number__c = 1;
            liabilityAdditionalInsuredRelationship.Party_Type__c = 'Liability Additional Insured';
            liabilityAdditionalInsuredRelationship.Related_Risk__c = risk1.Id;
            liabilityAdditionalInsuredRelationship.Quote__c = quote.Id;
            
            BindQuoteRiskContactAccountWrapper wrapper1 = new bindQuoteRiskContactAccountWrapper();
            wrapper1.quoteRiskRelationship = additionalInsuredAccountRelationship;
            wrapper1.account = additionalInsuredAccount;
            
            BindQuoteRiskContactAccountWrapper wrapper2 = new bindQuoteRiskContactAccountWrapper();
            wrapper2.quoteRiskRelationship = additionalInsuredContactRelationship;
            wrapper2.contact = additionalInsuredContact;
            
            BindQuoteRiskContactAccountWrapper wrapper3 = new bindQuoteRiskContactAccountWrapper();
            wrapper3.quoteRiskRelationship = lossPayeeRiskContactRelationship;
            wrapper3.contact = lossPayeeContact;
            
            BindQuoteRiskContactAccountWrapper wrapper4 = new bindQuoteRiskContactAccountWrapper();
            wrapper4.quoteRiskRelationship = lossPayeeRiskAccountRelationship;
            wrapper4.account = lossPayeeAccount;
            
            BindQuoteRiskContactAccountWrapper wrapper5 = new bindQuoteRiskContactAccountWrapper();
            wrapper5.quoteRiskRelationship = premierFinancierRelationship;
            wrapper5.account = premierFinancierAccount;
            
            BindQuoteRiskContactAccountWrapper wrapper6 = new bindQuoteRiskContactAccountWrapper();
            wrapper6.quoteRiskRelationship = otherClientBillRelationship;
            wrapper6.contact = otherClientBillContact;
            
            BindQuoteRiskContactAccountWrapper wrapper7 = new bindQuoteRiskContactAccountWrapper();
            wrapper7.quoteRiskRelationship = liabilityAdditionalInsuredRelationship;
            wrapper7.contact = liabilityAdditionalInsuredContact;
            
            List<BindQuoteRiskContactAccountWrapper> bindWrapperList = new List<BindQuoteRiskContactAccountWrapper>();
            bindWrapperList.add(wrapper1);
            bindWrapperList.add(wrapper2);
            bindWrapperList.add(wrapper3);
            bindWrapperList.add(wrapper4);
            bindWrapperList.add(wrapper5);
            bindWrapperList.add(wrapper6);
            bindWrapperList.add(wrapper7);
            
            /*    
            List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = new List<Quote_Risk_Relationship__c>();
            quoteRiskRelationshipList.add(additionalInsuredAccountRelationship);
            quoteRiskRelationshipList.add(lossPayeeRiskContactRelationship);
            quoteRiskRelationshipList.add(lossPayeeRiskAccountRelationship);
            quoteRiskRelationshipList.add(additionalInsuredContactRelationship);
            quoteRiskRelationshipList.add(premierFinancierRelationship);
            quoteRiskRelationshipList.add(otherClientBillRelationship);
            quoteRiskRelationshipList.add(liabilityAdditionalInsuredRelationship);
              
            List<Account> accountList = new List<Account>();
            accountList.add(additionalInsuredAccount);
            accountList.add(lossPayeeAccount);
            accountList.add(premierFinancierAccount);
            
            List<Contact> contactList = new List<Contact>();
            contactList.add(lossPayeeContact);
            contactList.add(additionalInsuredContact);
            contactList.add(otherClientBillContact);
            contactList.add(liabilityAdditionalInsuredContact);*/
              
            System.assert(quote.Id !=null, 'quote should not be null');
            
            List<Risk__c> riskList = new List<Risk__c>();
            riskList.add(risk1);
            riskList.add(risk2);
            
            Test.startTest();
            FP_AngularPageController.saveQuoteState(JSON.serialize(quote), riskList, bindWrapperList);
            System.assert(additionalInsuredAccountRelationship.Account__c != null, 'Account on the quoteRiskRelationship should not be null');
            System.assert(lossPayeeRiskContactRelationship.Contact__c != null, 'Contact on the quoteRiskRelationship should not be null');
            System.assert(lossPayeeContact.MailingStreet !=null ,'Mailing address on contact should not be null');
            FP_AngularPageController.bindQuote(JSON.serialize(quote));
            Test.stopTest();
        }
    }
    
    @isTest
    private static void creditCardPayment() {
       // setup the data that we'd expect to be in place at finalization
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            setupBaseTestData();
            quote.Terms_And_Conditions__c = 'anything';
            quote.Scheduled_Items__c = false;
            quote.Effective_Date__c = Date.today();
            quote.Policy_Payment_Plan_Type__c = 'Credit Card';
            quote.Credit_Card_Type__c = 'American Express';
            quote.Credit_Card_Expiration_Month__c = '10';
            quote.Credit_Card_Expiration_Year__c = '18';
            quote.Credit_Card_Name__c = 'CARDHOLDER NAME';
            quote.Payment_Frequency__c = '1 Installment';
            quote.Package_Type_Selected__c = 'Standard';
            
            List<Risk__c> riskList = new List<Risk__c>();
            riskList.add(risk1);
            riskList.add(risk2);
            
            // other client bill - contact
            Contact otherClientBillContact = new Contact();
            otherClientBillContact.FirstName = 'Other';
            otherClientBillContact.LastName = 'Clientbill';
            otherClientBillContact.Attention__c = null;
            otherClientBillContact.Contract_Number__c = null;
            otherClientBillContact.MailingStreet = '120 Nowhere Street';
            otherClientBillContact.MailingCity = 'Toronto';
            otherClientBillContact.MailingCountry = 'Canada';
            otherClientBillContact.MailingState = 'Ontario';
            otherClientBillContact.MailingPostalCode = 'M4S2P3';
            otherClientBillContact.Email = 'emailf@fakeemail.ca';
            otherClientBillContact.Phone = '5122922311';
            otherClientBillContact.AccountId = clientAccount.Id;
            //insert otherClientBillContact;
            
            Quote_Risk_Relationship__c otherClientBillRelationship = new Quote_Risk_Relationship__c();
            otherClientBillRelationship.Contact__r = otherClientBillContact;
            otherClientBillRelationship.Party_Number__c = 1;
            otherClientBillRelationship.Party_Type__c = 'Other Client Bill';
            otherClientBillRelationship.Related_Risk__c = risk2.Id;
            otherClientBillRelationship.Quote__c = quote.Id;
            
            BindQuoteRiskContactAccountWrapper wrapper = new bindQuoteRiskContactAccountWrapper();
            wrapper.quoteRiskRelationship = otherClientBillRelationship;
            wrapper.contact = otherClientBillContact;
            
            List<BindQuoteRiskContactAccountWrapper> bindWrapperList = new List<BindQuoteRiskContactAccountWrapper>();
            bindWrapperList.add(wrapper);
            
            Test.startTest();
            FP_AngularPageController.saveQuoteState(JSON.serialize(quote), riskList, bindWrapperList);
            FP_AngularPageController.bindQuote(JSON.serialize(quote));
            Test.stopTest();
        }
    }
    
    @isTest
    private static void withdrawalPlanPayment() {
       // setup the data that we'd expect to be in place at finalization
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            setupBaseTestData();
            
            quote.Terms_And_Conditions__c = 'anything';
            quote.Scheduled_Items__c = false;
            quote.Effective_Date__c = Date.today();
            quote.Policy_Payment_Plan_Type__c = 'Monthly Withdrawal Plan';
            quote.Downpayment_Amount__c = 100;
            quote.Monthly_Withdrawal_Preferred_Day__c = 1;
            quote.Bank_Transit_Id__c = '1234567';
            quote.Bank_Account_Number__c  = '1234567890';
            
            quote.Package_Type_Selected__c = 'Standard';
            
            List<Risk__c> riskList = new List<Risk__c>();
            riskList.add(risk1);
            riskList.add(risk2);
            
            Test.startTest();
            FP_AngularPageController.saveQuoteState(JSON.serialize(quote), riskList,new List<BindQuoteRiskContactAccountWrapper>() );
            FP_AngularPageController.bindQuote(JSON.serialize(quote));
            Test.stopTest();
        }
    }
   
    
    @isTest
    private static void directBillPayment() {
        // setup the data that we'd expect to be in place at finalization
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            setupBaseTestData();
            
            quote.Terms_And_Conditions__c = 'anything';
            quote.Scheduled_Items__c = false;
            quote.Effective_Date__c = Date.today();
            quote.Policy_Payment_Plan_Type__c = 'Direct Bill (One Time Payment)';
            quote.Downpayment_Amount__c = 100;
            
            quote.Package_Type_Selected__c = 'Standard';
            
            List<Risk__c> riskList = new List<Risk__c>();
            riskList.add(risk1);
            riskList.add(risk2);
            
            Test.startTest();
            FP_AngularPageController.saveQuoteState(JSON.serialize(quote), riskList, new List<BindQuoteRiskContactAccountWrapper>());
            FP_AngularPageController.bindQuote(JSON.serialize(quote));
            Test.stopTest();
        }
    }
    
    @isTest
    private static void agencyBill() {
        // setup the data that we'd expect to be in place at finalization
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            setupBaseTestData();
            
            quote.Terms_And_Conditions__c = 'anything';
            quote.Scheduled_Items__c = false;
            quote.Effective_Date__c = Date.today();
            quote.Policy_Payment_Plan_Type__c = 'Agency Bill';
            
            quote.Package_Type_Selected__c = 'Standard';
            
            List<Risk__c> riskList = new List<Risk__c>();
            riskList.add(risk1);
            riskList.add(risk2);
            
            Test.startTest();
            FP_AngularPageController.saveQuoteState(JSON.serialize(quote), riskList, new List<BindQuoteRiskContactAccountWrapper>());
            FP_AngularPageController.bindQuote(JSON.serialize(quote));
            Test.stopTest();
        }
    }

    // sets up a basic set of test data

    private static void setupBaseTestData()
    {   
        // generate the custom settings and base data
        SMEQ_TestDataGenerator.initializeExternalConnections();
        SMEQ_TestDataGenerator.initializeUWSicCodeSegments();
        SMEQ_TestDataGenerator.initializeSMEMappings();

        // start with a broker account to raise the case against
        brokerAccount = SMEQ_TestDataGenerator.getBrokerageAccount();
        System.assert(brokerAccount.Id !=null, 'Broker ID should not be null');
        
        // insured account
        clientAccount = SMEQ_TestDataGenerator.getInsuredAccount();
           
        /*
        SELECT Id, BROKER_NUMBER__c, PARENT1_BROKER_NUMBER__c, PARENT2_BROKER_NUMBER__c, PARENT3_BROKER_NUMBER__c, WORKFLOW_EP__c, CLASSPROF__c, AGTSECID__c FROM Broker_Stage__c 
        WHERE ( BROKER_NUMBER__c = : brokerNumber OR PARENT1_BROKER_NUMBER__c = : brokerNumber OR PARENT2_BROKER_NUMBER__c = : brokerNumber OR PARENT3_BROKER_NUMBER__c = : brokerNumber )
         AND ( WORKFLOW_EP__c = 'NB' ) AND ( CLASSPROF__c = 'C001' OR CLASSPROF__c = 'C002' )
        */
        
        brokerStage = new Broker_Stage__c();
        brokerStage.BROKER_NUMBER__c = brokerAccount.bkrAccount_HuonBrokerNumber__c;
        brokerStage.PARENT1_BROKER_NUMBER__c  = brokerAccount.bkrAccount_HuonBrokerNumber__c;
        brokerStage.PARENT2_BROKER_NUMBER__c  = brokerAccount.bkrAccount_HuonBrokerNumber__c;
        brokerStage.PARENT3_BROKER_NUMBER__c  = brokerAccount.bkrAccount_HuonBrokerNumber__c;
        brokerStage.WORKFLOW_EP__c = 'NB';
        brokerStage.CLASSPROF__c = 'C001';
        insert brokerStage;
        
        // case
        testCase = SMEQ_TestDataGenerator.getOpenSprntCase();
        Case checkCase = [
            SELECT id, accountId, segment__c
            FROM Case
            WHERE id = :testCase.Id
        ];

        System.assert(checkCase.Id != null, 'Test case ID should not be null');
        System.assert(checkCase.AccountId != null, 'The account ID on the case should not be null');
        System.assert(checkCase.AccountId == brokerAccount.Id, 'The account ID on the case is not the Broker Account');
        System.assert(checkCase.Segment__c != null, 'Segment on the case should not be null');
        
        // quote
        quote = new Quote__c();
        quote.Case__c = testCase.Id;
        quote.Status__c = 'Finalized';
        quote.Quote_Number__c = '12345678';
        quote.Quote_Date__c = Date.today();
        quote.Quote_Expiry_Date__c = quote.Quote_Date__c.addMonths(3);
        quote.Canadian_Revenue__c = 250000;
        //quote.Claim_Free_Years__c = 1;
        quote.Coverage_Selected__c = 'Commercial General Liability';
        quote.Currently_Insured__c = false;
        quote.Effective_Date__c = quote.Quote_Date__c.addMonths(1);
        quote.Foreign_Revenue__c = 0;
        quote.Package_Type_Selected__c = 'Standard';
        quote.Premium__c = 250000;
        quote.Reference_Number__c = '123456';
        quote.Total_Revenue__c = quote.Canadian_Revenue__c;
        quote.US_Revenue__c = 0;
        quote.Version__c = '1';
        quote.Business_Start_Date__c = Date.valueOf('2012-01-01 01:00:00');
        insert quote;
        
        // risk #1

        risk1 = new Risk__c();
        risk1.RecordTypeId = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Risk__c' and DeveloperName = 'Liability'].Id;
        risk1.Address_Line_1__c = '58 Taunton Road';
        risk1.Building_Value__c = 250000;
        risk1.City__c = 'Toronto';
        risk1.Construction_Type__c = 'Fire Resistive';
        risk1.Country__c = 'Canada';
        risk1.Coverage_Selected__c = 'Commercial General Liability';
        risk1.Distance_To_Fire_Hydrant__c = 'Within 150 Meters';
        risk1.Distance_To_Fire_Station__c = 'Within 5 KM';
        risk1.Equipment_Value__c = 2000;
        risk1.Number_of_Stories__c = 2;
        risk1.Postal_Code__c = 'M4S3P3';
        risk1.Province__c = 'Ontario';
        risk1.Sequence_Number__c = 1;
        risk1.Sprinkler_Coverage__c = true;
        risk1.Stock_Value__c = 5000;
        risk1.Total_Occupied_Area__c = 2000;
        risk1.Type_of_Roof__c = 'Brick or Stone';
        risk1.Type_of_Wall__c = 'Fire resistive';
        risk1.Year_Built__c = '2015';
        risk1.quote__c = quote.Id;
        insert risk1;
        
        // risk #2
        risk2 = new Risk__c();
        risk2.RecordTypeId = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Risk__c' and DeveloperName = 'Location'].Id;
        risk2.Address_Line_1__c = '58 Taunton Road';
        risk2.Building_Value__c = 250000;
        risk2.City__c = 'Toronto';
        risk2.Construction_Type__c = 'Fire Resistive';
        risk2.Country__c = 'Canada';
        risk2.Coverage_Selected__c = 'Location';
        risk2.Distance_To_Fire_Hydrant__c = 'Within 150 Meters';
        risk2.Distance_To_Fire_Station__c = 'Within 5 KM';
        risk2.Equipment_Value__c = 2000;
        risk2.Number_of_Stories__c = 2;
        risk2.Postal_Code__c = 'M4S3P3';
        risk2.Province__c = 'Ontario';
        risk2.Sequence_Number__c = 1;
        risk2.Sprinkler_Coverage__c = true;
        risk2.Stock_Value__c = 5000;
        risk2.Total_Occupied_Area__c = 2000;
        risk2.Type_of_Roof__c = 'Brick or Stone';
        risk2.Type_of_Wall__c = 'Fire resistive';
        risk2.Year_Built__c = '2015';
        risk2.quote__c = quote.Id;
        insert risk2;
    }
    
     @isTest
    private static void testRelationshipManagementConsole()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            setupBaseTestData();
            setupRelationshipManagementQuoteAndRisks();
            
            // create a new instance of the relationship management console associated with a quote
            ApexPages.StandardController quoteStandardController = new ApexPages.StandardController(quote);
            RelationshipControllerExtension quoteRelationshipController = new RelationshipControllerExtension(quoteStandardController);
            
            System.assert(quoteRelationshipController.partiesList.size() == 0, 'Controller should be initialized and have no parties');
            System.assert(quoteRelationshipController.currentQuote.Id == quoteStandardController.getRecord().Id, 'Controller should have a quote');
            
            // now do the same thing except for a risk
            ApexPages.StandardController riskStandardController = new ApexPages.StandardController(risk1);
            RelationshipControllerExtension riskRelationshipController = new RelationshipControllerExtension(riskStandardController);
            
            System.assert(riskRelationshipController.partiesList.size() == 0, 'Controller should be initialized and have no parties');
            System.assert(riskRelationshipController.currentRisk.Id == riskStandardController.getRecord().Id, 'Controller should have a risk');
        } 
    }
    
    private static void setupRelationshipManagementQuoteAndRisks()
    {
        List<Risk__c> riskList = new List<Risk__c>();
        riskList.add(risk1);
        riskList.add(risk2);
    }
    
    @isTest
    private static void testRelationshipConsoleSearch()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            Map<String, Id> accountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);
            
            setupBaseTestData();
            setupRelationshipManagementQuoteAndRisks();
            System.assertEquals(accountTypes.get('bkrAccount_RecordTypeBkrMGML'), brokerAccount.recordTypeId, 'Account should be broker type');
            System.assertEquals(accountTypes.get('Policyholder'), clientAccount.recordTypeId, 'Account should be insured type');
            
            // create a new instance of the relationship management console associated with a quote
            ApexPages.StandardController quoteStandardController = new ApexPages.StandardController(quote);
            RelationshipControllerExtension quoteRelationshipController = new RelationshipControllerExtension(quoteStandardController);
            System.assert(quoteRelationshipController.partiesList.size() == 0, 'Controller should be initialized and have no parties');
            
            //search
            quoteRelationshipController.searchText = 'My';
            quoteRelationshipController.searchForParties();
            System.assert(quoteRelationshipController.qrrList.size() == 0, 'qrrList should have 0 items');
            //System.assert(quoteRelationshipController.partiesList.size() == 2, 'First search results should have 2 items');
            
            //save with nothing selected
            quoteRelationshipController.saveQuoteRiskRelationships();
            System.assert(quoteRelationshipController.partiesList.size() == 0, 'Save should clear the first search results');
            
            //create a bindParty manually, add to partiesList, and save (because you can't do unit tests on SOSL search results)
            //List<BindParty> manualPartiesList = new List<BindParty>();
            BindParty bindParty = new BindParty();
            bindParty.account = brokerAccount.Id;
            bindParty.contactName = brokerAccount.Name;
            bindParty.isEditable = true;
            bindParty.toBeDeleted = false;
            bindParty.selected = true;
            bindParty.relationshipTypeOptions = new List<SelectOption>();
            bindParty.partyType = 'Premier Financier';
            quoteRelationshipController.partiesList.add(bindParty);
            
            quoteRelationshipController.saveQuoteRiskRelationships();
            System.assert(quoteRelationshipController.partiesList.size() == 1, 'Save should add the second search results as QRRs');
        }
    }
}