@RestResource(urlMapping='/underWriterTakeOver')
global with sharing class SMEQ_UnderwriterTakeOverService {
    /* This REST service updates the flag in Case and Quote objects to indicate this quote has been transferred to an Underwriter  */
	@HttpPost
    global static SMEQ_UnderWriterTakeOverResponseModel updateQuoteUnderWriterTakeOver (SMEQ_UnderWriterTakeOverModel underWriterTakeOver){
    	SMEQ_UnderWriterTakeOverResponseModel response = new SMEQ_UnderWriterTakeOverResponseModel();
        if (underWriterTakeOver!=null && underWriterTakeOver.quoteID!=null){


            try {

                             Case mycase =  [select a.Quote_application_handed_over_to_UW__c from case a
                              where a.id in (select case__c from quote__c where id = :underWriterTakeOver.quoteID)  LIMIT 1];
                             mycase.Quote_application_handed_over_to_UW__c = true;
                             upsert mycase;


                             response.status = 'SUCCESS'; 
            } catch(DmlException e) {
                System.debug('An unexpected error has occurred when setting UW Take Over flag: ' + e.getMessage());
                response.status = 'ERROR'; 
            }


        }

        return response;
    }


    global class SMEQ_UnderWriterTakeOverModel {
    	public Boolean underWriterTakeOver {public get; public set;}
  		public String quoteID {public get; public set;}
    }

    global class SMEQ_UnderWriterTakeOverResponseModel extends SMEQ_RESTResponseModel{
    	public String status {public get; public set;}
    }
	

}