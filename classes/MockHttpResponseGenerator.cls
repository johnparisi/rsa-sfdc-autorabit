/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
	global class MockHttpResponseGenerator implements HttpCalloutMock {
	    // Implement this interface method
	    global HTTPResponse respond(HTTPRequest req) {
	        // Optionally, only send a mock response for a specific endpoint
	        // and method.
	        
	        
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/xml');
	res.setHeader('Accept','application/xml');
	res.setBody('<feed xmlns:base="https://api.datamarket.azure.com/Data.ashx/DNB/BusinessVerificationOffice/v1/CompanyMatchByNameAddress" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns="http://www.w3.org/2005/Atom"><title type="text" /><subtitle type="text">DnB CompanyMatchByNameAddress</subtitle><id>https://api.datamarket.azure.com/Data.ashx/DNB/BusinessVerificationOffice/v1/CompanyMatchByNameAddress?Name=' + 'A Corp' + '&amp;StreetAddress=' + '46 Fieldway Rd' + '&amp;City='+'Etobicoke'+'&amp;State='+'ON'+'&amp;PostalCode='+'M8Z 3L2' +'&amp;CountryCode='+'CA'+'</id><rights type="text" /><updated>2015-07-01T12:55:49Z</updated><link rel="self" href="https://api.datamarket.azure.com/Data.ashx/DNB/BusinessVerificationOffice/v1/CompanyMatchByNameAddress?Name='+'A%20Corp'+'&amp;StreetAddress='+'46%20Fieldway%20Rd'+
				'&amp;City='+'Etobicoke'+'&amp;State='+'ON'+'&amp;PostalCode='+'M8Z%203L2'+'&amp;CountryCode='+'CA'+'" /><entry><id>https://api.datamarket.azure.com/Data.ashx/DNB/BusinessVerificationOffice/v1/CompanyMatchByNameAddress?Name='+'A Corp'+'&amp;StreetAddress='+'46 Fieldway Rd'+'&amp;City='+'Etobicoke'+'&amp;State='+'ON'+'&amp;PostalCode='+'M8Z 3L2'+'&amp;CountryCode='+'CA'+'&amp;$skip=0&amp;$top=1</id><title type="text">CleanseMatchByNameAddressEntity</title><updated>2015-07-01T12:55:49Z</updated><link rel="self" href="https://api.datamarket.azure.com/Data.ashx/DNB/BusinessVerificationOffice/v1/CompanyMatchByNameAddress?Name='+'A%20Corp'+'&amp;StreetAddress='+'46%20Fieldway%20Rd'+'&amp;City='+'Etobicoke'+'&amp;State='+'ON'+'&amp;PostalCode='+'M8Z%203L2'+'&amp;CountryCode='+'CA'+'&amp;$skip=0&amp;$top=1" /><content type="application/xml"><m:properties><d:DUNSNumber m:type="Edm.String">255139792</d:DUNSNumber><d:Company m:type="Edm.String">A Corp</d:Company><d:Address m:type="Edm.String">46 Fieldway Rd</d:Address><d:City m:type="Edm.String">Etobicoke</d:City><d:State m:type="Edm.String">ON</d:State><d:ZipCode m:type="Edm.String">M8Z 3L2</d:ZipCode><d:CountryISOCode m:type="Edm.String">CA</d:CountryISOCode><d:Phone m:type="Edm.String">4162336699</d:Phone><d:ConfidenceCode m:type="Edm.String">10</d:ConfidenceCode><d:MatchGrade m:type="Edm.String">AAAAAZZABFZ</d:MatchGrade><d:TransactionFlag m:type="Edm.String">Information</d:TransactionFlag><d:TransactionStatus m:type="Edm.String">Success</d:TransactionStatus><d:TransactionStatusCode m:type="Edm.String">CM000</d:TransactionStatusCode></m:properties></content></entry></feed>');
	
			
	        res.setStatusCode(200);
	        return res;
	    }
	}