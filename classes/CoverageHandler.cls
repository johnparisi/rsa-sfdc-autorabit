/**
 * CoverageHandler
 * Author: James Lee
 * Date: August 25 2016
 * 
 * The new Object Handler classes will follow this pattern.
 * 
 * It should handle all DML oportations and an init method that will segregate the data into Business Unit specific lists based on critera(usually record type).
 * Then pass the proper data off to the appropriate Business Unit Domain class for processing of actual business logic. 
 * 
 * You should never need to update this unless you are adding a business unit or record types. 
 */
public with sharing class CoverageHandler
{
    private List<Coverages__c> smeqCoverages = new List<Coverages__c>();
    private List<Coverages__c> smepCoverages = new List<Coverages__c>();
    
    public void beforeInsert(List<Coverages__c> records)
    {
        
        new CoverageDomain().beforeInsert(records);

        initCoverages(records);

        if (smeqCoverages.size() > 0)
        {
             //system.debug('BeforeInsert');
            new CoverageDomain.SMEQDomain().beforeInsert(smeqCoverages);
            //governor limit
          //after insert
System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
//governor limit   
        }
    }

    public void beforeUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
    {
        new CoverageDomain().beforeUpdate(records, oldRecords);
        //system.debug('beforeUpdate');

        initCoverages(records);

        if (smeqCoverages.size() > 0)
        {
            new CoverageDomain.SMEQDomain().beforeUpdate(smeqCoverages, oldRecords);
        }
    }

    public void afterInsert(List<Coverages__c> records)
    {
        new CoverageDomain().afterInsert(records);

        initCoverages(records);

        if (smeqCoverages.size() > 0)
        {
            //system.debug('AfterInsert');
            new CoverageDomain.SMEQDomain().afterInsert(smeqCoverages);
        }
    }
    
    public void afterUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
    {
        new CoverageDomain().afterUpdate(records, oldRecords);

        initCoverages(records);

        if (smeqCoverages.size() > 0) {
            new CoverageDomain.SMEQDomain().afterUpdate(smeqCoverages, oldRecords);
             //system.debug('afterUpdate');
        }       
        
        if (smepCoverages.size() > 0) {
            new CoverageDomain.SMEPDomain().afterUpdate(smepCoverages, oldRecords);
            //after update
            System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
//governor limit   
        }
    }

    private void initCoverages(List<Coverages__c> records)
    {   
        Map<String, Id> QuoteTypes = Utils.getRecordTypeIdsByDeveloperName(Quote__c.SObjectType,true);

        Map<Id, Map<Id,Id>> coverageRiskQuoteMap = new Map<Id, Map<Id,Id>>();
        Map<Id,Id> coverageRiskMap = new Map<Id,Id>();
        List <Id> parentRisks = new List<Id>();
        
        for (Coverages__c a : records) {
            parentRisks.add(a.Risk__c);
            coverageRiskMap.put(a.Id,a.Risk__c);
        }
        
        List <Risk__c> riskList = [SELECT id, Quote__r.recordTypeId 
                                    FROM Risk__c 
                                    WHERE id in :parentRisks];
                
        for (Coverages__c c : records) {
            for (Risk__c r: riskList) {
                if (coverageRiskMap.get(c.Id) == r.Id) {
                    Map<Id,Id> tempMap = new Map<Id,Id>();
                    tempMap.put(r.Id, r.Quote__r.recordTypeId);
                    coverageRiskQuoteMap.put(c.Id, tempMap);
                }
            }
        }

        for (Coverages__c c : records)
        {
            Id quoteRecordType = coverageRiskQuoteMap.get(c.Id).get(coverageRiskMap.get(c.Id));
            if (quoteRecordType == QuoteTypes.get(QuoteHandler.RECORDTYPE_POLICY)|| quoteRecordType == QuoteTypes.get(QuoteHandler.RECORDTYPE_POLICY_ClOSED)) {
                smepCoverages.add(c);
            } else {
                smeqCoverages.add(c);
            }
        }
    }
}