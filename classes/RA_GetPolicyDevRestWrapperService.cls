@RestResource(urlMapping='/policy/*')
global with sharing class RA_GetPolicyDevRestWrapperService {
    
    @HttpPost
    global static SearchResponse getPolicy(String comNumber) {
        system.debug('get policy' + comNumber);
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type','application/json');
        }
           
        SearchResponse wrapper = FP_AngularPageController.getPolicyService(comNumber, null);
     
        return wrapper;    
    }
    
}