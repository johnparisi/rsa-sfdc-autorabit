/**
 * Author: James Lee
 * Created At: November 1, 2016
 * 
 * Unit tests for CSIO_Addr.
 */
@isTest
public class CSIO_Addr_Test
{
	@testSetup
    static void dataSetup()
    {
        SMEQ_TestDataGenerator.initializeSMEMappings();
    }
    
    static testMethod void testConvertAccountAddressWithStreetNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            a = [
                SELECT BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Billing_Address_Line2__c
                FROM Account
                WHERE id = :a.Id
            ];
            
            CSIO_Addr ca = new CSIO_Addr(a);
            String output = '';
            output += ca;
            
            System.assertEquals(
                '<Addr>' +
                '<DetailAddr>' +
                ' <StreetName>' +  XMLHelper.toCData(a.BillingStreet.substringAfter(' ')) + '</StreetName>' +
                ' <StreetNumber>' + a.BillingStreet.substringBefore(' ') + '</StreetNumber>' +
                (a.Billing_Address_Line2__c != null && a.Billing_Address_Line2__c != '' ?
                 ' <UnitNumber>' + a.Billing_Address_Line2__c + '</UnitNumber>' : '') +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(a.BillingCity) + '</City>' +
                ' <StateProvCd>' + esbProvince.get(a.BillingState) + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(a.BillingPostalCode) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(a.BillingCountry) + '</CountryCd>' +
                '</Addr>',
                output
            );
        }
    }
    
    static testMethod void testConvertAccountAddressWithoutStreetNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            Account a = SMEQ_TestDataGenerator.getInsuredAccount();
            a = [
                SELECT BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Billing_Address_Line2__c
                FROM Account
                WHERE id = :a.Id
            ];
            a.BillingStreet = 'Rural Route';
            a.Billing_Address_Line2__c = 'TEST';
            
            CSIO_Addr ca = new CSIO_Addr(a);
            String output = '';
            output += ca;
            
            System.assertEquals(
                '<Addr>' +
                '<DetailAddr>' +
                ' <StreetName>' + XMLHelper.toCData(a.BillingStreet) + '</StreetName>' +
                (a.Billing_Address_Line2__c != null && a.Billing_Address_Line2__c != '' ?
                 ' <UnitNumber>' + a.Billing_Address_Line2__c + '</UnitNumber>' : '') +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(a.BillingCity) + '</City>' +
                ' <StateProvCd>' + esbProvince.get(a.BillingState) + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(a.BillingPostalCode) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(a.BillingCountry) + '</CountryCd>' +
                '</Addr>',
                output
            );
        }
    }
    
    static testMethod void testConvertRiskAddressWithStreetNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk();
            r = [
                SELECT Address_Line_1__c, Address_Line_2__c, City__c, Postal_Code__c, Province__c, Country__c
                FROM Risk__c
                WHERE id = :r.Id
            ];
            
            CSIO_Addr ca = new CSIO_Addr(r);
            String output = '';
            output += ca;
            
            System.assertEquals(
                '<Addr>' +
                '<DetailAddr>' +
                ' <StreetName>' + XMLHelper.toCData( r.Address_Line_1__c.substringAfter(' ')) + '</StreetName>' +
                ' <StreetNumber>' + r.Address_Line_1__c.substringBefore(' ') + '</StreetNumber>' +
                (r.Address_Line_2__c != null && r.Address_Line_2__c != '' ?
                 ' <UnitNumber>' + r.Address_Line_2__c + '</UnitNumber>' : '') +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(r.City__c) + '</City>' +
                ' <StateProvCd>' + esbProvince.get(r.Province__c) + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(r.Postal_Code__c) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(r.Country__c) + '</CountryCd>' +
                '</Addr>',
                output
            );
        }
    }
    
    static testMethod void testConvertRiskAddressWithoutStreetNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk();
            r = [
                SELECT Address_Line_1__c, Address_Line_2__c, City__c, Postal_Code__c, Province__c, Country__c
                FROM Risk__c
                WHERE id = :r.Id
            ];
            r.Address_Line_1__c = 'Rural Route';
            r.Address_Line_2__c = 'TEST';
            update r;
            
            CSIO_Addr ca = new CSIO_Addr(r);
            String output = '';
            output += ca;
            
            System.assertEquals(
                '<Addr>' +
                '<DetailAddr>' +
                ' <StreetName>' + XMLHelper.toCData(r.Address_Line_1__c) + '</StreetName>' +
                (r.Address_Line_2__c != null && r.Address_Line_2__c != '' ?
                 ' <UnitNumber>' + r.Address_Line_2__c + '</UnitNumber>' : '') +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(r.City__c) + '</City>' +
                ' <StateProvCd>' + esbProvince.get(r.Province__c) + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(r.Postal_Code__c) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(r.Country__c) + '</CountryCd>' +
                '</Addr>',
                output
            );
        }
    }
    
    static testMethod void testConvertRiskAddressWithOnlyStreetNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk();
            r = [
                SELECT Address_Line_1__c, Address_Line_2__c, City__c, Postal_Code__c, Province__c, Country__c
                FROM Risk__c
                WHERE id = :r.Id
            ];
            r.Address_Line_1__c = '12';
            update r;
            
            CSIO_Addr ca = new CSIO_Addr(r);
            String output = '';
            output += ca;
            
            System.assertEquals(
                '<Addr>' +
                '<DetailAddr>' +
                ' <StreetName>' + XMLHelper.toCData( r.Address_Line_1__c) + '</StreetName>' +
                (r.Address_Line_2__c != null && r.Address_Line_2__c != '' ?
                 ' <UnitNumber>' + r.Address_Line_2__c + '</UnitNumber>' : '') +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(r.City__c) + '</City>' +
                ' <StateProvCd>' + esbProvince.get(r.Province__c) + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(r.Postal_Code__c) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(r.Country__c) + '</CountryCd>' +
                '</Addr>',
                output
            );
        }
    }

    static testMethod void testConvertMailingAddressWithoutStreetNumber()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            //setupTestValues();
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk();
            r = [
                SELECT Address_Line_1__c, Address_Line_2__c, City__c, Postal_Code__c, Province__c, Country__c,
                Mailing_Address_Street__c, Mailing_City__c, Mailing_Address_Postal_Code__c, Mailing_Province__c, Mailing_Country__c
                FROM Risk__c
                WHERE id = :r.Id
            ];
            r.Address_Line_1__c = 'Rural Route';
            r.Mailing_Address_Street__c = 'Pacific Route';
            r.Mailing_City__c = 'Toronto';
            r.Mailing_Province__c = 'Ontario';
            r.Mailing_Address_Postal_Code__c = 'M2J 2Y2';
            r.Mailing_Country__c = 'Canada';
            update r;
            
            CSIO_Addr ca = new CSIO_Addr(r, true);
            String output = '';
            output += ca;
            
            System.assertEquals(
                '<Addr>' +
                '<AddrTypeCd>csio:1</AddrTypeCd>' +
                '<DetailAddr>' +
                ' <StreetName>' + XMLHelper.toCData(r.Mailing_Address_Street__c) + '</StreetName>' +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(r.Mailing_City__c) + '</City>' +
                //' <StateProvCd>' + esbProvince.get(r.Mailing_Province__c) + '</StateProvCd>' +
                ' <StateProvCd>' + r.Mailing_Province__c + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(r.Mailing_Address_Postal_Code__c) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(r.Mailing_Country__c) + '</CountryCd>' +
                '</Addr>',
                output
            );
        }
    }
    
    static testMethod void testNewMailingAddress()
    {
        
        User uwLevel1 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel1)
        {
            Risk__c r = SMEQ_TestDataGenerator.getLocationRiskForPolicy();

            List<Quote__C> quoteList = [ SELECT id FROM Quote__C];
            system.debug(quoteList.size());
            /*
            Quote__c q = quoteList[0];
            q.Status__c = 'Finalized';
            q.Reasons__c = 'AMOUNT OF INSURANCE INCREASED';
            q.Address_Line_1__c = '123 ABC Street';
            q.City__c = 'Toronto';
            q.State_Province__c = 'Ontario';
            q.Postal_Code__c = 'M2J 2Y2';
            q.Country__c = 'Canada';
            update q;
            */
            r = [
                SELECT Address_Line_1__c, Address_Line_2__c, City__c, Postal_Code__c, Country__c,
                id, RecordTypeId, Quote__c, Registered_Business_Name__c, Province__c,
                Distance_To_Fire_Hydrant__c, Distance_To_Fire_Station__c, 
                Electrical_Renovated_Year__c, Heating_Renovated_Year__c, Plumbing_Renovated_Year__c, Roof_Renovated_Year__c,
                Number_of_Stories__c, Sprinkler_Coverage__c, Construction_Type__c,
                Total_Occupied_Area__c, Year_Built__c,
                Building_Value__c, Equipment_Value__c, Stock_Value__c,
                Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c, 
                Mailing_City__c , Mailing_Country__c, Mailing_Address_Postal_Code__c, 
                Mailing_Province__c , Mailing_Address_Street__c , Business_Description__c, Deleted__c,
                Quote__r.Address_Line_1__c, Quote__r.Address_Line_2__c, Quote__r.City__c, related_com__c,
                Quote__r.Country__c, Quote__r.State_Province__c, Quote__r.Postal_Code__c, Quote__r.ePolicy__c,
                Quote__r.Status__c
                FROM Risk__c
                WHERE id = :r.Id
        	];
            
            
            
            Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
            Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
            
            CSIO_Addr ca = new CSIO_Addr(r, true);
            String output = '';
            output += ca;
            /*
            System.assertEquals(
                '<Addr>' +
                '<AddrTypeCd>csio:1</AddrTypeCd>' +
                '<DetailAddr>' +
                ' <StreetName>' + XMLHelper.toCData('ABC Street') + '</StreetName>' +
                ' <StreetNumber>' + 123 + '</StreetNumber>' +
                '</DetailAddr>' +
                ' <City>' + XMLHelper.toCData(r.Quote__r.City__c) + '</City>' +
                ' <StateProvCd>' + esbProvince.get(r.Quote__r.State_Province__c) + '</StateProvCd>' +
                ' <PostalCode>' + CSIO_Addr.formatPostalCode(r.Quote__r.Postal_Code__c) + '</PostalCode>' +
                ' <CountryCd>' + esbCountry.get(r.Quote__r.Country__c) + '</CountryCd>' +
                '</Addr>',
                output
            );
            */

      	}
    }
    
    /**
     * Test classes for formatting the postal code.
     * 
     * Assertions:
     * 1. Postal code is uppercased and a space is added.
     * 2. Postal code with space is uppercased.
     * 3. Postal code already uppercased has a space added.
     * 4. Postal code with leading capital letter is formatted.
     * 5. Zip codes are not affected by formatting.
     */
    static testMethod void testformatPostalCode()
    {
        String expectedPostalCode = 'A1A 1A1';
        String expectedZipCode = '12345';
        
        String postalCode = 'a1a1a1';
        System.assertEquals(expectedPostalCode, CSIO_Addr.formatPostalCode(postalCode));
        
        postalCode = 'a1a 1a1';
        System.assertEquals(expectedPostalCode, CSIO_Addr.formatPostalCode(postalCode));
        
        postalCode = 'A1A1A1';
        System.assertEquals(expectedPostalCode, CSIO_Addr.formatPostalCode(postalCode));
        
        postalCode = 'A1a1a1';
        System.assertEquals(expectedPostalCode, CSIO_Addr.formatPostalCode(postalCode));
        
        System.assertEquals(expectedZipCode, CSIO_Addr.formatPostalCode(expectedZipCode));
    }
}