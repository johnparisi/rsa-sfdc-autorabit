@RestResource(urlMapping='/getParty/*')
global with sharing class RA_GetPartyDevRestWrapperService {
    
    @HttpPost
    global static PartySearchResponse getParty(String userInput, Boolean contact) {
        system.debug('user input' + userInput);
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type','application/json');
        }
        
        PartySearchResponse partySearchResponse = FP_AngularPageController.getPartyList(userInput, contact);
        
        return partySearchResponse;
    }
    
}