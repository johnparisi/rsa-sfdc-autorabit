@isTest
public class SMEQ_CustomSettingSearchRestService_Test
{ 
     /**
     * Method to test custom setting search rest service with variant and type parameters
     * 1. Assert response is not null
     * 2. Assert customsetting size matches number of records in database
     * 3. Assert response code is ok.
     * 
     */
    @isTest static void testSearchCustomSettingByTypeWithTypeAndVariant() {

		User uw = UserService_Test.getUwUser('', UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        
        System.runAs(uw)
        {
	        setupTestValues();    
	        //do request
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();
	
	        req.requestURI = '/services/apexrest/searchCustomSettings';  
	        //send both type and variant parameters
	        req.addParameter('type', 'livechat');
	        req.addParameter('variant', 'FP');
	
	
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	
	        SMEQ_CustomSettingResponseModel cSettingResponse =  SMEQ_CustomSettingSearchRestService.searchCustomSettingsByType();
	        System.assert(cSettingResponse!=null); 
	        
	        //test data only has duplicate entries for SFDC_CHAT_BUTTON_ID
	        //duplicate entry should be removed and only three records should be returned
	        System.assert(cSettingResponse.customSettings.size() ==3);
	        
	        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, cSettingResponse.responseCode);
        }
    }

    /**
     * Method to test custom setting search rest service with variant and type parameters
     * 1. Assert response is not null
     * 2. Assert customsetting size matches number of records in database
     * 3. Assert response code is ok.
     * 
     */
    @isTest static void testSearchCustomSettingByTypeWithTypeAndDefaultVariant() {
		User uw = UserService_Test.getUwUser('', UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        
        System.runAs(uw)
        {
	        setupTestValues();    
	        //do request
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();
	
	        req.requestURI = '/services/apexrest/searchCustomSettings';  
	        //send both type and variant parameters
	        req.addParameter('type', 'livechat');
	        req.addParameter('variant', 'www'); //www is the default variant
	
	
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	
	        SMEQ_CustomSettingResponseModel cSettingResponse =  SMEQ_CustomSettingSearchRestService.searchCustomSettingsByType();
	        System.assert(cSettingResponse!=null); 
	        
	        //test data only has duplicate entries for SFDC_CHAT_BUTTON_ID
	        //duplicate entry should be removed and only three records should be returned
	        System.assert(cSettingResponse.customSettings.size()==3);
	        
	        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, cSettingResponse.responseCode);
        }
    }

     /**
     * Method to test custom setting search rest service with variant and type parameters
     * 1. Assert response is not null
     * 2. Assert response code is ERROR.
     * 
     */
    @isTest static void testSearchCustomSettingByTypeWithNoParameters() {
		User uw = UserService_Test.getUwUser('', UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        
        System.runAs(uw)
        {
	        setupTestValues();    
	        //do request
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();
	
	        req.requestURI = '/services/apexrest/searchCustomSettings';  
	        // no parameters are sent
	
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	
	        SMEQ_CustomSettingResponseModel cSettingResponse =  SMEQ_CustomSettingSearchRestService.searchCustomSettingsByType();
	        System.assert(cSettingResponse!=null); 
	        
	        //if no type or variant parameter is specified error code should be returned
	        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, cSettingResponse.responseCode);
        }
    }


    /**
     * Method to test custom setting search with a type parameter that is non existant
     * 1. Assert custom setting response is equal to null
     * 2. Assert response code is ok 
     */
    @isTest 
    static void testSearchCustomSettingByTypeNonExistantParam() {

		User uw = UserService_Test.getUwUser('', UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        
        System.runAs(uw)
        {
	        setupTestValues();    
	        //do request
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();
	
	        req.requestURI = '/services/apexrest/searchCustomSettings';  
	        req.addParameter('type', 'notlivechat');
	
	
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	
	        SMEQ_CustomSettingResponseModel cSettingResponse =  SMEQ_CustomSettingSearchRestService.searchCustomSettingsByType();
	        System.assert(cSettingResponse.customSettings == null);
	        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, cSettingResponse.responseCode);
        }
           
    }


    /**
     * Method to test search custom settings with null parameter
     * 1. Assert customSettings map is null
     * 
     */
    @isTest static void testSearchCustomSettingWithNullParam() {

		User uw = UserService_Test.getUwUser('', UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        
        System.runAs(uw)
        {
        setupTestValues();    
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/searchCustomSettings';  
        req.addParameter('type', null);


        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        SMEQ_CustomSettingResponseModel cSettingResponse =  SMEQ_CustomSettingSearchRestService.searchCustomSettingsByType();
        System.assert(cSettingResponse.customSettings==null);
        }
    }

    /**
    * Add test values for SME_Mapping
    */
    private static void setupTestValues() {

    //insert SME_Mapping__c Custom Settings

    SME_Mapping__c deploymentIDMapping   = new SME_Mapping__c(Name = 'Broker-livechat-01',
                                                                Field__c = 'livechat',
                                                                externalValue__c = '00D6300000093xH',
                                                                SFKey__c =  'SFDC_CHAT_DEPLOYMENT_ID',
                                                                variant__c = 'www'
                                                            );
    insert deploymentIDMapping;


    SME_Mapping__c buttonIDMapping = new SME_Mapping__c(Name='Broker-livechat-02',
                                                           Field__c = 'livechat',
                                                           externalValue__c = '573630000008ORb',
                                                           SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                           variant__c = 'www'
                                                            );
    insert buttonIDMapping;



    SME_Mapping__c buttonIDMappingFP = new SME_Mapping__c(Name='Broker-livechat-02',
                                                               Field__c = 'livechat',
                                                               externalValue__c = '573630000008ORb',
                                                               SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                               variant__c = 'FP'
                                                                );
    insert buttonIDMappingFP;



    SME_Mapping__c orgIDMapping = new SME_Mapping__c(Name='Broker-livechat-03',
                                                           Field__c = 'livechat',
                                                           externalValue__c = 'SFDC_ORG_ID',
                                                           SFKey__c = '00D6300000093xH',
                                                           variant__c = 'www'
                                                            );
    insert orgIDMapping;


    }
    }