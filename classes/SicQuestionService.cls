/**
 * SICQuestionService
 * Author: James Lee
 * Date: December 22 2016
 * Feature: FP-748
 * 
 * The purpose of this service class is to manage questions related to the SME quoting tool.
 */
public class SicQuestionService
{
    public static final String CODE_EQUIPMENT = '90001';
    public static final String CODE_STOCK = '90002';
    public static final String CODE_CONTENTS = '90003';
    public static final String CODE_ONSITE_FACILITIES = '90004';
    public static final String CODE_NUMBER_OF_UNITS = '90005';
    public static final String CODE_CANADIAN_REVENUE = '90006';
    public static final String CODE_US_REVENUE = '90007';
    public static final String CODE_FOREIGN_REVENUE = '90008';
    public static final String CODE_LIQUOR_ANNUAL_REVENUE = '90010';
    public static final String CODE_FOOD_REVENUE = '90011';
    
    public static final String EMPTY_MULTIPICKLIST_VALUE = '[]';
    public static final String RECORDTYPE_APPLICATION = 'Application';
    public static final String RECORDTYPE_ELIGIBILITY = 'Eligibility';
    
    // TODO: Convert to enum.
    public static final String CATEGORY_QUOTE = 'Quote';
    public static final String CATEGORY_RISK = 'Risk';
    
    public static final String SOBJECTTYPE_QUOTE = Quote__c.SObjectType.getDescribe().getName();
    public static final String SOBJECTTYPE_RISK = Risk__c.SObjectType.getDescribe().getName();
    
    public static final String INPUT_TYPE_TEXT = 'Text';
    public static final String INPUT_TYPE_NUMBER = 'Number';
    public static final String INPUT_TYPE_CURRENCY = 'Currency';
    public static final String INPUT_TYPE_CHECKBOX = 'Checkbox';
    public static final String INPUT_TYPE_LONG_TEXT = 'Long Text';
    public static final String INPUT_TYPE_SINGLE_SELECT_DROPDOWN = 'Single Select Dropdown';
    public static final String INPUT_TYPE_MULTI_SELECT_DROPDOWN = 'Multi Select Dropdown';
    
    //Each validation type key stores available validation types in SIC_Question_Validation_Messages__c
    //Validation Type Keys are also used to map to CustomValidators on the front end.
    public static final String INVALID_NUMBER_VALIDATION_TYPE = 'InvalidAmount';
    public static final String INVALID_MIN_AMOUNT_VALIDATION_TYPE = 'InvalidMinAmount';
    public static final String INVALID_REQUIRED_VALIDATION_TYPE = 'Required';
    
    private static final String numericRegex = '^[0-9]{1,3}(?:,?[0-9]{3})*(?:\\.[0-9].)?$';
    private static final Pattern numericPattern = pattern.compile(numericRegex);
    
    // Stores the map of question code to the question ID.
    private static Map<String, Sic_Question__c> storedSicQuestionIDs = new Map<String, Sic_Question__c>();
    
    /**
     * FP-748
     * 
     * Retrieves all questions related to a SIC code detail version.
     * 
     * @param scdv: The SIC code detail version to retrieve the list of all questions for.
     * @return a list of questions associated with the SIC code detail version.
     */
    public static List<SIC_Question__c> getSicQuestions(SIC_Code_Detail_Version__c scdv)
    {
        List<SIC_Code_Detail_Version__c> scdvs = new List<SIC_Code_Detail_Version__c>{scdv};
        
        return getSicQuestions(scdvs).get(scdv);
    }
    
    /**
     * FP-748
     * 
     * Retrieves all question ID mappings from its question code.
     * 
     * @return questionCodeMap: map of external ids for the question and equivalent record id
     */
    public static Map<String, Sic_Question__c> getSicQuestionExternalIDMap()
    {
        if (storedSicQuestionIDs.isEmpty())
        {
            storedSicQuestionIDs = new Map<String, Sic_Question__c>();
            for (SIC_Question__c scqa : [
                SELECT id, QuestionId__c, Category__c
                FROM SIC_Question__c
            ])
            {
                storedSicQuestionIDs.put(String.valueOf(scqa.questionId__c), scqa);
            }
        }
        
        return storedSicQuestionIDs; 
    }
    
    /**
     * FP-748
     * 
     * Retrieves all questions related to a SIC code detail versions.
     * 
     * @param scdvs: The SIC code detail version to retrieve the list of all questions for.
     * @return a list of questions associated with the SIC code detail version.
     */
    public static Map<SIC_Code_Detail_Version__c, List<SIC_Question__c>> getSicQuestions(List<SIC_Code_Detail_Version__c> scdvs)
    {
        // SIC Code Detail Version to SIC Questions map to be returned.
        Map<SIC_Code_Detail_Version__c, List<SIC_Question__c>> scdv2sqs = new Map<SIC_Code_Detail_Version__c, List<SIC_Question__c>>();
        
        // Convert List to Id mapped List elements.
        Map<Id, SIC_Code_Detail_Version__c> scdvMap = new Map<Id, SIC_Code_Detail_Version__c>(scdvs);
        
        // Initialize SIC Code Detail Version to SIC Questions map.
        for (SIC_Code_Detail_Version__c scdv : scdvMap.values())
        {
            scdv2sqs.put(scdv, new List<SIC_Question__c>());
        }
        
        // Query for all question associations.
        Map<Id, Id> scdvm = new Map<Id, Id>();
        List<Id> questionIds = new List<Id>();
        for (SIC_Code_Question_Association__c scqa : [
            SELECT id, questionId__c, SIC_Code_Detail_Version__c
            FROM SIC_Code_Question_Association__c
            WHERE SIC_Code_Detail_Version__c IN :scdvs
            ORDER BY order__c ASC
        ])
        {
            scdvm.put(scqa.questionId__c, scqa.SIC_Code_Detail_Version__c);
            questionIds.add(scqa.questionId__c);
        }
        
        // Create the list of questions for each SIC Code Detail Version.
        for (SIC_Question__c sq : [
            SELECT id, QuestionId__c,
            AvailableValuesEn__c, AvailableValuesFr__c,
            Category__c,
            Sub_Category__c,
            Input_Type__c,
            IsRequired__c,
            QuestionText_en__c,
            QuestionText_fr__c,
            Question_Text__c,
            Xpath__c,
            Resource_Bundle_Label__r.string_id__c,
            Resource_Bundle_Place_Holder_Key__r.string_id__c,
            RecordtypeId,
            RecordType.DeveloperName,
            Question_Code__c,
            FrontEndVisibility__c,
            Order__c,
            Minimum_Validation_Amount__c,
            Minimum_Validation_Required__c
            FROM SIC_Question__c
            WHERE id IN :questionIds
            ORDER BY QuestionId__c
        ])
        {
            SIC_Code_Detail_Version__c scdv = scdvMap.get(scdvm.get(sq.id));
            
            List<SIC_Question__c> sqs = scdv2sqs.get(scdv);
            sqs.add(sq);
            scdv2sqs.put(scdv, sqs);
        }
        
        return scdv2sqs;
    }
    
    /**
     * FP-760
     * 
     * Bulkified retrieval of all answers.
     * 
     * @param sIds: A list of sObject Ids to retrieve SIC Answers for.
     * @param sqs: A list of SIC Questions to retrieve SIC Answers for.
     * @return: A map from an sObject id to a map from a SIC Question to its SIC Answer.
     */
    public static Map<Id, Map<SIC_Question__c, SIC_Answer__c>> getSicAnswers(List<Id> sIds, List<SIC_Question__c> sqs)
    {
        Map<Id, Map<SIC_Question__c, SIC_Answer__c>> idsqam = new Map<Id, Map<SIC_Question__c, SIC_Answer__c>>();
        
        List<SIC_Answer__c> sas = new List<SIC_Answer__c>();
        
        if (sIds != null && !sIds.isEmpty())
        {
            Set<Id> ids = new Set<Id>(sIds);
            ids.remove(null); // Remove all null ids.
            
            sas = [
                SELECT id, Quote__c, Risk__c, Value__c, Long_Value__c, Question_Code__c, Question_Code__r.questionId__c
                FROM SIC_Answer__c
                WHERE Question_Code__c IN :sqs
                AND (
                    quote__c IN :ids
                    OR
                    risk__c IN :ids
                )
            ];
        }
        // Retrieve the SIC Question to SIC Answer map for each sObject ID.
        for (Id sid : sids)
        {
            Map<SIC_Question__c, SIC_Answer__c> sqam = new Map<SIC_Question__c, SIC_Answer__c>();
            String objType = sid == null ? null : utils.getObjectType(sid);
            String objTypeName;
            if(objType == SOBJECTTYPE_RISK){
                objTypeName = 'Risk';
            }
            if(objType == SOBJECTTYPE_QUOTE){
                objTypeName = 'Quote';
            }
            // The map is driven by the questions.
            for (SIC_Question__c sq : sqs)
            {
                for (SIC_Answer__c sa : sas)
                {
                    if (sa.Question_Code__c == sq.Id && 
                        ((sa.Quote__c == sid && objType == SOBJECTTYPE_QUOTE) ||
                         (sa.Risk__c == sid && objType == SOBJECTTYPE_RISK)))
                    {
                        sqam.put(sq, sa); // Only add the question answer pair if the question belongs to the correct parent type.
                        break;
                    }
                }
                
                // If no answer to the question is found, then insert the key anyway and map to a new empty SIC Answer record.
                if (!sqam.containsKey(sq))
                {
                    SIC_Answer__c sa = new SIC_Answer__c();
                    sa.Question_Code__c = sq.Id;
                    
                    // Skip answer assignment if no parent record is specified.
                    if (sId != null)
                    {
                        if(sq.Category__c == objTypeName){
                            // Assign answer to the appropriate parent sObject type.
                            if (objType == SOBJECTTYPE_QUOTE)
                            {
                                sa.Quote__c = sId;
                            }
                            else if (objType == SOBJECTTYPE_RISK)
                            {
                                sa.Risk__c = sId;
                            }
                        }
                        else{
                            continue;
                        }
                    }
                    
                    sqam.put(sq, sa);
                }
            }
            
            idsqam.put(sid, sqam);
        }
        return idsqam;
    }
    
    /**
     * FP-748
     * 
     * Retrieves all Application Sic Questions from the Sic Questions service.
     * 
     * @param scdv: The SIC Code Detail Version to retrieve all Sic Questions for.
     * @param category: The category of the question. i.e. Quote or Risk
     * @return: A List of only Application record type SIC Questions for the SIC Code Detail Version.
     */
    public static List<SIC_Question__c> getAllApplicationQuestions(SIC_Code_Detail_Version__c scdv, String category)
    {
        List<SIC_Question__c> sqs = new List<SIC_Question__c>();
        
        if (scdv != null)
        {
            Map<String,Id> SICQuestionTypes = Utils.GetRecordTypeIdsByDeveloperName(SIC_Question__c.SObjectType, true);
            
            for (SIC_Question__c sq : getSicQuestions(scdv))
            {
                if (sq.RecordTypeId == SICQuestionTypes.get(RECORDTYPE_APPLICATION) &&
                    sq.Category__c == category)
                {
                    sqs.add(sq);
                }
            }
        }
        
        return sqs;
    }
    
    /**
     * FP-748
     * 
     * Retrieves all answers.
     * 
     * @param sId: An ID of a quote or risk.
     * @param sqs: The list of all questions to retrieve answers for.
     * @return a map of questions to answers associated with the SIC code detail version.
     */
    public static Map<SIC_Question__c, SIC_Answer__c> getSicAnswers(Id sId, List<SIC_Question__c> sqs)
    {
        // Add the sId to a list and pass to the bulkified version of getSicAnswers.
        return getSicAnswers(new List<Id>{sId}, sqs).get(sId);
    }
    
    /**
     * FP-748
     * 
     * Retrieves all answers.
     * 
     * @param sId: An ID of a quote or risk.
     * @return a list of answers associated with the ID.
     */
    public static List<SIC_Answer__c> getSicAnswers(ID sId)
    {
        List<Sic_Answer__c> answersList = new List<Sic_Answer__c>();
        
        Map<Id, List<SIC_Answer__c>> answersMap = getSicAnswers(new Set<Id>{sId});
        if (answersMap.containsKey(sId))
        {
            answersList = answersMap.get(sId);
        }
        
        return answersList;
    }
    
    /**
     * Retrieves all answers.
     * 
     * @param sId: A set of IDs of a quote or risk.
     * @return a map of answers associated with the IDs.
     */
    public static Map<Id, List<Sic_Answer__c>> getSicAnswers(Set<Id> sIds)
    {
        sIds.remove(null);
        
        Map<Id, List<Sic_Answer__c>> answersMap = new Map<Id, List<Sic_Answer__c>>();
        
        List<Sic_Answer__c> sas = [
            SELECT id, Quote__c, Risk__c, Value__c, Long_Value__c, Question_Code__c, Question_Code__r.questionId__c
            FROM SIC_Answer__c
            WHERE (
                quote__c IN :sIds
                OR
                risk__c IN :sIds
            )
        ];
        
        for (Id sid : sIds)
        {
            String objType = utils.getObjectType(sid);
            
            List<SIC_Answer__c> answersList = new List<SIC_Answer__c>();
            
            for (Sic_Answer__c sa : sas)
            {
                // Assign answer to the appropriate parent sObject type.
                if ((objType == SOBJECTTYPE_QUOTE && sa.Quote__c == sId) ||
                    (objType == SOBJECTTYPE_RISK && sa.Risk__c == sId))
                {
                    answersList.add(sa);
                }
            }
            
            answersMap.put(sId, answersList);
        }
        
        return answersMap;
    }
    
    /**
     * FP-748
     * 
     * Validate the answers returned. 
     */
    public static ResultSet validateSicAnswers(List<SIC_Answer__c> sas)
    {
        ResultSet rs = new ResultSet();
        
        // Map the SIC Question Id to the SIC Answer. 
        Map<Id, SIC_Answer__c> sqids = new Map<Id, SIC_Answer__c>();
        Map<String, String> validationMessageMap  = new Map<String, String>();
        Map<String, String> userLanguageMap = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('language_code__c');
        System.debug('User lang set to #####' + UserInfo.getLanguage() + '######' + userLanguageMap + '######' + userLanguageMap);
        
        String languageCode = userLanguageMap.get(UserInfo.getLanguage());
        System.debug('Users language is currently #########' + languageCode);
        String validationLookupKey = '';
        String validationMessage = '';
        Matcher numericMatcher;
        
        for (SIC_Answer__c sa : sas)
        {
            sqids.put(sa.Question_Code__c, sa);
        }
        
        List<SIC_Question__c> sqs = [
            SELECT id, Input_Type__c, isRequired__c,
            AvailableValuesEn__c, AvailableValuesFr__c,
            Validation_Error_Message_Label__c, 
            Minimum_Validation_Required__c,
            Minimum_Validation_Amount__c
            FROM SIC_Question__c
            WHERE Id IN :sqids.keySet()
        ];
        
        System.debug('Question list being found ######' + sqs);
        //get map of validation messages available for questions
        validationMessageMap = getValidationMessagesMap(sqids.keySet());
        System.debug('Validation Message Map is #######' + validationMessageMap);
        
        // Map the SIC questions to the SIC Answers.
        Map<SIC_Question__c, SIC_Answer__c> sqam = new Map<SIC_Question__c, SIC_Answer__c>();
        for (SIC_Question__c sq : sqs)
        {
            sqam.put(sq, sqids.get(sq.Id));
        }
        
        // Validate answers for each question.
        for (SIC_Question__c sq : sqam.keySet())
        {
            SIC_Answer__c sa = sqam.get(sq);
            String key = getAnswerRecordKey(sa);
            ProcessingResult pr = new ProcessingResult(key);
            
            if (sa.Value__c != '' && 
                sa.Value__c != null && 
                sa.Long_Value__c != '' &&
                sa.Long_Value__c != null)
            {
                pr.addError(new ProcessingError('Only one value field should be populated'));
            }
            else if (sq.Input_type__c == INPUT_TYPE_CHECKBOX && 
                     sa.Value__c != 'true' &&
                     sa.Value__c != 'false')
            {
                pr.addError(new ProcessingError('Checkbox only allows true or false'));
            }
            //check if field is a required field and apply required validation
            else if (sq.IsRequired__c &&
                     (
                         (
                             (sq.Input_Type__c == INPUT_TYPE_LONG_TEXT ||
                              sq.Input_Type__c == INPUT_TYPE_MULTI_SELECT_DROPDOWN) &&
                             (sa.Long_Value__c == null ||
                              sa.Long_Value__c == '')
                         )
                         ||
                         (
                             (sq.Input_Type__c == INPUT_TYPE_TEXT ||
                              sq.Input_Type__c == INPUT_TYPE_NUMBER ||
                              sq.Input_Type__c == INPUT_TYPE_CURRENCY ||
                              sq.Input_Type__c == INPUT_TYPE_CHECKBOX ||
                              sq.Input_Type__c == INPUT_TYPE_SINGLE_SELECT_DROPDOWN) &&
                             (sa.Value__c == null ||
                              sa.Value__c == '')
                         )
                     )
                    )
            {
                validationLookupKey = sq.Id + INVALID_REQUIRED_VALIDATION_TYPE + languageCode; 
                validationMessage = validationMessageMap.get(validationLookupKey);
                pr.addError(new ProcessingError(validationMessage));
            }
            
            //for number and currency field types apply type type validations
            //two types of validations can be applied on currency and number fields
            //type validation, minimum amount validation
            else if ((sq.Input_Type__c == INPUT_TYPE_CURRENCY || sq.Input_Type__c == INPUT_TYPE_NUMBER))
            {
                if (sa.Value__c != null && sa.Value__c != '')
                {
                    System.debug('YYYYY Entering invalid number #######' + sa.Value__c);
                    //type validation
                    numericMatcher = numericPattern.matcher(sa.Value__c);
                    if (!numericMatcher.matches())
                    {
                        //invalid number
                        validationLookupKey = sq.Id + INVALID_NUMBER_VALIDATION_TYPE + languageCode; 
                        validationMessage = validationMessageMap.get(validationLookupKey);
                        pr.addError(new ProcessingError(validationMessage));
                    }
                    //minimum amount validation
                    else if(sq.Minimum_Validation_Required__c &&
                            sq.Minimum_Validation_Amount__c != NULL && 
                            sq.Minimum_Validation_Amount__c != 0 &&
                            Double.valueOf((sa.Value__c).replace(',','')) < sq.Minimum_Validation_Amount__c)
                    {
                        //invalid minimum amount
                        validationLookupKey = sq.Id + INVALID_MIN_AMOUNT_VALIDATION_TYPE + languageCode; 
                        validationMessage = validationMessageMap.get(validationLookupKey);
                        pr.addError(new ProcessingError(validationMessage));
                    }
                }
            }
            //apply type validations for drop down fields
            else if (sq.Input_Type__c == INPUT_TYPE_SINGLE_SELECT_DROPDOWN)
            {
                if (sa.Value__c != null && !sq.AvailableValuesEn__c.contains(sa.Value__c))
                {
                    pr.addError(new ProcessingError('Value not applicable.'));
                }
            }
            //apply type validation for muliselect drop down fields
            else if (sq.Input_Type__c == INPUT_TYPE_MULTI_SELECT_DROPDOWN)
            {
                Boolean hasInvalidValue = false;
                if (sa.Value__c != null)
                {
                    for (String value : sa.Value__c.split(';'))
                    {
                        if (!sq.AvailableValuesEn__c.contains(value))
                        {
                            hasInvalidValue = true;
                            break;
                        }
                    }
                    
                    if (hasInvalidValue)
                    {
                        pr.addError(new ProcessingError('Value not applicable.'));
                    }
                }
            }
            
            rs.addResult(pr);
        }
        return rs;
    }
    
    /**
     * FP-748
     * 
     * Preformats answers if necessary, save a list of answers to the database.
     */
    public static void saveSicAnswers(List<SIC_Answer__c> sas)
    {
        List<Id> saqid = new List<Id>();
        for (SIC_Answer__c sa : sas)
        {
            saqid.add(sa.Question_Code__c);
        }
        
        Map<Id, Sic_Question__c> sqs = new Map<Id, Sic_Question__c>([
            SELECT id, Input_Type__c
            FROM Sic_Question__c
            WHERE id IN :saqid
        ]);
        
        for (SIC_Answer__c sa : sas)
        {
            if (sqs.get(sa.Question_Code__c) != null)
            {
                String inputType = sqs.get(sa.Question_Code__c).Input_Type__c;
                
                // Remove commas from numeric answers.
                if (sa.Value__c != null &&
                    (inputType == INPUT_TYPE_CURRENCY || inputType == INPUT_TYPE_NUMBER))
                {
                    sa.Value__c = sa.Value__c.remove(',');
                }
            }
        }
        
        upsert sas;
    }

    /**
     * FP-2824
     * Returns Map of validation message values for dynamic questions
     * Map will be used to lookup validation messages on the underwriter view
     * Aggregate Key questionID, 
     * @param ID
     * @return Map<String,String>
     */
    public static Map<String, String> getValidationMessagesMap(Set<Id> queIDList)
    {
        Map<String, String> validationMessageMap = new Map<String, String>();
        //select SIC Question Validation messages where IDs exist in sic validation messages
        List<SIC_Question_Validation_Messages__c> validationMessages = [
            SELECT ID, 
            Resource_Bundle_Validation_Message__r.Id,
            Resource_Bundle_Validation_Message__r.string_id__c, 
            Resource_Bundle_Validation_Message__r.value__c,
            Resource_Bundle_Validation_Message__r.language_code__c,
            Validation_Type_Key__c,
            SIC_Question__r.Id
            FROM SIC_Question_Validation_Messages__c
            WHERE SIC_Question__r.Id IN:queIDList
        ];
        
        if (!validationMessages.isEmpty())
        {
            for (SIC_Question_Validation_Messages__c vm : validationMessages)
            {
                //create a combined key for the validation message map
                //unique key will include questionID, validationType, languageCode
                String validationLookupKey = vm.SIC_Question__r.Id  +  
                    vm.Validation_Type_Key__c + 
                    vm.Resource_Bundle_Validation_Message__r.language_code__c;
                String validationMessage = vm.Resource_Bundle_Validation_Message__r.value__c;
                
                //build map of resource bundle value and uniqueIDs
                validationMessageMap.put(validationLookupKey, validationMessage);
            }
        }
        return validationMessageMap;
        
    }
    

    /**
     * FP-2824
     * Returns Map of validation message values for dynamic questions
     * Map will be used to lookup validation messages in the Broker view
     *
     * @return Map<String,Map>
     */
    public static Map<String, Map<String, String>> getValidationMessagesMapForBrokerView(Set<Id> queIDList)
    {
        
        Map<String, Map<String, String>> errorMessageMap = new Map<String,  Map<String, String>>();
        //select SIC Question Validation messages where IDs exist in sic validation messages
        List<SIC_Question_Validation_Messages__c> validationMessages = [
            SELECT ID, 
            Resource_Bundle_Validation_Message__r.Id,
            Resource_Bundle_Validation_Message__r.string_id__c, 
            Resource_Bundle_Validation_Message__r.value__c,
            Resource_Bundle_Validation_Message__r.language_code__c,
            Validation_Type_Key__c,
            SIC_Question__r.Id
            FROM SIC_Question_Validation_Messages__c
            WHERE SIC_Question__r.Id IN:queIDList
        ];
        
        if(!validationMessages.isEmpty())
        {
            for(ID queID:queIDList)
            {
                Map<String, String> valTypeMessageMap = new Map<String, String>();
                for(SIC_Question_Validation_Messages__c vm: validationMessages)
                {
                    
                    
                    String valQueID = vm.SIC_Question__r.Id;
                    String valType  = vm.Validation_Type_Key__c;
                    String valStringId  = vm.Resource_Bundle_Validation_Message__r.string_id__c;
                    
                    //if value hasnt been added to map already(duplicates would exist for string ids with same language)
                    if(vm.SIC_Question__r.Id==queID && (valTypeMessageMap.get(valType) == null))
                    {
                        if(valType == SicQuestionService.INVALID_REQUIRED_VALIDATION_TYPE)
                        {
                            valType = valType.toLowerCase();
                        }
                        
                        valTypeMessageMap.put(valType, valStringId);
                    }
                }
                
                errorMessageMap.put(String.valueOf(queID), valTypeMessageMap);
            }
            
            
        }
        
        return errorMessageMap;
    }
    
    /**
     * FP-748
     * 
     * The key is required to identify a SIC Answer during validation. 
     * If the record has already been inserted into the database, the record ID is the key.
     * Otherwise, generate it based on the question ID and target ID tuple. 
     * 
     * @param sa: The SIC Answer to retrieve its key.
     * @return: A string uniquely representing the SIC Answer.
     */
    public static String getAnswerRecordKey(SIC_Answer__c sa)
    {
        if (sa.Id != null)
        {
            return sa.Id;
        }
        else
        {
            // Get the quote or risk ID.
            Id targetId = sa.Quote__c != null ? sa.Quote__c : sa.Risk__c != null ? sa.Risk__c : null;
            return sa.Question_Code__c + ':' + targetId;
        }
    }
    
    public class SicQuestionServiceException extends Exception
    {
        
    }
    
}