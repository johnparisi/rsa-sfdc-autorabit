public with sharing class QuoteVersionTriggerHandler {
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public static boolean firstRun = true;
    
    public QuoteVersionTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        public void OnBeforeInsert(Quote_Version__c[] newQuoteVersions){
        //CISME_Int : Set the source System User on Sigma cases
        SLID_SetSourceSystemUser.SetSourceSystemUser(newQuoteVersions); 
        
        Map<ID,ID> mapCaseIdQuoteVersionIds = new Map<ID,ID>();
        //Example usage
        for(Quote_Version__c newQuoteVersion : newQuoteVersions){
            if(newQuoteVersion.Primary__c==true)
                mapCaseIdQuoteVersionIds.put(newQuoteVersion.Case__c, null);System.debug('Limits.getAggregateQueries - '+ Limits.getAggregateQueries());
System.debug('BeforeInsert');
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getQueriesRow - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
System.debug('BeforeInsertEnd');
                
        }
        resetQuoteVersions(mapCaseIdQuoteVersionIds);
    }

    public void OnAfterInsert(Quote_Version__c[] newQuoteVersions){
        //CISME_Int : Close parent case if all child quotes are closed
        SLID_CaseToClosed.updateStatusToClosed(null,newQuoteVersions);
    }
    /*
    @future public static void OnAfterInsertAsync(Set<ID> newQuoteVersionIDs){
        //Example usage
        if (firstRun) {
            firstRun = false;
        }        
        //QuoteVersionTriggerHandler handler = new QuoteVersionTriggerHandler(null, null);
        //handler.copyQuoteVersionToEmail(newQuoteVersionIDs);             
    }*/
    
    public void OnBeforeUpdate(Quote_Version__c[] oldQuoteVersions, Quote_Version__c[] updatedQuoteVersions, Map<ID, Quote_Version__c> QuoteVersionMap){
        //CISME_Int : Set the source System User on Sigma cases
        SLID_SetSourceSystemUser.SetSourceSystemUser(updatedQuoteVersions); 

        //Example usage
        Map<ID,ID> mapCaseIdQuoteVersionIds = new Map<ID,ID>();
        //Example usage
        for(Quote_Version__c newQuoteVersion : updatedQuoteVersions){
            if(newQuoteVersion.Primary__c==true)
                mapCaseIdQuoteVersionIds.put(newQuoteVersion.Case__c, newQuoteVersion.Id);
System.debug('BeforeUpdate');
System.debug('Limits.getLimitAggregateQueries - '+ Limits.getLimitAggregateQueries());
System.debug('Limits.getCallouts - '+ Limits.getCallouts());
System.debug('Limits.getLimitCallouts - '+ Limits.getLimitCallouts());
System.debug('Limits.getCpuTime - '+ Limits.getCpuTime());
System.debug('Limits.getLimitCpuTime - '+ Limits.getLimitCpuTime());
System.debug('Limits.getDMLRows - '+ Limits.getDMLRows());
System.debug('Limits.getLimitDMLRows - '+ Limits.getLimitDMLRows());
System.debug('Limits.getDMLStatements - '+ Limits.getDMLStatements());
System.debug('Limits.getLimitDMLStatements - '+ Limits.getLimitDMLStatements());
System.debug('Limits.getEmailInvocations - '+ Limits.getEmailInvocations());
System.debug('Limits.getLimitEmailInvocations - '+ Limits.getLimitEmailInvocations());
System.debug('Limits.getFutureCalls - '+ Limits.getFutureCalls());
System.debug('Limits.getLimitFutureCalls - '+ Limits.getLimitFutureCalls());
System.debug('Limits.getHeapSize - '+ Limits.getHeapSize());
System.debug('Limits.getLimitHeapSize - '+ Limits.getLimitHeapSize());
System.debug('Limits.getMobilePushApexCalls - '+ Limits.getMobilePushApexCalls());
System.debug('Limits.getLimitMobilePushApexCalls - '+ Limits.getLimitMobilePushApexCalls());
System.debug('Limits.getQueries - '+ Limits.getQueries());
System.debug('Limits.getQueriesRow - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueries - '+ Limits.getLimitQueries());
System.debug('Limits.getQueryLocatorRows - '+ Limits.getQueryLocatorRows());
System.debug('Limits.getLimitQueryLocatorRows - '+ Limits.getLimitQueryLocatorRows());
System.debug('Limits.getQueryRows - '+ Limits.getQueryRows());
System.debug('Limits.getLimitQueryRows - '+ Limits.getLimitQueryRows());
System.debug('Limits.getQueueableJobs - '+ Limits.getQueueableJobs());
System.debug('Limits.getLimitQueueableJobs - '+ Limits.getLimitQueueableJobs());
System.debug('Limits.getSoslQueries - '+ Limits.getSoslQueries());
System.debug('Limits.getLimitSoslQueries - '+ Limits.getLimitSoslQueries());
System.debug('BeforeInsertEnd');
        }
        resetQuoteVersions(mapCaseIdQuoteVersionIds);         
    }
    
    public void OnAfterUpdate(Quote_Version__c[] oldQuoteVersions, Quote_Version__c[] updatedQuoteVersions, Map<ID, Quote_Version__c> QuoteVersionMap){
        //CISME_Int : Close parent case if all child quotes are closed
        SLID_CaseToClosed.updateStatusToClosed(oldQuoteVersions,updatedQuoteVersions);
        
        if (firstRun) {
            firstRun = false;
        }
        else {
            System.debug('Already ran!');
            return;
        }       
        
    }
    
    /*
    @future public static void OnAfterUpdateAsync(Set<ID> updatedQuote_Version__cIDs){
        //QuoteVersionTriggerHandler handler = new QuoteVersionTriggerHandler(null, null);
        //handler.copyQuoteVersionToEmail(updatedQuoteVersionIDs);
    }
    
    public void OnBeforeDelete(Quote_Version__c[] QuoteVersionsToDelete, Map<ID, Quote_Version__c> QuoteVersionMap){
        
    }
    
    public void OnAfterDelete(Quote_Version__c[] deletedQuoteVersions, Map<ID, Quote_Version__c> QuoteVersionMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedQuoteVersionIDs){
        
    }
    
    public void OnUndelete(Quote_Version__c[] restoredQuote_Versions){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }*/

    public void resetQuoteVersions(Map<ID,ID> mapCaseIdQuoteVersionIds){
        
        List<Quote_Version__c> lstQuoteVersion = new List<Quote_Version__c>();
        string strParentId;
        
        for(Quote_Version__c qvs : [SELECT Id, Primary__c
                                    FROM Quote_Version__c 
                                    WHERE Case__c IN : mapCaseIdQuoteVersionIds.keySet()
                                    AND Id NOT IN : mapCaseIdQuoteVersionIds.values()
                                    AND Primary__c = true]){
            qvs.Primary__c = false;                            
            lstQuoteVersion.add(qvs);
        } 
        
        if(lstQuoteVersion.size() > 0)
            update lstQuoteVersion;    
    }       
}