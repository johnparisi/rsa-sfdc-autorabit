/**
* @File Name    :  BigMachinesQuoteTriggerTest
* @Description  :  Test class for BigMachinesQuoteTrigger
* @Date Created :  10/03/2017
* @Author       :  Priyanka Sirohi @ Deloitte
* @group        :  testing class
* @Modification Log:
*****************************************************************
* Ver       Date        Author              Modification
* 1.0       01/20/2016  Priyanka Sirohi        Created the file/class
**/

@isTest
Private Class BigMachinesQuoteTriggerTest {

static testmethod void TestupdateOpportunityStageBasedOnQuoteState() {

       //Product
        Product2 product = new Product2();
        product.Name ='Property & Casualty GSL Test';
        product.IsActive= true;
        product.Family='Mid-Market & Global Specialty Lines';
        insert product;

    // Instantiate the Pricebook2 record first, setting the Id
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        // Run an update DML on the Pricebook2 record
        // This is the weird workaround that enables IsStandard to become true
        // on the PricebookEntry record
        update standardPricebook;
        
        // Re-Query for the Pricebook2 record, for debugging
        standardPricebook = [SELECT IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];
        
        
        
        // Create the PricebookEntry
        PricebookEntry testPbe = new PricebookEntry(
            Pricebook2Id = standardPricebook.Id,
            Product2Id = product.Id,
            UnitPrice = 100,
            IsActive = true
        );
        
        insert testPbe;
        
     
       //Create New Account for Contact
          Account acc = new Account ();
          acc.Name = 'MICHAEL H. BRUCE INSURANCE Ltd. Testing';
          acc.RecordTypeId = '012o0000000Ag6G';
          insert acc;   
               
         //Create new Producer contact
        Contact con = new Contact();
        con.FirstName ='Priyanka';
        con.LastName = 'Sirohi';
        con.AccountId = acc.ID;
        con.RecordTypeId = '012o0000000Adbq';
        insert con;
          
      BigMachines__Configuration_Record__c bmsite = New BigMachines__Configuration_Record__c();    
      bmsite.BigMachines__bm_site__c = 'rsacanada';
      bmsite.BigMachines__process_id__c = '1';
      bmsite.BigMachines__action_id_copy__c = '2';
      bmsite.BigMachines__action_id_open__c = '3';
      bmsite.BigMachines__document_id__c = '4';
      bmsite.BigMachines__version_id__c = '5';
      bmsite.BigMachines__process__c = 'oraclecpqo';
      bmsite.BigMachines__Is_Active__c = true;
      insert bmsite;
      
      //Create a Sample Quote Record 
      BigMachines__Quote__c bmquote = New BigMachines__Quote__c();
      bmquote.Name = 'GSL-1000V1';
      bmquote.BigMachines__Is_Primary__c = True;
      bmquote.BigMachines__Site__c = bmsite.ID;
      bmquote.BigMachines__Status__c = 'Pending';
      bmquote.Policy_Effective_Date__c = Date.today().addDays(12);
      bmquote.Policy_Expiry_Date__c = Date.today().addDays(377);
      bmquote.Premium__c = 10000.00;
      
                
              
      // Create New Business Opportunity.
        
        Opportunity opp = new Opportunity();
        opp.Type = 'New Business';
        opp.bkrProductLineEffectiveDate__c = Date.today().addDays(12);
        opp.bkrProduct__c = product.Id;
        opp.bkrOpportunity_Producer__c = con.Id;
        opp.RecordTypeId = '012o0000000xtgu';
        opp.Name = 'Test opp line item';
        opp.StageName = 'Underwriting';
        opp.Probability = 10.0;
        opp.CloseDate = Date.today().addDays(12);
        opp.bkrOpportunity_Region__c = 'Atlantic';
        insert opp;
      
        OpportunityLineItem OppLineItem = new opportunitylineitem(Discount=10.00,Quantity=3,UnitPrice=1000,
                                                                  opportunityid=opp.Id,
                                                                  pricebookentryid=testPbe.id,ServiceDate =System.Today()+40);
        insert OppLineItem;
        
        bmquote.BigMachines__Opportunity__c = opp.Id;
        
        insert bmquote;
      
        Opportunity insertedOpportunity = [SELECT Id, StageName FROM Opportunity][0];

        System.assert(insertedOpportunity.StageName == 'Underwriting','Opportunity Stage on the NB Opportunity' + insertedOpportunity.Id + 'should be underwriting');
  }
}