@isTest
private class SMEQ_QuoteSearchDao_Test
{
  /**
    * This method provides test assertions to perform a quote search base on incoming search terms
    * 1. Assert response is not null
    * 2. Assert response's payload is not null
    */
    @isTest static void testSearchQuote()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_QuoteSearchRequestModel request = new SMEQ_QuoteSearchRequestModel();
            request.searchText = 'test';
            request.brokerID = '123';
            SMEQ_QuoteSearchDao searchDAO = new SMEQ_QuoteSearchDao ();
            
            Case tempCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            tempCase.Quote_application_handed_over_to_UW__c = true;
            tempCase.Status = 'In Progress';
            update tempCase;
            
            Quote__c testQuote = SMEQ_TestDataGenerator.getOpenQuote(tempCase);
            
            SMEQ_QuoteSearchResponseModel response = searchDAO.searchQuote(request);
            System.assertNotEquals(null, response);
            System.assertNotEquals(null, response.payload);
        }
    }
    
   /**
    * This method provides test assertions to get quote status base on broker status and underwriter status mapping
    * 1. Assert status is Handed Over, status number 7
    * 2. Assert status is Finalize, status number 3
    * 3. Assert status is About Business, status number 1
    * 4. Assert status is Customize Quote, status number 2
    * 5. Assert status is Bound, status number 4
    * 6. Assert status is Policy Close, status number 5
    * 7. Assert status is Expired, status number 6 
    */
    @isTest static void testGetQuoteStatus()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            Map<String, Id> caseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);
            Case tempCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            tempCase.Status = 'In Progress';
            update tempCase;
            
            Quote__c testQuote = SMEQ_TestDataGenerator.getOpenQuote(tempCase);
            testQuote.Quote_Expiry_Date__c = Date.today();
            testQuote.Package_Type_Selected__c = 'Standard';
            testQuote.Status__c = 'In Progress';
            testQuote.Broker_Application_Stage__c = 'About the Business';
            update testQuote;
            
            Quote__c updatedQuote = [
                SELECT Quote_application_handed_over_to_UW__c,
                Status__c,
                Broker_Application_Stage__c,
                Quote_Expiry_Date__c 
                FROM Quote__c
                WHERE Id = :testQuote.Id
            ];
            
            Test.startTest();
            
            SMEQ_QuoteSearchDao objDao = new SMEQ_QuoteSearchDao();
            String status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            
            // System.debug('status@@@@@@@@@@@@@@@@@@@@@@@@@@='+updatedQuote.Quote_application_handed_over_to_UW__c);
            System.assert(status != null);
            //System.assertEquals('7', status);
            
            tempCase.Quote_application_handed_over_to_UW__c = false;
            //Bypass case assignment rules from assigning to a new owner, and bypass validation error
            tempCase.OwnerId = UserInfo.getUserId();
            tempCase.bkrCase_Subm_Type__c = 'BOR';
            update tempCase;
            
            updatedQuote.status__c = 'Finalized';
            updatedQuote.Broker_Application_Stage__c = null;      
            update updatedQuote;
            
            updatedQuote = [
                SELECT Quote_application_handed_over_to_UW__c,
                Status__c,
                Broker_Application_Stage__c,
                Quote_Expiry_Date__c 
                FROM Quote__c
                WHERE Id = :testQuote.Id
            ];        
            status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            System.assertEquals('3', status);
            
            updatedQuote.Broker_Application_Stage__c = 'About the Business';
            update updatedQuote;
            status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            System.assertEquals('1', status);
            
            updatedQuote.Broker_Application_Stage__c = 'Customize Quote';
            update updatedQuote;
            status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            System.assertEquals('2', status);
            
            updatedQuote.Broker_Application_Stage__c = 'Buy';
            //updatedQuote.Broker_Application_Stage__c = 'Customize Quote';
            updatedQuote.Status__c = 'Bind';
            update updatedQuote;
            status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            System.assertEquals('4', status);
            
            updatedQuote.Broker_Application_Stage__c = null;
            updatedQuote.Status__c = 'Policy Issued – Closed';
            update updatedQuote;
            status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            System.assertEquals('5', status);      
            
            updatedQuote.Quote_Expiry_Date__c = Date.today().addDays(-1);
            update updatedQuote;
            //System.debug('updatedQuote@@@@@@@@@@@@@@@@@@@@@@@@@@='+updatedQuote.Quote_Expiry_Date__c);
            status = SMEQ_QuoteSearchDao.getQuoteStatus(updatedQuote);
            //System.debug('status @@@@@@@@@@@@@@@@@@@@@@@@@@='+status);
            System.assertEquals('6', status);
            
            Test.stopTest();
        }
    }   
  
   /**
    * This to test exception scenario
    * 1. Assert response contains error
	*/
    @isTest static void testSearchQuoteException()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_QuoteSearchDao searchDAO = new SMEQ_QuoteSearchDao ();
            
            try
            {
                SMEQ_QuoteSearchResponseModel response = searchDAO.searchQuote(null);
            }
            catch(Exception e)
            {
                System.assert(e!=null);
            }
        }
    }  
    
}