/**
 * Author: James Lee
 * Created At: November 4, 2016
 * 
 * Unit tests for CaseTriggerTest.
 */
@isTest
public class CaseTriggerTest
{
    static testMethod void testConvertTriageCasesToSMEQCases()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {

            Map<String,Id> caseTypes = Utils.GetRecordTypeIdsByDeveloperName(Case.SObjectType, true);
            
            Case tc = SMEQ_TestDataGenerator.getTriageCase();
            Group g = SMEQ_TestDataGenerator.getSMEBrokerUWGroup();
            System.assert(g != null, 'Queue (SME_Broker_UW) is required for the deployment of SME UW.');
            tc.OwnerId = g.Id;
            update tc;
            
            tc = [
                SELECT id, recordTypeId
                FROM Case
                WHERE id = :tc.Id
            ];
            
            System.assertEquals(caseTypes.get('CI_Open_SPRNT'), tc.RecordTypeId);
        }
    }
    
    static testMethod void testGenerateSMEQuoteForTriageCases()
    {
        User uwLevel3 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel3)
        {
            Id userId = UserInfo.getUserId();
            
            Map<String,Id> caseTypes = Utils.GetRecordTypeIdsByDeveloperName(Case.SObjectType, true);

            Account ins = SMEQ_TestDataGenerator.getInsuredAccount();
            Case tc = SMEQ_TestDataGenerator.getTriageCase();
            Group g = SMEQ_TestDataGenerator.getSMEBrokerUWGroup();
            
            System.assert(g != null, 'Queue (SME_Broker_UW) is required for the deployment of SME UW.');
            
            // Updating a case owner to the SME_Broker_UW queue will generate a quote for the case. 
            List<Quote__c> q = [
                SELECT id
                FROM Quote__c
                WHERE case__c = :tc.Id
            ];
            
            System.assertEquals(0, q.size()); // Confirm no quote has been created for it yet.
            
            tc.OwnerId = g.Id;
            tc.Offering_Project__c = UserService.getOffering(userId)[0];
            tc.bkrCase_Region__c = UserService.getRegion(userId)[0];
            update tc; // Update triage case with sufficient information.
            
            q = [
                SELECT id, Status__c, Canadian_Revenue__c
                FROM Quote__c
                WHERE case__c = :tc.Id
            ];
            
            System.assertEquals(1, q.size()); // Confirm a quote has now been created for the case.
            System.assertEquals('New', q[0].Status__c);
            
            List<Sic_Answer__c> sas = SicQuestionService.getSicAnswers(q[0].Id);
            for (Sic_Answer__c sa : sas)
            {
                if (sa.questionId__c == Integer.valueOf(SicQuestionService.CODE_CANADIAN_REVENUE))
                {
                    System.assertEquals(String.valueOf(ins.AnnualRevenue), sa.Value__c);
                }
            }
            
            // If a case with a quote is converted to a non-CI_Open_SPRNT case,
            // and the reverted to a CI_Open_SPRNT case,
            // another quote should not be generated for the case.
            tc.RecordTypeId = caseTypes.get('CI_New_Business');
            update tc;
            tc.RecordTypeId = caseTypes.get('CI_Open_SPRNT');
            update tc;
            
            q = [
                SELECT id
                FROM Quote__c
                WHERE case__c = :tc.Id
            ];
            System.assert(q.size() == 1); // Confirm no new quote has been created for the case.
        }
    }
}