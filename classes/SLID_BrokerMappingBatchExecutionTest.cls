@isTest (SeeAllData=False)
private class SLID_BrokerMappingBatchExecutionTest {

    static testMethod void testBrokerMappingBatch() {
        // Create an account 
        Account acct = new Account();
        acct.RecordTypeId='012o0000000AgDrAAK';
        acct.name = 'Test1';
        acct.accountNumber = '0119274543';
        acct.bkrAccount_Status__c='Active';
        insert acct;
         
        //Creating test data for the Stage Broker Object
        Broker_Stage__c brokerData = new Broker_Stage__c();
        brokerData.account__c = acct.id;
        brokerData.ADDRIND1__c = 'P';
        brokerData.ADDRNO1__c = '169';
        brokerData.AGENT_CITY__c = 'GUYSBOROUGH';
        brokerData.AGENT_POSTAL_CODE__c = 'B0H1N0';
        brokerData.AGENT_STATE__c = 'NS';
        brokerData.AGENT_STREET__c = '9996 HIGHWAY 16   PO BOX 169';
        brokerData.AGENT__c = 703830514;
        brokerData.BROKER_NUMBER__c = '150383074';
        brokerData.PARENT1_BROKER_NUMBER__c = '50205887';
        brokerData.PARENT2_BROKER_NUMBER__c = '';
        brokerData.PARENT3_BROKER_NUMBER__c ='';
        insert brokerData;
        
        //Populate the data for the custom setting
        SLID_Broker_Mapping_Settings__c brokerMappingSettings = new SLID_Broker_Mapping_Settings__c();
        brokerMappingSettings.Batch_Job_Admin_Email__c = 'echeong@salesforce.com';
        brokerMappingSettings.SLID_CreateBrokerMappingClassName__c= 'SLID_CreateBrokerMapping';
        brokerMappingSettings.SLID_PurgeBrokerMappingClassName__c= 'SLID_PurgeBrokerMapping';
        brokerMappingSettings.SLID_PurgeBrokerStageClassName__c= 'SLID_PurgeBrokerStage';
        insert brokerMappingSettings;
 
            
        //Invoke the VF page
        Test.StartTest();
        PageReference pageRef = Page.SLID_BrokerMappingBatchExecution;  
        Test.setCurrentPage(pageRef);
        
        SLID_BrokerMappingBatchExecutionCntrl controller = new SLID_BrokerMappingBatchExecutionCntrl();
        
        boolean renderAsyncjobs = controller.buttonsBlockRendered;
        Boolean renderBlock = controller.statusBlockRendered;
        
        if (renderAsyncjobs) {
          controller.executeDeleteBatchJob();
          controller.executeUpdateMappingDataJob(); 
               
          List<AsyncApexJob> jobs = [Select Id , JobType, Status , CompletedDate , ApexClassId 
                                From AsyncApexJob 
                                Where ApexClassId IN :controller.classIds
                                And JobType = 'BatchApex'
                                ORDER BY CompletedDate DESC LIMIT 10];
          system.assertEquals(2, jobs.size());
        }
        
        renderAsyncjobs = controller.buttonsBlockRendered;
        
        //test exception
        SLID_BrokerMappingBatchExecutionCntrl controllerException = new SLID_BrokerMappingBatchExecutionCntrl();
        
        controllerException.classIds = null;
        controllerException.executeDeleteBatchJob();
        controllerException.executeUpdateMappingDataJob(); 
        
        controllerException.classIdProcessNameMap = null;
        controllerException.mapBrokerMappingClassIds(controllerException.classIdProcessNameMap);
        Test.stopTest();        
    }
}