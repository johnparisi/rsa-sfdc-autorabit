public class mycontroller {
	public string currentobject { get; set; }
	public string currentfield { get; set; }
	public List<Wrapper> wrapList {get;set;}
	
	public selectoption[] getobjects() {
		selectoption[] objects = new selectoption[0];
		map<string,schema.sobjecttype> describe = schema.getglobaldescribe();
		for(string objectname:describe.keyset()) {
			if(objectname.toLowerCase() == 'quote')
			    objects.add(new selectoption(objectname,describe.get(objectname).getdescribe().getname()));
		}
		return objects;
	}
	
	public class wrapper {
	    public String strName {get;set;}
	    public String strLabel {get;set;}
	    public Schema.DisplayType strType {get;set;}
	    public Integer strLength {get;set;}
	}
	
	public mycontroller() {
	    getfields();
	}
	
	public wrapper[] getfields() {
		selectoption[] fields = new selectoption[0];
		currentobject = 'quote';
		map<string,schema.sobjecttype> describe = schema.getglobaldescribe();
		if(describe.containskey(currentobject)) {
			map<string,schema.sobjectfield> fieldmap = describe.get(currentobject).getdescribe().fields.getmap();
			
			wrapList = new List<Wrapper>();
			for(string fieldname:fieldmap.keyset()) {
				wrapper wrap = new wrapper();
				wrap.strLabel = fieldmap.get(fieldname).getdescribe().getlabel();
				wrap.strName = fieldmap.get(fieldname).getdescribe().getname();
				wrap.strType = fieldmap.get(fieldname).getdescribe().gettype();
				wrap.strLength = fieldmap.get(fieldname).getdescribe().getlength();
				
				wrapList.add(wrap);
			}
		}
		return wrapList;
	}
}