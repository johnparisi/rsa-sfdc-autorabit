public class CSIO_CommlCoverage
{
    public static boolean sendToEpolicy= false; // FP-6460
    public static String locationCoverageCodes = ''; // FP-6460
    public Coverages__c coverage;
    
    private String coverageName;
    
    public Id riskId;
    public String packageID;
    
    public Integer qLimit;
    public Integer qDeductible;
    public Integer qDeductiblePercent; //FRZR-86

    public String coverageCd;
    public String coverageDesc;
    public Integer indicator;
    public CurrencyAmt quoteLimit;
    public CurrencyAmt quoteDeductible;
    
    private Boolean showIndicator;
    
    // For reverting a CSIO format to SOQL format.
    public CSIO_CommlCoverage(String coverageCd, Integer qLimit, Integer qDeductible, String riskId, String packageId)
    {
        this.coverageCd = coverageCd;
        this.qLimit = qLimit;
        this.qDeductible = qDeductible;
        this.riskId = riskId;
        this.packageID = packageID;
    }

    // For reverting a CSIO format to SOQL format.
    //FRZR-86 : Starts
    public CSIO_CommlCoverage(String coverageCd, Integer qLimit, Integer qDeductible,Integer qDeductiblePercent, String riskId, String packageId)
    {
        this.coverageCd = coverageCd;
        this.qLimit = qLimit;
        this.qDeductible = qDeductible;
        this.qDeductiblePercent = qDeductiblePercent;
        this.riskId = riskId;
        this.packageID = packageID;
    }
    //FRZR-86 : Ends

    // For converting a SOQL format to CSIO format.
    public CSIO_CommlCoverage(Coverages__c coverage)
    {
        this.coverage = coverage;
        convert();
    }
    
    private void convert()
    {
        Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageCd');
        esbCoverageCd.putAll(SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServicesByVariant('esbCoverageCd', SOQLDataSet.scdv.ePolicy_Package__c));
        Map<String, String> esbCoverageDesc = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCoverageDesc');
        esbCoverageDesc.putAll(SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServicesByVariant('esbCoverageDesc', SOQLDataSet.scdv.ePolicy_Package__c));

        this.coverageCd = esbCoverageCd.get(this.coverage.type__c);
        this.coverageDesc = esbCoverageDesc.get(this.coverage.type__c);

		if(CSIO_CommlCoverage.sendToEpolicy){ // FP-6460
			if(this.coverage.Selected__c)
				this.indicator = 1;
			else
				this.indicator = 0;
		}
		else {
			if (this.coverage.TransactionType__c == 'New Business') {
				if (this.coverage.Limit_Value__c == -1) this.indicator = 0;
				else this.indicator = 1;
			} else {
				if (this.coverage.Selected__c && this.coverage.Limit_Value__c != -1) this.indicator = 1;
				else this.indicator = 0;
			}
		}
        this.quoteLimit = new CurrencyAmt((Double) this.coverage.Limit_Value__c);
        this.quoteDeductible = new CurrencyAmt((Double) this.coverage.Deductible_Value__c);
    }
    
    public Coverages__c revert()
    {
        Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldTypeVariant('esbCoverageCd', null);
        esbCoverageCd.putAll(SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldTypeVariant('esbCoverageCd', CSIOXMLProcessor.scdv.ePolicy_Package__c));
        
        Coverages__c coverage = new Coverages__c();
        coverage.Risk__c = this.riskId;
        coverage.Package_ID__c = this.packageID;
        coverage.Limit_Value__c = this.qLimit;
        coverage.Deductible_Value__c = this.qDeductible;
        coverage.Deductible_Percent__c = this.qDeductiblePercent; //FRZR-86
        coverage.Type__c = esbCoverageCd.get(this.coverageCd); //Fixed?
        if (coverage.Type__c == null)
        {
            // Temporary exception until a proper solution is agreed upon.
          throw new RSA_ESBException();
        }

        return coverage;
    }
    
    public static Coverages__c revert(String riskId, String commlCoverage)
    {
        System.debug('### riskId: ' + riskId + ' commlCoverage: ' + commlCoverage);
        Map<String, String> esbCoverageCd = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbCoverageCd');
        commlCoverage = commlCoverage.replace('<rsa:', '<');
        commlCoverage = commlCoverage.replace('</rsa:', '</');
        Coverages__c c = new Coverages__c();
        c.Risk__c = riskId;
        c.Type__c = esbCoverageCd.get(XMLHelper.retrieveValue(commlCoverage, CSIOXMLProcessor.OPTIONS_COVERAGECD_PATH));
        c.Limit_Value__c = Integer.valueOf(XMLHelper.retrieveValue(commlCoverage, CSIOXMLProcessor.OPTIONS_LIMIT_PATH));
        c.Deductible_Value__c = Integer.valueOf(XMLHelper.retrieveValue(commlCoverage, CSIOXMLProcessor.OPTIONS_DEDUCTIBLE_PATH));
		if (c.Type__c == null)
        {
            // Temporary exception until a proper solution is agreed upon.
          throw new RSA_ESBException();
        }
        c.Limit_Value__c = Integer.valueOf(XMLHelper.retrieveValue(commlCoverage, CSIOXMLProcessor.OPTIONS_LIMIT_PATH));
        c.Deductible_Value__c = Integer.valueOf(XMLHelper.retrieveValue(commlCoverage, CSIOXMLProcessor.OPTIONS_DEDUCTIBLE_PATH));

        return c;
    }
    
    public override String toString()
    {
        String xml = '';
		if(CSIO_CommlCoverage.sendToEpolicy){ // FP-6460
			if(!locationCoverageCodes.contains(this.coverageCd)) {
				xml += '<CommlCoverage>';
				xml += ' <CoverageCd>' + this.coverageCd + '</CoverageCd>';
				xml += ' <CoverageDesc>' + this.coverageDesc + '</CoverageDesc>';
				if (this.quoteLimit.amt != null && this.quoteLimit.amt > 0)
				{
					xml += ' <Limit>';
					xml += this.quoteLimit;
					xml += ' </Limit>';
				}
				if (this.quoteDeductible.amt != null && this.quoteDeductible.amt > 0)
				{
					xml += ' <Deductible>';
					xml += this.quoteDeductible;
					xml += ' </Deductible>';
				}
				xml += ' <rsa:Indicator>' + this.indicator + '</rsa:Indicator>';
				xml += '</CommlCoverage>';
				locationCoverageCodes += this.coverageCd;
			}
		}
		else {
			if (this.quoteLimit.amt != null || this.quoteDeductible.amt != null) // SES-102: Remove coverage if no limit or deductible found.
			{
				xml += '<CommlCoverage>';
				xml += ' <CoverageCd>' + this.coverageCd + '</CoverageCd>';
				xml += ' <CoverageDesc>' + this.coverageDesc + '</CoverageDesc>';
				if (this.quoteLimit.amt != null)
				{
					if (this.quoteLimit.amt > 0)
					{
						xml += ' <Limit>';
						xml += this.quoteLimit;
						xml += ' </Limit>';
					}
				}
				if (this.quoteDeductible.amt != null && this.quoteDeductible.amt > 0)
				{
					xml += ' <Deductible>';
					xml += this.quoteDeductible;
					xml += ' </Deductible>';
				}

				xml += ' <rsa:Indicator>' + this.indicator + '</rsa:Indicator>';
				xml += '</CommlCoverage>';
			}
		}
        return xml;
    }
    
    public class CurrencyAmt
    {
        public Integer amt;
        
        public CurrencyAmt(Decimal amt)
        {
            this.amt = (Integer) amt;
        }
        
        public override String toString()
        {
            String xml = '';
            
            xml += '<FormatCurrencyAmt>';
            xml += ' <Amt>' + this.amt + '</Amt>';
            xml += '</FormatCurrencyAmt>';
            
            return xml;
        }
    }
}