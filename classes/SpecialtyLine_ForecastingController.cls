/**
* @File Name    :   SpecialtyLine_ForecastingController
* @Description  :   Controller Class for Forecasting Tab to generate Forecasting URL
* @Date Created :   03/07/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Controller
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       03/07/2017  Habiba Zaman  	Created the file/class
*/
public with sharing class SpecialtyLine_ForecastingController {
	public SpecialtyLine_ForecastingController() {
		
	}

	public String forecastUrl{
        get{
            String apexPageUrl = System.URL.getSalesforceBaseUrl().toExternalForm()+'/_ui/sales/forecasting/ui/ForecastingTabPage';


            return apexPageUrl;
        }

        private set;
    }
}