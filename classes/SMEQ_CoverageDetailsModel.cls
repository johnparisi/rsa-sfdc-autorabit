global with sharing class SMEQ_CoverageDetailsModel {
    public String id {public get; public set;}
    public String type {public get; public set;}
    public Double limitValue {public get; public set;}
    public Double deductibleValue {public get; public set;}
    public Double annualPremium {public get; public set;}
    public String packageID {public get; public set;}
    public String selected {public get; public set;}
    public String riskId {public get; public set;}
}