/**
  * @author        Saumil Bapat
  * @date          11/4/2016
  * @description   Batch class to delete existing broker stage records
*/
global class SLID_PurgeBrokerStage implements Database.Batchable<sObject>
{

   //Query of logs to delete
   global String query;

   private static final String batchCompleteMessage = 'Batch Process to Delete Broker Stage Data is completed';
   private static final String noErrorEncounteredMessage = 'No Errors encountered in the batch job';

   //Retrieve the broker mapping settings 
   public static SLID_Broker_Mapping_Settings__c brokerMappingSettings = SLID_Broker_Mapping_Settings__c.getInstance();
   
   //Constructor to instantiate the query
   global SLID_PurgeBrokerStage()
   {
      this.query = 'Select Id from Broker_Stage__c';
   }
   //Start method for the batch
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   //Execute method for the batch
   global void execute(Database.BatchableContext BC,List<SObject> scope)
   {
      try
      {
        if(!scope.isEmpty())
        {
          delete scope;
        }
         
      }
      catch (Exception e)
      {
        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_PurgeBrokerStage','execute','',UTIL_Logging.DEBUG_LEVEL_ERROR);
        UTIL_Logging.logException(log);
        throw e;
      }
   }

   //Finish method for the batch
   global void finish(Database.BatchableContext BC)
   {
      //Send the completion email for this batch job
      String adminEmail = SLID_PurgeBrokerStage.brokerMappingSettings.Batch_Job_Admin_Email__c;
      if (adminEmail != null && adminEmail != '')
      {
        sendCompletionEmail(BC);
      }
   }

   private void sendCompletionEmail(Database.BatchableContext bc)
   {
      AsyncApexJob job = [Select Id , Status , NumberOfErrors
                          From AsyncApexJob
                          Where Id =: bc.getJobId()];

      String emailBody = batchCompleteMessage + '<br/>';
      if(job.NumberOfErrors == 0 || job.NumberOfErrors == null)
        emailBody += noErrorEncounteredMessage;
      else if(job.NumberOfErrors > 0)
        emailBody += 'The batch job encountered ' + job.NumberOfErrors + ' errors.';

      //Reserve a email capacity
      Messaging.reserveSingleEmailCapacity(1);
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {SLID_PurgeBrokerStage.brokerMappingSettings.Batch_Job_Admin_Email__c};
      mail.setToAddresses(toAddresses);
      mail.setSenderDisplayName('Salesforce Batch Apex');
      mail.setSubject('Broker Stage Purge Complete');
      mail.setBccSender(false);
      mail.setUseSignature(false);
      mail.setHTMLBody(emailBody);
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }
}