@isTest
private class SMEQ_UnderwriterTakeOverService_Test
{
    /**
    * This method provides test assertions 
    */
    static testMethod void testUnderwriterTakeOver()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/sic/underWriterTakeOver';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            
            Case testCase = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c testQuote = SMEQ_TestDataGenerator.getOpenQuote(testCase);
            
            SMEQ_UnderwriterTakeOverService.SMEQ_UnderWriterTakeOverModel underwriterTakeOverModel = new SMEQ_UnderwriterTakeOverService.SMEQ_UnderWriterTakeOverModel();
            underwriterTakeOverModel.underWriterTakeOver = true;
            underwriterTakeOverModel.quoteID = testQuote.Id;
            SMEQ_UnderwriterTakeOverService.SMEQ_UnderWriterTakeOverResponseModel responseWrapper = SMEQ_UnderwriterTakeOverService.updateQuoteUnderWriterTakeOver(underwriterTakeOverModel);
            System.debug('Response Objcet:' + responseWrapper);
            
            System.assertNotEquals(null, responseWrapper);
            System.assertNotEquals(null, responseWrapper.getResponseCode());
            System.assertEquals('OK', responseWrapper.getResponseCode());
            System.assertEquals('SUCCESS', responseWrapper.status);
        }
    }
}