/**
 * Author: James Lee
 * Created At: November 20, 2017
 * 
 * Unit tests for CSIO_CommlScheduleestablishment.
 */
@isTest
public class CSIO_CommlScheduleEstablishment_Test
{
    static testMethod void testCreditCardType()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Policy_Payment_Plan_Type__c = CSIO_CommlScheduleestablishment.BILLING_METHOD_CREDIT_CARD;
            q.Credit_Card_Expiration_Month__c = '12';
            q.Credit_Card_Expiration_Year__c = '20';
            q.Credit_Card_Type__c = '';
            q.Payment_Frequency__c = '';
            q.Credit_Card_Name__c = '';
            update q;
            
            CSIO_CommlScheduleEstablishment ccse = new CSIO_CommlScheduleEstablishment(q);
            String output = '';
            output += ccse;
            
            Map<String, String> esbCreditCardType = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Credit_Card_Type__c');
            Map<String, String> esbCreditCardPaymentFrequencyCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCreditCardPaymentFrequencyCd');
            
            System.assertEquals(
                '<CommlScheduleEstablishment>' +
                ' <PaymentOption>' +
                '  <MethodPaymentCd>' + esbCreditCardType.get(q.Credit_Card_Type__c) + '</MethodPaymentCd>' +
                '  <ElectronicFundsTransfer>' +
                '   <FromAcct>' +
                '     <AccountNumberId>XXXX</AccountNumberId>' +
                '     <CreditCardExpirationDt>' + '20' + q.Credit_Card_Expiration_Year__c + '-' +
                ((q.Credit_Card_Expiration_Month__c.length() == 1) ? '0' : '') + q.Credit_Card_Expiration_Month__c + '</CreditCardExpirationDt>' +
                '     <CommercialName>' +  q.Credit_Card_Name__c + '</CommercialName>' +
                '     <rsa:CreditCardPaymentFrequencyCd>' + esbCreditCardPaymentFrequencyCd.get(q.Payment_Frequency__c) + '</rsa:CreditCardPaymentFrequencyCd>' +
                '   </FromAcct>' +
                '   <TransferAmt>' +
                '     <Amt>' + 0 + '</Amt>' +
                '   </TransferAmt>' +
                '  </ElectronicFundsTransfer>' +
                ' </PaymentOption>' +
                '</CommlScheduleEstablishment>',
                output
            );
        }
    }
    
    static testMethod void testMonthlyWithdrawalType()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Policy_Payment_Plan_Type__c = CSIO_CommlScheduleestablishment.BILLING_METHOD_MONTHLY_WITHDRAWAL_PLAN;
            q.Bank_Account_Number__c = '10000';
            q.Bank_Transit_ID__c = '123 456';
            q.Downpayment_Amount__c = 1000.0;
            q.Monthly_Withdrawal_Preferred_Day__c = 12;
            update q;
            
            CSIO_CommlScheduleEstablishment ccse = new CSIO_CommlScheduleEstablishment(q);
            String output = '';
            output += ccse;
            
            System.assertEquals(
                '<CommlScheduleEstablishment>' +
                ' <PaymentOption>' +
                '  <DayMonthDue>' + q.Monthly_Withdrawal_Preferred_Day__c.toPlainString() + '</DayMonthDue>' +
                '  <ElectronicFundsTransfer>' +
                '    <FromAcct>' +
                '      <AccountNumberId>' + q.Bank_Account_Number__c + '</AccountNumberId>' +
                '      <BankInfo>' +
                '        <BankId>' + q.Bank_Transit_ID__c.split(' ')[0] + '</BankId>' +
                '      </BankInfo>' +
                '    </FromAcct>' +
                '    <TransferAmt>' +
                '      <Amt>' + q.Downpayment_Amount__c + '</Amt>' +
                '     </TransferAmt>' +
                '  </ElectronicFundsTransfer>' +
                ' </PaymentOption>' +
                '</CommlScheduleEstablishment>',
                output
            );
        }
    }
    
    static testMethod void testDirectBillType()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Policy_Payment_Plan_Type__c = CSIO_CommlScheduleestablishment.BILLING_METHOD_DIRECT_BILL;
            q.Downpayment_Amount__c = 1000.0;
            update q;
            
            CSIO_CommlScheduleEstablishment ccse = new CSIO_CommlScheduleEstablishment(q);
            String output = '';
            output += ccse;
            
            System.assertEquals(
                '<CommlScheduleEstablishment>' +
                ' <PaymentOption>' +
                '  <PaymentIntervalCd>csio:1</PaymentIntervalCd>' +
                '  <ElectronicFundsTransfer>' +
                '   <TransferAmt>' +
                '    <Amt>' + q.Downpayment_Amount__c + '</Amt>' +
                '   </TransferAmt>' +
                '  </ElectronicFundsTransfer>' +
                ' </PaymentOption>' +
                '</CommlScheduleEstablishment>',
                output
            );
        }
    }
    
    static testMethod void testAgencyBillType()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.Policy_Payment_Plan_Type__c = CSIO_CommlScheduleestablishment.BILLING_METHOD_AGENCY_BILL;
            update q;
            
            CSIO_CommlScheduleEstablishment ccse = new CSIO_CommlScheduleEstablishment(q);
            String output = '';
            output += ccse;
            
            System.assertEquals(
                '',
                output
            );
        }
    }
}