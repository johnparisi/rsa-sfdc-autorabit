/**
Wrapper class for local integration / unit testing with Node client. 
Will be replaced with 'real' RemoteActions when served from Salesforce
*/

@RestResource(urlMapping='/getQuoteByID/*')
global without sharing class FP_QuoteByIDRestWrapperService {
        /**
     Class used to communicate success / failure state to the Angular client
     */
     
  /* @TestVisible  
     global class OutcomeResponse {
       public boolean success;
       public List<String> errorMessages;
     }
   */   
   
   
    @HttpPut
    global static QuoteRequestWrapper getQuoteByID(string quoteId) {
        System.debug('entering QuoteRequestWrapper, quoteId is' + quoteId);
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        //FP_AngularPageController.OutcomeResponse outcomeResponse = new 
        QuoteRequestWrapper outcomeResponse = null;
        
        try {
            //outcomeResponse = FP_AngularPageController.saveQuoteState(quote, risks, quoteRiskRelationship, contacts, accounts);
            //outcomeResponse.success = 
            System.debug('about to releaes otucomeResopnse');
            outcomeResponse = FP_AngularPageController.getQuoteById(quoteId, null);
            System.debug('### dumping QuoteRequestWrapper' + outcomeResponse);
            

        }
        catch (Exception e) {
              System.debug('caught an error yo!');
              // outcomeResponse = '{null}';
            //outcomeResponse.errorMessages.add(e.getMessage());

        }
        return outcomeResponse;
    }   
    
 
    
}