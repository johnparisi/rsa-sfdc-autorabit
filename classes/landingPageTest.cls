/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class landingPageTest {
    
     /**
    *This function tests LandingPageController, to make sure the controller's functions are working
    *Assertions:
    *1. Tests if the page header is properly set, true if it was set, false setting failed.
  */
    public static testmethod void doTest () {
        String underwriterProfile = 'RSA Standard User [CI, Policy-Level, No Case Queue]';
        UserRole nonPortalRole = [
            Select Id
            FROM UserRole
            Where PortalType = 'None'
            Limit 1
        ];
        System.debug('UserRole is ' + nonPortalRole);
        
        Profile uwProfile = [
            SELECT Id
            FROM Profile
            WHERE name = :underwriterProfile
        ];
        User uwUser = new User(
            UserRoleId = nonPortalRole.Id,
            ProfileId = uwProfile.Id,
            Username = Math.random() + 'uw@rsatest.com',
            Alias = 'uw',
            Email = System.now().millisecond() + 'uw@rsatest.com',
            EmailEncodingKey = 'UTF-8',
            Firstname = 'uw',
            Lastname = 'test',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Chicago',
          /*  Offering_Project__c = offerings,
            Primary_Region__c = regions.substringBefore(';'),
            Secondary_Regions__c = regions.substringAfter(';'),*/
            uwUser_Level__c = UserService_Test.UW_LEVEL3,
            Primary_Region__c = UserService_Test.REGION_1,
            Offering_Project__c = UserService_Test.OFFERING_1.substringBefore(' ')
           // uwUser_Level__c = uwLevel
        );
        insert uwUser;
        system.runAs(uwUser){
        LandingPageController controller= new LandingPageController();
        controller.getFaviconUrl();
        controller.getVariant();
        controller.navigateToNextPage();
        controller.getRegion();
        controller.toggleLanguage();
        
        System.assertEquals('IE=edge',Apexpages.currentPage().getHeaders().get('X-UA-Compatible'));
        }
    }
    
    
    @testSetup static void setup() {

        // landing page data   
        Landing_Page_Component__c landingPageComponent1 = new Landing_Page_Component__c(); 
        landingPageComponent1.Offering_Project__c = 'HUB'; 
        landingPageComponent1.icon__c = 'img';
        landingPageComponent1.Instructions__c = 'START';
        landingPageComponent1.Position__c = 1;
        insert landingPageComponent1;
        
        Landing_Page_Component__c landingPageComponent2 = new Landing_Page_Component__c(); 
        landingPageComponent2.Offering_Project__c = 'SPRNT v1'; 
        landingPageComponent2.icon__c = 'img';
        landingPageComponent2.Instructions__c = 'MANAGE';
        landingPageComponent2.Position__c = 2;
        insert landingPageComponent2;
        
        User testUser;
        System.runAs(SMEQ_TestDataGenerator.getCurrentRunningUserWithRole()){
            UserService_Test.setupTestData();
            testUser= UserService_Test.getPortalUser(UserService_Test.OFFERING_1,UserService_Test.REGION_1);
        }
    }
  /*   @isTest    
    public static void doTestRetrieveComponents(){
       
        // call retrieve components
       	LandingPageController controller= new LandingPageController();
       
		List<Landing_Page_Component__c> incomingRecords = null;
        incomingRecords =  controller.incomingRecords;
        system.debug(incomingRecords);
        for (Landing_Page_Component__c record : incomingRecords){
            system.assert(record.Offering_Project__c == UserService.getOffering(UserInfo.getUserId())[0]);
        }
        system.assert(incomingRecords.size() == 1);
    }*/


}