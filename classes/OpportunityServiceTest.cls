/**
* @File Name    :   OpportunityServiceTest
* @Description  :   Test class for OpportunityService
* @Date Created :   01/20/2016
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   testing class
* @Modification Log:
*****************************************************************
* Ver       Date        Author              Modification
* 1.0       01/20/2016  Habiba Zaman        Created the file/class
**/
@isTest
private class OpportunityServiceTest {
    
@isTest
    static void TestCreateRenewalOpportunities()
    {
         // Create a Sample Client Account
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;

        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testu1', Email='testuser1@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = userProfile.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg.com');

        insert u;
        Map<id,id> PlcyMap= new Map<id,id>();
        Policy__c policy = new Policy__c();
        policy.Name ='0000001123';
        policy.Client_Name__c = clientAcc.Name;
        policy.Client_Account__c = clientAcc.Id;
        policy.Master_Broker_Account__c = brokerageAcc.Id;
        policy.Strategic_Segment__c = 'Contracting';
        policy.Region_Group__c = 'Atlantic';
        policy.Term_Expiry_Date__c = Date.today();
        policy.Term_Expiry_Date__c = Date.today();
        policy.Policy_Stage__c = 'Renewal Cycle Initialized';
        policy.bkrPolicy_Premium__c = 123.23;
        policy.Product__c = 'Property/Casualty';
        policy.OwnerId = u.Id;

        Test.startTest();
            List<Policy__c> pList = new List<Policy__c>();
            pList.add(policy);
            insert pList;
            PlcyMap.put( policy.Id, policy.OwnerId);
            OpportunityService oppService = new OpportunityService();
            oppService.createRenewalOpportunities(pList);
            oppService.updatePolicyOwner(PlcyMap);
        Test.stopTest();
        System.assertEquals(policy.Policy_Stage__c, 'Renewal Opportunity Created');
    }

   @isTest
    static void TestCreateOppTeamMember()
    {
        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testo1', Email='testo1@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = userProfile.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg9.com');

        insert u;

        User uKam = new User(Alias='testKam', Email='testuser1Kam@testorg.com',
                EmailEncodingKey='UTF-8', LastName='TestingKam', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = userProfile.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='Kamtestuser1g4egas4qgj6kl8r@testorg.com');

        insert uKam;
        System.runAs(uKam){
            //Product
            Product2 product = new Product2();
            product.Name ='Property & Casualty GSL Test';
            product.IsActive= true;
            product.Family='Mid-Market & Global Specialty Lines';
            insert product;

            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id pricebookId = Test.getStandardPricebookId();

            // 1. Insert a price book entry for the standard price book.
            // Standard price book entries require the standard price book ID we got earlier.
            PricebookEntry standardPrice = new PricebookEntry(
                    Pricebook2Id = pricebookId, Product2Id = product.Id,
                    UnitPrice = 10000, IsActive = true);
            insert standardPrice;

            // Create a custom price book
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;

            // 2. Insert a price book entry with a custom price.
            PricebookEntry customPrice = new PricebookEntry(
                    Pricebook2Id = customPB.Id, Product2Id = product.Id,
                    UnitPrice = 12000, IsActive = true);
            insert customPrice;

            // Opportunity.
            OpportunityService service = new OpportunityService();
            List<Opportunity> oppList = new List<Opportunity>();
            Map<id,Opportunity> oldmap= new Map<id,Opportunity>();
            Opportunity opp = new Opportunity();
            opp.bkrProductLineEffectiveDate__c = Date.today().addDays(12);
            opp.bkrProduct__c = product.Id;
            opp.bkrMMGSL_KAM__c = uKam.Id;
            opp.bkrMMGSL_UnderwritingManager__c = u.Id;
            opp.RecordTypeId = service.getRenewalOpportunityRecordType();
            opp.Name = 'Test opp line item';
            opp.StageName = 'Renewal Assigned';
            opp.Probability = 10.0;
            opp.CloseDate = Date.today().addDays(12);
            oppList.add(opp);

            Test.startTest();
                insert oppList;
                oldmap.put(opp.id,opp);
                update oppList;
                service.updateNewBusinessOpportunityAfterUpdate(oppList,oldmap);
                service.updateRenewalOpportunityBeforeUpdate(oppList,oldmap);
                service.getPriceBookEntry();
                service.updateOpportunityTeamMembers(oppList,oldmap);
                service.getCompletedTaskCountForOpportunity(oppList);
                service.validateSpecialtyLineStageChange(oppList,oldmap);
                service.createNewBusinessPolicy(oppList);
                List<OpportunityTeamMember> otm = [Select UserId from OpportunityTeamMember where  OpportunityId =:opp.Id];
                System.assertEquals(otm.size(), 2);
            Test.stopTest();
        }
    }
}