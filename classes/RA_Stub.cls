// A stub class for generation of dummy data to test R&A functionality prior to getting services up and running
// Will be deprecated when services are fully stood up
public class RA_Stub {

    public static Policy_History__c ph1;
    public static Policy_History__c ph2;
    public static Policy_History__c ph3;
    
    // create 3 publically accessible policy histories
    public void initializePolicyHistories(String quoteId){

        ph1 = new Policy_History__c();
        ph1.Change_Description__c = 'New party';
        ph1.Change_Name__c = 'Conor McGurk';
        ph1.Federation_Id__c = 'abcd1';
        ph1.New_Premium__c = 1000;
        ph1.Old_Premium__c = 500;
        ph1.Related_Quote__c = quoteId;
        
        ph2 = new Policy_History__c();
        ph2.Change_Description__c = 'Some kind of change';
        ph2.Change_Name__c = 'Fred George';
        ph2.Federation_Id__c = 'abcd2';
        ph2.New_Premium__c = 1040;
        ph2.Old_Premium__c = 100;
        ph2.Related_Quote__c = quoteId;
        
        ph3 = new Policy_History__c();
        ph3.Change_Description__c = 'Some OTHER kind of change';
        ph3.Change_Name__c = 'Bob McGurk';
        ph3.Federation_Id__c = 'abcd3';
        ph3.New_Premium__c = 2000;
        ph3.Old_Premium__c = 100;
        ph3.Related_Quote__c = quoteId;
        
        insert ph1;
        insert ph2;
        insert ph3;
       
    }

}