/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_MoveAttachmentFlow {

    static testMethod void TestInvokableCopyEmailMessageAndAttachments() {
        
        /************** Inserting Cases *************/
        Case objCase1 = new Case();
        insert objCase1;
        Case objCase2 = new Case();
        insert objCase2;
 		
 		system.assert(objCase1.Id != NULL);
 		system.assert(objCase2.Id != NULL);
 		
 		/************** Inserting EmailMessage *************/
 			EmailMessage em = new EmailMessage();
 			   em.ToAddress = 'uw@rsagroup.ca';
 			   em.FromAddress = 'uw@rsagroup.ca';
               em.Subject = 'test subject';
               em.TextBody = 'test subject';
               em.ParentId = objCase1.id;
               em.Incoming = true;
               insert em;
                		
 		/************** Inserting Attachment *************/
        Attachment ObjAttachment=new Attachment();   	
    	ObjAttachment.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	ObjAttachment.body=bodyBlob;
        ObjAttachment.parentId=em.id;
        insert ObjAttachment;
    	
    	system.assert(ObjAttachment.Id != NULL);

        Test.startTest();
        
            List<InvokableCopyEmailMessageAndAttachments.CopyEmailRequest> lstReq = new List<InvokableCopyEmailMessageAndAttachments.CopyEmailRequest>();
        	InvokableCopyEmailMessageAndAttachments.CopyEmailRequest req = new InvokableCopyEmailMessageAndAttachments.CopyEmailRequest();
            req.oldParentId = String.valueOf(objCase1.Id);
            req.newParentId = String.valueOf(objCase2.Id);
            req.copyOnlyLatest = true;
			lstReq.add(req);
        
			InvokableCopyEmailMessageAndAttachments.InvokableCopyEmailMessageAndAttachments(lstReq);
        
		Test.stopTest();
        
		List<EmailMessage> lstAttachment2 = new List<EmailMessage>([select Id, HasAttachment from EmailMessage where ParentId =: objCase2.Id]);
		
		system.assert(lstAttachment2.size() == 1);
		system.assert(lstAttachment2[0].HasAttachment == true);
		
    }

    static testMethod void TestTaskAndAttachmentHandler() {

        /************** Inserting Cases *************/
        Case objCase1 = new Case();
        insert objCase1;
        Case objCase2 = new Case();
        insert objCase2;
 		
 		system.assert(objCase1.Id != NULL);
 		system.assert(objCase2.Id != NULL); 	

        Test.startTest();
        
            /************** Inserting EmailMessage *************/
                Task t = new Task();
                   t.Subject = 'test subject';
                   t.Whatid = objCase1.Id;
                   t.Status = 'Completed';
                   t.Type = 'Email';
                   insert t;
                            
            /************** Inserting Attachment *************/
            Attachment ObjAttachment=new Attachment();   	
            ObjAttachment.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            ObjAttachment.body=bodyBlob;
            ObjAttachment.parentId=t.id;
            insert ObjAttachment;
            
            system.assert(ObjAttachment.Id != NULL);
        
		Test.stopTest();
        
		List<EmailMessage> lstAttachment2 = new List<EmailMessage>([select Id, HasAttachment from EmailMessage where ParentId =: objCase1.Id]);
		
		system.assert(lstAttachment2.size() == 1);
		system.assert(lstAttachment2[0].HasAttachment == true);
		
    }   

    static testMethod void TestQuoteVersion() {

        /************** Inserting Cases *************/
        Case objCase1 = new Case();
        insert objCase1;
        Case objCase2 = new Case();
        insert objCase2;
 		
 		system.assert(objCase1.Id != NULL);
 		system.assert(objCase2.Id != NULL); 	

        Test.startTest();
        
            /************** Inserting QuoteVersion *************/
                Quote_Version__c qv = new Quote_Version__c();
                   qv.Case__c = objCase1.Id;
                   qv.Primary__c = true;
                   insert qv;

                Quote_Version__c qv1 = new Quote_Version__c();
                   qv1.Case__c = objCase1.Id;
                   qv1.Primary__c = true;
                   insert qv1;        
        
		Test.stopTest();

		system.assert([select Id, Primary__c from Quote_Version__c where Id =: qv.Id].Primary__c == false);
		system.assert([select Id, Primary__c from Quote_Version__c where Id =: qv1.id].Primary__c == true);
    }    
}