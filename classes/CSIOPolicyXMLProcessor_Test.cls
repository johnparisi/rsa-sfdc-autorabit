/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CSIOPolicyXMLProcessor_Test extends RA_AbstractTransactionTest {

	@testSetup static void testSetup() {
		RA_AbstractTransactionTest.setup();
	}

    /**
    Test the entire first XML document
    */

    static testMethod void testXmlOne() {
        Case parentCase = new Case();
       	parentCase = [
            SELECT id, Account.Id, Contact.Id, bkrCase_Insured_Client__c, CreatedDate, OwnerId
            FROM Case
            LIMIT 1
        ];

        // process the XML policy document
        CSIOPolicyXMLProcessor csioXMLProcessor = new CSIOPolicyXMLProcessor(parentCase, RA_AbstractTransactionTest.xmlPolicy);
        csioXMLProcessor.process();

        // test that we can find the document in the database
        Quote__c policy = [
            SELECT Id, Air_Miles_Loyalty_Points_Standard__c, Total_Revenue__c, Standard_Premium__c, Claim_Free_Years__c
            FROM Quote__c
            WHERE ePolicy__c = 'COM810101133'
            LIMIT 1
        ];
        System.assertNotEquals(null, policy, 'Policy not found');

        Account insuredAccount = [
            SELECT Id, Name
            FROM Account
            WHERE Account.Id = :parentCase.bkrCase_Insured_Client__c
        ];

        List<Claim__c> claimList = [
            SELECT Name, Amount__c
            FROM Claim__c
            WHERE Account__c = :insuredAccount.Id
        ];

        List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = [
            SELECT Id, Account__c, Contact__c, Party_Type__c, Relationship_Type__c, Account__r.Name, Account__r.BillingStreet,
            Account__r.Billing_Address_Line2__c, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingPostalCode,
            Account__r.BillingCountry
            FROM Quote_Risk_Relationship__c
            WHERE Quote__c = :policy.Id
        ];
        // two additional insureds, plus eight additional interests
        system.assertEquals(9, quoteRiskRelationshipList.size(), 'There should be 9 QRRs related to the quote');

        List<Risk__c> riskList = [
            SELECT Name, Address_Line_1__c, RecordType.developername
            FROM Risk__c
            WHERE Quote__c = :policy.Id
        ];
        system.assertEquals(4, riskList.size(), 'There should be 4 risks on this policy');

        Integer locationCount = 0;
        Integer liabilityCount = 0;
        for (Risk__c locationOrLiability: riskList) {
            if (locationOrLiability.RecordType.developername == RiskService.RISK_RECORDTYPE_LOCATION) {
                locationCount++;
            }
            else {
                liabilityCount++;
            }
        }
        system.assertEquals(3, locationCount, 'There should be three risk Locations');

        system.assertEquals(1, liabilityCount, 'There should only be one liability on this policy');

		List<Coverages__c> coverageList = [
            SELECT Id, Type__c, Limit_Value__c, Risk__c, Deductible_Value__c
            FROM Coverages__c
        ];
		for (Coverages__c coverage : coverageList) {
			System.debug('#### coverage: ' + coverage);
		}
        // first check the number of coverages
        System.assertEquals(4, coverageList.size(), 'Expecting 4 coverages');

        // pick a few of them and check if they fit
        Boolean buildingFound = false;
        Boolean stockFound = false;
        Boolean cglFound = false;
        for (Coverages__c coverage : coverageList) {
            if (coverage.Type__c == RiskService.COVERAGE_BUILDING) {
                buildingFound = true;
                System.assertEquals(1500000, coverage.Limit_Value__c, 'Building coverage limit value should be 1500000');
                System.assertEquals(1000, coverage.Deductible_Value__c, 'Deductible value should be 1000');
            }
            if (coverage.Type__c == RiskService.COVERAGE_STOCK) {
                stockFound = true;
                System.assertEquals(500000, coverage.Limit_Value__c, 'Stock coverage limit value should be 500000');
            }
            if (coverage.Type__c == RiskService.COVERAGE_CGL) {
            	if (cglFound) {
            		System.assert(false, 'CGL should only be found once');
            	}
            	cglFound = true;
            }
        }

        System.assert(buildingFound, 'Should have found the building coverage');
        System.assert(!stockFound, 'Should have not found the stock coverage');
        System.assert(cglFound, 'CGL should have been found');

        // check that we have a session
        System.assertNotEquals(null, RA_SessionManagementUtil.getSession(parentCase.Id), 'Should have a session record');

        List<Session_Information__c> sessionList = [
            SELECT id, Case__c, Session_Id__c, User__c
            FROM Session_Information__c
            WHERE Case__c = :parentCase.Id
            AND User__c = :UserInfo.getUserID()
        ];
        System.assertEquals(1, sessionList.size(), 'Should be a session record');
    }

    static testMethod void testXmlOneCSIOPolicyCommlPolicyXMLProcessor() {
        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(RA_AbstractTransactionTest.xmlPolicy);
        CSIOPolicyCommlPolicyXMLProcessor processor = new CSIOPolicyCommlPolicyXMLProcessor();
        Dom.XMLNode soapNode = xmlDocument.getRootElement().getChildElement('Body', CSIOPolicyXMLProcessor.SOAP_NAMESPACE);
        Dom.XMLNode acordNode = soapNode.getChildElement('ACORD', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode insuranceSvcRsNode = acordNode.getChildElement('InsuranceSvcRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode policySyncRsNode = insuranceSvcRsNode.getChildElement('PolicySyncRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlPkgPolicyQuoteInqRs = policySyncRsNode.getChildElement('CommlPkgPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlPolicyNode = commlPkgPolicyQuoteInqRs.getChildElement('CommlPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Case testCaseOne = new Case();
        processor.process(commlPolicyNode, testCaseOne);
        Quote__c policy = processor.policy;
        // check that the policy exists and attributes
        // Id, Air Miles, Segment, Flavour, Annual Sales, Current Term Amount, Claims Free Years
        // Id
        System.assertNotEquals(null, policy, 'Policy not found');
        // Air Miles
        System.assertEquals(180, policy.Air_Miles_Loyalty_Points_Standard__c, 'Air miles should be set to 180');
        // Annual Sales
        // System.assertEquals(100000, policy.Total_Revenue__c, 'Annual sales should be 100000');
        // Current term amount
        //System.assertEquals(3964, policy.Standard_Premium__c, 'Current term amount should be 3964');
        // TBC : Segment, Flavour
        System.assertEquals(0, policy.Claim_Free_Years__c, 'Claims free years should be 0');

        // check that claims associated with the policy are there
        List<Claim__c> claimList = processor.claimList;
        System.assertEquals(0, claimList.size(), 'Should be no claims associated with the policy');
        //System.assert(claimList[0].Amount__c == 10000, 'Claim amount should be 10000, is actually ' + claimList[0].Amount__c);
        //System.assert(claimList[0].Month__c == '2', 'Claim month should be 2, is actually ' + claimList[0].Month__c);
        //System.assert(claimList[0].Year__c == 2016, 'Claim year should be 2016, is actually ' + claimList[0].Year__c);
        //System.assert(claimList[0].Account__r.Id == policy.Case__r.bkrCase_Insured_Client__r.Id, 'Insured client should be set');

        // check that the additional interests (3) are there
        //List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = processor.qrrList;
        //System.assert(quoteRiskRelationshipList.size() == 1, 'Should be one party related at the quote level');

        // recent transactions - should be one
        List<Policy_History__c> policyHistory = processor.PolicyHistoryList;
        System.assertEquals(3, policyHistory.size(), 'Should be 3 policy history records');
        //Policy_History__c historyRecord = policyHistory[0];
        //System.assert(historyRecord.Change_Date__c.year() == 2017);

        Integer foundCount = 0;

        System.debug('#### qrr list size ' + processor.accountQrrMap.keySet().size());
        for (Quote_Risk_Relationship__C quoteRiskRelationship : processor.accountQrrMap.keySet()) {
            System.debug('#### party type is: ' + quoteRiskRelationship.Party_Type__c);
            if (quoteRiskRelationship.Party_Type__c == RelationshipControllerExtension.PREMIER_FINANCIER) {
                foundCount++;
            }
            else if (quoteRiskRelationship.Party_Type__c == RelationshipControllerExtension.ADDITIONAL_INSURED) {
                //System.assert(quoteRiskRelationship.Relationship_Type__c == 'A Division Of', 'Additional insured relationship type should be "A Division Of", was ' + quoteRiskRelationship.Relationship_Type__c);
                foundCount++;
            }
        }

        System.assertEquals(1, foundCount, 'Should have found one QRRs');

        foundCount = 0;

        for (Account account : processor.accountQrrMap.values()) {
            /*if (account.Name == 'this company') {
                System.assert(account.Name == 'this company', 'Other client bill name should be "this company"');
                System.assert(account.BillingStreet == '388 Yonge Street Apt 7016', 'Other client bill billing address should be "388 Yonge Street Apt 7016", is ' +account.BillingStreet);
                System.assert(account.BillingPostalCode == 'M4N 4L4', 'Other client bill postal code should be "M4N 4L4"');
                foundCount++;
            }*/
            if (account.Name == 'High Finance Company') {
                System.assertEquals('High Finance Company', account.Name, 'Premier financier name should be "High Finance Company"');
                System.assertEquals('Etobicoke', account.BillingCity, 'Premier Financier billing city should be "Etobicoke"');
                System.assertEquals('Ontario', account.BillingState, 'Premier Financier billing state should be "ON"');
                //System.assert('Canada', account.BillingCountry, 'Premier Financier billing country should be "CA"');
                foundCount++;
            }
            /*else if (account.Name == 'another company') {
                System.assert(account.Name == 'another company', 'Additional insured name should be "another company"');
                foundCount++;
            }*/
        }
        System.assertEquals(1, foundCount, 'Should have found three accounts');
    }

}