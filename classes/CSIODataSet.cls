public class CSIODataSet
{        
    private RequestInfo ri;
    private SOQLDataSet sds;
    public CSIO_Producer producer;
    public String caseCreateDateLong;
    public String caseCreateDateShort;
    public String language;
    public String quoteNumber;
    private String comNumber;
    private String rsaOperation;
    private String rsaContextId;
    private String offeringProject;
    private String federationId;
    private Map<String, String> esbContext;
    
    public CSIODataSet(SOQLDataSet soqlDataSet, RequestInfo ri)
    {
        this.sds = soqlDataSet;
        this.ri = ri;
        rsaOperation = ri.getServiceType();
        this.producer = new CSIO_Producer(sds.brokerage, sds.producer, sds.getAgencyId(), sds.caseId);
        esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');    
        rsaContextId = esbContext.get(sds.smeCase.Offering_Project__c);
    }
    //Creating this new constructor for GETPOLICY from broker tool where sds does not exist yet since no case/quote exists in SF until after service callout  
    public CSIODataSet(String comNumber, RequestInfo ri, String offeringProject, String federationId)
    {
        this.ri = ri; 
        this.comNumber = comNumber;   
        this.federationId = federationId; 
        this.offeringProject = offeringProject;
    }
    
    public String getCSIOXMLFormat()
    {
        //Setting condition for GETPOLICY from broker tool
        if (ri.svcType != RequestInfo.ServiceType.GET_POLICY &&
           ri.svcType != RequestInfo.ServiceType.LOAD_POLICY &&
           ri.svcType != RequestInfo.ServiceType.LOAD_POLICY_X &&
           ri.svcType != RequestInfo.ServiceType.ABORT_TRANSACTION &&
           ri.svcType != RequestInfo.ServiceType.RABIND_QUOTE && 
           ri.svcType != RequestInfo.ServiceType.FINALIZE_QUOTE && 
           ri.svcType != RequestInfo.ServiceType.RAFINALIZE_QUOTE &&           
           ri.svcType != RequestInfo.ServiceType.DOWNLOAD_QUOTE &&
           ri.svcType != RequestInfo.ServiceType.DOWNLOAD_BINDER &&
           ri.svcType != RequestInfo.ServiceType.UPLOAD_QUOTE &&
           ri.svcType != RequestInfo.ServiceType.GET_3_QUOTE){
               
                String dateFormatShort = 'YYYY-MM-dd';
                String dateFormatLong = 'yyyy-MM-dd\'T\'hh:mm:ss\'Z\'';
                String todayDateShort = DateTime.now().format(dateFormatShort);
                String todayDateLong = DateTime.now().format(dateFormatLong);
               
                caseCreateDateLong = sds.smeCase.CreatedDate.format(dateFormatLong);
                caseCreateDateShort = sds.smeCase.CreatedDate.format(dateFormatShort);
                quoteNumber = sds.quote.Quote_Number__c == null ? '' : sds.quote.Quote_Number__c; //Fix this
                language = sds.quote.Language__c != null ? sds.quote.Language__c : UserService.getFormattedUserLanguage(); 
        }
        String xml = '';

        //adding this for GETPOLICY, calling the factory classes to generate the request 
        if (ri.svcType == RequestInfo.ServiceType.GET_POLICY){
            // The quote which contains the business source ID will not be created at this point. Default to user's profile.
            String businessSource = UserService.getUser(UserInfo.getUserId()).Profile.Name == UserService.PROFILE_BROKER ? 'BDIG' : 'UDIG';
            CSIOGetPolicyRequestFactory getPolicyDocumentFactory = new CSIOGetPolicyRequestFactory(comNumber, businessSource, ri, offeringProject, federationId);
            xml = getPolicyDocumentFactory.buildXMLRequest();
        }
        else if (ri.svcType == RequestInfo.ServiceType.LOAD_POLICY || ri.svcType == RequestInfo.ServiceType.LOAD_POLICY_X){
            CSIOGetPolicyRequestFactory getPolicyDocumentFactory = new CSIOGetPolicyRequestFactory(sds, ri);
            xml = getPolicyDocumentFactory.buildXMLRequest();            
        }

        else if (ri.svcType == RequestInfo.ServiceType.GET_QUOTE || ri.svcType == RequestInfo.ServiceType.RAGET_QUOTE){
            system.debug('getting quote');
            CSIOGetQuoteRequestFactory getQuoteDocumentFactory = new CSIOGetQuoteRequestFactory(sds, ri);
            xml = getQuoteDocumentFactory.buildXMLRequest();
        }
        else if (ri.svcType == RequestInfo.ServiceType.FINALIZE_QUOTE || ri.svcType == RequestInfo.ServiceType.RAFINALIZE_QUOTE){
            CSIOFinalizeQuoteRequestFactory finalizeRequestFactory = new CSIOFinalizeQuoteRequestFactory(sds, ri);
            xml = finalizeRequestFactory.buildXMLRequest();
        }
        else if (ri.svcType == RequestInfo.ServiceType.BIND_QUOTE || ri.svcType == RequestInfo.ServiceType.RABIND_QUOTE){
            CSIOBindQuoteRequestFactory bindQuoteRequestFactory = new CSIOBindQuoteRequestFactory(this.sds, ri);
            xml = bindQuoteRequestFactory.buildXMLRequest();
        }
        else if (ri.svcType == RequestInfo.ServiceType.ABORT_TRANSACTION) {
        	CSIOAbortRequestFactory abortQuoteRequestFactory = new CSIOAbortRequestFactory(sds, ri);
        	xml = abortQuoteRequestFactory.buildXMLRequest();
        }
        else if (ri.svcType == RequestInfo.ServiceType.SAVE_QUOTE){
            CSIOSaveQuoteRequestFactory saveQuoteRequestFactory = new CSIOSaveQuoteRequestFactory(sds, ri);
            xml = saveQuoteRequestFactory.buildXMLRequest();
        } 
        else if (ri.svcType == RequestInfo.ServiceType.GET_3_QUOTE){
            CSIOGet3QuoteRequestFactory get3QuoteRequestFactory = new CSIOGet3QuoteRequestFactory(sds, ri);
            xml = get3QuoteRequestFactory.buildXMLRequest();
        } 
         else if (ri.svcType == RequestInfo.ServiceType.UPLOAD_QUOTE){
            CSIOUploadQuoteRequestFactory uploadQuoteRequestFactory = new CSIOUploadQuoteRequestFactory(sds, ri);
            xml = uploadQuoteRequestFactory.buildXMLRequest();
        }      
        else if (ri.svcType == RequestInfo.ServiceType.DOWNLOAD_QUOTE){
            CSIODownloadQuoteRequestFactory downloadQuoteRequestFactory = new CSIODownloadQuoteRequestFactory(sds, ri);
            xml = downloadQuoteRequestFactory.buildXMLRequest();
        }
		else if (ri.svcType == RequestInfo.ServiceType.DOWNLOAD_BINDER){
            CSIODownloadBinderRequestFactory downloadBinderRequestFactory = new CSIODownloadBinderRequestFactory(sds, ri);
            xml = downloadBinderRequestFactory.buildXMLRequest();
        }
        return xml;
    }
}