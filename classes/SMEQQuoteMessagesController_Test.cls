/**
 * Author: Suman Haldar
 * Created At: July 30, 2018
 * 
 * Unit tests for SMEQQuoteMessagesController.
 */
@isTest
public class SMEQQuoteMessagesController_Test{
    
    static testMethod void testEnglishUserFloodZone4(){
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL2, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        Test.startTest();
        System.runAs(uw){
            Account ins = SMEQ_TestDataGenerator.getInsuredAccount();
            Claim__c c = SMEQ_TestDataGenerator.getClaim(ins, 1000, '5',2017);
            c.Type_of_Claim__c = 'Flood';
            update c;
            
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase(sicCode);
            cse.bkrCase_Insured_Client__c = ins.Id;
            update cse;
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(cse);
            
            List<Risk__c> listRisks = new List<Risk__c>();
            Risk__c r1 = SMEQ_TestDataGenerator.getLocationRiskForPolicy(q);
            r1.Flood_Zone__c = 4;
            listRisks.add(r1);
            
            Risk__c r2 = SMEQ_TestDataGenerator.getLocationRiskForPolicy(q);
            r2.Flood_Zone__c = 4;
            listRisks.add(r2);
            update listRisks;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            SMEQQuoteMessagesController smeqQuoteMsgObj = new SMEQQuoteMessagesController(sc);
        }
        Test.stopTest();
    }
    
    static testMethod void testFrenchUserFloodZone4(){
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL2, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        uw.LanguageLocaleKey = 'fr';
        update uw;
        
        Test.startTest();
        System.runAs(uw){
            Account ins = SMEQ_TestDataGenerator.getInsuredAccount();
            Claim__c c = SMEQ_TestDataGenerator.getClaim(ins, 1000, '5',2017);
            c.Type_of_Claim__c = 'Flood';
            update c;
            
            SIC_Code__c sicCode = SMEQ_TestDataGenerator.getSicCode();
            Case cse = SMEQ_TestDataGenerator.getOpenSprntCase(sicCode);
            cse.bkrCase_Insured_Client__c = ins.Id;
            update cse;
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(cse);
            Risk__c r = SMEQ_TestDataGenerator.getLocationRiskForPolicy(q);
            r.Flood_Zone__c = 4;
            update r;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            SMEQQuoteMessagesController smeqQuoteMsgObj = new SMEQQuoteMessagesController(sc);
        }
        Test.stopTest();
    }

    static testMethod void testEnglishUserFloodZone5(){
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        Test.startTest();
        System.runAs(uw){
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            
            List<Risk__c> listRisks = new List<Risk__c>();
            Risk__c r1 = SMEQ_TestDataGenerator.getLocationRiskForPolicy(q);
            r1.Flood_Zone__c = 5;
            listRisks.add(r1);
            
            Risk__c r2 = SMEQ_TestDataGenerator.getLocationRiskForPolicy(q);
            r2.Flood_Zone__c = 5;
            listRisks.add(r2);
            update listRisks;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            SMEQQuoteMessagesController smeqQuoteMsgObj = new SMEQQuoteMessagesController(sc);
        }
        Test.stopTest();
    }
    
    static testMethod void testFrenchUserFloodZone5(){
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        uw.LanguageLocaleKey = 'fr';
        update uw;
        
        Test.startTest();
        System.runAs(uw){
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRiskForPolicy(q);
            r.Flood_Zone__c = 5;
            update r;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            SMEQQuoteMessagesController smeqQuoteMsgObj = new SMEQQuoteMessagesController(sc);
        }
        Test.stopTest();
    }
    
    static testMethod void testEnglishUserReferralOrError(){
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        
        Test.startTest();
        System.runAs(uw){
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.ePolicy_Message_English__c = generateEnglishEPolicyMessageNodes();
            update q;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            SMEQQuoteMessagesController smeqQuoteMsgObj = new SMEQQuoteMessagesController(sc);
        }
        Test.stopTest();
    }
    
    static testMethod void testFrenchUserReferralOrError(){
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        uw.LanguageLocaleKey = 'fr';
        update uw;
        
        Test.startTest();
        System.runAs(uw){
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            q.ePolicy_Message_French__c = generateFrenchEPolicyMessageNodes();
            update q;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(q);
            SMEQQuoteMessagesController smeqQuoteMsgObj = new SMEQQuoteMessagesController(sc);
        }
        Test.stopTest();
    }
    
    static String generateEnglishEPolicyMessageNodes(){
        String xml = '';
        xml += '<CommlPkgPolicyQuoteInqRs>';
        xml += '<rsa:Flavour>FL1</rsa:Flavour>';
        xml += '<rsa:LiveChatMessage_English>';
        xml += 'Refer:The following coverage(s) must be reviewed by an underwriter: Commercial General Liability - Occurrence . You may click [Next] to complete the remaining quote information and then click [Submit Referral] for our review. If you do not want RSA to review this application any further, please click [Complete the Decline] to end your transaction.Ineligible: Hurray!!!!  This risk does not meet minimum eligibility requirements because: The question Has the applicant been previously cancelled? was answered: Yes. Please click the Complete the Decline button to end your transaction.';
        xml += '</rsa:LiveChatMessage_English>';
        xml += '</CommlPkgPolicyQuoteInqRs>';
        xml += '<CommlPkgPolicyQuoteInqRs>';
        xml += '<rsa:Flavour>FL2</rsa:Flavour>';
        xml += '<rsa:LiveChatMessage_English>';
        xml += 'Refer:The following coverage(s) must be reviewed by an underwriter: Commercial General Liability - Occurrence . You may click [Next] to complete the remaining quote information and then click [Submit Referral] for our review. If you do not want RSA to review this application any further, please click [Complete the Decline] to end your transaction.Ineligible: This risk does not meet minimum eligibility requirements because: The question Has the applicant been previously cancelled? was answered: Yes. Please click the Complete the Decline button to end your transaction.';
        xml += '</rsa:LiveChatMessage_English>';
        xml += '</CommlPkgPolicyQuoteInqRs>';
        xml += '<CommlPkgPolicyQuoteInqRs>';
        xml += '<rsa:Flavour>FL3</rsa:Flavour>';
        xml += '<rsa:LiveChatMessage_English>';
        xml += 'Refer:The following coverage(s) must be reviewed by an underwriter: Commercial General Liability - Occurrence . You may click [Next] to complete the remaining quote information and then click [Submit Referral] for our review. If you do not want RSA to review this application any further, please click [Complete the Decline] to end your transaction.Ineligible: This risk does not meet minimum eligibility requirements because: The question Has the applicant been previously cancelled? was answered: Yes. Please click the Complete the Decline button to end your transaction.';
        xml += '</rsa:LiveChatMessage_English>';
        xml += '</CommlPkgPolicyQuoteInqRs>';
        return xml;
    }
    
    static String generateFrenchEPolicyMessageNodes(){
        String xml = '';
        xml += '<CommlPkgPolicyQuoteInqRs>';
        xml += '<rsa:Flavour>FL1</rsa:Flavour>';
        xml += '<rsa:LiveChatMessage_French>';
        xml += 'Référer: Jhingalala :::: Les garanties suivantes doivent être examinées par un rédacteur production: Flood. La situation 1 se trouve dans la zone 5 d’inondation et représente un risque élevé d’inondation. Une révision de production complète est nécessaire. Vous pouvez cliquer [Suivante] pour remplir les autres renseignements de la soumission et ensuite cliquez [Soumettre renvoi] pour notre examen. Si vous ne désirez pas que cette demande soit examinée de nouveau par la RSA examine davantage cette demande, veuillez cliquer [Finaliser le refus] pour terminer votre transaction.';
        xml += '</rsa:LiveChatMessage_French>';
        xml += '</CommlPkgPolicyQuoteInqRs>';
        xml += '<CommlPkgPolicyQuoteInqRs>';
        xml += '<rsa:Flavour>FL2</rsa:Flavour>';
        xml += '<rsa:LiveChatMessage_French>';
        xml += 'Référer: Les garanties suivantes doivent être examinées par un rédacteur production: Flood. La situation 1 se trouve dans la zone 5 d’inondation et représente un risque élevé d’inondation. Une révision de production complète est nécessaire. Vous pouvez cliquer [Suivante] pour remplir les autres renseignements de la soumission et ensuite cliquez [Soumettre renvoi] pour notre examen. Si vous ne désirez pas que cette demande soit examinée de nouveau par la RSA examine davantage cette demande, veuillez cliquer [Finaliser le refus] pour terminer votre transaction.';
        xml += '</rsa:LiveChatMessage_French>';
        xml += '</CommlPkgPolicyQuoteInqRs>';
        xml += '<CommlPkgPolicyQuoteInqRs>';
        xml += '<rsa:Flavour>FL3</rsa:Flavour>';
        xml += '<rsa:LiveChatMessage_French>';
        xml += 'Référer: hurrayy:::Les garanties suivantes doivent être examinées par un rédacteur production: Flood. La situation 1 se trouve dans la zone 5 d’inondation et représente un risque élevé d’inondation. Une révision de production complète est nécessaire. Vous pouvez cliquer [Suivante] pour remplir les autres renseignements de la soumission et ensuite cliquez [Soumettre renvoi] pour notre examen. Si vous ne désirez pas que cette demande soit examinée de nouveau par la RSA examine davantage cette demande, veuillez cliquer [Finaliser le refus] pour terminer votre transaction.';
        xml += '</rsa:LiveChatMessage_French>';
        xml +='</CommlPkgPolicyQuoteInqRs>';
        return xml;
    }
}