public with sharing class SMEQ_ApplicationPDFDao
{
    public QuoteRequestWrapper quoteRequest {public get; public set;}
    public Quote__c quote {public get; public set;}
    public Case cse {public get; public set;}
    public List<Account> accList {public get; public set;}
    public List<Contact> contactList {public get; public set;}
    public List<Risk__c> liabilityRisks {public get; public set;}
    public List<Risk__c> locationRisks {public get; public set;}
    public Map<String, Map<String, SIC_Answer__c>> dynamicAnswers {public get; public set;}
    public List<Coverages__c> locationCoverage {public get; public set;}
    public List<Coverages__c> liabilityCoverage {public get; public set;}
    public List<Claim__c> claims {public get; public set;}
    public Integer numOfClaims {public get; public set;}
    public List<Coverages__c> selectedCoverages {public set; public get;}
    public Coverages__c buildingCoverage {public set; public get;}
    public String lang {public get; public set;}
    public String currencyFormat {public get; public set;}
    public Boolean FrenchUser {get; set;}
    
    public String CODE_EQUIPMENT {public get {return SicQuestionService.CODE_EQUIPMENT;} private set;}
    public String CODE_STOCK {public get {return SicQuestionService.CODE_STOCK;} private set;}
    public String CODE_CANADIAN_REVENUE {public get {return SicQuestionService.CODE_CANADIAN_REVENUE;} private set;}
    public String CODE_US_REVENUE {public get {return SicQuestionService.CODE_US_REVENUE;} private set;}
    public String CODE_FOREIGN_REVENUE {public get {return SicQuestionService.CODE_FOREIGN_REVENUE;} private set;}

    public SMEQ_ApplicationPDFDao()
    {
        quoteRequest = getDetailsForOtherMarketsPDF();

        if (quoteRequest != null)
        {
            quote = quoteRequest.quote;
            cse = quoteRequest.cse;
            accList = quoteRequest.accList;
            contactList = quoteRequest.contactList;
            liabilityRisks = quoteRequest.liabilityRisks;
            locationRisks = quoteRequest.locationRisks;
            locationCoverage = quoteRequest.locationCoverage;
            liabilityCoverage = quoteRequest.liabilityCoverage;
            claims = quoteRequest.claims;
            numOfClaims = claims.size();

            system.debug('### {!selectedCoverages} ' + selectedCoverages);

            lang = UserService.getFormattedUserLanguage();
            if (lang == 'en')
            {
                currencyFormat = '{0, number, currency}';
            }
            else
            {
                currencyFormat = '{0, number, 0¤}';
            }

            dynamicAnswers = quoteRequest.dynamicAnswers;
        }
    }

    public String getCoverageTypeById(Id id, List<Coverages__c> coverages)
    {
        for(Coverages__c c : coverages)
        {
            if (c.Id == id)
            {
                return c.Type__c;
            }
        }
        return null;
    }

    public Risk__c getTranslatedRisk(Id id)
    {
        return [Select Id, toLabel(Province__c), toLabel(Construction_Type__c) from Risk__c where Id = :id];
    }

    /**
     * Get Quote Details for PDF for Other Markets
     * @param
     * @return  QuoteRequestWrapper
     */
    @TestVisible
    public QuoteRequestWrapper getDetailsForOtherMarketsPDF()
    {
        String quoteId = '';
        buildingCoverage = new Coverages__c();
        selectedCoverages = new List<Coverages__c>();

        if (Apexpages.currentpage().getparameters().get('quoteId') != null)
        {
            quoteId = Apexpages.currentpage().getparameters().get('quoteId');
            QuoteRequestWrapper quoteWrapper = FP_AngularPageController.getQuoteById(quoteId, null);
            system.debug('### quoteWrapper ' + quoteWrapper);
            Case c = [
                SELECT id
                FROM Case
                WHERE caseNumber = :quoteWrapper.cse.CaseNumber
            ];

            SIC_Code_Detail_Version__c scdv = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(c.ID)[0];
            if (UserInfo.getLanguage() == 'fr')
            {
                quoteWrapper.quote.Sic_Code_Description__c = scdv.Short_Description_Fr__c;
            }
            else
            {
                quoteWrapper.quote.Sic_Code_Description__c = scdv.Short_Description_En__c;
            }

            for (Coverages__c cov : quoteWrapper.locationCoverage)
            {
                if (cov.Type__c == RiskService.COVERAGE_BUILDING)
                {
                    buildingCoverage = cov;
                }
                else if (cov.Package_Id__c == quoteWrapper.quote.Package_Type_Selected__c &&
                         scdv.Coverage_Add_Ons__c.contains(cov.Type__c))
                {
                    selectedCoverages.add(cov);
                }
            }
            return quoteWrapper;
        }
        else
        { return null; }
    }
}