/**
* Author: Ashraf Iqbal
* Created on: April 19, 2018
*
* The purpose of this class is to consolidate the creation of SMEQ records for testing.
*/
@isTest
public class CSIOSaveQuoteRequestFactory_Test {
    private static Quote__c getQuote()
    {
        return [
            SELECT id, Package_Type_Selected__c, Case__c, Case__r.Segment__c
            FROM Quote__c
            LIMIT 1
        ];
    }
    static testmethod void testSaveQuote() {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw) {
	        SMEQ_TestDataGenerator.generateFullTestQuoteV2();
	        Quote__c q = getQuote();
	        RequestInfo ri = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.SAVE_QUOTE, q);            
	        SOQLDataSet sds = new SOQLDataSet(q.Id, ri);
	        CSIOSaveQuoteRequestFactory csqrf = new CSIOSaveQuoteRequestFactory(sds, ri);
        }
    }
}