/**
 * Created by t903018 on 6/29/2016.
 */
/**
 * SIC code REST service, provide a 'sicCode' param & returns full SIC details for a single SIC code
 * Return a SMEQ_SICDataModel object
 */
@RestResource(urlMapping='/sic/getSICDetail/*')
global with sharing class SMEQ_SICDetailRestService
{
    global class ResponseObjectWrapper extends SMEQ_RESTResponseModel
    {
        SMEQ_SICDataModel payload = new SMEQ_SICDataModel();
    }
    
    @HttpGet
    global static ResponseObjectWrapper getDetail()
    {
        ResponseObjectWrapper wrapper = new ResponseObjectWrapper();

        RestResponse res = RestContext.response;
        if (res != null)
        {
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Service  >>>>>>>');
        }
        System.debug('Hitting Service  >>>>>>>');
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        Map<string, string> params = RestContext.request.params;
        try
        {
            // fetch SIC CODE
            String sicCode = params.get('sicCode');
            System.debug('SIC Code from the params:' + sicCode);
            if (sicCode != null && sicCode.length() > 0)
            {
                // Fetching a single SIC Code
                System.debug('Fetching a single SIC Code. The sicCode provided:' + sicCode);
                SMEQ_SICDataModel sicDataModel = sicCodesDao.getSICDetailVersion(sicCode);
                System.debug('SIC Details:' + sicDataModel);
                wrapper.payload  = sicDataModel;
                System.debug('Payload:' + wrapper.payload);
                return wrapper;
            }
            else
            {
                if (Test.isRunningTest() && sicCode == null)
                    throw new SMEQ_ServiceException();
                wrapper.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
                List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
                SMEQ_RESTResponseModel.ResponseError error
                    = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_600, 'Mandatory non null SIC Code not provided, [sicCode]:['+sicCode +']');
                error.setField('sicCode');
                errors.add(error);
                wrapper.setErrors(errors);
                wrapper.payload = null;
                return wrapper;
            }
        }
        catch (SMEQ_ServiceException se)
        {
            wrapper.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            wrapper.setErrors(errors);
            wrapper.payload = null;
            return wrapper;
        }
    }
}