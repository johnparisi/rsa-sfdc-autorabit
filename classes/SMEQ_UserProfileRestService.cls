/**
 * User Profile REST service returns users detail object ** for Broker Users only ***
 *
 * Returns SMEQ_UserProfileDataModel
 */

@RestResource(urlMapping='/user/getUserProfile/*')
global with sharing class SMEQ_UserProfileRestService {

    String DEFAULT_LANG = 'en';

    global class ResponseObjectWrapper extends SMEQ_RESTResponseModel {
        SMEQ_UserProfileDataModel payload = new SMEQ_UserProfileDataModel();
        public SMEQ_UserProfileDataModel getPayload()
        {
            return payload;
        }
    }

    @HttpGet
    global static ResponseObjectWrapper getUserProfile() {
        ResponseObjectWrapper wrapper = new ResponseObjectWrapper();
        RestResponse res = RestContext.response;
         if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        SMEQ_UserProfileDao profileDao = new SMEQ_UserProfileDao();
        try {
            wrapper.payload = profileDao.getSessionUserProfile();
            return wrapper;
        } catch (SMEQ_ServiceException se) {
            wrapper.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                    = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            errors.add(error);
            wrapper.setErrors(errors);
            return wrapper;
        }
    }
}