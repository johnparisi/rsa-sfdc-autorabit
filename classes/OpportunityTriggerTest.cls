/**
* @File Name    :   OpportunityTriggerTest
* @Description  :   Test class for OpportunityTrigger
* @Date Created :   02/03/2016
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   testing class
* @Modification Log:
*****************************************************************
* Ver       Date        Author              Modification
* 1.0       02/03/2016  Habiba Zaman        Created the file/class
**/
@isTest
private class OpportunityTriggerTest {
	

    @isTest
    static void Test_AfterUpdateOpportunity()
    {
         
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testo1', Email='testo1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg9.com');
    
        insert u;

        User uKam = new User(Alias='testKam', Email='testuser1Kam@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='TestingKam', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='Kamtestuser1g4egas4qgj6kl8r@testorg.com');
    
        insert uKam;

        // First, set up test price book entries.
        // Insert a test product.
        //Product
        Product2 product = new Product2();
        product.Name ='Property & Casualty';
        product.IsActive= true;
        product.Family='Mid-Market & Global Specialty Lines';
        //product.CanUseRevenueSchedule
        insert product;

        Product2 product2 = new Product2();
        product2.Name ='Fleet & Garage (Mid-Market & GSL)';
        product2.IsActive= true;
        product2.Family='Mid-Market & Global Specialty Lines';
        //product.CanUseRevenueSchedule
        insert product2;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = product.Id,
            UnitPrice = 1120, IsActive = true);
        insert standardPrice;

        PricebookEntry standardPrice2 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = product2.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice2;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = product.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPrice;
        PricebookEntry customPrice2 = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = product2.Id,
        UnitPrice = 1000, IsActive = true);
        insert customPrice2;
        
        // Next, perform some tests with your test price book entries.
        OpportunityService service = new OpportunityService();

        Opportunity opp = new Opportunity();
        opp.bkrProductLineEffectiveDate__c = Date.today().addDays(12);
        opp.bkrProduct__c = product.Id;
        opp.RecordTypeId = service.getRenewalOpportunityRecordType();
        opp.bkrMMGSL_KAM__c = uKam.Id;
        opp.Name = 'Test opp line item';
        opp.StageName = 'Renewal Assigned';
        opp.Probability = 10.0;
        opp.CloseDate = Date.today().addDays(12);
       
        Test.startTest();
        
        insert opp;
        opp.bkrProduct__c = product2.Id;
        opp.bkrMMGSL_KAM__c = u.Id; 
        update opp;        
        
            
   

        Test.stopTest();

        List<OpportunityTeamMember> otm = [Select UserId from OpportunityTeamMember where  OpportunityId =:opp.Id and UserId=:u.Id];
        System.assertEquals(otm.get(0).UserId, u.Id);

        

    }



    // On change Opportunity Owner Policy Owner related to the current opportunity need to be updated.
    @isTest
    static void TestOpportunityOwnerChange()
    {
        //Custom setting
        insert new bkrMMGSL_CustomSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Auto_Renewal_ThresholdDays__c=12);

        // Create a Sample Client Account
        PolicyService pservice = new PolicyService();
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255203813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        brokerageAcc.bkrAccount_HuonBrokerNumber__c='001231942';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testu1', Email='testu1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', FirstName='Test',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg2.com');
    
        insert u;

        User u2 = new User(Alias='testu2', Email='testu2@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Tespting', LanguageLocaleKey='en_US', FirstName='Tepst',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testr1g4egas4qgj6kl8r@testorg3.com');
    
        insert u2;

        //Policy to Client Mapping


        Policy_to_Client_Mapping__c pcm2 = new Policy_to_Client_Mapping__c();
        pcm2.Policy_number__c = '0000000122';
        pcm2.Client_Account__c = clientAcc.Id;
        insert pcm2;

        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();

        // Policy Data

        Policy__c policy2 = new Policy__c();
        policy2.Name ='0000000122';
        policy2.Client_Name__c = clientAcc.Name;
        policy2.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy2.Master_Broker_Account__c = brokerageAcc.Id;
        policy2.Strategic_Segment__c = 'Construction';
        policy2.Region_Group__c = 'Atlantic';
        policy2.Product_Code__c='COM';
        policy2.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy2.Policy_Status__c='Future';
        policy2.Effective_Date__c= Date.today().addDays(-300);
        policy2.Term_Expiry_Date__c = Date.today().addDays(100);
        policy2.bkrPolicy_Premium__c = 123.23;
        policy2.Product__c = 'Property/Casualty';
        policy2.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy2.Control_Field__c = pservice.createPolicyControlField(policy2.Name, policy2.Effective_Date__c);

        Test.startTest();         

        bkrMMGSL_CustomSettings__c cs = bkrMMGSL_CustomSettings__c.getOrgDefaults();
        System.assertEquals(cs.Auto_Renewal_ThresholdDays__c,12);

        insert policy2;

        policy2.OwnerId = u2.Id;
        policy2.Force_Renewal__c = true;
        update policy2;

        Policy__c newPolicy2 = [Select Policy_Stage__c,Qualified_Renewal__c from Policy__c where Id =:policy2.Id];
        System.assertEquals(newPolicy2.Policy_Stage__c,'Renewal Opportunity Created');



        Test.stopTest();

    }

@isTest
    static void Test_PolicyIssuesInOpportunity()
    {
        Map<String,Id> OpportunityTypes = Utils.GetRecordTypeIdsByDeveloperName(Opportunity.SObjectType, true); 
        
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testo1', Email='testo1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg9.com');
    
        insert u;

         // Create a Sample Client Account
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        insert brokerageAcc;

        // First, set up test price book entries.
        // Insert a test product.
        //Product
        Product2 product = new Product2();
        product.Name ='Property & Casualty';
        product.IsActive= true;
        product.Family='Mid-Market & Global Specialty Lines';
        //product.CanUseRevenueSchedule
        insert product;

        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = product.Id,
            UnitPrice = 1120, IsActive = true);
        insert standardPrice;

        
        
        // Next, perform some tests with your test price book entries.
        OpportunityService service = new OpportunityService();

        Opportunity opp = new Opportunity();
        opp.bkrProductLineEffectiveDate__c = Date.today().addDays(12);
        opp.bkrProduct__c = product.Id;
        opp.RecordTypeId = OpportunityTypes.get('bkrMMGSL_Opportunity');
        opp.bkrClientAccount__c = clientAcc.Id;
        opp.AccountId = brokerageAcc.Id;
        opp.Name = 'Test opp line item';
        opp.bkrMMGSL_StrategicSegment__c = 'Project Construction';
        opp.StageName = 'Closed Won - Pre-Processing Work';
        opp.bkrOpportunity_ReasonWonorLost__c = 'Knowledge';
        opp.CloseDate = Date.today().addDays(12);
        opp.bkrPolicyNumber__c='COM1234342';
        opp.bkrMMGSL_PolicyRecordCreated__c = true;

        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
       
        Test.startTest();
        service.createNewBusinessPolicy(oppList);    
        
            
   

        Test.stopTest();

       
        List<Policy__c> policy = [Select Id, Name from Policy__c where Name = 'COM1234342'];
        System.assertEquals(policy.get(0).Name, 'COM1234342');

        

    }

    @isTest
    static void Test_SpecialtyLineTaskCount()
    {
        Map<String,Id> OpportunityTypes = Utils.GetRecordTypeIdsByDeveloperName(Opportunity.SObjectType, true); 
        
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='testo1', Email='testo1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg9.com');
    
        insert u;
    
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        insert brokerageAcc;

        
        // Next, perform some tests with your test price book entries.
        OpportunityService service = new OpportunityService();

        Opportunity opp = new Opportunity();
        opp.CloseDate = Date.today().addDays(12);
        opp.RecordTypeId = OpportunityTypes.get('Commercial_Insurance_Submission');
        opp.AccountId = brokerageAcc.Id;
        opp.Name = 'Test opp line item';
        opp.StageName = 'Cold Lead';
        opp.Type = 'New Business';

        Insert opp;
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        Map<String,Id> TaskTypes = Utils.GetRecordTypeIdsByDeveloperName(Task.SObjectType, true); 
            
        Task newTask = new Task();
        newTask.Subject = 'Test';
        newTask.Status = 'Completed';
        newTask.Priority ='Normal';
        newTask.Type = 'Broker presentation and opportunity discussion';
        newTask.RecordTypeId = TaskTypes.get('Specialty_Line_Task');
        newTask.WhatId = opp.Id;
        insert newTask;

        Test.startTest();
    
        Map<Id,Integer> oppTask = service.getCompletedTaskCountForOpportunity(oppList);

        System.assertEquals(oppTask.get(opp.Id), 1);
        try{
            opp.StageName = 'Active Lead';
            update opp;
        }
        catch (Exception ex){
            Boolean expectedExceptionThrown = ex.getMessage().contains('Script-thrown exception') ? true : false; 
            System.assertEquals(expectedExceptionThrown, false);
        }
        Test.stopTest();

    }

}