/**
  * @author        Anmol Bakshi
  * @date          01/24/2017
  * @description   Class to hold the web service operation to invoke the batch job for the Broker Stage Update Process.
                   The batch job would update the mapping object to map the broker number to the account Id.
*/

global class SLID_ESB_BrokerStageService 
{
    private static String SUCCESS_RESP_MSG = 'SLID_PurgeBrokerMapping batch job successfully queued with Id = ';
    private static String FAILURE_RESP_MSG = 'Something went wrong while queuing up the batch job SLID_PurgeBrokerMapping in Salesforce. Please contact your Salesforce Administrator';
    
    @TestVisible
    private static Boolean IS_TEST = false;
    
    global class ServiceResponse
    {
        webservice Boolean isSuccess;
        webservice String message;
    }
    
    webservice static ServiceResponse executeBrokerStageBatchProcess()
    {
    	
    	 //Create Integration Log
        Util_Logging.integrationLogId = UTIL_Logging.createIntegrationLog(
                                                            SLID_ESB_ServiceUtils.inboundRecordTypeId,
                                                            '',
                                                            'Auto Execute Broker Stage Mapping Update after Data Load',
                                                            'ESB',
                                                            String.valueOf(System.Datetime.now())
                                                            );
    	
        try
        {
            //If there is already existing/queued jobs for purging the broker mapping or Creating broker mapping , then we delete those jobs first
            // and then queue them in this method.
            abortExistingQueuedJobs();
            String batchJobId = queueNewMappingJob();
            
            //Create Response
            ServiceResponse resp = new ServiceResponse();
            
            if(batchJobId != null && batchJobId != '')
            {
                resp.isSuccess = true;
                resp.message = SUCCESS_RESP_MSG + batchJobId;
            }
            else
            {
                resp.isSuccess = false;
                resp.message =  FAILURE_RESP_MSG;
            }
            
            return resp;
        }
        catch(Exception e)
        {
            //Create Failure Response
            ServiceResponse rsp = new ServiceResponse();
            rsp.isSuccess = false;
            rsp.message = e.getMessage();
            
             //Create a exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_BrokerStageService','executeBrokerStageBatchProcess','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            
            return rsp;
        }
        return null;
    }
    
    private static String queueNewMappingJob()
    {
        ID batchProcessId = Database.executeBatch(new SLID_PurgeBrokerMapping(false),500);
        return String.valueOf(batchProcessId);
    }
    
    private static void abortExistingQueuedJobs()
    {
        try
        {
            //get the class Ids of the apex class that is used for the batch process.
            List<String> classIds = fetchBrokerMappingClassIds();
            
            //query all apex jobs for the broker mapping process that are at at a Holding or Queued status.
            List<String> jobStatus = new List<String>{'Preparing','Processing','Holding','Queued'};
            
            List<AsyncApexJob> jobs = [Select Id , JobType, Status , CompletedDate , ApexClassId 
                                        From AsyncApexJob 
                                        Where ApexClassId IN: classIds
                                        And JobType = 'BatchApex'
                                        And Status IN: jobStatus];
            
            if(jobs != null && !jobs.isEmpty())
            {
                if(IS_TEST == true)
                    jobs = null;
                    
                for(AsyncApexJob job : jobs)
                    System.abortJob(job.Id);
            }
        }
        catch(Exception e)
        {
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_BrokerStageService','abortExistingQueuedJobs','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
        }
    }
    
    @TestVisible
    private static List<String> fetchBrokerMappingClassIds()
    {
        try
        {
            List<String> brokerClassIds = new List<String>();
            
            if(IS_TEST == true)
                brokerClassIds = null;
            
            SLID_Broker_Mapping_Settings__c brokerMappingSettings = SLID_Broker_Mapping_Settings__c.getInstance();
            
            List<String> classNames = new List<String>();
            
            classNames.add(brokerMappingSettings.SLID_CreateBrokerMappingClassName__c);
            classNames.add(brokerMappingSettings.SLID_PurgeBrokerMappingClassName__c);
            classNames.add(brokerMappingSettings.SLID_PurgeBrokerStageClassName__c);
            
            List<ApexClass> classes = [Select Status, Name, Id From ApexClass Where Name IN: classNames];
            
            for(ApexClass c : classes)
                brokerClassIds.add(c.Id);
            
            return brokerClassIds;
        }
        catch(Exception e)
        {
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_BrokerStageService','fetchBrokerMappingClassIds','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            if(!test.isRunningTest())
                throw e;
            else
                return null;
        }   
      }
}