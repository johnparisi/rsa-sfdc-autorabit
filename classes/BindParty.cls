public class BindParty {

    public Id id {get; set;}
    public Boolean selected {get; set;}
    public String name {get; set;}
    public String partyType {get; set;}
    public Double partyNumber {get; set;}
    public String relationshipType {get; set;}
    public String relationshipNumber {get; set;}
    public Id quote {get; set;}
    public Id relatedRisk {get; set;}
    public String relatedRiskType {get; set;}
    public Boolean isEditable {get; set;}
    public Boolean toBeDeleted {get; set;}
    public Id contact {get; set;}
    public String contactName {get; set;}
    public Id account {get; set;}
    public String accountName {get; set;}
    public List<SelectOption> relationshipTypeOptions {get;set;} 
    public String accountStreet {get; set;}
    public String accountProvince {get; set;}
    public String contactStreet {get; set;}
    public String contactProvince {get; set;}
    
    public boolean addedBySearch {
        get { return Id == null;}
    }
    
}