/**
  * @author        Saumil Bapat
  * @date          10/20/2016
  * @description   ObjectUtils class for create upserting SObjects dynamically
*/
public class SLID_ESB_ObjectUtils
{
    //store describe results for targetObject Ids
    public static Map<String, Schema.SObjectField> targetFieldDescribeMap = new Map<String, Schema.SObjectField>();

    //store target Objects to avoid redoing the describe calls
    public static Map<Id, Schema.SObjectType> targetObjectDescribeMap = new Map<Id, Schema.SObjectType>();

    //store whether allOrNone is set to true
    public static boolean allOrNone = false;

    //store if a DML failure has occurred
    public static boolean dmlFailure = false;

    //Store the intial database State
    private static Savepoint initialState;

    //Instiate the targetObject and targetField describe maps
    public static void instantiateDescribeMap(List<SLID_Mapping_TargetObject__c> targetObjects)
    {
        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('instantiateDescribeMap');

            //Do a global describe to retrieve all object definitions in the org
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();

            //loop through target objects for the service process
            for (SLID_Mapping_TargetObject__c targetObject: targetObjects)
            {
                Schema.SObjectType targetObjectDescribe =  gd.get(targetObject.Target_Object_API_Name__c);

                Map<String, Schema.SObjectField> targetObjectFieldsDescribeMap = targetObjectDescribe.getDescribe().fields.getMap();
                //Store the metadata definition of the target objects
                targetObjectDescribeMap.put(targetObject.Id, targetObjectDescribe);

                //loop through child target fields for this target object
                for (SLID_Mapping_TargetField__c targetField : targetObject.SLID_Mapping_TargetField__r)
                {
                    Schema.SObjectField tagetFieldDescribe = targetObjectFieldsDescribeMap.get(targetField.Target_Field_API_Name__c);

                    //Add target  field describe to map
                    SLID_ESB_ObjectUtils.targetFieldDescribeMap.put(
                        targetField.Id,
                        tagetFieldDescribe
                    );
                }
            }
        }
        catch(Exception e)
        {
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ObjectUtils','instantiateDescribeMap','','Error');
            UTIL_Logging.logException(log);
            throw e;
        }
    }

    //Cast and set the field value on the record
    public static void setFieldValue(SObject currentObject, String convertedValue, SLID_Mapping_TargetObject__c targetObject, SLID_Mapping_TargetField__c targetField)
    {
        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('setFieldValue');

            System.Debug('~~~setFieldValue TargetField: ' + targetField.Id);
            System.Debug('~~~setFieldValue convertedValue: ' + convertedValue);
            Schema.SObjectField fieldDefinition = SLID_ESB_ObjectUtils.targetFieldDescribeMap.get(targetField.Id);
            Schema.DescribeFieldResult fieldDescribe = fieldDefinition.getDescribe();
            Schema.DisplayType fieldType = fieldDescribe.getType();

            if(targetField.External_Id_for_Target_Field__c == null)
            {
                system.debug('currentObject -->'+currentObject);
                system.debug('convertedValue -->'+convertedValue);
                system.debug('targetField.Target_Field_API_Name__c -->'+targetField.Target_Field_API_Name__c);
                system.debug('fieldType -->'+fieldType);
                castFieldValue(currentObject, convertedValue, targetField.Target_Field_API_Name__c, fieldType);
            }
            else
            {
                String relationshipName = fieldDescribe.getRelationshipName();
                Schema.sObjectType parentObjectType = fieldDescribe.getReferenceTo()[0];
                SObject parentRecord = parentObjectType.newSObject();
                castFieldValue(parentRecord, convertedValue, targetField.External_Id_for_Target_Field__c, Schema.DisplayType.Reference);
                currentObject.putSObject(relationshipName, parentRecord);
            }
        }
        catch(Exception e)
        {
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ObjectUtils','setFieldValue','','Error');
            UTIL_Logging.logException(log);
            throw e;
        }
    }

    //Cast and set the field value on the record
    public static void castFieldValue(SObject currentObject, String convertedValue,  String targetFieldAPIName, Schema.DisplayType fieldType)
    {
        system.debug('currentObject -->'+currentObject);
        system.debug('convertedValue -->'+convertedValue);
        system.debug('targetFieldAPIName -->'+targetFieldAPIName);
        system.debug('fieldType -->'+fieldType);
        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('castFieldValue');

            if(convertedValue == null || convertedValue == '') //Checkbox Field
            {
              currentObject.put(targetFieldAPIName, null);
            }
            else if(fieldType == Schema.DisplayType.Boolean) //Checkbox Field
            {
              Boolean fieldVal = Boolean.valueOf(convertedValue);
              currentObject.put(targetFieldAPIName, fieldVal);
            }
            else If (fieldType == Schema.DisplayType.Currency) //Currency Field
            {
              Decimal fieldVal = Decimal.valueOf(convertedValue);
              currentObject.put(targetFieldAPIName, fieldVal);
            }
            else If (fieldType == Schema.DisplayType.Date) //DateTime Field
            {
              Date fieldVal = Date.valueOf(convertedValue);
              currentObject.put(targetFieldAPIName, fieldVal);
            }
            else If (fieldType == Schema.DisplayType.DateTime || fieldType == Schema.DisplayType.Time) //DateTime Field
            {
              DateTime fieldVal = DateTime.valueOf(convertedValue);
              currentObject.put(targetFieldAPIName, fieldVal);
            }
            else If (fieldType == Schema.DisplayType.Integer) //Integer Field
            {
              Integer fieldVal = Integer.valueOf(convertedValue);
              currentObject.put(targetFieldAPIName, fieldVal);
            }
            else If (fieldType == Schema.DisplayType.Double) //Double Field
            {
              Double fieldVal = Double.valueOf(convertedValue);
              currentObject.put(targetFieldAPIName, fieldVal);
            }
            else //All fields which can be treated as String (Text / long Text / picklist / Id)
            {
                currentObject.put(targetFieldAPIName, convertedValue);
            }
        }
        catch(Exception e)
        {
            System.Debug('~~~Error: convertedValue: ' + convertedValue);
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ObjectUtils','castFieldValue','','Error');
            UTIL_Logging.logException(log);
            throw e;
        }
    }

    public static SObject returnNewSObjectInstance(String targetObjectId)
    {
        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('returnNewSObjectInstance');

            sObject newRecord = SLID_ESB_ObjectUtils.targetObjectDescribeMap.get(targetObjectId).newSObject();
            return newRecord;
        }
        catch(Exception e)
        {
            System.Debug('~~~ERROR targetObjectId: ' + targetObjectId);
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ObjectUtils','returnNewSObjectInstance','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
        }
        return null;
    }

    public static List<Database.upsertResult> upsertGenericSObjectList(Schema.SObjectType objectType, Schema.SObjectField externalIdField, List<SObject> Records)
    {
        //initialize the save point the first time we go into this method
        if(SLID_ESB_ObjectUtils.initialState == null)
        {
          SLID_ESB_ObjectUtils.initialState =  Database.setSavepoint();
        }

        try
        {
            //Throw a exception if testing (to test catch statements)
            SLID_ESB_ServiceConstants.throwTestingException('upsertGenericSObjectList');

            //List of upsert results to return
            List<Database.upsertResult> targetObjectUpsertResults;

            if(objectType == Schema.Account.getSObjectType())
            {
                List<Account> accountRecords = new List<Account>();
                for (SObject o : Records) accountRecords.add((Account) o);
                if(externalIdField != null)
                {
                    targetObjectUpsertResults = Database.Upsert(accountRecords, externalIdField, false);
                }
                else
                {
                    targetObjectUpsertResults = Database.Upsert(accountRecords, false);
                }
            }
            else if(objectType == Schema.Case.getSObjectType())
            {
                List<Case> caseRecords = new List<Case>();
                for (SObject o : Records) caseRecords.add((Case) o);
                if(externalIdField != null)
                {
                    System.Debug('~~~Case Upsert W/ ExtID: ' + caseRecords);
                    targetObjectUpsertResults = Database.Upsert(caseRecords, externalIdField, false);
                }
                else
                {
                    System.Debug('~~~Case Upsert W/O ExtID: ' + caseRecords);
                    targetObjectUpsertResults = Database.Upsert(caseRecords, false);
                }
            }
            else if(objectType == Schema.CaseComment.getSObjectType())
            {
                List<CaseComment> caseCommentRecords = new List<CaseComment>();
                for (SObject o : Records) caseCommentRecords.add((CaseComment) o);
                if(externalIdField != null)
                {
                    System.Debug('~~~Case Upsert W/ ExtID: ' + caseCommentRecords);
                    targetObjectUpsertResults = Database.Upsert(caseCommentRecords, externalIdField, false);
                }
                else
                {
                    System.Debug('~~~Case Upsert W/O ExtID: ' + caseCommentRecords);
                    targetObjectUpsertResults = Database.Upsert(caseCommentRecords, false);
                }
            }
            else if(objectType == Schema.Opportunity.getSObjectType())
            {
                List<Opportunity> opportunityRecords = new List<Opportunity>();
                for (SObject o : Records) opportunityRecords.add((Opportunity) o);
                if(externalIdField != null)
                {
                    targetObjectUpsertResults = Database.Upsert(opportunityRecords, externalIdField, false);
                }
                else
                {
                    targetObjectUpsertResults = Database.Upsert(opportunityRecords, false);
                }
            }
            else if(objectType == Schema.Quote_Version__c.getSObjectType())
            {
                List<Quote_Version__c> quoteVersionRecords = new List<Quote_Version__c>();
                for (SObject o : Records) quoteVersionRecords.add((Quote_Version__c) o);
                if(externalIdField != null)
                {
                    targetObjectUpsertResults = Database.Upsert(quoteVersionRecords, externalIdField, false);
                }
                else
                {
                    targetObjectUpsertResults = Database.Upsert(quoteVersionRecords, false);
                }
            }
            else if(objectType == Schema.Broker_Stage__c.getSObjectType())
            {
                List<Broker_Stage__c> brokerStageRecords = new List<Broker_Stage__c>();
                for (SObject o : Records) brokerStageRecords.add((Broker_Stage__c) o);
                if(externalIdField != null)
                {
                    targetObjectUpsertResults = Database.Upsert(brokerStageRecords, externalIdField, false);
                }
                else
                {
                    targetObjectUpsertResults = Database.Upsert(brokerStageRecords, false);
                }
            }
            //Check the results for any failures
            checkForExceptions(targetObjectUpsertResults);

            if(SLID_ESB_ObjectUtils.dmlFailure == true && SLID_ESB_ObjectUtils.allOrNone == true)
            {
              //Do a database rollback
              Database.rollback(SLID_ESB_ObjectUtils.initialState);
            }
            return targetObjectUpsertResults;
        }
        catch(Exception e)
        {
            System.Debug('~~~ERROR: upsertGenericSObjectList');
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ObjectUtils','upsertGenericSObjectList','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
        }
        return null;
    }

    //Method to check the upsert results for any failures
    private static void checkForExceptions(List<Database.UpsertResult> upsertResults)
    {
      try
      {
        //check if the dmlFailure flag has already been set
        if (SLID_ESB_ObjectUtils.dmlFailure == true)
        {
          return;
        }

        //Iterate through the upsertResults to check for a failure
        for (Database.upsertResult upResult : upsertResults)
        {
          if (!upResult.isSuccess())
          {
            SLID_ESB_ObjectUtils.dmlFailure = true;
            return;
          }
        }
      }
      catch(Exception e)
      {
          System.Debug('~~~ERROR: checkForExceptions: ' + upsertResults);
          UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ObjectUtils','checkForExceptions','',UTIL_Logging.DEBUG_LEVEL_ERROR);
          UTIL_Logging.logException(log);
          throw e;
      }
    }
}