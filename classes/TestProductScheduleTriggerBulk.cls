@isTest(seeAllData=true)
public class TestProductScheduleTriggerBulk {
    static testMethod void TestProductScheduleTrigger() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
        Pricebook2 pbk1 = new Pricebook2 (Name='Test Pricebook',Description='Test Pricebook', isActive=true);
        insert pbk1;
        Product2 prd1 = new Product2 (Name = 'Test Product', IsActive = true, CanUseRevenueSchedule = true);
        insert prd1;
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb.id,UnitPrice=50, isActive=true);
        insert pbe1;
        Opportunity opp1 = new Opportunity (Name = 'Test Opportunity', CloseDate = Date.today(), StageName = 'Proposal/Price Quote', Pricebook2Id = pbe1.Pricebook2Id, AccountId = acc.id);
        insert opp1;
        
        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=pbe1.id, quantity=4, totalprice=200);
        insert lineItem1;
    
        Test.startTest();
    
        lineItem1.Term__c = 10;
        update lineItem1;
        
        Test.stopTest();
    }
}