@isTest
public class LiveChat_CustomSettingUtility_Test
{
     /**
     * Method to test getCustomSettingsForLiveChat returns valid list with a valid parameter
     * Assertions
     * 1. Assert returned custom setting maps size is > 1 after setUpTest values have been inserted
     */ 
    @isTest static void test_getChatSettings()
    {   
        List<LiveChat_Mapping__c> lcMappingsList = new List<LiveChat_Mapping__c>();
        //lcMappingsList = LiveChat_CustomSettingUtility.getChatSettings('livechat','Ontario','EN','www','RA');
        LiveChat_CustomSettingUtility lcUtil = new LiveChat_CustomSettingUtility();
        lcMappingsList = lcUtil.getChatSettings('livechat','Ontario','EN','www','RA');
System.debug (lcMappingsList);
System.debug( lcMappingsList.size() );           
        System.assert(lcMappingsList.size() == 4);
    }
    
     @testSetup 
     static void setupTestData() {

    //insert LiveChat_Mapping__c Custom Settings

    LiveChat_Mapping__c deploymentIDMapping   = new LiveChat_Mapping__c(Name = 'RA-livechat-01',
                                                                Field__c = 'livechat',
                                                                externalValue__c = '00D6300000093xH',
                                                                SFKey__c =  'SFDC_CHAT_DEPLOYMENT_ID',
                                                                variant__c = 'www',
                                                                bundle__c = 'RA'                                                                
                                                            );
    insert deploymentIDMapping;


    LiveChat_Mapping__c buttonIDMapping = new LiveChat_Mapping__c(Name='RA-livechat-02',
                                                           Field__c = 'livechat',
                                                           externalValue__c = '573630000008ORb',
                                                           SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                           variant__c = 'www',
                                                           bundle__c = 'RA'
                                                            );
    insert buttonIDMapping;


    LiveChat_Mapping__c buttonIDMappingFP = new LiveChat_Mapping__c(Name='RA-livechat-02',
                                                               Field__c = 'livechat',
                                                               externalValue__c = '573630000008ORb',
                                                               SFKey__c = 'SFDC_CHAT_BUTTON_ID',
                                                               variant__c = 'www',
                                                               bundle__c = 'RA'
                                                                );
    insert buttonIDMappingFP;



    LiveChat_Mapping__c orgIDMapping = new LiveChat_Mapping__c(Name='RA-livechat-03',
                                                           Field__c = 'livechat',
                                                           externalValue__c = 'SFDC_ORG_ID',
                                                           SFKey__c = '00D6300000093xH',
                                                           variant__c = 'www',
                                                           bundle__c = 'RA'
                                                            );
    insert orgIDMapping;

  
  }

}