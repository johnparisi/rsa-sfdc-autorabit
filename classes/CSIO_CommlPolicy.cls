public class CSIO_CommlPolicy
{
    public Static Final String COMPANYCD_RSA = 'ROY';
    public Static Final String COMPANYCD_CNS = 'CNS';
    public static final String REGION_PACIFIC = 'Pacific';

    private RequestInfo ri;
    private List<Claim__c> claims;
    private Quote__c quote;
    private List<SOQLDataSet.Risk> policyRisks;
    private Case parentCase;
    private List<Contact> contactList = new List<Contact>();
    private List<Account> accountList = new List<Account>();
    private List<Quote_Risk_Relationship__c> quoteRiskRelationshipList = new List<Quote_Risk_Relationship__c>();
    
    public String LOBCd;
    public String languageCd;
    public List<Loss> loss;
    public String underwriterId;
    public OtherOrPriorPolicy otherOrPriorPolicy;
    public CommlPolicySupplement commlPolicySupplement;
    public CommlCoverage commlCoverageClaims;
    public List<CSIO_CommlCoverage> commlCoveragePolicy;
    public String segment;
    public String flavour;
    public String rsaPackage;
    public String rsaOperation;
    public String rsaContextId;
    public String billingMethodCd;
    public String userRegion;
    public String companyCd;
    public Integer yearsClaimsFree;
    public string sfCaseNumber; 
    public CSIO_QuoteInfo quoteInfo;
    public List<CSIO_AdditionalInterest> additionalInterestList;

    public CSIO_CommlPolicy(RequestInfo ri, List<Claim__c> claims, Quote__c quote, List<SOQLDataSet.Risk> policyRisks)
    {
        this.ri = ri;
        this.claims = claims;
        this.quote = quote;
        this.policyRisks = policyRisks;
        convert();
    }
    //adding this for GETPOLICY where claims, and quote are still not populated 
    public CSIO_CommlPolicy(RequestInfo ri, Case parentCase) 
    {
        this.ri = ri;
        this.parentCase = parentCase;
        convertCase();
    }
    
    public CSIO_CommlPolicy(
        RequestInfo ri, List<Claim__c> claims, Quote__c quote, List<SOQLDataSet.Risk> policyRisks,
        List<Account> accountList, List<Contact> contactList, List<Quote_Risk_Relationship__c> quoteRiskRelationshipList)
    {
        this.ri = ri;
        this.claims = claims;
        this.quote = quote;
        this.policyRisks = policyRisks;
        this.accountList = accountList;
        this.contactList = contactList;
        this.quoteRiskRelationshipList = quoteRiskRelationshipList;
        convert();
    }

    private void convert()
    {
        this.LOBCd = 'csio:1'; //Fix this
        this.languageCd = this.quote.Language__c != null ? this.quote.Language__c : UserService.getFormattedUserLanguage();

        this.loss = new List<Loss>();
        for (Claim__c c : claims)
        {
            if (!c.HUON_Claim__c){
                Loss l = new Loss(c,quote);
                this.loss.add(l);
            }
        }
        
        if (ri.getUnderwriterId() != null)
        {
            this.underwriterId = ri.getUnderwriterId().FederationIdentifier;
        }
        
        this.otherOrPriorPolicy = new OtherOrPriorPolicy(this.claims);
        this.commlPolicySupplement = new CommlPolicySupplement(this.quote, this.ri);
        
        this.commlCoverageClaims = new CommlCoverage(this.claims);
        
        this.commlCoveragePolicy = new List<CSIO_CommlCoverage>();
        for (SOQLDataSet.Risk r : policyRisks)
        {
            if (!r.coverages.isEmpty())
            {
                // Denote privacy breach as removed if not selected.
                if (r.coverages[0].Type__c == RiskService.COVERAGE_CYBER && !this.quote.Selected_Packages_with_Privacy_Breach__c.contains(r.packageId))
                {
                    r.Coverages[0].Limit_Value__c = -1;
                }
                
                // Policy level risks only have one type of coverage, use the first one in the list.
                this.commlCoveragePolicy.add(new CSIO_CommlCoverage(r.coverages[0]));
            }
        }

        Map<String, String> esbSegment = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbSegment');
        Map<String, String> esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');
        Map<String, String> esbBillingMethodCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbBillingMethodCd');

        this.segment = esbSegment.get(ri.sgmtType);
        this.flavour = ri.getPackageType();
        this.rsaOperation = ri.getServiceType();
        
        this.billingMethodCd = esbBillingMethodCd.get(quote.Policy_Payment_Plan_Type__c);
        this.rsaPackage = this.quote.Retrieved_Package_Code__c;

        this.rsaContextId = esbContext.get(quote.Case__r.Offering_Project__c);

        this.userRegion = UserService.getRegion(UserInfo.getUserId())[0];
        this.companyCd = this.userRegion == REGION_PACIFIC ? COMPANYCD_CNS : COMPANYCD_RSA;
        this.yearsClaimsFree = this.quote.Claim_Free_Years__c != null ? Integer.valueOf(this.quote.Claim_Free_Years__c) : 0;
        
        this.quoteInfo = new CSIO_QuoteInfo(quote.Quote_Number__c);
        this.sfCaseNumber = quote.Case__r.CaseNumber; 
        
        if (this.quoteRiskRelationshipList != null)
        {
            this.additionalInterestList = new List<CSIO_AdditionalInterest>();
            for (Quote_Risk_Relationship__c quoteRiskRelationship : this.quoteRiskRelationshipList)
            {
                if(quoteRiskRelationship.Party_Type__c == RelationshipControllerExtension.PREMIER_FINANCIER){

                    System.assert(!(quoteRiskRelationship.Account__r.Id == null && quoteRiskRelationship.Contact__r.Id == null), 'Both contact and account should not be null');
                    // find the related account or contact
                    for (Account account : accountList)
                    {
                        if (quoteRiskRelationship.Account__r.Id == account.Id)
                        {
                            System.assert(quoteRiskRelationship.Account__r != null, 'Account should not be null');
                            additionalInterestList.add(new CSIO_AdditionalInterest(account, quoteRiskRelationship));
                            break;
                        }
                    }
                    // if we've got this far then its a contact
                    for (Contact contact : contactList)
                    {
                        if (quoteRiskRelationship.Contact__r.Id == contact.Id)
                        {
                            System.assert(quoteRiskRelationship.Contact__r != null, 'Contact should not be null');
                            System.assert(contact.LastName != null, 'Last Name should not be null');
                            //System.assert(contact.MailingStreet != null, 'Mailing street should not be null');
                            additionalInterestList.add(new CSIO_AdditionalInterest(contact, quoteRiskRelationship));
                        }
                    }
                }
            }
        }
    }
    //Adding a method to convert case in GETPOLICY scenario 
    public void convertCase() 
    {
        this.LOBCd = 'csio:1'; //Fix this
        this.languageCd = this.quote.Language__c != null ? this.quote.Language__c : UserService.getFormattedUserLanguage();
        
        if (ri.getUnderwriterId() != null)
        {
            this.underwriterId = ri.getUnderwriterId().FederationIdentifier;
        }
      
        this.rsaOperation = ri.getServiceType();
        system.debug('ri' + ri);
       
        Map<String, String> esbContext = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbContext');
        this.rsaContextId = esbContext.get(parentCase.Offering_Project__c);

        this.userRegion = UserService.getRegion(UserInfo.getUserId())[0];
        this.companyCd = this.userRegion == REGION_PACIFIC ? COMPANYCD_CNS : COMPANYCD_RSA;
        
        this.quoteInfo = new CSIO_QuoteInfo(null);
        this.sfCaseNumber = parentCase.CaseNumber;
        
    }
      
    public override String toString()
    {
        String xml = '';

        xml += '<CommlPolicy id="' + this.quote.Id + '">'; //Fix this
        //this gets called when GETRATE is called on Renewal/Amendment transactions 
        if (quote.ePolicy__c != null){
            xml += '<PolicyNumber>' + this.quote.ePolicy__c + '</PolicyNumber>';
        }
        
        xml += ' <LOBCd>' + this.LOBCd + '</LOBCd>';
        if (billingMethodCd != null && quote.ePolicy__c == null)
        {
            xml += ' <BillingMethodCd>' + this.billingMethodCd + '</BillingMethodCd>'; 
        }
        xml += ' <LanguageCd>' + this.languageCd + '</LanguageCd>';

        for (Loss l : this.loss)
        {
            xml += l;
        }
        if(!claims.isEmpty()){
            xml += ' <NumLosses>' + this.claims.size() + '</NumLosses>';
        }
        if (quote.Currently_Insured__c)
        {
            xml += this.otherOrPriorPolicy;
        }
        xml += this.quoteInfo;
        if (additionalInterestList != null) {
            for (CSIO_AdditionalInterest additionalInterest : additionalInterestList) {
                xml += additionalInterest.toString();
            }
        }
        if (this.underwriterId != null)
        {
            xml += ' <UnderwritingDecisionInfo>';
            xml += '  <UnderwriterId>' + this.underwriterId + '</UnderwriterId>';
            xml += ' </UnderwritingDecisionInfo>';
        }
        
        xml += ' <csio:CompanyCd>' + this.companyCd + '</csio:CompanyCd>'; // Fix for FP-3884
        
        
        xml += this.commlPolicySupplement;
        xml += this.commlCoverageClaims;
        for (CSIO_CommlCoverage ccp : this.commlCoveragePolicy)
        {
            xml += ccp;
        }
        
        xml += ' <rsa:Segment>' + this.segment + '</rsa:Segment>';
        if(this.rsaOperation == 'RAR' || this.rsaOperation == 'SQ' || this.rsaOperation == 'RAF' || this.rsaOperation == 'RAB'){
			xml += '<rsa:Package>'+this.rsaPackage+'</rsa:Package>';
        }
        xml += ' <rsa:Flavour>' + this.flavour + '</rsa:Flavour>';
        if (quote.Currently_Insured__c)
        {           
            xml += '<rsa:YearsClaimsFree>' + this.yearsClaimsFree + '</rsa:YearsClaimsFree>';
        }
        xml += ' <rsa:SalesforceCaseNumber>' + this.sfCaseNumber + '</rsa:SalesforceCaseNumber>'; 
        
        //This has been moved into InsuranceSvcRq as of Renewals and Amendments introduction 
       // xml += ' <rsa:ServiceOperationType>' + this.rsaOperation + '</rsa:ServiceOperationType>';
        //xml += ' <rsa:Context>' + this.rsaContextId + '</rsa:Context>';

        /*if (quote.ePolicy__c != null && this.ri.svcType == RequestInfo.ServiceType.FINALIZE_QUOTE){
            xml += '<rsa:TransactionReasonCode>' + this.quote.Reasons__c + '</rsa:TransactionReasonCode>';
        }*/
        xml += '</CommlPolicy>';
        
        return xml;
    }

    public class Loss
    {
        private Claim__c claim;
        private Quote__c quote;
        public String lossDt;
        public String lossCauseCd;
        public String csioCompanyCd;
        public LossPayment lossPayment;

        public Loss(Claim__c claim, Quote__c quote)
        {
            this.claim = claim;
            this.quote = quote;
            convert();
        }

        private void convert()
        {
            String month = (this.claim.Month__c.length() < 2) ? ('0' + this.claim.Month__c) : this.claim.Month__c;
            this.lossDt = this.claim.Year__c + '-' + month + '-' + '01'; // Defaulting to the first of the month was agreed upon by the business.
            // BZRD-116
            List<Claim_Types_And_Codes__c> listCT = [Select Id, Claim_Code__c, Claim_Type__c From Claim_Types_And_Codes__c];
            Map<String, Claim_Types_And_Codes__c> mapCT = new Map<String, Claim_Types_And_Codes__c>();
            for (Claim_Types_And_Codes__c tmp: listCT)
                mapCT.put(tmp.Claim_Type__c, tmp);
            if(!mapCT.isEmpty())
                this.lossCauseCd = mapCT.get(this.claim.Type_of_Claim__c).Claim_Code__c;
            this.csioCompanyCd = 'ABI'; //Fix this
            this.lossPayment = new LossPayment(this.claim);
        }

        public override String toString()
        {
            String xml = '';

            xml += '<Loss>';
            xml += ' <LossDt>' + this.lossDt + '</LossDt>';
            xml += ' <LossCauseCd>' + this.lossCauseCd + '</LossCauseCd>';
            xml += ' <csio:CompanyCd>' + this.csioCompanyCd + '</csio:CompanyCd>';
            xml += this.lossPayment;
            xml += '</Loss>';

            return xml;
        }
    }

    public class LossPayment
    {
        private Claim__c claim;

        public Integer amt;

        public LossPayment(Claim__c claim)
        {
            this.claim = claim;
            convert();
        }

        private void convert()
        {
            this.amt = (Integer) this.claim.Amount__c;
        }

        public override String toString()
        {
            String xml = '';

            xml += '<LossPayment>';
            xml += ' <TotalPaidAmt>';
            xml += '  <Amt>' + this.amt + '</Amt>';
            xml += ' </TotalPaidAmt>';
            xml += '</LossPayment>';

            return xml;
        }
    }

    public class OtherOrPriorPolicy
    {
        public String policyCd;
        public String csioCompanyCd;
        public List<Claim__c> claims;

        public OtherOrPriorPolicy()
        {
            convert();
        }
        
        public OtherOrPriorPolicy(List<Claim__c> claims)
        {
        	this.claims =claims; 
            convert();
        }     
        

        private void convert()
        {
            this.policyCd = 'Prior'; //Fix this
            this.csioCompanyCd = COMPANYCD_RSA;
        }

        public override String toString()
        {
            String xml = '';

            xml += '<OtherOrPriorPolicy>';
            xml += ' <PolicyCd>' + this.policyCd + '</PolicyCd>';
            xml += ' <csio:CompanyCd>' + this.csioCompanyCd + '</csio:CompanyCd>';
            xml += '</OtherOrPriorPolicy>';

            return xml;
        }
    }

    public class CommlPolicySupplement
    {
        private Quote__c quote;
        private RequestInfo ri;

        public Integer annualSalesAmt;

        public CommlPolicySupplement(Quote__c quote, RequestInfo ri)
        {
            this.quote = quote;
            this.ri = ri;
            convert();
        }

        private void convert()
        {
            this.annualSalesAmt = (Integer) (this.quote.Total_Revenue__c == null ? 0 : this.quote.Total_Revenue__c);
        }

        public override String toString()
        {
            String xml = '';

            xml += '<CommlPolicySupplement>';
            xml += ' <AnnualSalesAmt>';
            xml += '  <Amt>' + annualSalesAmt + '</Amt>';
            xml += ' </AnnualSalesAmt>';
            xml += new CreditOrSurcharge(this.quote, this.ri);
            xml += '</CommlPolicySupplement>';

            return xml;
        }
    }
    
    public class CreditOrSurcharge
    {
        private Quote__c quote;
        private RequestInfo ri;
        
        private Boolean includeDeviation;
        
        public Decimal formatPct;
        public String deviateReason;
        
        public CreditOrSurcharge(Quote__c quote, RequestInfo ri)
        {
            this.quote = quote;
            this.ri = ri;
            convert();
        }
        
        private void convert()
        {
            Map<String, String> esbDeviationReason = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbDeviationReason');
            Map<String, String> esbFlavor = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbFlavor');
            
            this.formatPct = this.quote.Calculated_Deviation__c.setScale(0);
            this.deviateReason = esbDeviationReason.get(this.quote.Deviation_Reason_s__c);
            
            //condition set because deviation should apply even if package is not selected for renewals and amendments 
            if(this.quote.ePolicy__c == null){
                this.includeDeviation = this.formatPct != 0 &&
                    this.quote.Selected_Packages_to_Deviate__c != null &&
                    this.quote.Selected_Packages_to_Deviate__c.contains(esbFlavor.get(ri.getPackageType()));
            }
            else{
                this.includeDeviation = this.formatPct != 0;
            }
        }
        
        public override String toString()
        {
            String xml = '';
            
            if (this.includeDeviation)
            {
                xml += '<CreditOrSurcharge>';
                xml += ' <NumericValue>';
                xml += '  <FormatPct>' + this.formatPct + '</FormatPct>';
                xml += ' </NumericValue>';
                xml += ' <rsa:DeviateReason>' + this.deviateReason + '</rsa:DeviateReason>';
                xml += '</CreditOrSurcharge>';
            }
            
            return xml;
        }
    }

    public class CommlCoverage
    {
        private List<Claim__c> claims;
        
        public String coverageCd;
        public Integer claimsMadeInd;

        public CommlCoverage(List<Claim__c> claims)
        {
            this.claims = claims;
            convert();
        }

        private void convert()
        {
            this.claimsMadeInd = this.claims.size() == 0 ? 0 : 1;
            this.coverageCd = 'csio:2'; //Fix this
        }

        public override String toString()
        {
            String xml = '';

            xml += '<CommlCoverage>';
            xml += ' <CoverageCd>' + this.coverageCd + '</CoverageCd>';
            xml += ' <CommlCoverageSupplement>';
            xml += '  <ClaimsMadeInfo>';
            xml += '   <ClaimsMadeInd>' + this.claimsMadeInd + '</ClaimsMadeInd>';
            xml += '  </ClaimsMadeInfo>';
            xml += ' </CommlCoverageSupplement>';
            xml += '</CommlCoverage>';

            return xml;
        }
    }
}