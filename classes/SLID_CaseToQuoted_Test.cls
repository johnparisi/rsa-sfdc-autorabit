/**
  * @author        Saumil Bapat
  * @date          11/16/2016
  * @description   Test class for SLID_UpdateQuoteVersion
*/
@isTest
private Class SLID_CaseToQuoted_Test {
    static testMethod void testUpdateStatusToQuoted()
    {
      //Set Integration Log Id
      SLID_TestDataFactory.initializeIntegrationLogId();
    
      //Set the source values on the record
      Quote_Version__c quoteVersion = SLID_TestDataFactory.createQuoteVersion();
      quoteVersion.Status__c = 'Quoted';
      update quoteVersion;
      case parentCase = [Select Id, Status, Recordtype.Name, (Select Id from Policy_Quotes__r where Active__c = true) from Case where Id = :quoteVersion.Case__c];

      //Set case status to referred
      parentCase.Status = 'Referred';
      parentCase.bkrCase_Subm_Stat_Rsn__c = 'Case referred';
      update parentCase;

      parentCase = [Select Id, Status, Recordtype.Name, (Select Id from Policy_Quotes__r where Active__c = true) from Case where id = :parentCase.Id];
      System.Debug('~~~parentCase: ' + parentCase);

      //Set status to not referred
      parentCase.Status = 'New';
      update parentCase;

      //Requery the quote version
      parentCase = [Select Id, Status, Recordtype.Name, (Select Id from Policy_Quotes__r where Active__c = true) from Case where id = :parentCase.Id];
      System.Debug('~~~parentCase: ' + parentCase);

      //Assert the quote version status has been updated
      System.Assert(parentCase.Status == 'Quoted', '~~~Case status has not been updated, status = ' + parentCase.Status);
    }
}