@isTest
private class UpdateFirmographics_test{
    private static testmethod void test() {

        
        List<Account> accounts = new List<Account>{
            new Account(Name = 'Test',BillingCountry = 'CANADA',BillingStreet='LineText,647 Coursol Rd',
                        BillingCity='Toronto',BillingState='test',BillingPostalCode='P2B0A9'),
            new Account(Name = 'Test2', BillingCountry = 'CANADA',Last_Firmographics_Update__c = system.today(),BillingStreet='LineText,647 Coursol Rd',
                        BillingCity='Toronto',BillingState='test',BillingPostalCode='P2B0A9',bkrAccount_DUNS__c='255293813'),
             new Account(Name = 'Test3', BillingCountry = 'CANADA',BillingStreet='LineText,647 Coursol Rd',
                        BillingCity='Toronto',BillingState='test',BillingPostalCode='P2B0A9',bkrAccount_DUNS__c='255293814')           
        };
        insert accounts;
        
         // Create a Broker to Work at the Brokerage Account
        Contact brokerCntc = new Contact();
        brokerCntc.FirstName= 'John';
        brokerCntc.LastName='Smith';
        brokerCntc.AccountId=accounts[2].Id;
        insert brokerCntc;
        
        // Create a Case from the Broker Contact
        Case c = new Case();
        c.AccountId = accounts[2].Id;
        c.ContactId = brokerCntc.Id;
       // c.bkrCase_Insured_Client__c=clientAcc.id;
        c.Status='New';
        c.Origin='Email';
        c.Type='Request for a Quote';
        insert c;
        
        Test.startTest();
          DNBCallout.dnbEntry fnt= new  DNBCallout.dnbEntry();
       
       
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        UpdateFirmographics controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[0]));
      /*  controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[2]));
        controller.checkLastUpdate();
        fnt.DUNSNumber='255293814';
        fnt.Company='test'; 
        controller.updateAccount();*/
        
        Boolean str=controller.renderResults;
        controller.checkLastUpdate();
        controller.searchDnB();
       /* //controller.selectRecord();
        fnt.DUNSNumber='255293814';
        fnt.Company='test'; */// Added by me becz the this variable is the use to give Account N
        controller.record = new DNBCallout.dnbEntry();
        controller.record.DUNSNumber='255293814';
        controller.record.Company='Test';
        
        
        ApexPages.currentPage().getParameters().put('duns','255293814');
        controller.matchingAccount =accounts[1]; 
        controller.matchingAccounts =accounts; 
        controller.DUNS = '255293814';
        //controller.checkLastUpdate();
        controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[1]));
        controller.record = new DNBCallout.dnbEntry();
        controller.record.DUNSNumber='255293814';
        controller.record.Company='Test';
        ApexPages.currentPage().getParameters().put('duns','255293814');
        controller.matchingAccount =accounts[1]; 
        controller.matchingAccounts =accounts; 
        controller.DUNS = '255293814';
        controller.checkLastUpdate();
        
        
        
       /* accounts[2].bkrAccount_DUNS__c='';
        update accounts;
        controller.searchDnB();
        
        controller.updateAccount();*/
        
        Test.stopTest();
    }
    private static testmethod void test2() {

        
        List<Account> accounts = new List<Account>{
            new Account(Name = 'Test',BillingCountry = 'US',BillingStreet='LineText,647 Coursol Rd',
                        BillingCity='Toronto',BillingState='test',BillingPostalCode='P2B0A9'),
            new Account(Name = 'Test2', BillingCountry = 'CANADA',Last_Firmographics_Update__c = system.today(),BillingStreet='LineText,647 Coursol Rd',
                        BillingCity='Toronto',BillingState='test',BillingPostalCode='P2B0A9',bkrAccount_DUNS__c='255293813'),
             new Account(Name = 'Test3', BillingCountry = 'CANADA',BillingStreet='LineText,647 Coursol Rd',
                        BillingCity='Toronto',BillingState='test',BillingPostalCode='P2B0A9',bkrAccount_DUNS__c='255293814')           
        };
        insert accounts;
        
         // Create a Broker to Work at the Brokerage Account
        Contact brokerCntc = new Contact();
        brokerCntc.FirstName= 'John';
        brokerCntc.LastName='Smith';
        brokerCntc.AccountId=accounts[2].Id;
        insert brokerCntc;
        
        // Create a Case from the Broker Contact
        Case c = new Case();
        c.AccountId = accounts[2].Id;
        c.ContactId = brokerCntc.Id;
       // c.bkrCase_Insured_Client__c=clientAcc.id;
        c.Status='New';
        c.Origin='Email';
        c.Type='Request for a Quote';
        insert c;
        
        Test.startTest();
          DNBCallout.dnbEntry fnt= new  DNBCallout.dnbEntry();
       
       
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        UpdateFirmographics controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[0]));
        controller = new UpdateFirmographics(new ApexPages.StandardController(accounts[2]));
        controller.checkLastUpdate();
        controller.record = new DNBCallout.dnbEntry();
        controller.record.DUNSNumber='255293814';
        controller.record.Company='Test'; 
        controller.updateAccount();
        
        
        
        
        
       /* accounts[2].bkrAccount_DUNS__c='';
        update accounts;
        controller.searchDnB();
        
        controller.updateAccount();*/
        
        Test.stopTest();
    }
}