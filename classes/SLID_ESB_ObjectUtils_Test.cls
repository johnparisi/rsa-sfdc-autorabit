@isTest
private Class SLID_ESB_ObjectUtils_Test {

    static testMethod void testUpsertGenericSObjectList_CaseComment(){
        Case objCase = SLID_TestDataFactory.createTestCase();
        
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SobjectType ccType = gd.get('CaseComment');
        SObject objCaseComment = ccType.newSObject();
        objCaseComment.put('CommentBody','Test Comments');
        objCaseComment.put('ParentId',objCase.Id);
        
        List<SObject> Records = new List<SObject>();
        Records.add(objCaseComment);
        
        Schema.SObjectType objectType = Schema.CaseComment.getSObjectType();
        
        List<Database.upsertResult> listGenericSObjectList;
        
        test.startTest();
            listGenericSObjectList = SLID_ESB_ObjectUtils.upsertGenericSObjectList(objectType,null,Records);
        test.stopTest();
    }
    
    static testMethod void testUpsertGenericSObjectList_Quote_Version(){
        Case objCase = SLID_TestDataFactory.createTestCase();
        
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SobjectType ccType = gd.get('Quote_Version__c');
        SObject objRecord = ccType.newSObject();
        objRecord.put('Quote_Number__c','QPC-87264');
        objRecord.put('Name','QPC-87264');
        objRecord.put('Case__c',objCase.Id);
        
        List<SObject> Records = new List<SObject>();
        Records.add(objRecord);
        
        Schema.SObjectType objectType = Schema.Quote_Version__c.getSObjectType();
        
        Schema.DescribeFieldResult F = Quote_Version__c.Quote_Number__c.getDescribe();
        Schema.sObjectField sobjectField = F.getSObjectField();
        
        List<Database.upsertResult> listGenericSObjectList;
        
        test.startTest();
            listGenericSObjectList = SLID_ESB_ObjectUtils.upsertGenericSObjectList(objectType,sobjectField,Records);
        test.stopTest();
    }
    
    static testMethod void testUpsertGenericSObjectList_Quote_Version_NullExtId(){
        Case objCase = SLID_TestDataFactory.createTestCase();
        
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SobjectType ccType = gd.get('Quote_Version__c');
        SObject objRecord = ccType.newSObject();
        objRecord.put('Quote_Number__c','QPC-87264');
        objRecord.put('Name','QPC-87264');
        objRecord.put('Case__c',objCase.Id);
        
        List<SObject> Records = new List<SObject>();
        Records.add(objRecord);
        
        Schema.SObjectType objectType = Schema.Quote_Version__c.getSObjectType();
        
        List<Database.upsertResult> listGenericSObjectList;
        
        test.startTest();
            listGenericSObjectList = SLID_ESB_ObjectUtils.upsertGenericSObjectList(objectType,null,Records);
        test.stopTest();
    }
}