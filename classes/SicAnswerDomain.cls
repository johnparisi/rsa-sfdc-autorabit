/**
* SicAnswerDomain
* Author: James Lee
* Date: April 28 2016
* 
* The object domain is where the real work happens, this is where we will apply the actual business logic. 
* We have the main Object Domain which should handle all common processing. 
* Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
* Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
* just implement logic in the appopriate method. 
*/
public with sharing class SicAnswerDomain
{
    public static Boolean isActive = true;
    
    private SicAnswerService service = new SicAnswerService();
    
    public void beforeInsert(List<Sic_Answer__c> records)
    {
    }
    
    public void beforeUpdate(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
    {
    }
    
    public void afterInsert(List<Sic_Answer__c> records)
    {
    }
    
    public void afterUpdate(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
    {
    }
    
    //SicAnswers
    public class SMEQDomain
    {
        private SicAnswerService service = new SicAnswerService();
        public void beforeInsert(List<Sic_Answer__c> records)
        {
            if (isActive)
            {
                service.setCanadianValueFromAccountRevenue(records);
            }
        }
        
        public void beforeUpdate(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
        {
            
        }
        
        public void afterInsert(List<Sic_Answer__c> records)
        {
            if (isActive)
            {
                service.updateDefaultCoverageLimits(records, null);
                service.updateTotalRevenue(records, null);
                service.updateNumberOfUnits(records, null);
            }
        }
        
        public void afterUpdate(List<Sic_Answer__c> records, Map<Id, Sic_Answer__c> oldRecords)
        {
            if (isActive)
            {
                service.updateDefaultCoverageLimits(records, oldRecords);
                service.updateTotalRevenue(records, oldRecords);
                service.updateNumberOfUnits(records, oldRecords);
            }
        }
    }
}