/**
 * CSIORequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to store all common methods required in the service 
 * specific factory classes that generate the xml requests for the services
 */
public interface CSIORequestFactory {
    String buildXMLRequest();
}