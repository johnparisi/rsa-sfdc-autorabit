@isTest
private class bkrAccount_AccountDetailsPDFCtlr_Test {
	private final static String RecordTypeName = 'Brokerage (MM-Level)';
	private final static Id BRK_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecordTypeName).getRecordTypeId();
	private final static Id COMP_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Competitor').getRecordTypeId();

	static testMethod void test1() {
	
		List<Account> accounts = new List<Account>();

		//Broker Profile Account
		Account a1 = new Account();
		a1.Name = 'CORNELL INSURANCE BROKERS LTD.';
		a1.AccountNumber = '0110261111';
		a1.bkrAccount_Status__c = 'Active';
		a1.Phone = '(905) 909-8978';
		a1.bkrAccount_InfoEmail__c = 'cornellcontact@gmail.com';
		a1.Website = 'http://www.cornellinsurance.com';
		a1.BillingStreet = '132 Renfrew Drive';
		a1.BillingCity = 'Markham';
		a1.BillingState = 'Ontario';
		a1.BillingPostalCode = 'L3R 0C8';
		a1.BillingCountry = 'Canada';
		a1.RecordTypeId = BRK_RT;

		a1.bkrAccount_ExecutiveBriefing__c = 'This brokerage is interested in Charity events/NGO.';
		

		accounts.add(a1);

		//Competitor Account 
		Account a2 = new Account();
		a2.Name = 'Aviva';
		a2.AnnualRevenue = 1000000000;
		a2.NumberOfEmployees = 90;
		a2.RecordTypeId = COMP_RT;

		accounts.add(a2);

		insert accounts;

		List<bkrCompVol__c> competitorVolumes = new List<bkrCompVol__c>();

		bkrCompVol__c b1 = new bkrCompVol__c();
		b1.bkrCompVol_CompetitorName__c = a2.Id;
		b1.bkrCompVol_BrokerageName__c = a1.Id;
		b1.bkrCompVol_TotalVolume__c = 10000000;
		b1.bkrCompVol_CommercialPercent__c = 50;

		b1.PI_Brokerage_Volume__c = 5;
		b1.CI_Brokerage_Volume__c = 25;
		

		competitorVolumes.add(b1);

		insert competitorVolumes;

		Test.startTest();

		ApexPages.StandardController sc = new ApexPages.StandardController(a1);
        bkrAccount_AccountDetailsPDFCtlr bkrAccountCtlr = new bkrAccount_AccountDetailsPDFCtlr(sc);
        
        PageReference pageRef = Page.bkrAccount_AccountDetailsPDF;
        pageRef.getParameters().put('id', String.valueOf(a1.Id));
        Test.setCurrentPage(pageRef);

		System.assert(bkrAccountCtlr.competitorVolumesCIList.size() > 0);

		Test.stopTest();


	}
}