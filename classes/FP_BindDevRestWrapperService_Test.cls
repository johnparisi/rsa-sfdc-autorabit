@isTest
public class FP_BindDevRestWrapperService_Test{

    static testmethod void myTest1(){
    
        Quote__c quote = new Quote__C();
        List<Risk__c> risks = new List<Risk__c>();
        List<BindQuoteRiskContactAccountWrapper> bindQuoteRiskContactAccountWrappers = new List<BindQuoteRiskContactAccountWrapper>();       
        FP_BindDevRestWrapperService.saveBindState(quote, risks, bindQuoteRiskContactAccountWrappers);
        
        FP_BindDevRestWrapperService.bindQuote(quote, risks, bindQuoteRiskContactAccountWrappers);
    
    
    }
}