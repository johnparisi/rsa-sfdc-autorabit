//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

@IsTest
public class DNBAppend_Test {
	
	static testMethod void testParse() {
		String json = '{  '+
		'   \"responseCode\":\"OK\",'+
		'   \"DNBResponse\":{  '+
		'      \"OrderProductResponse\":{  '+
		'         \"@ServiceVersionNumber\":\"5.0\",'+
		'         \"TransactionDetail\":{  '+
		'            \"ServiceTransactionID\":\"Id-c49bca5895242e0060263f006f966c13-2\",'+
		'            \"TransactionTimestamp\":\"2017-03-16T10:05:58.412-04:00\"'+
		'         },'+
		'         \"TransactionResult\":{  '+
		'            \"SeverityText\":\"Information\",'+
		'            \"ResultID\":\"CM000\",'+
		'            \"ResultText\":\"Success\"'+
		'         },'+
		'         \"OrderProductResponseDetail\":{  '+
		'            \"InquiryDetail\":{  '+
		'               \"DUNSNumber\":\"251063236\",'+
		'               \"CountryISOAlpha2Code\":\"CA\"'+
		'            },'+
		'            \"Product\":{  '+
		'               \"DNBProductID\":\"DCP_ENH\",'+
		'               \"Organization\":{  '+
		'                  \"SubjectHeader\":{  '+
		'                     \"DUNSNumber\":\"251063236\",'+
		'                     \"OrganizationSummaryText\":\"Eating places\",'+
		'                     \"LastUpdateDate\":{  '+
		'                        \"$\":\"2012-12-18\"'+
		'                     },'+
		'                     \"TransferDUNSNumberRegistration\":[  '+
		'                        {  '+
		'                           \"TransferDate\":{  '+
		'                              \"$\":\"2007-07-27\"'+
		'                           },'+
		'                           \"TransferredFromDUNSNumber\":\"202347899\",'+
		'                           \"TransferredToDUNSNumber\":\"251063236\",'+
		'                           \"TransferReasonText\":[  '+
		'                              {  '+
		'                                 \"@DNBCodeValue\":420,'+
		'                                 \"$\":\"Duplicate Record Transferred\"'+
		'                              }'+
		'                           ]'+
		'                        }'+
		'                     ],'+
		'                     \"MarketabilityIndicator\":true'+
		'                  },'+
		'                  \"Telecommunication\":{  '+
		'                     \"FacsimileNumber\":[  '+
		'                        {  '+
		'                           \"TelecommunicationNumber\":\"(709) 754-3356\",'+
		'                           \"InternationalDialingCode\":\"1\"'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"Location\":{  '+
		'                     \"PrimaryAddress\":[  '+
		'                        {  '+
		'                           \"StreetAddressLine\":[  '+
		'                              {  '+
		'                                 \"LineText\":\"386 Stavanger Dr Suite 900\"'+
		'                              }'+
		'                           ],'+
		'                           \"PrimaryTownName\":\"ST. JOHN\'S\",'+
		'                           \"CountryISOAlpha2Code\":\"CA\",'+
		'                           \"TerritoryAbbreviatedName\":\"NL\",'+
		'                           \"PostalCode\":\"A1A 5M9\",'+
		'                           \"PremisesDetail\":[  '+
		'                              {  '+
		'                                 \"PremisesAreaMeasurement\":2637,'+
		'                                 \"PremisesAreaMeasurementReliabilityText\":{  '+
		'                                    \"@DNBCodeValue\":9093,'+
		'                                    \"$\":\"Estimated\"'+
		'                                 },'+
		'                                 \"PremisesAreaMeasurementUnitText\":{  '+
		'                                    \"@DNBCodeValue\":3848,'+
		'                                    \"$\":\"Square Feet\"'+
		'                                 }'+
		'                              }'+
		'                           ],'+
		'                           \"AddressUsageTenureDetail\":[  '+
		'                              {  '+
		'                                 \"TenureTypeText\":{  '+
		'                                    \"@DNBCodeValue\":1129,'+
		'                                    \"$\":\"Rents\"'+
		'                                 }'+
		'                              }'+
		'                           ],'+
		'                           \"TerritoryOfficialName\":\"NEWFOUNDLAND\",'+
		'                           \"CountryGroupName\":\"North America\",'+
		'                           \"LatitudeMeasurement\":47.614477,'+
		'                           \"LongitudeMeasurement\":-52.707871,'+
		'                           \"GeographicalPrecisionText\":{  '+
		'                              \"@DNBCodeValue\":30257,'+
		'                              \"$\":\"Street Address Centroid\"'+
		'                           },'+
		'                           \"UndeliverableIndicator\":false,'+
		'                           \"RegisteredAddressIndicator\":false'+
		'                        }'+
		'                     ],'+
		'                     \"MailingAddress\":[  '+
		'                        {  '+
		'                           \"CountryISOAlpha2Code\":\"CA\",'+
		'                           \"UndeliverableIndicator\":false'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"Financial\":{  '+
		'                     \"KeyFinancialFiguresOverview\":[  '+
		'                        {  '+
		'                           \"StatementHeaderDetails\":{  '+
		'                              \"FinancialPeriodDuration\":\"P1Y\"'+
		'                           },'+
		'                           \"SalesRevenueAmount\":[  '+
		'                              {  '+
		'                                 \"@CurrencyISOAlpha3Code\":\"CAD\",'+
		'                                 \"@ReliabilityText\":\"Modelled\",'+
		'                                 \"@UnitOfSize\":\"SingleUnits\",'+
		'                                 \"$\":6200000'+
		'                              }'+
		'                           ]'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"OrganizationName\":{  '+
		'                     \"OrganizationPrimaryName\":[  '+
		'                        {  '+
		'                           \"OrganizationName\":{  '+
		'                              \"$\":\"Harmonie Foods Limited\"'+
		'                           }'+
		'                        }'+
		'                     ],'+
		'                     \"TradeStyleName\":[  '+
		'                        {  '+
		'                           \"OrganizationName\":{  '+
		'                              \"$\":\"Tim Hortons\"'+
		'                           }'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"OrganizationDetail\":{  '+
		'                     \"FamilyTreeMemberRole\":[  '+
		'                        {  '+
		'                           \"FamilyTreeMemberRoleText\":{  '+
		'                              \"@DNBCodeValue\":12775,'+
		'                              \"$\":\"Global Ultimate\"'+
		'                           }'+
		'                        },'+
		'                        {  '+
		'                           \"FamilyTreeMemberRoleText\":{  '+
		'                              \"@DNBCodeValue\":12774,'+
		'                              \"$\":\"Domestic Ultimate\"'+
		'                           }'+
		'                        },'+
		'                        {  '+
		'                           \"FamilyTreeMemberRoleText\":{  '+
		'                              \"@DNBCodeValue\":12771,'+
		'                              \"$\":\"Headquarters\"'+
		'                           }'+
		'                        }'+
		'                     ],'+
		'                     \"StandaloneOrganizationIndicator\":false,'+
		'                     \"ControlOwnershipDate\":{  '+
		'                        \"$\":\"1986\"'+
		'                     },'+
		'                     \"ControlOwnershipTypeText\":{  '+
		'                        \"@DNBCodeValue\":0,'+
		'                        \"$\":\"Unknown\"'+
		'                     },'+
		'                     \"BoneyardOrganizationIndicator\":false,'+
		'                     \"OperatingStatusText\":{  '+
		'                        \"@DNBCodeValue\":9074,'+
		'                        \"$\":\"Active\"'+
		'                     },'+
		'                     \"OrganizationStartYear\":\"1986\"'+
		'                  },'+
		'                  \"RegisteredDetail\":{  '+
		'                     \"LegalFormDetails\":{  '+
		'                        \"LegalFormText\":{  '+
		'                           \"@DNBCodeValue\":451,'+
		'                           \"$\":\"Corporation\"'+
		'                        }'+
		'                     },'+
		'                     \"IncorporationYear\":\"1986\"'+
		'                  },'+
		'                  \"IndustryCode\":{  '+
		'                     \"IndustryCode\":[  '+
		'                        {  '+
		'                           \"@DNBCodeValue\":25838,'+
		'                           \"@TypeText\":\"D&B Hoovers Industry Code\",'+
		'                           \"IndustryCode\":{  '+
		'                              \"$\":\"1837\"'+
		'                           },'+
		'                           \"IndustryCodeDescription\":[  '+
		'                              {  '+
		'                                 \"@LanguageCode\":39,'+
		'                                 \"@IndustryCodeDescriptionLengthCode\":9120,'+
		'                                 \"$\":\"Coffee Shops\"'+
		'                              }'+
		'                           ],'+
		'                           \"DisplaySequence\":1'+
		'                        },'+
		'                        {  '+
		'                           \"@DNBCodeValue\":700,'+
		'                           \"@TypeText\":\"NAICS\",'+
		'                           \"IndustryCode\":{  '+
		'                              \"$\":\"722513\"'+
		'                           },'+
		'                           \"DisplaySequence\":1'+
		'                        },'+
		'                        {  '+
		'                           \"@DNBCodeValue\":24664,'+
		'                           \"@TypeText\":\"North American Industry Classification System 2012\",'+
		'                           \"IndustryCode\":{  '+
		'                              \"$\":\"722513\"'+
		'                           },'+
		'                           \"IndustryCodeDescription\":[  '+
		'                              {  '+
		'                                 \"@LanguageCode\":39,'+
		'                                 \"@IndustryCodeDescriptionLengthCode\":9120,'+
		'                                 \"$\":\"Limited-Service Restaurants\"'+
		'                              }'+
		'                           ],'+
		'                           \"DisplaySequence\":1'+
		'                        },'+
		'                        {  '+
		'                           \"@DNBCodeValue\":21182,'+
		'                           \"@TypeText\":\"UK SIC 2003\",'+
		'                           \"IndustryCode\":{  '+
		'                              \"$\":\"55.300\"'+
		'                           },'+
		'                           \"IndustryCodeDescription\":[  '+
		'                              {  '+
		'                                 \"@LanguageCode\":39,'+
		'                                 \"@IndustryCodeDescriptionLengthCode\":9120,'+
		'                                 \"$\":\"Restaurants\"'+
		'                              }'+
		'                           ],'+
		'                           \"DisplaySequence\":1'+
		'                        },'+
		'                        {  '+
		'                           \"@DNBCodeValue\":3599,'+
		'                           \"@TypeText\":\"D&B Standard Industry Code\",'+
		'                           \"IndustryCode\":{  '+
		'                              \"$\":\"58120304\"'+
		'                           },'+
		'                           \"IndustryCodeDescription\":[  '+
		'                              {  '+
		'                                 \"@LanguageCode\":39,'+
		'                                 \"@IndustryCodeDescriptionLengthCode\":2121,'+
		'                                 \"$\":\"COFFEE SHOP\"'+
		'                              }'+
		'                           ],'+
		'                           \"DisplaySequence\":1'+
		'                        },'+
		'                        {  '+
		'                           \"@DNBCodeValue\":399,'+
		'                           \"@TypeText\":\"US Standard Industry Code 1987 - 4 digit\",'+
		'                           \"IndustryCode\":{  '+
		'                              \"$\":\"5812\"'+
		'                           },'+
		'                           \"IndustryCodeDescription\":[  '+
		'                              {  '+
		'                                 \"@LanguageCode\":39,'+
		'                                 \"@IndustryCodeDescriptionLengthCode\":1441,'+
		'                                 \"$\":\"Eating place\"'+
		'                              }'+
		'                           ],'+
		'                           \"DisplaySequence\":1'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"ActivitiesAndOperations\":{  '+
		'                     \"LineOfBusinessDetails\":[  '+
		'                        {  '+
		'                           \"LineOfBusinessDescription\":{  '+
		'                              \"$\":\"Eating places\"'+
		'                           }'+
		'                        }'+
		'                     ],'+
		'                     \"PrimarySICCategoryValue\":\"G\"'+
		'                  },'+
		'                  \"EmployeeFigures\":{  '+
		'                     \"IndividualEntityEmployeeDetails\":{  '+
		'                        \"TotalEmployeeQuantity\":4,'+
		'                        \"ReliabilityText\":{  '+
		'                           \"@DNBCodeValue\":9093,'+
		'                           \"$\":\"Estimated\"'+
		'                        }'+
		'                     },'+
		'                     \"ConsolidatedEmployeeDetails\":{  '+
		'                        \"TotalEmployeeQuantity\":150,'+
		'                        \"EmployeeCategoryDetails\":[  '+
		'                           {  '+
		'                              \"EmployeeBasisText\":{  '+
		'                                 \"@DNBCodeValue\":9064,'+
		'                                 \"$\":\"Principals\"'+
		'                              }'+
		'                           }'+
		'                        ],'+
		'                        \"ReliabilityText\":{  '+
		'                           \"@DNBCodeValue\":9093,'+
		'                           \"$\":\"Estimated\"'+
		'                        }'+
		'                     }'+
		'                  },'+
		'                  \"Linkage\":{  '+
		'                     \"LinkageSummary\":{  '+
		'                        \"GlobalUltimateFamilyTreeLinkageCount\":3'+
		'                     },'+
		'                     \"GlobalUltimateOrganization\":{  '+
		'                        \"DUNSNumber\":\"251063236\",'+
		'                        \"OrganizationPrimaryName\":[  '+
		'                           {  '+
		'                              \"OrganizationName\":{  '+
		'                                 \"$\":\"Harmonie Foods Limited\"'+
		'                              }'+
		'                           }'+
		'                        ],'+
		'                        \"PrimaryAddress\":[  '+
		'                           {  '+
		'                              \"StreetAddressLine\":[  '+
		'                                 {  '+
		'                                    \"LineText\":\"386 Stavanger Dr Suite 900\"'+
		'                                 }'+
		'                              ],'+
		'                              \"PrimaryTownName\":\"St. John\'s\",'+
		'                              \"CountryISOAlpha2Code\":\"CA\",'+
		'                              \"TerritoryAbbreviatedName\":\"NL\",'+
		'                              \"PostalCode\":\"A1A 5M9\",'+
		'                              \"TerritoryOfficialName\":\"NEWFOUNDLAND\"'+
		'                           }'+
		'                        ]'+
		'                     },'+
		'                     \"DomesticUltimateOrganization\":{  '+
		'                        \"DUNSNumber\":\"251063236\",'+
		'                        \"OrganizationPrimaryName\":[  '+
		'                           {  '+
		'                              \"OrganizationName\":{  '+
		'                                 \"$\":\"Harmonie Foods Limited\"'+
		'                              }'+
		'                           }'+
		'                        ],'+
		'                        \"PrimaryAddress\":[  '+
		'                           {  '+
		'                              \"StreetAddressLine\":[  '+
		'                                 {  '+
		'                                    \"LineText\":\"386 Stavanger Dr Suite 900\"'+
		'                                 }'+
		'                              ],'+
		'                              \"PrimaryTownName\":\"St. John\'s\",'+
		'                              \"CountryISOAlpha2Code\":\"CA\",'+
		'                              \"TerritoryAbbreviatedName\":\"NL\",'+
		'                              \"PostalCode\":\"A1A 5M9\",'+
		'                              \"TerritoryOfficialName\":\"NEWFOUNDLAND\"'+
		'                           }'+
		'                        ]'+
		'                     }'+
		'                  },'+
		'                  \"Assessment\":{  '+
		'                     \"CommercialCreditScore\":[  '+
		'                        {  '+
		'                           \"MarketingRiskClassText\":{  '+
		'                              \"@DNBCodeValue\":10925,'+
		'                              \"$\":\"Low\"'+
		'                           }'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"PrincipalsAndManagement\":{  '+
		'                     \"CurrentPrincipal\":[  '+
		'                        {  '+
		'                           \"PrincipalName\":{  '+
		'                              \"@type\":\"fir:IndividualNameType\",'+
		'                              \"FullName\":\"Ida Pike\"'+
		'                           },'+
		'                           \"JobTitle\":[  '+
		'                              {  '+
		'                                 \"JobTitleText\":{  '+
		'                                    \"$\":\"President & Board Member\"'+
		'                                 }'+
		'                              }'+
		'                           ],'+
		'                           \"PrincipalIdentificationNumberDetail\":[  '+
		'                              {  '+
		'                                 \"@DNBCodeValue\":24215,'+
		'                                 \"@TypeText\":\"Professional Contact Identifier\",'+
		'                                 \"PrincipalIdentificationNumber\":\"2510632361\"'+
		'                              }'+
		'                           ]'+
		'                        }'+
		'                     ]'+
		'                  },'+
		'                  \"SocioEconomicIdentification\":{  '+
		'                     \"MinorityOwnedIndicator\":false,'+
		'                     \"SmallBusinessIndicator\":false,'+
		'                     \"OwnershipEthnicity\":[  '+
		'                        {  '+
		'                           \"EthnicityTypeText\":{  '+
		'                              \"@DNBCodeValue\":0,'+
		'                              \"$\":\"Unknown\"'+
		'                           }'+
		'                        }'+
		'                     ]'+
		'                  }'+
		'               }'+
		'            }'+
		'         }'+
		'      }'+
		'   }'+
		'}';
		dnbAppend obj = dnbAppend.parse(json);
		System.assert(obj != null);
	}
}