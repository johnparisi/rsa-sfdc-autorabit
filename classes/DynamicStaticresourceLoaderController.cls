public class DynamicStaticresourceLoaderController {
    public String resourcename  {get;set;}
    public String langParam     {get;set;}
    
    public void setResourceName() {
        if(resourcename != 'resource1')
            resourcename = 'resource1';
        else
            resourcename = 'resource2';
            
        langParam = 'Multi-Language';
    }
}