/**
Single convenience utility class to manage handling session information.
*/

public with sharing class RA_SessionManagementUtil {
	
	public static void removeSession(Id caseId) {
		Id userId = UserInfo.getUserId();
		Session_Information__c sessionInformation = getSessionInformation(userId, caseId);
		delete sessionInformation;
	}
	
	public static void createSession(Case caseRecord, String sessionId) {
        List <Session_Information__c> existingSession = [Select id FROM Session_Information__c WHERE case__c = :caseRecord.Id and user__c = :userInfo.getUserId()];
        if (existingSession.isEmpty()){
            Session_Information__c session = new Session_Information__c();
            session.case__c = caseRecord.Id;
            session.session_id__c = sessionId;
            session.user__c = userInfo.getUserId();
            System.debug('#### creating a session for case: ' + caseRecord.Id + ' with session id ' + sessionId + ' and user ' + userInfo.getUserId());
        	upsert session;	
        }
	}
	
	public static String getSession(Id caseID) {
		return getSessionInformation(userInfo.getUserId(), caseID).session_id__c;
	}
	
	private static Session_Information__c getSessionInformation(Id userId, Id caseId ) {
		List<Session_Information__c> sessionInformationList = [SELECT id, Case__c, Session_Id__c, User__c FROM Session_Information__c WHERE Case__c = :caseId and User__c = :UserInfo.getUserId()];
		System.assert(!(sessionInformationList.size() == 0) || !(sessionInformationList.size() > 1), new RSA_MissingSessionException('Should be a single session record for userId ' + userID + ' caseId' + caseId + ', found:' + sessionInformationList));
        if (sessionInformationList.size() != 0){
	        return sessionInformationList[0];
        }
        else{
            return new Session_Information__c();
        }
    }
}