@isTest
public class SMEQ_FinalizeQuoteRestService_Test
{   
	/**
     * Method to test save page state with valid parameters
     * 1. Assert response is not null
     * 2. Assert response values  are set on response
     * 3. Assert response code is ok.
     */
    @isTest
    static void testFinalizeQuoteWithValidParam()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            //create mock case and quote
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_testDataGenerator.getOpenQuote(c);
            
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/savePageState';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_FinalizeQuoteRequestModel finalizeQuoteReq = buildMockFinalizeQuoteRequest(aQuote.Id);
            SMEQ_FinalizeQuoteResponseModel response = SMEQ_FinalizeQuoteRestService.saveFinalizedQuote(finalizeQuoteReq);
            Quote__c updatedQuote = [
                SELECT Id, Package_Type_Selected__c, Status__c
                FROM Quote__c
                WHERE ID=:aQuote.ID
                LIMIT 1
            ];
            System.assertNotEquals(null, response);
            System.assertNotEquals(null, response.finalizeDate);
            System.assertNotEquals(null, response.ePolicyNumber);
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, response.responseCode);
        }
    }

    /**
     * Method to test save page state with invalid parameters
     * 1. Assert response is not null
     * 2. Assert response values  are set on response
     * 3. Assert response code is ok.
     * 
     */
    @isTest
    static void testFinalizeQuoteWithInValidQuoteID()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            SMEQ_TestDataGenerator.initializeSMEMappings();
            
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            //create mock case and quote
            Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
            Quote__c aQuote = SMEQ_testDataGenerator.getOpenQuote(c);
            
            //do request
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/savePageState';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_FinalizeQuoteRequestModel finalizeQuoteReq = buildMockFinalizeQuoteRequest(null);
            SMEQ_FinalizeQuoteResponseModel response = SMEQ_FinalizeQuoteRestService.saveFinalizedQuote(finalizeQuoteReq);
            Quote__c updatedQuote = [
                SELECT Id, Package_Type_Selected__c, Status__c
                FROM Quote__c
                WHERE ID = :aQuote.ID
                LIMIT 1
            ];
            System.assertNotEquals(null, response);
            System.debug('response finalize quote on null is ' + response.finalizeDate);
            System.assertNotEquals(null, response.finalizeDate); //WILL NEED TO BE MODIFIED WHEN RESPONSE IS NO LONGER MOCK
            System.assertNotEquals(null, response.ePolicyNumber); //WILL NEED TO BE MODIFIED WHEN RESPONSE IS NO LONGER MOCK
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, response.responseCode);
        }
    }
    
   	//use to populate mock finalize quote Request
   	public static SMEQ_FinalizeQuoteRequestModel buildMockFinalizeQuoteRequest(String quoteID)
	{
		SMEQ_FinalizeQuoteRequestModel mockRequest = new SMEQ_FinalizeQuoteRequestModel();
        mockRequest.quoteID = quoteID;
        mockRequest.packageID = 'P2';
        return mockRequest;
	}
}