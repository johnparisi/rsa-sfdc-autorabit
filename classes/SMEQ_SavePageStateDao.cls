public without sharing class SMEQ_SavePageStateDao
{

	//Broker Application Stages - Map to a page number (1-5) on the front-end
    private static final String GETTING_STARTED = 'Getting Started';
    private static final String ABOUT_THE_BUSINESS = 'About the Business';
    private static final String CUSTOMIZE_QUOTE = 'Customize Quote';
    private static final String REVIEW ='Review';
    private static final String BUY ='Buy';     
    private static final String ISSUED ='Issued';  
    //Quote Status
    private static final String IN_PROGRESS = 'In-Progress'; //Geting started About Business and Customize Quote always maps to In-Progress  
    private static final String FINALIZED = 'Finalized';
    private static final String BOUND= 'Bind';
    private static final String CLOSED= 'Policy Issued – Closed';
    private static final String NEW_QUOTE = 'New';


    /**
     * Saves the Last Visited Page Number on a specified Quote 
     * @param  SMEQ_SavePageStateRequestModel request
     * @return SMEQ_SavePageStateResponseModel 
     */
    @TestVisible 
    public SMEQ_SavePageStateResponseModel saveApplicationStageOnQuote(SMEQ_SavePageStateRequestModel request)
    {
        SMEQ_SavePageStateResponseModel response = new SMEQ_SavePageStateResponseModel ();

        if(request.quoteID !=null && request.lastVisitedPageNum !=null)
        {

        	List<Quote__c> quoteList = [SELECT Id FROM Quote__c WHERE id =:request.quoteID];
        	Map<String,String> applicationStageLookup = new Map<String,String>();
            applicationStageLookup = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Broker_Application_Stage__c');
        	
            String applicationPageNumber = String.valueOf(request.lastVisitedPageNum);
        	String applicationStage = applicationStageLookup.get(applicationPageNumber); //returns null if invalid page number is sent
           
	        if(!quoteList.isEmpty() && applicationStage!=null)
	        {
	            Quote__c aQuote = quoteList[0];
	            aQuote.Status__c = getQuoteStatus(applicationStage);
                if(applicationStage == GETTING_STARTED || applicationStage == ABOUT_THE_BUSINESS || applicationStage == CUSTOMIZE_QUOTE){
                    aQuote.Broker_Application_Stage__c = applicationStage;
                }
                else if(applicationStage == BUY) {
                    aQuote.Broker_Application_Stage__c = BUY;
                }
                else{
                    aQuote.Broker_Application_Stage__c = null;
                }
	            Savepoint sp = Database.setSavepoint();
	            try
	            {
	            	upsert aQuote;
                    response.responseMessage = 'SUCCESS';
	            	System.debug('Upsert SUCCESSFUL ' + aQuote);
	            }
	    		catch(Exception e){
                    System.debug('Entering exception *******************');
		           	Database.rollback(sp);
		            System.debug('Run into DML Exception' + e);
		            throw new SMEQ_ServiceException(e.getMessage(), e);
        		} 
	    	
	        }

	        else
	        {
	        	System.debug('*** Save Page State failed quote could not be found or page number invalid *****');
	        	response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
	        	response.responseMessage = 'ERROR';
	        }
	    }
	    else
	    {
	    	System.debug('*** Save Page State failed INCOMPLETE REQUEST *****');
	        response.responseMessage = 'ERROR';
            response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
	        
	    }
        return response;
    }

    @TestVisible 
    public String getQuoteStatus(String applicationStage)
    {
    	String quoteStatus = '';
        if(applicationStage != null)
        {
            
        	if(applicationStage == GETTING_STARTED || applicationStage == ABOUT_THE_BUSINESS || applicationStage == CUSTOMIZE_QUOTE)
        	{
        		quoteStatus = IN_PROGRESS;
        	}

        	else if(applicationStage == REVIEW)
        	{
        		quoteStatus = FINALIZED;
        	}

        	else if(applicationStage == BUY)
        	{
        		quoteStatus = BOUND;
        	}
            else if(applicationStage == ISSUED)
            {
                quoteStatus = CLOSED;
            }
        	else
        	{
        		quoteStatus = NEW_QUOTE; //default value
        	}

            return quoteStatus;
        }

        else
        {
            return null;
        }
        	

    }
   
}