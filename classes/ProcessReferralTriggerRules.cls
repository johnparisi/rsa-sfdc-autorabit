/**
 * ProcessReferralTriggerRules
 * Author: James Lee
 * Date: January 20 2017
 * Feature: FP-518
 * 
 * The purpose of this class is to process any referral trigger rules.
 */
public class ProcessReferralTriggerRules
{
	public static String caseRegion{get;set;}
	public static String caseOffering{get;set;}
	public static SIC_Code_detail_version__c sicCodeDetailVersion{get;set;}
	
    /**
     * Invokes the ReferralTriggerService class to retrieve a list of classes to run for a quoteRequest
     * dynamically instantiates each class returned by the service and runs the checkRules method in each class
     * 
     * @param quoteRequest: 
     * @return: a list of reasons why referral rules have been triggered.
     */
    public static List<String> runReferralTriggerRules(SMEQ_QuoteRequest quoteRequest, Case caseToUpsert)
    {
        List<String> ruleResponses = new List<String>();
        List<ReferralTriggerRules__c> rtrs = ReferralTriggerRulesService.getReferralTriggerClasses(quoteRequest, caseToUpsert);
        
        caseOffering = UserService.getOffering(UserInfo.getUserId())[0];
        caseRegion = UserService.getRegion(UserInfo.getUserId())[0];
        
        if (caseToUpsert.bkrCase_SIC_Code__c != null)
        {
        	sicCodeDetailVersion = SMEQ_SicCodeService.getSicDetailsBySicProjectRegion(caseOffering, caseRegion, caseToUpsert.bkrCase_SIC_Code__c)[0];
        }
        
        for (ReferralTriggerRules__c rtr : rtrs)
        {
            Type t = Type.forName(ReferralTriggerRules.INTERFACE_CLASS_NAME + '.' + rtr.Rule_Class_Name__c);
            if (t != null)
            {
                ReferralTriggerRules.ReferralTriggerRule rt;
                
                try
                {
                    // Cast the class instance into the ReferralTriggerRules.ReferralTriggerRule interface.
                    rt = (ReferralTriggerRules.ReferralTriggerRule) t.newInstance();
                }
                catch (TypeException te)
                {
                    System.debug(te.getMessage());
                    System.debug(te.getStackTraceString());
                }
                
                if (rt.isReferral(quoteRequest))
                {
                    ruleResponses.add(rtr.Referral_Trigger_Reasons__c);
                }
            }
        }
        
        return ruleResponses;
    }
    
    
    public static Map<Id, List<String>> runReferralTriggerRules(List<Risk__c> records, Map<Id, Risk__c> oldRecords, Map<Id, Case> riskToCase) //quoteToCase is a map containing all the affected quote id and their case id
    {
      	List<String> ruleResponseString = new List<String>();
        Map<Id, List<String>> ruleResponses = new Map<Id, List<String>>();
        Map<Id, List<ReferralTriggerRules__c>> rtrs = ReferralTriggerRulesService.getReferralTriggerClasses(riskToCase.values()); // this returns a map of the case Id and the list of offerings by that id
        //before the ruleResponses contained the list of rule triggers but this can now be returned 
        
        //reverse mapping for get caseToRisks (caseId to Risk)
        Map<Id, List<Risk__c>> casetoRisks = new Map<Id, List<Risk__c>>(); 
        for (Id riskId : riskToCase.keySet()) //for all the ids in the riskToCase keyset 
        {
            for (Risk__c r : records) //for every risk in the record 
            {
                if (r.Id == riskId) //if there's a match between the risk in the record and the risk in the riskToCase:
                {
                    List<Risk__c> risks = casetoRisks.get(riskToCase.get(riskId).Id); //riskToCase.get(riskId) returns the case associated with that id; caseToRisks.get(case.id) then grabs the list of risks associated with that case id
        
                    if (risks == null)
                    {
                        risks = new List<Risk__c>(); //if it's null then create a new one and assign the list to it  
                    }
                    
                    risks.add(r); //add that risk object to that list 
                    caseToRisks.put(riskToCase.get(riskId).Id, risks); //now map the
                }
            }
        } 
        

        for(Id caseId : rtrs.keySet())// additional loop to map each of the of the caseIds in the keyset
        {
            ruleResponseString = new List<String>();

        	for (ReferralTriggerRules__c rtr : rtrs.get(caseId)) 
            {
                Type t = Type.forName(ReferralTriggerRules.INTERFACE_CLASS_NAME + '.' + rtr.Rule_Class_Name__c);
                if (t != null)
                {
                    ReferralTriggerRules.ReferralTriggerRule rt;
                    
                    try
                    {
                        // Cast the class instance into the ReferralTriggerRules.ReferralTriggerRule interface.
                        rt = (ReferralTriggerRules.ReferralTriggerRule) t.newInstance();
                    }
                    catch (TypeException te)
                    {
                        System.debug(te.getMessage());
                        System.debug(te.getStackTraceString());
                    }
                    
                    if (rt.isTriggeredReferral(caseToRisks.get(caseId), oldRecords))
                    {
                        ruleResponseString.add(rtr.Referral_Trigger_Reasons__c);
                    }
                }
            }
          ruleResponses.put(caseId, ruleResponseString);
        }

    	return ruleResponses; //has to return a map with case Id and list of trigger reasons Map<Id, List<String>> caseReferralTriggerReasons
   }

    public static List<String> runReferralTriggerRulesFromESB(String xml, Case caseToUpdate)
    {
        List<String> ruleResponses = new List<String>();
        List<ReferralTriggerRules__c> rtrs = ReferralTriggerRulesService.getReferralTriggerClassesFromESB(xml, caseToUpdate);
        
        caseOffering = UserService.getOffering(UserInfo.getUserId())[0];
        caseRegion = UserService.getRegion(UserInfo.getUserId())[0];
        
        for (ReferralTriggerRules__c rtr : rtrs)
        {
            Type t = Type.forName(ReferralTriggerRules.INTERFACE_CLASS_NAME + '.' + rtr.Rule_Class_Name__c);
            if (t != null)
            {
                ReferralTriggerRules.ReferralTriggerRuleFromESB rt;
                
                try
                {
                    // Cast the class instance into the ReferralTriggerRules.ReferralTriggerRule interface.
                    rt = (ReferralTriggerRules.ReferralTriggerRuleFromESB) t.newInstance();
                }
                catch (TypeException te)
                {
                    System.debug(te.getMessage());
                    System.debug(te.getStackTraceString());
                }
                
                if (rt.isReferral(xml))
                {
                    ruleResponses.add(rtr.Referral_Trigger_Reasons__c);
                }
            }
        }
        
        return ruleResponses;
    }   
}