/**
This is the domain class related to the BigMachines trigger. Its structure is flat as we don't have multiple record 
types.

The object domain is where the real work happens, this is where we will apply the actual business logic. 
We have the main Object Domain which should handle all common processing. 
Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
just implement logic in the appopriate method.

@author Priyanka Sirohi
*/

public class BigMachinesDomain {
    
  private BigMachinesService service = new BigMachinesService();
    
  public void beforeInsert(List<BigMachines__Quote__c> records){
  }

  public void beforeUpdate(List<BigMachines__Quote__c> records, Map<Id, BigMachines__Quote__c> oldRecords){
  }

  public void afterInsert(List<BigMachines__Quote__c> records){
      service.updateOpportunityStageBasedOnQuoteState(records);
  }

  public void afterUpdate(List<BigMachines__Quote__c> records, Map<Id, BigMachines__Quote__c> oldRecords){
      service.updateOpportunityStageBasedOnQuoteState(records);
  }

}