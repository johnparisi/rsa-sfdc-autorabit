/**
 * CSIOUploadQuoteRequestFactory
 * Author: Sophie Tran
 * Date: November 24, 2017
 * 
 * The purpose of this class is to generate the xml Request for the UPLOAD service 
 */
public class CSIOUploadQuoteRequestFactory extends CSIOGetUploadFinalizeRequestFactory implements CSIORequestFactory {
   
    
    public CSIOUploadQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds, ri);
        this.soqlDataSet = sds; 
        this.ri = ri;
    }
    
    public String buildXMLRequest(){
        getHeaderXml();
        xml += CSIO128Header;
        xml += super.buildStandardRequestDocument();
        xml += CSIO128Footer;
        return xml;
    }
}