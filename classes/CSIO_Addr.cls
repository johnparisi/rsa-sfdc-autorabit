public class CSIO_Addr
{
    private Account account;
    private Contact contact;
    private Risk__c risk;
    private Quote__C quote;
    
    public DetailAddr detailAddr;  
    public String city;
    public String stateProvCd;
    public String postalCode;
    public String countryCd;
    
    private String sourceStreetLineOne;
    private String sourceStreetLineTwo;
    private String sourceCity;
    private String sourceProvince;
    private String sourcePostCode;
    private String sourceCountryCode;

    private Boolean isMailing = false;

    public CSIO_Addr(Account account)
    {
        this.account = account;
        sourceStreetLineOne = account.BillingStreet;
        sourceStreetLineTwo = account.Billing_Address_Line2__c;
        sourceCity = account.BillingCity;
        sourceProvince = account.BillingState;
        sourcePostCode = account.BillingPostalCode;
        sourceCountryCode = account.BillingCountry;
        convert();
    }
    
    public CSIO_Addr(Contact contact)
    {
        this.contact = contact;
        sourceStreetLineOne = contact.MailingStreet;
        sourceStreetLineTwo = null;
        sourceCity = contact.MailingCity;
        sourceProvince = contact.MailingState;
        sourcePostCode = contact.MailingPostalCode;
        sourceCountryCode = contact.MailingCountry;
        //System.assert(contact.MailingStreet != null, 'mailing street should not be null');
        //System.assert(sourceStreetLineOne !=null, 'source street line one should not be null');
        convert();
    }
    
    public CSIO_Addr(Risk__c risk)
    {
        this.risk = risk;
        convert();
    }
    
    public CSIO_Addr(Quote__c quote) {
        this.isMailing = true;
        this.quote = quote;
        convert();
    }

    public CSIO_Addr(Risk__c risk, Boolean isMailing) {
        this.risk = risk;
        this.isMailing = isMailing;
        convert();
    }
    
    public void convert()
    {
        this.detailAddr = new DetailAddr();
        
        Map<String, String> esbProvince = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Province__c');
        Map<String, String> esbCountry = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('esbCountry');
        
        // account or contact based address
        if (account != null || contact !=null)
        {
            this.detailAddr = new DetailAddr();
            try
            {
                if (!sourceStreetLineOne.contains(' '))
                {
                    throw new AddressFormatException();
                }
                
                // The purpose of this try block is the workaround to the apex's inability to compile
                // the [String].isNumeric() method. The try will catch the exception for the Integer.valueOf()
                // for non-numeric strings, and switch to te fallback strategy in the catch block.
                try
                {
                    String streetNumber = sourceStreetLineOne.substringBefore(' ');
                    Integer streetNumeric = Integer.valueOf(streetNumber);
                    this.detailAddr.streetNumber = String.valueOf(streetNumeric);
                    this.detailAddr.streetName = sourceStreetLineOne.subStringAfter(' ');
                }
                catch (TypeException te)
                {
                    throw new AddressFormatException();
                }
            }
            catch (AddressFormatException afe)
            {
                this.detailAddr.streetNumber = '';
                this.detailAddr.streetName = sourceStreetLineOne;
            }
            this.detailAddr.unitNumber = sourceStreetLineTwo != null ? sourceStreetLineTwo : '';
            
            this.city = sourceCity;
            
            this.stateProvCd = esbProvince.get(sourceProvince.Replace('é','e')); //Convert to short code, and use translation workbench for FP.
            if (this.stateProvCd == null)
            {
                this.stateProvCd = sourceProvince.Replace('é','e');
            }
            this.postalCode = formatPostalCode(sourcePostCode);
            
            String country = esbCountry.get(sourceCountryCode); //Convert to short code, and use translation workbench for FP.
            this.countryCd = country != null ? country : 'CA';
        }
        else if (risk != null)
        {
            this.detailAddr = new CSIO_Addr.DetailAddr();
            
            try
            {
                if (!risk.Address_Line_1__c.contains(' '))
                {
                    throw new AddressFormatException();
                }
                
                // The purpose of this try block is the workaround to the apex's inability to compile
                // the [String].isNumeric() method. The try will catch the exception for the Integer.valueOf()
                // for non-numeric strings, and switch to te fallback strategy in the catch block.
                try
                {
                    if (risk.Address_Line_1__c.contains(' '))
                    {
                        String streetNumber = risk.Address_Line_1__c.substringBefore(' ');
                        Integer streetNumeric = Integer.valueOf(streetNumber); // Exception will be thrown here for non-numeric only values.
                        this.detailAddr.streetNumber = String.valueOf(streetNumeric);
                        this.detailAddr.streetName = risk.Address_Line_1__c.subStringAfter(' ');
                    }
                }
                catch (TypeException te)
                {
                    throw new AddressFormatException();
                }
            }
            catch (AddressFormatException afe)
            {
                this.detailAddr.streetNumber = '';
                this.detailAddr.streetName = risk.Address_Line_1__c;
            }
            this.detailAddr.unitNumber = risk.Address_Line_2__c != null ? risk.Address_Line_2__c : '';
            
            this.city = risk.City__c;
            
            this.stateProvCd = esbProvince.get(risk.Province__c); //Fixed?
            this.postalCode = formatPostalCode(risk.Postal_Code__c);
            String country = esbCountry.get(risk.Country__c); //Fixed?
            this.countryCd = country != null ? country : 'CA';
        }

        if (risk != null && isMailing)
        {
            if (risk.Related_COM__c == null){
                this.detailAddr = new CSIO_Addr.DetailAddr();

                try
                {
                    if (risk.Mailing_Address_Street__c != null && !risk.Mailing_Address_Street__c.contains(' '))
                    {
                        throw new AddressFormatException();
                    }
                    try
                    {
                        if (risk.Mailing_Address_Street__c != null && risk.Mailing_Address_Street__c.contains(' '))
                        {
                            String streetNumber = risk.Mailing_Address_Street__c.substringBefore(' ');
                            Integer streetNumeric = Integer.valueOf(streetNumber); // Exception will be thrown here for non-numeric only values.
                            this.detailAddr.streetNumber = String.valueOf(streetNumeric);
                            this.detailAddr.streetName = risk.Mailing_Address_Street__c.subStringAfter(' ');
                        }
                    }
                    catch (TypeException te)
                    {
                        throw new AddressFormatException();
                    }
                }
                catch (AddressFormatException afe)
                {
                    this.detailAddr.streetNumber = '';
                    this.detailAddr.streetName = risk.Mailing_Address_Street__c;
                }
                this.city = risk.Mailing_City__c;

                //this.stateProvCd = esbProvince.get(risk.Mailing_Province__c); //Fixed?
                this.stateProvCd = risk.Mailing_Province__c;
                this.postalCode = formatPostalCode(risk.Mailing_Address_Postal_Code__c);
                String country = esbCountry.get(risk.Mailing_Country__c); //Fixed?
                this.countryCd = country != null ? country : 'CA';
            } 
            if (risk.Related_COM__c != null && risk.Quote__r.status__C == 'Finalized') {
                this.detailAddr = new CSIO_Addr.DetailAddr();
                String quoteAddr = risk.Quote__r.Address_Line_1__c + risk.Quote__r.Address_Line_2__c + risk.Quote__r.City__c  + risk.Quote__r.Country__c  + risk.Quote__r.State_Province__c ;
                String locationAddr = risk.Address_Line_1__c + risk.Address_Line_2__c + risk.City__c + risk.Country__c + risk.Province__c;
                String quoteAddrTrimmed = '';
                String locationAddrTrimed = '';
                
                if(quoteAddr.deleteWhitespace().toLowerCase() != locationAddr.deleteWhitespace().toLowerCase()){
                    try {
                        if (risk.quote__r.Address_Line_1__c != null && !risk.quote__r.Address_Line_1__c.contains(' ')) {
                            throw new AddressFormatException();
                        }
                        
                        try {
                            if (risk.quote__r.Address_Line_1__c != null && risk.quote__r.Address_Line_1__c.contains(' ')) {
                                String streetNumber = risk.quote__r.Address_Line_1__c.substringBefore(' ');
                                Integer streetNumeric = Integer.valueOf(streetNumber); // Exception will be thrown here for non-numeric only values.
                                this.detailAddr.streetNumber = String.valueOf(streetNumeric);
                                this.detailAddr.streetName = risk.quote__r.Address_Line_1__c.subStringAfter(' ');
                            }
                        }
                        catch (TypeException te)
                        {
                            throw new AddressFormatException();
                        }
                    }
                    catch (AddressFormatException afe)
                    {
                        this.detailAddr.streetNumber = '';
                        this.detailAddr.streetName = risk.quote__r.Address_Line_1__c;
                    }
    
    
                    this.city = risk.quote__r.City__c;
        
                    this.stateProvCd = esbProvince.get(risk.quote__r.State_Province__c); //Fixed?
                    if (this.stateProvCd == null) {
                        this.stateProvCd = risk.quote__r.State_Province__c;
                    }
                	system.debug('risk.quote__r.Postal' + risk.quote__r.Postal_Code__c);
                    this.postalCode = formatPostalCode(risk.quote__r.Postal_Code__c);
                    String country = esbCountry.get(risk.quote__r.Country__c); //Fixed?
                    this.countryCd = country != null ? country : 'CA';
                }
            }
            
        }
    }
    
    /**
     * FP-2913
     * 
     * Formats the postal code according to CSIO specifications.
     * 
     * @param postalCode: The postal code to format.
     * @return: The formatted postal code.
     */
    @testVisible
    private static String formatPostalCode(String postalCode)
    {
        if (postalCode.length() == 5)
        {
            return postalCode;
        }
        else
        {
            // Remove spaces in random parts of the string.
            postalCode = postalCode.remove(' ');
            
            // Switch postalcode to upper case.
            postalCode = postalCode.toUpperCase();
            
            // Add a space in between.
            postalCode = postalCode.substring(0, 3) + ' ' + postalCode.substring(3, 6);
            
            return postalCode;
        }
    }
    
    public override String toString()
    {
        String xml = '';
        xml += '<Addr>';
        if(this.isMailing) {
            xml += '<AddrTypeCd>csio:1</AddrTypeCd>';
        }
        xml += this.detailAddr;
        xml += ' <City>' + XMLHelper.toCData(this.city) + '</City>';
        xml += ' <StateProvCd>' + this.stateProvCd + '</StateProvCd>';
        xml += ' <PostalCode>' + this.postalCode + '</PostalCode>';
        xml += ' <CountryCd>' + this.countryCd + '</CountryCd>';
        xml += '</Addr>';
        return xml;
    }
    
    public class DetailAddr
    {
        public String streetName;
        public String streetNumber;
        public String unitNumber;
        
        public override String toString()
        {
            String xml = '';
            xml += '<DetailAddr>';
            xml += ' <StreetName>' + XMLHelper.toCData(this.streetName) + '</StreetName>';
            if (this.streetNumber != null && this.streetNumber != '')
            {
                xml += ' <StreetNumber>' + this.streetNumber + '</StreetNumber>';
            }
            if (this.unitNumber != null && this.unitNumber != '')
            {
                xml += ' <UnitNumber>' + this.unitNumber + '</UnitNumber>';
            }
            xml += '</DetailAddr>';
            return xml;
        }
    }
    
    public class AddressFormatException extends Exception
    {
        
    }
}