@isTest
public class SMEQ_ConfirmBrokerHandover_Test
{
    static void dataSetup()
    {
        Map<String, Id> CaseTypes = Utils.getRecordTypeIdsByDeveloperName(Case.SObjectType, true);
        Map<String, Id> QuoteTypes = Utils.getRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        
        Case c = SMEQ_TestDataGenerator.getOpenSprntCase();
        Quote__c q = SMEQ_TestDataGenerator.getOpenQuote(c);
    }
    
    /**
     * An underwriter should not be able to confirm a handover if the broker has not yet selected the handover button.
     * Assertions:
     * 1. The case has not been handed over yet by the broker.
     * 2. The underwriter recieves an error message upon clicking the confirm handover button.
     */
	static testMethod void testAssignOwnerWithoutHandover()
    {
        User portalUser;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            portalUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(portalUser)
        {
            dataSetup();
            
            Case c = [
                SELECT Id, Quote_application_handed_over_to_UW__c
                FROM Case
                LIMIT 1
            ];
            
            System.assertEquals(false, c.Quote_application_handed_over_to_UW__c);
        }
        
        Quote__c q = [
            SELECT Id
            FROM Quote__c
            LIMIT 1
        ];
        
        String result = SMEQ_ConfirmBrokerHandover.assignOwner(q.Id);
        System.assertEquals(SMEQ_ConfirmBrokerHandover.RESULT_BROKER_YET_TO_HANDOVER, result);
    }
    
    /**
     * An underwriter should be able to confirm a handover after the broker has selected the handover button.
     * An underwriter should not be able to confirm a handover more than once.
     * Assertions:
     * 1. The underwriter does not receive an error message for a quote released for underwriter handoever
     * 2. In a separate transaction, the quote has already been handed over
     * 3. The underwriter recieves an error message upon clicking the confirm handover button a second time.
     * 4. The case owner is assigned to the user who initially clicked on the confirm handover button.
     */
    static testMethod void testAssignOwnerWithHandover()
    {
        User adminUser = UserService_Test.getNonPortalUserWithRole();
        User portalUser;
        Quote__c q;
        Case c;
        String result;
        
        System.runAs(adminUser)
        {
            UserService_Test.setupTestData();
            portalUser = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
            
            System.runAs(portalUser)
            {
                dataSetup();
                
                c = [
                    SELECT Id, Quote_application_handed_over_to_UW__c
                    FROM Case
                    LIMIT 1
                ];
                
                // The broker releases the quote to an underwriter
                c.Quote_application_handed_over_to_UW__c = true;
                update c;
            }
            
            q = [
                SELECT Id
                FROM Quote__c
                LIMIT 1
            ];
            
            result = SMEQ_ConfirmBrokerHandover.assignOwner(q.Id);
            System.assertEquals('', result);
        }
        
        System.runAs(adminUser)
        {
            q = [
                SELECT Id, case__r.Quote_application_handed_over_to_UW__c
                FROM Quote__c
                LIMIT 1
            ];
            
            System.assertEquals(true, q.case__r.Quote_application_handed_over_to_UW__c);
            
            result = SMEQ_ConfirmBrokerHandover.assignOwner(q.Id);
            System.assertEquals(SMEQ_ConfirmBrokerHandover.RESULT_UNDERWRITER_ALREADY_CONFIRMED, result);
        }
        
        c = [
            SELECT Id, OwnerId
            FROM Case
            LIMIT 1
        ];
        System.assertEquals(c.OwnerId, adminUser.Id);
    }
}