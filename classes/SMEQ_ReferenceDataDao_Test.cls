@isTest
public class SMEQ_ReferenceDataDao_Test {
	public SMEQ_ReferenceDataDao_Test() {
		
	}

	static testMethod void testGetReferenceData(){
		SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
		generateResourceBundlesTestData();
		SMEQ_ReferenceDataModel referenceDataModel = referenceDataDao.getReferenceData('www');
		System.assertEquals('EN', referenceDataModel.referenceDataListingEnglish.language);
		System.assertEquals('FR', referenceDataModel.referenceDataListingFrench.language);
		System.assertEquals(2, referenceDataModel.referenceDataListingFrench.referenceDataByKeyLists.size());
		for(SMEQ_ReferenceDataByKeyListsModel referenceDataKeyListModel:referenceDataModel.referenceDataListingFrench.referenceDataByKeyLists){
			System.debug('Referece Data Key: ' + referenceDataKeyListModel.referenceDataKey);
			for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.debug('Key: ' + keyValue.key);
					System.debug('Vlaue: ' + keyValue.value);
			}

			if(referenceDataKeyListModel.referenceDataKey == 'GENDER'){
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='M' || keyValue.key == 'F'));
				}
			}
			else{
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='Y' || keyValue.key == 'N'));
				}
			}
		}

		System.assertEquals(2, referenceDataModel.referenceDataListingEnglish.referenceDataByKeyLists.size());
		for(SMEQ_ReferenceDataByKeyListsModel referenceDataKeyListModel:referenceDataModel.referenceDataListingEnglish.referenceDataByKeyLists){
			if(referenceDataKeyListModel.referenceDataKey == 'GENDER'){
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='M' || keyValue.key == 'F'));
				}
			}
			else{
				for(SMEQ_KeyValueModel keyValue:referenceDataKeyListModel.keyValues){
					System.assert((keyValue.key=='Y' || keyValue.key == 'N'));
				}
			}
		}

	}

	static testMethod void testBuildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMapWithBothMapsEmptyAddingEnglisKeyValue(){
		SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
        SMEQ_REFERENCE_DATA__c referenceDateFromDataStore = buildReferenceDataObject('M', 'GENDER', 'EN', 'Male', 0, null);
        Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap = new Map<String, List<SMEQ_KeyValueModel>>();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap = new Map<String, List<SMEQ_KeyValueModel>>();
        referenceDataDao.buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceDateFromDataStore, 
        	referenceDataEnglishMap, referenceDataFrenchMap);
        List<SMEQ_KeyValueModel> keyVlaueModelListEnglish = referenceDataEnglishMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        List<SMEQ_KeyValueModel> keyVlaueModelListFrench = referenceDataFrenchMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        System.assertEquals(1, keyVlaueModelListEnglish.size());
        System.assert(keyVlaueModelListFrench == null);
    }


    static testMethod void testBuildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMapWithBothMapsEmptyAddingFrenchKeyValue(){
		SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
        SMEQ_REFERENCE_DATA__c referenceDateFromDataStore =  buildReferenceDataObject('M', 'GENDER', 'FR', 'MALE_FR', 0, null);
        Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap = new Map<String, List<SMEQ_KeyValueModel>>();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap = new Map<String, List<SMEQ_KeyValueModel>>();
        referenceDataDao.buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceDateFromDataStore, 
        	referenceDataEnglishMap, referenceDataFrenchMap);
        List<SMEQ_KeyValueModel> keyVlaueModelListEnglish = referenceDataEnglishMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        List<SMEQ_KeyValueModel> keyVlaueModelListFrench = referenceDataFrenchMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        System.assertEquals(1, keyVlaueModelListFrench.size());
        System.assert(keyVlaueModelListEnglish == null);
    }


    static testMethod void testBuildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMapWithBothMapsEmptyAddingMoreThanOneEnglishKeyValue(){
		SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap = new Map<String, List<SMEQ_KeyValueModel>>();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap = new Map<String, List<SMEQ_KeyValueModel>>();
        SMEQ_REFERENCE_DATA__c referenceDateFromDataStore = buildReferenceDataObject('M', 'GENDER', 'EN', 'Male', 0, null);
        SMEQ_REFERENCE_DATA__c referenceDateFromDataStore1 = buildReferenceDataObject('F', 'GENDER', 'EN', 'Female', 1, null);
        referenceDataDao.buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceDateFromDataStore, 
        	referenceDataEnglishMap, referenceDataFrenchMap);
        referenceDataDao.buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceDateFromDataStore1, 
        	referenceDataEnglishMap, referenceDataFrenchMap);
        List<SMEQ_KeyValueModel> keyVlaueModelListEnglish = referenceDataEnglishMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        List<SMEQ_KeyValueModel> keyVlaueModelListFrench = referenceDataFrenchMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        System.assertEquals(2, keyVlaueModelListEnglish.size());
        System.assert(keyVlaueModelListFrench == null);
    }


    static testMethod void testBuildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMapWithBothMapsEmptyAddingMoreThanOneFrenchKeyValue(){
		SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap = new Map<String, List<SMEQ_KeyValueModel>>();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap = new Map<String, List<SMEQ_KeyValueModel>>();
        SMEQ_REFERENCE_DATA__c referenceDateFromDataStore = buildReferenceDataObject('M', 'GENDER', 'FR', 'Male_FR', 0, null);
        SMEQ_REFERENCE_DATA__c referenceDateFromDataStore1 = buildReferenceDataObject('F', 'GENDER', 'FR', 'Female_FR', 1, null);
        referenceDataDao.buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceDateFromDataStore, 
        	referenceDataEnglishMap, referenceDataFrenchMap);
        referenceDataDao.buildKeyValueFromSMEQ_REFERENCE_DATAAndAddToMap(referenceDateFromDataStore1, 
        	referenceDataEnglishMap, referenceDataFrenchMap);
        List<SMEQ_KeyValueModel> keyVlaueModelListEnglish = referenceDataEnglishMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        List<SMEQ_KeyValueModel> keyVlaueModelListFrench = referenceDataFrenchMap.get(referenceDateFromDataStore.type__c + '_' + referenceDateFromDataStore.language__c);
        System.assertEquals(2, keyVlaueModelListFrench.size());
        System.assert(keyVlaueModelListEnglish == null);
    }


    static testMethod void testQueryReferenceData(){
    	SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
    	generateResourceBundlesTestData();
    	List<SMEQ_REFERENCE_DATA__c> referenceDataList =  referenceDataDao.queryReferenceData('www');
    	System.assertEquals(8, referenceDataList.size());
    }


    static testMethod void testBuildReferenceDataByKeyListModel(){
    	SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
    	List<SMEQ_KeyValueModel> keyValueModelList = new List<SMEQ_KeyValueModel>();
    	SMEQ_KeyValueModel keyValue = new SMEQ_KeyValueModel();
    	keyValue.key = 'M';
    	keyValue.value = 'Male';
    	keyValueModelList.add(keyValue);
    	SMEQ_ReferenceDataByKeyListsModel referenceDataByKeyListsModel = referenceDataDao.buildReferenceDataByKeyListModel('GENDER', keyValueModelList);
    	System.assertEquals('GENDER', referenceDataByKeyListsModel.referenceDataKey);
    	System.assertEquals(1, referenceDataByKeyListsModel.keyValues.size());
    }


    static testMethod void testBuildReferenceDataListingByLanguageModel(){
    	SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
    	Map<String,List<SMEQ_KeyValueModel>> referenceDataMapEnglish = new Map<String,List<SMEQ_KeyValueModel>>();
    	referenceDataMapEnglish.put('GENDER_EN', generateKeyValueModelObjectsByLang('EN','GENDER'));
    	referenceDataMapEnglish.put('YesOrNo_EN', generateKeyValueModelObjectsByLang('EN','YesOrNo'));
    	SMEQ_ReferenceDataListingByLanguageModel referenceDataListingByLanguageModel = referenceDataDao.buildReferenceDataListingByLanguageModel('EN', referenceDataMapEnglish);
    	System.debug('Language: ' + referenceDataListingByLanguageModel.language);
    	System.debug('Reference Data Key: ' + referenceDataListingByLanguageModel.referenceDataByKeyLists[0].referenceDataKey);
    	System.debug('Gender List Size ' + referenceDataListingByLanguageModel.referenceDataByKeyLists[0].keyValues.size());
    	System.assertEquals('EN', referenceDataListingByLanguageModel.language);
    	System.assertEquals('GENDER', referenceDataListingByLanguageModel.referenceDataByKeyLists[0].referenceDataKey);
    	System.assertEquals(2, referenceDataListingByLanguageModel.referenceDataByKeyLists[0].keyValues.size());
    }


    static public List<SMEQ_KeyValueModel> generateKeyValueModelObjectsByLang(String lang, String type){
    	List<SMEQ_KeyValueModel> keyValueModelList = new List<SMEQ_KeyValueModel>();
    	if(type == 'GENDER'){
    		SMEQ_KeyValueModel genderKeyValue = new SMEQ_KeyValueModel();
    		genderKeyValue.key = 'M';
    		genderKeyValue.value = 'Male' + '_' + lang;
    		SMEQ_KeyValueModel genderKeyValue1 = new SMEQ_KeyValueModel();
    		genderKeyValue1.key = 'F';
    		genderKeyValue1.value = 'Female' + '_' + lang;
    		keyValueModelList.add(genderKeyValue);
    		keyValueModelList.add(genderKeyValue1);
    	}

    	if(type == 'YesOrNo'){
    		SMEQ_KeyValueModel yesOrNoKeyValue = new SMEQ_KeyValueModel();
    		yesOrNoKeyValue.key = 'M';
    		yesOrNoKeyValue.value = 'Male' + '_' + lang;
    		SMEQ_KeyValueModel yesOrNoKeyValue1 = new SMEQ_KeyValueModel();
    		yesOrNoKeyValue1.key = 'F';
    		yesOrNoKeyValue1.value = 'Female' + '_' + lang;
    		keyValueModelList.add(yesOrNoKeyValue);
    		keyValueModelList.add(yesOrNoKeyValue1);
    	}

    	return keyValueModelList;

    }


    static public void generateResourceBundlesTestData(){
        List<SMEQ_REFERENCE_DATA__c> referenceDataList = new List<SMEQ_REFERENCE_DATA__c>();
        referenceDataList.add(buildReferenceDataObject('M','GENDER', 'EN','Male', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('F','GENDER', 'EN','Female', 2, 'www'));
        referenceDataList.add(buildReferenceDataObject('M','GENDER', 'FR','Male_FR', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('F','GENDER', 'FR','Female_FR', 2, 'www'));

        referenceDataList.add(buildReferenceDataObject('Y','YesNo', 'EN','Yes', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('N','YesNo', 'EN','No', 2, 'www'));
        referenceDataList.add(buildReferenceDataObject('Y','YesNo', 'FR','Yes_FR', 1, 'www'));
        referenceDataList.add(buildReferenceDataObject('N','YesNo', 'FR','No_FR', 2, 'www'));
        insert referenceDataList;
    }


    static public void testPopulateReferenceDataMapforEnglishAndFrench(){
    	SMEQ_ReferenceDataDao referenceDataDao = new SMEQ_ReferenceDataDao();
    	Map<String,List<SMEQ_KeyValueModel>> referenceDataEnglishMap = new Map<String, List<SMEQ_KeyValueModel>>();
        Map<String,List<SMEQ_KeyValueModel>> referenceDataFrenchMap = new Map<String, List<SMEQ_KeyValueModel>>();
        generateResourceBundlesTestData();
    	List<SMEQ_REFERENCE_DATA__c> referenceDataList = referenceDataDao.queryReferenceData('www');
        referenceDataDao.populateReferenceDataMapforEnglishAndFrench(referenceDataList,referenceDataEnglishMap, referenceDataFrenchMap);
        System.assertEquals(2, referenceDataEnglishMap.get('YesNo_FR').size());
        System.assertEquals(2, referenceDataEnglishMap.get('GENDER_EN').size());
    }
    
    
    static SMEQ_REFERENCE_DATA__c buildReferenceDataObject(String code, String type, String language, String value, Integer displayOrder, String variant){
    	SMEQ_REFERENCE_DATA__c referenceDataObject = new SMEQ_REFERENCE_DATA__c();
    	referenceDataObject.code__c = code;
    	referenceDataObject.type__c = type;
    	referenceDataObject.language__c = language;
    	referenceDataObject.value__c = value;
    	referenceDataObject.display_order__c = displayOrder;
    	referenceDataObject.variant__c = variant;
    	return referenceDataObject;
    }


}