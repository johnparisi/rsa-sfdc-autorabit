public with sharing class SIC_Code_Detail_VersionDomain {
    private SIC_Code_Detail_VersionService service = new SIC_Code_Detail_VersionService();
    public void beforeInsert(List<SIC_Code_Detail_Version__c> records){
        service.updateSIC_Code_Detail_Version_Id(records);
    }

    public void beforeUpdate(List<SIC_Code_Detail_Version__c> records, Map<Id, SIC_Code_Detail_Version__c> oldRecords){
        service.updateSIC_Code_Detail_Version_Id(records);        
    }
}