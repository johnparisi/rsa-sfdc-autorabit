/**
* @File Name    :   SMEQ_BrokerUserCreateService
* @Description  :   Batch Broker User Creation Service
*                   contains processing service methods related to creating broker user.
* @Date Created :   08/08/2017
* @Author       :   Habiba Zaman @ Deloitte [hzaman@deloitte.ca]
* @group        :   Service
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       08/08/2017  Habiba Zaman    Created the file/class
* 1.1       01/09/2017  Habiba Zaman    Updated code based on code review.
*/
public with sharing class SMEQ_BrokerUserCreateService {



  // Map for Account Record Type
    public Map<String,Id> accountTypes{
        get{

            return Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);
        }

        private set;
    }

    // Map for Contact Record Type
    public Map<String,Id> contactTypes{
        get{

            return Utils.GetRecordTypeIdsByDeveloperName(Contact.SObjectType, true);
        }

        private set;
    }
    //main user staging records
    public Map<Id,User_Staging__c> userStagingRecords {public get; public set;}

    private static final String SME_BROKER_PROFILE = 'SME Broker';
    private static final String ERROR_FOR_ACCOUNT = 'ERR1';
    private static final String ERROR_FOR_CONTACT = 'ERR2';
    private static final String ERROR_FOR_USER = 'ERR3';
    private static final String ACCOUNT_RECORD_TYPE = 'bkrAccount_RecordTypeBkrMGML';
    private static final String ACCOUNT_RECORD_TYPE_MM = 'bkrAccount_RecordTypeBkrMM';
    private static final String CONTACT_RECORD_TYPE = 'bkrContact_BrokerRecordType';

    public void init(List<User_Staging__c> brokerUserInfoList){
        userStagingRecords = new Map<Id, User_Staging__c>();

        for(User_Staging__c us : brokerUserInfoList){
            userStagingRecords.put(us.Id, us);
        }

        processAccount();
        processContact();
        processUser();
        processUserStageRecords();

    }

    /*
    @Author: Habiba Zaman
    @Date: Aug. 8 2017
    @Description: Use this to get the Broker Number from the new Broker Stage object
    */
    public Map<String,String> getStageBrokerNumberList(List<String> brokerNumberList){
        Map<String,String> brokerNumbers = new Map<String,String>();
        if(brokerNumberList.size() > 0){

            List<Broker_Stage__c> stagedBroker = [SELECT Id,SEQAGTNO__c, BROKER_NUMBER__c,
                                                PARENT1_BROKER_NUMBER__c, PARENT2_BROKER_NUMBER__c,
                                                PARENT3_BROKER_NUMBER__c,
                                                WORKFLOW_EP__c, CLASSPROF__c, AGTSECID__c
                                                FROM Broker_Stage__c
                                                WHERE ( BROKER_NUMBER__c in : brokerNumberList
                                                OR PARENT1_BROKER_NUMBER__c in : brokerNumberList
                                                OR PARENT2_BROKER_NUMBER__c in : brokerNumberList
                                                OR PARENT3_BROKER_NUMBER__c in : brokerNumberList )
                                                AND ( WORKFLOW_EP__c = 'NB' )
                                                AND ( CLASSPROF__c = 'C001' OR CLASSPROF__c = 'C002' )
                                                ];

            for(String brokerNumber :brokerNumberList){
                if (stagedBroker != null && !stagedBroker.isEmpty() )
                {
                  //add staged broker with BROKER_NUMBER__c = brokerNumber first
                    if(brokerNumber !=null){
                        for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                            if(stagedBrokerSelected.BROKER_NUMBER__c!= null && brokerNumber.equals(stagedBrokerSelected.BROKER_NUMBER__c)){
                                brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                            }
                        }
                        //if no staged broker with BROKER_NUMBER__c match brokerNumber then look for PARENT1_BROKER_NUMBER__c
                        for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                            if(stagedBrokerSelected.PARENT1_BROKER_NUMBER__c!= null && brokerNumber.equals(stagedBrokerSelected.PARENT1_BROKER_NUMBER__c)
                                && !brokerNumbers.containsKey(brokerNumber)){
                                brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                            }
                        }
                        //if no staged broker with PARENT1_BROKER_NUMBER__c match brokerNumber then look for PARENT2_BROKER_NUMBER__c
                        for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                            if(stagedBrokerSelected.PARENT2_BROKER_NUMBER__c!= null && brokerNumber.equals(stagedBrokerSelected.PARENT2_BROKER_NUMBER__c)
                                && !brokerNumbers.containsKey(brokerNumber)){
                                brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                            }
                        }
                        //if no staged broker with PARENT2_BROKER_NUMBER__c match brokerNumber then look for PARENT3_BROKER_NUMBER__c
                        for(Broker_Stage__c stagedBrokerSelected: stagedBroker){
                            if(stagedBrokerSelected.PARENT3_BROKER_NUMBER__c!= null && brokerNumber.equals(stagedBrokerSelected.PARENT3_BROKER_NUMBER__c)
                                && !brokerNumbers.containsKey(brokerNumber)){
                                brokerNumbers.put(brokerNumber, String.valueOf(stagedBrokerSelected.SEQAGTNO__c));
                            }
                        }
                    }
                }
                else{
                    //if no database result return null
                    return null;
                }
            }
        }
        return brokerNumbers;
    }

    /*
    @Author: Habiba Zaman
    @param : N/A
    @return : N/A
    @Date: Aug. 8 2017
    @Description: Find account records based on the HUON broker number in User Staging object.
    */
    public void processAccount(){
        List<String> userStageBorkerNumber = new List<String>();
        Map<String,User_Staging__c> brokerStageNumberMapping = new Map<String,User_Staging__c>();

        //process only new and account error related one.
        for(User_Staging__c us : userStagingRecords.values()){
            if(us.Broker_Account__c == null){
                userStageBorkerNumber.add(us.bkrAccount_HuonBrokerNumber__c);
            }
        }

        if(userStageBorkerNumber.size() > 0){
            List<Account> accountList = [ SELECT Id, bkrAccount_HuonBrokerNumber__c, IsPartner
                                        FROM Account
                                        WHERE (RecordTypeId =: accountTypes.get(ACCOUNT_RECORD_TYPE)
                                        OR RecordTypeId =: accountTypes.get(ACCOUNT_RECORD_TYPE_MM) )
                                        AND bkrAccount_HuonBrokerNumber__c IN : userStageBorkerNumber];


            Map<String, Id> accountWithPartnerEnable = new Map<String,Id>();
            if (accountList != null && !accountList.isEmpty() )
            {
                for(Account acc : accountList){
                    if(acc.IsPartner){
                        accountWithPartnerEnable.put(acc.bkrAccount_HuonBrokerNumber__c,acc.Id);
                    }
                }
                for (User_Staging__c us : userStagingRecords.values()){
                    if(accountWithPartnerEnable.containsKey(us.bkrAccount_HuonBrokerNumber__c)){
                        us.Broker_Account__c = accountWithPartnerEnable.get(us.bkrAccount_HuonBrokerNumber__c);
                    }
                    else{
                        us.Error_Code__c = ERROR_FOR_ACCOUNT;
                    }
                    userStagingRecords.put(us.Id,us);
                }
            }
        }

    }

    /*
    @Author: Habiba Zaman
    @param : N/A
    @return : N/A
    @Date: Aug. 8 2017
    @Description: Find/Update contacts records based on the account id  broker number in User Staging object.
    */
    public void processContact(){
        List<Contact> contactListToUpdate = new List<Contact>();

        // process records with proper contact
        Map<String, User_Staging__c> recordsToProcess = new Map<String, User_Staging__c>();
        List<String> brokerEmailList = new List<String>();
        List<String> userStageEPList = new List<String>();
        List<String> brokerAccountList = new List<String>();

        for(User_Staging__c us : userStagingRecords.values()){
            if(us.Broker_Account__c !=null && us.Broker_Contact__c == null){
                recordsToProcess.put(us.Email_Address__c, us);
                brokerAccountList.add(us.Broker_Account__c);
                brokerEmailList.add(us.Email_Address__c);
                userStageEPList.add(us.EP_Reference_Number__c);

            }
        }

        //Get broker stage broker number Map < EP Reference Number, Broker Number from Broker Stage>
        Map<String,String> epBrokerStageRelatedNumber = new Map<String,String>();
        epBrokerStageRelatedNumber = getStageBrokerNumberList(userStageEPList);

        List<Contact> contactsList = [Select Id, EP_Reference_Number__c, AccountId, Email
                                    FROM Contact
                                    WHERE RecordTypeId = : contactTypes.get(CONTACT_RECORD_TYPE)
                                    AND AccountId in: brokerAccountList
                                    AND Email in: brokerEmailList];

        if(contactsList.size() > 0 && !contactsList.isEmpty()){
            for(Contact c : contactsList){

                User_Staging__c us = new User_Staging__c();
                us = recordsToProcess.get(c.Email);

                if(us != null){
                    String contactEP = c.EP_Reference_Number__c;

                    if(String.isNotEmpty(contactEP )){
                        if(contactEP == us.EP_Reference_Number__c){
                            us.Broker_Contact__c = c.Id;
                            recordsToProcess.put(c.Email, us);
                        }
                        else if(!contactEP.equals(us.EP_Reference_Number__c)){
                            if(epBrokerStageRelatedNumber.get(us.EP_Reference_Number__c) != null){
                                c.EP_Reference_Number__c = us.EP_Reference_Number__c;
                                contactListToUpdate.add(c);
                            }
                            else{
                                us.Error_Code__c = ERROR_FOR_CONTACT ;
                                us.Error_Code_Details__c = 'Contacts existing EP reference did not match with User stage number and User stage EP ref no is not in Broker stage'; //added per Phil's recommendation
                            }
                        }
                    }
                    else{
                        if(epBrokerStageRelatedNumber.get(us.EP_Reference_Number__c) != null){
                                c.EP_Reference_Number__c = us.EP_Reference_Number__c;
                                contactListToUpdate.add(c);
                        }
                        else{
                            us.Error_Code__c = ERROR_FOR_CONTACT ;
                            us.Error_Code_Details__c = 'Contacts existing EP reference did not match with User stage number and User stage EP ref no is not in Broker stage'; //added per Phil's recommendation
                        }
                    }
                }
            }
        }

        //Update already existing contact records with correct Ep number
        if(contactListToUpdate.size() > 0){
            List<Database.SaveResult> res  = Database.update(contactListToUpdate,false);

            for (Integer i = 0; i < contactListToUpdate.size(); i++) {
                Database.SaveResult s = res[i];
                Contact origRecord = contactListToUpdate[i];
                User_Staging__c us = new User_Staging__c();

                if (!s.isSuccess()) {
                    us = recordsToProcess.get(origRecord.Email);
                    if(us.Email_Address__c == origRecord.Email){
                        us.Error_Code__c = ERROR_FOR_CONTACT;
                        us.Error_Code_Details__c =  s.getErrors()[0].getMessage();
                        recordsToProcess.put(origRecord.Email, us);
                    }
                 }
                 else{
                    us = recordsToProcess.get(origRecord.Email);
                     if(us.Email_Address__c == origRecord.Email){
                        us.Broker_Contact__c =origRecord.Id;
                        recordsToProcess.put(origRecord.Email, us);
                    }
                 }
            }
        }

        //find all records where no contact id and create contact record for them
        List<Contact> contactListToInsert = new List<Contact>();
        for(User_Staging__c us: recordsToProcess.values()){
            if(us.Broker_Account__c !=null && us.Broker_Contact__c == null){

                Contact cnt = new Contact(
                    FirstName = us.First_Name__c,
                    LastName = us.Last_Name__c,
                    AccountId = us.Broker_Account__c,
                    Email = us.Email_Address__c,
                    EP_Reference_Number__c = us.EP_Reference_Number__c,
                    bkrContact_BRAVO_ID__c = us.Federation_ID__c,
                    RecordTypeId = contactTypes.get(CONTACT_RECORD_TYPE)
                );
                contactListToInsert.add(cnt);
            }
        }

        if(contactListToInsert.size() > 0){

            List<Database.SaveResult> resInsert  = Database.insert(contactListToInsert,false);

            for (Integer i = 0; i < contactListToInsert.size(); i++) {
                Database.SaveResult s = resInsert[i];
                Contact origRecord = contactListToInsert[i];
                User_Staging__c us = new User_Staging__c();

                if (!s.isSuccess()) {
                    us = recordsToProcess.get(origRecord.Email);
                    if(us.Email_Address__c == origRecord.Email){
                        us.Error_Code__c = ERROR_FOR_CONTACT;
                        us.Error_Code_Details__c = s.getErrors()[0].getMessage();
                        recordsToProcess.put(us.Email_Address__c , us);
                    }
                 }
                 else{
                    us = recordsToProcess.get(origRecord.Email);
                    if(us.Email_Address__c == origRecord.Email){
                        us.Broker_Contact__c =origRecord.Id;
                        recordsToProcess.put(us.Email_Address__c , us);
                    }
                 }
            }
        }

        // update master user stage map with  contact information
        for(User_Staging__c usc : recordsToProcess.values()){
            userStagingRecords.put(usc.Id, usc);
        }

    }

    /*
    @Author: Habiba Zaman
    @param : N/A
    @return : N/A
    @Date: Aug. 8 2017
    @Description: Create users for contact with proper broker.
    */
    public void processUser(){
        //get profile id
        Profile userProfile = [SELECT Id FROM Profile WHERE Name LIKE :SME_BROKER_PROFILE];

        //filter the records with error record
        Map<String,User_Staging__c> usersData = new Map<String,User_Staging__c>();
        List<User> usersToCreate = new List<User>();
        for(User_Staging__c us : userStagingRecords.values()){
            if(us.Broker_Account__c !=null && us.Broker_Contact__c != null){
                User newUser = new User(
                    Username = us.Broker_Username__c,
                    ContactId = us.Broker_Contact__c,
                    ProfileId = userProfile.Id,
                    Alias = us.First_Name__c.left(4).trim()+''+ us.Last_Name__c.right(4).trim(),
                    Email = us.Email_Address__c,
                    EmailEncodingKey = 'UTF-8',
                    LastName = us.Last_Name__c,
                    FirstName = us.First_Name__c,
                    TimeZoneSidKey = us.Timezone__c,
                    LocaleSidKey = us.Locale__c,
                    LanguageLocaleKey = us.Language__c,
                    Primary_Region__c = us.Primary_Region__c,
                    Offering_Project_for_Brokers__c = us.Project_Offering__c,
                    FederationIdentifier = us.Federation_ID__c
                );
                usersToCreate.add(newUser);
                usersData.put(us.Broker_Username__c, us);
            }
        }
        for(User_Staging__c usc : usersData.values()){
            userStagingRecords.put(usc.Id, usc);
        }

        List<Database.SaveResult> resInsert  = Database.insert(usersToCreate,false);

        for (Integer i = 0; i < usersToCreate.size(); i++) {
            Database.SaveResult s = resInsert[i];
            User origRecord = usersToCreate[i];
            User_Staging__c us = new User_Staging__c();

            if (!s.isSuccess()) {
                us = usersData.get(origRecord.Username);
                if(us.Email_Address__c == origRecord.Email){
                    us.Error_Code__c = ERROR_FOR_USER;
                    us.Error_Code_Details__c = s.getErrors()[0].getMessage();
                    usersData.put(origRecord.Username, us);
                }
             }
             else{
                us = usersData.get(origRecord.Username);
                if(us.Email_Address__c == origRecord.Email){
                    us.Broker_User__c =origRecord.Id;
                    usersData.put(origRecord.Username, us);
                }
             }
        }

        // update master user stage map with  user information
        for(User_Staging__c usc : usersData.values()){
            userStagingRecords.put(usc.Id, usc);
        }

    }

    /*
    @Author: Habiba Zaman
    @param : N/A
    @return : N/A
    @Date: Aug. 8 2017
    @Description: After User creation, delete the sucesssful records.
    */
    public void processUserStageRecords(){

         List<Database.SaveResult> resInsert = Database.update(userStagingRecords.values());

        List<User_Staging__c> recordsToDelete = new List<User_Staging__c>();
        //finding records for which users have been created.
        List<User_Staging__c> updatedRecord = [Select Id, Broker_User__c from User_Staging__c];
        if(!updatedRecord.isEmpty() && updatedRecord != null){
            for(User_Staging__c us: updatedRecord){
                if(us.Broker_User__c != null) //if not null means broker user successfully created
                    recordsToDelete.add(us);
            }

            if(recordsToDelete.size() > 0){
                Database.delete(recordsToDelete);
            }

        }
    }


}