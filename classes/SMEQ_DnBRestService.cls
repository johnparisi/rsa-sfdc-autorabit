@RestResource(urlMapping='/getDnBAddress/*')
global with sharing class SMEQ_DnBRestService {    
    @HttpPost
    global static SMEQ_DnBResponseModel getDnBAddress(SMEQ_DnBRequestModel dnbLookuprequest){
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        SMEQ_DnBResponseModel response = new  SMEQ_DnBResponseModel();
        SMEQ_DnBCallOut dunsCallout = new SMEQ_DnBCallOut();
        try{
           response = dunsCallout.processDnBServiceCallOutRequest(dnbLookuprequest);
        }
        catch(Exception se){
            response.responseCode = SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR; List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>(); SMEQ_RESTResponseModel.ResponseError error = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_600, 'Unexpected System Exception'); errors.add(error); response.setErrors(errors);
        }
        return response;
    }
}