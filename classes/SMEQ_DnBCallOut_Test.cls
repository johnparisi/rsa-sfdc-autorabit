@isTest
global class SMEQ_DnBCallOut_Test implements HttpCalloutMock{
	
	static SMEQ_DnBRequestModel dnBRequestModel = new SMEQ_DnBRequestModel();
	//Expected response when no match found
	static String errorNoMatchResponse ='{"errors":[{"errorCode":"800","errorMessage":"D&B Error","detailErrorMessage":"CP001 No Match for the given input criteria."}],"responseCode":"OK"}';
	//Expected response when an error is returned by the DnB service
	static String errorResponse ='{"errors":[{"errorCode":"450","errorMessage":"Validation of input failed","detailErrorMessage":"object has missing required properties"}],"responseCode":"OK"}';
	//Expected Response
	static String response = '{"responseCode":"OK","DNBResponse":{"GetCleanseMatchResponse":{"@ServiceVersionNumber":"4.0","TransactionDetail":'
						+'{"ServiceTransactionID":"Id-c912e957686c3f008a6f2d00b990c496-1","TransactionTimestamp":"2016-09-26T08:21:30"},'
						+'"TransactionResult":{"SeverityText":"Information","ResultID":"CM000","ResultText":"Success"},"GetCleanseMatchResponseDetail":'
						+'{"InquiryDetail":{"SubjectName":"Tims","Address":{"PrimaryTownName":"Toronto","CountryISOAlpha2Code":"CA","TerritoryName":"ON"}},'
						+'"MatchResponseDetail":{"MatchDataCriteriaText":{"$":"Name and Address Lookup"},"CandidateMatchedQuantity":8,"MatchCandidate":'
						+ '[{"DUNSNumber":"242691066","OrganizationPrimaryName":{"OrganizationName":{"$":"Tim Hortons"}},"PrimaryAddress":'
						+'{"StreetAddressLine":[{"LineText":"647 Coursol Rd"}],"PrimaryTownName":"Sturgeon Falls","CountryISOAlpha2Code":"CA","PostalCode":'
						+'"P2B 0A9","TerritoryAbbreviatedName":"ON","UndeliverableIndicator":false},"MailingAddress":{"CountryISOAlpha2Code":"CA",'
						+'"UndeliverableIndicator":false},"TelephoneNumber":{"TelecommunicationNumber":"7057530804","UnreachableIndicator":false},'
						+ '"OperatingStatusText":{"$":"Active"},"StandaloneOrganizationIndicator":true,"MatchQualityInformation":{"ConfidenceCodeValue":4,'
						+ '"MatchBasis":[{"EndIndicator":false,"SubjectTypeText":"Business","SeniorPrincipalIndicator":false,"MatchBasisText":{"$":'
						+ '"Primary Name"}},{"EndIndicator":false,"SubjectTypeText":"Business","SeniorPrincipalIndicator":false,"MatchBasisText":{"$":'
						+ '"Primary Address"}}],"MatchGradeText":"AZZFAZZZZFZ","MatchGradeComponentCount":11,"MatchGradeComponent":[{"MatchGradeComponentTypeText":'
						+ '{"$":"Name"},"MatchGradeComponentRating":"A","MatchGradeComponentScore":100},{"MatchGradeComponentTypeText":{"$":"Street Number"},'
						+ '"MatchGradeComponentRating":"Z","MatchGradeComponentScore":0},{"MatchGradeComponentTypeText":{"$":"Street Name"},"MatchGradeComponentRating":"Z",'
						+ '"MatchGradeComponentScore":0},{"MatchGradeComponentTypeText":{"$":"City"},"MatchGradeComponentRating":"F","MatchGradeComponentScore":0},'
						+ '{"MatchGradeComponentTypeText":{"$":"State"},"MatchGradeComponentRating":"A","MatchGradeComponentScore":100},{"MatchGradeComponentTypeText":'
						+ '{"$":"PO Box"},"MatchGradeComponentRating":"Z","MatchGradeComponentScore":0},{"MatchGradeComponentTypeText":{"$":"Phone"},"MatchGradeComponentRating":'
						+ '"Z","MatchGradeComponentScore":0},{"MatchGradeComponentTypeText":{"$":"Postal Code"},"MatchGradeComponentRating":"Z","MatchGradeComponentScore":0},'
						+ '{"MatchGradeComponentTypeText":{"$":"Density"},"MatchGradeComponentRating":"Z","MatchGradeComponentScore":0},{"MatchGradeComponentTypeText":{"$":'
						+ '"Uniqueness"},"MatchGradeComponentRating":"F","MatchGradeComponentScore":20},{"MatchGradeComponentTypeText":{"$":"Sic"},"MatchGradeComponentRating":'
						+ '"Z","MatchGradeComponentScore":0}],"MatchDataProfileText":"0099990000999899000098009898","MatchDataProfileComponentCount":14,"MatchDataProfileComponent"'
						+ ':[{"MatchDataProfileComponentTypeText":{"$":"Name"},"MatchDataProfileComponentValue":"00"},{"MatchDataProfileComponentTypeText":{"$":"Street Number"}'
						+ ',"MatchDataProfileComponentValue":"99"},{"MatchDataProfileComponentTypeText":{"$":"Street Name"},"MatchDataProfileComponentValue":"99"},{"MatchDataProfileComponentTypeText"'
						+ ':{"$":"City"},"MatchDataProfileComponentValue":"00"},{"MatchDataProfileComponentTypeText":{"$":"State"},"MatchDataProfileComponentValue":"00"},'
						+ '{"MatchDataProfileComponentTypeText":{"$":"PO Box"},"MatchDataProfileComponentValue":"99"},{"MatchDataProfileComponentTypeText":{"$":"Phone"},'
						+ '"MatchDataProfileComponentValue":"98"},{"MatchDataProfileComponentTypeText":{"$":"Postal Code"},"MatchDataProfileComponentValue":"99"},'
						+ '{"MatchDataProfileComponentTypeText":{"$":"DUNS"},"MatchDataProfileComponentValue":"00"},{"MatchDataProfileComponentTypeText":{"$":"SIC"},'
						+ '"MatchDataProfileComponentValue":"00"},{"MatchDataProfileComponentTypeText":{"$":"Density"},"MatchDataProfileComponentValue":"98"},{"MatchDataProfileComponentTypeText"'
						+ ':{"$":"Uniqueness"},"MatchDataProfileComponentValue":"00"},{"MatchDataProfileComponentTypeText":{"$":"National ID"},"MatchDataProfileComponentValue"'
						+ ':"98"},{"MatchDataProfileComponentTypeText":{"$":"URL"},"MatchDataProfileComponentValue":"98"}]},"DisplaySequence":1}]}}}}}';

	@testSetup static void dataSetupForTestCases() {
	}


	/*
	* This method is for testing when no records found message is retruned by the D&B Service
	* Assertion 1: The payload in the response should be null
	* Assertion 2: The response code in the response model is OK
	**/
	@isTest 
	static void testProcessDnBServiceCallOutRequestNorecordsFound() {
		SMEQ_DnBCallOut dnbCallOut = new SMEQ_DnBCallOut();
		dnbCallOut.dnbSettings = generateDnBSettingsMap();
		Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
		dnBRequestModel.city = 'Toronto';
		dnBRequestModel.name = 'Tims';
		dnBRequestModel.state = 'XX';
		SMEQ_DnBResponseModel responseModel = dnbCallOut.processDnBServiceCallOutRequest(dnBRequestModel);
		System.assert(responseModel.payload == null);
		System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, responseModel.responseCode);
        dnbCallOut.getCompanyMatch('TIms','test','Toronto','XX','test','123','test');
        dnbCallOut.getCompanyMatch(dnBRequestModel );
        dnbCallOut.getCompanyAppend('123');
        dnbCallOut.processDnBServiceCallOutRequest(dnBRequestModel );
	}

	/*
	* This method is for testing when error message is retruned by the D&B Service
	* Assertion 1: The payload in the response should be null
	* Assertion 2: The response code in the response model is ERROR
	**/
	@isTest 
	static void testProcessDnBServiceCallOutRequestErrorReturned() {
		SMEQ_DnBCallOut dnbCallOut = new SMEQ_DnBCallOut();
		dnbCallOut.dnbSettings = generateDnBSettingsMap();
		Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
		dnBRequestModel.city = 'Toronto';
		dnBRequestModel.name = 'Tims';
		dnBRequestModel.state = 'NO';
		SMEQ_DnBResponseModel responseModel = dnbCallOut.processDnBServiceCallOutRequest(dnBRequestModel);
		System.assert(responseModel.payload == null);
		System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, responseModel.responseCode);
	}


	/*
	* This method is for testing when proper response with records is returned the D&B Service
	* Assertion 1: The payload is list and the size should be 1
	**/
	@isTest 
	static void testProcessDnBServiceCallOutRequest() {
		SMEQ_DnBCallOut dnbCallOut = new SMEQ_DnBCallOut();
		dnbCallOut.dnbSettings = generateDnBSettingsMap();
		Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
		dnBRequestModel.city = 'Toronto';
		dnBRequestModel.name = 'Tims';
		dnBRequestModel.state = 'ON';
		SMEQ_DnBResponseModel responseModel = dnbCallOut.processDnBServiceCallOutRequest(dnBRequestModel);
		System.assertEquals(1, responseModel.payload.size());
	}


	/*
	* This method is for testing the processing of the raw response and build the response model that need to be returned
	* Assertion 1: The payload is list and the size should be 1
	**/
	@isTest 
	static void testProcessRawResponseToBuildDnBResponseModel() {
		SMEQ_DnBResponseModel responseModel = new SMEQ_DnBResponseModel();
		SMEQ_DnBCallOut dnbCallOut = new SMEQ_DnBCallOut();
		dnbCallOut.dnbSettings = generateDnBSettingsMap();
		Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
		dnBRequestModel.city = 'Toronto';
		dnBRequestModel.name = 'Tims';
		dnBRequestModel.state = 'ON';
		SMEQ_DnBCallOutRequest calloutRequest = dnbCallOut.buildCallOutReqestFromDnBRequestModel(dnBRequestModel);
		String responseFromMock = dnbCallOut.buildHttpRequestAndPerformDnBCallOut(calloutRequest);
		Map<String, Object> responseDataMap = (Map<String, Object>)JSON.deserializeUntyped(responseFromMock);
		dnbCallOut.processRawResponseToBuildDnBResponseModel(responseDataMap, responseModel);
		System.assertEquals(1, responseModel.payload.size());
	}



	/*
	* This method is for testing the method that builds the D&B call out request from DnBRequestModel that is passed.
	* Assertion 1: SubjectName set in the call out should be Test
	* Assertion 2: StreetAddressLine1 set in the call out should be Test Street
	* Assertion 3: PrimaryTownName set in the call out should be Test City
	* Assertion 4: PostalCode set in the call out should be Test Postal Code
	* Assertion 5: TelephoneNumber set in the call out should be Test Phone Number
	* Assertion 6: ProductionIndicator set in the call out should be false
	* Assertion 6: SourceSystem set in the call out should be SMESF
	**/
	@isTest 
	static void testBuildCallOutReqestFromDnBRequestModel() {
		dnBRequestModel.name = 'Test';
		dnBRequestModel.streetAddress = 'Test Street';
		dnBRequestModel.city = 'Test City';
		dnBRequestModel.postalCode = 'Test Postal Code';
		dnBRequestModel.phoneNumber = 'Test Phone Number';
		dnBRequestModel.countryCode = 'Test countryCode';
		SMEQ_DnBCallOut dnbCallOut = new SMEQ_DnBCallOut();
		dnbCallOut.dnbSettings = generateDnBSettingsMap();
		SMEQ_DnBCallOutRequest callOutRequest = dnbCallOut.buildCallOutReqestFromDnBRequestModel(dnBRequestModel);
		System.assertEquals('Test', callOutRequest.RestRequest.SubjectName);
		System.assertEquals('Test Street', callOutRequest.RestRequest.StreetAddressLine1);
		System.assertEquals('Test City', callOutRequest.RestRequest.PrimaryTownName);
		System.assertEquals('Test Postal Code', callOutRequest.RestRequest.PostalCode);
		System.assertEquals('Test Phone Number', callOutRequest.RestRequest.TelephoneNumber);
		System.assertEquals(false, callOutRequest.CommonParms.ProductionIndicator);
		System.assertEquals('SMESF', callOutRequest.CommonParms.SourceSystem);
	}
	


	/*
	* This method is for testing building and performing the call out service to DnB.
	* Assertion 1: That we get proper response on calling with proper request
	*/
	@isTest 
	static void testBuildHttpRequestAndPerformDnBCallOut() {
		SMEQ_DnBCallOut dnbCallOut = new SMEQ_DnBCallOut();
		dnbCallOut.dnbSettings = generateDnBSettingsMap();
		Test.setMock(HttpCalloutMock.class, new SMEQ_DnBCallOut_Test());
		dnBRequestModel.city = 'Toronto';
		dnBRequestModel.name = 'Tims';
		dnBRequestModel.state = 'ON';
		SMEQ_DnBCallOutRequest calloutRequest = dnbCallOut.buildCallOutReqestFromDnBRequestModel(dnBRequestModel);
		String responseFromMock = dnbCallOut.buildHttpRequestAndPerformDnBCallOut(calloutRequest);
		System.assertEquals(SMEQ_DnBCallOut_Test.response, responseFromMock);
	}

	/*
	* This method is to populate the Map with configurations for DnB
	*/
	static Map<String, String> generateDnBSettingsMap(){
		Map<String, String> dnbSettings = new Map<String, String>();
		dnbSettings.put('DnBBasicAuthentication', 'Basic YVRZWkpnR2V2dVZzZkVjcjp1QUUyaFdUTXM2N2hzbXp1');
		dnbSettings.put('DnBDefaultCountry', 'CA');
		dnbSettings.put('DnBDomain', 'http://esb-10016.uat.rsabase.com');
		dnbSettings.put('DnBMatchOperationEndpoint', '/v1.0/match');
		dnbSettings.put('DnBProductionIndicator', 'false');
		dnbSettings.put('DnBSourceSystem', 'SMESF');
		return dnbSettings;
	}


	/**
	* This method is for mocking the http call outs for DnB Service. If the state has XX then no records returned response is set as 
	* response body, state has NO then error reponse is set as response body, otherwise; response with proper response is set as response
	* body.
	*/
	global HTTPResponse respond(HTTPRequest req)
    {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        System.debug('From Respond method:'+ dnBRequestModel);
        
        if (dnBRequestModel.state == null)
        {
            res.setBody(SMEQ_DnBCallOut_Test.response);
        }
        else if (dnBRequestModel.state.equals('XX'))
        {
        	res.setBody(SMEQ_DnBCallOut_Test.errorNoMatchResponse);
        }
        else if(dnBRequestModel.state.equals('NO'))
        {
        	res.setBody(SMEQ_DnBCallOut_Test.errorResponse);
        }
        else
        {
        	res.setBody(SMEQ_DnBCallOut_Test.response);
        }
        
        res.setStatusCode(200);
        System.debug('From Respond method Response:' + res.getBody());
        return res;
	}

	
}