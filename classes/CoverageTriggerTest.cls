/**
 * Author: James Lee
 * Created At: November 10, 2016
 * 
 * Unit tests for CoverageTrigger.
 */
@isTest
public class CoverageTriggerTest
{
    public static final Integer EARTHQUAKE_LIMIT_VALUE = 2500;
    public static final Integer FLOOD_LIMIT_VALUE = 2500;
    public static final Integer SEWERBACKUP_LIMIT_VALUE = 2500;
    public static final Integer EARTHQUAKE_DEDUCTIBLE_VALUE = 2500;
    public static final Integer FLOOD_DEDUCTIBLE_VALUE = 2500;
    public static final Integer SEWERBACKUP_DEDUCTIBLE_VALUE = 2500;
    
    public static final String COVERAGE_TYPE_EARTHQUAKE = 'Earthquake';
    public static final String COVERAGE_TYPE_FLOOD = 'Flood';
    public static final String COVERAGE_TYPE_SEWERBACKUP = 'Sewer Backup';
    
	static testMethod void testSynchronizedLimitsAndDeductiblesForExtraCoverages()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk();
            
            List<Coverages__c> standardExtraCoverages = new List<Coverages__c>();
            Coverages__c sEarthquake = SMEQ_TestDataGenerator.getExtraCoverage(r, RiskService.PACKAGE_STANDARD, COVERAGE_TYPE_EARTHQUAKE);
            Coverages__c sFlood = SMEQ_TestDataGenerator.getExtraCoverage(r, RiskService.PACKAGE_STANDARD, COVERAGE_TYPE_FLOOD);
            Coverages__c sSewerBackup = SMEQ_TestDataGenerator.getExtraCoverage(r, RiskService.PACKAGE_STANDARD, COVERAGE_TYPE_SEWERBACKUP);
            
            sEarthquake.Limit_Value__c = EARTHQUAKE_LIMIT_VALUE;
            sFlood.Limit_Value__c = FLOOD_LIMIT_VALUE;
            sSewerBackup.Limit_Value__c = SEWERBACKUP_LIMIT_VALUE;
            
            sEarthquake.Deductible_Value__c = EARTHQUAKE_DEDUCTIBLE_VALUE;
            sFlood.Deductible_Value__c = FLOOD_DEDUCTIBLE_VALUE;
            sSewerBackup.Deductible_Value__c = SEWERBACKUP_DEDUCTIBLE_VALUE;
            
            standardExtraCoverages.add(sEarthquake);
            standardExtraCoverages.add(sFlood);
            standardExtraCoverages.add(sSewerBackup);
            update standardExtraCoverages;
            
            List<Coverages__c> extraCoverages = SMEQ_TestDataGenerator.getExtraCoverages(r, RiskService.PACKAGE_OPTION_1, new List<String>{COVERAGE_TYPE_EARTHQUAKE, COVERAGE_TYPE_FLOOD, COVERAGE_TYPE_SEWERBACKUP});
            extraCoverages.addAll(SMEQ_TestDataGenerator.getExtraCoverages(r, RiskService.PACKAGE_OPTION_2, new List<String>{COVERAGE_TYPE_EARTHQUAKE, COVERAGE_TYPE_FLOOD, COVERAGE_TYPE_SEWERBACKUP}));
            insert extraCoverages;
            
            List<Coverages__c> optionalExtraCoverages = [
                SELECT id, risk__c, Limit_Value__c, Deductible_Value__c, Type__c, Package_ID__c
                FROM Coverages__c
                WHERE risk__c = :r.Id
                AND Package_Id__c != :RiskService.PACKAGE_STANDARD
            ];
            
            Test.startTest();
            for (Coverages__c c : optionalExtraCoverages)
            {
                for (Coverages__c cs : standardExtraCoverages)
                {
                    if (cs.Type__c == c.Type__c)
                    {
                        System.assertEquals(cs.Limit_Value__c, c.Limit_Value__c);
                        System.assertEquals(cs.Deductible_Value__c, c.Deductible_Value__c);
                    }
                }
            }
            Test.stopTest();
        }
    }
}