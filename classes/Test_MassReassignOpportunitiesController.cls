@isTest
public class Test_MassReassignOpportunitiesController {

 public static testMethod void testcase1() {
  Reassign_Helper__c RH= new Reassign_Helper__c();
  RH.Assign_to__c=userinfo.getuserid();
  RH.ClosedDate_From__c=system.today();
  RH.From__c=system.today();
  RH.ClosedDate_To__c=system.today();
  RH.To__c=system.today();
  insert RH;
  Opportunity opp = new Opportunity();

  opp.bkrProductLineEffectiveDate__c = Date.today().addDays(12);
  // opp.RecordTypeId = service.getRenewalOpportunityRecordType();
  opp.Name = 'Test opp line item';
  opp.StageName = 'Renewal Assigned';
  opp.Probability = 10.0;
  opp.CloseDate = Date.today();
  opp.OwnerId=RH.Assign_to__c;
  insert opp;
  Task ts= new Task();
  ts.Subject='test task';
  ts.status='Open';
  ts.whatid=opp.id;
  insert ts;

  MassReassignOpportunitiesController MRC= new MassReassignOpportunitiesController();
  MassReassignOpportunitiesController.cOpty innr = new MassReassignOpportunitiesController.cOpty(opp);
  //MRC.optyToUpdateList.add(opp);
  MRC.ErrorMsg='Test error message';
  MRC.helperRecord.From__c=RH.From__c;
  MRC.helperRecord.To__c=RH.To__c;
  MRC.helperRecord.closedDate_From__c=RH.closedDate_From__c;
  MRC.helperRecord.closedDate_To__c=RH.closedDate_To__c;
  MRC.refreshOptyList();
  MRC.refreshOptyListBySearch();
  innr.selected=true;
  MRC.optyList.add(innr);
  MRC.Assign();

 }
}