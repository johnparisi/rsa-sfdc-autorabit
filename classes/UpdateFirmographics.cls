public with sharing class UpdateFirmographics {
       
    public List<DNBCallout.dnbEntry> results { get; set; }
    public Boolean renderResults {
        get {
            return (results != null && !results.isEmpty() && matchWarning == null);
        }
    }
    public DNBCallout.dnbEntry record { get; set; }
    public String updateWarning { get; set; }
    public String matchWarning { get; set; }
    public String duns { get; set; }
    public Account matchingAccount { get; set; }
    public List<Account> matchingAccounts { get; set; }
    private Account acct;
    
    public UpdateFirmographics(ApexPages.StandardController controller) {
        acct = [SELECT Name, bkrAccount_DUNS__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone, Fax, AnnualRevenue, 
            bkrAccount_Employees_Here__c, bkrAccount_Employees_Total__c, bkrAccount_Legal_Status__c, bkrAccount_Company_Start_Year__c, bkrAccount_Single_Location__c, 
            bkrAccount_Out_of_Business_Indicator__c, bkrAccount_Line_of_Business__c, Sic, SicDesc, bkrAccount_Former_Company_Name__c, bkrAccount_Previous_DUNS_Number__c,
            bkrAccount_Industry_Code_1__c, bkrAccount_Industry_Code_2__c, bkrAccount_Industry_Code_3__c, bkrAccount_Industry_Code_4__c, bkrAccount_Industry_Code_5__c,
            bkrAccount_Doing_Business_As_1__c, bkrAccount_Doing_Business_As_2__c, bkrAccount_Doing_Business_As_3__c, bkrAccount_Doing_Business_As_4__c, bkrAccount_Doing_Business_As_5__c,            
            Last_Firmographics_Update__c
            FROM Account WHERE ID = :controller.getId()];
    }
    
    public PageReference checkLastUpdate() {
        if (acct.Last_Firmographics_Update__c != null && acct.Last_Firmographics_Update__c > Date.today().addDays(-60)) {
            updateWarning = 'Firmographics has been updated within the last 60 days. Are you sure you want to continue?';
            return null;
        }
        if (String.isBlank(acct.bkrAccount_DUNS__c)) {
            updateWarning = 'No DUNS Number found on the Account. Please add a DUNS Number before Continuing.';
            return null;
        }
        else {
            return searchDnB();
        }
    }
    
    public PageReference searchDnB() {
        updateWarning = null;
        matchWarning = null;
        results = new List<DNBCallout.dnbEntry>();
        try {
            if (acct.bkrAccount_DUNS__c == null) {
                String countryCode;
                if (acct.BillingCountry.toUpperCase() == 'CANADA' || acct.BillingCountry.toUpperCase() == 'CAN' || acct.BillingCountry.toUpperCase() == 'CA') {
                    countryCode = 'CA';
                }
                else if (acct.BillingCountry.toUpperCase() == 'UNITED STATES' || acct.BillingCountry.toUpperCase() == 'US' || acct.BillingCountry.toUpperCase() == 'USA') {
                    countryCode = 'US';
                }
                List<DNBCallout.dnbEntry> tempResults = DNBCallout.LookupByCompanyName(acct.Name, countryCode, acct.BillingStreet, acct.BillingCity, acct.BillingState, acct.BillingPostalCode, null);
                Map<String, Account> matchResultsMap = new Map<String, Account>();
                for (DNBCallout.dnbEntry r : tempResults) {
                    if (r.DUNSNumber != null && r.DUNSNumber != '') {
                        matchResultsMap.put(r.DunsNumber, null);
                    }
                }
                for (Account a : [SELECT ID, Name, bkrAccount_DUNS__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE bkrAccount_DUNS__c IN :matchResultsMap.keySet()]) {
                    matchResultsMap.put(a.bkrAccount_DUNS__c, a);
                }
                for (DNBCallout.dnbEntry r : tempResults) {
                    if (r.DUNSNumber != null && r.DUNSNumber != '') {
                        Decimal confidence = 0;
                        try {
                            confidence = Decimal.valueOf(r.ConfidenceCode);
                            matchingAccount = matchResultsMap.get(r.DUNSNumber);
                            if (matchingAccount != null) {
                                r.ExistingAccountId = matchingAccount.Id;
                            }
                            results.add(r);
                        }
                        catch (exception e) {                            
                        }
                        if (confidence >= 6) {
                            if (r.ExistingAccountId != null) {
                                //matchWarning = 'Account already exists with D-U-N-S Number: ' + r.DUNSNumber + '. Click <a href="/' + r.ExistingAccountId + '" target="_top">here</a>';
                                matchWarning = 'Existing Account found. Please use the existing Account.';
                                matchingAccounts = new List<Account>{matchingAccount};
                                return null;
                            }
                            acct.bkrAccount_DUNS__c = r.DUNSNumber;
                            continue;
                        }
                    }
                }
                if (results == null || results.isEmpty()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'No Results returned'));
                    return null;
                }
                if (acct.bkrAccount_DUNS__c == null) {
                    return null;
                }
            }            
            return selectRecord();
        }       
        catch (exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
            
    }
    
    public PageReference selectRecord() {
        if (acct.bkrAccount_DUNS__c == null) {
            acct.bkrAccount_DUNS__c = ApexPages.currentPage().getParameters().get('duns');
        }
        record = DNBCallout.lookupCompanyFirmographics(acct.bkrAccount_DUNS__c);
        if (record.DUNSNumber != null && record.DUNSNumber != '') {
            return updateAccount();
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'No Results returned'));
            return null;
        }
    }
    
    public PageReference updateAccount() {
        try {
            /*String country = '';
            if (record.Country != null && record.Country != '') {
                String[] splitCountry = record.Country.trim().split(' ');
                for(integer i = 0; i < splitCountry.size(); i++){
                    Country += splitCountry[i].toLowerCase().capitalize() + ' ';
                }
                country = country.trim();
                //Country = record.Country.toLowerCase().capitalize();
            }*/
            update new Account (
                Id = acct.Id,
                Name = record.Company != null && record.Company != '' ? record.Company : acct.Name,
                bkrAccount_DUNS__c = acct.bkrAccount_DUNS__c,
                BillingStreet = record.Address != null && record.Address != '' ? record.Address : acct.BillingStreet,
                BillingCity = record.City != null && record.City != '' ? record.City : acct.BillingCity,
                BillingState = record.State != null && record.State != '' ? record.State : acct.BillingState,
                BillingPostalCode = record.ZipCode != null && record.ZipCode != '' ? record.ZipCode : acct.BillingPostalCode,
                BillingCountry = record.Country != null && record.Country != '' ? record.Country : acct.BillingCountry,
                //BillingCountry = country != '' ? country : acct.BillingCountry,
                Phone = record.Phone != null && record.Phone != '' ? record.Phone : acct.Phone,                          
                Fax = record.Fax != null && record.Fax != '' ? record.Fax : acct.Fax,                          
                AnnualRevenue = record.AnnualSalesUSDollars != null && record.AnnualSalesUSDollars != '' ? Decimal.valueOf(record.AnnualSalesUSDollars) : acct.AnnualRevenue,
                bkrAccount_Employees_Here__c = record.EmployeesHere != null && record.EmployeesHere != '' ? Decimal.valueOf(record.EmployeesHere) : acct.bkrAccount_Employees_Here__c,
                bkrAccount_Employees_Total__c = record.EmployeesTotal != null && record.EmployeesTotal != '' ? Decimal.valueOf(record.EmployeesTotal) : acct.bkrAccount_Employees_Total__c,
                bkrAccount_Legal_Status__c = record.LegalStatus != null && record.LegalStatus != '' ? record.LegalStatus : acct.bkrAccount_Legal_Status__c,
                bkrAccount_Company_Start_Year__c = record.CompanyStartYear!= null && record.CompanyStartYear!= '' ? Decimal.valueOf(record.CompanyStartYear) : acct.bkrAccount_Company_Start_Year__c,
                bkrAccount_Single_Location__c = record.SingleLocation != null && record.SingleLocation != '' ? (record.SingleLocation.toUpperCase() == 'TRUE') : acct.bkrAccount_Single_Location__c,                
                bkrAccount_Out_of_Business_Indicator__c = record.OutOfBusinessInd != null && record.OutOfBusinessInd != '' ? (record.OutOfBusinessInd.toUpperCase() == 'TRUE') : acct.bkrAccount_Out_of_Business_Indicator__c,
                bkrAccount_Line_of_Business__c = record.LineOfBusiness != null && record.LineOfBusiness != '' ? record.LineOfBusiness : acct.bkrAccount_Line_of_Business__c,
                SicDesc = record.LineOfBusiness != null && record.LineOfBusiness != '' ? record.LineOfBusiness : acct.SicDesc,
                Sic = record.IndustryCode1 != null && record.IndustryCode1 != '' ? record.IndustryCode1 : acct.Sic,
                bkrAccount_Industry_Code_1__c = record.IndustryCode1 != null && record.IndustryCode1 != '' ? record.IndustryCode1 : acct.bkrAccount_Industry_Code_1__c,
                bkrAccount_Industry_Code_2__c = record.IndustryCode2 != null && record.IndustryCode2 != '' ? record.IndustryCode2 : acct.bkrAccount_Industry_Code_2__c,
                bkrAccount_Industry_Code_3__c = record.IndustryCode3 != null && record.IndustryCode3 != '' ? record.IndustryCode3 : acct.bkrAccount_Industry_Code_3__c,
                bkrAccount_Industry_Code_4__c = record.IndustryCode4 != null && record.IndustryCode4 != '' ? record.IndustryCode4 : acct.bkrAccount_Industry_Code_4__c,
                bkrAccount_Industry_Code_5__c = record.IndustryCode5 != null && record.IndustryCode5 != '' ? record.IndustryCode5 : acct.bkrAccount_Industry_Code_5__c,
                bkrAccount_Former_Company_Name__c = record.FormerCompanyName != null && record.FormerCompanyName != '' ? record.FormerCompanyName : acct.bkrAccount_Former_Company_Name__c,
                bkrAccount_Previous_DUNS_Number__c = record.PreviousDUNSNumber != null && record.PreviousDUNSNumber != '' ? record.PreviousDUNSNumber : acct.bkrAccount_Previous_DUNS_Number__c,
                bkrAccount_Doing_Business_As_1__c = record.DoingBusinessAs1 != null && record.DoingBusinessAs1 != '' ? record.DoingBusinessAs1 : acct.bkrAccount_Doing_Business_As_1__c,
                bkrAccount_Doing_Business_As_2__c = record.DoingBusinessAs2 != null && record.DoingBusinessAs2 != '' ? record.DoingBusinessAs2 : acct.bkrAccount_Doing_Business_As_2__c,
                bkrAccount_Doing_Business_As_3__c = record.DoingBusinessAs3 != null && record.DoingBusinessAs3 != '' ? record.DoingBusinessAs3 : acct.bkrAccount_Doing_Business_As_3__c,
                bkrAccount_Doing_Business_As_4__c = record.DoingBusinessAs4 != null && record.DoingBusinessAs4 != '' ? record.DoingBusinessAs4 : acct.bkrAccount_Doing_Business_As_4__c,
                bkrAccount_Doing_Business_As_5__c = record.DoingBusinessAs5 != null && record.DoingBusinessAs5 != '' ? record.DoingBusinessAs5 : acct.bkrAccount_Doing_Business_As_5__c,
                LastFirmograpicsResponse__c = record.ResponseBody.length() > 32000 ? record.ResponseBody.subString(0, 31999) : record.ResponseBody,
                Last_Firmographics_Update__c = Date.today()               
            );
            return new PageReference('/' + acct.Id);
        }
        catch (exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }
    
}