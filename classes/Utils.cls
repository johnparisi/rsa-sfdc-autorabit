/**
 * Utils
 * Author: Alexander Luksidadi
 * Date: April 26 2016
 * 
 * The object domain is where the real work happens, this is where we will apply the actual business logic. 
 * We have the main Object Domain which should handle all common processing. 
 * Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
 * Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
 * just implement logic in the appopriate method. 
 */
public with sharing class Utils {
	// Build a local cache so that we don't request this multiple times
	private static Map<Schema.SObjectType,Map<String,Id>> rtypesCache;

	static {
		rtypesCache = new Map<Schema.SObjectType,Map<String,Id>>();
	}

	// Returns a map of active, user-available RecordType IDs for a given SObjectType,
	// keyed by each RecordType's unique, unchanging DeveloperName 
	public static Map<String, Id> GetRecordTypeIdsByDeveloperName(Schema.SObjectType token, Boolean showAll){
		// Do we already have a result? 
		Map<String, Id> mapRecordTypes = rtypesCache.get(token);
		// If not, build a map of RecordTypeIds keyed by DeveloperName
		if (mapRecordTypes == null) {
			mapRecordTypes = new Map<String, Id>();
			rtypesCache.put(token,mapRecordTypes);
		} else {
			// If we do, return our cached result immediately!
			return mapRecordTypes;
		}

		// Get the Describe Result
		Schema.DescribeSObjectResult obj = token.getDescribe();

		// Obtain ALL Active Record Types for the given SObjectType token
		// (We will filter out the Record Types that are unavailable
		// to the Running User using Schema information)
		String soql = 
			'SELECT Id, Name, DeveloperName '
			+ 'FROM RecordType '
			+ 'WHERE SObjectType = \'' + String.escapeSingleQuotes(obj.getName()) + '\' '
			+ 'AND IsActive = TRUE';
		List<SObject> results;
		try {
			results = Database.query(soql);
		} catch (Exception ex) {
			results = new List<SObject>();
		}

		// Obtain the RecordTypeInfos for this SObjectType token
		Map<Id,Schema.RecordTypeInfo> recordTypeInfos = obj.getRecordTypeInfosByID();

		// Loop through all of the Record Types we found,
		//      and weed out those that are unavailable to the Running User
		for (SObject rt : results) {  
			if (showAll || recordTypeInfos.get(rt.Id).isAvailable()) {
				// This RecordType IS available to the running user,
				//      so add it to our map of RecordTypeIds by DeveloperName
				mapRecordTypes.put(String.valueOf(rt.get('DeveloperName')),rt.Id);
			}
		}

		return mapRecordTypes;
	}
    
    /*

	@Author: Stephen Piercey
	@Date: May 9 2016

	Method to get a List of Changed objects dynamically based on a List of field names. 

	@param: records - a List of SObject records you want to check for changed fields
	@param: oldRecords - a Map of the same SObjects keyed by Id that will contain the old values
	@param: fieldsToCheck - a List of Strings of the developer names of the fields you want to check
	@return: a list of the SObjects from records that have changed field values.
	*/
	public static List<SObject> getChangedObjects(List<SObject> records, Map<Id, SObject> oldRecords, List<String> fieldsToCheck){
		List<SObject> changedObjects = new List<SObject>();
		SObject tempOldRecord = null;
		for(SObject record : records){
			if(oldRecords.keyset().contains(record.Id)){
				tempOldRecord = oldRecords.get(record.Id);
				for(String field : fieldsToCheck){
					if(record.get(field) != tempOldRecord.get(field)){
						changedObjects.add(record);
						break;
					}
				}
			}
		}
		return changedObjects;
	}
    
    /*
     * @Author: James Lee
     * @Date: October 20 2016
     * 
     * Method to get the name of the object based on the provided ID.
     * 
     * @param: sId the ID of an sObject.
     * @return: The ID's sObject API Name.
     */
    public static String getObjectType(Id sId)
    {
        Map<String, String> mapping = new Map<String, String>();
        
        Map<String, Schema.SobjectType> describe = Schema.getGlobalDescribe();
        for (String s : describe.keyset())
        {
            mapping.put(describe.get(s).getDescribe().getKeyPrefix(), s);
        }
        
        return mapping.get(String.valueOf(sId).substring(0,3));
    }
    
    /*
     * @Author: James Lee
     * @Date: December 19 2016
     * 
     * Method to comma-delimit a list of items.
     * 
     * @param: oList
     * @return: The list of items comma-delimited.
     */
    public static String commaDelimit(List<Object> oList)
    {
        String cList = '';
        
        for (Object o : oList)
        {
            if (cList != '')
            {
                cList += ';';
            }
            
            cList += o;
        }
        
        return cList;
    }

	public static void sendEmailWithTemplate(String templateName, String fromEmailAddress, String relatedToID,  String recipientID)
	{
		EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName =:templateName];
		List<string> toAddress = new List<string>();

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: fromEmailAddress];
		if ( owea.size() > 0 ) {
			mail.setOrgWideEmailAddressId(owea.get(0).Id);
		}

		mail.setTemplateId(et.Id);
		mail.setToAddresses(toAddress);
		mail.setTargetObjectId(recipientID);  //recipient info
		mail.setWhatId(relatedToID);  //related to Info
		mail.setSaveAsActivity(false);
		mail.setUseSignature(false);
		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
		allmsg.add(mail);
		try {
			Messaging.sendEmail(allmsg,false);
			return;
		} catch (Exception e) {
			System.debug(e.getMessage());
		}

	}
}