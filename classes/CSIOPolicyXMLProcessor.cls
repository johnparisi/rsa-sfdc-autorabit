/**
This class takes in an insurance policy in XML format and persists it to the database for the Get Policy transaction response. This differs 
from the CSIOXMLProcessor class which extracts specific attributes across a number of CSIO responses.
*/

public with sharing class CSIOPolicyXMLProcessor
{
    public static final Integer RENEWAL_ELIGIBILITY_PERIOD = 60;
    
    // namespaces
    public static final String ACORD_NAMESPACE = 'http://www.ACORD.org/standards/PC_Surety/ACORD1/xml/';
    public static final String SOAP_NAMESPACE = 'http://schemas.xmlsoap.org/soap/envelope/';
    public static final String RSA_NAMESPACE = 'http://www.rsagroup.ca/schema/custom/xml/';
    public static final String CSIO_NAMESPACE = 'http://www.CSIO.org/standards/PC_Surety/CSIO1/xml/';
    
    private String xml; // this is the XML policy document that is being processed
    private Quote__c policy;
    private Account insuredAccount;
    private Case raCase;
    private List < Risk__c > locationList;
    private Map < Quote_Risk_Relationship__c, Account > accountQrrMap;
    private Map < Quote_Risk_Relationship__c, Contact > contactQrrMap;
    private List < Policy_History__c > policyHistoryList;
    private List < Coverages__c > coverageList;
    private Risk__c cglRisk;
    private Map < String, Risk__c > idRiskMap;
    private Map < String, Set < Quote_Risk_Relationship__c >> locationInterest;
    private Set < Quote_Risk_Relationship__c > qrrSet;
    private List < Quote_Risk_Relationship__c > qrrSObjectKeys = new List < Quote_Risk_Relationship__c > ();
    private List < Coverages__c > coveragesList;
    private Session_Information__c session = null;
	private String caseOffering = '';
    private Map<Id, Map<SIC_Question__c, SIC_Answer__c>> quoteSicAnswers = new Map<Id, Map<SIC_Question__c, SIC_Answer__c>>();
    private List<SIC_Question__c> sicQuestions = new List<SIC_Question__c>();
    private SIC_Code_Detail_Version__c sicCodeDetailVersion;
    static final Integer MaxRiskLimit = 5;
    public CSIOPolicyXMLProcessor(Case raCase, String xml) {
        this.xml = xml;
        this.raCase = raCase;
    }

    /**
    Processes the policy xml document passed in from the constructor
    */

    public void process() {
		
        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(xml);
        Dom.XMLNode soapNode = xmlDocument.getRootElement().getChildElement('Body', CSIOPolicyXMLProcessor.SOAP_NAMESPACE);
        Dom.XMLNode acordNode = soapNode.getChildElement('ACORD', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode signOnRs = acordNode.getChildElement('SignonRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XmlNode nameNode = signOnRs.getChildElement('ClientApp', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Name', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode insuranceSvcRsNode = acordNode.getChildElement('InsuranceSvcRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode PolicySyncRs = insuranceSvcRsNode.getChildElement('PolicySyncRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode CommlPkgPolicy = PolicySyncRs.getChildElement('CommlPkgPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode insuredNode = CommlPkgPolicy.getChildElement('InsuredOrPrincipal', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XmlNode insuredInfoNode = insuredNode.getChildElement('InsuredOrPrincipalInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XmlNode businessInfoNode = insuredInfoNode.getChildElement('BusinessInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlPolicyNode = CommlPkgPolicy.getChildElement('CommlPolicy', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode CommlScheduleEstablishment = commlPolicyNode.getChildElement('PaymentOption', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlPropertyLineBusinessNode = CommlPkgPolicy.getChildElement('CommlPropertyLineBusiness', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode propertyInfoNode = commlPropertyLineBusinessNode.getChildElement('PropertyInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode generalLiabilityLineBusinessNode = CommlPkgPolicy.getChildElement('GeneralLiabilityLineBusiness', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode liabilityInfoNode = generalLiabilityLineBusinessNode.getChildElement('LiabilityInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode commlCoverageNode = liabilityInfoNode.getChildElement('CommlCoverage', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XmlNode generalLiabilityNode = liabilityInfoNode.getChildElement('GeneralLiabilityClassification', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        List < Dom.XMLNode > cglXMLNodeList = new List < Dom.XMLNode > ();
        cglXMLNodeList.add(commlCoverageNode);
		
        //delete old quotes from case 
        if (raCase != null) {
            delete [SELECT Id FROM Quote__c WHERE Case__r.Id =: raCase.Id];
        } else {
            raCase = new Case();
        }


        //mapping the insured 
        Map < String, String > policyStatusType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('ePolicy_Status__c');
        String ePolicy_StatusCd = CommlPkgPolicy.getChildElement('PolicySummaryInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('PolicyStatusCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
        //TBD when data is complete in ePolicy, update this
        raCase.ePolicy_Status__c = policyStatusType.get(ePolicy_StatusCd) != null ? policyStatusType.get(ePolicy_StatusCd) : 'New Business (Quoted)';
        CSIOPolicyXMLInsuredProcessor insuredProcessor = new CSIOPolicyXMLInsuredProcessor();
        insuredProcessor.process(raCase, insuredNode);
        

        //Adding the additional insureds into our maps so they can be inserted as QRRs, Accounts, Contacts 
        accountQrrMap = new Map < Quote_Risk_Relationship__c, Account > ();
        contactQrrMap = new Map < Quote_Risk_Relationship__c, Contact > ();
        accountQRRmap.putAll(insuredProcessor.accountQrrMap);
        upsert insuredProcessor.insuredAccount;
        raCase.bkrCase_Insured_Client__c = insuredProcessor.insuredAccount.Id;

        raCase.New_Business_Source__c = nameNode.getText();
        upsert raCase;

        // generate session record
        String sessionId = insuranceSvcRsNode.getChildElement('SessionId', CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null ? insuranceSvcRsNode.getChildElement('SessionId', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText() : null;
        if (RA_SessionManagementUtil.getSession(raCase.Id) == null){
	        RA_SessionManagementUtil.createSession(raCase, sessionId );
        }
        //mapping the policy 
        CSIOPolicyCommlPolicyXMLProcessor policyProcessor = new CSIOPolicyCommlPolicyXMLProcessor();
        policyProcessor.process(commlPolicyNode, raCase);
        policy = policyProcessor.policy;
        policy.Case__c = raCase.Id;
        // commented out as was pulling the QPC number from the case rather than the COM #
        //policy.ePolicy__c = raCase.Com__c; 
        policy.Sic_Code_Description__c = businessInfoNode.getChildElement('OperationsDesc', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
        policy.Total_Number_of_Units_on_Policy__c = businessInfoNode.getChildElement('NumberOfUnits', CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null ?
            Decimal.valueOf(businessInfoNode.getChildElement('NumberOfUnits', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText()) : 0;
        policy.Business_Start_Date__c = Date.today();
        
        Dom.XmlNode ItemIdInfo = CommlPkgPolicy.getChildElement('PolicySummaryInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('ItemIdInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        String checkOutUser = null;
        if(ItemIdInfo != null){
        	 checkOutUser = ItemIdInfo.getChildElement('OtherIdentifier', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('OtherId', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();        
        }
        
        if (checkOutUser == null || checkOutUser == '' || checkOutUser == UserService.getUser(UserInfo.getUserId()).FederationIdentifier) {
            raCase.Locked_in_External_Policy_System__c = false;
        } else {
            raCase.Locked_in_External_Policy_System__c = true;
        }
        policy.Name_Insured__c = insuredProcessor.insuredAccount.Name;
        if (policyProcessor.policy.Effective_Date__c != null) {
            policy.Effective_Date__c = policyProcessor.policy.Effective_Date__c;
        } else {
            policy.Effective_Date__c = Date.valueOf(PolicySyncRs.getChildElement('TransactionEffectiveDt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
        }
        
        if (UserService.getUser(UserInfo.getUserId()).Profile.Name.contains(UserService.PROFILE_BROKER))
        {
            Map<Id, String> caseOrigins = CaseService.brokerOrigin(new List<Id>{UserInfo.getUserId()});
            raCase.Origin = caseOrigins.get(UserInfo.getUserId());
            
            String userOffering = UserService.getOffering(UserInfo.getUserId())[0];
            if (!userOffering.contains(UserService.OFFERING_SPRNT))
            {
                raCase.Offering_Project__c = userOffering; // Explicitly assign value if the user is a broker.
            }
        }
        
        if (raCase.bkrCase_Subm_Type__c == null && raCase.COM__c != null)
        {
            Date datePolicyBecomesEligibleForRenewal = policy.Quote_Expiry_Date__c - RENEWAL_ELIGIBILITY_PERIOD;
            if (system.today() < (datePolicyBecomesEligibleForRenewal)) //outside renewal period
            {
                raCase.bkrCase_Subm_Type__c = 'Complex Amendment';
            }
            else if (datePolicyBecomesEligibleForRenewal <= system.today() && system.today() <= policy.Quote_Expiry_Date__c) //inside renewal period
            {
                raCase.bkrCase_Subm_Type__c = 'Renewal';
            }
        }
        Map < String, Id > quoteTypes = Utils.GetRecordTypeIdsByDeveloperName(Quote__c.SObjectType, true);
        policy.RecordTypeId = quoteTypes.get('Policy');
        CSIOPolicyAddrXMLProcessor addressProcessor = new CSIOPolicyAddrXMLProcessor();
        Boolean hasCSIO1 = false;
        for (Dom.XmlNode insuredChildNode: insuredNode.getChildElement('GeneralPartyInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElements()){
            if (insuredChildNode.getName() == 'Addr'){
                //if the addressCd is csio:10 that indicates that it is a primary address, otherwise it is a mailing address (csio:1)
        		if (insuredChildNode.getChildElement('AddrTypeCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() == 'csio:1') {
                    addressProcessor.process(insuredChildNode);
                    policy.Address_Line_1__c = addressProcessor.streetAddress;
                    policy.City__c = addressProcessor.city;
                    policy.Country__c = addressProcessor.country;
                    policy.State_Province__c = addressProcessor.province;
                    policy.Postal_Code__c = addressProcessor.postalCode;
                    hasCSIO1 = true;
                }
                if (insuredChildNode.getChildElement('AddrTypeCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE). getText() == 'csio:10' && !hasCSIO1){
                    policy.Mailing_Address_Is_The_Same__c = true;
                }
            }
        }
        update raCase;  //The case needs to get updated so when the quote validation fires it has the latest info (e.g. type = Renewal)		
        insert policy;

        //the claim is associated to the account?
        for (Claim__c claim: policyProcessor.claimList) {
            claim.Quote__c = policy.Id;
        }

        insert policyProcessor.claimList;

        accountQRRmap.putAll(policyProcessor.accountQrrMap);
        contactQRRmap.putAll(policyProcessor.contactQrrMap);

        policyHistoryList = policyProcessor.policyHistoryList;
        for (Policy_History__c recentTransaction: policyHistoryList) {
            recentTransaction.Related_Quote__c = policy.Id;
        }

        insert policyHistoryList;

        map < String, LocationSubLocationAssociation > idLocationXMLNodeMap = new Map < String, LocationSubLocationAssociation > ();


        //mapping locations by id
        for (Dom.xmlNode nodeChild: CommlPkgPolicy.getChildElements()) {
            if (nodeChild.getName() == 'Location') {
                LocationSubLocationAssociation association = new LocationSubLocationAssociation();
                association.locationXMLNode = nodeChild;
                idLocationXMLNodeMap.put(nodeChild.getAttribute('id', null), association);
            }

        }

        //mapping sublocation by id to the location
        for (Dom.xmlNode nodeChild: CommlPkgPolicy.getChildElements()) {
            if (nodeChild.getName() == 'CommlSubLocation') {
                idLocationXMLNodeMap.get(nodeChild.getAttribute('LocationRef', null)).subLocationXMLNode = nodeChild;
            }
        }

        idRiskMap = new Map < String, Risk__c > ();
        locationInterest = new Map < String, Set < Quote_Risk_Relationship__c >> ();
        //mapping location and sublocation
        Integer index = 0;
        for (LocationSubLocationAssociation tempAssociation: idLocationXMLNodeMap.values()) {
            CSIOPolicyLocationXMLProcesser locationProcessor = new CSIOPolicyLocationXMLProcesser();
            locationProcessor.process(tempAssociation.locationXMLNode, tempAssociation.subLocationXMLNode);
            addressProcessor = new CSIOPolicyAddrXMLProcessor();
            for (Dom.XmlNode nodeChild: tempAssociation.locationXMLNode.getChildElements()) {
                if (nodeChild.getName() == 'Addr') {
                    addressProcessor.process(nodeChild);
                    locationProcessor.location.Address_Line_1__c = addressProcessor.streetAddress;
                    locationProcessor.location.City__c = addressProcessor.city;
                    locationProcessor.location.Country__c = addressProcessor.country;
                    locationProcessor.location.Province__c = addressProcessor.province;
                    locationProcessor.location.Postal_Code__c = addressProcessor.postalCode;
                    //if the mailing address is the same as primary address, populate the mailing address as addressTypeCd CSIO:1 will not come in 
                    if (policy.Country__c == null) {
                        policy.Address_Line_1__c = addressProcessor.streetAddress;
                        policy.City__c = addressProcessor.city;
                        policy.Country__c = addressProcessor.country;
                        policy.State_Province__c = addressProcessor.province;
                        policy.Postal_Code__c = addressProcessor.postalCode;
                    }
                }
            }

            idRiskMap.put(tempAssociation.locationXMLNode.getAttribute('id', null), locationProcessor.location);
            locationProcessor.location.Quote__c = policy.Id;
            if (index == 0 && !locationProcessor.location.Deleted__c){
                locationProcessor.location.Primary_Location__c = true;
            }
            qrrSet = new Set < Quote_Risk_Relationship__c > ();
            qrrSet.addAll(locationProcessor.accountQrrMap.keySet());
            qrrSet.addAll(locationProcessor.contactQrrMap.keySet());
            locationInterest.put(tempAssociation.locationXMLNode.getAttribute('id', null), qrrSet);
            accountQRRmap.putAll(locationProcessor.accountQrrMap);
            contactQRRmap.putAll(locationProcessor.contactQrrMap);
            if(!locationProcessor.location.Deleted__c){
            	index ++;
            }
            
        //setting limit, user should not be able to work on this policy in SF/broker tool if more than 5 locations are on it
       
            if (index > MaxRiskLimit) {
                raCase.Restriction_limit__c = true;
                break;
        	}
        }
        
		update raCase;
        
        //A trigger updates the Monoline field based on SIC Code that is updated in the insured section
        raCase = [SELECT id, Monoline__c, Segment__c, Restriction_limit__c, Offering_Project__c, bkrCase_Region__c, Resume_Transaction__c
                  FROM Case WHERE id =: raCase.Id];
         //if this is a monoline SIC Code, then the rest of the parsing does not need to be completed, record should not be created in SF
        if (raCase.Monoline__c || raCase.Restriction_limit__c ) {
            throw new RSA_ExceedsToolLimitsException(Label.Exceeds_limit_of_tool);
        }
        if(raCase.bkrCase_Region__c == null){
            String userRegion = UserService.getRegion(UserInfo.getUserId())[0];
            raCase.bkrCase_Region__c = userRegion;
        }
        
        Integer newPremium = Integer.valueOf(CommlPkgPolicy.getChildElement('PolicySummaryInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('FullTermAmt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Amt', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
        //Integer oldPremium = Integer.valueOf(CommlPkgPolicy.getChildElement('PolicySummaryInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('PriorTermPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElement('Amt',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
        Integer oldPremium = 0;
        String lastTransactionPremium = '';
        for (Dom.XMLNode policyHistoryXmlNode: commlPolicyNode.getChildElement('RecentTransactions',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElements()) {
            if (lastTransactionPremium != null && lastTransactionPremium != '') {
                oldPremium = Integer.valueOf(lastTransactionPremium);
            }
            lastTransactionPremium = policyHistoryXmlNode.getChildElement('TermPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE) != null? policyHistoryXmlNode.getChildElement('TermPremium',CSIOPolicyXMLProcessor.RSA_NAMESPACE).getChildElement('Amt',CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() : null;
        }
        if (oldPremium == 0 &&  lastTransactionPremium != '') oldPremium = Integer.valueOf(lastTransactionPremium);

        // There is some logic here that resume is false but it should still set new premium
        //if (newPremium != oldPremium  && newPremium != null && raCase.Resume_Transaction__c){
        if (newPremium != null){
            policy.New_Premium__c = newPremium;
        }
        policy.Premium__c = oldPremium;
        policy.Standard_Premium__c = oldPremium;
        //update region
        update raCase;
        
        //inserting riskList
        insert idRiskMap.values();
        // Retrieve the set of risk IDs.
        List<id> riskandPolicyIds = new List<id>();
		for(Risk__c risk: idRiskMap.values()){
            riskandPolicyIds.add(risk.Id);
        }
        
        // Re-query for the risks now with case ID.
        Map<Id, Risk__c> rs = new Map<Id, Risk__c>([
            Select id, Quote__c, Quote__r.Case__c
            FROM Risk__c
            WHERE id IN :riskandPolicyIds
        ]);

        // Replace mapped risk record.
        for (String riskValue : idRiskMap.keySet())
        {
            idRiskMap.put(riskValue, rs.get(idRiskMap.get(riskValue).Id));
        }


        riskandPolicyIds.add(policy.Id);
        //mapping the coverages 
        coverageList = new List < Coverages__c > ();
        List<Coverages__c> filteredCoverageList = new List < Coverages__c > ();
		caseOffering = raCase.Offering_Project__c;
        sicCodeDetailVersion = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(raCase.id)[0];
        sicQuestions = SicQuestionService.getSicQuestions(sicCodeDetailVersion);
        quoteSicAnswers = SicQuestionService.getSicAnswers(riskandPolicyIds, sicQuestions);
        
        Set<String> enabledCoverages = CoverageService.getEnabledCoverages(raCase.id);
        
        CSIOPolicyXMLCoverageProcessor coverageProcessor = new CSIOPolicyXMLCoverageProcessor();
        coverageList = new List<Coverages__c>();
        for (Dom.xmlNode nodeChild: propertyInfoNode.getChildElements()) {
            coverageProcessor = null;
            if (nodeChild.getName() == 'CommlPropertyInfo') {
                
                coverageProcessor = new CSIOPolicyXMLCoverageProcessor();
                coverageProcessor.process(nodeChild.getChildElements(), idRiskMap.get(nodeChild.getAttribute('LocationRef', null)), quoteSicAnswers, sicCodeDetailVersion, enabledCoverages);
                
				filteredCoverageList = new List<Coverages__c>();
                
                for (Coverages__c coverage : coverageProcessor.coverageList) {
                    if (caseOffering.contains('SPRNT v1')) {
                        if (coverage.Type__c.contains(RiskService.COVERAGE_BUILDING) || 
                            coverage.Type__c.contains(RiskService.COVERAGE_STOCK) ||
                            coverage.Type__c.contains(RiskService.COVERAGE_EQUIPMENT) ||
                            coverage.Type__c.contains(RiskService.COVERAGE_FLOOD) ||
                            coverage.Type__c.contains(RiskService.COVERAGE_SEWER) ||
                            coverage.Type__c.contains(RiskService.COVERAGE_EARTHQUAKE) ||
                            coverage.Type__c.contains(RiskService.COVERAGE_CONTENTS)) {
                            filteredCoverageList.add(coverage); 
                        }
                    } else {
                      filteredCoverageList.add(coverage); 
                    }
                }   
                coverageList.addall(filteredCoverageList);
            }
        }
        
        for(String riskId : idRiskMap.keySet()) {
            Dom.XmlNode commlSubLocationNode = idLocationXMLNodeMap.get(riskId).subLocationXMLNode;
            Dom.XmlNode exposureNode = commlSubLocationNode.getChildElement('ExposureInfo', ACORD_NAMESPACE);
            for (Sic_Question__c question: quoteSicAnswers.get(idRiskMap.get(riskId).id).keyset()) {
                if (question.questionId__c == Decimal.valueOf(SicQuestionService.CODE_NUMBER_OF_UNITS)) {
                    quoteSicAnswers.get(idRiskMap.get(riskId).id).get(question).Value__c = '0';
                    break; // Default number of units on locations to 0 when retrieving from policy.
                }
            }
            Map < String, String > facilityTypes = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionOnsiteFacilities');
            for (Sic_Question__c question: quoteSicAnswers.get(idRiskMap.get(riskId).id).keyset()) {
                List<String> exposureCodes = new List<String>();
                if (question.questionId__c == Decimal.valueOf(SicQuestionService.CODE_ONSITE_FACILITIES)) {
                    if (exposureNode != null) {
                        for (Dom.XmlNode child: exposureNode.getChildElements()) {
                            if (child.getName() == 'ExposureCd') {
                                exposureCodes.add(facilityTypes.get(child.getText()));
                            }
                        }
                    }
                    else
                    {
                        // Default value when none are returned.
                        exposureCodes.add(facilityTypes.get('No Recreational Facilities'));
                    }
                    quoteSicAnswers.get(idRiskMap.get(riskId).id).get(question).Value__c =  Utils.commaDelimit(exposureCodes);
                    break;
                }
            }
            
        }

        // read from XML to build coverages instead of using trigger like in NB (and turn off below)
        RiskDomain.suppressTrigger = true;

        // changing from update to upsert - accommodating situations where the coverage already exists
        upsert idRiskMap.values();

        RiskDomain.suppressTrigger = false;

        //mapping CGL 

        CSIOPolicyAdditionalPartyProcessor partyProcessor = new CSIOPolicyAdditionalPartyProcessor();
        cglRisk = new Risk__c();
        cglRisk.Quote__c = policy.Id;
        Map < String, Id > riskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
        cglRisk.RecordTypeId = riskTypes.get(riskservice.RISK_RECORDTYPE_LIABILITY);
        insert cglRisk;
        
        cglRisk = [Select id, Quote__c, Quote__r.Case__c FROM Risk__c WHERE id = :cglRisk.Id];
            

       //Mapping the CGL coverage 
        coverageProcessor = new CSIOPolicyXMLCoverageProcessor();
        coverageProcessor.process(cglXMLNodeList, cglRisk, quoteSicAnswers, sicCodeDetailVersion, enabledCoverages);
        coverageList.addall(coverageProcessor.coverageList);

        integer indexNum = 1;
        //Mapping the liability additional insureds 
        for (Dom.xmlNode nodeChild: generalLiabilityLineBusinessNode.getChildElements()) {
            if (nodeChild.getName() == 'AdditionalInterest') {
                partyProcessor.process(nodeChild);
                partyProcessor.quoteRiskRelationship.Party_Number__c = indexNum;
                partyProcessor.quoteRiskRelationship.Related_Risk__c = cglRisk.Id;
                partyProcessor.quoteRiskRelationship.Quote__c = policy.Id;
                partyProcessor.quoteRiskRelationship.Party_Type__c = 'Liability Additional Insured';
                String deletedLiabilityAdditionalInsured = nodeChild.getChildElement('Deleted', CSIOPolicyXMLProcessor.RSA_NAMESPACE).getText();
                if (deletedLiabilityAdditionalInsured == '1'){
                    partyProcessor.quoteRiskRelationship.Deleted__c = true;
                } else {
                    partyProcessor.quoteRiskRelationship.Deleted__c = false;
                }
                if (partyProcessor.isAccount) {
                    accountQrrMap.put(partyProcessor.quoteRiskRelationship, partyProcessor.partyAccount);
                } else {
                    contactQrrMap.put(partyProcessor.quoteRiskRelationship, partyProcessor.partyContact);
                }
                indexNum++;
            }
        }
        
        //Mapping the revenue based on region/ populating depending on the SIC answers 
        for (Dom.xmlNode nodeChild: liabilityInfoNode.getChildElements()) {
            if (nodeChild.getName() == 'GeneralLiabilityClassification') {
                Dom.XmlNode territoryNode = nodeChild.getChildElement('TerritoryCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                Dom.XmlNode exposureInfoNode = nodeChild.getChildElement('ExposureInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
                Map<String, String> esbPremiumBasisCd = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('esbPremiumBasisCd');
                if (territoryNode != null) {
                    if (territoryNode.getText().contains('CA')) {
                        for (Sic_Question__c question: quoteSicAnswers.get(policy.id).keyset()){
                        	if(question.questionId__c == Decimal.valueOf(SicQuestionService.CODE_CANADIAN_REVENUE)){
                                quoteSicAnswers.get(policy.id).get(question).Value__c = evaluateRevenuePath(exposureInfoNode);
                                break;
                            }
                        }
                    }
                    
                    if (territoryNode.getText() == 'US') {
                        for (Sic_Question__c question: quoteSicAnswers.get(policy.id).keyset()){
                            if(question.questionId__c == Decimal.valueOf(SicQuestionService.CODE_FOREIGN_REVENUE)){
                                quoteSicAnswers.get(policy.id).get(question).Value__c = evaluateRevenuePath(exposureInfoNode);
                                break;
                            }
                        }
                    }
                    if (territoryNode.getText() == 'ZZ') {
                        for (Sic_Question__c question: quoteSicAnswers.get(policy.id).keyset()){
                            if(question.questionId__c == Decimal.valueOf(SicQuestionService.CODE_US_REVENUE)){
                                quoteSicAnswers.get(policy.id).get(question).Value__c = evaluateRevenuePath(exposureInfoNode);
                                break;
                            }
                        }
                    }
                    if (territoryNode.getText() == 'Total') {
                        policy.Total_Revenue__c = Decimal.valueOf(exposureInfoNode.getChildElement('Exposure', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
                	}
                }
                else {
                    if (exposureInfoNode.getChildElement('PremiumBasisCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null) {
                        for (Sic_Question__c question : quoteSicAnswers.get(policy.id).keyset()) {
                            if (question.questionId__c == Decimal.valueOf(esbPremiumBasisCd.get(exposureInfoNode.getChildElement('PremiumBasisCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText()))) {
                                quoteSicAnswers.get(policy.id).get(question).Value__c = evaluateRevenuePath(exposureInfoNode);
                                break;
                            }
                        }
                    }
                    if (exposureInfoNode.getChildElement('ExposureCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null) {
                        if (exposureInfoNode.getChildElement('ExposureCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText() == 'rsa:Total'){
                            policy.Total_Revenue__c = Decimal.valueOf(nodeChild.getChildElement('ExposureInfo', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getChildElement('Exposure', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText());
                        }
                    }
                }
            }
        }
        update policy;
        upsert coverageList;

        //associate QRR and Account/Contact
        updateQuoteRiskMap(accountQrrMap);
        updateQuoteRiskMap(contactQrrMap);

        //associating QRR with risk
        for (String riskId: locationInterest.keySet()) {
            Set < Quote_Risk_Relationship__c > qrrSet = locationInterest.get(riskId);
            for (Quote_Risk_Relationship__c qrr: qrrSet) {
                qrr.Related_Risk__c = idRiskMap.get(riskId).id;
            }
        }
        List<Sic_Answer__c> answers = new List<Sic_Answer__c>();
        for(Map<Sic_Question__c, Sic_Answer__c> questionAnswerMap: quoteSicAnswers.values()){
            for(Sic_Question__c question: questionAnswerMap.keySet()){
				
                if (questionAnswerMap.get(question).Value__c == null || questionAnswerMap.get(question).Value__c == ''){
                    
                    throw new RSA_ESBException(question.questionText_en__c  + ' ' +  Label.SIC_Answer_Missing); 
                }
                answers.add(questionAnswerMap.get(question));
            }
        }
        //system.assert(false, answers);
        upsert answers;
        insert qrrSObjectKeys;
    }

    private class LocationSubLocationAssociation {

        public Dom.XMLNode locationXMLNode;
        public Dom.XMLNode subLocationXMLNode;

    }

    public static void debugChildren(Dom.XMLNode inputNode) {
        System.debug('#### enumerating the children of ' + inputNode.getName() + ':' + inputNode.getNamespace());
        for (Dom.XMLNode testNode: inputNode.getChildren()) {
            System.debug('#### ' + testNode.getName() + ':' + testNode.getNamespace());
        }
    }


    private void updateQuoteRiskMap(map < Quote_Risk_Relationship__c, sObject > qrrSObjectMap) {
        system.debug('map' + qrrSObjectMap);
        List < sObject > sObjectList = qrrSObjectMap.values();
        insert sObjectList;

       
        for (Quote_Risk_Relationship__c qrr: qrrSObjectMap.keySet()) {
            sObject mySObject = qrrSobjectMap.get(qrr);
            if (mySobject instanceof Account) {
                qrr.Account__c = mySobject.Id;

            } else {
                qrr.Contact__c = mySobject.Id;
            }
            qrr.Quote__c = policy.Id;
            
        }

        qrrSObjectKeys.addall(qrrSObjectMap.keySet());

    }
    
    public static String evaluatePath(Dom.XmlNode inputNode){
        
        String nodeValue = null;
        
        if (inputNode != null){
            nodeValue = inputNode.getText();
        }
        
        return nodeValue;
        
    }
    
    public static String evaluateRevenuePath(Dom.XmlNode exposureInfoNode)
    {
        String revenueValue = '0';
        
        if (exposureInfoNode != null && exposureInfoNode.getChildElement('Exposure', CSIOPolicyXMLProcessor.ACORD_NAMESPACE) != null){
            revenueValue = exposureInfoNode.getChildElement('Exposure', CSIOPolicyXMLProcessor.ACORD_NAMESPACE).getText();
        }
        
        return revenueValue;
    }

}