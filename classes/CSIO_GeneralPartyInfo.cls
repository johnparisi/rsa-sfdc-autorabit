public class CSIO_GeneralPartyInfo
{
    private Account accountParty;
    private Contact contactParty;
    private Risk__c risk;
    private Quote_Risk_Relationship__c quoteRiskRelationship;
    private Quote__c quote;
    public List<Quote_Risk_Relationship__c> quoteRiskRelationshipList;
    public List<Account> accountPartyList;
    public List<Contact> contactPartyList;
    private NameInfo nameInfo;
    private NameInfo nameInfo1;
    public CSIO_Addr addr;
    public CSIO_Addr mailingAddr;
    public Boolean isMailingAddressSame;

    public CSIO_GeneralPartyInfo(Account account)
    {
        this.accountParty = account;
        convert();
    }

    public CSIO_GeneralPartyInfo(Account account, Risk__c risk)
    {
        this.accountParty = account;
        this.risk = risk;
        this.isMailingAddressSame = risk.Quote__r.Mailing_Address_Is_The_Same__c;
        convert();
    }

    public CSIO_GeneralPartyInfo(Account account, Quote__c quote)
    {
        this.accountParty = account;
        this.quote = quote;
        this.isMailingAddressSame = quote.Mailing_Address_Is_The_Same__c;
        convert();
    }

    public CSIO_GeneralPartyInfo(Contact contact)
    {
        this.contactParty = contact;
        convert();
    }

    public CSIO_GeneralPartyInfo(Contact contact, Risk__c risk)
    {
        this.contactParty = contact;
        this.risk = risk;
        this.isMailingAddressSame = risk.Quote__r.Mailing_Address_Is_The_Same__c;
        convert();
    }


    public CSIO_GeneralPartyInfo(Account account, Quote_Risk_Relationship__c quoteRiskRelationship)
    {
        this.accountParty = account;
        this.quoteRiskRelationship = quoteRiskRelationship;
        convert();
    }


    public CSIO_GeneralPartyInfo(Contact contact, Quote_Risk_Relationship__c quoteRiskRelationship)
    {
        this.contactParty = contact;
        this.quoteRiskRelationship = quoteRiskRelationship;
        convert();
    }

    public CSIO_GeneralPartyInfo(Account acct, List<Quote_Risk_Relationship__c> quoteRiskRelationshipList, List<Account> accountPartyList, List<Contact> contactPartyList)
    {
        this.accountParty = acct;
        this.quoteRiskRelationshipList = new List<Quote_Risk_Relationship__c> ();
        for(Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationshipList){
            if(quoteRiskRelationship.Party_Type__c ==  RelationshipControllerExtension.ADDITIONAL_INSURED)
            {
                this.quoteRiskRelationshipList.add(quoteRiskRelationship);
            }
        }
        this.accountPartyList = accountPartyList;
        this.contactPartyList = contactPartyList;

        convert();
    }

    private void convert()
    {
        // name information
        if (accountParty != null)
        {
            if (quoteRiskRelationship != null)
            {
                this.nameInfo = new NameInfo(this.accountParty, 'csio:OTH', quoteRiskRelationship.Party_Type__c);
            }
            else
            {
                this.nameInfo = new NameInfo(this.accountParty);
            }
        }
        else
        {
            if (quoteRiskRelationship != null)
            {
                this.nameInfo = new NameInfo(this.contactParty, 'csio:OTH', quoteRiskRelationship.Party_Type__c);
            }
            else
            {
                this.nameInfo = new NameInfo(this.contactParty);
            }
        }
        // now the address

        // if the parties have an address as part of their attributes then use that
        if (this.risk.quote__r.ePolicy__c != null && this.risk.quote__r.Status__c == 'Finalized') {
            this.mailingAddr = new CSIO_Addr(this.risk, true);
        } else if (accountParty != null && this.risk != null && this.risk.Mailing_Address_Postal_Code__c != null) {
            this.mailingAddr = new CSIO_Addr(this.risk, true);
        }

        if (accountParty != null)
        {
            if (accountParty.BillingStreet != null)
            {
                this.addr = new CSIO_Addr(this.accountParty);
            }
        }
        else if (contactParty != null)
        {
            if (contactParty.MailingStreet != null)
            {
                this.addr = new CSIO_Addr(this.contactParty);
            }
        }
        // else if the risk isn't null then use that address
        else if (risk != null)
        {
            this.addr = new CSIO_Addr(this.risk);
        }

        if(quoteRiskRelationshipList != null){
            this.NameInfo = new NameInfo(this.accountParty, quoteRiskRelationshipList, accountPartyList, contactPartyList);
        }

    }

    public override String toString()
    {
        String xml = '';

        xml += '<GeneralPartyInfo>';
        xml += this.nameInfo;
        if (this.addr != null)
        {
            if(this.isMailingAddressSame != null){
                if(this.isMailingAddressSame){
                    xml += this.addr;
                }
                else{
                    xml += this.addr;
                    xml += new CSIO_Addr(this.risk);
                }
            }
            else
                xml += this.addr;
        }
        if (this.mailingAddr != null)
        {
            xml += this.mailingAddr;
        }
        xml += '</GeneralPartyInfo>';

        return xml;
    }

    public class NameInfo
    {
        private Account acct;
        private Contact contact;
        public String commercialName;
        public String familyNames;
        public String surname;
        public String supplementaryNameCode;
        public String supplementaryName;
        public List<Quote_Risk_Relationship__c> quoteRiskRelationshipList;
        public List<Account> accountPartyList;
        public List<Contact> contactPartyList;
        public List<SupplementaryNameInfo> supplementaryNameInfoList;

        public NameInfo(Account acct)
        {
            this.acct = acct;
            convert();
        }

        public NameInfo(Contact contact)
        {
            this.contact = contact;
            convert();
        }

        public NameInfo(Account acct, String supplementaryNameCode, String supplementaryName)
        {
            this.acct = acct;
            this.supplementaryNameCode = supplementaryNameCode;
            this.supplementaryName = supplementaryName;
            convert();
        }

        public NameInfo(Contact contact, String supplementaryNameCode, String supplementaryName)
        {
            this.contact = contact;
            this.supplementaryNameCode = supplementaryNameCode;
            this.supplementaryName = supplementaryName;
            convert();
        }

        public NameInfo(Account acct, List<Quote_Risk_Relationship__c> quoteRiskRelationshipList, List<Account> accountPartyList, List<Contact> contactPartyList)
        {
            this.acct = acct;
            this.quoteRiskRelationshipList = quoteRiskRelationshipList;
            this.accountPartyList = accountPartyList;
            this.contactPartyList = contactPartyList;
            convert();
        }

        private void convert()
        {
            this.supplementaryNameInfoList = new List<SupplementaryNameInfo>();
            if(this.quoteRiskRelationshipList != null){
                this.commercialName = acct.Name;
                for (Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationshipList){
                    SupplementaryNameInfo sninfo;
                    for( Account acc : accountPartyList){
                        if(sninfo == null && quoteRiskRelationship.Account__c == acc.Id){
                            sninfo = new SupplementaryNameInfo (quoteRiskRelationship ,acc);
                            break;
                        }

                    }
                    for( Contact contact : contactPartyList){
                        if(sninfo == null && quoteRiskRelationship.Contact__c == contact.Id){
                            sninfo = new SupplementaryNameInfo (quoteRiskRelationship ,contact);
                            break;
                        }

                    }
                    if(sninfo != null){
                        this.supplementaryNameInfoList.add(sninfo);
                    }
                }

            }
            else {
                if (acct !=null)
                {
                    this.commercialName = acct.Name;
                }
                else
                {
                    System.assert(contact.LastName != null, 'Contact attributes should not be null: contact ID ' + contact.Id);
                    this.surname = contact.LastName;
                    this.familyNames = contact.FirstName;
                }
            }
            if (supplementaryNameCode != null){
                SupplementaryNameInfo sninfo = new SupplementaryNameInfo(this.supplementaryName,this.supplementaryNameCode);
                this.supplementaryNameInfoList.add(sninfo);

            }
            System.debug(this.supplementaryNameInfoList);
            System.debug(this.supplementaryNameInfoList.size());
        }

        public override String toString()
        {
            String xml = '';

            xml += '<NameInfo>';
            if (acct != null)
            {
                xml += ' <CommlName>';
                xml += '  <CommercialName>' + XMLHelper.toCData(this.commercialName) + '</CommercialName>';
                xml += ' </CommlName>';
            }
            else
            {
                xml += ' <CommlName>';
                xml += '  <CommercialName>' + XMLHelper.toCData(this.familyNames) +' '+XMLHelper.toCData(this.surname)+ '</CommercialName>';
                xml += ' </CommlName>';

            }

            for(SupplementaryNameInfo sninfo : this.supplementaryNameInfoList){
                xml += sninfo;
            }
            xml += '</NameInfo>';

            return xml;
        }


    }

    public class SupplementaryNameInfo{

            private String supplementaryNameCode;
            private String supplementaryName;
            private Account accountParty;
            private Contact contactParty;
            private Quote_Risk_Relationship__c quoteRiskRelationship;


            public SupplementaryNameInfo(Quote_Risk_Relationship__c quoteRiskRelationship, Contact contact){
                this.contactParty = contact;
                this.quoteRiskRelationship = quoteRiskRelationship;
                convert();
            }

            public SupplementaryNameInfo(Quote_Risk_Relationship__c quoteRiskRelationship, Account account){
                this.accountParty = account;
                this.quoteRiskRelationship = quoteRiskRelationship;
                convert();
            }

            public SupplementaryNameInfo(String supplementaryName, String supplementaryNameCd){
                this.supplementaryName = supplementaryName;
                this.supplementaryNameCode = supplementaryNameCd;
                convert();
            }

            public void convert(){

                Map<String, String> supplementaryNameCd = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Relationship_Type__c');
                if(this.accountParty !=null){
                    this.supplementaryName = this.accountParty.Name;
                }
                else if(this.contactParty != null){
                    this.supplementaryName = this.contactParty.FirstName + ' ' + this.contactParty.LastName;
                }
                if(quoteRiskRelationship != null){
                    this.supplementaryNameCode = supplementaryNameCd.get(this.quoteRiskRelationship.Relationship_Type__c);
                }

            }

            public override String toString(){
                String xml = '';

                xml += '  <SupplementaryNameInfo>';
                xml += '   <SupplementaryNameCd>' +  this.supplementaryNameCode  + '</SupplementaryNameCd>';
                if (supplementaryName != null)
                {
                    xml += '    <SupplementaryName>' + XMLHelper.toCData(this.supplementaryName) + '</SupplementaryName>';
                }
                xml += '  </SupplementaryNameInfo>';
                return xml;
            }
        }
}