public with sharing class SMEQ_BrokerMessageDao
{
    private static String CASE_RECORDTYPE = 'CI_Open_SPRNT';
    private static String STATUS_FOR_NEW_CASE = 'New';
    private static String TRANSACTION_TYPE = 'New Business';
    private static String CASE_ORIGIN = 'Broker Tool';
    private static String CASE_PRODUCT = 'CI_SME_EPOLICY';
    private static String SME_QUEUE_NAME = 'SME Quotes';
    private static String GROUP_TYPE_QUEUE = 'queue';
    private static String SUCCESS_MESSAGE = 'SUCCESS';

    /**
     * Save message sent by broker when UnderWriter is not available as a case comment on broker's case.
     * @param SMEQ_BrokerMessageRequestModel
     * @return SMEQ_BrokerMessageResponseModel
     */
    @TestVisible
    public SMEQ_BrokerMessageResponseModel saveBrokerMessageToCase(SMEQ_BrokerMessageRequestModel brokerMsgReq)
    {
        String comment = '';
        Case casetoUpdate;
        SMEQ_BrokerMessageResponseModel brokerMesResponse = new SMEQ_BrokerMessageResponseModel();

        brokerMesResponse.successMessage = 'ERROR';

        //brokerMsgReq.bundle='NB';
        if (brokerMsgReq.bundle == 'NB') {
            CASE_RECORDTYPE = 'CI_Open_SPRNT';
            STATUS_FOR_NEW_CASE = 'New';
            TRANSACTION_TYPE = 'New Business';
            CASE_ORIGIN = 'Broker Tool';
            CASE_PRODUCT = 'CI_SME_EPOLICY';
            SME_QUEUE_NAME = 'SME Quotes';
            GROUP_TYPE_QUEUE = 'queue';
            SUCCESS_MESSAGE = 'SUCCESS';
        }
        else if (brokerMsgReq.bundle == 'RE') {
            CASE_RECORDTYPE = 'CI_Open_SPRNT';
            STATUS_FOR_NEW_CASE = 'New';
            TRANSACTION_TYPE = 'Renewal';
            CASE_ORIGIN = 'Broker Tool';
            CASE_PRODUCT = 'CI_SME_EPOLICY';
            SME_QUEUE_NAME = 'SME Renewals';
            GROUP_TYPE_QUEUE = 'queue';
            SUCCESS_MESSAGE = 'SUCCESS';            
        }
        else if (brokerMsgReq.bundle == 'AM') {
            CASE_RECORDTYPE = 'CI_Open_SPRNT';
            STATUS_FOR_NEW_CASE = 'New';
            TRANSACTION_TYPE = 'Complex Amendment';
            CASE_ORIGIN = 'Broker Tool';
            CASE_PRODUCT = 'CI_SME_EPOLICY';
            GROUP_TYPE_QUEUE = 'queue';
            SUCCESS_MESSAGE = 'SUCCESS';
        }
        else {
            return brokerMesResponse;
    	}
       
        if (brokerMsgReq.comment != null)
        {
            comment = buildCaseComment(brokerMsgReq.email, brokerMsgReq.phone, brokerMsgReq.extension, brokerMsgReq.comment);
            
            //if livechat is initiated by the broker but UnderWriter is unavailable and the Case has been previously saved
            if (brokerMsgReq.caseNumber != null)
            {
                comment = buildCaseComment(brokerMsgReq.email, brokerMsgReq.phone, brokerMsgReq.extension, brokerMsgReq.comment);
                
                caseToUpdate = [
                    SELECT Id, bkrCase_Region__c
                    FROM Case
                    WHERE caseNumber = :brokerMsgReq.caseNumber
                    LIMIT 1
                ];
            }
            else //if livechat is initiated by the broker but UnderWriter is unavailable and the Case has not been saved
            {
                Case c = new Case();
               	Id caseRecordTypeId = Utils.GetRecordTypeIdsByDeveloperName(Case.SObjectType,true).get(CASE_RECORDTYPE);
                c.RecordTypeId = caseRecordTypeId;
                c.bkrCase_Subm_Type__c = TRANSACTION_TYPE;
                c.origin = CASE_ORIGIN;
                c.Status = STATUS_FOR_NEW_CASE;
                c.bkrCase_Region__c = UserService.getRegion(UserInfo.getUserId())[0];
                
                List<Product2> productList = [
                    SELECT id
                    FROM Product2
                    WHERE ProductCode = :CASE_PRODUCT
                ];

                if (productList != null && productList.size() > 0)
                {
                    c.Product__c = productList.get(0).id;
                    c.productid = productList.get(0).id;
                }
                if (brokerMsgReq.sicCode != null)
                {
                    List<SIC_Code__c> scode = [
                        SELECT id
                        FROM SIC_Code__c
                        WHERE SIC_Code__c = :brokerMsgReq.sicCode
                    ];
                    c.bkrCase_SIC_Code__c = scode.get(0).id;
                }
                //insert c;
                caseToUpdate = c;
            }

           	if (brokerMsgReq.bundle == 'AM') {
                if (caseToUpdate.bkrCase_Region__c == 'Pacific') {
                    SME_QUEUE_NAME = 'Pacific Retention';
                }
                else if (caseToUpdate.bkrCase_Region__c == 'Quebec') {
                    SME_QUEUE_NAME = 'Quebec Retention';
                }
                else if (caseToUpdate.bkrCase_Region__c == 'Ontario' || caseToUpdate.bkrCase_Region__c == 'Atlantic' || caseToUpdate.bkrCase_Region__c == 'Prairies') {
                    SME_QUEUE_NAME = 'Central Retention';
                }
            }

            Group smeQueue = [
                SELECT id
                FROM group
                WHERE type = :GROUP_TYPE_QUEUE
                AND name = :SME_QUEUE_NAME
                LIMIT 1
            ];
            
            caseToUpdate.OwnerID = smeQueue.id;
            upsert caseToUpdate;
            CaseComment cc = new CaseComment(CommentBody = comment, ParentId = caseToUpdate.Id);
            insert cc;
            brokerMesResponse.successMessage = SUCCESS_MESSAGE; 
        }

        return brokerMesResponse;
    }

    public String buildCaseComment(String sentEmail, String sentPhone, String sentExtension, String sentComment)
    {
        String comment = '';
        
        if(sentEmail!=null)
        {
            comment = comment + 'Email: ' + sentEmail + '\n';
        }
        
        if(sentPhone!=null)
        {
            comment = comment + 'Phone: ' +  sentPhone + '\n';
        }
        
        if(sentExtension!=null)
        {
            comment = comment + 'Ext: ' + sentExtension + '\n';
        }
        
        if(sentComment!=null)
        {
            comment = comment + 'Message: ' + sentComment;            
        }
        
        return comment; 
    }
}