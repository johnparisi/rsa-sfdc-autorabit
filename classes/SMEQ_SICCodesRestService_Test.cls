@isTest
private class SMEQ_SICCodesRestService_Test
{
	/**
    * This method provides test assertions to get resource bundles for all variants and Bundle 
    */
    static testMethod void testSICCodes()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/sic/getSICCodes';
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_SICCodesRestService SICCodeRestTest = new SMEQ_SICCodesRestService();
            SMEQ_SICCodesRestService.ResponseObjectWrapper responseWrapper = SMEQ_SICCodesRestService.getSICCodes();
            System.debug('Response Objcet:' + responseWrapper);
            System.assert(responseWrapper != null);
            System.assert(responseWrapper.getResponseCode() != null);
            System.assertEquals('OK', responseWrapper.getResponseCode());
        }
    }
    static testMethod void testSICCodesException()
    {
             RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = 'https://rsa--dev3.cs43.my.salesforce.com/services/apexrest/sic/getSICCodes';
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            SMEQ_SICCodesRestService SICCodeRestTest = new SMEQ_SICCodesRestService();
            SMEQ_SICCodesRestService.ResponseObjectWrapper responseWrapper = SMEQ_SICCodesRestService.getSICCodes();
    }    
}