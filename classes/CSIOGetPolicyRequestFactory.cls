/**
 * CSIOGetPolicyRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to generate the xml Request for the GETPOLICY service 
 */
public class CSIOGetPolicyRequestFactory extends CSIORequestDocumentFactory implements CSIORequestFactory {
    public List <Session_Information__c> sessionId;
    
    public CSIOGetPolicyRequestFactory(String ComNumber, String businessSource, RequestInfo ri, String offeringProject, String federationId){
        super(federationId, null, businessSource, ri);
        this.comNumber = ComNumber;
        rsaOperation = ri.getServiceType();
        this.soqlDataSet = null; 
        this.ri = ri;
        this.federationId = federationId;
        rsaContextId = esbContext.get(offeringProject);
    }
    
    public CSIOGetPolicyRequestFactory(SoqlDataSet sds, RequestInfo ri){
        super(sds.requestingUser.federationIdentifier, sds.smeCase.New_Business_Source__c, sds.quote.Business_Source_ID__c, ri);
        this.comNumber = sds.smeCase.Com__c;
        rsaOperation = ri.getServiceType();
        this.soqlDataSet = sds; 
        this.ri = ri;
        rsaContextId = esbContext.get(this.soqlDataSet.smeCase.Offering_Project__c);
    }
    
    public String buildXMLRequest(){
        getHeaderXml();
        xml += CSIO128Header;
        xml += '<RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
        xml += ' <PolicySyncRq>';
        xml += '   <RqUID>' + XMLHelper.encodeUID(uniqueId) + '</RqUID>';
        xml += '   <TransactionRequestDt>' + todayDateLong + '</TransactionRequestDt>';

        if(this.ri.svcType == RequestInfo.ServiceType.LOAD_POLICY) {
            Date dateTemp = this.soqlDataSet.quote.Transaction_Effective_Date__c;
            xml += '   <AsOfDt>' + DateTime.newInstance(dateTemp.year(), dateTemp.month(), dateTemp.day()).format(dateFormatShort) + '</AsOfDt>';   
        }else{
            xml += '   <AsOfDt>' + todayDateShort + '</AsOfDt>';   
        }
        
        xml += '   <PolicyNumber>' + this.comNumber + '</PolicyNumber>';
        
        if(this.ri.svcType == RequestInfo.ServiceType.LOAD_POLICY || this.ri.svcType == RequestInfo.ServiceType.LOAD_POLICY_X){
            xml += ' <rsa:TransactionType>' + transactionType.get(this.soqlDataSet.smeCase.bkrCase_Subm_Type__c) +'</rsa:TransactionType>';
        }
        xml += ' </PolicySyncRq>';
        xml += ' <rsa:ServiceOperationType>' + rsaOperation + '</rsa:ServiceOperationType>';
        xml += ' <rsa:Context>' + rsaContextId + '</rsa:Context>';
        if(this.ri.svcType == RequestInfo.ServiceType.LOAD_POLICY || this.ri.svcType == RequestInfo.ServiceType.LOAD_POLICY_X){
            xml += ' <rsa:SessionId>' + this.soqlDataSet.sessionId + '</rsa:SessionId>';
            
        }
        
        xml += CSIO128Footer;
        
        return xml;
    }
}