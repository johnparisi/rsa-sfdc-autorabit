/**
* CoverageDomain
* Author: James Lee
* Date: November 10 2016
* 
* The object domain is where the real work happens, this is where we will apply the actual business logic. 
* We have the main Object Domain which should handle all common processing. 
* Then there are inner Business Unit specific domain that will handle the logic for just that business unit(if these grow to large we can evaluate moving them to their own class).
* Any data passed into these domains is assumed to be for that domain(Handler does that for us). So there is no need to check record types, 
* just implement logic in the appopriate method. 
*/
public with sharing class CoverageDomain
{
    private CoverageService service = new CoverageService();
    
    public void beforeInsert(List<Coverages__c> records)
    {
    }
    
    public void beforeUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
    {
    }
    
    public void afterInsert(List<Coverages__c> records)
    {
    }
    
    public void afterUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
    {
    }
    
    //Coverage with Record Type of Open Coverage or Close Coverage
    public class SMEQDomain
    {
        private CoverageService service = new CoverageService();
        public void beforeInsert(List<Coverages__c> records)
        {
            service.synchroniseValuesFromStandardCoverages(records);
        }
        
        public void beforeUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
        {
        }
        
        public void afterInsert(List<Coverages__c> records)
        {
        }
        
        public void afterUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
        {
            service.updateContentsCoverage(records, oldRecords);
        }
    }
    
    public class SMEPDomain
    {
        private CoverageService service = new CoverageService();
        public void beforeInsert(List<Coverages__c> records)
        {
        }
        
        public void beforeUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
        {
        }
        
        public void afterInsert(List<Coverages__c> records)
        {
        }
        
        public void afterUpdate(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
        {
            service.syncSicAnswers(records, oldRecords);
        }
    }
}