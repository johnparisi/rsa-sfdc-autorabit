/*
Extension class for the Relationship Management Console, for managing Quote Risk Relationships associated to Quote and Risks.
The console appears on both the Quote and Risk pages, and each console has its own Visual Force page that use this extension class.

Author: Lauren Abramsky
Date: June 12, 2017
*/
public class RelationshipControllerExtension
{
    public Id currentRecordID {get; set;}  
    public Risk__c currentRisk {get; set;}
    public Quote__c currentQuote {get; set;}
    public String searchText {get; set;}
    public List<BindParty> partiesList {get; set;} 
    public List<Id> qrrList {get; set;} 
    public BindParty selectedBindParty {get; set;}  
    public BindParty deletedBindParty {get; set;}      
    public List<SelectOption> partyTypesList {get;set;} 
    public List<SelectOption> relationshipTypesList {get;set;}         
    public String otherPartyAccountURL {get;set;}
    public String otherPartyContactURL {get;set;}
    
    private Boolean isQuote {get; set;}
    private List<SelectOption> lossPayeeRelationshipTypeSelectOptionList = null;
    private List<SelectOption> AdditionalInsuredTypeSelectOptionList = null;
    private Id liabilityRecordTypeId {get;set;}
    public static final String LOSS_PAYEE = 'Loss Payee';
    public static final String LIABILITY_ADDITIONAL_INSURED = 'Liability Additional Insured';
    public static final String PREMIER_FINANCIER = 'Premier Financier';
    public static final String ADDITIONAL_INSURED = 'Additional Insured';
    public static final String OTHER_CLIENT_BILL = 'Other Client Bill';
    private static final String OTHER_PARTY_ACCOUNT = 'Other_Party_Account';
	private static final String OTHER_PARTY_CONTACT = 'Other_Party_Contact';
    private static final String QUOTE = 'Quote__c';	
    private static final String ACCOUNT_RECORD_TYPE_URL = '/001/e?retURL=%2F001%2Fo&RecordType=';
    private static final String CONTACT_RECORD_TYPE_URL = '/003/e?retURL=%2F003%2Fo&RecordType=';
    private static final String ACCOUNT_ID_URL = '&ent=Account';
    private static final String CONTACT_ID_URL = '&ent=Contact';
    private static final List<String> LOSS_PAYEE_RELATIONSHIPS = new List<String> {'First Mortgagee' , 'First Private Mortgagee' ,'Lessor','Lienholder' ,'Private Loan' ,'Second Mortgagee' ,'Second Private Mortgagee' ,'Third Mortgagee' ,'Third Private Mortgagee'};
    private static final List<String> ADDITIONAL_INSURED_RELATIONSHIPS = new List<String> {'A division of', 'A joint venture of', 'And', 'At', 'Division of', 'Doing business as', 'General Partnership for', 'In Trust for', 'Of', 'Operated By', 'Operating As', 'Or', 'Owned and Operated as', 'Owners and Operated by', 'Partnership', 'Subsidiary Of', 'Trading As' };
    
	/**
    * This method is the class constructor.
    * @param accepts the Apex Standard Controller, implicitly either a Quote__c or Risk__c controller
    **/
    public RelationshipControllerExtension(ApexPages.StandardController controller)
    {      
        this.currentRecordID = controller.getRecord().Id;
        String objectName = this.currentRecordID.getSObjectType().getDescribe().getName();
        // Check if on a quote or risk page
        if (objectName == QUOTE) {
            this.currentQuote = [
            SELECT id, name, case__c
            FROM quote__c
            WHERE id = :this.currentRecordId
            ];
            this.isQuote = true;
        } else {
            this.currentRisk = [
                SELECT id, name, quote__c, quote__r.case__c, recordtypeid
                FROM risk__c
                WHERE id = :this.currentRecordId
            ];
            this.isQuote = false;
        }
        Schema.DescribeFieldResult partyTypeResult = Quote_Risk_Relationship__c.Party_Type__c.getDescribe();
        List<Schema.PicklistEntry> partyPickListEntries = partyTypeResult.getPicklistValues();       
        Map<String, Id> riskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);		
        this.liabilityRecordTypeId = riskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY);       
        this.partyTypesList = new List<SelectOption>();
        // Create a map to hold the selectedoptions indexed by value
        Map<String, SelectOption> selectOptionMap = new Map<String, SelectOption>();
        // Iterate over the relationships listed in the QRR object.
        for (Schema.PicklistEntry party : partyPickListEntries)
        {
            //create object and put it into the map
            SelectOption so = new SelectOption(party.getValue(), party.getLabel()); //label is en or french version
            selectOptionMap.put(party.getValue(), so);
        }
        // create the relationship type lists
        createLossPayeeAdditionalInsuredRelationshipSelectOptionList();
        if (currentRisk != null) {
            if (currentRisk.recordtypeid == liabilityRecordTypeId) {
                this.partyTypesList.add(selectOptionMap.get(LIABILITY_ADDITIONAL_INSURED));
            }
            else {
                this.partyTypesList.add(selectOptionMap.get(LOSS_PAYEE));
            }
        }
        else {
                this.partyTypesList.add(selectOptionMap.get(ADDITIONAL_INSURED));
                this.partyTypesList.add(selectOptionMap.get(PREMIER_FINANCIER));
        }
        Map<String, Id> accountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);		
        Id otherPartyAccountRecordTypeId = accountTypes.get(OTHER_PARTY_ACCOUNT); 
        Map<String, Id> contactTypes = Utils.GetRecordTypeIdsByDeveloperName(Contact.SObjectType, true);		
        Id otherPartyContactRecordTypeId = contactTypes.get(OTHER_PARTY_CONTACT); 
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        this.otherPartyAccountURL = baseURL + ACCOUNT_RECORD_TYPE_URL + otherPartyAccountRecordTypeId + ACCOUNT_ID_URL;
       	this.otherPartyContactURL = baseURL + CONTACT_RECORD_TYPE_URL + otherPartyContactRecordTypeId + CONTACT_ID_URL;
        buildPartiesList();
        this.selectedBindParty = new BindParty(); 
        this.deletedBindParty = new BindParty(); 
        
    }
    
    /**
    * This method is called from the controller, creating the specific dropdown list for Loss Payee and 
    * Liability Additional Insured relationship types.
    **/
    private void createLossPayeeAdditionalInsuredRelationshipSelectOptionList() {
       	List<String> lossPayeeRelationshipTypes = LOSS_PAYEE_RELATIONSHIPS;
        List<String> AdditionalInsuredRelationshipTypes = ADDITIONAL_INSURED_RELATIONSHIPS;
        this.lossPayeeRelationshipTypeSelectOptionList = new List<SelectOption>();
        this.AdditionalInsuredTypeSelectOptionList = new List<SelectOption>();
        Schema.DescribeFieldResult relationshipTypeResult = Quote_Risk_Relationship__c.Relationship_Type__c.getDescribe();
        List<Schema.PicklistEntry> relationshipPickListEntries = relationshipTypeResult.getPicklistValues();
        for (Schema.PicklistEntry rel : relationshipPickListEntries) {
            for (String lossPayeeRelationshipValue : lossPayeeRelationshipTypes) {
                if (lossPayeeRelationshipValue == rel.value) {
                   	SelectOption so = new SelectOption(rel.getValue(), rel.getLabel()); 
                	this.lossPayeeRelationshipTypeSelectOptionList.add(so); 
                 }
            }
            for (String AdditionalInsuredRelationshipValue : AdditionalInsuredRelationshipTypes) {
                if (AdditionalInsuredRelationshipValue == rel.value) {
                    SelectOption so = new SelectOption(rel.getValue(), rel.getLabel()); 
                    this.AdditionalInsuredTypeSelectOptionList.add(so); 
                }
            }
        }
    }        
    
    /**
     * This method is called from the edit button on a list item in the console, rerendering that list item's Party Type
     * and Relationship Type fields as dropdowns. Upon clicking the save button, any updated Party and Relationship Type 
     * values will be saved. 
     **/
    public void editToggle() 
    {
        for (BindParty currentBindParty : this.partiesList) {
            if (currentBindParty.id == this.selectedBindParty.id) 
            {
                currentBindParty.isEditable = !currentBindParty.isEditable;
            }
        }   
    }
    
    /**
     * This method is called from the delete button on a list item in the console, rerendering that list item as unselected. 
     * Upon clicking the save button, unselected list items / Quote Risk Relationships will be deleted. 
     **/
    public void deleteToggle() 
    {
        for (BindParty currentBindParty : this.partiesList) {
            if (currentBindParty.id == this.deletedBindParty.id) 
            {
                currentBindParty.selected = false;
                currentBindParty.toBeDeleted = true;
            }
        }   
    }
    
    /**
      * This method is called from the constructor, getting the current Quote Risk Relationships associated
      * to the Quote or Risk page and converting them into a list of BindParty objects that are displayed as 
      * selected, editable (isEditable attribute is initially false), and deletable records in the console table.
      **/
    public void buildPartiesList() 
    {
        this.partiesList = new List<BindParty>();
        List<Quote_Risk_Relationship__c> quoteRiskRelationships = getQuoteRiskRelationships();
        this.qrrList = new List<Id>();
        for (Quote_Risk_Relationship__c quoteRiskRelationship : quoteRiskRelationships) {
            this.qrrList.add(quoteRiskRelationship.account__c);
            this.qrrList.add(quoteRiskRelationship.contact__c);
            BindParty bindParty = new BindParty();
            bindParty.Id = quoteRiskRelationship.Id;
            bindParty.name = quoteRiskRelationship.Name + ' ';
            bindParty.account = quoteRiskRelationship.account__c;
            bindParty.contact = quoteRiskRelationship.contact__c;            
            bindParty.partyNumber = quoteRiskRelationship.party_number__c;
            bindParty.partyType = quoteRiskRelationship.party_type__c;
            bindParty.relationshipNumber = quoteRiskRelationship.Quote_Relationship_Number__c;
            bindParty.relationshipType = quoteRiskRelationship.Relationship_Type__c;
            bindParty.relatedRisk = quoteRiskRelationship.Related_Risk__c;
            if (bindParty.RelatedRisk != null) {
            	if (quoteRiskRelationship.Related_Risk__r.Recordtypeid == this.liabilityRecordTypeId) {
                	bindParty.relatedRiskType = LIABILITY_ADDITIONAL_INSURED;
            	}
            	else {
                    bindParty.relatedRiskType = LOSS_PAYEE;
                }
        	}
            bindParty.quote = quoteRiskRelationship.Quote__r.id;
            bindParty.accountName = quoteRiskRelationship.account__r.name;
            bindParty.contactName = quoteRiskRelationship.contact__r.name;
            bindParty.accountStreet = quoteRiskRelationship.account__r.BillingStreet;
            bindParty.accountProvince = quoteRiskRelationship.account__r.BillingState;
            bindParty.contactStreet = quoteRiskRelationship.contact__r.MailingStreet;
            bindParty.contactProvince = quoteRiskRelationship.contact__r.MailingState;            
            bindParty.isEditable = false;
            bindParty.toBeDeleted = false;
            bindParty.selected = true;
            //limits the relationship type dropdown depening on if it's a quote, location risk, or liability risk
            if (bindParty.relatedRisk != null) {
             	if (bindParty.relatedRiskType == LOSS_PAYEE) {
                    bindParty.relationshipTypeOptions = lossPayeeRelationshipTypeSelectOptionList;
                } 
            } 
            else {
                if (bindParty.partyType == ADDITIONAL_INSURED) {
                    bindParty.relationshipTypeOptions = AdditionalInsuredTypeSelectOptionList;
                } 
                else {
                    bindParty.relationshipTypeOptions = new List<SelectOption>();
                } 
            }     
            partiesList.add(bindParty);
         }
    }
    
    /**
      * This method is used in the saveQuoteRiskRelationships() and buildPartiesList() methods, checking whether the current page is
      * a Quote or Risk, then returning the list of Quote Risk Relationships currently associated to that page. 
      * @return List<Quote_Risk_Relationship__c> returning the list of Quote Risk Relationships currently associated to that Quote or Risk
      **/
    private List<Quote_Risk_Relationship__c> getQuoteRiskRelationships()
    {
        List<Quote_Risk_Relationship__c> quoteRiskRelationships = null;
        if (isQuote) {
                quoteRiskRelationships = [
                SELECT id, name, account__c, account__r.name, contact__c, contact__r.name, party_number__c, toLabel(party_type__c), toLabel(Relationship_Type__c), Quote_Relationship_Number__c,
                Related_Risk__c, Related_Risk__r.Recordtypeid, Quote__c, Quote__r.id
                , Contact__r.MailingStreet, Contact__r.MailingState, Account__r.BillingStreet, Account__r.BillingState                 
                FROM Quote_Risk_Relationship__c
                WHERE quote__c = :this.currentQuote.id AND Related_Risk__c = null
                AND Deleted__c = false                
            ]; 
        } else {
                quoteRiskRelationships = [
                SELECT id, name, account__c, account__r.name, contact__c, contact__r.name, party_number__c, toLabel(party_type__c), toLabel(Relationship_Type__c), Quote_Relationship_Number__c,
                Related_Risk__c, Related_Risk__r.Recordtypeid, Quote__c, Quote__r.id
                , Contact__r.MailingStreet, Contact__r.MailingState, Account__r.BillingStreet, Account__r.BillingState 
                FROM Quote_Risk_Relationship__c
                WHERE related_risk__c = :this.currentRisk.id
                AND Deleted__c = false
            ]; 
        }
        return quoteRiskRelationships;
    }
    
    /**
      * This method is called from the search button, using a fuzzy SOSL to search all of the user's accessible 
      * contacts and accounts. The search results (15 max) are added to the partiesList, appearing as new relationships 
      * in the console that are in "edit mode" (isEditable attribute is true), unselected, and undeletable. 
      **/
    public void searchForParties()
    {
        //clear any previous search results
        saveQuoteRiskRelationships(); 
        //search on search text
        if (this.searchText.length()>1) {
            List<List<SObject>> sObjectList = 
                [
                    FIND 
                        :this.searchText 
                    IN 
                    ALL FIELDS RETURNING 
                        Account(Name, Id, BillingStreet, BillingState WHERE id NOT IN :qrrList),
                        Contact(FirstName, LastName, Id, MailingStreet, MailingState WHERE id NOT IN :qrrList)
                    LIMIT 15
                ];
            //cast into lists of account and contact objects
            List<Account> accountList = (List<Account>) sObjectList[0]; 
            List<Contact> contactList = (List<Contact>) sObjectList[1];
            //add accounts into bind party list
            for (Account account : accountList) 
            {
                BindParty bindParty = new BindParty();
                bindParty.account = account.Id;
                bindParty.accountName = account.Name;
                bindParty.isEditable = true;
                bindParty.selected = false;
                if (this.currentRisk != null) {
                    if (this.currentRisk.recordtypeid == this.liabilityRecordTypeId) {
                        bindParty.relatedRiskType = LIABILITY_ADDITIONAL_INSURED;
                    } else {
                        bindParty.relationshipTypeOptions = lossPayeeRelationshipTypeSelectOptionList;
                        bindParty.relatedRiskType = LOSS_PAYEE;
                    } 
                } else {
                    // Default in search is additionalinsured so set this.. onchange handles properly otherwise.
                    //bindParty.relationshipTypeOptions = new List<SelectOption>();                    
                    bindParty.relationshipTypeOptions = AdditionalInsuredTypeSelectOptionList;
                }
                bindParty.accountStreet = account.BillingStreet;
                bindParty.accountProvince = account.BillingState;    
                partiesList.add(bindParty);
            }
            //add contacts into bind party list
            for (Contact contact : contactList) 
            {
                BindParty bindParty = new BindParty();
                bindParty.contact = contact.Id;
                bindParty.contactName = contact.FirstName + ' ' + contact.LastName;
                bindParty.isEditable = true;
                bindParty.selected = false;
                if (this.currentRisk != null) {
                    if (this.currentRisk.recordtypeid == this.liabilityRecordTypeId) {
                        bindParty.relatedRiskType = LIABILITY_ADDITIONAL_INSURED;
                    } else {
                        bindParty.relationshipTypeOptions = lossPayeeRelationshipTypeSelectOptionList;
                        bindParty.relatedRiskType = LOSS_PAYEE;
                    } 
                } else {
                    // Default in search is additionalinsured so set this.. onchange handles properly otherwise.
                    //bindParty.relationshipTypeOptions = new List<SelectOption>();                    
                    bindParty.relationshipTypeOptions = AdditionalInsuredTypeSelectOptionList;
                }
                bindParty.contactStreet = contact.MailingStreet;
                bindParty.contactProvince = contact.MailingState;
                partiesList.add(bindParty);
            }
        }
    }
    
    /**
      * This method is called from the save button, deletes the current Quote Risk Relationships on that Quote or Risk
      * and creates new Quote Risk Relationship records for each item in the console that is selected, using the updated
      * information (if a list item has been edited). After the save button is clicked, console is cleared to only include
      * selected items, as the new QRR's are added to the partiesList.
      **/
    public void saveQuoteRiskRelationships() 
    {
        SavePoint savePoint = Database.setSavepoint();
        try 
        {
            List<Quote_Risk_Relationship__c> oldQRRs = getQuoteRiskRelationships();
            delete oldQRRs;
            
            // Get the label to value map for party and relationship types.
            Map<String, String> labelToValue = getlabelToValue();
            
            //create new QRR's
            List<Quote_Risk_Relationship__c> newQRRs = new List<Quote_Risk_Relationship__c>();
            for (BindParty bindParty : this.partiesList)
            {
                if (bindParty.selected || (bindparty.toBeDeleted == null ? false : bindparty.toBeDeleted))
                {
                    Quote_Risk_Relationship__c quoteRiskRelationship = new Quote_Risk_Relationship__c();
                    if (bindParty.account != null) {
                        quoteRiskRelationship.account__c = bindParty.account;
                    } else {
                        quoteRiskRelationship.contact__c = bindParty.contact;
                    }
                    quoteRiskRelationship.party_number__c = bindParty.partyNumber == null ? 0 : bindParty.partyNumber;
                    quoteRiskRelationship.party_type__c = labelToValue.get(bindParty.partyType) == null ? bindParty.partyType : labelToValue.get(bindParty.partyType);
                    quoteRiskRelationship.Relationship_Type__c = labelToValue.get(bindParty.relationshipType) == null ? bindParty.relationshipType : labelToValue.get(bindParty.relationshipType);
                    
                    if (!isQuote)
                    {
                        quoteRiskRelationship.Related_Risk__c = currentRisk.id;
                        quoteRiskRelationship.Quote__c = currentRisk.Quote__r.Id;
                    }
                    else {
                        if (bindParty.relatedRisk != null) 
                        {
                            quoteRiskRelationship.Related_Risk__c = bindParty.relatedRisk;
                        }
                        quoteRiskRelationship.Quote__c = currentQuote.Id;
                    }
                    if (bindparty.toBeDeleted==null ? false : bindparty.toBeDeleted ) quoteRiskRelationship.Deleted__c = true;
                    newQRRs.add(quoteRiskRelationship);
                }
            }
            insert newQRRs;
            buildPartiesList();
        } catch (Exception e) {
            Database.rollback(savePoint);
            throw e;
        }
    }
    
    /**
     * The purpose of this helper method is to retrieve the map from labels to values for the party and relationship picklist fields.
     */
    private static Map<String, String> getlabelToValue()
    {
        Map<String, String> labelToValue = new Map<String, String>();
        
        Schema.DescribeFieldResult partyTypeResult = Quote_Risk_Relationship__c.Party_Type__c.getDescribe();
        for (Schema.PicklistEntry party : partyTypeResult.getPicklistValues())
        {
            labelToValue.put(party.getLabel(), party.getValue());
        }
        
        Schema.DescribeFieldResult relationshipTypeResult = Quote_Risk_Relationship__c.Relationship_Type__c.getDescribe();
        for (Schema.PicklistEntry party : relationshipTypeResult.getPicklistValues())
        {
            labelToValue.put(party.getLabel(), party.getValue());
        }
        
        return labelToValue;
    }
    
    public void RequeryRelationshipTypeOptions() 
    {  
        for (BindParty currentBindParty : this.partiesList) {
            if (currentBindParty.id == this.selectedBindParty.id)    {
                if (currentBindParty.partyType == ADDITIONAL_INSURED) {
                    currentBindParty.relationshipTypeOptions = AdditionalInsuredTypeSelectOptionList;
                } 
                else { currentBindParty.relationshipTypeOptions = new List<SelectOption>(); }    
            }
        }   
    }
}