global class getInputValues implements vlocity_ins.VlocityOpenInterface2{
  
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
         system.debug('input received: ' + JSON.serializePretty(input));
         for(String key: input.keySet()){
           system.debug('key '+ key);
          // system.debug('Value '+ input.get(key));
           system.debug('VIPValue '+ JSON.serializePretty(input.get(key)));
           system.debug('!@methodName '+ methodName);
           system.debug('!@inputs '+ input.keySet());
           system.debug('!@inputs '+ input.values());
           system.debug('!@output '+ output);
           system.debug('!@options '+ options); 
       }
        return true; 
    }
}