/**
 * CSIOGet3QuoteRequestFactory
 * Author: Sara Al Mouaswas
 * Date: August 23, 2017
 * 
 * The purpose of this class is to generate the xml Request for the GET3QUOTE service 
 */
public class CSIOGet3QuoteRequestFactory extends CSIOGetUploadFinalizeRequestFactory implements CSIORequestFactory {
   
    
    public CSIOGet3QuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds, ri);
        this.soqlDataSet = sds; 
        this.ri = ri;
    }
    
    public String buildXMLRequest(){
        getHeaderXml();
        xml += CSIO128Header;
        xml += buildStandardRequestDocument();
        xml += CSIO128Footer;
        return xml;
    }
}