/**
* SMEQ_SicCodeService
* Author: Yasmin Shash
* Date: October 31 2016
* 
* The purpose of this service class is to return a list of SIC Codes in appetite
* for a region and or project.
*/
global with sharing class SMEQ_SicCodeService
{
    // Stores the list of SIC Code Detail Versions for an offering and region tuple.
    private static Map<String, List<SIC_Code_Detail_Version__c>> storedSCDVs = new Map<String, List<SIC_Code_Detail_Version__c>>();
    // Stores the map of case ID to the SIC Code Detail Version tuple.
    private static Map<Id, String> caseToTuple = new Map<Id, String>();
    // Stores the map of quote ID to the SIC Code Detail Version tuple.
    private static Map<Id, String> quoteToTuple = new Map<Id, String>();
    // Stores the map of case ID to the SIC Code Detail Version.
    private static Map<Id, SIC_Code_Detail_Version__c> storedCaseToSCDVs = new Map<Id, SIC_Code_Detail_Version__c>();
    // Stores the map of quote ID to the SIC Code Detail Version.
    private static Map<Id, SIC_Code_Detail_Version__c> storedQuoteToSCDVs = new Map<Id, SIC_Code_Detail_Version__c>();
    
    /**
     * The purpose of this inner class is to unify the definition
     * of a tuple for a specific SIC Code Detail Version.
     */
    private class SCDVTuple
    {
        String sicCode;
        String offering;
        String region;
        
        public SCDVTuple(String sicCode, String offering, String region)
        {
            this.sicCode = sicCode;
            this.offering = offering;
            this.region = region;
        }
        
        public SCDVTuple(Case c)
        {
            this.sicCode = c.bkrCase_SIC_Code__r.SIC_Code__c;
            this.offering = c.Offering_Project__c;
            this.region = c.bkrCase_Region__c;
        }
        
        public SCDVTuple(Quote__c q)
        {
            this.sicCode = q.Case__r.bkrCase_SIC_Code__r.SIC_Code__c;
            this.offering = q.Case__r.Offering_Project__c;
            this.region = q.Case__r.bkrCase_Region__c;
        }
        
        /*
         * Returns the unique key for a SIC Code Detail Version.
         */
        public override String toString()
        {
            return this.sicCode + this.offering + this.region;
        }
    }
    
    /*
     * This returns default list of SIC Code Details. Default SIC Code records are stored in 
     * SIC_Code_Detail__c object if SIC Code Parameter is null
     * All records are passed back.
     * 
     * @return List<SIC_Code_Detail__c>.
     */
    public static  List<SIC_Code_Detail__c> getDefaultSicCodes(String SICCode)
    {
        List<SIC_Code_Detail__c> sicCodeDetailList = new List<SIC_Code_Detail__c>();
        
        //get SIC Code
        if (SICCode != null)
        {
            List<SIC_Code__c> sicCodeList = [
                SELECT ID, SIC_Code_Detail__c
                FROM SIC_Code__c
                WHERE ID =:SICCode
            ];
            
            if (!sicCodeList.isEmpty())
            {
                String sicCodeDetailID = sicCodeList[0].SIC_Code_Detail__c;
                System.debug(sicCodeDetailID);
                sicCodeDetailList = [
                    SELECT Id, Action__c,Canadian_Liability__c,
                    Crime_Hazard__c,
                    E_O_Liability__c,Fid_Hazard__c,Mid_Market__c,Name,Non_RSA__c,
                    Poll_Liability__c,Property_Hazard__c,RAG__c,Referral_Level__c,
                    Segment__c,SIC_Code__c,SME__c,
                    Subsegment__c, USA_Liability__c
                    FROM SIC_Code_Detail__c
                    WHERE Id =:sicCodeDetailID
                ];
            }
        }
        
        else
        {
            sicCodeDetailList = [
                SELECT id, Action__c,Canadian_Liability__c,
                Crime_Hazard__c,
                E_O_Liability__c,Fid_Hazard__c, Mid_Market__c,Name,Non_RSA__c,
                Poll_Liability__c,Property_Hazard__c,RAG__c,Referral_Level__c,
                Segment__c,SIC_Code__c,SME__c,
                Subsegment__c, USA_Liability__c
                FROM SIC_Code_Detail__c
            ];
        }
        
        return sicCodeDetailList;
    }
    
    
    /*
     * This returns default list of SIC Code Details for case.
     * 
     * @return List<SIC_Code_Detail__c>.
     */
    public static  List<SIC_Code_Detail__c> getDefaultSicCodesForCase(String caseID)
    {
        List<SIC_Code_Detail__c> sicCodeDetailList = new List<SIC_Code_Detail__c>();
        
        //get SIC Code Details associated with case
        List<Case> caseSICCodeList = [
            SELECT id, bkrCase_SIC_Code__c
            FROM Case
            WHERE id = :caseID
        ];
        
        if (!caseSICCodeList.isEmpty())
        {
            String SICCode = caseSICCodeList[0].bkrCase_SIC_Code__c;
            
            if (SICCode != null)
            {
                List<SIC_Code__c> sicCodeList = [
                    SELECT ID, SIC_Code_Detail__c
                    FROM SIC_Code__c
                    WHERE ID = :SICCode
                ];
                
                if (!sicCodeList.isEmpty())
                {
                    String sicCodeDetailID = sicCodeList[0].SIC_Code_Detail__c;
                    System.debug(sicCodeDetailID);
                    sicCodeDetailList = [
                        SELECT Action__c,Canadian_Liability__c,
                        Crime_Hazard__c,
                        E_O_Liability__c,Fid_Hazard__c,Id,Mid_Market__c,Name,Non_RSA__c,
                        Poll_Liability__c,Property_Hazard__c,RAG__c,Referral_Level__c,
                        Segment__c,SIC_Code__c,SME__c,
                        Subsegment__c, USA_Liability__c
                        FROM SIC_Code_Detail__c
                        WHERE Id =:sicCodeDetailID
                    ];
                }
            }
        }
        
        return sicCodeDetailList;
    }
    
    /*
     * This returns a list of SIC Code Detail Versions based on the region and or project assoicated with a case
     * 
     * @param String caseID.
     * @return List<SIC_Code_Detail__c>.
     */
    public static List<SIC_Code_Detail_Version__c> getSicCodesInAppetiteForCase(String caseID)
    {
        List<SIC_Code_Detail_Version__c> sicCodesList = new List<SIC_Code_Detail_Version__c>();
        
        SIC_Code_Detail_Version__c scdv = storedCaseToSCDVs.get(caseID);
        
        if (scdv == null)
        {
            List<Case> caseList = [
                SELECT id, bkrCase_SIC_Code__c, bkrCase_SIC_Code__r.SIC_Code__c, Offering_Project__c, bkrCase_Region__c
                FROM Case
                WHERE id = :caseID
            ];
            
            List<SCDVTuple> scdvs = new List<SCDVTuple>();
            for (Case c : caseList)
            {
                scdvs.add(new SCDVTuple(c));
            }
            
            sicCodesList = getSicCodesInAppetiteForCasesHelper(scdvs);
            // Null check given for avoiding test class failure
            if(!sicCodesList.isEmpty())
                storedCaseToSCDVs.put(caseID, sicCodesList[0]);
        }
        else
        {
            sicCodesList.add(scdv);
        }
        
        return sicCodesList; 
    }
    
    /**
     * This returns a list of SIC Code Detail Versions associated for a list of cases.
     * 
     * @param List<ID> cases: A list of case IDs.
     * @return: A List of SIC Code Detail Versions.
     */
    public static List<SIC_Code_Detail_Version__c> getSicCodesInAppetiteForCases(List<Id> cases)
    {
        List<Id> nonHitCases = new List<Id>();
        
        // Get a list of all IDs which were not found in the static map.
        for (Id cid : cases)
        {
            if (storedCaseToSCDVs.get(cid) == null)
            {
                nonHitCases.add(cid);
            }
        }
        
        List<SIC_Code_Detail_Version__c> inAppetite = new List<SIC_Code_Detail_Version__c>();
        
        if (nonHitCases.size() > 0)
        {
            // Retrieve all Cases to retrieve their SIC Code Detail Versions.
            List<Case> caseList = [
                SELECT id, bkrCase_SIC_Code__c, bkrCase_SIC_Code__r.SIC_Code__c, Offering_Project__c, bkrCase_Region__c
                FROM Case
                WHERE id IN :nonHitCases
            ];
            
            List<SCDVTuple> scdvList = new List<SCDVTuple>();
            for (Case c : caseList)
            {
                caseToTuple.put(c.Id, (new SCDVTuple(c)).toString());
                scdvList.add(new SCDVTuple(c));
            }
            
            for (SIC_Code_Detail_Version__c scdv : getSicCodesInAppetiteForCasesHelper(scdvList))
            {
                for (Id cid : caseToTuple.keySet())
                {
                    if (supportedBySicCodeDetailVersion(scdv, caseToTuple.get(cid)))
                    {
                        storedCaseToSCDVs.put(cid, scdv);
                    }
                }
            }
        }
        
        // Retrieve all SIC Code Detail Versions by ID from the stored static map.
        for (Id cid : cases)
        {
            inAppetite.add(storedCaseToSCDVs.get(cid));
        }
        
        return inAppetite;
    }
    
    /**
     * This returns a list of SIC Code Detail Versions associated for a list of quotes.
     * 
     * @param List<ID> quotes: A list of quote IDs.
     * @return: A List of SIC Code Detail Versions.
     */
    public static Map<ID, SIC_Code_Detail_Version__c> getSicCodesInAppetiteForQuotes(List<Id> quotes)
    {
        List<Id> nonHitQuotes = new List<Id>();
        
        // Get a list of all IDs which were not found in the static map.
        for (Id qid : quotes)
        {
            if (storedQuoteToSCDVs.get(qid) == null)
            {
                nonHitQuotes.add(qid);
            }
        }
        
        Map<Id, SIC_Code_Detail_Version__c> inAppetite = new Map<Id, SIC_Code_Detail_Version__c>();
        
        if (nonHitQuotes.size() > 0)
        {
            // Extract the fields needed to retrieve the SIC Code Detail Version.
            List <Quote__c> quoteCase = [
                SELECT id, Case__c, Case__r.bkrCase_SIC_Code__r.SIC_Code__c, 
                Case__r.Offering_Project__c, Case__r.bkrCase_Region__c
                FROM Quote__c
                WHERE id in :nonHitQuotes
            ];
                        
            List<SCDVTuple> scdvList = new List<SCDVTuple>();
            for (Quote__c q : quoteCase)
            {
                quoteToTuple.put(q.Id, (new SCDVTuple(q)).toString());
                scdvList.add(new SCDVTuple(q));
            }
            
            for (SIC_Code_Detail_Version__c scdv : getSicCodesInAppetiteForCasesHelper(scdvList))
            {
                for (Id qid : quoteToTuple.keySet())
                {
                    if (supportedBySicCodeDetailVersion(scdv, quoteToTuple.get(qid)))
                    {
                        storedQuoteToSCDVs.put(qid, scdv);
                    }
                }
            }
        }
        
        // Retrieve all SIC Code Detail Versions by ID from the stored static map.
        for (Id qid : quotes)
        {
            inAppetite.put(qid, storedQuoteToSCDVs.get(qid));
        }
        
        return inAppetite;
    }
    
    /**
     * This returns a list of SIC Code Detail Versions associated for a list of risks.
     * 
     * @param List<ID> risks: A list of risk IDs.
     * @return: A List of SIC Code Detail Versions.
     */
    public static Map<ID, SIC_Code_Detail_Version__c> getSicCodesInAppetiteForRisks(List<Id> risks)
    {
        Map<Id, Sic_Code_Detail_Version__c> riskToSCDVs = new Map<Id, Sic_Code_Detail_Version__c>();
        
        Map<Id, List<Id>> quoteToRiskIds = new Map<Id, List<Id>>();
        for (Risk__c r : [
            SELECT id, quote__c
            FROM Risk__c
            WHERE id IN :risks
        ])
        {
            List<Id> tempRisks = quoteToRiskIds.get(r.Quote__c);
            if (tempRisks == null) {
                quoteToRiskIds.put(r.Quote__c, new List<Id> {r.Id});
            }
            else {
                tempRisks.add(r.Id);
            }
        }
        
        Map<Id, Sic_Code_Detail_Version__c> quote2SCDVs = getSicCodesInAppetiteForQuotes(new List<Id>(quoteToRiskIds.keySet()));
        for (Id qid : quote2SCDVs.keySet())
        {
            for (Id rId : quoteToRiskIds.get(qid)) {
                riskToSCDVs.put(rId,quote2SCDVs.get(qid));
            }
        }
        
        return riskToSCDVs;
    }
    
    /**
     * This helper method will check if the SIC Code Detail Version's region and offering values supports the scdvTuple.
     * @param scdv: The Sic Code Detail Version.
     * @param scdvTuple: The String representation of a SCDV Tuple (Sic Code, Offering, Region).
     * @return: True if the Sic Code Detail Version supports the scdvTuple, false otherwise.
     */
    private static Boolean supportedBySicCodeDetailVersion(Sic_Code_Detail_Version__c scdv, String scdvTuple) {
        if (scdv.Region__c != null && scdv.Offering__c != null) {
            String sicCode = scdv.SIC_Code__r.SIC_Code__c;
            for (String o : scdv.Offering__c.split(';')) {
                for (String r : scdv.Region__c.split(';')) {
                    if (scdvTuple == new SCDVTuple(sicCode, o, r).toString()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * FP-748
     * Author: James Lee
     * 
     * The purpose of this method is to provide a bulkified version of the getSicCodesInAppetiteForCase function above.
     * This returns a list of SIC Code Detail Versions based on the SIC Code, offering, and region assoicated with a list of cases.
     */
    private static List<SIC_Code_Detail_Version__c> getSicCodesInAppetiteForCasesHelper(List<SCDVTuple> cases)
    {
        System.debug('Cases in Apetite being called *******');
        List<SIC_Code_Detail_Version__c> sicCodesList = new List<SIC_Code_Detail_Version__c>();
        
        System.debug('Cases in Apetite case list ******' + cases);
        // This block establishes what the unique tuples of (SIC Code, Offering, Region) exist within the list of cases.
        Set<String> existingTuples = new Set<String>();
        List<SCDVTuple> uniqueCaseTuples = new List<SCDVTuple>();
        for (SCDVTuple c : cases)
        {
            //String tuple = getSCDVTuple(c);
            // "c.sicCode != null" check added for defect PWP:363
            if (c.sicCode != null && c.offering != null && c.region != null && !existingTuples.contains(c.toString()))
            {
                uniqueCaseTuples.add(c);
                existingTuples.add(c.toString());
            }
        }
        
        // Iterate over the unique tuples and generate a dynamic SOQL query 
        // to retrieve all unique SIC Code Detail Versions within the list of cases.
        if (!uniqueCaseTuples.isEmpty())
        {
            String qry = 'SELECT Id, Name, Action__c, ' + 
                'Application_Enabled__c, Building_Required__c, ePolicy_Package__c,' +
                'Canadian_Liability__c, USA_Liability__c, ' +
                'Crime_Hazard__c, Fid_Hazard__c, Property_Hazard__c, ' +
                'E_O_Liability__c, Poll_Liability__c, ' +
                'Coverage_Add_ons__c, Coverage_Defaults__c, ' +
                'RAG__c, Referral_Level__c, ' +
                'Segment__c, Subsegment__c, ' +
                'SME__c, Non_RSA__c, Mid_Market__c, ' +
                'SIC_Code__r.SIC_Code__c, ' +
                'Short_Description_En__c, Short_Description_Fr__c, ' +
                'Description_Operations_En__c, Description_Operations_Fr__c, ' +
                'Category__c, Subcategory__c, ' +
                'Offering__c, Region__c, Disabled_Policy_Actions__c ' +
                'FROM SIC_Code_Detail_Version__c ';
                
            String whereClause = '';
            for (SCDVTuple c : uniqueCaseTuples)
            {
                if (whereClause == '')
                {
                    whereClause += 'WHERE ';
                }
                else
                {
                    whereClause += 'OR ';
                }
                whereClause += '(SIC_Code__r.SIC_Code__c = \'' + String.escapeSingleQuotes(c.sicCode) + '\' ';
                whereClause += 'AND Offering__c INCLUDES (\'' + String.escapeSingleQuotes(c.offering) + '\')';
                whereClause += 'AND Region__c INCLUDES (\'' + String.escapeSingleQuotes(c.region) + '\') ';
                whereClause += ') ';
            }
            qry += whereClause;
            
            System.debug('Alan Debug: ' + qry);
            
            sicCodesList = Database.query(qry);
        }
        
        return sicCodesList;
    }
    
    
    /*
     * This returns a list of SIC Code Details based on the region and or offering
     * 
     * @param String Project, String Region, ID sicCode.
     * @return List<SIC_Code_Detail__c>.
     */
    public static List<SIC_Code_Detail_Version__c> getSicDetailsBySicProjectRegion(String offering, String region, ID sicCode)
    {
        List<SIC_Code_Detail_Version__c> allSicCodesInAppetite = getSicCodesByProjectAndRegion(offering, region);
        List<SIC_Code_Detail_Version__c> ret = new List<SIC_Code_Detail_Version__c>();
        if(allSicCodesInAppetite != null){
            for (SIC_Code_Detail_Version__c scdv : allSicCodesInAppetite)
            {
                if (scdv.sic_code__r.id == sicCode)
                {
                    ret.add(scdv);
                    break;
                }
            }
        }
        
        return ret;
    }
    
    /*
     * This returns a list of SIC Code Details based on the region and or offering
     * 
     * @param String Project, String Region, String sicCode.
     * @return List<SIC_Code_Detail__c>.
     */
    public static List<SIC_Code_Detail_Version__c> getSicDetailsBySicProjectRegion(String offering, String region, String sicCode)
    {
        List<SIC_Code_Detail_Version__c> allSicCodesInAppetite = getSicCodesByProjectAndRegion(offering, region);
        List<SIC_Code_Detail_Version__c> ret = new List<SIC_Code_Detail_Version__c>();
        if(allSicCodesInAppetite != null){
            for (SIC_Code_Detail_Version__c scdv : allSicCodesInAppetite)
            {
                if (scdv.sic_code__r.sic_code__c == sicCode)
                {
                    ret.add(scdv);
                    break;
                }
            }
        }
        return ret;
    }
    
    /*
     * This returns a list of SIC Code Details based on the region and or offering
     * 
     * @param String Offering, String Region.
     * @return List<SIC_Code_Detail__c>.
     */
    public static List<SIC_Code_Detail_Version__c> getSicCodesByProjectAndRegion(String offering, String region)
    {
        String uniqueTuple = offering + region;
        
        List<SIC_Code_Detail_Version__c> sicCodesList = storedSCDVs.get(uniqueTuple);
        if (sicCodesList != null)
        {
            return sicCodesList;
        }
        system.debug('offering : '+offering);
        system.debug('region : '+region);
        
        //project is specified and region is specified. If not match is found return an empty list
        if (offering != null && region != null)
        {
            sicCodesList = [
                SELECT Id, Name, Action__c,
                Application_Enabled__c, Building_Required__c, ePolicy_Package__c,
                Canadian_Liability__c, USA_Liability__c,
                Crime_Hazard__c, Fid_Hazard__c, Property_Hazard__c,
                E_O_Liability__c, Poll_Liability__c,
                Coverage_Add_ons__c, Coverage_Defaults__c,
                RAG__c, Referral_Level__c,
                Segment__c, Subsegment__c, 
                SME__c, Non_RSA__c, Mid_Market__c,
                SIC_Code__r.SIC_Code__c,
                Short_Description_En__c, Short_Description_Fr__c, // Broker tool fields
                Application_Name__c,
                Description_Operations_En__c, Description_Operations_Fr__c, // Broker tool fields
                Category__c, Subcategory__c, Monoline__c,
                Region__c, Offering__c, Disabled_Policy_Actions__c
                FROM SIC_Code_Detail_Version__c
                WHERE Region__c INCLUDES (:region)
                AND Offering__c INCLUDES (:offering)
                ORDER BY Category__c ASC
            ];
            
            storedSCDVs.put(uniqueTuple, sicCodesList);
        }
        return sicCodesList;
    }
}