/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class BankTransitTriggerTest {

    static testMethod void myUnitTest() {
    	
    	// create a bank transit number
        Bank_Transit_Number__c bankTransitOne = new Bank_Transit_Number__c();
    	bankTransitOne.Effective_Date__c = Date.today().addDays(-1);
    	bankTransitOne.Role_Type__c = 'FI';
    	bankTransitOne.Client__c = 123456781;
    	bankTransitOne.Active_to__c = Date.today().addYears(1);
    	bankTransitOne.Active_From__c = Date.today().addDays(-1);
    	bankTransitOne.Inter_Code__c = '000304821';
    	bankTransitOne.Client_Reference__c = 123456781;
    	bankTransitOne.Role_Code__c = '000304821';
    	bankTransitOne.Financial_Institution_Name__c = 'Bank One';
    	bankTransitOne.Address1__c = 'Unit 14';
    	bankTransitOne.Address2__c = '1 West Street';
    	bankTransitOne.City__c = 'Toronto';
    	bankTransitOne.Province__c = 'ON';
    	bankTransitOne.Postal_Code__c = 'M4S2P2';
    	bankTransitOne.Client_Type__c ='FI';
    	bankTransitOne.Withdrawal_Date__c = null;
    	insert bankTransitOne;
    	
    	// now check whether there is an account called 'Bank One'
    	List<Account> bankTransitAccountList = [SELECT Id, RecordTypeId, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Bank_Transit_Number__r.Id FROM Account WHERE Bank_Transit_Number__r.Id = :bankTransitOne.Id];
    	System.assertEquals(bankTransitAccountList.size(), 1, 'Should be a single account');
    	
    	Account bankTransitAccount = bankTransitAccountList[0];
    	// check that the attributes line up
    	System.assertEquals(bankTransitOne.Address1__c + ' ' + bankTransitOne.Address2__c, bankTransitAccount.BillingStreet, 'Bank transit address concatenation should equal account transit');
    	System.assertEquals(bankTransitOne.City__c, bankTransitAccount.BillingCity, 'City  should be the same');
    	System.assertEquals(bankTransitOne.Province__c, bankTransitAccount.BillingState, 'province should be the same as the account state');
    	System.assertEquals(bankTransitOne.Postal_Code__c, bankTransitAccount.BillingPostalCode, 'Postal code should be the same');
    	
    	// check the record type
    	Id recordTypeId = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' and DeveloperName = 'Financial_Institution'].Id;
       	System.assertEquals(bankTransitAccount.RecordTypeId, recordTypeId, 'Record type should be financial institution');
    	
    	// name is the concatentation of name and first couple of address lines - luckily account name accommodate 255 characters
    	String bankAccountNameConcatentation = bankTransitOne.Financial_Institution_Name__c  + ' ' + bankTransitOne.Address1__c + ' ' + bankTransitOne.Address2__c;
    	System.assertEquals(bankAccountNameConcatentation, bankTransitAccount.Name,'Name should be a concatentation of name and address fields');
    	
    	// update the bank transit record and check that the updates are propagated
    	bankTransitOne.Financial_Institution_Name__c = 'Bank Six';
    	update bankTransitOne;
    	
    	bankTransitAccountList = [SELECT Id, RecordTypeId, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Bank_Transit_Number__r.Id FROM Account WHERE Bank_Transit_Number__r.Id = :bankTransitOne.Id];
    	bankTransitAccount = bankTransitAccountList[0];
    	System.assertEquals(bankTransitAccountList.size(), 1, 'Should be a single account');
    	bankAccountNameConcatentation = bankTransitOne.Financial_Institution_Name__c  + ' ' + bankTransitOne.Address1__c + ' ' + bankTransitOne.Address2__c;
    	System.assertEquals(bankAccountNameConcatentation, bankTransitAccount.Name,'Name should be a concatentation of name and address fields');
    	
    }
}