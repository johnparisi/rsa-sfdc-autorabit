global with sharing class SMEQ_DnBRequestModel
{
    public String name {public get; public set;}
    public String streetAddress {public get; public set;}
    public String city {public get; public set;}
    public String state {public get; public set;}
    public String postalCode {public get; public set;}
    public String phoneNumber {public get; public set;}
    public String countryCode {public get; public set;}
    public String dunsnumber {public get; public set;}
   
}