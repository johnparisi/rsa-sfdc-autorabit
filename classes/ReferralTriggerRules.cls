/**
 * ReferralTriggerRules
 * Author: James Lee
 * Date: January 20 2017
 * Feature: FP-518
 * 
 * The purpose of this service class is to store all referral rules implementing the ReferralTriggerRule interface.
 */
public class ReferralTriggerRules
{
    public static final String INTERFACE_CLASS_NAME = 'ReferralTriggerRules';
    
    public static final String RETAIL_EXCLUDING_AUTOMOBILE = 'SEG-RTEA';
    public static final String BUSINESS_PROFESSIONAL_SERVICES = 'SEG-BPS';
    public static final String CONSTRUCTION_INSTALLATION_ERECTION_SEG = 'SEG-CEI';
    public static final String CONTRACTING_PROFESSIONAL_SERVICES ='SEG-CPS';
    public static final String HOSPITALITY_SERVICES = 'SEG-HHR';
    public static final String REALTY_SERVICES = 'SEG-RE';
    
    public static final String RESIDENTIAL_TENANT = 'Residential Tenant';
    
    public static final Integer MAXIMUM_NUMBER_OF_CLAIMS = 2;
    public static final Integer MINUMUM_NUMBER_OF_UNITS = 6;
    public static final Integer MAXIMUM_CLAIMS_AMOUNT = 100000;
    public static final Integer LIQUOR_FOOD_REVENUE_MAXIMUM = 5000000;
    
    public static final Integer MAXIMUM_PROPERTY_VALUE = 5000000;
    public static final Integer MAXIMUM_TOTAL_REVENUE = 5000000;
    public static final Integer MAXIMUM_TOTAL_REVENUE_SME_RETAIL = 10000000;
    public static final Integer MAXIMUM_TOTAL_RENTAL_REVENUE = 2000000;
    //FRZR-108
    public static final String INTERFACE_CLASS_NAME_ESB = 'ReferralTriggerRuleFromESB';    
    
    /*===================================================================
     * Interface
     *==================================================================*/
    
    /*
     * Method that checks if a given quoteRequest triggers a referral rule. 
     * Rules will be defined in classes that implement this interface.
     * 
     * @return: String describing the reason for the ReferralTrigger. E.g 'Total Claim Amount Greater than 5 Million'
     */
    public interface ReferralTriggerRule
    {
        Boolean isReferral(SMEQ_QuoteRequest quoteRequest);
        Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords);

    }
    
    public interface ReferralTriggerRuleFromESB
    {
        Boolean isReferral(String esbResponse);
        Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords);

    }
    
    /*===================================================================
     * Referral Triggers
     *==================================================================*/
    
    /*
     * US Revenue is greater than $0
     */
    public class REF_USRevenueEnteredRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            List<SMEQ_QuestionAnswerModel> questionAnswermodels = quoteRequestToQuestionAnswerModel(quoteRequest);
            List<String> answers = retrieveAnswers(questionAnswermodels, SicQuestionService.CODE_US_REVENUE);
            
            for (Decimal answer : toNumbersList(answers))
            {
                if (answer > 0)
                {
                    return true;
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            return false; 
        }
    }
    
    /*
     * Foreign Revenue is greater than $0
     */
    public class REF_ForeignRevenueEnteredRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            List<SMEQ_QuestionAnswerModel> questionAnswers = quoteRequestToQuestionAnswerModel(quoteRequest);
            List<String> answers = retrieveAnswers(questionAnswers, SicQuestionService.CODE_FOREIGN_REVENUE);
            
            for (Decimal answer : toNumbersList(answers))
            {
                if (answer > 0)
                {
                    return true;
                }
            }
            return false;
        }
	   public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
       {
           return false; 
       }
    }
    
    /*
     * The sum of claims (i.e. total claim amount) is equal or greater than $100,000
     */
    public class REF_TotalClaimAmountRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (quoteRequest.businessDetails != null && 
                quoteRequest.businessDetails.claims != null)
            {
                if (getTotalClaimAmount(quoteRequest.businessDetails.claims) >= MAXIMUM_CLAIMS_AMOUNT)
                {
                    return true;
                }
            }
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
        
        /**
         * Get total claim amount based on a list of claims
         * @param List<SMEQ_ClaimDetailsModel> claimsList
         * @return Decimal sum of claims amount in claim list
         */
        private Decimal getTotalClaimAmount(List<SMEQ_ClaimDetailsModel> claimsList)
        {
            Decimal claimAmount = 0;
            for (SMEQ_ClaimDetailsModel aClaim : claimsList)
            {
                if (aClaim.amount != null)
                {
                    claimAmount += aClaim.amount;
                }
            }
            return claimAmount;
        }
    }
    
    /*
     * Business Segment = ALL
     * Two or more previous claims
     */
    public class REF_NumberOfClaimsRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (quoteRequest.businessDetails != null)
            {
                if (quoteRequest.businessDetails.claims != null &&
                    quoteRequest.businessDetails.claims.size() >= MAXIMUM_NUMBER_OF_CLAIMS)
                {
                    return true;
                }
            }
            
            return false;
        }
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            return false; 
        }
    }

    /*
     * Business Segment = Retail excluding Automobile
     * Foreign revenue > 10% of total Revenue
     */
    public class REF_ForeignRevenuePercentageRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (getSegmentCode() == ReferralTriggerRules.RETAIL_EXCLUDING_AUTOMOBILE)
            {
                Decimal totalRevenue = getTotalRevenue(quoteRequest);
                Decimal foreignRevenue = getForeignRevenue(quoteRequest);
                
                if (foreignRevenue > 0 && totalRevenue > 0)
                {
                    Decimal foreignRevenuePercentage = (foreignRevenue / totalRevenue) * 100;
                    
                    if (foreignRevenuePercentage > 10)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
        
        private Decimal getForeignRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            return getAnswerSum(quoteRequest, SicQuestionService.CODE_FOREIGN_REVENUE);
        }
        
        private Decimal getTotalRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal canadianRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_CANADIAN_REVENUE);
            Decimal usRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_US_REVENUE);
            Decimal foreignRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOREIGN_REVENUE);
            Decimal liquorRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE);
            Decimal foodRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOOD_REVENUE);
            
            return canadianRevenue + usRevenue + foreignRevenue + liquorRevenue + foodRevenue;
        }
    }
    
    /*
     * Scenario 1: Business Segment = Professional Services and Total Revenue >= 5,000,000
     */
    public class REF_TotalRevenueRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal totalRevenue = getTotalRevenue(quoteRequest);
            
            if (totalRevenue > MAXIMUM_TOTAL_REVENUE)
            {
                // Regretfully, this specific clause must be included as the framework cannot handle this odd case.
                if (getSegmentCode() == RETAIL_EXCLUDING_AUTOMOBILE &&
                    ProcessReferralTriggerRules.sicCodeDetailVersion.offering__c == 'SPRNT v1')
                {
                    return false;
                }
                
                return true;
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            return false; 
        }
        
        private Decimal getTotalRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal canadianRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_CANADIAN_REVENUE);
            Decimal usRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_US_REVENUE);
            Decimal foreignRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOREIGN_REVENUE);
            Decimal liquorRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE);
            Decimal foodRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOOD_REVENUE);
            
            return canadianRevenue + usRevenue + foreignRevenue + liquorRevenue + foodRevenue;
        }
    }
    
    /*
     * Scenario 2: Business Segment = Retail Excluding Automobile and Total Revenue > 10,000,000
     */
    public class REF_TotalRevenueRuleSMERetailOnly implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (getSegmentCode() == RETAIL_EXCLUDING_AUTOMOBILE)
            {
                Decimal totalRevenue = getTotalRevenue(quoteRequest);
                
                if (totalRevenue > MAXIMUM_TOTAL_REVENUE_SME_RETAIL)
                {
                    return true;
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
        
        private Decimal getTotalRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal canadianRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_CANADIAN_REVENUE);
            Decimal usRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_US_REVENUE);
            Decimal foreignRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOREIGN_REVENUE);
            Decimal liquorRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE);
            Decimal foodRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOOD_REVENUE);
            
            return canadianRevenue + usRevenue + foreignRevenue + liquorRevenue + foodRevenue;
        }
    }
    
    /*
     * The combined value of Building + Stock + Equipment + Content is equal to or greater than $5,000,000
     */    
    public class REF_CombinedLocationValueRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (getLocationTotalValue(quoteRequest) >= MAXIMUM_PROPERTY_VALUE)
            {
                return true;
            }
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
        
        
        /**
         * Get the total value of equipment, stock and building for a list of locations
         * @return Decimal total value
         */
        private Decimal getLocationTotalValue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal totalBuilding = 0;
            
            if (quoteRequest.businessDetails.locationRisks != null)
            {
                for (SMEQ_LocationDetailsModel location : quoteRequest.businessDetails.locationRisks)
                {
                    if (location.buildingValue != null)
                    {
                        totalBuilding += location.buildingValue;
                    }
                }
            }
            
            return totalBuilding + getTotalPropertyValue(quoteRequest);
        }
        
        private Decimal getTotalPropertyValue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal totalEquipment = getAnswerSum(quoteRequest, SicQuestionService.CODE_EQUIPMENT);
            Decimal totalStock = getAnswerSum(quoteRequest, SicQuestionService.CODE_STOCK);
            Decimal totalContent = getAnswerSum(quoteRequest, SicQuestionService.CODE_CONTENTS);
            
            return totalStock + totalEquipment + totalContent;
        }
    }
    
    /*
     * The broker enters a Rental Revenue greater than $2,000,000
     */
    public class REF_RentalRevenueRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (quoteRequest.businessDetails != null)
            {
                if (quoteRequest.businessDetails.locationRisks != null)
                {
                    Decimal totalRental = 0;
                    for (SMEQ_LocationDetailsModel location : quoteRequest.businessDetails.locationRisks)
                    {
                        totalRental += location.rentalRevenue == null ? 0 : location.rentalRevenue;
                    }
                    
                    if (totalRental > MAXIMUM_TOTAL_RENTAL_REVENUE)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
    }
    
    /*
     * Business segment = Hospitality
     * The broker enters liquor revenue greater than 25% of Total Revenue. 
     * Total Revenue = Food and Other Revenue + Liquor Revenue
     */
    public class REF_LiquorRevenueRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (getSegmentCode() == HOSPITALITY_SERVICES)
            {
                Decimal totalRevenue = getTotalRevenue(quoteRequest);
                
                if (totalRevenue > 0)
                {
                    Decimal liquorRevenue = getLiquorRevenue(quoteRequest);
                    
                    Decimal liquorRevenuePercentage = (liquorRevenue / totalRevenue) * 100;
                    
                    if (liquorRevenuePercentage > 25)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
        
        private Decimal getTotalRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal liquorRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE);
            Decimal foodRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOOD_REVENUE);
            
            return liquorRevenue + foodRevenue;
        }
        
        private Decimal getLiquorRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            return getAnswerSum(quoteRequest, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE);
        }
    }
    
    /*
     * Business segment = Hospitality
     * The broker enters liquor and food revenue greater than 5,000,000
     */
    public class REF_SumOfLiquorFoodRevenueRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (getSegmentCode() == HOSPITALITY_SERVICES)
            {
                Decimal foodAndLiquorRevenue = getFoodAndLiquorRevenue(quoteRequest);
                
                if (foodAndLiquorRevenue > LIQUOR_FOOD_REVENUE_MAXIMUM)
                {
                    return true;
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
        
        private Decimal getFoodAndLiquorRevenue(SMEQ_QuoteRequest quoteRequest)
        {
            Decimal liquorRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_LIQUOR_ANNUAL_REVENUE);
            Decimal foodRevenue = getAnswerSum(quoteRequest, SicQuestionService.CODE_FOOD_REVENUE);
            
            return liquorRevenue + foodRevenue;
        }
    }
    
    /*
     * The broker enters number of units less than 6
     */
    public class REF_NumberOfUnitsRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            String businessSegment = getSegmentCode();
            String businessSubSegment = ProcessReferralTriggerRules.sicCodeDetailVersion.Subsegment__c;
            
            if (businessSegment == REALTY_SERVICES && 
                businessSubSegment == RESIDENTIAL_TENANT)
            {
                List<SMEQ_QuestionAnswerModel> questionAnswers = quoteRequestToQuestionAnswerModel(quoteRequest);
                List<String> answers = retrieveAnswers(questionAnswers, SicQuestionService.CODE_NUMBER_OF_UNITS);
                
                for (Decimal units : toNumbersList(answers))
                {
                    if (units < MINUMUM_NUMBER_OF_UNITS)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
    }
    
    /*
     * Response of "Yes" to any additional questions updates the Referral Level of the SIC code
     */
    public class REF_AdditionalQuestionsRule implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            if (quoteRequest.affirmativeConditionAnsweredYes != null &&
                quoteRequest.affirmativeConditionAnsweredYes)
            {
                return true;
            }
            
            return false;
        }
        
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
             return false; 
        }
    }
    
    public class REF_NumberOfStoriesChangedRule implements ReferralTriggerRule{
      
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            return false; 
        }

        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            List<Risk__c> changedFields = (List <Risk__c>) Utils.getChangedObjects(risks, oldRecords, new List<String> {'Number_of_Stories__c'});
            if(!changedFields.isEmpty()){
                return true;
            } else { 
                return false;
            }
        }
    }

    public class REF_ConstructionTypeChangedRule implements ReferralTriggerRule
    {
      
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            return false; 
          }

        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            List<Risk__c> changedFields = (List <Risk__c>) Utils.getChangedObjects(risks, oldRecords, new List<String> {'Construction_Type__c'});
            if(!changedFields.isEmpty()){
                return true;
            } else {
                return false; 
            }
        }
    }

    public class REF_YearBuiltChangedRule implements ReferralTriggerRule
    {
      public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
      {
        return false; 
      }

        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            List<Risk__c> changedFields = (List <Risk__c>) Utils.getChangedObjects(risks, oldRecords, new List<String> {'Year_Built__c'});
            if(!changedFields.isEmpty()){
                return true;
            } else {
                return false; 
            }
        }

    }

    public class REF_SprinklerCoverageChangedRule implements ReferralTriggerRule {
      
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            return false; 
        }

        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            List<Risk__c> changedFields = (List <Risk__c>) Utils.getChangedObjects(risks, oldRecords, new List<String> {'Sprinkler_Coverage__c'});
            if(!changedFields.isEmpty()){
                return true;
            } else {
                return false; 
            }
        }

    }

     /* FRZR-81
     * Business Segment = ALL
     * One or more previous claims
     */
    public class REF_FloodZone4AndPriorClaim implements ReferralTriggerRule
    {
        public Boolean isReferral(SMEQ_QuoteRequest quoteRequest)
        {
            // check if Flood Zone = 4 exists
            boolean locationfz4Exists = false;        
            if (quoteRequest.quoteID != null) {
                List<Risk__c> riskFloodZone4 = [Select ID From Risk__c Where Quote__c = : quoteRequest.quoteID And flood_zone__c = 4.0];
                if (riskFloodZone4.size() > 0)
                    locationfz4Exists = true;
    
            }
            if (quoteRequest.businessDetails != null)
            {
                // Check if there was a claim of type "Flood"
                boolean typeOfClaimIsFlood = false;
                if (quoteRequest.businessDetails.claims != null) {
                    for (integer i =0; i<quoteRequest.businessDetails.claims.size(); i++ ) {
                        if (quoteRequest.businessDetails.claims[i].claimType != null) {
                            Map<String, String> claimTypeMap = new Map<String, String>();
                            claimTypeMap = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('Type_of_Claim__c');
                            String floodLabel = claimTypeMap.get(quoteRequest.businessDetails.claims[i].claimType);                        	
                            if (floodLabel == 'Flood') {
                                typeOfClaimIsFlood = true;
                                break;
                            }
                        }
                    }                    
                }
                if (locationfz4Exists && typeOfClaimIsFlood) {
                    return true;
                }
            }
            
            return false;
        }
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            return false; 
        }
    }
    
    /* FRZR-108
     * Business Segment = ALL
     * One or more previous claims
     */
    public class REF_FloodZone4AndPriorClaimFromESB implements ReferralTriggerRuleFromESB
    {
        public Boolean isReferral(String esbResponse)
        {
            // check if Flood Zone = 4 exists
            boolean locationfz4Exists = false;
            integer floodZone;
            for (String floodZBlock : XMLHelper.extractBlocks(esbResponse, 'rsa:FloodZone'))
            {
                floodZone = Integer.valueOf(XMLHelper.retrieveValue(XMLHelper.stripNamespace(floodZBlock, 'rsa'),CSIOXMLProcessor.FLOOD_ZONE_PATH));
                if (floodZone == 4) {
                    locationfz4Exists = true;
                    break;
                }
            }
            boolean typeOfClaimIsFlood = false;
            // Find Casenumber
            String curCaseNumber;
            for (String caseBlock : XMLHelper.extractBlocks(esbResponse, 'rsa:SalesforceCaseNumber'))
            {
                curCaseNumber = XMLHelper.retrieveValue(XMLHelper.stripNamespace(caseBlock, 'rsa'),CSIOXMLProcessor.CASE_NUMBER_PATH);
            }
            // Find insured client
            Case c = [Select ID, bkrCase_Insured_Client__c From Case Where CaseNumber = :curCaseNumber][0];
            String insuredClientId = c.bkrCase_Insured_Client__c;
            List<claim__c> claims = [Select Id From Claim__c Where type_of_claim__c = 'Flood' And Account__c=:insuredClientId];
            if (claims.size() > 0) {
                typeOfClaimIsFlood = true;
            }
            if (locationfz4Exists && typeOfClaimIsFlood) {
                return true;
            }
            return false;
        }
        public Boolean isTriggeredReferral(List<Risk__c> risks, Map<Id, Risk__c> oldRecords)
        {
            return false; 
        }
    }
          
    /*===================================================================
     * Utilities
     *==================================================================*/
    
    /**
     * Retrieve the aggregate sum of the answers of one question code.
     */
    @TestVisible
    private static Decimal getAnswerSum(SMEQ_QuoteRequest quoteRequest, String questionCode)
    {
        List<SMEQ_QuestionAnswerModel> questionAnswers = quoteRequestToQuestionAnswerModel(quoteRequest);
        
        List<String> answer = retrieveAnswers(questionAnswers, questionCode);
        
        Decimal answerTotal = 0;
        
        for (Decimal ans : toNumbersList(answer))
        {
            answerTotal += ans;
        }
        
        return answerTotal;
    }
    
    /**
     * Return all the value from the answers base on the questions
     */
    @TestVisible
    private static List<String> retrieveAnswers(List<SMEQ_QuestionAnswerModel> questionAnswermodels, String questionText)
    {
        List<String> answers = new List<String>();
         
        for (SMEQ_QuestionAnswerModel questionAnswer : questionAnswermodels)
        {
            if (questionAnswer.questionCode == questionText &&
                questionAnswer.answer != null)
            {
                answers.add(questionAnswer.answer);
            }
        }
        return answers;
    }
    
    /**
     * Convert a list of string values to double values.
     */
    @TestVisible
    private static List<Decimal> toNumbersList(List<String> answers)
    {
        List<Decimal> ans = new List<Decimal>();
        
        for (String an : answers)
        {
            if (an != null && an != '')
            {
                // Filter out numeric separators.
                ans.add(Decimal.valueOf(an.remove(',').remove(' ')));
            }
        }
        
        return ans;
    }
    
    /**
     * Retrieve all dynamic question-answer pairs from quote request.
     */
    @TestVisible
    private static List<SMEQ_QuestionAnswerModel> quoteRequestToQuestionAnswerModel(SMEQ_QuoteRequest quoteRequest)
    {
        List<SMEQ_QuestionAnswerModel> questionAnswermodels = new List<SMEQ_QuestionAnswerModel>();
        
        if (quoteRequest.businessDetails != null)
        {
            // Retrieve all quote level answers.
            if (quoteRequest.businessDetails.questionAnswers != null)
            {
                questionAnswermodels.addAll(quoteRequest.businessDetails.questionAnswers);
            }
            
            // Retrieve all risk level answers.
            if (quoteRequest.businessDetails.locationRisks != null)
            {
                for (SMEQ_LocationDetailsModel riskLocation : quoteRequest.businessDetails.locationRisks)
                {
                    if (riskLocation.questionAnswers != null)
                    {
                        questionAnswermodels.addAll(riskLocation.questionAnswers);
                    }
                }
            }
        }
        
        return questionAnswermodels;
    }
    
    /**
     * This function checks the 
     * @return: Segment code for the Sic Code Detail Version.
     */
    private static String getSegmentCode()
    {
        String segmentCode = '';

        Map<String, String> segmentCodesMap = SMEQ_CustomSettingSearchUtility.getLookupValuesForExternalServices('Segment__c');
        if (segmentCodesMap.get(ProcessReferralTriggerRules.sicCodeDetailVersion.Segment__c) != null)
        {
            segmentCode = segmentCodesMap.get(ProcessReferralTriggerRules.sicCodeDetailVersion.Segment__c);
        }
        
        return segmentCode;        
    }
}