/*
 * SMEQ_QuotePDF
 * Author: James Lee
 * Date: October 27 2016
 * 
 * The purpose of this controller is to provide the broker tool with a URL endpoint
 * for retrieving a PDF from ePolicy via the ESB asynchronously using the Continuation Pattern: 
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_continuation_overview.htm
 */
public with sharing class SMEQ_QuotePDF
{
    private static final Integer CONTINUATION_TIMEOUT = 120;
    private Quote__c quote;
    private RSA_ESBService service;
    // Make the variable requestLabel accessible from test methods
    @TestVisible private String requestLabel;
    private String request;
    
    /*
     * Retrieve the quote from the provided quote ID.
     */
    public SMEQ_QuotePDF()
    {
        String quoteId = Apexpages.currentpage().getparameters().get('quoteid');
        
        List<Quote__c> qs = [
            SELECT id, case__r.segment__c
            FROM Quote__c
            WHERE id = :quoteId
            LIMIT 1
        ];
        
        if (qs.size() == 0)
        {
            throw new QuotePDFException(Label.Invalid_quote_ID + quoteId + ')');
        }
        
        this.quote = qs[0];
    }
    
    /*
     * Performs the continuation call to retrieve the PDF from ePolicy.
     */
    public Object retrieveQuotePDF()
    {
        Continuation con = new Continuation(CONTINUATION_TIMEOUT);
        con.continuationMethod = 'processResponse';
        
        RequestInfo ri = new RequestInfo(
            RequestInfo.ServiceType.DOWNLOAD_QUOTE, 
            'Standard', 
            quote.case__r.segment__c
        );
        
        this.service = new RSA_ESBService(ri, quote.Id);
        this.request = service.getRequestXML();
        System.debug('DOWNLOAD PDF Request:'+ this.request);
        HttpRequest req = service.getHttpRequest(request);
        this.requestLabel = con.addHttpRequest(req);
        
        return con;
    }

    /*
     * Performs the continuation call to retrieve the PDF from ePolicy.
     */
    public Object retrieveBinderPDF()
    {
        Continuation con = new Continuation(CONTINUATION_TIMEOUT);
        con.continuationMethod = 'processResponse';
        
        RequestInfo ri = new RequestInfo(
            RequestInfo.ServiceType.DOWNLOAD_BINDER, 
            'Standard', 
            quote.case__r.segment__c
        );
        
        this.service = new RSA_ESBService(ri, quote.Id);
        this.request = service.getRequestXML();
        System.debug('DOWNLOAD PDF Request:'+ this.request);
        HttpRequest req = service.getHttpRequest(request);
        this.requestLabel = con.addHttpRequest(req);
        
        return con;
    }
    
    /*
     * Parses the response body from ePolicy.
     */
    public PageReference processResponse()
    {
        HttpResponse response = Continuation.getResponse(requestLabel);
        RSA_ESBService.insertNote(this.quote.Id, 'Generated Request', this.request);
        system.debug('response: ' + response);
        RSA_ESBService.insertNote(this.quote.Id, 'Generated Response', response.getBody());
        
        try
        {
            // Extracts the PDF attachment from the response and persists it into Salesforce.
            this.service.parseResponseXML(response.getBody());
        }
        catch(Exception e)
        {
            String caughtException = e.getMessage() + '\n' + e.getStackTraceString();
            RSA_ESBService.insertNote(this.quote.Id, 'Errors', caughtException);
        }
        
        return getPDFReference();
    }
    
    /*
     * Retrieve the reference to the attached PDF to the quote.
     */
    private PageReference getPDFReference()
    {
        List<Attachment> atts = [
            SELECT Id
            FROM Attachment
            WHERE ParentID = :quote.Id
            AND Name LIKE '%Effecti%' 
            ORDER BY CreatedDate DESC
            LIMIT 1
        ];
        
        if (atts.size() == 0)
        {
            throw new QuotePDFException(Label.QuoteParens + quote.Id + Label.in_declined_status_on_ePolicy_or_the_PDF_is_not_ready_for_download_yet);
        }
        
        String attachmentID = atts[0].Id;
        
        String redirectURL = '/servlet/servlet.FileDownload?file=' + attachmentID;
        PageReference pageRef = new PageReference(redirectURL);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public class QuotePDFException extends Exception
    {
        
    }
}