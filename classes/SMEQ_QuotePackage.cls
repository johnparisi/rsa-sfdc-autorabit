public class SMEQ_QuotePackage
{
    public String packageID {get; set;}
    public Integer annualPremium {get; set;}
    public Integer airMilesEarned {get; set;}
    public Integer cglLimit  {get; set;}
    public Integer propertyDeductible  {get; set;}
    //public Boolean deviation {get; set;} // Deprecated.
    public Boolean deviate {get; set;}
    public Integer newPremium {get; set;}
    public Integer proratedPremium {get; set;}
    
    // Stores the continuation ID when getting rate for multiple packages.
    public String continuationReferenceID {get; set;}
}