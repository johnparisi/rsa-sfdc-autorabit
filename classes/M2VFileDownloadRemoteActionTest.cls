@isTest
public class M2VFileDownloadRemoteActionTest {
    @isTest
    private static void testInitQuoteDownloadPDFData(){
        Map<String, Object> input = new Map<String,Object>();
        input.put('quoteNumber','123');
        input.put('quoteId','456');
        input.put('AgencyId','abc');
        
        Map<String, Object> output = new Map<String,Object>();
        Map<String, Object> options = new Map<String,Object>();
        
        M2VFileDownloadRemoteAction action = new M2VFileDownloadRemoteAction();
        action.invokeMethod('initQuoteDownloadPDFData', input, output, options);
        System.assert((String)output.get('CustPermId') == 'abc');       
        
    }
    
    @isTest
    private static void testGetQuotePDF(){
        Map<String, Object> input = new Map<String,Object>();
        Account a = new Account();
        a.name = 'test';
        insert a;
        input.put('quoteId',a.Id);
        String body = '</soap:Envelope>'
            + '--uuid:7d68d690-7c42-4818-8722-02fa023a1519\n'
            + 'Content-Type: application/pdf\n'
            + 'Content-Transfer-Encoding: binary\n'
            + 'Content-ID: \n'
            + '<Effective 21-01-2020 QPC0093888-01 04_42_39.335.pdf>\n'
            + 'fileName: Effective 21-01-2020 QPC0093888-01 04_42_39.335.pdf\n'
            + ' \n'
            + 'JVBERi0xLjQKJYCAgIANMSAwIG9iago8PAovVGl0bGUgPD4KL0F1d';
        
        input.put('httpResponse',body);
        Map<String, Object> output = new Map<String,Object>();
        Map<String, Object> options = new Map<String,Object>();
        
        M2VFileDownloadRemoteAction action = new M2VFileDownloadRemoteAction();
        action.invokeMethod('getQuotePDF', input, output, options);
        
        List<ContentVersion> cvs  = [select id,PathOnClient from ContentVersion];        
        System.assert(cvs.size() == 1, 'One file should be inserted');
        System.assert(cvs[0].PathOnClient == 'Effective 21-01-2020 QPC0093888-01 04_42_39.335.pdf', 'file name should be Effective 21-01-2020 QPC0093888-01 04_42_39.335.pdf');
        
    }
}