public with sharing class TaskTriggerHandler {
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public static boolean firstRun = true;
    
    public TaskTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        
    /*public void OnBeforeInsert(Task[] newTasks){
        //Example usage
        for(Task newTask : newTasks){

        }
    }
    
    public void OnAfterInsert(Task[] newTasks){


    }*/
    
    @future public static void OnAfterInsertAsync(Set<ID> newTaskIDs){
        //Example usage
        TaskTriggerHandler handler = new TaskTriggerHandler(null, null);
		handler.copyTaskToEmail(newTaskIDs);	           
    }
    
    /*public void OnBeforeUpdate(Task[] oldTasks, Task[] updatedTasks, Map<ID, Task> TaskMap){
        //Example usage
        for (integer i=0;i<oldTasks.size();i++){            
                updatedTasks[i].addError('');
        }           
    }
    
    public void OnAfterUpdate(Task[] oldTasks, Task[] updatedTasks, Map<ID, Task> TaskMap){
        
        if (firstRun) {
            firstRun = false;
        }
        else {
            System.debug('Already ran!');
            return;
        }       
        
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedTaskIDs){
        TaskTriggerHandler handler = new TaskTriggerHandler(null, null);
		handler.copyTaskToEmail(updatedTaskIDs);
    }
    
    public void OnBeforeDelete(Task[] TasksToDelete, Map<ID, Task> TaskMap){
        
    }
    
    public void OnAfterDelete(Task[] deletedTasks, Map<ID, Task> TaskMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedTaskIDs){
        
    }
    
    public void OnUndelete(Task[] restoredTasks){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }*/
    
    public void copyTaskToEmail(Set<ID> newTaskIDs){
        List<Task> newTasks = [SELECT Id, WhatId, Type, Subject, Description
        						 FROM Task 
        						 WHERE Id IN :newTaskIDs];
        Map<ID,EmailMessage> mapTaskIdEmailMessage = new Map<ID,EmailMessage>();
        Map<ID,Task> mapTaskIdTask = new Map<ID,Task>();
        string strWhatId;
        for(Task t : newTasks){
            strWhatId = t.WhatId;
            if(strWhatId != null 
            	&& strWhatId.substring(0,3) == '500' 
            	&& t.Type == 'Email'){ // is related to a Case
                mapTaskIdEmailMessage.put(t.Id,null);
                mapTaskIdTask.put(t.Id,t);
            }
        }   
        // See if a matching EmailMessage was already created   
        for(EmailMessage e : [Select Id, ActivityId from EmailMessage where ActivityId in: mapTaskIdEmailMessage.keyset()]){
            mapTaskIdEmailMessage.remove(e.ActivityId);
        }
        // Create a new Email Message
        EmailMessage em;

        for(Id actId : mapTaskIdEmailMessage.keyset()){
            em = new EmailMessage();
            em.ActivityId = actId;
            //em.FromAddress
            em.Incoming = true;
            //MessageDate
            em.ParentId = mapTaskIdTask.get(actId).WhatId;
            em.Status = '1';
            em.Subject = mapTaskIdTask.get(actId).Subject;
            em.TextBody = mapTaskIdTask.get(actId).Description;
            //em.ToAddress
            mapTaskIdEmailMessage.put(actId,em);
        }
        insert mapTaskIdEmailMessage.values();  

    }	    	
}