/*
* Risk__c Service Layer
* Contains processing service methods related to the Risk__c Object.
*/
public without sharing class CoverageService
{
    /*
     * This method will synchronize the limit and deductible values for extra coverages using the standard package values.
     * 
     * @param records A list of coverages to be updated.
     */
    public void synchroniseValuesFromStandardCoverages(List<Coverages__c> records)
    {
        List<Id> riskIds = new List<Id>();
        for (Coverages__c c : records)
        {
            riskIds.add(c.Risk__c);
        }
        
        Map<Id, Sic_Code_Detail_Version__c> riskToSCDVs = SMEQ_SicCodeService.getSicCodesInAppetiteForRisks(riskIds);
        
        Set<Id> keepRiskIds = new Set<Id>();
        
        // Get the risks which have newly inserted extra coverages.
        // Check if the coverage type is an add-on type.
        for (Coverages__c c : records)
        {
            if (riskToSCDVs.get(c.Risk__c) != null && riskToSCDVs.get(c.Risk__c).Coverage_Add_Ons__c != null)
            {
                if (c.Package_ID__c != RiskService.PACKAGE_STANDARD &&
                    riskToSCDVs.get(c.Risk__c).Coverage_Add_Ons__c.contains(c.Type__c))
                {
                    keepRiskIds.add(c.Risk__c);
                }
            }
        }
        
        // Get the standard version of the coverage.
        List<Coverages__c> standardPackageCoverages = [
            SELECT id, Risk__c, Limit_Value__c, Deductible_Value__c, Type__c
            FROM Coverages__c
            WHERE Risk__c IN :keepRiskIds
            AND Package_ID__c = :RiskService.PACKAGE_STANDARD
        ];
        
        // Search and update the limit and deductible for matching types.
        for (Coverages__c c : records)
        {
            for (Coverages__c cs : standardPackageCoverages)
            {
                if (c.Risk__c == cs.Risk__c && 
                    c.Type__c == cs.Type__c)
                {
                    c.Limit_Value__c = cs.Limit_Value__c;
                    c.Deductible_Value__c = cs.Deductible_Value__c;
                }
            }
        }
    }
    
    /*
     * This method will update the contents coverage if the equipment and/or stock coverages have been updated.
     */
    public void updateContentsCoverage(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords)
    {
        Set<Id> riskIds = new Set<Id>();
        
        // Only update if the limit has been updated.
        for (Coverages__c c : (List<Coverages__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Limit_Value__c', 'Deductible_Value__c'}))
        {
            // Filter for only equipment and stock coverages.
            if (c.Type__c == RiskService.COVERAGE_EQUIPMENT || c.Type__c == RiskService.COVERAGE_STOCK)
            {
                riskIds.add(c.risk__c);
            }
        }
        
        if (!riskIds.isEmpty())
        {
            Map<String, Decimal> contentLimits = new Map<String, Decimal>();
            Map<String, Decimal> contentDeductibles = new Map<String, Decimal>();
            
            List<Coverages__c> riskCoverages = [
                SELECT id, Limit_Value__c, Deductible_Value__c, Package_Id__c, Type__c, Risk__c
                FROM Coverages__c
                WHERE Risk__c IN :riskIds
                AND Type__c IN (
                    :RiskService.COVERAGE_EQUIPMENT,
                    :RiskService.COVERAGE_STOCK,
                    :RiskService.COVERAGE_CONTENTS
                )
            ];
            
            // Calculate the new value of contents.
            for (Coverages__c c : riskCoverages)
            {
                if (c.Type__c == RiskService.COVERAGE_EQUIPMENT || c.Type__c == RiskService.COVERAGE_STOCK)
                {
                    String key = c.Risk__c + c.Package_ID__c;
                    
                    Decimal contentLimit = contentLimits.get(key);
                    if (contentLimit == null)
                    {
                        contentLimit = 0;
                    }
                    contentLimit += c.Limit_Value__c != null ? c.Limit_Value__c : 0;
                    contentLimits.put(key, contentLimit);
                    
                    Decimal contentDeductible = contentDeductibles.get(key);
                    contentDeductible = c.Deductible_Value__c;
                    contentDeductibles.put(key, contentDeductible);
                }
            }
            
            // Update all content values.
            List<Coverages__c> contents = new List<Coverages__c>();
            for (Coverages__c c : riskCoverages)
            {
                if (c.Type__c == RiskService.COVERAGE_CONTENTS)
                {
                    c.Limit_Value__c = contentLimits.get(c.Risk__c + c.Package_ID__c);
                    c.Deductible_Value__c = contentDeductibles.get(c.Risk__c + c.Package_ID__c);
                    contents.add(c);
                }
            }
            
            if (!contents.isEmpty())
            {
                update contents;
            }
        }
        // RA-1611
        updateRiskBuildingValue(records, oldRecords);        
    }
    
    /*
     * This method will sync sic answers with coverage values for policy record types only
     */
    public void syncSicAnswers(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords) {
        
        // get risk id for record
        // get SA records for risk
        // if SA question code == stock || equipment
        // 		SA answer = coverage.stock or coverage.equipment
        
        List<Id> raIds = new List<Id>();
       	Map<Id, List<Sic_Answer__c>> risk2SA = new Map<Id, List<Sic_Answer__c>>();
        List<Coverages__c> filteredRecords = (List<Coverages__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Limit_Value__c'});
        
        for (Coverages__c coverage : filteredRecords)
        {
            if (coverage.Risk__c != null)
            {
                raIds.add(coverage.Risk__c);
                risk2SA.put(coverage.Risk__c, new List<Sic_Answer__c>());
            }
        }
        
        if (!raIds.isEmpty()) {
            List<Sic_Answer__c> updatedSas = [
                SELECT id, Risk__c, Value__c, Question_Code__r.QuestionId__c
                FROM Sic_Answer__c
                WHERE Risk__c IN :raIds
            ];
            
            for (Sic_Answer__c sa : updatedSas)
            {
                if (sa.Risk__c != null)
                {
                    List<Sic_Answer__c> sas = risk2SA.get(sa.Risk__c);
                    sas.add(sa);
                    risk2SA.put(sa.Risk__c, sas);
                }
            }
                        
            Map<String, String> dynamicQuestionCoverageType = SMEQ_CustomSettingSearchUtility.getCustomSettingsByFieldType('dynamicQuestionCoverage');
            if (!risk2SA.isEmpty())
            {                
                for (Coverages__c c : filteredRecords)
                {                    
                    for (Sic_Answer__c sa : risk2SA.get(c.Risk__c))
                    {
                        if (c.Type__c == dynamicQuestionCoverageType.get(String.valueOf(sa.Question_Code__r.QuestionId__c)) &&
                            (String.valueOf(sa.Question_Code__r.QuestionId__c) == SicQuestionService.CODE_STOCK
                            || String.valueOf(sa.Question_Code__r.QuestionId__c) == SicQuestionService.CODE_EQUIPMENT))
                        {
                            system.debug('###Limit_Value__c'+String.valueOf(c));
                            sa.Value__c = String.valueOf(c.Limit_Value__c);
                        }
                    }
                }
                update updatedSas;
            }
        }
        // RA-1611
        updateRiskBuildingValue(records, oldRecords);                    
    }
    
    /*
     * This helper method returns the default coverages associated with the case's SIC Code Detail Version.
     * 
     * @param caseId: A case's ID.
     * @return: The set of all default coverages for the case's SIC Code Detail Version.
     */
    public static Set<String> getDefaultCoverages(Id caseId)
    {
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(caseId);
        List<String> defaults = scdvs[0].Coverage_Defaults__c.split(';');
        return new Set<String>(defaults);
    }
    
    /*
     * This helper method returns the enabled coverages associated with the case's SIC Code Detail Version.
     * 
     * @param caseId: A case's ID.
     * @return: The set of all default + add-on coverages for the case's SIC Code Detail Version.
     */
    public static Set<String> getEnabledCoverages(Id caseId)
    {
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(caseId);
        Set<String> defaults = new Set<String>();
        defaults.addAll(scdvs[0].Coverage_Defaults__c.split(';'));
        defaults.addAll(scdvs[0].Coverage_Add_Ons__c.split(';'));
        return defaults;
    }
    
    /*
     * This helper method retrieves all default coverages based on the case's associated SIC Code Detail Version and returns the translated value.
     * 
     * @param caseId: A case's ID.
     * @return: The set of all labels for default coverages for the case's SIC Code Detail Version.
     */
    public static Set<String> getDefaultCoverageLabels(Id caseId)
    {
        Map<String, String> toLabel = getCoverageLabel();
        
        Set<String> coveragesFr = new Set<String>();
        for (String s : getDefaultCoverages(caseId)) // Get the English default coverages.
        {
            coveragesFr.add(toLabel.get(s));
        }
        
        return coveragesFr;
    }
    
    /*
     * This helper method returns the add on coverages associated with the case's SIC Code Detail Version.
     * 
     * @param caseId: A case's ID.
     * @return: The set of all add-on coverages for the case's SIC Code Detail Version.
     */
    public static Set<String> getAddOnCoverages(Id caseId)
    {
        List<SIC_Code_Detail_Version__c> scdvs = SMEQ_SicCodeService.getSicCodesInAppetiteForCase(caseId);
        
        if (scdvs.isEmpty()) // Case does not have an assigned SIC Code.
        {
            return new Set<String>();
        }
        
        List<String> addOns = scdvs[0].Coverage_Add_Ons__c.split(';');
        
        return new Set<String>(addOns);
    }
    
    /*
     * This helper method removes all non-location coverages from the list of coverages.
     */
    public static Set<String> removeNonLocationCoverages(Set<String> coverages)
    {
        Set<String> locationCoverages = new Set<String>(coverages);
        locationCoverages.remove(RiskService.COVERAGE_CGL);
        locationCoverages.remove(RiskService.COVERAGE_AGGREGATE);
        locationCoverages.remove(RiskService.COVERAGE_CYBER);
        
        Map<String, String> toLabel = getCoverageLabel();
        locationCoverages.remove(toLabel.get(RiskService.COVERAGE_CGL));
        locationCoverages.remove(toLabel.get(RiskService.COVERAGE_AGGREGATE));
        locationCoverages.remove(toLabel.get(RiskService.COVERAGE_CYBER));
        
        return locationCoverages;
    }
    
    /*
     * This helper method returns the value to label mapping for coverages. 
     * The labels will be translated based on the running user's language.
     */
    public static Map<String, String> getCoverageLabel()
    {
        Schema.DescribeFieldResult fieldResult = Risk__c.Coverage_Selected_Option_1__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Map<String, String> toLabel = new Map<String, String>();
        for (Schema.PicklistEntry f : ple)
        {
            toLabel.put(f.getValue(), f.getLabel());
        }
        
        return toLabel;
    }

    // RA-1611
    public void updateRiskBuildingValue(List<Coverages__c> records, Map<Id, Coverages__c> oldRecords) {
        Map<Id, Double> riskBuildValueMap = new Map<Id, Double>();
        Set<Id> riskBuildValueSet = new Set<Id>();
        for (Coverages__c c : (List<Coverages__c>) Utils.getChangedObjects(records, oldRecords, new List<String> {'Limit_Value__c'})) {
            if (c.Type__c == RiskService.COVERAGE_BUILDING) {
                riskBuildValueMap.put(c.Risk__c,c.Limit_Value__c);
            }
        }
        if (!riskBuildValueMap.isEmpty()) {
            List<Risk__c> risksToUpdate = new List<Risk__c>();
            Set<Id> riskIds = riskBuildValueMap.keySet();
            for (Risk__c r : [Select Id, Building_Value__c From Risk__c Where Id In :riskIds]) {
                r.Building_Value__c = riskBuildValueMap.get(r.ID);
                risksToUpdate.add(r);
            }
            Update risksToUpdate;
        }
    }
    
}