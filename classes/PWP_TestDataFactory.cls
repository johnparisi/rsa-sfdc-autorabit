@isTest
public class PWP_TestDataFactory {

    public static Profile getSystemAdminProfile(){
        Profile SysAdmin = [Select id from Profile where Name = 'System Administrator' limit 1];
        return SysAdmin;
    }

    public static User createAdminUser1(Profile SysAdmin){
        User adminUser1 = new User(
                firstname='First Name1',lastName='Last Name 1',email='adminuser1@gmail.com',Username='adminuser1@gmail.com.dev',Offering_Project__c='HUB; SPRNT',
                EmailEncodingKey = 'ISO-8859-1',Alias ='admin1',isActive = TRUE,TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',
                LanguageLocaleKey='en_US',ProfileId=SysAdmin.Id,Primary_Region__c='Ontario',Secondary_Regions__c='Atlantic; Ontario; Prairies; Pacific; Quebec');
        insert adminUser1;

        return adminUser1;
    }

    public static User createAdminUser2(Profile SysAdmin){
        User adminUser2 = new User(
                firstname='First Name2',lastName='Last Name 2',email='adminuser1@gmail.com',Username='adminuser2@gmail.com.dev',Offering_Project__c='HUB; SPRNT',
                EmailEncodingKey = 'ISO-8859-1',Alias ='admin2',isActive = TRUE,TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',
                LanguageLocaleKey='en_US',ProfileId=SysAdmin.Id,Primary_Region__c='Ontario',Secondary_Regions__c='Atlantic; Ontario; Prairies; Pacific; Quebec');
        insert adminUser2;
        return adminUser2;
    }

    public static List<Policy_Works_Code_Value_Mappings__c> createPWCustomSettingData1(){
        List<Policy_Works_Code_Value_Mappings__c> listCS = new List<Policy_Works_Code_Value_Mappings__c>();

        Policy_Works_Code_Value_Mappings__c cs1 = new Policy_Works_Code_Value_Mappings__c(Name = '1',Code__c = 'L',Field_Name__c = 'Type_of_Roof__c',Value__c = 'Steel');
        Policy_Works_Code_Value_Mappings__c cs2 = new Policy_Works_Code_Value_Mappings__c(Name = '2',Code__c = 'W',Field_Name__c = 'Type_of_Roof__c',Value__c = 'Wood');
        Policy_Works_Code_Value_Mappings__c cs3 = new Policy_Works_Code_Value_Mappings__c(Name = '3',Code__c = '2',Field_Name__c = 'Construction_Type__c',Value__c = 'Masonry Non-combustible');
        Policy_Works_Code_Value_Mappings__c cs4 = new Policy_Works_Code_Value_Mappings__c(Name = '4',Code__c = '6',Field_Name__c = 'Construction_Type__c',Value__c = 'Frame');
        Policy_Works_Code_Value_Mappings__c cs5 = new Policy_Works_Code_Value_Mappings__c(Name = '5',Code__c = '1',Field_Name__c = 'Type_of_Wall__c',Value__c = 'Fire resistive');
        Policy_Works_Code_Value_Mappings__c cs6 = new Policy_Works_Code_Value_Mappings__c(Name = '6',Code__c = '6',Field_Name__c = 'Type_of_Wall__c',Value__c = 'Frame and all other');
        Policy_Works_Code_Value_Mappings__c cs7 = new Policy_Works_Code_Value_Mappings__c(Name = '7',Code__c = 'Newfoundland/Labrador',Field_Name__c = 'Province__c',Value__c = 'Newfoundland and Labrador');

        listCS.add(cs1);
        listCS.add(cs2);
        listCS.add(cs3);
        listCS.add(cs4);
        listCS.add(cs5);
        listCS.add(cs6);
        listCS.add(cs7);
        if(listCS.size() > 0)
            insert listCS;

        return listCS;
    }

    public static List<Policy_Works_Content_Types__c> createPWCustomSettingData2(){
        List<Policy_Works_Content_Types__c> listCS = new List<Policy_Works_Content_Types__c>();

        Policy_Works_Content_Types__c cs1 = new Policy_Works_Content_Types__c(Name = '1',Extension__c = 'pdf',Type__c = 'application/pdf');
        Policy_Works_Content_Types__c cs2 = new Policy_Works_Content_Types__c(Name = '2',Extension__c = 'jpg',Type__c = 'image/jpeg');

        listCS.add(cs1);
        listCS.add(cs2);

        if(listCS.size()>0)
            insert listCS;
        return listCS;
    }

    public static List<PW_Exception_SIC_Codes__c> createPWCustomSettingData3(){
        List<PW_Exception_SIC_Codes__c> listCS = new List<PW_Exception_SIC_Codes__c>();

        PW_Exception_SIC_Codes__c cs1 = new PW_Exception_SIC_Codes__c(Name = '7932',Value__c = '7932A');
        PW_Exception_SIC_Codes__c cs2 = new PW_Exception_SIC_Codes__c(Name = '1001',Value__c = '1001A');

        listCS.add(cs1);
        listCS.add(cs2);

        if(listCS.size()>0)
            insert listCS;

        return listCS;
    }

    public static PWP_ESB_Service.CallData createPWPESBPayload(){

        List<Policy_Works_Code_Value_Mappings__c> listCS1 = PWP_TestDataFactory.createPWCustomSettingData1();
        List<Policy_Works_Content_Types__c> listCS2 = PWP_TestDataFactory.createPWCustomSettingData2();
        List<PW_Exception_SIC_Codes__c> listCS3 = PWP_TestDataFactory.createPWCustomSettingData3();

        PWP_ESB_Service.CallData pwpSamplePayloadData = new PWP_ESB_Service.CallData();
        pwpSamplePayloadData.source = 'PolicyWorks';
        pwpSamplePayloadData.processName = 'Policy Works Integration';
        pwpSamplePayloadData.transactionId = '1510829889808-[B@5ac8daaa';
        pwpSamplePayloadData.records = PWP_TestDataFactory.createPolicyWorksPayloadDataRecord();
        pwpSamplePayloadData.locationRiskList = PWP_TestDataFactory.createPolicyWorksPayloadLocationRiskRecord();
        pwpSamplePayloadData.attRecords = PWP_TestDataFactory.createPolicWorksPayloadAttachmentRecord();

        Product2 objProduct = PWP_TestDataFactory.createProduct();
        SIC_Code__c objSicCode = PWP_TestDataFactory.createSicCode();
        Contact objContact = PWP_TestDataFactory.createContact();
        Case objCase = PWP_TestDataFactory.createCase(objProduct,objSicCode,objContact);
        Quote__c objQuote = PWP_TestDataFactory.createQuote(objCase);
        List<SIC_Question__c> listSicQuestion = PWP_TestDataFactory.createSICQuestions();
        List<Risk__c> listLocationRisks = PWP_TestDataFactory.createLocationRisk(objQuote);
        List<SIC_Answer__c> listSicAnswers = PWP_TestDataFactory.createSICAnswers(objQuote,listLocationRisks,listSicQuestion);
        List<Coverages__c> listCoverages = PWP_TestDataFactory.createCoverages(listLocationRisks);
        Task objTask = PWP_TestDataFactory.createTask(objCase);
        EmailMessage objEmailMessage = PWP_TestDataFactory.createEmailMessage(objCase,objTask);

        return pwpSamplePayloadData;
    }

    public static PWP_ESB_Service.CallData createPWPPayloadDataForException(){

        List<Policy_Works_Code_Value_Mappings__c> listCS1 = PWP_TestDataFactory.createPWCustomSettingData1();
        List<Policy_Works_Content_Types__c> listCS2 = PWP_TestDataFactory.createPWCustomSettingData2();
        List<PW_Exception_SIC_Codes__c> listCS3 = PWP_TestDataFactory.createPWCustomSettingData3();

        PWP_ESB_Service.CallData pwpSamplePayloadData = new PWP_ESB_Service.CallData();
        pwpSamplePayloadData.source = 'PolicyWorks';
        pwpSamplePayloadData.processName = 'Policy Works Integration';
        pwpSamplePayloadData.transactionId = '1510829889808-[B@5ac8daaa';
        pwpSamplePayloadData.records = PWP_TestDataFactory.createPolicyWorksExceptionPayloadDataRecord();
        pwpSamplePayloadData.locationRiskList = PWP_TestDataFactory.createPolicyWorksPayloadLocationRiskRecord();
        pwpSamplePayloadData.attRecords = PWP_TestDataFactory.createPolicWorksPayloadAttachmentRecord();

        Product2 objProduct = PWP_TestDataFactory.createProduct();
        SIC_Code__c objSicCode = PWP_TestDataFactory.createSicCode();
        Contact objContact = PWP_TestDataFactory.createContact();
        Case objCase = PWP_TestDataFactory.createCase(objProduct,objSicCode,objContact);
        Quote__c objQuote = PWP_TestDataFactory.createQuote(objCase);
        List<SIC_Question__c> listSicQuestion = PWP_TestDataFactory.createSICQuestions();
        List<Risk__c> listLocationRisks = PWP_TestDataFactory.createLocationRisk(objQuote);
		List<SIC_Answer__c> listSicAnswers = PWP_TestDataFactory.createSICAnswers(objQuote,listLocationRisks,listSicQuestion);
        List<Coverages__c> listCoverages = PWP_TestDataFactory.createCoverages(listLocationRisks);
        Task objTask = PWP_TestDataFactory.createTask(objCase);
        EmailMessage objEmailMessage = PWP_TestDataFactory.createEmailMessage(objCase,objTask);

        return pwpSamplePayloadData;
    }

    public static List<PWP_ESB_Service.DataRecord> createPolicyWorksPayloadDataRecord(){
        List<PWP_ESB_Service.DataRecord> listRecords = new List<PWP_ESB_Service.DataRecord>();
        PWP_ESB_Service.DataRecord objDataRecord = new PWP_ESB_Service.DataRecord();
        objDataRecord.params = new List<PWP_ESB_Service.KeyValPair>();
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.bkrCase_Region__c', 'Ontario'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.ContactId', 'suman.haldar@rsagroup.ca'));
        //objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.PW_Business_Source_ID__c', 'PWEB'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.bkrCase_SIC_Code__c', '7932'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Quote__c.Business_Start_Date__c', '1985'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Quote__c.Effective_Date__c', '2017-12-12'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Quote__c.Canadian_Revenue__c', '200000.0'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.FromName', 'Suman Haldar(CAN)'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.ToAddress', 'pw2qa_qc@rsagroup.ca'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.FromAddress', 'suman.haldar@rsagroup.ca'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.TextBody', 'Sample Email Text Body'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.HtmlBody', '<h2>Sample Email Text Body</h2>'));
        listRecords.add(objDataRecord);
        return listRecords;
    }

    public static List<PWP_ESB_Service.DataRecord> createPolicyWorksExceptionPayloadDataRecord(){
        List<PWP_ESB_Service.DataRecord> listRecords = new List<PWP_ESB_Service.DataRecord>();
        PWP_ESB_Service.DataRecord objDataRecord = new PWP_ESB_Service.DataRecord();
        objDataRecord.params = new List<PWP_ESB_Service.KeyValPair>();
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.bkrCase_Region__c', 'Ontario'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.ContactId', 'suman.haldar@rsagroup.ca'));
        //objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.PW_Business_Source_ID__c', 'PWEB'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Case.bkrCase_SIC_Code__c', '7932'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Quote__c.Business_Start_Date__c', '1985'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Quote__c.Canadian_Revenue__c', '200000.0'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('Quote__c.Effective_Date__c', 'value'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.FromName', 'Suman Haldar(CAN)'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.ToAddress', 'pw2qa_qc@rsagroup.ca'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.FromAddress', 'suman.haldar@rsagroup.ca'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.TextBody', 'Sample Email Text Body'));
        objDataRecord.params.add(new PWP_ESB_Service.KeyValPair('EmailMessage.HtmlBody', '<h2>Sample Email Text Body</h2>'));
        listRecords.add(objDataRecord);
        return listRecords;
    }

    public static List<PWP_ESB_Service.RiskList> createPolicyWorksPayloadLocationRiskRecord(){
        List<PWP_ESB_Service.RiskRecord> listRiskRecord = new List<PWP_ESB_Service.RiskRecord>();

        PWP_ESB_Service.RiskRecord objRiskRecord1 = new PWP_ESB_Service.RiskRecord();
        objRiskRecord1.params = new List<PWP_ESB_Service.KeyValPair>();
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Address_Line_1__c', '5730 Des Epinettes Avenue'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.City__c', 'Toronto'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Construction_Type__c', '2'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Country__c', 'Canada'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Hydrant__c', '150'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Station__c', '5'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Electrical_Renovated_Year__c', '2011'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Heating_Renovated_Year__c', '2011'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Number_of_Stories__c', '2.0'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Plumbing_Renovated_Year__c', '2011'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Postal_Code__c', 'M4Y 2P7'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Province__c', 'Ontario'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Roof_Renovated_Year__c', '2011'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Sprinkler_Coverage__c', 'Yes'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Total_Occupied_Area__c', '1540'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Roof__c', 'L'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Wall__c', '6'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Year_Built__c', '1985'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Stock_Value__c', '10000.0'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Building_Value__c', '10000.0'));
        objRiskRecord1.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Equipment_Value__c', '10000.0'));
		/*
        PWP_ESB_Service.RiskRecord objRiskRecord2 = new PWP_ESB_Service.RiskRecord();
        objRiskRecord2.params = new List<PWP_ESB_Service.KeyValPair>();
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Address_Line_1__c', '5730 Des Epinettes Avenue'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.City__c', 'Toronto'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Construction_Type__c', '6'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Country__c', 'Canada'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Hydrant__c', '250'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Station__c', '7'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Electrical_Renovated_Year__c', '2011'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Heating_Renovated_Year__c', '2011'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Number_of_Stories__c', '2.0'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Plumbing_Renovated_Year__c', '2011'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Postal_Code__c', 'M4Y 2P7'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Province__c', 'Newfoundland/Labrador'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Roof_Renovated_Year__c', '2011'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Sprinkler_Coverage__c', 'Yes'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Total_Occupied_Area__c', '1540'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Roof__c', 'W'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Wall__c', '6'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Year_Built__c', '1985'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Stock_Value__c', '10000.0'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Building_Value__c', '10000.0'));
        objRiskRecord2.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Equipment_Value__c', '10000.0'));

        PWP_ESB_Service.RiskRecord objRiskRecord3 = new PWP_ESB_Service.RiskRecord();
        objRiskRecord3.params = new List<PWP_ESB_Service.KeyValPair>();
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Address_Line_1__c', '5730 Des Epinettes Avenue'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.City__c', 'Toronto'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Construction_Type__c', '6'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Country__c', 'Canada'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Hydrant__c', '350'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Station__c', '10'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Electrical_Renovated_Year__c', '2011'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Heating_Renovated_Year__c', '2011'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Number_of_Stories__c', '2.0'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Plumbing_Renovated_Year__c', '2011'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Postal_Code__c', 'M4Y 2P7'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Province__c', 'Newfoundland/Labrador'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Roof_Renovated_Year__c', '2011'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Sprinkler_Coverage__c', 'Yes'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Total_Occupied_Area__c', '1540'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Roof__c', 'W'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Wall__c', '6'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Year_Built__c', '1985'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Stock_Value__c', '10000.0'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Building_Value__c', '10000.0'));
        objRiskRecord3.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Equipment_Value__c', '10000.0'));

        PWP_ESB_Service.RiskRecord objRiskRecord4 = new PWP_ESB_Service.RiskRecord();
        objRiskRecord4.params = new List<PWP_ESB_Service.KeyValPair>();
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Address_Line_1__c', '5730 Des Epinettes Avenue'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.City__c', 'Toronto'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Construction_Type__c', '6'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Country__c', 'Canada'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Hydrant__c', '350'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Station__c', '13'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Electrical_Renovated_Year__c', '2011'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Heating_Renovated_Year__c', '2011'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Number_of_Stories__c', '2.0'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Plumbing_Renovated_Year__c', '2011'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Postal_Code__c', 'M4Y 2P7'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Province__c', 'Newfoundland/Labrador'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Roof_Renovated_Year__c', '2011'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Sprinkler_Coverage__c', 'Yes'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Total_Occupied_Area__c', '1540'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Roof__c', 'W'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Wall__c', '6'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Year_Built__c', '1985'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Stock_Value__c', '10000.0'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Building_Value__c', '10000.0'));
        objRiskRecord4.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Equipment_Value__c', '10000.0'));

        PWP_ESB_Service.RiskRecord objRiskRecord5 = new PWP_ESB_Service.RiskRecord();
        objRiskRecord5.params = new List<PWP_ESB_Service.KeyValPair>();
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Address_Line_1__c', '5730 Des Epinettes Avenue'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.City__c', 'Toronto'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Construction_Type__c', '6'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Country__c', 'Canada'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Hydrant__c', '350'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Distance_To_Fire_Station__c', '15'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Electrical_Renovated_Year__c', '2011'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Heating_Renovated_Year__c', '2011'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Number_of_Stories__c', '2.0'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Plumbing_Renovated_Year__c', '2011'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Postal_Code__c', 'M4Y 2P7'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Province__c', 'Newfoundland/Labrador'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Roof_Renovated_Year__c', '2011'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Sprinkler_Coverage__c', 'Yes'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Total_Occupied_Area__c', '1540'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Roof__c', 'W'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Type_of_Wall__c', '6'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Year_Built__c', '1985'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Stock_Value__c', '10000.0'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Building_Value__c', '10000.0'));
        objRiskRecord5.params.add(new PWP_ESB_Service.KeyValPair('Risk__c.Equipment_Value__c', '10000.0'));
		*/
        listRiskRecord.add(objRiskRecord1);
		/*
        listRiskRecord.add(objRiskRecord2);
        listRiskRecord.add(objRiskRecord3);
        listRiskRecord.add(objRiskRecord4);
        listRiskRecord.add(objRiskRecord5);
		*/
        List<PWP_ESB_Service.CoverageRecord> listCoverageRecords = new List<PWP_ESB_Service.CoverageRecord>();
        PWP_ESB_Service.CoverageRecord objCoverageRecord1 = new PWP_ESB_Service.CoverageRecord();
        objCoverageRecord1.params = new List<PWP_ESB_Service.KeyValPair>();
        objCoverageRecord1.params.add(new PWP_ESB_Service.KeyValPair('Coverages__c.Type__c', 'csio:Stock'));

        PWP_ESB_Service.CoverageRecord objCoverageRecord2 = new PWP_ESB_Service.CoverageRecord();
        objCoverageRecord2.params = new List<PWP_ESB_Service.KeyValPair>();
        objCoverageRecord2.params.add(new PWP_ESB_Service.KeyValPair('Coverages__c.Type__c', 'csio:Equipment'));

		PWP_ESB_Service.CoverageRecord objCoverageRecord3 = new PWP_ESB_Service.CoverageRecord();
		objCoverageRecord3.params = new List<PWP_ESB_Service.KeyValPair>();
		objCoverageRecord3.params.add(new PWP_ESB_Service.KeyValPair('Coverages__c.Type__c', 'csio:Building'));

        listCoverageRecords.add(objCoverageRecord1);
        listCoverageRecords.add(objCoverageRecord2);
		listCoverageRecords.add(objCoverageRecord3);

        List<PWP_ESB_Service.CoverageList> listCoverages = new List<PWP_ESB_Service.CoverageList>();
        PWP_ESB_Service.CoverageList objCoverageList = new PWP_ESB_Service.CoverageList();
        objCoverageList.coverageRecord = listCoverageRecords;
        listCoverages.add(objCoverageList);

        List<PWP_ESB_Service.RiskDetail> listRiskDetail = new List<PWP_ESB_Service.RiskDetail>();
        PWP_ESB_Service.RiskDetail objRiskDetail = new PWP_ESB_Service.RiskDetail();
        objRiskDetail.riskRecord = listRiskRecord;
        objRiskDetail.coverageList = listCoverages;
        listRiskDetail.add(objRiskDetail);

        List<PWP_ESB_Service.RiskList> listLocationRisks = new List<PWP_ESB_Service.RiskList>();
        PWP_ESB_Service.RiskList objRiskList = new PWP_ESB_Service.RiskList();
        objRiskList.riskDetail = listRiskDetail;
        listLocationRisks.add(objRiskList);

        return listLocationRisks;
    }

    public static List<PWP_ESB_Service.AttachmentRecords> createPolicWorksPayloadAttachmentRecord(){
        List<PWP_ESB_Service.AttachmentRecords> listAttachments = new List<PWP_ESB_Service.AttachmentRecords>();
        PWP_ESB_Service.AttachmentRecords objAttachmentRecord = new PWP_ESB_Service.AttachmentRecords();
        objAttachmentRecord.params = new List<PWP_ESB_Service.KeyValPair>();
        objAttachmentRecord.params.add(new PWP_ESB_Service.KeyValPair('SampleAttachment.pdf', Blob.valueOf('Sample PDF Document Content').toString()));
        listAttachments.add(objAttachmentRecord);

        return listAttachments;
    }

    public static Product2 createProduct(){
        Product2 objProduct = new Product2(
                ProductCode = 'CI_SME_EPOLICY',Name = 'Property & Casualty (ePolicy-based)',isActive = TRUE,
                Description = 'Property and casualty policies written within the ePolicy system and underwritten by commercial insurance.');

        insert objProduct; // = [SELECT Id FROM Product2 WHERE ProductCode = 'CI_SME_EPOLICY' LIMIT 1];
        return objProduct;
    }

    public static SIC_Code__c createSicCode(){
        SIC_Code_Detail__c objSicCodeDetail = new SIC_Code_Detail__c(Name = '7932A',SIC_Code__c = '7932A');
        insert objSicCodeDetail;

        SIC_Code__c objSicCode = new SIC_Code__c(Name = '7932A',SIC_Code__c = '7932A',SIC_Code_Detail__c = objSicCodeDetail.Id);
        insert objSicCode;

        Set<String> setOfferring = PWP_ESB_ServiceUtils.getPicklistValues('SIC_code_Detail_Version__c','Offering__c');
        String caseOfferring;

        for(String tmp: setOfferring){
            if(tmp == 'SPRNT v1')
                caseOfferring = tmp;
        }

        SIC_code_Detail_Version__c objSicCodeDetailversion = new SIC_code_Detail_Version__c(
                Region__c = 'Atlantic; Ontario; Pacific; Prairies; Quebec',Offering__c = caseOfferring,SIC_Code__c = objSicCode.Id,
                Coverage_Defaults__c = 'Commercial General Liability; Building; Equipment; Stock; Contents'
        );

        insert objSicCodeDetailversion;
        return objSicCode;
    }

    public static Contact createContact(){
        Account objAccount = new Account(Name = 'Test Account');
        insert objAccount;

        Contact objContact = new Contact(LastName = 'Test Last Name', AccountId = objAccount.Id, Email = 'suman.haldar@rsagroup.ca');
        insert objContact;
        return objContact;
    }

    public static Case createCase(Product2 objProduct, SIC_Code__c objSicCode, Contact objContact){
        Profile sysAdminProfile = PWP_TestDataFactory.getSystemAdminProfile();
        Case objCase = new Case(
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CI Open SPRNT').getRecordTypeId(),
                ProductId = objProduct.Id,ContactId = objContact.Id,Origin = 'pw_smenb@rsagroup.ca',Priority = NULL,
                OwnerId = PWP_TestDataFactory.createAdminUser2(sysAdminProfile).Id,Offering_Project__c = 'SPRNT v1',
                PW_Business_Source_ID__c = 'PWEB',bkrCase_Subm_Type__c = 'New Business',bkrCase_Region__c = 'Ontario',
                bkrCase_SIC_Code__c = objSicCode.Id
        );
        insert objCase;
        return objCase;
    }

    public static Quote__c createQuote(Case objCase){
        Quote__c objQuote = new Quote__c(
                RecordTypeId = Schema.SObjectType.Quote__c.getRecordTypeInfosByName().get('Open Quote').getRecordTypeId(),
                Case__c = objCase.Id,Quote_Date__c = System.Today(),Package_Type_Selected__c = 'Standard',
                Selected_Packages_with_Privacy_Breach__c = 'Standard;Option 1;Option 2',Status__c = 'New'
        );
        insert objQuote;
        return objQuote;
    }

	public static List<SIC_Question__c> createSICQuestions(){
		List<SIC_Question__c> listSicQuestions = new List<SIC_Question__c>();
		SIC_Question__c objQuoteSicQuestion = new SIC_Question__c();
		objQuoteSicQuestion.questionId__c = 90006;
		objQuoteSicQuestion.questionText_en__c = 'Enter the annual Canadian revenue';

		SIC_Question__c objRiskSicQuestion1 = new SIC_Question__c();
		objRiskSicQuestion1.questionId__c = 90001;
		objRiskSicQuestion1.questionText_en__c = 'Enter the total value of equipment';

		SIC_Question__c objRiskSicQuestion2 = new SIC_Question__c();
		objRiskSicQuestion2.questionId__c = 90002;
		objRiskSicQuestion2.questionText_en__c = 'Enter the total value of stock';

		listSicQuestions.add(objQuoteSicQuestion);
		listSicQuestions.add(objRiskSicQuestion1);
		listSicQuestions.add(objRiskSicQuestion2);
		insert listSicQuestions;
		return listSicQuestions;
	}

	public static List<SIC_Answer__c> createSICAnswers(Quote__c objQuote,List<Risk__c> listRisk,List<SIC_Question__c> listSicQuestion){
		List<SIC_Answer__c> listSicAnswer = new List<SIC_Answer__c>();
		for(SIC_Question__c tmp: listSicQuestion){
			if(tmp.questionId__c == 90006){
				SIC_Answer__c objQuoteSicAns = new SIC_Answer__c();
				objQuoteSicAns.Quote__c = objQuote.Id;
				objQuoteSicAns.Question_Code__c = tmp.Id;
				objQuoteSicAns.Value__c = '200000';
				listSicAnswer.add(objQuoteSicAns);
			}
			else if(tmp.questionId__c == 90001 || tmp.questionId__c == 90002){
				for(Risk__c tmpRisk: listRisk){
					SIC_Answer__c objRiskSicAns = new SIC_Answer__c();
					objRiskSicAns.Risk__c = tmpRisk.Id;
					objRiskSicAns.Question_Code__c = tmp.Id;
					objRiskSicAns.Value__c = '10000';
					listSicAnswer.add(objRiskSicAns);
				}
			}
		}
		insert listSicAnswer;
		return listSicAnswer;
	}

    public static List<Risk__c> createLocationRisk(Quote__c objQuote){
        List<Risk__c> listLocationRisks = new List<Risk__c>();

        Risk__c objRisk1 = new Risk__c(
                RecordTypeId = Schema.SObjectType.Risk__c.getRecordTypeInfosByName().get('Location').getRecordTypeId(),
                Quote__c = objQuote.Id,Address_Line_1__c = '5730 Des Epinettes Avenue', City__c = 'Toronto',Construction_Type__c = 'Masonry Non-combustible',
                Country__c = 'Canada',Distance_To_Fire_Hydrant__c = 'Within 150 Meters',Distance_To_Fire_Station__c = 'Within 5 KM',Equipment_Value__c = 10000.0,
                Electrical_Renovated_Year__c = 2011,Heating_Renovated_Year__c = 2011,Number_of_Stories__c = 2.0,Plumbing_Renovated_Year__c = 2011,
                Postal_Code__c = 'M4Y 2P7',Province__c = 'Ontario',Roof_Renovated_Year__c = 2011,Sprinkler_Coverage__c = TRUE,Total_Occupied_Area__c = 1540,
                Type_of_Roof__c = 'Steel',Type_of_Wall__c = 'Frame and all other',Year_Built__c = '1985',Stock_Value__c = 10000.0,Building_Value__c = 10000.0
        );
		/*
        Risk__c objRisk2 = new Risk__c(
                RecordTypeId = Schema.SObjectType.Risk__c.getRecordTypeInfosByName().get('Location').getRecordTypeId(),
                Quote__c = objQuote.Id,Address_Line_1__c = '33 Isabella Street',City__c = 'Toronto',Construction_Type__c = 'Frame',Roof_Renovated_Year__c = 2011,
                Country__c = 'Canada',Distance_To_Fire_Hydrant__c = 'Within 300 Meters',Distance_To_Fire_Station__c = 'Within 8 KM',Equipment_Value__c = 10000.0,
                Electrical_Renovated_Year__c = 2011,Heating_Renovated_Year__c = 2011,Number_of_Stories__c = 2.0,Plumbing_Renovated_Year__c = 2011,
                Postal_Code__c = 'M4Y 2P6',Province__c = 'Newfoundland and Labrador',Sprinkler_Coverage__c = TRUE,Total_Occupied_Area__c = 1540,
                Type_of_Roof__c = 'Wood',Type_of_Wall__c = 'Fire resistive',Year_Built__c = '1985',Stock_Value__c = 10000.0,Building_Value__c = 10000.0
        );

        Risk__c objRisk3 = new Risk__c(
                RecordTypeId = Schema.SObjectType.Risk__c.getRecordTypeInfosByName().get('Location').getRecordTypeId(),
                Quote__c = objQuote.Id,Address_Line_1__c = '33 Isabella Street',City__c = 'Toronto',Construction_Type__c = 'Frame',Roof_Renovated_Year__c = 2011,
                Country__c = 'Canada',Distance_To_Fire_Hydrant__c = 'Over 300 Meters',Distance_To_Fire_Station__c = 'Over 8 KM',Equipment_Value__c = 10000.0,
                Electrical_Renovated_Year__c = 2011,Heating_Renovated_Year__c = 2011,Number_of_Stories__c = 2.0,Plumbing_Renovated_Year__c = 2011,
                Postal_Code__c = 'M4Y 2P6',Province__c = 'Newfoundland and Labrador',Sprinkler_Coverage__c = TRUE,Total_Occupied_Area__c = 1540,
                Type_of_Roof__c = 'Wood',Type_of_Wall__c = 'Fire resistive',Year_Built__c = '1985',Stock_Value__c = 10000.0,Building_Value__c = 10000.0
        );

        Risk__c objRisk4 = new Risk__c(
                RecordTypeId = Schema.SObjectType.Risk__c.getRecordTypeInfosByName().get('Location').getRecordTypeId(),
                Quote__c = objQuote.Id,Address_Line_1__c = '33 Isabella Street',City__c = 'Toronto',Construction_Type__c = 'Frame',Roof_Renovated_Year__c = 2011,
                Country__c = 'Canada',Distance_To_Fire_Hydrant__c = 'Over 300 Meters',Distance_To_Fire_Station__c = 'Within 13 KM',Equipment_Value__c = 10000.0,
                Electrical_Renovated_Year__c = 2011,Heating_Renovated_Year__c = 2011,Number_of_Stories__c = 2.0,Plumbing_Renovated_Year__c = 2011,
                Postal_Code__c = 'M4Y 2P6',Province__c = 'Newfoundland and Labrador',Sprinkler_Coverage__c = TRUE,Total_Occupied_Area__c = 1540,
                Type_of_Roof__c = 'Wood',Type_of_Wall__c = 'Fire resistive',Year_Built__c = '1985',Stock_Value__c = 10000.0,Building_Value__c = 10000.0
        );

        Risk__c objRisk5 = new Risk__c(
                RecordTypeId = Schema.SObjectType.Risk__c.getRecordTypeInfosByName().get('Location').getRecordTypeId(),
                Quote__c = objQuote.Id,Address_Line_1__c = '33 Isabella Street',City__c = 'Toronto',Construction_Type__c = 'Frame',Roof_Renovated_Year__c = 2011,
                Country__c = 'Canada',Distance_To_Fire_Hydrant__c = 'Over 300 Meters',Distance_To_Fire_Station__c = 'Over 13 KM',Equipment_Value__c = 10000.0,
                Electrical_Renovated_Year__c = 2011,Heating_Renovated_Year__c = 2011,Number_of_Stories__c = 2.0,Plumbing_Renovated_Year__c = 2011,
                Postal_Code__c = 'M4Y 2P6',Province__c = 'Newfoundland and Labrador',Sprinkler_Coverage__c = TRUE,Total_Occupied_Area__c = 1540,
                Type_of_Roof__c = 'Wood',Type_of_Wall__c = 'Fire resistive',Year_Built__c = '1985',Stock_Value__c = 10000.0,Building_Value__c = 10000.0
        );
		*/
        listLocationRisks.add(objRisk1);
		/*
        listLocationRisks.add(objRisk2);
        listLocationRisks.add(objRisk3);
        listLocationRisks.add(objRisk4);
        listLocationRisks.add(objRisk5);
		*/
        insert listLocationRisks;
        return listLocationRisks;
    }

    public static List<Coverages__c> createCoverages(List<Risk__c> listLocationRisks){
        List<Coverages__c> listCoverages = new List<Coverages__c>();

        for(Risk__c tmp: listLocationRisks){
			Coverages__c objCoverage1 = new Coverages__c(Package_ID__c='Standard',Risk__c=tmp.Id,Selected__c=TRUE,Limit_Value__c=10000.0,Type__c='Building');
			Coverages__c objCoverage2 = new Coverages__c(Package_ID__c='Standard',Risk__c=tmp.Id,Selected__c=TRUE,Limit_Value__c=10000.0,Type__c='Stock');
			Coverages__c objCoverage3 = new Coverages__c(Package_ID__c='Standard',Risk__c=tmp.Id,Selected__c=TRUE,Limit_Value__c=10000.0,Type__c='Equipment');
            listCoverages.add(objCoverage1);
            listCoverages.add(objCoverage2);
            listCoverages.add(objCoverage3);
        }
        insert listCoverages;
        return listCoverages;
    }

    public static Task createTask(Case objCase){

        Profile sysAdminProfile = PWP_TestDataFactory.getSystemAdminProfile();
        User systemUser = new User(
                firstname = 'System First Name',lastName = 'System Last name',
                email = 'system@gmail.com',Username = 'system@gmail.com.dev',
                EmailEncodingKey = 'ISO-8859-1',Alias ='SYSTEM',isActive = TRUE,
                TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',ProfileId = sysAdminProfile .Id
        );

        insert systemUser;

        Task objTask = new Task(
                ActivityDate = System.Today(),bkrActivity_Original_Due_Date__c = System.Today(),
                Priority = 'Normal',Status = 'Completed',Subject = 'Email: '+objCase.Subject,
                WhatId = objCase.Id,WhoId = objCase.ContactId,OwnerId = systemUser.Id
        );
        insert objTask;
        return objTask;
    }

    public static EmailMessage createEmailMessage(Case objCase, Task objTask){
        EmailMessage objEmailMessage = new EmailMessage(
                ActivityId = objTask.Id,Incoming = TRUE,ParentId = objCase.Id,
                Status = String.valueOf(1),Subject = 'Email: '+objCase.Subject
        );
        insert objEmailMessage;
        return objEmailMessage;
    }
}