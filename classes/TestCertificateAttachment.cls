@isTest
public class TestCertificateAttachment {
    static testMethod void test() {
        Campaign camp = new Campaign(Name='Test');
        insert camp;
        
        Account a = new Account(Name='Test');
        insert a;
        
        Contact c = new Contact(LastName='A', AccountId=a.Id);
        insert c;
        
        CampaignMember mem = new CampaignMember(
            CampaignId=camp.Id,
            ContactId=c.Id
        );
        insert mem;
        
        CertificateAttachment comp = new CertificateAttachment();
        comp.relatedTo = camp;
        comp.recipient = c;
        camp = comp.campRec;
        c = comp.contactRec;
        comp.relatedTo = null;
        comp.recipient = null;
        try {
            camp = comp.campRec;
        } catch (exception e) {}
        
        try {
            c = comp.contactRec;
        } catch (exception e) {}
    }
}