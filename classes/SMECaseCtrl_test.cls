@isTest
private class SMECaseCtrl_test{
    private static testmethod void test()
    {
       User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
     System.runAs(uw)
        {
        SMEQ_TestDataGenerator.createTestDataForBrokerStage();
         Account a = SMEQ_TestDataGenerator.getInsuredAccount();
         // Create a Broker to Work at the Brokerage Account
        Contact brokerCntc = new Contact();
        brokerCntc.FirstName= 'John';
        brokerCntc.LastName='Smith';
        brokerCntc.AccountId=a.Id;
        insert brokerCntc;
        
        // Create a Case from the Broker Contact
          String recordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('CI Open SPRNT').getRecordTypeId();
        
        Case c = new Case();
        c.AccountId = a.Id;
        c.ContactId = brokerCntc.Id;
       // c.bkrCase_Insured_Client__c=clientAcc.id;
        c.Status='New';
        c.Origin='Email';
        c.Type='Request for a Quote';
        c.recordtypeId = recordTypeId;
        c.bkrCase_Subm_Type__c = 'Simple Amendment';
        c.COM__c = 'test';
        c.Segment__c = 'Construction, Erection, Installation';
        insert c;
        
        c.bkrCase_Insured_Client__c = a.Id;
            update c;
            
            
        Session_Information__c session = new Session_Information__c(session_id__c = '123',Case__c = c.id, User__c = UserInfo.getUserId());
        insert session;
        
         
           // SMEQ_TestDataGenerator.initializeExternalConnections();
           // SMEQ_TestDataGenerator.initializeSMEMappings();
            //Quote__c quote = SMEQ_TestDataGenerator.getOpenQuote(c);
             SMEQ_TestDataGenerator.generateFullTestQuoteV2();
           // quote.Package_Type_Selected__c = 'standrad';
           // update quote;
            
             
            
        
        SMECaseCtrl controller = new SMECaseCtrl(new ApexPages.StandardController(c));
         
              
              Continuation con = (Continuation)controller.getPolicy();
            
          
            quote__c quote1 = [SELECT id, Package_Type_Selected__c, Case__r.Segment__c,Year_Business_Started__c FROM Quote__c LIMIT 1]; 
            
            PageReference pageRef = new PageReference('/apex/SMEQ_GETPolicy?caseId ='+c.id);
            Test.setCurrentPage(pageRef);
            
             Map<String, HttpRequest> requests = con.getRequests(); 
            HttpResponse response = new HttpResponse();
            response.setBody('Mock PDF body');
            RequestInfo riDownloadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, quote1);
            RSA_ESBService service = new RSA_ESBService(riDownloadQuote, Quote1.Id);
            String request = service.getRequestXML();
            HttpRequest req = service.getHttpRequest(request);
            String requestLabel = con.addHttpRequest(req);
        //    system.debug('requestLabel: '+ requestLabel);
            Test.setContinuationResponse(requestLabel, response);
            //controller.processResponse();
            try{
                 controller.processResponse();
                 }catch(exception ex){}
        
        
   }
        
    }
         
  }