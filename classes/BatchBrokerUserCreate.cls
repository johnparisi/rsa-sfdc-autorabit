/**
* @File Name    :   BatchBrokerUserCreate [FP-4996]
* @Description  :   Batch Class for Broker User for community
* @Date Created :   08/08/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   Batch
* @Modification Log:
**************************************************************************************
* Ver       Date        Author          Modification
* 1.0       08/08/2017  Habiba Zaman  	Created the file/class
*/
global class BatchBrokerUserCreate implements Database.Batchable<sObject> {
	
	String query;
	
	global BatchBrokerUserCreate() {
		query =	'SELECT bkrAccount_HuonBrokerNumber__c,Broker_Account__c,'+
				' Broker_Contact__c,Broker_Username__c,ConnectionReceivedId, '+
				'ConnectionSentId,CreatedById,CreatedDate,Email_Address__c,'+
				'EP_Reference_Number__c,Error_Code__c,Federation_ID__c,First_Name__c,'+
				'Id,IsDeleted,Language__c,Last_Name__c,Locale__c,Name,OwnerId,Primary_Region__c,'+
				'Project_Offering__c,SystemModstamp,Timezone__c FROM User_Staging__c  ORDER BY Error_Code__c DESC NULLS FIRST LIMIT 100';
		

	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		//call brokerusercreate class for all related methods
   		if(scope.size() > 0){
			SMEQ_BrokerUserCreateService sbr  = new SMEQ_BrokerUserCreateService();
			sbr.init((List<User_Staging__c>)scope);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}