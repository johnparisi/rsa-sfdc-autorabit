/**

This class is the trigger service implementation for the big machines quote object.
@author Priyanka Sirohi : Modified the stagenames to Underwriting in lines 38 and 47
*/


public class BigMachinesService {

    /**
    This trigger implementation updates the opportunity stage based on the associated primary CPQ quote status for both
    mid-market new business and renewal opportunities. 
    @param opportunityList a list of opportunities
    */

    public void updateOpportunityStageBasedOnQuoteState(List<BigMachines__Quote__c> bmQuoteList) {
    
        // grab the opportunity record type for renewals
        Map<String,Id> OpportunityTypes = Utils.GetRecordTypeIdsByDeveloperName(Opportunity.SObjectType, true);
        Id opportunityTypeRenewals = OpportunityTypes.get('bkrMMGSL_Opportunity_Renewals');
        Id opportunityTypeNewBusiness = OpportunityTypes.get('bkrMMGSL_Opportunity');
    
        // first thing to do is to get all the quotes associated with opportunities that we are getting passed in
    
        Map<Id, BigMachines__Quote__c> bmQuoteMap = new Map<Id, BigMachines__Quote__c>();
        List<Id> opportunityIdList = new List<Id>();
    
        // build a quick map to make later processing more efficient
        for (BigMachines__Quote__c bmQuote : bmQuoteList) {
            if (bmQuote.BigMachines__Is_Primary__c) {
                //opportunityIdList.add(bmQuote.BigMachines__Opportunity__r.Id);
                opportunityIdList.add(bmQuote.BigMachines__Opportunity__c);
            }
        }
            
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id,RecordTypeId, StageName, bkrOpportunity_ReasonWonorLost__c, bkrOpportunity_WonOrLostCommentary__c,bkrMMGSL_StrategicSegment__c   FROM Opportunity WHERE Id in :opportunityIdList]);
    
        for (BigMachines__Quote__c bmQuote : bmQuoteList) {
            Opportunity opportunityToUpdate = opportunityMap.get(bmQuote.BigMachines__Opportunity__c);
            String bmStatus = bmQuote.BigMachines__Status__c;
            if(!String.isEmpty(bmStatus) && opportunityToUpdate != null){
                if (bmQuote.BigMachines__Status__c == 'Pending') { opportunityToUpdate.StageName = 'Underwriting'; }
                else if (bmQuote.BigMachines__Status__c == 'Quoted') { opportunityToUpdate.StageName = 'Quote Issued'; }
                else if (bmQuote.BigMachines__Status__c == 'Bound') 
                { opportunityToUpdate.StageName = 'Closed Won - Pre-Processing Work';
                if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Won';}
                 if (String.isEmpty(opportunityToUpdate.bkrMMGSL_StrategicSegment__c ) == true ) {
                     opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                else if (bmQuote.BigMachines__Status__c == 'Issued') 
                { opportunityToUpdate.StageName = 'Closed Won - Policy Issued';
                if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Won';}
                 if (opportunityToUpdate.bkrMMGSL_StrategicSegment__c == null ) {
                     opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                else if (bmQuote.BigMachines__Status__c == 'Unsuccessful'&& opportunityToUpdate.RecordTypeId == opportunityTypeNewBusiness)
                { opportunityToUpdate.StageName = 'Closed Lost'; 
                 if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Not Won';}
                 if (opportunityToUpdate.bkrMMGSL_StrategicSegment__c == null ) {
                    opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                else if (bmQuote.BigMachines__Status__c == 'Declined' && opportunityToUpdate.RecordTypeId == opportunityTypeNewBusiness) 
                { opportunityToUpdate.StageName = 'Closed Declined';
                if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Not Won';}
                 if (opportunityToUpdate.bkrMMGSL_StrategicSegment__c == null ) {
                     opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                else if (bmQuote.BigMachines__Status__c == 'Closed' && opportunityToUpdate.RecordTypeId == opportunityTypeNewBusiness) 
                { opportunityToUpdate.StageName = 'Closed Lost'; 
                 if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Not Won';}
                 if (opportunityToUpdate.bkrMMGSL_StrategicSegment__c == null ) {
                    opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                else if (bmQuote.BigMachines__Status__c == 'Lapsed' && opportunityToUpdate.RecordTypeId == opportunityTypeRenewals) 
                { opportunityToUpdate.StageName = 'Closed Lost'; 
                if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Not Won';}
                 if (opportunityToUpdate.bkrMMGSL_StrategicSegment__c == null ) {
                    opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                else if (bmQuote.BigMachines__Status__c == 'Cancelled' && opportunityToUpdate.RecordTypeId == opportunityTypeRenewals ) 
                { opportunityToUpdate.StageName = 'Closed Lost'; 
                 if (opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c == null) {
                     opportunityToUpdate.bkrOpportunity_ReasonWonorLost__c = 'Pricing';}
                 if (opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c == null) {
                     opportunityToUpdate.bkrOpportunity_WonOrLostCommentary__c ='Not Won';}
                 if (opportunityToUpdate.bkrMMGSL_StrategicSegment__c == null ) {
                    opportunityToUpdate.bkrMMGSL_StrategicSegment__c ='Business & Professional Services';}
                }
                
				//GRQ-2636 update premium on opportunity record from primary quote
                if(bmQuote.BigMachines__Is_Primary__c)
                {
                    opportunityToUpdate.bkrPremium__c = bmQuote.Premium__c;
                }
    
                update opportunityToUpdate;
                }
            }
        }
    

}