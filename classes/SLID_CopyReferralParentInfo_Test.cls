/**
  * @author        Saumil Bapat
  * @date          12/07/2016
  * @description   Test class for SLID_CopyReferralParentInfo
*/
@isTest
private Class SLID_CopyReferralParentInfo_Test {
    static testMethod void testCopyReferralParentInfo()
    {
        //Query the fields in the case referral info field set
        List<Schema.FieldSetMember> fieldSetMembers = SObjectType.Case.FieldSets.Referral_Case_Info.getFields();
  
        //Create User with LanId
        User adminUser = SLID_TestDataFactory.adminUser;
        adminUser.userName = 'TestAdmin1@Testing.com';
        adminUser.alias = 'Test';
        insert adminUser;
        System.Debug('~~~adminUser: ' + adminUser);

        List<Case> newRecords = new List<Case>();

        test.startTest();
          System.runAs(adminUser)
          {
            //Create testProduct
            Product2 testProduct = SLID_TestDataFactory.returnTestProduct();
            insert testProduct;
            System.Debug('~~~testProduct: ' + testProduct);

            //Create a test Account
            Account testAccount = SLID_TestDataFactory.returnTestAccount();
            insert testAccount;
            System.Debug('~~~testAccount: ' + testAccount);

            //Create a test Contact
            Contact testContact = SLID_TestDataFactory.returnTestContact();
            testContact.AccountId = testAccount.Id;
            insert testContact;
            System.Debug('~~~testContact: ' + testContact);

            //Create a test parent Case
            Case parentCase = SLID_TestDataFactory.returnTestNewBusinessCase();
            parentCase.AccountId = testAccount.Id;
            parentCase.ContactId = testContact.Id;
            parentCase.ProductId = testProduct.Id;
            parentCase.bkrCase_Region__c = 'Ontario';
            insert parentCase;

            //Query the parent Case
            String parentCaseId = parentCase.Id;
            String parentCaseQuery = 'Select ';
            for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
            {
              parentCaseQuery += fieldSetMember.getFieldPath() + ',';
            }
            parentCaseQuery = parentCaseQuery.SubString(0, parentCaseQuery.length() - 1);
            parentCaseQuery += ' from Case where Id =:parentCaseId';
            List<SObject> queriedRecords = Database.Query(parentCaseQuery);
            parentCase = (Case) queriedRecords[0];
            System.Debug('~~~parentCase: ' + parentCase);

            //Create child Referral Case with LanId
            Case referralCase = SLID_TestDataFactory.returnTestReferralCase();
            referralCase.Parent_Case__c = parentCase.Id;
            insert referralCase;

            String referralCaseId = referralCase.Id;
            String referralCaseQuery = 'Select ';
            for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
            {
              referralCaseQuery += fieldSetMember.getFieldPath() + ',';
            }
            referralCaseQuery = referralCaseQuery.SubString(0, referralCaseQuery.length() - 1);
            referralCaseQuery += ' from Case where Id =:referralCaseId';
            queriedRecords = Database.Query(referralCaseQuery);
            referralCase = (Case) queriedRecords[0];
            System.Debug('~~~referralCase: ' + referralCase);

            for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
            {

              String fieldPath = fieldSetMember.getFieldPath();
              System.Debug('~~~fieldPath: ' + fieldPath);
              //if (fieldPath != 'Subject')
              //System.Assert(referralCase.get(fieldPath) == parentCase.get(fieldPath), '~~~' + fieldPath + ' not set properly');
            }
            newRecords.add(referralCase);
          }
          SLID_CopyReferralParentInfo.CopyReferralParentInfo(newRecords);
        test.stopTest();
    }
}