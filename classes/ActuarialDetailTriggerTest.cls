/**
* @File Name    :   ActuarialDetailTriggerTest
* @Description  :   Test class for ActuarialDetailTrigger
* @Date Created :   12/12/2017
* @Author       :   Habiba Zaman @ Deloitte
* @group        :   testing class
* @Modification Log:
*****************************************************************
* Ver       Date        Author              Modification
* 1.0       12/12/2017  Habiba Zaman        Created the file/class
**/
@isTest
private class ActuarialDetailTriggerTest {
    

    // Test for inserting AD data 
    @isTest
    static void TestAfterInsert()
    {
        //custom setting
        
        // Create a Sample Client Account
        PolicyService pservice = new PolicyService();
        Account clientAcc = new Account();
        clientAcc.RecordTypeId='012o0000000x3vq';
        clientAcc.Name= 'My Sample Client Co.';
        clientAcc.AccountNumber= '0202020';
        clientAcc.BillingStreet= '18 York Street';
        clientAcc.BillingCity= 'Toronto';
        clientAcc.BillingState= 'ON';
        clientAcc.BillingPostalCode= 'M5J 2T8';
        clientAcc.BillingCountry='Canada';
        clientAcc.bkrAccount_DUNS__c='255293813';
        insert clientAcc;
        
        // Create a New Brokerage Account
        Account brokerageAcc = new Account();
        brokerageAcc.Name= 'My Brokerage Co.';
        brokerageAcc.RecordTypeId='012o0000000Ag6G';
        brokerageAcc.AccountNumber= '001';
        brokerageAcc.BillingStreet= '18 York Street';
        brokerageAcc.BillingCity= 'Toronto';
        brokerageAcc.BillingState= 'ON';
        brokerageAcc.BillingPostalCode= 'M5J 2T8';
        brokerageAcc.BillingCountry='Canada';
        brokerageAcc.bkrAccount_HuonBrokerNumber__c='001231242';
        insert brokerageAcc;

        //Create new Underwriter
        Profile userProfile = [SELECT Id FROM Profile WHERE Name='RSA Standard User [Regional Mid-Market & GSL, Policy-Level]'];
        User u = new User(Alias='test1', Email='test1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', FirstName='Test',
                          LocaleSidKey='en_US', ProfileId = userProfile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser1g4egas4qgj6kl8r@testorg1.com');
    
        insert u;

        //Policy to Client Mapping

        Policy_to_Client_Mapping__c pcm = new Policy_to_Client_Mapping__c();
        pcm.Policy_number__c = '000123123';
        pcm.Client_Account__c = clientAcc.Id;
        insert pcm;


        // Policy Data
        Map<String,Id> PolicyTypes = Utils.GetRecordTypeIdsByDeveloperName(Policy__c.SObjectType, true);
        Set<Id> policyRecordTypes = new Set<Id>();


        Policy__c policy = new Policy__c();
        policy.Name ='000000023';
        policy.Client_Name__c = clientAcc.Name;
        policy.Master_Broker__c = brokerageAcc.bkrAccount_HuonBrokerNumber__c;
        policy.Strategic_Segment__c = 'Contracting';
        policy.Region_Group__c = 'GRS';
        policy.Effective_Date__c=  Date.today().addDays(-30);
        policy.Term_Expiry_Date__c = Date.today().addDays(1);
        //policy.Product_Code__c='COM';
        policy.RecordTypeId = PolicyTypes.get('bkrMMGSL_Policy_Renewals');
        policy.bkrPolicy_Premium__c = 123.23;
        policy.Product__c = 'Property/Casualty';
        policy.Policy_Underwriter__c = u.LastName+' '+u.FirstName;
        policy.Control_Field__c = pservice.createPolicyControlField(policy.Name, policy.Effective_Date__c);
        insert policy;
        
        Actuarial_Detail__c ad = new Actuarial_Detail__c();
        ad.Policy_Number__c = policy.Id;
        ad.Effective_Date__c = policy.Effective_Date__c;
        ad.Expiry_Date__c = policy.Term_Expiry_Date__c;
        ad.Decile__c = 4.0;
        ad.LLP__c = 3.0;
        ad.Building_and_Contents_Charged_Premium__c = 8714.00;
        ad.Control_Field__c = policy.Control_Field__c;
        
        Test.startTest(); 
        insert ad;
        
        
        ad.LLP__c = 5.0;
        update ad;

        PolicyService ps = new PolicyService();
       
        Policy__c newPolicy = [Select Policy_Actuarial_Detail__c,Policy_Stage__c,Qualified_Renewal__c from Policy__c where Id =:policy.Id];
        System.assertEquals(ad.Id, newPolicy.Policy_Actuarial_Detail__c);

        Test.stopTest();

    }
}