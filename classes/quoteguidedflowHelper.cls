global class quoteguidedflowHelper implements vlocity_ins.VlocityOpenInterface {
    
    global quoteguidedflowHelper() {}
    
    global Boolean invokeMethod(String methodName, Map < String, Object > inputMap, Map < String, Object > outMap, Map < String, Object > options) {
        Boolean result = true;
        try {
            if (methodName.equals('getDnB')) {
                getDnB(inputMap, outMap);
            }
            else {
                result = false;
            }
        }catch (Exception e) {
            System.debug('exception: ' + e + ' line no' + e.getLineNumber());
            result = false;
        }
        return result;
    }
    
    public void getDnB(Map<String,Object> inputMap, Map<String,Object> outMap) {
        //system.debug('inputMap::'+inputMap);
        Map<String,Object> applicationResults = (Map<String,Object>) inputMap.get('Application');
        system.debug('applicationResults::'+applicationResults);
        Map<String,Object> ClientInfoResults = (Map<String,Object>) applicationResults.get('ClientInfo');
        system.debug('ClientInfoResults::'+ClientInfoResults);
        String city = String.valueOf(ClientInfoResults.get('City'));
        String PostalCode = String.valueOf(ClientInfoResults.get('PostalCode'));
        String Province = String.valueOf(ClientInfoResults.get('Province'));
        String RegisteredBusinessAddress = String.valueOf(ClientInfoResults.get('RegisteredBusinessAddress'));
        String RegisteredBusinessName = String.valueOf(ClientInfoResults.get('RegisteredBusinessName'));
        String UnitNumber = String.valueOf(ClientInfoResults.get('UnitNumber'));
        
        /* DnB Search (String name, String streetAddress, String city, String state, String postalCode
        , String phoneNumber, String countryCode)*/
        SMEQ_DnBCallOut calloutMethod = new SMEQ_DnBCallOut ();
        DNBMatch dnbResults = calloutMethod.getCompanyMatch(RegisteredBusinessName,RegisteredBusinessAddress,city,Province,PostalCode,'',Province);
        system.debug('dnbResults::'+dnbResults);
        //Map<String,Object> resultMap = new Map<String,Object>();
        //resultMap.put('dnbResults',(Object)dnbResults);
        outMap.put('dnbResults',dnbResults);
    }
}