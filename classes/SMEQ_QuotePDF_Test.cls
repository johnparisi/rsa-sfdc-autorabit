@isTest
private class SMEQ_QuotePDF_Test
{
    static testMethod void testQuotePDF()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c testQuote = [
                SELECT id, Package_Type_Selected__c, Case__c,
                Case__r.bkrCase_SIC_Code__r.SIC_Code_Detail__r.Segment__c,
                Case__r.Segment__c
                FROM Quote__c
                LIMIT 1
            ];
            testQuote.Quote_Number__c = 'QPC0000000-01';
            update testQuote;
            Attachment att = new Attachment(ParentId=testQuote.Id, Name='Effective', Body=Blob.valueOf('abc'));
            insert att;
            
            PageReference pageRef = new PageReference('/apex/SMEQ_QuotePDF?quoteid='+testQuote.id);
            Test.setCurrentPage(pageRef);
            SMEQ_QuotePDF quotePDF = new SMEQ_QuotePDF();
            SME_Mapping__c smeMapEntry = new SME_Mapping__c(name = 'esbFlavor01', field__c = 'esbFlavor', SFKey__c = 'Standard', externalValue__c = 'FL1');
            insert smeMapEntry;
            Continuation conti = (Continuation)quotePDF.retrieveQuotePDF();
            Map<String, HttpRequest> requests = conti.getRequests();
            system.assert(requests.size() == 1);
            
            HttpResponse response = new HttpResponse();
            response.setBody('Mock PDF body');
            RequestInfo riDownloadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, testQuote);
            RSA_ESBService service = new RSA_ESBService(riDownloadQuote, testQuote.Id);
            String request = service.getRequestXML();
            HttpRequest req = service.getHttpRequest(request);
            quotePDF.requestLabel = conti.addHttpRequest(req);
            Test.setContinuationResponse(quotePDF.requestLabel, response);
            Object result = Test.invokeContinuationMethod(quotePDF, conti);
        }
    }
    
    static testMethod void testBinderPDF()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            SMEQ_TestDataGenerator.createTestDataForBrokerStage();
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c testQuote = [
                SELECT id, Package_Type_Selected__c, Case__c,
                Case__r.bkrCase_SIC_Code__r.SIC_Code_Detail__r.Segment__c,
                Case__r.Segment__c
                FROM Quote__c
                LIMIT 1
            ];
            testQuote.ePolicy__c = 'COM0000000-01';
            testQuote.Quote_Number__c = 'QPC0000000-01';
            
            update testQuote;
            
            PageReference pageRef = new PageReference('/apex/SMEQ_QuotePDF?quoteid='+testQuote.id);
            Test.setCurrentPage(pageRef);
            SMEQ_QuotePDF quotePDF = new SMEQ_QuotePDF();
            SME_Mapping__c smeMapEntry = new SME_Mapping__c(name = 'esbFlavor01', field__c = 'esbFlavor', SFKey__c = 'Standard', externalValue__c = 'FL1');
            insert smeMapEntry;
            Continuation conti = (Continuation)quotePDF.retrieveBinderPDF();
            Map<String, HttpRequest> requests = conti.getRequests();
            system.assert(requests.size() == 1);
            
            HttpResponse response = new HttpResponse();
            response.setBody('Mock PDF body');
            RequestInfo riDownloadQuote = SMEQ_TestDataGenerator.getRequestInfo(RequestInfo.ServiceType.DOWNLOAD_QUOTE, testQuote);
            RSA_ESBService service = new RSA_ESBService(riDownloadQuote, testQuote.Id);
            String request = service.getRequestXML();
            HttpRequest req = service.getHttpRequest(request);
            String requestLabel = conti.addHttpRequest(req);
            system.debug('requestLabel: '+ requestLabel);
            Test.setContinuationResponse(requestLabel, response);
            //Object result = Test.invokeContinuationMethod(quotePDF, conti);
        }
    }
}