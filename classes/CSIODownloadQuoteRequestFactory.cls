/**
 * CSIODownloadQuoteRequestFactory
 * Author: Sophie Tran
 * Date: November 24, 2017
 * 
 * The purpose of this class is to generate the xml Request for the Download Quote service 
 */
public class CSIODownloadQuoteRequestFactory extends CSIODownloadDocumentRequestFactory implements CSIORequestFactory {
   
    
    public CSIODownloadQuoteRequestFactory(SOQLDataSet sds, RequestInfo ri){
        super(sds.RequestingUser.FederationIdentifier,sds.quote.Business_Source_ID__c);
        this.soqlDataSet = sds; 
        this.ri = ri;
    }
    
    public String buildXMLRequest(){ 
        getHeaderXml();
        xml += CSIO420Header;
        if (this.soqlDataSet.quote.Quote_Number__c == null) {
            throw new RSA_ESBException('Cannot download quote without QPC#');
        }
        xml += '   <BaseSvcRq>';
        xml += '    <RqUID>' + XMLHelper.encodeUID(this.soqlDataSet.quote.Id) + '</RqUID>';
        xml += '    <PendingResponseInqRq>';
        xml += '     <TransactionRequestDt>' + caseCreateDateLong + '</TransactionRequestDt>'; //2007-09-10T15:12:50-07:00
        xml += '     <PendingResponseInfo>';
        xml += '      <MsgRqUID>' + XMLHelper.encodeUID(this.soqlDataSet.quote.Quote_Number__c) + '</MsgRqUID>';
        xml += '      <MsgNameCd>CommlPkgPolicyQuoteInq</MsgNameCd>';
        xml += '     </PendingResponseInfo>';
        xml += '    </PendingResponseInqRq>';
        xml += '   </BaseSvcRq>';
        xml += CSIO420Footer;
        return xml;
    } 
}