public class SLID_ESB_ServiceUtils
{
    //Set the inbound integration LogRecordType Id
    public static String inboundRecordTypeId
    {
        get
        {
            if(SLID_ESB_ServiceUtils.inboundRecordTypeId == null) //if the inbound ID is not already set
            {
                for(RecordType rt : [Select SobjectType, Name, IsActive, DeveloperName From RecordType where SobjectType = 'Integration_Log__c'])
                {
                    if(rt.DeveloperName == SLID_ESB_ServiceConstants.INBOUND_STRING)
                    {
                        SLID_ESB_ServiceUtils.inboundRecordTypeId = rt.Id;
                        break;
                    }
                }
            }
            return SLID_ESB_ServiceUtils.inboundRecordTypeId;
        }
        set;
    }

    public static SLID_Mapping_ServiceProcess__c getServiceProcessByNameAndSource(String processName , String sourceSystem)
    {
        List<SLID_Mapping_ServiceProcess__c> serviceProcesses; //List of service processes

        try
        {
            serviceProcesses = [Select Name, Active__c, All_or_None__c, Source__c, Process_Description__c, (Select id , Target_Fields_Count__c from TargetObjects__r)
            From SLID_Mapping_ServiceProcess__c
            Where Name =: processName
            and Active__c = true
            And Source__c includes (:sourceSystem)];
        }
        catch(Exception e)
        {
            //Create a custom exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ServiceUtils','queryServiceProcessByNameAndSource','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
        }

        //Check if the service process is valid or throw a error
        SLID_ESB_ServiceUtils.isValidServiceProcess(serviceProcesses);

        //return the service process
        return serviceProcesses[0];
    }

    //Check if the service process is valid. Throw a custom error if not.
    public static Boolean isValidServiceProcess(List<SLID_Mapping_ServiceProcess__c> serviceProcesses)
    {
        try
        {
            //Instantiate a exception variable
            Exception ex;

            //This method is to validate the mapping structure is defined.
            List<SLID_Mapping_TargetObject__c> lstTargetObject;

            //Check whether a service process is defined
            if (serviceProcesses == null || serviceProcesses.size() == 0)
            {
                //throw an error if the return recordset is null
                ex = UTIL_Logging.createMappingException(SLID_ESB_ServiceConstants.NO_MAPPING_FOR_SERVICE_PROCESS_MSG);
                throw ex;
            }

            //Throw a error if the recordset is greater than 1
            if (serviceProcesses.size() > 1)
            {
                //throw an error if the return recordset is null or greater than 1
                ex = UTIL_Logging.createMappingException(SLID_ESB_ServiceConstants.NO_UNIQUE_MAPPING_FOR_SERVICE_PROCESS_MSG);
                throw ex;
            }

            //if One Service record is returned, check whether there are any records for Target object and and Target field
            if(serviceProcesses != null && serviceProcesses.size() == 1)
            {
                lstTargetObject = serviceProcesses[0].TargetObjects__r;
            }

            if (lstTargetObject == null || lstTargetObject.size() == 0)
            {
                ex = UTIL_Logging.createMappingException(SLID_ESB_ServiceConstants.NO_TARGET_OBJECT_FOR_SERVICE_PROCESS_MSG);
                throw ex;
            }

            //Check how many target fields associated to the Target obejct
            else if (lstTargetObject != null && lstTargetObject.size() > 0)
            {
                for(SLID_Mapping_TargetObject__c tarObj : lstTargetObject)
                {
                    if(tarObj.Target_Fields_Count__c == 0)
                    {
                        ex = UTIL_Logging.createMappingException(SLID_ESB_ServiceConstants.NO_TARGET_FIELD_FOR_TARGET_OBJECT_MSG);
                        throw ex;
                    }
                }
            }
            return true;
        }
        catch(Exception e)
        {
            //Create a custom exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ServiceUtils','isValidServiceProcess','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
            return false;
        }
        return false;
    }


    //This class will handle the logic for creating and inserting the object records from the ESB Service
    public Class ObjectHandler {

        //Service Process for this handler
        SLID_Mapping_ServiceProcess__c serviceProcess;

        //Target Objects for this handler
        SLID_Mapping_TargetObject__c[] targetObjects;

        //Map of target object Ids to target objects
        Map<Id, SLID_Mapping_TargetObject__c> targetObjectsMap = new Map<Id, SLID_Mapping_TargetObject__c>();

        //Map of key value pairs fo rthe target fields format <"Key" + "_" + "Mapping Type">:<Value>
        Map<String,String> keyValueMap = new Map<String,String>();

        //Set data from ESB
        SLID_ESB_Service.CallData dataFromESB;

        //current object
        SObject currentObject;

        //Map of targetObjectId ->List of records of that type to upsert (eg. CaseTargetObject.Id -> List<Case> caseRecords)
        Map<Id, List<SObject>> recordsToUpsert = new Map<Id, List<SObject>>();

        //Map of target objects to upsertResults
        Map<ID,list<Database.UpsertResult>> targetObjectUpsertResultsMap;

        //Maintain service response
        SLID_ESB_Service.ServiceResponse response;

        //Constructor
        public ObjectHandler(SLID_Mapping_ServiceProcess__c serviceProcess, SLID_ESB_Service.CallData dataFromESB)
        {
            //response state
            this.serviceProcess = serviceProcess;
            this.dataFromESB = dataFromESB;
            SLID_ESB_ObjectUtils.allOrNone = serviceProcess.All_Or_None__c;

            //Query the target objects for this service process and initialize map: targetObjectsMap & List: targetObjects
            this.queryTargetObjectsByServiceProcess();
            this.instantiateKeyValueMap();
        }

        //Query the target objects for this service process
        private void queryTargetObjectsByServiceProcess()
        {
            try
            {
                //Query the target objects
                this.targetObjectsMap = new Map<Id, SLID_Mapping_TargetObject__c>([select Id, External_Id__c, Target_Object_API_Name__c,
                (Select Id, Mapping_Type__c, Default_Value__c,RecordType.Name,
                        Source_fields__c, Target_Field_API_Name__c, Target_Object__c,
                        Conversion_Type__c, External_Id_for_Target_Field__c
                from SLID_Mapping_TargetField__r)
                from SLID_Mapping_TargetObject__c
                where ServiceProcess__c = :this.serviceProcess.id]);

                //Also initialize the list of target objects
                this.targetObjects = [select Id, External_Id__c, Target_Object_API_Name__c, Priority__c,
                (Select Id, Mapping_Type__c, Default_Value__c, RecordType.Name,
                        Source_fields__c, Target_Field_API_Name__c, Target_Object__c,
                        Conversion_Type__c, External_Id_for_Target_Field__c
                from SLID_Mapping_TargetField__r Order By Priority__c ASC)
                from SLID_Mapping_TargetObject__c
                where ServiceProcess__c = :this.serviceProcess.id Order By Priority__c ASC];

                //Instantiate the describes for the target object so we have their metadata on hand
                SLID_ESB_ObjectUtils.instantiateDescribeMap(targetObjects);
            }
            catch(Exception e)
            {
                //Create a custom exception record
                UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ServiceUtils','queryTargetObjectsByServiceProcess','',UTIL_Logging.DEBUG_LEVEL_ERROR);
                UTIL_Logging.logException(log);
                throw e;
            }
        }

        /*
          This function initializes the keyValueMap in the SLID_ESB_MappingUtils class
        */
        private void instantiateKeyValueMap()
        {
            //List of query filters which will be used to query the SLID_Mapping_KeyValue__c object
            List<String> queryFilterValues = new List<String>();

            //Iterate through the data sent by the ESB
            for (Integer dataIndex = 0; dataIndex < this.dataFromESB.records.size(); dataIndex++)
            {
                SLID_ESB_Service.DataRecord data = this.dataFromESB.records[dataIndex];

                //Iterate over the target objects
                for (Integer targetObjectIndex = 0; targetObjectIndex < targetObjects.size(); targetObjectIndex++)
                {
                    SLID_Mapping_TargetObject__c targetObject = targetObjects[targetObjectIndex];

                    //Iterage over the target fields
                    for (SLID_Mapping_TargetField__c targetField : targetObject.SLID_Mapping_TargetField__r)
                    {
                        if (targetField.RecordType.Name == SLID_ESB_ServiceConstants.targetFieldRecTypeNameForMappedValue)
                        {
                            //Compose the base value for this field from the base data
                            String baseValue = retrieveBaseValue(data, targetObject, targetField);

                            //Set the queryFilterValue to retrieve the right SLID_Mapping_KeyValue__c record
                            if(targetField.Mapping_Type__c != null && targetField.Mapping_Type__c != '')
                            {
                                String queryFilterValue = baseValue + '_' + targetField.Mapping_Type__c;

                                //Add to list of filter values
                                queryFilterValues.add(queryFilterValue);
                            }
                        }
                        else if(targetField.RecordType.Name == SLID_ESB_ServiceConstants.targetFieldRecTypeNameForDefaultValue){ // Added by suman
                            //Compose the base value for this field from the base data
                            String baseValue = targetField.Default_Value__c;

                            //Set the queryFilterValue to retrieve the right SLID_Mapping_KeyValue__c record
                            if(targetField.Mapping_Type__c != null && targetField.Mapping_Type__c != '')
                            {
                                String queryFilterValue = baseValue + '_' + targetField.Mapping_Type__c;

                                //Add to list of filter values
                                queryFilterValues.add(queryFilterValue);
                            }
                        }
                    }
                }
            }

            //Initialize list of SLID_Mapping_KeyValue__c records
            try
            {
                //Query based on the queryFilterValues initialized earlier.
                SLID_ESB_MappingUtils.instantiateWithFilterValues(queryFilterValues);
            }
            catch (Exception e)
            {
                //Create a custom exception record
                UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_Service','instantiateKeyValueMap','',UTIL_Logging.DEBUG_LEVEL_ERROR);
                UTIL_Logging.logException(log);
                throw e;
            }
        }

        //Iterate through the ESB data and instantiate the list of records
        public void instantiateRecords()
        {
            for (Integer dataIndex = 0; dataIndex < this.dataFromESB.records.size(); dataIndex++)
            {
                SLID_ESB_Service.DataRecord data = this.dataFromESB.records[dataIndex];

                //Loop through the targetObjects which needs to be created for this data batch
                for (Integer targetObjectIndex = 0; targetObjectIndex < targetObjects.size(); targetObjectIndex++)
                {
                    SLID_Mapping_TargetObject__c targetObject = targetObjects[targetObjectIndex];

                    //Initial object based on sObject type
                    currentObject =  SLID_ESB_ObjectUtils.returnNewSObjectInstance(targetObject.Id);

                    //Add to recordsToUpsertList
                    addRecordToUpsertList(targetObject);

                    //Loop through the fields which need to be set for this object
                    for (SLID_Mapping_TargetField__c targetField : targetObject.SLID_Mapping_TargetField__r)
                    {
                        //Set the field value for this object
                        this.setFieldValue(data, targetObject, targetField);
                    }
                }
            }
        }

        //Inner Method to add object to update list
        private void addRecordToUpsertList(SLID_Mapping_TargetObject__c targetObject)
        {
            if(recordsToUpsert.get(targetObject.Id) == null)
            {
                recordsToUpsert.put(targetObject.Id, new List<SObject>{currentObject});
            }
            else
            {
                List<SObject> objects = recordsToUpsert.get(targetObject.Id);
                objects.add(currentObject);
                recordsToUpsert.put(
                        targetObject.Id,
                        objects
                );
            }
        }

        //Set the field value of a object
        public void setFieldValue(SLID_ESB_Service.DataRecord data, SLID_Mapping_TargetObject__c targetObject, SLID_Mapping_TargetField__c targetField)
        {

            try
            {
                if (targetField.RecordType.Name == SLID_ESB_ServiceConstants.targetFieldRecTypeNameForDefaultValue)
                { // Modified by suman
                    //Retrieve the base value from the data
                    String baseValue = targetField.Default_Value__c;

                    //Retrieve the mapped value from the custom settings
                    String mappedValue = SLID_ESB_ServiceUtils.returnConvertedValue(baseValue, targetField.Conversion_Type__c);

                    //If no mapped value is found, use the base value
                    String fieldValue = mappedValue == '' ? baseValue : mappedValue;

                    //Set default value on the record field
                    SLID_ESB_ObjectUtils.setFieldValue(currentObject, fieldValue, targetObject, targetField);
                }

                else
                {
                    //Retrieve the base value from the data
                    String baseValue = retrieveBaseValue(data, targetObject, targetField);
                    //system.debug('baseValue -->'+baseValue);

                    //Retrieve the mapped value from the custom settings
                    String mappedValue = SLID_ESB_MappingUtils.getMappedValue(baseValue, targetField.Mapping_Type__c);
                    //system.debug('mappedValue -->'+mappedValue);

                    //If no mapped value is found, use the base value
                    String fieldValue = mappedValue == '' ? baseValue : mappedValue;
                    //system.debug('fieldValue -->'+fieldValue);


                    //Retrieve the mapped value from the
                    if((targetField.Conversion_Type__c == 'RecordType'
                            || targetField.Conversion_Type__c == 'User'
                            || targetField.Conversion_Type__c == 'Queue'
                            || targetField.Conversion_Type__c == 'Product')
                            && fieldValue != NULL
                            && fieldValue.length() != 0){

                        fieldValue = SLID_ESB_ServiceUtils.returnConvertedValue(fieldValue, targetField.Conversion_Type__c);
                    }
                    /*
                    if(targetField.Conversion_Type__c == 'User' && fieldValue != NULL && fieldValue.length() != 0){
                        String userId = SLID_ESB_ServiceUtils.returnConvertedValue(fieldValue, targetField.Conversion_Type__c);
                        if(userId != NULL)
                            fieldValue = userId;
                        else
                            fieldValue = '';
                    }
                    else if(targetField.Conversion_Type__c == 'Queue' && fieldValue != NULL && fieldValue.length() != 0){
                        String queueId = SLID_ESB_ServiceUtils.returnConvertedValue(fieldValue, targetField.Conversion_Type__c);
                        if(queueId != NULL)
                            fieldValue = queueId;
                        else
                            fieldValue = '';
                    }
                    else if(targetField.Conversion_Type__c == 'Product' && fieldValue != NULL && fieldValue.length() != 0){
                        String queueId = SLID_ESB_ServiceUtils.returnConvertedValue(fieldValue, targetField.Conversion_Type__c);
                        if(queueId != NULL)
                            fieldValue = queueId;
                        else
                            fieldValue = '';
                    }

                    */
                    system.debug('targetField.Conversion_Type__c -->'+targetField.Conversion_Type__c);
                    system.debug('fieldValue -->'+fieldValue);
                    system.debug('targetField.Target_Field_API_Name__c -->'+targetField.Target_Field_API_Name__c);
                    //Set converted value on the record field
                    if(fieldValue != NULL && fieldValue.length() != 0)
                        SLID_ESB_ObjectUtils.setFieldValue(currentObject, fieldValue, targetObject, targetField);
                }
            }
            catch(Exception e)
            {
                //Create a custom exception record
                UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ServiceUtils.ObjectHandler','setFieldValue','',UTIL_Logging.DEBUG_LEVEL_ERROR);
                UTIL_Logging.logException(log);
                throw e;
            }
        }

        //Logic to retrieve the base value from the data source
        private String retrieveBaseValue(SLID_ESB_Service.DataRecord data, SLID_Mapping_TargetObject__c targetObject, SLID_Mapping_TargetField__c targetField)
        {

            String baseValue = '';
            //Interate over the source fields from the data
            if (targetField.Source_fields__c != '' && targetField.Source_fields__c != null)
            {
                for (String source : targetField.Source_fields__c.split(','))
                {
                    baseValue += data.get(source);
                    baseValue += ',';
                }
                //Remove trailing ,
                baseValue = baseValue.left(basevalue.length()-1);
            }
            //Return value
            return baseValue;
        }

        //Upsert the Objects
        public void upsertObjects()
        {
            try
            {
                //Set the map of upsert results
                Map<Id, List<Database.upsertResult>> upsertResultMap = new Map<Id, List<Database.upsertResult>>();

                //Loop over the target objects
                for (String targetObjectId : recordsToUpsert.keySet())
                {
                    //List of upsert results
                    List<Database.upsertResult> targetObjectUpsertResults;

                    //retrieve the list of upsert records for this Target Object
                    List<SObject> upsertRecords = recordsToUpsert.get(targetObjectId);

                    //Retrieve the target object record
                    SLID_Mapping_TargetObject__c targetObject = this.targetObjectsMap.get(targetObjectId);

                    //Get sObjectType for upsert call
                    Schema.SObjectType targetObjectType = SLID_ESB_ObjectUtils.targetObjectDescribeMap.get(targetObject.Id);

                    //Get External ID field Describe for upsert call
                    Schema.SObjectField externalIdField;
                    if (targetObject.External_Id__c != null)
                    {
                        externalIdField = targetObjectType.getDescribe().fields.getMap().get(targetObject.External_Id__c);
                    }

                    //Upsert the list of retrieved records
                    targetObjectUpsertResults = SLID_ESB_ObjectUtils.upsertGenericSObjectList(targetObjectType, externalIdField, upsertRecords);

                    //Add upsert results to map
                    upsertResultMap.put(targetObjectId, targetObjectUpsertResults);
                }


                this.targetObjectUpsertResultsMap = upsertResultMap;
            }
            catch(Exception e)
            {
                //Create a custom exception record
                System.Debug('~~~ERROR: upsertObjects');
                UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ServiceUtils.ObjectHandler','upsertObjects','',UTIL_Logging.DEBUG_LEVEL_ERROR);
                UTIL_Logging.logException(log);
                throw e;
            }
        }

        //Creates and returns the ESB response
        public SLID_ESB_Service.ServiceResponse getServiceResponse()
        {
            return SLID_ESB_ServiceUtils.createServiceResponse(targetObjectUpsertResultsMap, dataFromESB, targetObjectsMap);
        }
    }

    /**
        Author: Suman Haldar
        Method Name: returnReferenceId(targetFieldAPIName,fieldValue);
        Description: This method will be executed to convert the Record Type & Queue Names into the respective Ids for the case record.
        This method is executed for Target Field values when the record type if 'Default Value'
    **/
    public static String returnConvertedValue(String fieldValue, String conversionType){
        if(conversionType == 'Queue'){
            List<Group> q = [SELECT DeveloperName,Id FROM Group WHERE Type = 'Queue' AND DeveloperName =: fieldValue LIMIT 1];
            if(!q.isEmpty())
                fieldValue = q[0].Id;
            else
                    fieldValue = '';
        }
        else if(conversionType == 'User'){
            List<User> u = [SELECT Id FROM User WHERE Alias =: fieldValue LIMIT 1];
            if(!u.isEmpty())
                fieldValue = u[0].Id;
            else
                    fieldValue = '';
        }
        else if(conversionType == 'RecordType'){
            List<RecordType> r = [SELECT Id FROM RecordType WHERE DeveloperName =: fieldValue LIMIT 1];
            if(!r.isEmpty())
                fieldValue = r[0].Id;
            else
                    fieldValue = '';
        }
        else if(conversionType == 'Product'){
            List<Product2> p =[SELECT Id FROM Product2 WHERE ProductCode =: fieldValue LIMIT 1];
            if(!p.isEmpty())
                fieldValue = p[0].Id;
            else
                    fieldValue = '';
        }
        return fieldValue;
    }

    public static SLID_ESB_Service.ServiceResponse createServiceResponse(Map<Id,list<Database.UpsertResult>> upsertResult , SLID_ESB_Service.CallData esbData, Map<Id, SLID_Mapping_TargetObject__c> targetObjectMap)
    {
        try
        {
            SLID_ESB_Service.ServiceResponse resp = new SLID_ESB_Service.ServiceResponse();
            resp.itemResponses = new list<SLID_ESB_Service.ItemResponse>();
            resp.isSuccess = true;
            resp.allOrNone = SLID_ESB_ObjectUtils.allOrNone;

            list<UTIL_Logging.ExceptionLog> exceptionLogs = new List<UTIL_Logging.ExceptionLog>();

            for(Integer i = 0 ; i < esbData.records.size(); i++)
            {
                SLID_ESB_Service.ItemResponse iResponse = new SLID_ESB_Service.ItemResponse();
                iResponse.recResponse = new List<SLID_ESB_Service.RecordResponse>();
                iResponse.isSuccess = true;

                for(ID key : targetObjectMap.keySet())
                {
                    SLID_Mapping_TargetObject__c tObj = targetObjectMap.get(key);
                    list<Database.UpsertResult> upResults = upsertResult.get(key);

                    //Retrieve the upsert result for this record
                    Database.UpsertResult upResult = upResults[i];

                    //Create a response record for this record, and add it to the batch response
                    SLID_ESB_Service.RecordResponse recResponse = new SLID_ESB_Service.RecordResponse();
                    recResponse.recordId = upResult.getId();
                    recResponse.isSuccess = upResult.isSuccess();

                    if(upResult.isCreated()) //If the record was inserted
                    {
                        recResponse.recOperation = SLID_ESB_ServiceConstants.CREATE_RECORD;
                    }
                    else //If the record was updated
                    {
                        recResponse.recOperation = SLID_ESB_ServiceConstants.UPDATE_RECORD;
                    }

                    //If the record did not upsert successfully, create a exception log
                    if(!recResponse.isSuccess)
                    {
                        recResponse.failureMessage = String.valueOf(upResult.getErrors());

                        //Create Exception for every failed item in the upsert result.
                        UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(upResult,'SLID_ESB_ServiceUtils','createServiceResponse','',UTIL_Logging.DEBUG_LEVEL_ERROR);
                        exceptionLogs.add(log);

                        //update the Item Response and Service Response Fauilure Message.
                        if(iResponse.failureMessage == null || iResponse.failureMessage == '')
                        {
                            iResponse.failureMessage = SLID_ESB_ServiceConstants.DML_FAIL_MESSAGE;
                        }
                        iResponse.isSuccess = false;

                        if(resp.failureMessage == null || resp.failureMessage == '')
                        {
                            resp.failureMessage = SLID_ESB_ServiceConstants.DML_FAIL_MESSAGE;
                        }
                        resp.isSuccess = false;
                        resp.failureType = 'DML EXCEPTION';
                    }
                    recResponse.recordType = tObj.Target_Object_API_Name__c;

                    iResponse.recResponse.add(recResponse);
                    // }
                }

                resp.itemResponses.add(iResponse);
            }

            //Log all DML exceptions
            if(exceptionLogs != null && !exceptionLogs.isEmpty())
            {
                UTIL_Logging.logExceptions(exceptionLogs);
            }
            return resp;
        }
        catch(Exception e)
        {
            //Create a custom exception record
            UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_ESB_ServiceUtils','createServiceResponse','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
        }
        return null;
    }

}