/**
* Author: Salesforce Services
* Name: SLID_BrokerMappingBatchExecutionCntrl.cls
* Purpose: This is the Controller class for the VF Page SLID_BrokerMappingBatchExecution.page.  
*/
public class SLID_BrokerMappingBatchExecutionCntrl 
{
	
	public Boolean buttonsBlockRendered
	{
		get
		{
			for(AsyncApexJobWrapper j : apexJobs)
			{
				if(pendingJobStatus.contains(j.status))
				{
					return false;
					break;
				}
			}
			return true;
		}
		set;
	}
	
	public Boolean statusBlockRendered
	{
		get
		{
			if(!apexJobs.isEmpty())
				return true;
			return false;
		}
		set;
	}
	
	@TestVisible
	private Map<Id,String> classIdProcessNameMap = new Map<Id,String>();
	
	private Set<String> pendingJobStatus = new Set<String>{'Holding','Queued','Preparing','Processing'};
	
	public List<AsyncApexJobWrapper> apexJobs {get;set;}
	
	@TestVisible
	private List<String> classIds = new list<String>(); 
	
	private static final String BROKER_PURGE_PROCESS = 'PURGE BROKER STAGE DATA';
	
	private static final String PURGE_BROKER_MAPPING_DATA = 'PURGE BROKER MAPPING DATA';
	
	private static final String BROKER_MAPPING_UPDATE_PROCESS = 'UPDATE BROKER MAPPING DATA';
	
	private static final String BROKER_MAPPING_UPDATE_MI_DATA_PROCESS = 'UPDATE MI DATA ON BROKER STAGE RECORDS';
	
	/**
	* Name : AsyncApexJobWrapper
	* @description  This is the innerclass which would be the wrapper class for the Apex job record. Instances of this class would be used
	* 				as the data provider to the data table on the vf page. The wrapper class is required to display the process name 
	*				,for each of the batch executions , within the data table.				 
	*/
	public class AsyncApexJobWrapper
	{
		public String compDate {get;set;}
		public String status {get;set;}
		public String processName {get;set;}
		public String jobId {get;set;}
		public String jobType {get;set;}
		
		public AsyncApexJobWrapper(AsyncApexJob j , Map<Id,String> classIdProcessNameMap)
		{
			jobId = j.Id;
			jobType = j.JobType;
			if(j.CompletedDate != null)
				compDate = j.CompletedDate.format();
			status = j.Status;
			processName = classIdProcessNameMap.get(j.ApexClassId);
		}
	}
	
	/**
      * @description       Constructor for the class. This Constructor would execute the following:
      *						- Set the list of class Ids for the batch jobs that would be triggered using the vf page.
      *						-  Populate the data table with the last 10 jobs that were executed as a part of this process. 
      * @throws            Exception 
      */    
    public SLID_BrokerMappingBatchExecutionCntrl()
    {
    	try
    	{
    		classIds = mapBrokerMappingClassIds(classIdProcessNameMap);
    		populateApexJobsInfo();
    		
    	}
    	catch(Exception e)
    	{
    		
    		UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_BrokerMappingBatchExecutionCntrl','SLID_BrokerMappingBatchExecutionCntrl','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);      
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
    	}
    }
    
    
	/**
      * @description       This method would query for the Apex jobs that are related to the Broker Data Batch Process and display the 
      *					   jobs in the table so that the user can get a view of all the previous executed batch jobs with the status.  
      *						 
      * @throws            Exception 
      */  
    private void populateApexJobsInfo()
    {
    	try
    	{
	    	apexJobs = new list<AsyncApexJobWrapper>();
	    	
	    	if(!classIds.isEmpty())
	    	{
	    		List<AsyncApexJob> jobs = [Select Id , JobType, Status , CompletedDate , ApexClassId 
	      					 	 	From AsyncApexJob 
	      					  	 	Where ApexClassId IN: classIds
	      					  	 	And JobType = 'BatchApex'
	      					  	 	ORDER BY CompletedDate DESC LIMIT 10];
	      		
	      		if(!jobs.isEmpty())
	      		{
	      			for(integer i = 0 ; i < jobs.size() ; i++)
	      			{
	      				apexJobs.add(new AsyncApexJobWrapper((AsyncApexJob)jobs[i],classIdProcessNameMap));
	      			}
	      		}
	      		
	    	}
    	}
    	catch(Exception e)
    	{
    		UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_BrokerMappingBatchExecutionCntrl','populateApexJobsInfo','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            throw e;
    	}
    	
    }
    
    /**
      * @description       This method would execute the batch job to delete all records in the broker stage object.
      *						 
      * @throws            Exception 
      */   
    public void executeDeleteBatchJob()
    {
    	try
    	{
    		Database.executeBatch(new SLID_PurgeBrokerStage(),2000);
    		populateApexJobsInfo();
    	}
    	catch(Exception e)
    	{
    		UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_BrokerMappingBatchExecutionCntrl','executeDeleteBatchJob','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
    	}
    }
    
     /**
      * @description       This method would execute the batch job to update the mapping object with the broker mapping data
      *						 
      * @throws            Exception 
      */   
    public void executeUpdateMappingDataJob()
    {
    	try
    	{
    		Database.executeBatch(new SLID_PurgeBrokerMapping(true),200);
    		populateApexJobsInfo();
    	}
    	catch(Exception e)
    	{
    		UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_BrokerMappingBatchExecutionCntrl','executeUpdateMappingDataJob','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
    	}
    }
    
    public void executeUpdateMIdataOnBrokerStageRecs()
    {
    	try
    	{
    		Database.executeBatch(new SLID_UpdateBrokerFieldsFromMI(),200);
    		populateApexJobsInfo();
    	}
    	catch(Exception e)
    	{
    		UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_BrokerMappingBatchExecutionCntrl','executeUpdateMIdataOnBrokerStageRecs','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
    	}
    }
    
    /**
      * @description       This method queries for the class records for the batch classes and populates the map to get the 
      *					   Class Id from Class Name
      * @throws            Exception 
      */  
    @TestVisible 
    private List<String> mapBrokerMappingClassIds(Map<Id,String> classIdProcessNameMap)
    {
    	try
    	{
 	    	List<String> brokerClassIds = new List<String>();
	    	
	    	SLID_Broker_Mapping_Settings__c brokerMappingSettings = SLID_Broker_Mapping_Settings__c.getInstance();
	    	
	    	List<String> classNames = new List<String>();
	    	
	    	classNames.add(brokerMappingSettings.SLID_CreateBrokerMappingClassName__c);
	    	classNames.add(brokerMappingSettings.SLID_PurgeBrokerMappingClassName__c);
	    	classNames.add(brokerMappingSettings.SLID_PurgeBrokerStageClassName__c);
	    	classNames.add(brokerMappingSettings.SLID_UpdateBrokerFieldsFromMIClassName__c);
	    	
	    	List<ApexClass> classes = [Select Status, Name, Id From ApexClass Where Name IN: classNames];
	    	
	    	for(ApexClass c : classes)
	    	{
	    		brokerClassIds.add(c.Id);
	    		if(c.Name == brokerMappingSettings.SLID_CreateBrokerMappingClassName__c)
	    			classIdProcessNameMap.put(c.Id,BROKER_MAPPING_UPDATE_PROCESS);
	    		if(c.Name == brokerMappingSettings.SLID_PurgeBrokerMappingClassName__c)
	    			classIdProcessNameMap.put(c.Id,PURGE_BROKER_MAPPING_DATA);
	    		if(c.Name == brokerMappingSettings.SLID_PurgeBrokerStageClassName__c)
	    			classIdProcessNameMap.put(c.Id,BROKER_PURGE_PROCESS);
	    		if(c.Name == brokerMappingSettings.SLID_UpdateBrokerFieldsFromMIClassName__c)
	    			classIdProcessNameMap.put(c.Id,BROKER_MAPPING_UPDATE_MI_DATA_PROCESS);
	    	} 
	    	
			return brokerClassIds;
    	}
    	catch(Exception e)
    	{
    		UTIL_Logging.ExceptionLog log = new UTIL_Logging.ExceptionLog(e,'SLID_BrokerMappingBatchExecutionCntrl','mapBrokerMappingClassIds','',UTIL_Logging.DEBUG_LEVEL_ERROR);
            UTIL_Logging.logException(log);
            if(!test.isRunningTest())
            	throw e;
            else
            	return null;
    	}	
      }
}