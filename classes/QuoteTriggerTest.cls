/**
 * Author: James Lee
 * Created At: November 5, 2016
 * 
 * Unit tests for QuoteTriggerTest.
 */
@isTest
public class QuoteTriggerTest
{
    // Should be moved to a Constants class at some point.
    private static final String DML_VALIDATION_EXCEPTION_ERROR = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
    
    static testMethod void testInsertDefaultRisks()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
            
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            List<Risk__c> rs = [
                SELECT id, RecordTypeId, Coverage_Selected_Option_1__c, Coverage_Selected_Option_2__c
                FROM Risk__c
                WHERE quote__c = :q.Id
                AND recordTypeId != :riskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION)
            ];
            
            for (Risk__c r : rs)
            {
                if (r.recordTypeId == RiskTypes.get(RiskService.RISK_RECORDTYPE_LIABILITY))
                {
                    System.assertEquals(RiskService.COVERAGE_CGL, r.Coverage_Selected_Option_1__c);
                    System.assertEquals(RiskService.COVERAGE_CGL, r.Coverage_Selected_Option_2__c);
                }
                else if (r.recordTypeId == RiskTypes.get(RiskService.RISK_RECORDTYPE_AGGREGATE))
                {
                    System.assertEquals(RiskService.COVERAGE_AGGREGATE, r.Coverage_Selected_Option_1__c);
                    System.assertEquals(RiskService.COVERAGE_AGGREGATE, r.Coverage_Selected_Option_2__c);
                }
                else if (r.recordTypeId == RiskTypes.get(RiskService.RISK_RECORDTYPE_CYBER))
                {
                    System.assertEquals(RiskService.COVERAGE_CYBER, r.Coverage_Selected_Option_1__c);
                    System.assertEquals(RiskService.COVERAGE_CYBER, r.Coverage_Selected_Option_2__c);
                }
            }
        }
    }
    
    static testMethod void testUpdateSelectedCoverages()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            
            q.Package_Type_Selected__c = 'Option 1';
            update q;
            
            List<Coverages__c> coverages = [
                SELECT Id, Package_ID__c, Selected__c
                FROM Coverages__c
                WHERE Risk__c = :r.Id
            ];
            
            for (Coverages__c c : coverages)
            {
                if (c.Package_ID__c == q.Package_Type_Selected__c)
                {
                    System.assertEquals(true, c.Selected__c);
                }
                else
                {
                    System.assertEquals(false, c.Selected__c);
                }
            }
        }
    }
    
    /**
     * FP-1534
     * This test method enforces the functionality around the underwriter's experience around premium deviation.
     */
    static testMethod void testDeviationsForUnderwriter()
    {
        String deviationReason = 'Underwriter rate adjustment – decrease';
        Integer maxDiscountLevel1 = 40;
        Integer maxDiscountLevel2 = 40;
        Integer maxDiscountLevel3 = 40;
        
        Case c;
        Quote__c q;
        
        // Test assertions:
        // 1. A new quote will not have any deviations.
        // 2. Calculated deviation value matches the setting for deviation type and deviation value.
        // 3. A deviation reason must be provided if a deviation is set
        // 4. UW can add packages for deviate without activating the trigger.
        // 5. UW can apply a maximum discount value.
        // 6. UW can remove packages for deviate without activating the trigger.
        // 7. UW will activate trigger when no packages are selected when deviating.
        // 8. UW cannot apply a negative deviation percentage.
        // 8. UW cannot apply a higher discount value than configured.
        User uwLevel1 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel1)
        {
            // Generate empty quote.
            c = SMEQ_TestDataGenerator.getOpenSprntCase();
            q = SMEQ_TestDataGenerator.getOpenQuote(c);
            
            // Query the new quote with deviation information.
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(null, q.Deviation_Percentage__c, 'No deviation should set for a new quote.');
            System.assertEquals(null, q.Deviation_Type__c, 'No deviation type should be set for a new quote.');
            System.assertEquals(null, q.Selected_Packages_to_Deviate__c, 'A new quote should not have selected packages to deviate.');
            System.assertEquals(null, q.Deviation_Reason_s__c, 'A new quote should not have deviation reasons.');
            
            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_DISCOUNT;
            q.Deviation_Percentage__c = maxDiscountLevel1;
            try
            {
                // Update without deviation reason.
                update q;
                System.assert(false, 'A deviation reason is expected.');
            }
            catch (DMLException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
            
            // Amend with deviation reason.
            q.Deviation_Reason_s__c = deviationReason;
            // Only select two packages to deviate.
            q.Selected_Packages_to_Deviate__c = RiskService.PACKAGE_STANDARD + ';' + RiskService.PACKAGE_OPTION_1;
            update q;
            
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(-maxDiscountLevel1, q.Calculated_Deviation__c, 'Calculated deviation does not match expected deviation.');
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_STANDARD), 'Standard package expected.');
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_OPTION_1), 'Option 1 package expected.');
            System.assert(!q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_OPTION_2), 'Option 2 package unexpected.');
            System.assertNotEquals(null, q.Deviation_Reason_s__c, 'A deviation must be accompanied by a deviation reason.');
            
            q.Selected_Packages_to_Deviate__c = null;
            update q;
            
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(null, q.Selected_Packages_to_Deviate__c, 
                                'Packages selected for deviation should not bounce back to selected if they are removed.');
            
            // Reset deviation values for next scenario.
            q.Selected_Packages_to_Deviate__c = null;
            update q;
            
            // Change the deviation percentage to activate trigger for selected packages.
            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_DISCOUNT;
            q.Deviation_Percentage__c = maxDiscountLevel1 - 5;
            update q;
            
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_STANDARD), 'Standard package expected.');
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_OPTION_1), 'Option 1 package expected.');
            System.assert(q.Selected_Packages_to_Deviate__c.contains(RiskService.PACKAGE_OPTION_2), 'Option 2 package expected.');
            
            // Attempt to input a negative deviation percentage.
            q.Deviation_Percentage__c = -maxDiscountLevel1;
            try
            {
                update q;
                System.assert(false, 'Negative deviation percentage value.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
            
            // Attempt to deviate beyond level of authority.
            q.Deviation_Percentage__c = maxDiscountLevel1 + 1;
            try
            {
                update q;
                System.assert(false, 'Deviation exceeds authority.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
        }
        
        // Test assertions:
        // 1. Calculated deviation value matches the setting for deviation type and deviation value.
        // 2. UW can apply a maximum discount value.
        // 3. Updated deviate value will return packages to deviate via trigger.
        // 4. UW cannot apply a higher discount value than configured.
        User uwLevel2 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL2, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel2)
        {
            // Query the new quote with deviation information.
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            q.Deviation_Percentage__c = maxDiscountLevel2;
            update q;
            
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(-maxDiscountLevel2, q.Calculated_Deviation__c, 'Calculated deviation does not match expected deviation.');
            
            // Attempt to deviate beyond level of authority.
            q.Deviation_Percentage__c = maxDiscountLevel2 + 1;
            try
            {
                update q;
                System.assert(false, 'Deviation exceeds authority.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
        }
        
        // Test assertions:
        // 1. Calculated deviation value matches the setting for deviation type and deviation value.
        // 2. UW can apply a maximum discount value.
        // 3. UW cannot apply a higher discount value than configured.
        User uwLevel3 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL3, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel3)
        {
            // Query the new quote with deviation information.
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            
            q.Deviation_Percentage__c = maxDiscountLevel3;
            update q;
            
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(-maxDiscountLevel3, q.Calculated_Deviation__c, 'Calculated deviation does not match expected deviation.');
            
            // Attempt to deviate beyond level of authority.
            q.Deviation_Percentage__c = maxDiscountLevel3 + 1;
            try
            {
                update q;
                System.assert(false, 'Deviation exceeds authority.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
        }
    }
    
    /**
     * FP-1534
     * This test method enforces the functionality around the broker's experience around premium deviation.
     */
    static testMethod void testDeviationsAsBroker()
    {
        String deviationReason = 'Broker rate adjustment – decrease';
        Integer maxDiscountBroker = 20;
        Integer maxSurchargeBroker = 100;
        
        Quote__c q;
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        // Test assertions:
        // 1. A new quote will not have any deviations.
        // 2. Calculated deviation value matches the setting for deviation type and deviation value.
        // 3. A deviation reason must be provided if a deviation is set.
        // 4. Broker can apply a maximum discount value.
        // 5. Broker will not activate the trigger to automatically update the selected packages to deviate field.
        // 6. Broker cannot apply a negative deviation percentage value.
        // 7. Broker cannot apply a higher discount value than configured.
        System.runAs(u)
        {
            // Generate empty quote.
            q = SMEQ_TestDataGenerator.getOpenQuote();
            
            // Query the new quote with deviation information.
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(null, q.Deviation_Percentage__c, 'No deviation should set for a new quote.');
            System.assertEquals(null, q.Deviation_Type__c, 'No deviation type should be set for a new quote.');
            System.assertEquals(null, q.Selected_Packages_to_Deviate__c, 'A new quote should not have selected packages to deviate.');
            System.assertEquals(null, q.Deviation_Reason_s__c, 'A new quote should not have a deviation reason.');
            
            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_DISCOUNT;
            q.Deviation_Percentage__c = maxDiscountBroker;
            
            try
            {
                // Update without deviation reason.
                update q;
                System.assert(false, 'A deviation reason is expected.');
            }
            catch (DMLException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
            
            // Amend with deviation reason.
            q.Deviation_Reason_s__c = deviationReason;
            update q;
            
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ];
            System.assertEquals(-maxDiscountBroker, q.Calculated_Deviation__c, 'Calculated deviation does not match expected deviation.');
            System.assertEquals(null, q.Selected_Packages_to_Deviate__c, 'No packages should be automatically selected via trigger.');
            System.assertNotEquals(null, q.Deviation_Reason_s__c, 'A deviation must be accompanied by a deviation reason.');
            
            // Attempt to input a negative deviation percentage.
            q.Deviation_Percentage__c = -maxDiscountBroker;
            try
            {
                update q;
                System.assert(false, 'Negative deviation percentage value.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
            
            // Attempt to deviate beyond level of authority for discount.
            q.Deviation_Percentage__c = maxDiscountBroker + 1;
            try
            {
                update q;
                System.assert(false, 'Deviation exceeds authority.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
            
            // Attempt to deviate beyond level of authority for surcharge.
            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_SURCHARGE;
            q.Deviation_Percentage__c = maxSurchargeBroker + 1;
            try
            {
                update q;
                System.assert(false, 'Deviation exceeds authority.');
            }
            catch (DmlException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
        }
    }
    
    /**
     * FP-4041
     * This test method enforces the functionality around the underwriter's Level TUA experience around premium deviation.
     */
    static testMethod void testDeviationsForTUA()
    {
        String deviationReason = 'Underwriter rate adjustment – decrease';        
        Integer deviationAmount = 10;
        Case c;
        Quote__c q;
        
        User userTUA = UserService_Test.getUwUser(UserService_Test.UW_TUA, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        User uwLevel1 = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uwLevel1)
        {
            // Setting up the test data as an UW to leveragege the authority they have to update the deviation percentage as TUA does not have the same authority.
            // Generate empty quote.
            c = SMEQ_TestDataGenerator.getOpenSprntCase();
            q = SMEQ_TestDataGenerator.getOpenQuote(c);
            
            // Query the new quote with deviation information.
            q = [
                SELECT id, Selected_Packages_to_Deviate__c, Deviation_Type__c, Deviation_Percentage__c, Calculated_Deviation__c, Deviation_Reason_s__c
                FROM Quote__c
                WHERE id = :q.Id
            ]; 
                     
            // Test assertions:
            // 1. A new quote will not have any deviations.
            // 2. Calculated deviation value matches the setting for deviation type and deviation value.
            // 3. A deviation reason must be provided if a deviation is set.
          
            
            System.assertEquals(null, q.Deviation_Percentage__c, 'No deviation should set for a new quote.');
            System.assertEquals(null, q.Deviation_Type__c, 'No deviation type should be set for a new quote.');
            System.assertEquals(null, q.Selected_Packages_to_Deviate__c, 'A new quote should not have selected packages to deviate.');
            System.assertEquals(null, q.Deviation_Reason_s__c, 'A new quote should not have deviation reasons.');

            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_DISCOUNT;
            q.Deviation_Percentage__c = deviationAmount;
            
            try
            {
                // Update without deviation reason.
                update q;
                System.assert(false, 'A deviation reason is expected.');
            }
            catch (DMLException dmle)
            {
                System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
            }
            catch (Exception e)
            {
                System.assert(false, 'Should not reach here.');
            }
            
            // Amend with deviation reason.
            q.Deviation_Reason_s__c = deviationReason;
            // Only select two packages to deviate.
            q.Selected_Packages_to_Deviate__c = RiskService.PACKAGE_STANDARD + ';' + RiskService.PACKAGE_OPTION_1;
            update q;
            
        }
        
        System.runAs(userTUA)
        {
            //// Test assertion:
            // 1. TUA does not have the authority to modify the deviation percentage.
            
            
            try
             {   
                 q.Deviation_Percentage__c = deviationAmount+5;
                 update q;
                 System.assert(false, 'TUA does not have authority to change deviation.');
             }
             catch (DmlException dmle)
             {
                 System.assert(dmle.getMessage().contains(DML_VALIDATION_EXCEPTION_ERROR), 'Validation exception expected.');
             }
             catch (Exception e)
             {
                 System.assert(false, 'Should not reach here.');
             }
        }
        
    }
    
    static testMethod void testInsertCaseRoleSharing()
    {
        //give a role to running user who is going to create portal users
        User currentUser = [Select UserRoleId, Id from User where Id=:UserInfo.getUserId()];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        currentUser.UserRoleId = portalRole.Id;
        System.runAs(new User(Id = Userinfo.getUserId())){
            update currentUser;
        }
        
        List<Profile> profiles = [select id from profile where name = 'SME Broker'];
        Id TestProfileId = profiles[0].id;
        
        Account testAccount = SMEQ_TestDataGenerator.getBrokerageAccount();
        Contact testContact1 = SMEQ_TestDataGenerator.getProducerContact(testAccount);
        
        User PartnerUser1 = new User(alias = 'test123', email = 'test123@noemail.com', emailencodingkey = 'UTF-8',
                lastname = 'Testing', languagelocalekey = 'en_US',
                ProfileId = TestProfileId,
                country = 'United States', IsActive = true,
                ContactId = testContact1.Id,
                LocaleSidKey = 'en_CA',
                timezonesidkey = 'America/Los_Angeles', username = 'UNITTEST1@USER_NAME.com');
        System.runAs(new User(Id=Userinfo.getUserId())){
            insert PartnerUser1;
        }
        // Needed to create a separete contact for testing purpose and seprate portal user.
        Contact testContact2 = new Contact();
            testContact2.RecordTypeId = testContact1.RecordTypeId;
            testContact2.AccountId = testAccount.Id;
            testContact2.FirstName = 'XioaTest';
            testContact2.LastName = 'MayTest' + Math.random();
            testContact2.Email = 'xioarsauseremail@rsaregroupe.ca';
            testContact2.bkrContact_Status__c = 'Active';
        System.runAs(new User(Id=Userinfo.getUserId())){
            insert testContact2;
        }
        
        User PartnerUser2 = new User(alias = 'test123', email = 'test234@noemail.com', emailencodingkey = 'UTF-8',
                lastname = 'Testing', languagelocalekey = 'en_US',
                ProfileId = TestProfileId,
                country = 'United States', IsActive = true,
                ContactId = testContact2.Id,
                LocaleSidKey = 'en_CA',
                timezonesidkey = 'America/Los_Angeles', username = 'UNITTEST2@USER_NAME.com');
        System.runAs(new User(Id=Userinfo.getUserId())){
            insert PartnerUser2;
        }
        
        Case testCase = SMEQ_TestDataGenerator.getOpenSprntCase();
        testCase.ownerId = PartnerUser1.id;
        update testCase;
        
        ContactShare cs1 = new ContactShare(UserOrGroupId=PartnerUser1.id, ContactId = testContact1.Id, ContactAccessLevel = 'Read');
        ContactShare cs2 = new ContactShare(UserOrGroupId=PartnerUser2.id, ContactId = testContact2.Id, ContactAccessLevel = 'Read');
        AccountShare as1 = new AccountShare(UserOrGroupId=PartnerUser1.id, AccountId = testAccount.Id, AccountAccessLevel = 'Read',OpportunityAccessLevel = 'Read');
        AccountShare as2 = new AccountShare(UserOrGroupId=PartnerUser2.id, AccountId = testAccount.Id, AccountAccessLevel = 'Read',OpportunityAccessLevel = 'Read');
        insert cs1;
        insert cs2;
        insert as1;
        insert as2;
        Test.startTest();
        System.runAs(PartnerUser1){
            SMEQ_TestDataGenerator.getOpenQuote(testCase);
        }
        
        System.runAs(PartnerUser2){
            List<CaseShare> testShare = [select UserOrGroupId from CaseShare where UserOrGroupId =: PartnerUser2.id];
            System.assert(testShare != null);
        }
        Test.stopTest();
    }
    
    /**
     * This method will test populateCaseInfo for finalize quote method in QuoteService class
     */
    static testMethod void testPopulateCaseInfoFinalizeQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            
            q.Package_Type_Selected__c = 'Standard';
            q.Air_Miles_Loyalty_Points_Standard__c=19000;
            q.Premium__c = 12345;
            q.ePolicy__c= '12345';
            q.Status__c='Finalized';
            Test.startTest();
            update q;
            Test.stopTest();
            Case cse = [
                Select Id, Airmiles_from_Quote__c, bkrCase_Quoted_Premium__c
                FROM Case
                WHERE id = :q.Case__c
            ];
            
            system.assertEquals(cse.Airmiles_from_Quote__c, q.Air_Miles_Loyalty_Points_Standard__c);
            system.assertEquals(cse.bkrCase_Quoted_Premium__c, q.Premium__c);
        }
    }
    
    /**
     * This method will test populateCaseInfo for bind quote method in QuoteService class
     */
    static testMethod void testPopulateCaseInfoBindQuote()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            
            q.Package_Type_Selected__c = 'Standard';
            q.Air_Miles_Loyalty_Points_Standard__c=19000;
            q.Premium__c = 12345;
            q.ePolicy__c= '12345';
            q.Status__c='Bind';
            Test.startTest();
            update q;
            Test.stopTest();
            Case cse = [
                SELECT Id, Airmiles_from_Quote__c, bkrCase_Quoted_Premium__c, bkrCase_Policy__c, bkrCase_Bound_Premium__c
                FROM Case
                WHERE id = :q.Case__c
            ];
            system.assertEquals(cse.bkrCase_Bound_Premium__c, q.Premium__c);
            system.assertEquals(cse.bkrCase_Policy__c, q.ePolicy__c);
        }
    }
    
    /**
     * This method will test InsertLocationRisk method in QuoteService class
     */
    static testMethod void testInsertLocationRisks()
    {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Map<String, Id> RiskTypes = Utils.GetRecordTypeIdsByDeveloperName(Risk__c.SObjectType, true);
            Map<String, Id> AccountTypes = Utils.GetRecordTypeIdsByDeveloperName(Account.SObjectType, true);            
            SMEQ_TestDataGenerator.generateFullTestQuoteV2();
            Quote__c quote = [Select Id from Quote__c LIMIT 1];
            quote.Identical_Location_And_Business_Address__c = true;
            
            update quote;
            Risk__c risk = [
                SELECT Id, Address_Line_1__c, City__c, Postal_Code__c, Province__c, Country__c
                FROM Risk__c
                WHERE recordtypeid = :RiskTypes.get(RiskService.RISK_RECORDTYPE_LOCATION) LIMIT 1
            ];
            Account insured = [
                SELECT BillingStreet,BillingCity, BillingCountry, BillingState, BillingPostalCode
                FROM Account
                WHERE recordtypeid = :AccountTypes.get('Policyholder') LIMIT 1 
            ];
            system.assert(risk != null);
            system.assertEquals(risk.Address_Line_1__c,insured.BillingStreet);
            system.assertEquals(risk.City__c,insured.BillingCity);
            system.assertEquals(risk.Postal_Code__c,insured.BillingPostalCode);
            system.assertEquals(risk.Province__c,insured.BillingState);
            system.assertEquals(risk.Country__c,insured.BillingCountry);
           
         }
    }
    
    /*
     * This test method enforces the functionality to update the referral trigger reason when the LLP score meets a threshold.
     */
    static testMethod void testUpdateCaseWithLLPReferralTriggerReason()
    {
        String deviationReason = 'Underwriter rate adjustment – decrease';
        Integer deviationAmount = 1;
        
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
            Quote__c q = SMEQ_TestDataGenerator.getOpenQuote();
            Risk__c r = SMEQ_TestDataGenerator.getLocationRisk(q);
            
            q.Large_Loss_Propensity_Score__c = QuoteService.LLP_REFERRAL_TRIGGER_THRESHOLD;
            q.Selected_Packages_to_Deviate__c = RiskService.PACKAGE_OPTION_1;
            q.Deviation_Type__c = QuoteService.DEVIATION_TYPE_DISCOUNT;
            q.Deviation_Reason_s__c = deviationReason;
            q.Deviation_Percentage__c = deviationAmount;
            Test.startTest();
            update q;
            Test.stopTest();
            Case cse = [
                Select Id, Referral_Trigger_Reason_s__c
                FROM Case
                WHERE id = :q.Case__c
            ];
            
            System.assertNotEquals(null, cse.Referral_Trigger_Reason_s__c, 'Expecting a referral trigger reason');
            System.assert(cse.Referral_Trigger_Reason_s__c.contains(QuoteService.LLP_REFERRAL_TRIGGER_REASON));
        }
    }
}