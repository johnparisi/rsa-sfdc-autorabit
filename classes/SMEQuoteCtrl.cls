/**
 * SMEQuoteCtrl
 * Author: James Lee
 * Date: October 6 2016
 *
 * The purpose of this controller is to capture button press actions on a visualforce page to send
 * a quote request to ePolicy via the ESB asynchronously using the Continuation Pattern:
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_continuation_overview.htm
 */
global class SMEQuoteCtrl
{
    public static final Integer CONTINUATION_TIMEOUT = 120;
    public static final Integer CONTINUATION_PARALLEL_CALLOUT_LIMIT = 3;

    private static final String SME_RECORD_TYPE = 'CI Open SPRNT';
    private static final String SME_INDUSTRY_SEGMENT = 'SME-EP';
    public static final String REQUEST_LOG = 'REQUEST';
    public static final String RESPONSE_LOG = 'RESPONSE';
    public static final String METHOD_LOG = 'METHOD';
    public static final String RESPONSE_METHOD_LOG = 'RESPONSE METHOD';    
    private ApexPages.StandardController controller;
    
    private static String STATUS_SUCCESS = 'Success';
    private static STring STATUS_ERROR = 'Error';
    private static String SOAP_ENVELOPE = '/Envelope/Body';
    private static String ERROR_RESPONSE_CODE_PATH =             '/ca.rsagroup.domain.Response/responseCode';
    private static String ERROR_CODE_PATH =                     '/ca.rsagroup.domain.Response/errors/ca.rsagroup.domain.Error/errorCode';
    private static String ERROR_MESSAGE_FAILURE_PATH =             '/ca.rsagroup.domain.Response/errors/ca.rsagroup.domain.Error/errorMessage';  
    private static String ERROR_RESPONSE_CODE_PATH_SOAP =         SOAP_ENVELOPE + ERROR_RESPONSE_CODE_PATH; 
    private static String ERROR_CODE_PATH_SOAP =                 SOAP_ENVELOPE + ERROR_CODE_PATH;
    private static String ERROR_MESSAGE_FAILURE_PATH_SOAP =     SOAP_ENVELOPE + ERROR_MESSAGE_FAILURE_PATH;

    // Tracks the request label returned by the continuation server with the request body string.
    private Map<String, String> requestMap;
    private Map<String, RequestInfo> ris;

    public Id qId { get; set; }
    public transient Quote__c quote { get; set; }
    public Case parentCase {get; set;}
    public final String parentCaseId;
    public String comNumber { get;set; }
    
    private RSA_ESBService service;
    
    private String abortRequestLabel;
    private String saveRequestLabel;

    //to use for re-directing user to the newly generated quote for LOADQUOTE
    private List <Quote__c> policyList;
    
    // Continuation object to facilitate asynchronous callout to ePolicy.
    // Marked as transient as it does not need to be serialized and used as part of the response parsing.
    private transient Continuation con;
    private Map<String, String> logWrappers;

    public SMEQuoteCtrl(ApexPages.StandardController controller)
    {
        this.controller = controller;

        quote = (Quote__c) controller.getRecord();
        qId = quote.Id;
        // Added the field isLiveChatIndicator__c in Quote query for FRZR-123 & FRZR-166
        quote = [
            SELECT id, Name, case__r.Offering_Project__c,case__r.bkrCase_Region__c,case__r.recordType.name,case__r.Account.Industry_segment__c,
            Quote_Date__c, Finalized_Date__c, Package_Type_Selected__c, isLiveChatIndicator__c,
            Standard_Premium__c, Option_1_Premium__c, Option_2_Premium__c, Case__r.bkrCase_Subm_Type__c, 
            Case__r.bkrCase_SIC_Code__r.Name, ePolicy__c, Received_Date__c, Transaction_Effective_Date__c, Reasons__c, Renewal_Reason_s__c,
            Case__r.Segment__c, Case__r.contact.id,Case__r.bkrCase_SIC_Code__r.sic_code__c, Case__r.ePolicy_Transaction_Status__c,
            Quote_Number__c, Status__c, Case__r.bkrCase_SIC_Code__r.SIC_Code_Detail__r.Segment__c,Foreign_Revenue__c,US_Revenue__c,Case__r.Monoline__c, 
            New_Premium__c,Case__r.Resume_Transaction__c, of_days_out_from_renewal__c 
            FROM Quote__c
            WHERE id = :qId
            LIMIT 1
        ];

        this.requestMap = new Map<String, String>();
        this.ris = new Map<String, RequestInfo>();
        this.comNumber = quote.ePolicy__c;
        this.parentCase = quote.Case__r;
        this.parentCaseId = parentCase.Id;
        
        // Continuation object to facilitate asynchronous callout to ePolicy.
        this.con = new Continuation(CONTINUATION_TIMEOUT);
        this.con.continuationMethod = 'processResponse';
        this.logWrappers = new Map<String, String>();
    }

    /*
     * This method calls the GET_QUOTE service from the ESB. It will update the premium for the specified package type selected on the quote.
     */
    public Object getQuote()
    {
        RequestInfo ri = new RequestInfo(
            RequestInfo.ServiceType.GET_QUOTE,
            quote.Package_Type_Selected__c,
            quote.Case__r.Segment__c
        );

        addContinuationRequest(ri);
        return this.con;
    }

    /*
     * Requests the rates for the underwriter.
     */
    public Object getQuoteUW()
    {
        DateTime methodStart = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper methodLog;
        ApplicationLogWrapper requestLog;
        Map<String, String> logWrappers = new Map<String, String>();

        try
        {
            Boolean hasReferralRevenues = false;
            
            if(quote.ePolicy__c == null){

	            for (SIC_Answer__c sa : SicQuestionService.getSicAnswers(quote.Id))
	            {
	                // Check if the quote has populated values for the US or foreign revenues.
	                if (!String.isBlank(sa.Value__c) &&
	                    (isQuestion(sa, SicQuestionService.CODE_FOREIGN_REVENUE) ||
	                     isQuestion(sa, SicQuestionService.CODE_US_REVENUE))
	                   )
	                {
	                    // Check if the quote has populated values for the US or foreign revenues.
	                    if (!String.isBlank(sa.Value__c) && 
	                        (isQuestion(sa, SicQuestionService.CODE_FOREIGN_REVENUE) || 
	                         isQuestion(sa, SicQuestionService.CODE_US_REVENUE))
	                       )
	                    {
	                        hasReferralRevenues = true;
	                        break;
	                    }
	                }
	            }
            
	            if (quote.case__r.recordType.name == SME_RECORD_TYPE)
	            {
	                if ((quote.case__r.Account.Industry_segment__c == null) ||
	                    (quote.case__r.Account.Industry_segment__c != null && !quote.case__r.Account.Industry_segment__c.contains(SME_INDUSTRY_SEGMENT)))
	                {
	                    throw new RSA_ESBException(Label.Unable_to_rate_this_quote_The_selected_brokerage_account_is_not_supported_in_eP);
	                }
	            }
	            if (!RSA_ESBUWPermissions.getPermission(quote.Case__r.Offering_Project__c, quote.Case__r.Segment__c).Get_Rate__c ||
	                    hasReferralRevenues ||
	                    quote.case__r.Monoline__c)
	                    {
	                    // This quote is not eligible for UW quoting from Salesforce, and must be uploaded to ePolicy.
	                    throw new RSA_ESBException(Label.UW_InvalidSMESICCode);
	                    }
	        } 
            else {
                if (quote.case__r.Monoline__c)
                {
                    // This quote is not eligible for UW quoting from Salesforce, and must be uploaded to ePolicy.
                    throw new RSA_ESBException(Label.UW_InvalidSMESICCode);
                }
            }

            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'getQuoteUW Method Call', null, null, methodStart, methodEnd);
            requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'getQuoteUW Request Call', null, null, methodEnd, null);
            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));
            logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
        }
        
        catch (Exception e)
        {
            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.ERROR, className, methodName, null, 'Quote', 'getQuoteUW Exception Call', null, e, methodStart, methodEnd);
            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));

            throw e;
        }

        this.logWrappers = logWrappers;

        // When a quote is new, the three premium fields will not be populated.
        if ((quote.Standard_Premium__c == null && quote.Option_1_Premium__c == null && quote.Option_2_Premium__c == null) ||
            (quote.Standard_Premium__c == 0 && quote.Option_1_Premium__c == 0 && quote.Option_2_Premium__c == 0))
        {
            return get3Quote();
        }
        else
        {
            return getQuotes();
        }
    }

    /*
     * This method calls the GET_3_QUOTE service from the ESB. It will return default values for the quote based on the SIC Code and Segment.
     */
    public Object get3Quote()
    {
        RequestInfo ri = new RequestInfo(
            RequestInfo.ServiceType.GET_3_QUOTE,
            RiskService.PACKAGE_STANDARD,
            quote.Case__r.Segment__c
        );

        addContinuationRequest(ri);
        return this.con;
    }

    /*
     * The purpose of this method is to retrieve three separate premiums to improve underwriter efficiency.
     * One limitation to keep in mind is that continuations are limited to a maximum of three callouts.
     * As all three callout slots are utilized by this method, further improvements to increase the number of get rates
     * will need to continue alternative strategies, such as bundling all three rate requests into the same callout via
     * updating the CSIO schema to accomdate a list of request.
     */
    public Object getQuotes()
    {
        /*
         * Callout in this continuation for the RA Get rate.
         */ 
        if (quote.ePolicy__c != null){
            RequestInfo riStandard = new RequestInfo(
                RequestInfo.ServiceType.RAGET_QUOTE,
                RiskService.PACKAGE_STANDARD,
                quote.Case__r.Segment__c
            );
            addContinuationRequest(riStandard);
        }
        else {
            /*
         * First callout in this continuation for the Standard package.
         */ 
            RequestInfo riStandard = new RequestInfo(
                RequestInfo.ServiceType.GET_QUOTE,
                RiskService.PACKAGE_STANDARD,
                quote.Case__r.Segment__c
            );
            addContinuationRequest(riStandard);
        
        /*
         * Second callout in this continuation for the Option 1 package.
         */
        RequestInfo riOption1 = new RequestInfo(
            RequestInfo.ServiceType.GET_QUOTE,
            RiskService.PACKAGE_OPTION_1,
            quote.Case__r.Segment__c
        );
        addContinuationRequest(riOption1);

        /*
         * Third callout in this continuation for the Option 2 package.
         */
        RequestInfo riOption2 = new RequestInfo(
            RequestInfo.ServiceType.GET_QUOTE,
            RiskService.PACKAGE_OPTION_2,
            quote.Case__r.Segment__c
        );
        addContinuationRequest(riOption2);

        }
        // Return the continuation object to the VisualForce page.
        return this.con;
    }

    /*
     * This method calls the UPLOAD_QUOTE service from the ESB. It will persist the quote into ePolicy.
     */
    public Object uploadQuote()
    {
        DateTime methodStart = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper methodLog;
        ApplicationLogWrapper requestLog;
        Map<String, String> logWrappers = new Map<String, String>();
        RequestInfo ri;
        try
        {
            ri = new RequestInfo(
                RequestInfo.ServiceType.UPLOAD_QUOTE,
                quote.Package_Type_Selected__c,
                quote.Case__r.Segment__c
            );
            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'uploadQuote Method Call', null, null, methodStart, methodEnd);
            requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'getQuoteUW Request Call', null, null, methodEnd, null);

            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));
            logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
        }
        catch (Exception e)
        {
            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.ERROR, className, methodName, null, 'Quote', 'uploadQuote Exception Call', null, e, methodStart, methodEnd);
            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));

            throw e;
        }

        this.logWrappers = logWrappers;
        addContinuationRequest(ri);
        return con;
    }

    /*
     * This method calls the FINALIZE_QUOTE service from the ESB. It will persist the quote into ePolicy and set it into a finalized state.
     */
    public Object finalizeQuote(){
        DateTime methodStart = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper methodLog;
        ApplicationLogWrapper requestLog;
        Map<String, String> logWrappers = new Map<String, String>();
        
        try{
            // If logic added for FRZR-123 to stop finalization in UW tool when ePolicy sends messages
            if(quote.isLiveChatIndicator__c){ // FRZR-166
                throw new RSA_ESBException(Label.Stop_Finalization);
            }
            else if (quote.Quote_Number__c != null){
                quote.Status__c = 'Finalized';
                update quote;

                PageReference pageRef = new PageReference('/' + qId);
                pageRef.setRedirect(true);
                return pageRef; //Returns to the quote page
            }

            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.ERROR, className, methodName, null, 'Quote', 'Finalize UW Call', null, null, methodStart, methodEnd);
            requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'getQuoteUW Request Call', null, null, methodEnd, null);

            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));
            logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
        }
        catch (Exception e)
        {
            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.ERROR, className, methodName, null, 'Quote', 'Finalize UW Exception Call', null, e, methodStart, methodEnd);
            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));

            throw e;
        }
        this.logWrappers = logWrappers;

        RequestInfo ri = new RequestInfo(
            RequestInfo.ServiceType.FINALIZE_QUOTE,
            quote.Package_Type_Selected__c,
            quote.Case__r.Segment__c
        );
        
        addContinuationRequest(ri);
        return con;
    }

    /*
     * This method calls the DOWNLOAD_QUOTE service from the ESB.
     */
    public Object downloadQuote()
    {
    	DateTime methodStart = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper methodLog;
        ApplicationLogWrapper requestLog;
        RequestInfo ri;
        Map<String, String> logWrappers = new Map<String, String>();
        try
        {
            ri = new RequestInfo(
                RequestInfo.ServiceType.DOWNLOAD_QUOTE,
                quote.Package_Type_Selected__c,
                quote.Case__r.Segment__c
            );
            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'Download Quote Exception Call', null, null, methodStart, methodEnd);
            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));
            requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'getQuoteUW Request Call', null, null, methodEnd, null);
            logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
        }
        catch (Exception e)
        {
            DateTime methodEnd = DateTime.now();
            methodLog = ApplicationLog.createLogWrapper(ApplicationLog.ERROR, className, methodName, null, 'Quote', 'Download Quote Exception Call', null, e, methodStart, methodEnd);
            logWrappers.put(METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));

            throw e;
        }

        this.logWrappers = logWrappers;
        addContinuationRequest(ri);
        return con;
    }
    
        /*
     * This method calls the LOAD_POLICY service from the ESB. 
     */
    public Object loadQuote()
    {
        this.con.continuationMethod = 'processRAResponse';
        if (quote.Case__r.bkrCase_Subm_Type__c.contains('Amendment')){
            if (quote.Received_Date__c == null){
                throw new RSA_ESBException(Label.Amendment_Received_Date_Validation);
            }
            if(quote.Transaction_Effective_Date__c == null){
                 throw new RSA_ESBException(Label.Amendment_Transaction_Date_Validation); 
            }
        }  
        if((quote.Case__r.bkrCase_Subm_Type__c.contains('Renewal') || quote.Case__r.bkrCase_Subm_Type__c.contains('Renewal Re-visit')) && !quote.Case__r.Resume_Transaction__c && quote.of_days_out_from_renewal__c > 60){
            throw new RSA_ESBException(Label.Not_in_the_Renewal_Period);
        }
        if (quote.Case__r.ePolicy_Transaction_Status__c == 'Pending'){
            RequestInfo ri =  new RequestInfo(
	            RequestInfo.ServiceType.LOAD_POLICY_X
	        );
        this.con.state = ri;
        addContinuationRequest(ri);
        return con;
        } else {
            RequestInfo ri =  new RequestInfo(
                    RequestInfo.ServiceType.LOAD_POLICY
                );
            this.con.state = ri;
            addContinuationRequest(ri);
            return con;
        }
    }
   
       /*
     * This method calls the Finalize service from the ESB for a renewal or amendment. 
     */
    public Object issueQuote()
    {
        
        if(quote.Reasons__c == null && quote.Case__r.bkrCase_Subm_Type__c.contains('Amendment')){
            throw new RSA_ESBException(Label.Reason_Error);
        }
        if(quote.Renewal_Reason_s__c == null && quote.Case__r.bkrCase_Subm_Type__c.contains('Renewal')){
            throw new RSA_ESBException(Label.Renewal_Reason_Error);
        }
            RequestInfo ri =  new RequestInfo(
                RequestInfo.ServiceType.RAFINALIZE_QUOTE,
                RiskService.PACKAGE_STANDARD,
                quote.Case__r.Segment__c
             );

        DateTime methodEnd = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper requestLog;
        requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'finalizeQuote Request Call', null, null, methodEnd, null);
        logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
        addContinuationRequest(ri);
        
        return con;
        
    }
    
           /*
     * This method calls the UW Bind service from the ESB for a renewal or amendment. 
     */
    public Object bindQuote()
    {
            RequestInfo ri =  new RequestInfo(
            RequestInfo.ServiceType.RABIND_QUOTE,
            quote.Package_Type_Selected__c,
            quote.Case__r.Segment__c
                );

        DateTime methodEnd = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper requestLog;
        requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'bindQuote Request Call', null, null, methodEnd, null);
        logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
       
            addContinuationRequest(ri);
  
            return con;
        
    }

    
     /*
     * This method calls the Save
     */
    public Object saveQuote()
    {
        if (this.quote.New_Premium__c == null) {
        	throw new RSA_ESBException(Label.Get_rate_before_save);
        } 
   
        RequestInfo ri =  new RequestInfo(RequestInfo.ServiceType.SAVE_QUOTE,
                                          RiskService.PACKAGE_STANDARD,
                                          quote.Case__r.Segment__c);
        
        DateTime methodEnd = DateTime.now();
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        ApplicationLogWrapper requestLog;
        requestLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, null, 'Quote', 'saveQuote Request Call', null, null, methodEnd, null);
        logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));
        
        addContinuationRequest(ri);
        con.continuationMethod = 'processSaveResponse';

        return con;
    }
    
    
    /**
    Aborts the transaction. Manages its own continuation as needs to return to the case page.
    */
    
    public Object abortTransaction() {
    	RequestInfo.ServiceType serviceType = RequestInfo.ServiceType.ABORT_TRANSACTION;
    	RequestInfo ri = new RequestInfo(serviceType);
    	String comNumber = quote.ePolicy__c;
        this.service = new RSA_ESBService(ri, parentCase.Id);
        String request = service.getRequestXML();
        HttpRequest req = service.getHttpRequest(request);
        abortRequestLabel = this.con.addHttpRequest(req);
        requestMap.put(abortRequestLabel, request);
        ris.put(abortRequestLabel, ri);   
        con.continuationMethod = 'processAbortResponse';
    	return con;
    }
    
    /**
    Processes the response from the abort transaction, which involves navigating back to the case.
    */
    
    public PageReference processAbortResponse() {
        System.debug('#### ' + requestMap.keySet());
        Id caseId = parentCase.Id;        
        HttpResponse response = Continuation.getResponse(abortRequestLabel);
        
        System.debug('#### abort response is ' + response.getBody());
        
        // check that the response is successful
        if (!checkAbortSuccess(response.getBody())) {
        	throw new RSA_ESBException('Unsuccessful response recevied on abort on policy ' + quote.Id);
        }    
        // remove the current session
        System.debug('#### processAbortResponse removing the session against case ' + caseId);

        RA_SessionManagementUtil.removeSession(caseId);
        
        // remove the policy
        System.debug('#### processAbortResponse removing the quote ' + quote.Id);
        delete quote;
        
        // change the case status
        System.debug('#### processAbortResponse updating the status on the parentCase ' +caseId );
        parentCase.status = 'New';
        parentCase.Resume_Transaction__c = false;
        update parentCase;
        // return the user to the case page layout
        System.debug('#### processAbortResponse removing the redirecting back to the case');
        PageReference pageRef = new PageReference('/' + caseId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference processSaveResponse() {
        System.debug('#### ' + requestMap.keySet());
        Id caseId = parentCase.Id;        
        HttpResponse response = Continuation.getResponse(saveRequestLabel);
        RSA_ESBService.insertNote(this.qId, 'Generated SAVE Request', requestMap.get(saveRequestLabel));
        RSA_ESBService.insertNote(this.qId, 'Generated SAVE Response', response.getBody());
        // check that the response is successful
        if (!checkAbortSuccess(response.getBody())) {
            throw new RSA_ESBException('Unsuccessful response received on saving quote ' + quote.Id);
        }    
        // remove the current session
        System.debug('#### processSaveResponse removing the session against case ' + caseId);
        
        RA_SessionManagementUtil.removeSession(caseId);
        
        // return the user to the case page layout
        System.debug('#### processSaveResponse removing the redirecting back to the case');
        PageReference pageRef = new PageReference('/' + caseId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    private boolean checkAbortSuccess(String response) {  	
    // check for errors in response              
    // Without SOAP Envelope
    String responseCode = XMLHelper.retrieveValue(response, ERROR_RESPONSE_CODE_PATH);
    if (responseCode != null && responseCode == STATUS_ERROR) {
        String errorCode = XMLHelper.retrieveValue(response, ERROR_CODE_PATH);
        String errorMessage = XMLHelper.retrieveValue(response, ERROR_MESSAGE_FAILURE_PATH);        
        throw new RSA_ESBException(errorCode + ' - ' + errorMessage);
    }
    // With SOAP Envelope
    responseCode = XMLHelper.retrieveValue(response, ERROR_RESPONSE_CODE_PATH_SOAP);
    if (responseCode != null && responseCode == STATUS_ERROR) {
        String errorCode = XMLHelper.retrieveValue(response, ERROR_CODE_PATH_SOAP);
        String errorMessage = XMLHelper.retrieveValue(response, ERROR_MESSAGE_FAILURE_PATH_SOAP);    
        throw new RSA_ESBException(errorCode + ' - ' + errorMessage);
    }
    // get to the node   
    	Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(response);
        Dom.XMLNode soapNode = xmlDocument.getRootElement().getChildElement('Body', CSIOPolicyXMLProcessor.SOAP_NAMESPACE);
        Dom.XMLNode acordNode = soapNode.getChildElement('ACORD', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode insuranceSvcRsNode = acordNode.getChildElement('InsuranceSvcRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode policySyncRsNode = insuranceSvcRsNode.getChildElement('PolicySyncRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        if (policySyncRsNode == null) {
        	policySyncRsNode = insuranceSvcRsNode.getChildElement('CommlPkgPolicyQuoteInqRs', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        }
        Dom.XMLNode messageStatusNode = policySyncRsNode.getChildElement('MsgStatus', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        Dom.XMLNode messageCode = messageStatusNode.getChildElement('MsgStatusCd', CSIOPolicyXMLProcessor.ACORD_NAMESPACE);
        String success = messageCode.getText().toLowerCase();
        return success == 'success';
    }
        
    /*
     * Takes the RequestInfo object to produce the request and adds it to the continuation server.
     */
    private void addContinuationRequest(RequestInfo ri)
    {
        this.service = new RSA_ESBService(ri, this.qId);
        String request = service.getRequestXML();
        HttpRequest req = service.getHttpRequest(request);
        String requestLabel = this.con.addHttpRequest(req);
        requestMap.put(requestLabel, request);
        ris.put(requestLabel, ri);
        if (ri.svcType == RequestInfo.ServiceType.SAVE_QUOTE) {
            saveRequestLabel = requestLabel;
        }
    }

    /*
     * Processes all the responses to the requests made to the continuation server.
     */
    public PageReference processResponse()
    {
        System.debug('!!! ' + requestMap.keySet());
        for (String requestLabel : requestMap.keySet())
        {
            parseResponse(requestLabel);
        }

        PageReference pageRef = new PageReference('/' + qId);
        pageRef.setRedirect(true);
        return pageRef;//Returns to the quote page
    }

    /*
     * Processes all the RA responses to the requests made to the continuation server.
     */
    public PageReference processRAResponse()
    {
        System.debug('!!! ' + requestMap.keySet());
        for (String requestLabel : requestMap.keySet())
        {
            parseRAResponse(requestLabel);
        }
        
        PageReference pageRef = new PageReference('/' + policyList[0].id);
        pageRef.setRedirect(true);
        policyList[0].Status__c = 'In-Progress';
        policyList[0].Received_Date__c = quote.Received_Date__c;
        policyList[0].Transaction_Effective_Date__c = quote.Transaction_Effective_Date__c;
        parentCase.Status = 'In Progress';
        update policyList[0];
        update parentCase;
        return pageRef;//Returns to the quote page
    }
    
    /*
     * This helper method extracts the response body and sends it to be processed.
     */
    private void parseResponse(String requestLabel)
    {
        String className = FP_AngularPageController.getClassName();
        String methodName = FP_AngularPageController.getMethod();
        DateTime methodStart = DateTime.now();
        Map<String, String> logWrappers = this.logWrappers;
        ApplicationLogWrapper requestLog = ApplicationLog.deserializeLogWrapper(logWrappers.get(REQUEST_LOG));
        DateTime requestStartTime = requestLog.startTime;
        requestLog.endTime = methodStart;
        requestLog.timer = requestLog.endTime.getTime() - requestLog.startTime.getTime();
        logWrappers.put(REQUEST_LOG, ApplicationLog.serializeLogWrapper(requestLog));

        HttpResponse response = Continuation.getResponse(requestLabel);
        RSA_ESBService.insertNote(this.qId, 'Generated Request', requestMap.get(requestLabel)); //Fix this - remove it.
        RSA_ESBService.insertNote(this.qId, 'Generated Response', response.getBody()); //Fix this - remove it.

        try
        {
            service.parseResponseXML(response.getBody());
            DateTime methodEnd = DateTime.now();
            ApplicationLogWrapper methodLog = ApplicationLog.createLogWrapper(ApplicationLog.INFO, className, methodName, quote.Id, 'Quote', 'processResponse method call', response.getBody(), null, methodStart, methodEnd);
            logWrappers.put(RESPONSE_METHOD_LOG, ApplicationLog.serializeLogWrapper(methodLog));
        }
        catch(Exception e)
        {
            RSA_ESBService.insertNote(this.qId, 'Error ' + e.getMessage().substring(0, Math.min(e.getMessage().length(), 70)), e.getMessage() + '\n' + e.getStackTraceString()); //Fix this - remove it.
            if (this.quote.ePolicy__c != null){
                this.quote.Case__r.external_policy_system_error_message__c = e.getMessage();
            }
        }
        finally
        {
            ApplicationLog.logMessage(logWrappers);
        }
    }

    /**
     * Helper function to confirm if the Sic Answer is for a specific SIC Question.
     */
    @testVisible
    private static Boolean isQuestion(SIC_Answer__c sa, String questionCode)
    {
        return sa.Question_Code__r.questionId__c == Decimal.valueOf(questionCode);
    }
    
    /*
     * This helper method extracts the response body and sends it to be processed for RA transactions which use the CSIOPolicyXMLProcessor.
     */
    private void parseRAResponse(String requestLabel)
    {	
        HttpResponse response = Continuation.getResponse(requestLabel);
        String xml = response.getBody() + requestMap.get(requestLabel);
        new CSIOPolicyErrorHandling(response.getBody()).process(); 
        new CSIOPolicyXMLProcessor(parentCase, response.getBody()).process();
        system.debug('response' + response.getBody());
        system.debug('request' + requestMap.get(requestLabel));

        //The policy record will be deleted and a new record will be generated and populated, response and request need to be stored in the new record 
        policyList = [SELECT Id, Case__r.Locked_in_External_Policy_System__c, Last_Federation_Id__c
                                              FROM Quote__c where Case__c = :parentCase.Id];
        
        if (policyList.size() != 0){
                    RSA_ESBService.insertNote(policyList[0].Id, 'Generated LoadPolicy Response', response.getBody());
                    RSA_ESBService.insertNote(policyList[0].Id, 'Generated LoadPolicy Request', requestMap.get(requestLabel));
        }
    }
}