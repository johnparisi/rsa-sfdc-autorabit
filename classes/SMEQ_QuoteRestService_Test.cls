@isTest
private class SMEQ_QuoteRestService_Test
{
	 static testMethod void testSaveQuoteWithOutException()
     {
         User u;
         
         System.runAs(UserService_Test.getNonPortalUserWithRole())
         {
             UserService_Test.setupTestData();
             u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
         }
        
        System.runAs(u)
        {
            SMEQ_TestDataGenerator.getSicCode();
            
            Test.startTest();
            // SMEQ_QuoteRequest quoteRequest = populateQuoteRequestForInsertingData();
            SMEQ_QuoteRequest quoteRequest = populateQuoteRequestForSave();
            
            SMEQ_QuoteResponseModel response = SMEQ_QuoteRestService.saveQuote(quoteRequest);
            String[] testReferralReasons = new List<String>();
            testReferralReasons.add('good');
            response.referralReasons = testReferralReasons;
            SMEQ_QuoteRestService.createResponse(response,'Dummy');
            Test.stopTest();
            
            System.assert(response != null);
            System.debug('response: ' + response);
            System.debug('response QuoteRequest caseNumber: ' + response.quoteRequest.caseNumber);
            System.assert(response.quoteRequest.caseNumber != null);
            System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_OK, response.responseCode);
            System.debug('Reference Number' + response.caseId);
        }
    }

    static testMethod void testSaveQuoteWithException()
    {
    	SMEQ_QuoteRequest quoteRequest = SMEQ_QuoteDao_Test.populateQuoteRequestForInsertingData();
        quoteRequest.businessDetails.canadianRevenue = 2222222222222222222222222.8983333;
        quoteRequest.businessDetails.businessName = 'fffffff';
        SMEQ_QuoteResponseModel response = SMEQ_QuoteRestService.saveQuote(quoteRequest);
        
        Test.startTest();
        System.assert(response != null);
        System.assert(response.caseId == null);
        System.assertEquals(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR, response.responseCode);
        Test.stopTest();
    }
    
    static testMethod void testGenerateDummyQuoteResponse()
    {
    	Test.startTest();
    	SMEQ_QuoteRequest quoteRequest = populateQuoteRequestForInsertingData();
    	SMEQ_QuotePackage qp1 = new SMEQ_QuotePackage();
    	qp1.packageID = 'P1';
    	SMEQ_QuotePackage qp2 = new SMEQ_QuotePackage();
    	qp2.packageID = 'P2';
    	SMEQ_QuotePackage qp3 = new SMEQ_QuotePackage();
    	qp3.packageID = 'P3';
    	List<SMEQ_QuotePackage> qps = new List<SMEQ_QuotePackage>();
    	qps.add(qp1);
    	qps.add(qp2);
    	qps.add(qp3);
    	quoteRequest.packagesRequireRating = qps;
    	
    	SMEQ_QuoteResponseModel response = SMEQ_QuoteRestService.saveQuote(quoteRequest);
    	SMEQ_QuoteRestService.generateDummyQuoteResponse(response,true);
        Test.stopTest();        
    }
    
    private static SMEQ_QuoteRequest populateQuoteRequestForSave()
    {
    	//
    	SIC_Code__c sicCodeObject = SMEQ_TestDataGenerator.getSicCode();
        SMEQ_TestDataGenerator.generateSICQuestions(SMEQ_TestDataGenerator.getSICCodeDetailVersion());
    	
    	//
    	
        SMEQ_QuoteRequest quoteRequest = new SMEQ_QuoteRequest();
        SMEQ_BusinessDetailsModel  businessDetails = new SMEQ_BusinessDetailsModel();
        businessDetails.businessName = 'Test';
        //businessDetails.sicCode = '1234';
        businessDetails.sicCode = sicCodeObject.SIC_Code__c;
        
        SMEQ_AddressModel address = new SMEQ_AddressModel();
        address.addressLine1 = 'Address Line 1';
        address.city = 'Ottawa';
        address.postalCode = 'K2G 6R4';
        address.province = 'Ontario';
        businessDetails.mailingAddress = address;
        businessDetails.noOfClaims = 0;
        businessDetails.noOfClaimFreeYears = 1;
        quoteRequest.businessDetails = businessDetails;
        
        List<SMEQ_QuestionAnswerModel> questionAnswersList = new List<SMEQ_QuestionAnswerModel>();
        SMEQ_QuestionAnswerModel cnRevenue = new SMEQ_QuestionAnswerModel();
        cnRevenue.questionCode = SicQuestionService.CODE_CANADIAN_REVENUE;
        cnRevenue.answer = '1000';
        questionAnswersList.add(cnRevenue);
        businessDetails.questionAnswers = questionAnswersList;

        return quoteRequest;
    }

    private static SMEQ_QuoteRequest populateQuoteRequestForInsertingData()
    {
        SMEQ_QuoteRequest quoteRequest = new SMEQ_QuoteRequest();
        SMEQ_BusinessDetailsModel  businessDetails = new SMEQ_BusinessDetailsModel();
        businessDetails.businessName = 'Live Chat';
        SMEQ_AddressModel address = new SMEQ_AddressModel();
        address.addressLine1 = 'Address Line 1';
        address.city = 'Ottawa';
        address.postalCode = 'K2G 6R4';
        address.province = 'Ontario';
        businessDetails.mailingAddress = address;
        businessDetails.canadianRevenue = 10000;
        businessDetails.foreignRevenue = 2000;
        businessDetails.noOfClaims = 6;
        quoteRequest.businessDetails = businessDetails;
        quoteRequest.finalizedDate = '2016-11-01';
        quoteRequest.quoteNumber = 'SME-1234';
        quoteRequest.selectedAnnualPremium = 1000;
        quoteRequest.selectedAirMilesEarned = 400;
        quoteRequest.policyEffectiveDate = '2016-12-02';
        quoteRequest.issuedDate = '2016-11-15';
        
        return quoteRequest;
    }

	
}