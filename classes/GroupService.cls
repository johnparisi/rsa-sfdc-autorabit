/*
GroupService
Author: Stephen Piercey
Date: Feb. 13, 2017
Description: Used to query the Group object and store results in static maps.  
*/
public with sharing class GroupService {

	public static Set<Id> centralQueueIds;
	public static Set<Id> pacificQueueIds;
	
	public static Set<Id> getCentralQueueIds(){
		if (GroupService.centralQueueIds == null){
			GroupService.centralQueueIds = new Map<Id, Group>([SELECT Id, Name 
													FROM Group 
													WHERE (Name LIKE 'Central%') 
													AND Type = 'Queue']).keySet();
		}

		return GroupService.centralQueueIds;
	}

	public static Set<Id> getPacificQueueIds(){
		if (GroupService.pacificQueueIds == null){
			GroupService.pacificQueueIds = new Map<Id, Group>([SELECT Id, Name 
													FROM Group 
													WHERE ((Name LIKE 'Pacific%') 
													AND ((NOT Name LIKE '%Retention%') AND (NOT Name LIKE '%Processing%') AND (NOT Name LIKE '%Portfolio%')))
													AND Type = 'Queue']).keySet();
		}

		return GroupService.pacificQueueIds;
	}
}