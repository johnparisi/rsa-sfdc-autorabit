@isTest
public class SMEQ_UserProfileDao_Test {

    public static String TEST_LASTNAME = 'UNITTEST_LASTNAME';
    public static String TEST_FIRSTNAME = 'UNITTEST_FIRSTNAME';
    public static String TEST_PHONE = '165645543';
    public static String TEST_EMAIL = 'email@emailaccount.com';
    public static String TEST_ACCOUNT_NAME = '1UNITTEST_ACCOUNT';
    public static String TEST_PARENT_ACCOUNT_NAME = 'PARENT_UNITTEST_ACCOUNT';
    public static String TEST_GRANDPARENT_ACCOUNT_NAME = 'GRANDPARENT_UNITTEST_ACCOUNT';

    /**
     * HELPER METHOD ONLY : Setup test data
     */
    @testSetup
    static void contactAccountSetup() {
        System.debug('testSetup CALLED  .....');
        User currentUser = [Select UserRoleId, Id from User where Id=:UserInfo.getUserId()];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        currentUser.UserRoleId = portalRole.Id;
        System.runAs(new User(Id = Userinfo.getUserId())){
            update currentUser;
        }
        
        Account testChildAccount = new Account(name = TEST_ACCOUNT_NAME);
        insert testChildAccount;

        Contact testContact = new Contact(LastName = TEST_LASTNAME,
                FirstName = TEST_FIRSTNAME,
                Phone = TEST_PHONE,
                email = TEST_EMAIL,
                AccountId = testChildAccount.Id);
        insert testContact;
    }

    /**
     * HELPER METHOD ONLY : Return top most Parent Account
     */
    private static Account setupHierarchyRecords(Account childAccount) {
        // setup parent account
        Account testGrandParentAccount = new Account(name = TEST_GRANDPARENT_ACCOUNT_NAME);
        insert testGrandParentAccount;
        Account testParentAccount = new Account(name = TEST_PARENT_ACCOUNT_NAME, ParentId = testGrandParentAccount.Id);
        insert testParentAccount;
        childAccount.ParentId = testParentAccount.id;

        update childAccount;
        return childAccount;
    }


    /**
     * Run test as System user , No Contact , should see error
     */
    static testMethod void testUserProfileNoContact() {
        SMEQ_UserProfileDataModel model = null;
        try {
            SMEQ_UserProfileDao dao = new SMEQ_UserProfileDao();
            model = dao.getSessionUserProfile();
        } catch (Exception e) {
            // expected
            System.assert(model == null);
            System.Assert(e instanceof SMEQ_ServiceException);
        }
    }

   /**
    * Run User Profile Service as a valid SME broker profile user - should pass
    * Exercises that it correctly populates the UserProfile for return
    *
    * Assertions:
    * 1. The generated User profile model contactId has to match Contact.contactId used to call DAO
    * 2. The generated User profile model ownBrokerage. accountName has to match Account used to call DAO  has to match Account used to call DAO
    * 3. The generated User profile model email has to match Contact.email used to call DAO
    * 4. The generated User profile model phone has to match Contact.phone used to call DAO
    * 5. The generated User profile model firstName has to match Contact.firstName used to call DAO
    * 6. The generated User profile model lastName has to match Contact.lastName used to call DAO
    */
    static testMethod void testUserProfile() {
        Id tContactId = null;
        Id tBrokerProfileId = null;
        Id tUserId = null;
        Id tAccountId = null;

        List<Account> accounts = [Select Id from Account where name = :TEST_ACCOUNT_NAME];
        tAccountId = accounts[0].Id;
        List<Profile> profiles = [select id from profile where name = 'SME Broker'];
        tBrokerProfileId = profiles[0].Id;
        List<Contact> contacts = [Select Id from Contact where LastName = :TEST_LASTNAME];
        tContactId = contacts[0].Id;

        Contact theContact = contacts[0];
        User tUser = setupUser(tBrokerProfileId, tContactId);
        System.runAs(tUser) {
            SMEQ_UserProfileDao dao = new SMEQ_UserProfileDao();
            SMEQ_UserProfileDataModel model = dao.getSessionUserProfile();
            System.assert(model.contactId == tContactId);
            System.assert(model.ownBrokerage.accountName == TEST_ACCOUNT_NAME);
            System.assert(model.ownBrokerage.accountId == tAccountId);
            System.assert(model.email == TEST_EMAIL);
            System.assert(model.phone == TEST_PHONE);
            System.assert(model.firstName == TEST_FIRSTNAME);
            System.assert(model.lastName == TEST_LASTNAME);
        }
    }

    /**
    * Run User Profile Service as a valid SME broker profile user - should pass
    * Exercise DAO so it correctly populates the UserProfile for return
    * includes specific check for hierarchy population
    * Assertions:
    * 1. The generated User profile model contactId has to match Contact.contactId used to call DAO
    * 2. The generated User profile model ownBrokerage. accountName has to match Account used to call DAO  has to match Account used to call DAO
    * 3. The generated User profile model email has to match Contact.email used to call DAO
    * 4. The generated User profile model phone has to match Contact.phone used to call DAO
    * 5. The generated User profile model firstName has to match Contact.firstName used to call DAO
    * 6. The generated User profile model lastName has to match Contact.lastName used to call DAO
    * 7. The generated User profile model broker hierarchy matches the Account & Parent Account hierarchy for the user calling the service
    */
    @istest
    static void testHierarchy() {
        List<Account> accounts = [Select Id from Account where name = :TEST_ACCOUNT_NAME];
        system.debug('account: ' + accounts[0] + ' size: ' + accounts.size());
        Account testChildAccount = setupHierarchyRecords(accounts[0]);
        system.debug('again account: ' + accounts[0] + ' size: ' + accounts.size() + ' parent: ' + accounts[0].parent.id);
        Id tContactId = null;
        Id tBrokerProfileId = null;
        Id tUserId = null;
        Id tAccountId = null;

        //tAccountId = accounts[0].Id;
        List<Profile> profiles = [select id from profile where name = 'SME Broker'];
        tBrokerProfileId = profiles[0].Id;
        List<Contact> contacts = [Select Id from Contact where LastName = :TEST_LASTNAME];
        tContactId = contacts[0].Id;
        System.debug('xxx created contacts: Id' + tContactId);

        Contact theContact = contacts[0];
        User tUser = setupUser(tBrokerProfileId, tContactId);

        List<Account> allAccountsCurrent = [Select Id, Name from Account];
        System.debug('all accounts Current: ' + allAccountsCurrent.size());
        
        System.runAs(tUser) {
            List<Account> accounts2 = [Select Id, Name, parentId from Account where name = :TEST_ACCOUNT_NAME];
            system.debug('here2: accounts2[0].parentId: '+accounts2[0].parentId);
            tAccountId = accounts2[0].Id;
            system.debug('Account in tUser2: ' + accounts2[0] + ' size: ' + accounts2.size() + ' parent: ' + accounts2[0].parentid);
            
            System.debug('Here user: ' + tUser.email + ' contact: ' + tUser.ContactId);
            SMEQ_UserProfileDao dao = new SMEQ_UserProfileDao();
            SMEQ_UserProfileDataModel model = dao.getSessionUserProfile();
            System.debug('finished getSession');
            System.assert(model.contactId == tContactId);
            System.assert(model.ownBrokerage.accountName == TEST_ACCOUNT_NAME);
            System.assert(model.ownBrokerage.accountId == tAccountId);
            System.assert(model.email == TEST_EMAIL);
            System.assert(model.phone == TEST_PHONE);
            System.assert(model.firstName == TEST_FIRSTNAME);
            System.assert(model.lastName == TEST_LASTNAME);

            // compare model.
            SMEQ_UserProfileDataModel.BrokerAccount grandParentAccount = model.brokerageHierarchy;
            SMEQ_UserProfileDataModel.BrokerAccount parentAccount = model.brokerageHierarchy;
            SMEQ_UserProfileDataModel.BrokerAccount childAccount = model.brokerageHierarchy;
                    
            /*
             * grandParentAccount, parentAccount and childAccount are all grandParentAccount based on the class assignment.
             * tUser won't be able to access parent and grand parents. As a result, the following assertion will not work
            System.assert(testChildAccount.Id == childAccount.accountId);
            System.assert(testChildAccount.Name == childAccount.accountName);
            System.assert(testChildAccount.ParentId == parentAccount.accountId);
            System.assert(testChildAccount.Parent.Name == parentAccount.accountName);
            */
            
        }
    }

    public static User setupUser(Id profileId, Id contactId) {
        //UserRole portalRole = new UserRole();
        User testUser = new User(alias = 'test123', email = TEST_EMAIL, Phone = TEST_PHONE, emailencodingkey = 'UTF-8',
                //UserRoleId = portalRole.Id,
                lastname = 'Testing', languagelocalekey = 'en_US',
                ProfileId = profileId,
                country = 'United States', IsActive = true,
                ContactId = contactId,
                LocaleSidKey = 'en_CA',
                Primary_Region__c = 'Pacific',
                Offering_Project_for_Brokers__c = 'SPRNT v1',
                timezonesidkey = 'America/Los_Angeles', username = 'UNITTEST@USER_NAME.com');
        System.runAs(new User(Id = Userinfo.getUserId())){
            insert testUser;
        }        
        return testUser;
    }
}