@RestResource(urlMapping='/saveQuote')
global with sharing class SMEQ_QuoteRestService
{
    private static final String ACCOUNT_RECORDTYPE_FOR_QUOTE = 'Policyholder';
    private static final String CASE_RECORDTYPE_FOR_QUOTE  = 'CI_New_Business'; 
    
    @HttpPost
    global static SMEQ_QuoteResponseModel saveQuote(SMEQ_QuoteRequest quoteRequest)
    {
        RestResponse res = RestContext.response;
        if(res!=null){
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
            System.debug('Hitting Service  >>>>>>>' + quoteRequest);
        }
        SMEQ_QuoteResponseModel response = new SMEQ_QuoteResponseModel();
        SMEQ_QuoteDao quoteDao = new SMEQ_QuoteDao();
        try{
            quoteDao.saveQuoteRequest(quoteRequest);
            response.quoteRequest = quoteRequest;
        }
        catch(SMEQ_ServiceException se){
            System.debug('Exception***************:' + se );
            response.setResponseCode(SMEQ_RESTResponseModel.RESPONSE_CODE_ERROR);
            List<SMEQ_RESTResponseModel.ResponseError> errors = new List<SMEQ_RESTResponseModel.ResponseError>();
            SMEQ_RESTResponseModel.ResponseError error
                        = new SMEQ_RESTResponseModel.ResponseError(SMEQ_RESTResponseModel.ERROR_CODE_699, 'Unexpected System Exception');
            //error.detailErrorMessage = se.toString();
            errors.add(error);
            response.setErrors(errors);
        }
        System.debug('Hitting Response >>>>>>>' + response);
        Boolean setLiveChat = false;
        if(quoteRequest.businessDetails.businessName!=null && quoteRequest.businessDetails.businessName.contains('Live Chat')){
            setLiveChat = true;
        }
        response.quoteRequest = quoteRequest;
//        SMEQ_QuoteRestService.generateDummyQuoteResponse(response, setLiveChat);
        return response;
    }

    public static void generateDummyQuoteResponse(SMEQ_QuoteResponseModel response, Boolean setDummyliveChat){
        //response.caseId = '1234567890';
       // SMEQ_QuoteRequest quoteRequest = new SMEQ_QuoteRequest();
      // response.quoteRequest.caseId = '1234567890';
      //response.quoteRequest.caseId = '50063000002vGwVAAU';
      //response.quoteRequest.quoteID = 'a0p630000041KzNAAU';
      //response.quoteRequest.insuredClientID = '0016300000CttMeAAJ';
        List<SMEQ_QuotePackage> packages = new List<SMEQ_QuotePackage>();
        List<SMEQ_QuotePackage> quotePackageList = response.quoteRequest.packagesRequireRating;
        for(SMEQ_QuotePackage quotePackage:quotePackageList){
            if(quotePackage.packageID.equals('P1')){
                quotePackage.annualPremium = 2456;
            }
            else if(quotePackage.packageID.equals('P2')){
                quotePackage.annualPremium = 3456;
            }
            else{
                quotePackage.annualPremium = 4567;
            }
            
            quotePackage.airMilesEarned = (quotePackage.annualPremium * 0.05).intValue();
        }

       /* SMEQ_QuotePackage quotePackage1 = new SMEQ_QuotePackage();
        quotePackage1.packageID = 'P1';
        quotePackage1.annualPremium = 2345;
        quotePackage1.airMilesEarned = (quotePackage1.annualPremium * 0.05).intValue();
        packages.add(quotePackage1);
        SMEQ_QuotePackage quotePackage2 = new SMEQ_QuotePackage();
        quotePackage2.packageID = 'P2';
        quotePackage2.annualPremium = 3456;
        quotePackage2.airMilesEarned = (quotePackage2.annualPremium * 0.05).intValue();
        packages.add(quotePackage2);

        SMEQ_QuotePackage quotePackage3 = new SMEQ_QuotePackage();
        quotePackage3.packageID = 'P3';
        quotePackage3.annualPremium = 4867;
        quotePackage3.airMilesEarned = (quotePackage3.annualPremium * 0.05).intValue();
        packages.add(quotePackage3);
        response.quoteRequest.packagesRequireRating = packages;*/

        if(setDummyliveChat){
            response.quoteRequest.liveChatRequired = true;
        }
    }
    
    @TestVisible 
    private static void createResponse(SMEQ_QuoteResponseModel response, String caseId){
        response.caseId = caseId;
    }
}