@isTest
private class SMEQ_SICCodesDao_Test
{
    /**
     * Testing to get all SIC codes that belong to only SME application.
     * Creating four SIC codes that are general without SME as application another
     * four as SME specific application. The sicCodesDao should return 4 records
     * when getAllSICCodes method in DAO is called. Also make sure that the English
     * and French values match
     * Assertions:
     * 1: Make sure we get back a list
     * 2: Make sure that we get back 4 records in the results
     * 3: Make sure that the description for English and French match
     */
    public testMethod static void testGetAllSICCodes()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_UnitTestData.createSICCodeDetailData('0736');
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            List<SMEQ_SICDataModel> sicCodesDaoList = sicCodesDao.getAllSICCodes();
            System.Test.startTest();
            for(SMEQ_SICDataModel sicDataModel:sicCodesDaoList)
            {
                System.assertEquals('Short Desc En', sicDataModel.shortDescription_EN);
                System.assertEquals('Short Desc Fr', sicDataModel.shortDescription_FR);
            }
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing to getSIC codes details that has only terms and conditions
     * Input SIC code 0736
     * Assertions:
     * 1. Sic data is not null
     * 2. The terms and conditions are not null
     * 3. There are 3 terms and conditions associated with the SIC code.
     */
    public testMethod static void testGetSICDetailWithOnlyTermsAndConditions()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            SMEQ_UnitTestData.createSICCodeDetailData('0736');
            SMEQ_SICDataModel sicData = sicCodesDao.getSICDetailVersion('0736');
            System.Test.startTest();
            System.assert(sicData != null);
            System.assert(sicData.termsAndConditions != null);
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing to getSIC codes details that has only terms and conditions
     * Input SIC code 1755A
     * Assertions:
     * 1. Sic data is not null
     * 2. The terms and conditions are not null
     * 3. There are 3 terms and conditions associated with the SIC code.
     * 4. The questions is not null
     * 5. There is 1 question associated with the SIC code
     * 6. There are affirmative conditions with the question
     * 7. There are 3 affirmative conditions with the question.
     */
    public testMethod static void testGetSICDetailWithQuestionsAndAffirmativeConditions()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<SIC_Code_Detail_Version__c> listSicCodeD = SMEQ_UnitTestData.createSICCodeDetailData('1755A');
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            SMEQ_SICDataModel sicData = sicCodesDao.getSICDetailVersion('1755A');
            System.Test.startTest();
            System.assert(sicData != null);
            System.assert(sicData.termsAndConditions != null);
            System.assertEquals(1, sicData.termsAndConditions.size());
            //Will investigate after this deploy to UAT as its environment specific
            System.assertEquals(1, sicData.termsAndConditions.size());
            System.assert(sicData.questions != null);
            System.assertEquals(1, sicData.questions.size());
            //createSIC_Code_Question_Affirmative_Condition was comment out in SMEQ_UnitTestData, so no affirmativeConditions will be created
            System.assertEquals(null, sicData.questions.get(0).affirmativeConditions);
            System.Test.stopTest();
        }
    }
    
    /*
     * Testing the category and subcategory fields.
     * Input SIC code 1755A
     * 
     * Assertions:
     * 1. SIC data is not null
     * 2. Category is null
     * 3. Subcategory is null
     */
    public testMethod static void testGetWithoutCategories()
    {
        String TEST_SIC_CODE = '1755A';
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<SIC_Code_Detail_Version__c> listSicCodeD = SMEQ_UnitTestData.createSICCodeDetailData(TEST_SIC_CODE);
            SMEQ_UnitTestData.removeSICCodeCategory(TEST_SIC_CODE);
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            SMEQ_SICDataModel sicData = sicCodesDao.getSICDetailVersion(TEST_SIC_CODE);
            System.Test.startTest();
            System.assertNotEquals(null, sicData);
            System.assertEquals(null, sicData.category);
            System.assertEquals(null, sicData.subCategory);
            System.Test.stopTest();
        }
    }
    
    /*
     * Testing the category and subcategory fields.
     * Input SIC code 1755A
     * 
     * Assertions:
     * 1. SIC data is not null
     * 2. Category is not null
     * 3. Subcategory is not null
     */
    public testMethod static void testGetCategories()
    {
        String TEST_SIC_CODE = '1755A';
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<SIC_Code_Detail_Version__c> listSicCodeD = SMEQ_UnitTestData.createSICCodeDetailData(TEST_SIC_CODE);
            
            SIC_Code_Detail_Version__c scdv = [
                SELECT id, category__c, subcategory__c
                FROM SIC_Code_Detail_Version__c
                WHERE sic_code__r.sic_code__c = :TEST_SIC_CODE
            ];
            
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            SMEQ_SICDataModel sicData = sicCodesDao.getSICDetailVersion(TEST_SIC_CODE);
            System.Test.startTest();
            System.assertNotEquals(null, sicData);
            System.assertEquals(scdv.category__c, sicData.category);
            System.assertEquals(scdv.subcategory__c, sicData.subCategory);
            System.Test.stopTest();
        }
    }
    
    /**
     * This is to test if the isComplexSIC Flag to true or false based on the SIC. The flag
     * has to be set to true if the referral value is LEVEL 2 or LELVEL 3
     * Assertions:
     * 1. The isComplexSIC has to be true if the referral is LEVEL 2
     * 2. The isComplexSIC has to be true if the referral is LEVEL 3
     * 3. The isComplexSIC has to be false if the referral is LEVEL 1
     */
    public testMethod static void testUpdateReferralStateFlagsForSicDataModelForComplexSICCode()
    {
        SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
        sicDataModel.referral = 'LEVEL 2';
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        System.Test.startTest();
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.isComplexSIC);
        sicDataModel.referral = 'LEVEL 3';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.isComplexSIC);
        sicDataModel.referral = 'LEVEL 1';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(false,sicDataModel.isComplexSIC);
        System.Test.stopTest();
    }
    
    /**
     * This is to test if the isNormalSIC Flag to true or false based on the SIC. The flag
     * has to be set to true if the referral null, blank, or LEVEL 1 and has to be false
     * for LEVEL 2 or LELVEL 3
     * Assertions:
     * 1. The isNormalSIC has to be true when the referral is null
     * 2. The isNormalSIC has to be true when the referral is blank
     * 3. The isNormalSIC has to be true when the referral is LEVEL 1
     * 4. The isNormalSIC has to be false when the referral is LEVEL 2
     * 5. The isNormalSIC has to be false when the referral is LEVEL 3
     * 6. The isNormalSIC has to be false when the referral is DECLINE
     */
    public testMethod static void testUpdateReferralStateFlagsForSicDataModelForNormalSICCode()
    {
        SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
        sicDataModel.referral = null;
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        System.Test.startTest();
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.isNormalSIC);
        sicDataModel.referral = '';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.isNormalSIC);
        sicDataModel.referral = 'LEVEL 1';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.isNormalSIC);
        
        sicDataModel.referral = 'LEVEL 2';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(false,sicDataModel.isNormalSIC);
        sicDataModel.referral = 'LEVEL 3';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(false,sicDataModel.isNormalSIC);
        sicDataModel.referral = 'DECLINE';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(false,sicDataModel.isNormalSIC);
        System.Test.stopTest();
    }
    
    /**
     * This is to test if the hasAppetite Flag to true or false based on the SIC. The flag has to be
     * set to true if the referral null, blank, LEVLE 1, LEVEl2 or LEVEL 3 and has to be false for DECLINE
     * Assertions:
     * 1. The hasAppetite has to be true when the referral is null
     * 2. The hasAppetite has to be true when the referral is blank
     * 3. The hasAppetite has to be true when the referral is LEVEL 1
     * 4. The hasAppetite has to be true when the referral is LEVEL 2
     * 5. The hasAppetite has to be true when the referral is LEVEL 3
     * 6. The hasAppetite has to be false when the referral is DECLINE
     */
    public testMethod static void testUpdateReferralStateFlagsForSicDataModelForHasAppetiteSICCode()
    {
        SMEQ_SICDataModel sicDataModel = new SMEQ_SICDataModel();
        sicDataModel.referral = null;
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.Test.startTest();
        System.assertEquals(true,sicDataModel.hasAppetite);
        sicDataModel.referral = '';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.hasAppetite);
        sicDataModel.referral = 'LEVEL 1';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.hasAppetite);
        
        sicDataModel.referral = 'LEVEL 2';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.hasAppetite);
        sicDataModel.referral = 'LEVEL 3';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(true,sicDataModel.hasAppetite);
        sicDataModel.referral = 'DECLINE';
        sicCodesDao.updateReferralStateFlagsForSicDataModel(sicDataModel);
        System.assertEquals(false,sicDataModel.hasAppetite);
        System.Test.stopTest();
    }
    
    
    /**
     * Testing the Converting the SICCodeDetails objects to custom SICDataModel objects.
     * The following minimum fields if peresnt in the SIC_Code_Detail should be avaialble 
     * in the domain object as well and match the same.
     */
    public testMethod static void testConvertSicCodeDetailListToSICDataModel()
    {
        List<SIC_Code_Detail_Version__c> sicCodeDetailList = new List<SIC_Code_Detail_Version__c>();
        SIC_Code_Detail_Version__c sicCodeDetail = new SIC_Code_Detail_Version__c();
        sicCodeDetail.Short_Description_En__c = 'Test EN';
        sicCodeDetail.Short_Description_Fr__c = 'Test FR';
        sicCodeDetailList.add(sicCodeDetail);
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        List<SMEQ_SICDataModel> sicDataModelList = sicCodesDao.convertSicCodeDetailListToSICDataModel(sicCodeDetailList);
        SMEQ_SICDataModel sicDataModel = sicDataModelList.get(0);
        System.Test.startTest();
        System.assertEquals('Test EN', sicDataModel.shortDescription_EN);
        System.assertEquals('Test FR', sicDataModel.shortDescription_FR);
        System.Test.stopTest();
    }
    
    /**
     * Testing to convert Affirmative Conditions to domain models and associating them with the questions
     * Using SIC Code 1755A for testing the conditions
     * Assertions:
     * 1. There is a question associated with the sic code 1755A
     * 2. There is only one question associated with the SICCode 1755A
     * 3. There are affirmation conditions associated with the question
     * 4. There are three affirmative conditions associated with the question.
     */
    public testMethod static void testconvertSICCodeQuestionAffirmativeConditionToAffirmativeConditionDomainAndAddToQuestionDomain()
    {
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            Sic_Code__c sc = SMEQ_TestDataGenerator.getSicCode();
            List<SIC_Code_Question_Association__c> sqas = SMEQ_UnitTestData.createSIC_Code_Question_AssociationData(sc.SIC_Code__c);
            
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            List<SIC_Code_Question_Association__c> sicCodeQuestionList = sicCodesDao.getQuestionsAssociatedWithSICCode(sc.SIC_Code__c);
            Map<String, SMEQ_SICDataModel.Question> questionModelMap = new Map<String, SMEQ_SICDataModel.Question>();
            List<SMEQ_SICDataModel.Question> questionsList = sicCodesDao.convertSicCodeQuestionToQuestionModel(sicCodeQuestionList, questionModelMap);
            
            List<SIC_Question__c> questionObjectList = [
                SELECT Id
                FROM SIC_Question__c
                LIMIT 1
            ];
            
            SMEQ_UnitTestData.createSIC_Code_Question_Affirmative_ConditionWithQuesions(questionObjectList);
            //Get the Affirmative Conditions for the Questions for the SIC Code
            //Build the list of questionId that we need to get affermative conditions
            List<String> questionIDList = new List<String>();
            for (SIC_Code_Question_Association__c sicCodeAssociation : sicCodeQuestionList)
            {
                questionIDList.add(sicCodeAssociation.questionId__c);
            }
            List<SIC_Code_Question_Affirmative_Condition__c> affirmativeConditionsList = sicCodesDao.getAffirmativeConditionsForAllQuestionsAssiatedWithSicCode(questionIDList);
            sicCodesDao.convertSICCodeQuestionAffirmativeConditionToAffirmativeConditionDomainAndAddToQuestionDomain(affirmativeConditionsList, questionModelMap);
            System.Test.startTest();
            System.assertNotEquals(null, questionsList);
            System.assertEquals(1, questionsList.size());
            System.assertNotEquals(null, questionsList.get(0).affirmativeConditions);
            System.assertEquals(1, questionsList.get(0).affirmativeConditions.size());
            System.Test.stopTest();
        }
    }
    
    /**
     * Testing to get all affirmative conditions associated with the questions. The following question ID are used
     * for testing 1) a0P63000000tLe7EAE 2) a0P63000000tS3WEAU
     * Assertions:
     * 1. AfirmativeConditionList should not be null
     * 2. The number of records returned should be 5
     */
    public testMethod static void testGetAffirmativeConditionsForAllQuestionsAssiatedWithSicCode()
    {
        List<SIC_Code_Question_Affirmative_Condition__c> listSCQAC = SMEQ_UnitTestData.createSIC_Code_Question_Affirmative_Condition();
        List<String> questionIDList = new List<String>();
        questionIDList.add(listSCQAC.get(0).questionId__c);
        SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
        List<SIC_Code_Question_Affirmative_Condition__c> affirmativeConditionList = sicCodesDao.getAffirmativeConditionsForAllQuestionsAssiatedWithSicCode(questionIDList);
        System.Test.startTest();
        System.assert(affirmativeConditionList != null);
        System.assertEquals(1, affirmativeConditionList.size());
        System.Test.stopTest();
    }

    static void setupTestData()
    {
        Map<String,Id> AccountTypes = Utils.getRecordTypeIdsByDeveloperName(Account.SObjectType, true);
        Map<String,Id> ContactTypes = Utils.getRecordTypeIdsByDeveloperName(Contact.SObjectType, true);

        Account a = new Account();
        a.RecordTypeId = AccountTypes.get('bkrAccount_RecordTypeBkrMM');
        a.name = 'TEST';
        a.bkrAccount_Status__c = 'Active';
        insert a;
        a.isPartner = true;
        update a;
        a = [
                SELECT id, recordTypeId, bkrAccount_Status__c
                FROM Account
                WHERE id = :a.Id
        ];
        System.debug('!!! ' + a);

        Contact c = new Contact();
        c.RecordTypeId = ContactTypes.get('bkrContact_BrokerRecordType');
        c.AccountId = a.Id;
        c.LastName = 'TEST';
        c.Email = 'test@test.com';
        c.bkrContact_Status__c = 'Active';
        insert c;
        c = [
                SELECT id, recordTypeId, bkrContact_Status__c
                FROM Contact
                WHERE id = :c.Id
        ];
        System.debug('!!! ' + c);
    }

    private static User getNonPortalUserWithRole()
    {
        UserRole nonPortalRole = [
                Select Id
                FROM UserRole
                Where PortalType = 'None'
                Limit 1
        ];
        System.debug('UserRole is ' + nonPortalRole);

        Profile adminProfile = [
                SELECT Id
                FROM Profile
                WHERE name = 'System Administrator'
        ];
        User portalAccountOwner = new User(
                UserRoleId = nonPortalRole.Id,
                ProfileId = adminProfile.Id,
                Username = System.now().millisecond() + 'test2@rsatest.com',
                Alias = 'batman',
                Email='bruce.wayne@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayne',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner;

        return portalAccountOwner;
    }
    
    /**
     * FP-720
     * Testing for the filtering of SIC codes when the Application Enabled field is false.
     * Assertions:
     * 1. SIC data is not null.
     * 2. SIC code is not filtered out.
     */
    public testMethod static void testFilterForEnabledApplicationEnabledTrue()
    {
        String TEST_SIC_CODE = '1755A';
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<SIC_Code_Detail_Version__c> listSicCodeD = SMEQ_UnitTestData.createSICCodeDetailData(TEST_SIC_CODE);
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            List<SMEQ_SICDataModel> sicData = sicCodesDao.getAllSICCodes();
            System.Test.startTest();
            System.assertNotEquals(null, sicData);
            System.assertEquals(1, sicData.size());
            System.Test.stopTest();
        }
    }
    
    /**
     * FP-720
     * Testing for the filtering of SIC codes when the Application Enabled field is false.
     * Assertions:
     * 1. SIC data is not null.
     * 2. SIC code is filtered out.
     */
    public testMethod static void testFilterForEnabledApplicationEnabledFalse()
    {
        String TEST_SIC_CODE = '1755A';
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<SIC_Code_Detail_Version__c> listSicCodeD = SMEQ_UnitTestData.createSICCodeDetailData(TEST_SIC_CODE);
            SMEQ_UnitTestData.removeApplicationEnabled(TEST_SIC_CODE);
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            List<SMEQ_SICDataModel> sicData = sicCodesDao.getAllSICCodes();
            System.Test.startTest();
            System.assertNotEquals(null, sicData);
            System.assertEquals(1, sicData.size());
            System.Test.stopTest();
        }
    }
    
    /**
     * FP-5790
     * Testing for the decline of SIC codes when the Monoline field is true.
     * Assertions:
     * 1. SIC data is not null.
     * 2. SIC code is declined.
     */
    public testMethod static void testDeclinedForMonolineTrue()
    {
        String TEST_SIC_CODE = '1755A';
        User u;
        
        System.runAs(UserService_Test.getNonPortalUserWithRole())
        {
            UserService_Test.setupTestData();
            u = UserService_Test.getPortalUser(UserService_Test.OFFERING_1, UserService_Test.REGION_1);
        }
        
        System.runAs(u)
        {
            List<SIC_Code_Detail_Version__c> listSicCodeD = SMEQ_UnitTestData.createSICCodeDetailData(TEST_SIC_CODE);
            SMEQ_UnitTestData.applyMonolineTrue(TEST_SIC_CODE);
            SMEQ_SICCodesDao sicCodesDao = new SMEQ_SICCodesDao();
            List<SMEQ_SICDataModel> sicData = sicCodesDao.getAllSICCodes();
            System.Test.startTest();
            System.assertNotEquals(null, sicData);
            System.assertEquals(1, sicData.size());
            System.assertEquals('DECLINED', sicData[0].referral);
            System.Test.stopTest();
        }
    }

}