/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RA_IssueTest extends RA_AbstractTransactionTest {

	@testSetup static void testSetup() {
		RA_AbstractTransactionTest.setup();
	}
    
    /**
    Test the entire first XML document
    */
    
    static testMethod void testLoad() {
        User uw = UserService_Test.getUwUser(UserService_Test.UW_LEVEL1, UserService_Test.REGION_1, UserService_Test.OFFERING_1);
        System.runAs(uw)
        {
		// find the case (shouldn't need to do this !)  
        Case parentCase = [SELECT Id, Com__c, bkrCase_Subm_Type__c, bkrCase_Region__c, Offering_Project__c, Segment__c from Case][0];
        // process the XML policy document
        CSIOPolicyXMLProcessor csioXMLProcessor = new CSIOPolicyXMLProcessor(parentCase, RA_AbstractTransactionTest.xmlPolicy);
        csioXMLProcessor.process();
        
        
        
        // test that we can find the document in the database
        Quote__c policy = [SELECT Id, Renewal_Reason_s__c, ePolicy__c, Air_Miles_Loyalty_Points_Standard__c, Case__r.segment__C, Total_Revenue__c, Standard_Premium__c, Claim_Free_Years__c, Case__c  from Quote__c WHERE case__c = :parentCase.id][0];
        parentCase.COM__c = policy.ePolicy__c;
        policy.Renewal_Reason_s__c = 'Flowthrough';
        policy.Package_Type_Selected__c = RiskService.PACKAGE_STANDARD;
        update parentCase;
        update policy;
        System.assert(policy !=null , 'Policy not found');
        System.assert(policy.Case__c == parentCase.Id, 'Case ID on policy ' +policy.Case__c + ' should be the same as the case ID on the parent case ' +  parentCase.Id);
        System.assert(policy.ePolicy__c != null, 'Comnumber not set on policy' + policy.ePolicy__c); 
        System.assert(policy.ePolicy__c == parentCase.COM__c, 'Policy and Case policy numbers do not match' + policy.ePolicy__c + parentCase.COM__c);
        
        ApexPages.Standardcontroller standardQuoteController = new ApexPages.Standardcontroller(policy);
        SMEQuoteCtrl smeQuoteController = new SMEQuoteCtrl(standardQuoteController);
        
        smeQuoteController.BindQuote();
              
        }   
    }
    
}